/*******************************************************************************
 *  Copyright (C) 2007, 2014:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.launcher.engine;

import org.integratedmodelling.engine.main.RESTServer;

/**
 * For now just a proxy to RESTServer.main(). In the future, allow configuration,
 * upgrade checks and possibly make it authentication-aware.
 * 
 * Also used for debugging inside Eclipse, so do not remove just because it seems
 * useless.
 * 
 * @author Ferd
 *
 */
public class Main {

	public static void main(String[] args) throws Exception {
		
		/**
		 * TODO may wrap this in a launcher that first checks for upgrades.
		 */
		RESTServer.main(args);
	}

}
