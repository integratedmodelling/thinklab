Server distribution package, used in CI server to create the "official" server distributions, installers and update sites.

# Use for debugging (within Eclipse or any time client is launched from thinklab/client)

Client uses class DevelopmentServer to launch the server from here if a fully compiled source distribution is found relative to the current 
path, unless the thinklab.client.nodev configuration exists equals "true".

To update the jars in the distribution (transitive closure of dependencies for engine):

	mvn dependency:copy-dependencies
	
This will put the jars in target/dependencies and will need to be run before the development engine is run
for the first time and every time the engine dependencies change.
	
The resulting jar directory will contain the im-* jar files from all other modules in this same distribution. The development
launcher will prepend the correspondent target/classes paths to the classpath, so that the very latest class files (assuming
Eclipse keeps them current) are used.

# Building binary scripts and distribution:

mvn package appassembler:assemble (scripts are still default, i.e. don't work - must create and provide templates).

# Eclipse launcher

Create a Maven build launcher with exec:java -Dexec.mainClass="org.integratedmodelling.launcher.engine.Main" as a goal. Make sure
the project is eclipse-launcher and that "Resolve Workspace Artifacts" is checked. Also check "skip tests" which is currently not needed as there are no tests, but will allow faster startup as soon as tests exist.