/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.kserver.controller;

import java.io.File;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Set;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.integratedmodelling.api.annotations.ResourceService;
import org.integratedmodelling.api.configuration.IConfiguration;
import org.integratedmodelling.common.beans.Directory;
import org.integratedmodelling.common.beans.FileResource;
import org.integratedmodelling.common.beans.Node;
import org.integratedmodelling.common.beans.Status;
import org.integratedmodelling.common.configuration.KLAB;
import org.integratedmodelling.common.configuration.KLAB.BootMode;
import org.integratedmodelling.common.interfaces.IResourceService;
import org.integratedmodelling.common.network.API;
import org.integratedmodelling.common.network.Broadcaster;
import org.integratedmodelling.common.utils.FileUtils;
import org.integratedmodelling.engine.KLABEngine;
import org.integratedmodelling.engine.KLABModelingEngine;
import org.integratedmodelling.engine.KLABNodeEngine;
import org.integratedmodelling.exceptions.ThinklabIOException;
import org.integratedmodelling.kserver.Configuration;
import org.integratedmodelling.kserver.resources.services.DirectoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.ClassPathScanningCandidateComponentProvider;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.FileSystemResource;
import org.springframework.core.io.Resource;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.core.type.filter.AnnotationTypeFilter;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class KServerController {

    @Autowired
    ApplicationContext appContext;

    private static Log logger = LogFactory.getLog(KServerController.class);

    @Value("${server.port}")
    private int port;

    private Broadcaster advertiser;

    /*
     * extract core knowledge to a directory. Should obviously be generalized, but 
     * will become unnecessary as we can use /get/directory/im:ks:core.knowledge
     * for the same purposes.
     * 
     * The code below will also insert the Directory bean in the DirectoryService
     * catalog, so we can just remove the file extraction when it's time.
     */
    public static void extractKnowledge() throws Exception {

        File kdir = KLAB.CONFIG.getDataPath("knowledge");
        File sdir = KLAB.CONFIG.getDataPath("ssh");

        PathMatchingResourcePatternResolver resolver = new PathMatchingResourcePatternResolver();
        Resource[] resources = resolver.getResources("/knowledge/**");
        Resource[] keyring = resolver.getResources("/ssh/pubring.gpg");

        if (keyring.length > 0) {
            try (InputStream in = keyring[0].getURL().openStream()) {
                FileUtils.copyInputStreamToFile(in, new File(sdir + File.separator + "pubring.gpg"));
            } catch (Exception e) {
                throw new ThinklabIOException(e);
            }
        }

        Directory directory = new Directory();
        directory.setResources(new ArrayList<FileResource>());

        for (Resource resource : resources) {

            String path = null;
            if (resource instanceof FileSystemResource) {
                path = ((FileSystemResource) resource).getPath();
            } else if (resource instanceof ClassPathResource) {
                path = ((ClassPathResource) resource).getPath();
            }
            if (path == null) {
                throw new ThinklabIOException("internal: cannot establish path for resource " + resource);
            }

            if (!path.endsWith("owl")) {
                continue;
            }

            String filePath = path.substring(path.indexOf("knowledge/") + "knowledge/".length());

            if (resource instanceof FileSystemResource) {
                directory.getResources().add(FileResource
                        .newFromFile(filePath, ((FileSystemResource) resource).getPath(), resource
                                .lastModified()));
            } else if (resource instanceof ClassPathResource) {
                directory.getResources().add(FileResource
                        .newFromClasspath(filePath, ((ClassPathResource) resource).getPath(), resource
                                .lastModified()));
            }

            int pind = filePath.lastIndexOf('/');
            if (pind >= 0) {
                String fileDir = filePath.substring(0, pind);
                File destDir = new File(kdir + File.separator + fileDir);
                destDir.mkdirs();
            }
            File dest = new File(kdir + File.separator + filePath);
            InputStream is = resource.getInputStream();
            FileUtils.copyInputStreamToFile(is, dest);
            is.close();
        }

        DirectoryService.predefinedDirectories.put(API.CORE_KNOWLEDGE_URN, directory);
    }

    public static boolean setup(BootMode mode) {

        KLAB.setLogger(logger);
        System.setProperty(IConfiguration.THINKLAB_DATA_DIRECTORY_PROPERTY, Configuration
                .getApplicationPath());
        KLAB.CONFIG = new org.integratedmodelling.common.configuration.Configuration(false);

        try {
            // TODO check package name; also use any plug-in jars found at startup (which should declare the
            // package in manifest)
            scanClasspath("org.integratedmodelling");
            extractKnowledge();
            if (mode == BootMode.MODELER) {
                KLABModelingEngine.boot();
            } else if (mode == BootMode.NODE) {
                KLABNodeEngine.boot();                
            }
        } catch (Exception e) {
            return false;
        }

        return true;
    }

    /**
     * Start the multicast of our IP address and identity so that clients can discover us.
     * 
     * @param applicationId
     */
    public void startAdvertising(String applicationId) {
        KLAB.info("Starting multicast of IP with identity " + applicationId + " for port " + port);
        this.advertiser = new Broadcaster(applicationId, port);
    }

    @SuppressWarnings("unchecked")
    private static void scanClasspath(String packageId) {
        /*
         * TODO merge with current classpath scanning in engine and use this single loop for all components.
         * TODO have plug-in component declare their packages and allow component-scope scanning at load.
         */
        ClassPathScanningCandidateComponentProvider provider = new ClassPathScanningCandidateComponentProvider(false);
        provider.addIncludeFilter(new AnnotationTypeFilter(ResourceService.class));
        Set<BeanDefinition> beans = provider.findCandidateComponents(packageId);
        for (BeanDefinition bd : beans) {

            try {
                Class<?> cls = Class.forName(bd.getBeanClassName());
                ResourceService ziocan = cls.getAnnotation(ResourceService.class);
                if (ziocan != null && IResourceService.class.isAssignableFrom(cls)) {
                    GetResourceController.serviceRegistry
                            .put(ziocan.service(), (Class<? extends IResourceService>) cls);
                }
            } catch (ClassNotFoundException e) {
                // eccheccazzo
                continue;
            }
        }
    }

    @RequestMapping(value = API.CAPABILITIES, method = RequestMethod.GET)
    public Node capabilities() {
        Node ret = new Node();
        return ret;
    }

    @RequestMapping(value = API.STATUS, method = RequestMethod.GET)
    public Status status() {
        return KLABEngine.get().getEngineStatus();
    }

    public void shutdown() {
        if (advertiser != null) {
            advertiser.close();
        }
        KLABEngine.shutdown();
    }
}
