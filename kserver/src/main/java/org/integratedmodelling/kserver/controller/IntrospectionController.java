package org.integratedmodelling.kserver.controller;

import org.integratedmodelling.engine.geospace.Geospace;
import org.opengis.referencing.crs.CoordinateReferenceSystem;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class IntrospectionController {
    
    @RequestMapping(value = "/inspect/projection", method = RequestMethod.GET)
    public Object inspectProjection(@RequestParam(value = "id") String id) {
        CoordinateReferenceSystem crs = Geospace.getCRSFromID(id);
        return crs == null ? "" : crs.toWKT();
    }
}
