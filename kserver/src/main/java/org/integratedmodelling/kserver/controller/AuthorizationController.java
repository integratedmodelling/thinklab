package org.integratedmodelling.kserver.controller;

import org.integratedmodelling.api.auth.IAuthorizationProvider;
import org.integratedmodelling.api.modelling.IModelBean;
import org.integratedmodelling.common.network.API;
import org.integratedmodelling.exceptions.ThinklabException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class AuthorizationController {

    @Autowired
    IAuthorizationProvider provider;
    
    @RequestMapping(path=API.AUTHENTICATE, method=RequestMethod.POST, consumes="text/plain")
    IModelBean authenticate(@RequestBody String certificateContents) throws ThinklabException {
        return provider.authorize(certificateContents);
    }
    
}
