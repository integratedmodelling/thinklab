package org.integratedmodelling.kserver.controller;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URLConnection;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.IOUtils;
import org.apache.http.Header;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.client.methods.RequestBuilder;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.integratedmodelling.api.auth.IUser;
import org.integratedmodelling.api.modelling.IModelBean;
import org.integratedmodelling.common.interfaces.IResourceService;
import org.integratedmodelling.common.network.API;
import org.integratedmodelling.exceptions.ThinklabException;
import org.integratedmodelling.exceptions.ThinklabIOException;
import org.integratedmodelling.exceptions.ThinklabInternalErrorException;
import org.integratedmodelling.exceptions.ThinklabUnsupportedOperationException;
import org.springframework.http.HttpHeaders;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * Entry point for all the /get endpoints, and bridge to URN resolution and resource
 * retrieval handled by plug-in service.
 * 
 * @author ferdinando.villa
 *
 */
@RestController
public class GetResourceController {

    static Map<String, Class<? extends IResourceService>> serviceRegistry = new HashMap<>();

    @RequestMapping(value = API.GET_RESOURCE, method = RequestMethod.GET)
    @ResponseBody
    public Object getResourceByService(@PathVariable String service, @PathVariable String urn, @RequestHeader HttpHeaders headers, HttpServletRequest request, HttpServletResponse response)
            throws ThinklabException {

        Class<? extends IResourceService> servclass = serviceRegistry.get(service);
        if (servclass == null) {
            throw new ThinklabUnsupportedOperationException("resource access service " + service
                    + " not found in registry");
        }

        /*
         * TODO get user (groups actually) from headers (do we need an injected security policy?)
         */
        IUser user = null;

        try {
            IResourceService serv = servclass.newInstance();
            
            if (serv == null) {
                // unlikely indeed.
                throw new ThinklabUnsupportedOperationException("resource service " + service + " could not be created");
            }
            
            Object o = serv.resolveUrn(urn, user);

            if (o instanceof IModelBean) {
                return o;
            } else if (o instanceof File) {
                response.setContentType(URLConnection.guessContentTypeFromName(((File) o).getName()));
                try (InputStream in = new FileInputStream((File) o)) {
                    IOUtils.copy(in, response.getOutputStream());
                } catch (IOException e) {
                    throw new ThinklabIOException(e);
                }
            } else if (o instanceof URI) {
                HttpUriRequest proxiedRequest = createHttpUriRequest((URI) o, request);
                try (CloseableHttpClient client = HttpClients.createDefault();
                        CloseableHttpResponse proxiedResponse = client.execute(proxiedRequest)) {
                    writeToResponse(proxiedResponse, response);
                } catch (Exception e) {
                    throw new ThinklabIOException(e);
                }
            } else {
                throw new ThinklabUnsupportedOperationException("could not understand " + service + " service response for URN " + urn);
            }
        } catch (Exception e) {
            throw new ThinklabInternalErrorException(e);
        }

        return null;
    }

    private void writeToResponse(HttpResponse proxiedResponse, HttpServletResponse response) throws ThinklabIOException {
        for (Header header : proxiedResponse.getAllHeaders()) {
            if ((header.getName() != "Transfer-Encoding") || (header.getValue() != "chunked")) {
                response.addHeader(header.getName(), header.getValue());
            }
        }
        try (InputStream is = proxiedResponse.getEntity().getContent();
             OutputStream os = response.getOutputStream()) {
            IOUtils.copy(is, os);
        } catch (Exception e) {
            throw new ThinklabIOException(e); 
        }
    }

    private HttpUriRequest createHttpUriRequest(URI uri, HttpServletRequest request)
            throws URISyntaxException {

        RequestBuilder rb = RequestBuilder.create(request.getMethod());
        rb.setUri(uri);

        Enumeration<String> headerNames = request.getHeaderNames();
        while (headerNames.hasMoreElements()) {
            String headerName = headerNames.nextElement();
            String headerValue = request.getHeader(headerName);
            rb.addHeader(headerName, headerValue);
        }
        
        /*
         * add any parameters from request
         */
        for (String zio : request.getParameterMap().keySet()) {
            rb.addParameter(zio, request.getParameter(zio));
        }

        HttpUriRequest proxiedRequest = rb.build();
        return proxiedRequest;
    }

}
