package org.integratedmodelling.kserver.resources.services;

import java.io.File;

import org.integratedmodelling.api.annotations.ResourceService;
import org.integratedmodelling.api.auth.IUser;
import org.integratedmodelling.common.interfaces.IResourceService;
import org.integratedmodelling.common.utils.StringUtils;
import org.integratedmodelling.exceptions.ThinklabException;

@ResourceService(service = "file")
public class FileService implements IResourceService {

    /*
     * TODO move to bean factory or whatever (it's used as an inline string in FileResource bean).
     */
    public static final String URN_FILE_LOCAL_PREFIX = "local:file:";

    @Override
    public Object resolveUrn(String urn, IUser user) throws ThinklabException {

        Object inp = getFileFromUrn(urn, user);
        if (inp != null) {
            return inp;
        }
        
        return null;
    }

    private File getFileFromUrn(String urn, IUser user) {
        if (urn.startsWith(URN_FILE_LOCAL_PREFIX)) {
            String fn = urn.substring(URN_FILE_LOCAL_PREFIX.length());
            fn = StringUtils.replace(fn, "..", File.separator);
            return new File(fn);
        }
        return null;
    }

}
