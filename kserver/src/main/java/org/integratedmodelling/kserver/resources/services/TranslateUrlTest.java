package org.integratedmodelling.kserver.resources.services;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.HashMap;
import java.util.Map;

import org.integratedmodelling.api.annotations.ResourceService;
import org.integratedmodelling.api.auth.IUser;
import org.integratedmodelling.api.modelling.IModelBean;
import org.integratedmodelling.common.interfaces.IResourceService;
import org.integratedmodelling.exceptions.ThinklabException;

@ResourceService(service="translate")
public class TranslateUrlTest implements IResourceService {

    /**
     * Pre-filled at initialization with things that the engine makes available no matter
     * what, such as the knowledge map.
     */
    public static Map<String,IModelBean> predefinedDirectories = new HashMap<>();
    
    @Override
    public Object resolveUrn(String urn, IUser user) throws ThinklabException {
        try {
            return new URI("http://integratedmodelling.org/geoserver/wcs");
        } catch (URISyntaxException e) {
            return null;
        }
    }

}
