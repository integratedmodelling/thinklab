package org.integratedmodelling.kserver.resources;

import java.io.File;
import java.util.Set;

import org.integratedmodelling.api.network.IComponent;
import org.integratedmodelling.api.project.IProject;

public class ResourcePublisher {

    public String publishDirectory(File file, Set<String> groups) {
        return null;
    }

    public String publishFile(File file, Set<String> groups) {
        return null;
    }
    
    public String publishProject(IProject project, boolean isSynchronized, Set<String> groups) {
        return null;
    }

    public String publishComponent(IComponent component, Set<String> groups) {
        return null;
    }
}
