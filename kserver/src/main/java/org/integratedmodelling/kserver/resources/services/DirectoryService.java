package org.integratedmodelling.kserver.resources.services;

import java.util.HashMap;
import java.util.Map;

import org.integratedmodelling.api.annotations.ResourceService;
import org.integratedmodelling.api.auth.IUser;
import org.integratedmodelling.api.modelling.IModelBean;
import org.integratedmodelling.common.interfaces.IResourceService;
import org.integratedmodelling.exceptions.ThinklabException;

@ResourceService(service="directory")
public class DirectoryService implements IResourceService {

    /**
     * Pre-filled at initialization with things that the engine makes available no matter
     * what, such as the knowledge map.
     */
    public static Map<String,IModelBean> predefinedDirectories = new HashMap<>();
    
    @Override
    public Object resolveUrn(String urn, IUser user) throws ThinklabException {
        IModelBean ret = predefinedDirectories.get(urn);
        if (ret != null) {
            return ret;
        }
        return null;
    }

}
