/*******************************************************************************
 *  Copyright (C) 2007, 2014:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.launcher.client;

import org.integratedmodelling.client.console.Console;

/**
 * Dumb launcher. TODO wrap into upgrade checker etc.
 * 
 * Path forward:
 *  1. make this an independent, small GUI app with SWT jar, commons exec and nothing else.
 *     Have intro screen with current versions of everything, news etc. which is only shown if update is possible or
 *     necessary. Also set memory levels, debug levels and stuff (can write initial properties).
 *     May also want to adapt the .thinklab dir and the workspace, offering to reset/clean or save/restore previous.
 *  2. On startup, check both client and server distros against latest. If diffs are detected, pop up GUI and offer
 *     to update. If not updating, no diffs or not online, don't launch GUI and continue to 3.
 *  3. Use commons-exec to launch client from local distro (no scripts).
 *  4. Have other start script that brings up the GUI anyway (call it Thinklab updater or something).
 *  5. Eventually, also download configured Eclipse.
 * @author Ferd
 *
 */
public class Main {

    public static void main(String[] args) throws Exception {
        Console.main(args);
    }

}
