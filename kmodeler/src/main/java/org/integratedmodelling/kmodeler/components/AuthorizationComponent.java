package org.integratedmodelling.kmodeler.components;

import java.net.URL;

import org.integratedmodelling.api.auth.IAuthorizationProvider;
import org.integratedmodelling.common.auth.LicenseManager;
import org.integratedmodelling.common.beans.auth.AuthenticationResultResource;
import org.integratedmodelling.common.configuration.KLAB;
import org.integratedmodelling.common.network.API;
import org.integratedmodelling.exceptions.ThinklabException;
import org.integratedmodelling.exceptions.ThinklabIOException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

/**
 * Remote authorization component: uses the primary server as authentication endpoint
 * and just sends back what the remote server authorized. Honor any white or
 * blacklists.
 * 
 * @author ferdinando.villa
 *
 */
@Component
public class AuthorizationComponent implements IAuthorizationProvider {
    
    @Autowired
    private ApplicationContext appContext;
    
    // TODO this will need to be in the certificate but for now, certfiles have the k.LAB primary server and
    // they're different servers.
    String serverUrl = "https://integratedmodelling.org/collaboration";

    /**
     * FIXME this should just be passed the auth token by the client. Only one that should authenticate
     * with a certificate is the legitimate certificate owner for the engine. Other clients should 
     * authenticate with the certificate and send it here to validate and retrieve profile info from 
     * the authenticating endpoint. That would be enough security to open a session identified by the
     * same token.
     */
    @Override
    public AuthenticationResultResource authorize(String certificateContents) throws ThinklabException {

        if (serverUrl == null) {
            try {
                URL publicKey = appContext.getResource("classpath:keyring/pubring.gpg").getURL();
                LicenseManager.get().readCertificate(certificateContents, publicKey, new String[] {});
            } catch (Exception e) {
                throw new ThinklabIOException("error decrypting certificate");
            }
        }
        
        // pass through to engine
        RestTemplate restTemplate = new RestTemplate();
        AuthenticationResultResource ret = restTemplate.postForObject(serverUrl
                + API.AUTHENTICATION_CERT_FILE, certificateContents, AuthenticationResultResource.class);
        
        if (KLAB.NETWORK.getUser().getUsername().equals(ret.getUsername())) {
            ret.setExclusive(true);
        } else {

            /*
             * TODO check whitelists and blacklists for authorization; throw authorization exception if
             *      excluded.
             * TODO setExclusive() on ret if configuration says user can lock.
             */
            
        }
        
        return ret;
    }

}
