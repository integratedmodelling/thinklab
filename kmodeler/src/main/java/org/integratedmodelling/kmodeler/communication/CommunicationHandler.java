package org.integratedmodelling.kmodeler.communication;

import org.springframework.context.annotation.Configuration;
import org.springframework.messaging.simp.config.MessageBrokerRegistry;
import org.springframework.web.socket.TextMessage;
import org.springframework.web.socket.WebSocketSession;
import org.springframework.web.socket.config.annotation.AbstractWebSocketMessageBrokerConfigurer;
import org.springframework.web.socket.config.annotation.EnableWebSocketMessageBroker;
import org.springframework.web.socket.config.annotation.StompEndpointRegistry;
import org.springframework.web.socket.handler.TextWebSocketHandler;

@Configuration
@EnableWebSocketMessageBroker
public class CommunicationHandler extends AbstractWebSocketMessageBrokerConfigurer {

    // HUH
    public class WebsocketEndPoint extends TextWebSocketHandler {

        @Override
        protected void handleTextMessage(WebSocketSession session,
                TextMessage message) throws Exception {
            super.handleTextMessage(session, message);
            TextMessage returnMessage = new TextMessage(message.getPayload()+" received at server");
            session.sendMessage(returnMessage);
        }
    }
    
    @Override
    public void registerStompEndpoints(StompEndpointRegistry registry) {
        registry.addEndpoint("/message").withSockJS();
    }

    @Override
    public void configureMessageBroker(MessageBrokerRegistry config) {
        config.enableSimpleBroker("/task", "/global");
        config.setApplicationDestinationPrefixes("/control");
    }
}
