package org.integratedmodelling.kmodeler.communication;

import org.integratedmodelling.common.configuration.KLAB;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.Message;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class CommunicationController {
    
    private SimpMessagingTemplate template;
    
    @Autowired
    public CommunicationController(SimpMessagingTemplate template) {
        this.template = template;
    }
    
    /**
     * this gets message sent to /control/sperma
     * @param message
     * @return
     */
    @MessageMapping("/sperma/**")
    @SendTo("/global")
    public String handleTask(Message<?> message) {
        KLAB.info("HOSTIA: " + message);
        return "[" +  message + "]";
    }
    
//    @RequestMapping(path="/dispatch", method=RequestMethod.POST)
//    public void handleControl(@RequestBody String text) {
//        KLAB.info("HOSTIE: " + text);
//        this.template.convertAndSend("/control", text);
//    }
}
