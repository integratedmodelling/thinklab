/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.kmodeler;

import java.io.File;

import org.integratedmodelling.common.configuration.KLAB;

public class Configuration {

    public static String APPLICATION_PATH_SYSTEM_PROPERTY = "klab.data.dir";
    public static String SERVICE_PORT_SYSTEM_PROPERTY     = "klab.port";
    private static final String DEFAULT_APPLICATION_PATH = "/opt/klab";

    private static Integer servicePort;
    private static String  applicationPath;

    public static String getApplicationPath() {

        if (applicationPath == null) {
            applicationPath = System.getProperty(APPLICATION_PATH_SYSTEM_PROPERTY);
            if (applicationPath /* still */ == null) {
                applicationPath = DEFAULT_APPLICATION_PATH;
            }
        }

        File appath = new File(applicationPath);

        if (!appath.exists()) {
            try {
                appath.mkdirs();
            } catch (Throwable e) {
                KLAB.warn("could not create chosen application path " + appath);
            }

            if (!appath.exists()) {

                /*
                 * force to $HOME/.kserver
                 */
                appath = new File(System.getProperty("user.home") + File.separator + ".kserver");
                appath.mkdirs();
                applicationPath = appath.toString();

                KLAB.warn("fallback application path set to " + appath);
            }
        }

        return applicationPath;
    }

    public static int getServicePort() {
        if (servicePort == null) {
            String sp = System.getProperty(SERVICE_PORT_SYSTEM_PROPERTY);
            servicePort = sp == null ? null : Integer.parseInt(sp);
            // if (servicePort /* still */ == null) {
            // servicePort = Integer.parseInt(DEFAULT_SERVICE_PORT);
            // }
        }
        return servicePort;
    }

}
