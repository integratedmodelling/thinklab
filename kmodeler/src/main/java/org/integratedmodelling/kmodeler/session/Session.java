package org.integratedmodelling.kmodeler.session;

import java.io.IOException;
import java.io.InputStream;
import java.io.PrintStream;
import java.util.List;

import org.integratedmodelling.api.auth.IUser;
import org.integratedmodelling.api.monitoring.IMonitor;
import org.integratedmodelling.api.monitoring.INotification;
import org.integratedmodelling.api.runtime.IContext;
import org.integratedmodelling.api.runtime.ISession;
import org.integratedmodelling.api.runtime.ITask;
import org.integratedmodelling.common.utils.NameGenerator;

public class Session implements ISession {

    IUser   user;
    boolean isExclusive;
    String id;
    
    public Session(IUser user, boolean isExclusive) {
        this.isExclusive = isExclusive;
        this.user = user;
        this.id = NameGenerator.newName();
    }

    @Override
    public void setMonitor(IMonitor monitor) {
        // TODO Auto-generated method stub

    }

    @Override
    public IMonitor getMonitor() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public void close() throws IOException {
        // TODO Auto-generated method stub

    }

    @Override
    public PrintStream out() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public InputStream in() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public String getId() {
        return id;
    }

    @Override
    public IUser getUser() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public void notify(INotification notification) {
        // TODO Auto-generated method stub

    }

    @Override
    public List<INotification> getNotifications(boolean clear) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public IContext getContext() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public List<IContext> getContexts() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public IContext getContext(long contextId) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public List<ITask> getTasks() {
        // TODO Auto-generated method stub
        return null;
    }

}
