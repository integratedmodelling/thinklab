# README #

Tis project is the core module of the Thinklab semantic modeling software platform. It provides the semantic modeling language Thinklab, a modeling engine that implements it, and client infrastructure for modelers and developers.

The Thinklab software stack is divided in three main repositories:

1. thinklab (this one) which provides the main client and server functionalities, a command-line client, and is built with maven.
2. thinkql, which contains the parser for the modeling language used in Thinklab, and is based on XText, therefore is more naturally built and developed in Eclipse. The thinklab package uses artifacts produced in thinkql through a maven dependencies, therefore does not depend directly on it.
3. thinkcap, which contains Eclipse plug-ins and artifacts for the Thinkcap IDE. It depends on thinklab and is built only in Eclipse (no Maven setup yet).

In addition, the org.integratedmodelling.collaboration package implements the user cloud services that allow user and artifact registration and management for the im cluster. Sites that want to manage authentication and data warehousing for a Thinklab node can use it to implement their services. The package is independent from Thinklab and documented in the corresponding project.

* Building and developing

This package should be built simply by checking out the code and running 'mvn install'. It can be also built in Eclipse using the m2e Maven integration. Some of the Maven submodules in this package also double as OSGI plug-ins; their dependencies will only be found if 'mvn dependency:copy-dependencies' is run, so an Eclipse installation will initially show errors that will go away once this operation is done.

The master branch is the one from which the publicly distributed artifacts are built. This happens automatically at commit: nobody should commit to master unless explicitly authorized. Development happens according to the gitflow workflow and the main development branch is 'dev'. Developers should use dev as the base branch.

This project should build with Maven without any issues (if slowly due to the sheer amount of dependencies). In Eclipse, ensure that it is imported with "Scan nested projects" turned on to read in projects for all the submodules. As usual with m2e, a few cycles of manually issuing 'mvn install' and refreshing the project will be required before the two sides agree. Do not forget the 'copy-dependencies' target described above to ensure that the target/dependency dir is populated: this is not in the repository and the OSGI manifests in some of the plugins refer to it. 

* Dependencies

This project depends on a huge number of packages. Some of the versions are critical: for example, you may be tempted to update the geotools packages, but doing so will break crucial features in the jgrasstools package. In other instances, different versions will bring in dependencies that will clash with others in other packages, causing obscure and hard to debug problems, only on some architectures. So don't mess with the version numbers unless you know what you're doing.


* Configuration

The system manages its own internal databases and should need no configuration. All work files are kept under the THINKLAB WORK DIRECTORY under the user's $HOME, which defaults to '.thinklab'. That's also where properties and certificates are installed. You can keep independent installations by using the Java property -Dthinklab.work.directory=... in the launcher for the client (which will propagate it to the engine). Note however that the engine will run as a REST server on port 8182, so unless you redefine that port you cannot run more than one engine at the same time.

Setting thinklab.client.debug=on in the thinklab.properties file will make the engine run with debug enabled (when started by the client) and show a console where messages appear and introspection commands can be issued. Several points in the software honor this setting, so other things may happen from time to time (e.g. maps showing up on the screen).

When starting, the engine will use the THINKLAB_REST_PORT and THINKLAB_REST_CONTEXT environmental variables to override the default port and context name for the REST endpoint. These are by default set to 8182 and 'rest' respectively; the default url for the 'ping' service is therefore http://127.0.0.1:8182/rest/ (with the slash at the end).

* Configuring an Eclipse environment for development and debug

In order to develop and debug a thinklab/thinkcap installation on Eclipse, have the three projects above in your workspace, on the dev branch, and ensure everything compiles correctly. At that point what you need is to:

1. Launch thinkcap from Eclipse using an Eclipse Application launch configuration;
2. Have the latter launch the modeling engine (Thinklab) from the development directories and using the Eclipse-compiled classes, with debug enabled.

The first is achieved by creating an Eclipse application launcher; if every project is in the workspace, the Thinkcap perspective should be available and Thinkcap will launch when that is selected. When thinkcap initializes, it will start an engine. In order to have it start the "development engine" from your development directories, add the following to the VM parameters in the launcher:

	 -Dthinklab.source.distribution=<your GIT directory>\thinklab -Dthinklab.work.directory=.thinkdev

the second -D is not essential but I find it useful to redirect the work directory to something other than the default ($HOME/.tl) so that different installations don't interfere.

On Java 7 you'll also want to up the perm size (not necessary in Java 8). My full VM parameter line is 

-Dosgi.requiredJavaVersion=1.7 -Xms128m -Xmx1024m -XX:MaxPermSize=128M -Dthinklab.source.distribution=C:\Users\ferdinando.villa\git\thinklab -Dthinklab.work.directory=.thinkdev


If this is all done correctly, Thinkcap will launch an engine from your development directory and you'll see "Development engine" in the Engine view. This engine will not be launched with debug enabled unless you add the following line

	thinklab.client.debug = on

to the thinklab.properties file in the work directory ($HOME/.thinkdev if you have the above config). When that is done (and the engine is restarted), Thinkcap will launch the engine with remote debug configuration on port 8897. At that point you can debug it by connecting a remote debug session to that port.

* How to run tests

Test cases are sorely missing at the moment.

* Deployment instructions

All deployment is at the moment done through the IM continuous development system, and is set up for the IDE configuration so that fresh versions can automatically be synchronized. 

### Who do I talk to? ###

ferdinando.villa@bc3research.org
integrated.modelling@gmail.com