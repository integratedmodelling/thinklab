package org.integratedmodelling.test.engine;

import org.integratedmodelling.api.monitoring.IMonitor;
import org.junit.AfterClass;
import org.junit.BeforeClass;

/**
 * Base test class providing bootstrap and shutdown methods. If a thinklab.test.workspace 
 * property is passed, all the project in the directory points to are loaded at bootstrap.
 * 
 * @author ferdinando.villa
 *
 */
public class BaseEngineTest {

    // protected static IObservationKbox kbox;
    protected static IMonitor monitor;
    protected static String   projectDir = System.getProperty("thinklab.test.workspace");

    /**
     * Use to check if knowledge is available before running a test that needs it.
     * 
     * @param id
     * @return
     */
    protected static boolean check(String id) {
        return true;
        // return Env.MMANAGER.findModelObject(id) != null
        // || (SemanticType.validate(id) && Thinklab.get().getConcept(id) != null);
    }

    @BeforeClass
    public static void setUpBeforeClass() throws Exception {

        // Thinklab.boot();
        //
        // if (projectDir != null) {
        // Thinklab.get().registerProjectDirectory(new File(projectDir));
        // Thinklab.get().load(false, null);
        // }
    }

    @AfterClass
    public static void tearDownAfterClass() throws Exception {
        // // kbox = null;
        // Thinklab.shutdown();
    }
}
