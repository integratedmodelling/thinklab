/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.engine.time.extents;

import org.integratedmodelling.api.time.ITemporalExtent;
import org.integratedmodelling.api.time.ITemporalSeries;
import org.integratedmodelling.api.time.ITimeInstant;
import org.integratedmodelling.api.time.ITimePeriod;
import org.integratedmodelling.engine.modelling.TemporalSeries;
import org.integratedmodelling.engine.time.literals.PeriodValue;
import org.integratedmodelling.exceptions.ThinklabException;

public class TimeHelper {
    public static ITimeInstant max(ITimeInstant t1, ITimeInstant t2) {
        if (t1.compareTo(t2) > 0) {
            return t1;
        }
        return t2;
    }

    public static ITimeInstant min(ITimeInstant t1, ITimeInstant t2) {
        if (t1.compareTo(t2) < 0) {
            return t1;
        }
        return t2;
    }

    public static ITemporalExtent getConstrainedTemporalExtent(ITemporalExtent extentToConstrain,
            ITemporalExtent constrainingExtent) throws ThinklabException {
        ITemporalExtent result;
        if (extentToConstrain == null) {
            result = constrainingExtent.collapse();
        } else if (constrainingExtent == null) {
            result = extentToConstrain;
        } else {
            ITimePeriod collapsedRootScale = constrainingExtent.collapse();

            // get the start/end of the result extent. As long as they are not null, they will have
            // meaningful values (INFINITE == very big number, NEGATIVE_INFINITE == very small number)
            ITimeInstant startTime = max(extentToConstrain.getStart(), collapsedRootScale.getStart());
            ITimeInstant endTime = min(extentToConstrain.getEnd(), collapsedRootScale.getEnd());

            // initialize a single time period covering the whole result extent
            ITemporalSeries<Object> newTemporalSeries = new TemporalSeries<Object>();
            newTemporalSeries.put(new PeriodValue(startTime.getMillis(), endTime.getMillis()), new Object());

            for (int i = 0; i < extentToConstrain.getValueCount(); i++) {
                ITimeInstant pStart = extentToConstrain.getExtent(i).getStart();
                if (pStart.compareTo(endTime) >= 0) {
                    // we're outside the range of our overall time period, so stop processing.
                    break;
                }
                if (pStart.compareTo(startTime) > 0) {
                    // bisect the appropriate time segment in the Temporal Scale
                    newTemporalSeries.bisect(pStart, new Object());
                }
            }
            result = newTemporalSeries;
        }

        return result;
    }
}
