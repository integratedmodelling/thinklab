/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.engine.time;

import java.io.File;

import org.integratedmodelling.api.knowledge.IConcept;
import org.integratedmodelling.api.modelling.IScale;
import org.integratedmodelling.api.modelling.IState;
import org.integratedmodelling.common.configuration.KLAB;
import org.integratedmodelling.common.vocabulary.NS;
import org.integratedmodelling.engine.KLABEngine;

/**
 * @author Ferd, Luke
 * 
 * Will have non-static methods to persist temporal states and the like.
 *
 */
public class Time {

    private static Time _this = null;

    public static final String PLUGIN_ID = "org.integratedmodelling.thinklab.time";

    public static final String CONTINUOUS_TIME_OBSERVABLE_INSTANCE = "time:ContinuousTimeObservableInstance";
    public static final String ABSOLUTE_TIME_OBSERVABLE_INSTANCE   = "time:AbsoluteTimeObservableInstance";

    public static final String DATETIME_TYPE_ID     = getProperty("DateTimeTypeID", "time:DateTimeValue");
    public static final String TIMERECORD_TYPE_ID   = getProperty("TemporalLocationRecordTypeID", "time:TemporalLocationRecord");
    public static final String TEMPORALGRID_TYPE_ID = getProperty("TemporalGridTypeID", "time:RegularTemporalGrid");
    public static final String PERIOD_TYPE_ID       = getProperty("PeriodTypeID", "time:PeriodValue");
    public static final String DURATION_TYPE_ID     = getProperty("DurationTypeID", "time:DurationValue");

    public static final String STARTS_AT_PROPERTY_ID = getProperty("StartsAtPropertyID", "time:startsAt");
    public static final String ENDS_AT_PROPERTY_ID   = getProperty("EndsAtPropertyID", "time:endsAt");
    public static final String STEP_SIZE_PROPERTY_ID = getProperty("StepSizePropertyID", "time:inStepsOf");

    public static final IConcept TIME_GRID_CONCEPT   = KLABEngine
            .c(TEMPORALGRID_TYPE_ID);
    public static final IConcept TIME_RECORD_CONCEPT = KLABEngine
            .c(TIMERECORD_TYPE_ID);
    public static final IConcept DATE_TIME_CONCEPT   = KLABEngine.c(DATETIME_TYPE_ID);
    public static final IConcept DURATION_CONCEPT    = KLABEngine.c(DURATION_TYPE_ID);
    public static final IConcept TIME_DOMAIN         = KLABEngine.c(NS.TIME_DOMAIN);

    // public static final ISemanticObject<?> absoluteTimeInstance = null; //
    // km.requireInstance(ABSOLUTE_TIME_OBSERVABLE_INSTANCE);
    // public static final ISemanticObject<?> continuousTimeInstance = null; //
    // km.requireInstance(CONTINUOUS_TIME_OBSERVABLE_INSTANCE);

    private static String getProperty(String key1, String key2) {
        return KLAB.CONFIG.getProperties().getProperty(key1, key2);
    }

    public static IConcept domainConcept() {
        return TIME_DOMAIN;
    }

    public static Time get() {

        if (_this == null) {
            _this = new Time();
        }
        return _this;
    }

    /**
     * Called upon to persist a temporally distributed, non-spatial state to a
     * sensible format. Should probably be a CSV file with data in the first column.
     *
     * @param state
     * @param file
     */
    public void persistState(IState state, File file, Iterable<IScale.Locator> locators) {
        // TODO Auto-generated method stub

    }
}
