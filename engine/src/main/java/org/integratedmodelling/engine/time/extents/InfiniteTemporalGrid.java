/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.engine.time.extents;

import java.util.Iterator;

import org.integratedmodelling.api.knowledge.IConcept;
import org.integratedmodelling.api.knowledge.IKnowledge;
import org.integratedmodelling.api.knowledge.IProperty;
import org.integratedmodelling.api.metadata.IMetadata;
import org.integratedmodelling.api.modelling.IExtent;
import org.integratedmodelling.api.modelling.IObservable;
import org.integratedmodelling.api.modelling.IObserver;
import org.integratedmodelling.api.modelling.IScale;
import org.integratedmodelling.api.modelling.IScale.Index;
import org.integratedmodelling.api.modelling.IScale.Locator;
import org.integratedmodelling.api.modelling.ITopologicallyComparable;
import org.integratedmodelling.api.modelling.scheduling.ITransition;
import org.integratedmodelling.api.modelling.storage.IStorage;
import org.integratedmodelling.api.space.ISpatialExtent;
import org.integratedmodelling.api.time.ITemporalExtent;
import org.integratedmodelling.api.time.ITimeDuration;
import org.integratedmodelling.api.time.ITimeInstant;
import org.integratedmodelling.api.time.ITimePeriod;
import org.integratedmodelling.collections.Pair;
import org.integratedmodelling.common.time.TimeLocator;
import org.integratedmodelling.common.vocabulary.NS;
import org.integratedmodelling.engine.KLABEngine;
import org.integratedmodelling.engine.time.Time;
import org.integratedmodelling.engine.time.literals.DurationValue;
import org.integratedmodelling.exceptions.ThinklabException;

public class InfiniteTemporalGrid implements ITemporalExtent {

    private final ITimeDuration resolution;

    public InfiniteTemporalGrid(long stepMs) {
        resolution = new DurationValue(stepMs);
    }

    public ITimeDuration getResolution() {
        return resolution;
    }

    @Override
    public int[] getDimensionSizes() {
        return new int[] { (int) getMultiplicity() };
    }

    @Override
    public IKnowledge getType() {
        return getObservable().getType();
    }

    @Override
    public int[] getDimensionOffsets(int linearOffset, boolean rowFirst) {
        return new int[] { linearOffset };
    }

    @Override
    public ITimeInstant getStart() {
        return null;
    }

    @Override
    public ITimeInstant getEnd() {
        return null;
    }

    @Override
    public boolean isSpatiallyDistributed() {
        return false;
    }

    @Override
    public boolean isTemporallyDistributed() {
        return true;
    }

    @Override
    public ITemporalExtent intersection(IExtent other) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public IExtent union(IExtent other) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public boolean contains(IExtent o) throws ThinklabException {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public boolean overlaps(IExtent o) throws ThinklabException {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public boolean intersects(IExtent o) throws ThinklabException {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public ITimePeriod collapse() {
        return null;
    }

    @Override
    public ITemporalExtent getExtent(int index) {
        return null;
    }

    @Override
    public boolean isCovered(int stateIndex) {
        return true;
    }

    @Override
    public IConcept getDomainConcept() {
        return Time.TIME_DOMAIN;
    }

    @Override
    public IObserver getObserver() {
        return null;
    }

    @Override
    public IProperty getDomainProperty() {
        return KLABEngine.p(NS.TEMPORAL_EXTENT_PROPERTY);
    }

    @Override
    public boolean isTemporal() {
        return true;
    }

    @Override
    public boolean isSpatial() {
        return false;
    }

    @Override
    public ISpatialExtent getSpace() {
        return null;
    }

    @Override
    public ITemporalExtent getTime() {
        return this;
    }

    @Override
    public boolean isConsistent() {
        return true;
    }

    @Override
    public IProperty getCoverageProperty() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public ITemporalExtent merge(IExtent extent, boolean force) throws ThinklabException {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public Pair<ITopologicallyComparable<?>, Double> checkCoverage(ITopologicallyComparable<?> obj) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public ITopologicallyComparable<IExtent> union(ITopologicallyComparable<?> other)
            throws ThinklabException {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public ITopologicallyComparable<IExtent> intersection(ITopologicallyComparable<?> other)
            throws ThinklabException {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public double getCoveredExtent() {
        // TODO Auto-generated method stub
        return 0;
    }

    @Override
    public boolean isEmpty() {
        return false;
    }

    @Override
    public long getMultiplicity() {
        // this is inherited from ITopology, but in this context it means the same thing as 'value count'
        return getValueCount();
    }

    @Override
    public long getValueCount() {
        return INFINITE;
    }

    @Override
    public IMetadata getMetadata() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public IObservable getObservable() {
        return null;
    }

    @Override
    public IScale getScale() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public Object getValue(int index) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public Iterator<Object> iterator(Index index) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public ITemporalExtent getExtent() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public ITimeDuration getStep() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public IStorage<?> getStorage() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public int locate(Locator locator) {
        if (locator == null)
            return 0;
        if (locator instanceof ITransition) {
            return ((ITransition) locator).getTimeIndex();
        }
        if (locator instanceof TimeLocator) {
            return ((TimeLocator) locator).isAll() ? GENERIC_LOCATOR : ((TimeLocator) locator).getSlice();
        }
        return INAPPROPRIATE_LOCATOR;
    }

    @Override
    public Mediator getMediator(IExtent extent, IObservable observable, IConcept trait) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public boolean isConstant() {
        return getMultiplicity() == 1;
    }

}
