/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.engine.runtime;

import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.integratedmodelling.api.knowledge.IConcept;
import org.integratedmodelling.api.modelling.IActiveSubject;
import org.integratedmodelling.api.modelling.ICoverage;
import org.integratedmodelling.api.modelling.IDirectActiveObservation;
import org.integratedmodelling.api.modelling.IDirectObserver;
import org.integratedmodelling.api.modelling.IExtent;
import org.integratedmodelling.api.modelling.IModel;
import org.integratedmodelling.api.modelling.IModelObject;
import org.integratedmodelling.api.modelling.INamespace;
import org.integratedmodelling.api.modelling.IObservable;
import org.integratedmodelling.api.modelling.IObservation;
import org.integratedmodelling.api.modelling.IState;
import org.integratedmodelling.api.modelling.ISubject;
import org.integratedmodelling.api.modelling.agents.IObservationGraphNode;
import org.integratedmodelling.api.modelling.resolution.IResolutionContext;
import org.integratedmodelling.api.modelling.visualization.IMedia;
import org.integratedmodelling.api.monitoring.IMonitor;
import org.integratedmodelling.api.monitoring.IMonitorable;
import org.integratedmodelling.api.monitoring.IReport;
import org.integratedmodelling.api.runtime.IContext;
import org.integratedmodelling.api.runtime.ISession;
import org.integratedmodelling.api.runtime.ITask;
import org.integratedmodelling.common.configuration.KLAB;
import org.integratedmodelling.common.reporting.Report;
import org.integratedmodelling.common.states.State;
import org.integratedmodelling.common.utils.MapUtils;
import org.integratedmodelling.common.utils.Path;
import org.integratedmodelling.common.utils.ZipUtils;
import org.integratedmodelling.common.vocabulary.Observable;
import org.integratedmodelling.engine.KLABEngine;
import org.integratedmodelling.engine.ModelFactory;
import org.integratedmodelling.engine.modelling.TemporalCausalGraph;
import org.integratedmodelling.engine.modelling.monitoring.Monitor;
import org.integratedmodelling.engine.modelling.resolver.ResolutionContext;
import org.integratedmodelling.engine.modelling.runtime.DirectObservation;
import org.integratedmodelling.engine.modelling.runtime.Subject;
import org.integratedmodelling.engine.visualization.VisualizationFactory;
import org.integratedmodelling.exceptions.ThinklabException;
import org.integratedmodelling.exceptions.ThinklabRuntimeException;
import org.integratedmodelling.exceptions.ThinklabValidationException;
import org.integratedmodelling.lang.IRemoteSerializable;

import com.google.common.io.Files;

/**
 * Manages observation and resolution. Initialized with a subject observer (and a session/monitor). Holds
 * the root subject and the correspondent resolution context. Allows contextualization for all the dependent
 * observations.
 *  
 * @author Ferd
 * 
 */
public class Context implements IContext, IMonitorable, IRemoteSerializable {

    // values for _status
    final static int EMPTY          = 0;
    final static int DEFINED        = 1;
    final static int INITIALIZED    = 2;
    final static int RUNNING        = 3;
    final static int CONTEXTUALIZED = 4;
    final static int ERROR          = -1;
    final static int INTERRUPTED    = -2;

    /*
     * Current subject, observer and coverage - modified at each observation
     */
    ISubject  subject;
    ICoverage coverage;

    // this is set only when the object is created and is passed along to copies
    long               contextId;
    int                status               = EMPTY;
    int                currentTime          = -1;
    boolean            reset                = false;
    volatile Boolean   paused               = false;
    IResolutionContext resolutionContext    = null;
    String             deferredObservableId = null;
    boolean            isDeferred           = false;
    /*
     * these don't change or only change explicitly from the API
     */
    ISession           session;
    Monitor            monitor;
    List<String>       scenarioIds          = new ArrayList<String>();
    INamespace         namespace;
    String             name;
    Set<String>        breakpoints          = new HashSet<String>();
    IReport            report               = new Report();

    private long               timeOfLastChange = new Date().getTime();
    private IDirectObserver    rootObserver;
    // forcings to apply to scale if deferred initialization is requested and not null.
    private ArrayList<IExtent> forcings         = null;

    static Long ___ID = 1L;

    private static long nextId() {
        synchronized (___ID) {
            return ___ID++;
        }
    }

    public IReport getReport() {
        return report;
    }

    class RunThread implements Runnable {

        @Override
        public void run() {
            status = RUNNING;
            try {
                /**
                 * TODO install listener to be notified of structural changes
                 */
                ((IActiveSubject) subject).contextualize();
            } catch (ThinklabException e) {
                monitor.error(e);
                status = ERROR;
                return;
            }
            status = monitor.isStopped() ? INTERRUPTED : CONTEXTUALIZED;
            if (status == CONTEXTUALIZED) {
                monitor.info("coverage from observation of " + subject.getId() + " is "
                        + coverage.getCoverage(), null);
            }
        }
    }

    /**
     * If modeling with the API, use this one with the ID of a known
     * subject observer to create a context.
     * 
     * @param subjectObserverId
     * @return
     * @throws ThinklabException 
     */
    public static IContext create(String subjectObserverId) throws ThinklabException {

        ISession session = new Session();
        IMonitor monitor = new Monitor(nextId(), session);
        IModelObject obj = KLAB.MMANAGER.findModelObject(subjectObserverId);

        if (!(obj instanceof IDirectObserver)) {
            throw new ThinklabRuntimeException("cannot create a context from anything other than an observation: "
                    + subjectObserverId);
        }

        Context ret = new Context(session, monitor, (IDirectObserver) obj);
        return ret.observe();
    }

    public Context(ISession session, IMonitor monitor, IDirectObserver observer) throws ThinklabException {
        this.session = session;
        this.namespace = ((Session) session).getNamespace();
        this.name = observer.getId();
        this.monitor = (Monitor) monitor;
        this.contextId = nextId();
        this.rootObserver = observer;
        this.subject = (ISubject) KLAB.MFACTORY.createSubject(observer, null, monitor);
        this.resolutionContext = ResolutionContext.root(subject, null, monitor, null);
        ((Monitor) monitor).setReport(report);
    }

    public Context(ISession session, IMonitor monitor, String observerId, ArrayList<IExtent> forcings)
            throws ThinklabException {
        this.session = session;
        this.namespace = ((Session) session).getNamespace();
        this.name = Path.getLast(observerId, '.');
        this.monitor = (Monitor) monitor;
        this.deferredObservableId = observerId;
        this.contextId = nextId();
        this.isDeferred = true;
        this.forcings = (forcings == null || forcings.size() == 0) ? null : forcings;
        ((Monitor) monitor).setReport(report);

    }

    public void resolveObservable() throws ThinklabException {

        KLAB.PMANAGER.load(false, KLAB.MFACTORY.getRootParsingContext());
        IModelObject obs = KLAB.MMANAGER.findModelObject(deferredObservableId);
        if (!(obs instanceof IDirectObserver)) {
            throw new ThinklabValidationException("cannot find observer " + deferredObservableId);
        }
        rootObserver = (IDirectObserver) obs;
        if (forcings != null) {
            rootObserver = ModelFactory.forceScale(rootObserver, forcings);
        }
        subject = (ISubject) KLAB.MFACTORY.createSubject(rootObserver, null, monitor);
        resolutionContext = ResolutionContext.root(subject, null, monitor, null);
        isDeferred = false;
    }

    public Context(Context c) {

        /**
         * TODO some of these probably must be deep copies - particularly the observer.
         */
        this.name = c.name;
        this.coverage = c.coverage;
        this.monitor = c.monitor;
        this.namespace = c.namespace;
        this.session = c.session;
        this.status = c.status;
        this.contextId = c.contextId;
        this.subject = c.subject;
        this.currentTime = c.currentTime;
        this.reset = c.reset;
        this.rootObserver = c.rootObserver;
        this.resolutionContext = c.resolutionContext;
        this.breakpoints.addAll(c.breakpoints);
        this.isDeferred = c.isDeferred;
        this.deferredObservableId = c.deferredObservableId;
    }

    private boolean isPaused() {
        synchronized (paused) {
            return paused;
        }
    }

    public void pause(boolean status) {
        synchronized (paused) {
            paused = status;
        }
    }

    /**
     * Use to create the main subject or a single contextual observation in an existing one.
     * 
     * @param observableDefinition
     * @return
     */
    @Override
    public ITask observeAsynchronous() {

        Task ret = null;
        ret = observeInternal();
        ret.start();
        reset = false;
        // FIXME move to task
        timeOfLastChange = new Date().getTime();
        return ret;
    }

    /*
     * convenience - calls the observation on the target subject.
     */
    public ITask observeAsynchronous(Object observable, ISubject targetSubject)
            throws ThinklabValidationException {

        Task ret = null;
        ret = observeInternal(observable, targetSubject);
        ret.start();
        reset = false;
        // FIXME move to task
        timeOfLastChange = new Date().getTime();
        return ret;
    }

    /**
     * Call after critical operations to decide whether to continue.
     * 
     * @return
     */
    public boolean hasErrors() {
        return monitor.hasErrors();
    }

    public Context withScenario(Object... scenarios) throws ThinklabValidationException {

        scenarioIds.clear();

        for (Object o : scenarios) {
            if (o instanceof String) {
                scenarioIds.add((String) o);
            } else if (o instanceof INamespace && ((INamespace) o).isScenario()) {
                scenarioIds.add(((INamespace) o).getId());
            } else {
                throw new ThinklabValidationException("cannot use " + o + "  as a scenario");
            }
        }
        return this;
    }

    /**
     * Observe anything either in the current context or as a main observation. Pass only one subject
     * generator (or its ID), or however many things to observe in the current subject; if doing the latter,
     * you should call this indirectly with the with() semantics.
     * 
     * @param observable
     * @return
     */
    private Task observeInternal() {
        return new Task(this, monitor, reset, scenarioIds);
    }

    private Task observeInternal(Object observable, ISubject target) {
        return new Task(this, monitor, reset, scenarioIds, observable, target);
    }

    public static String describeObservable(Object observable) {

        if (observable instanceof IModel) {
            return "model " + ((IModel) observable).getName();
        } else if (observable instanceof IConcept) {
            return "concept " + observable;
        } else if (observable instanceof IDirectObserver) {
            return "object " + ((IDirectObserver) observable).getName();
        } else if (observable instanceof IObservable) {
            return ((IObservable) observable).getFormalName();
        }
        return observable.toString();
    }

    @Override
    public IMonitor getMonitor() {
        return monitor;
    }

    public List<String> getScenarios() {
        return scenarioIds;
    }

    public TemporalCausalGraph<IDirectActiveObservation, IObservationGraphNode> getCausalGraph() {
        return ((Subject) subject).getCausalGraph();
    }

    public IState getState(Object o) {

        /**
         * TODO allow to search state by observer, model or anything else useful. TODO ensure things work
         * properly if called during run - must return most recent state without conflicts.
         */
        IConcept c = o instanceof IConcept ? (IConcept) o : KLABEngine.c(o.toString());

        for (IState s : subject.getStates()) {
            if (s.getObservable().is(c))
                return s;
        }

        return null;
    }

    public static Context create(IDirectObserver sg, ISession session, IMonitor monitor)
            throws ThinklabException {
        return new Context(session, monitor, sg);
    }

    /**
     * Create a context that will defer resolution of the observable to the task started by
     * observeAsynchronous().
     *  
     * @param sg the subject observer ID. The project manager will be refreshed before the name is resolved.
     * @param session
     * @param monitor
     * @param forcings 
     * @return
     * @throws ThinklabException
     */
    public static Context createDeferred(String sg, ISession session, IMonitor monitor, ArrayList<IExtent> forcings)
            throws ThinklabException {
        return new Context(session, monitor, sg, forcings);
    }

    @Override
    public void setMonitor(IMonitor monitor) {
        this.monitor = (Monitor) monitor;
    }

    @Override
    public boolean isEmpty() {
        return status == EMPTY;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public boolean isFinished() {
        return status == CONTEXTUALIZED;
    }

    @Override
    public boolean isRunning() {
        return status == RUNNING;
    }

    @Override
    public IContext inScenario(String... scenarios) {

        Context ret = new Context(this);

        for (String s : scenarios) {
            ret.scenarioIds.add(s);
        }

        return ret;
    }

    @Override
    public ITask runAsynchronous() {

        Task ret = new Task(this, monitor);
        ret.start();
        return ret;
    }

    @Override
    public ICoverage getCoverage() {
        return coverage;
    }

    @Override
    public Object adapt() {
        return MapUtils
                .of("coverage", coverage == null ? 0.0
                        : coverage.getCoverage(), "context", contextId, "name", name, "session", session
                                .getId(), "scenarios", scenarioIds, "subject", subject, "timestamp", timeOfLastChange, "current", currentTime);
    }

    // @Override
    // public ITask setTime(int time) {
    // // TODO must step sequentially
    // return null;
    // }

    @Override
    public long getId() {
        return contextId;
    }

    public int getCurrentTimeIndex() {
        return currentTime;
    }

    public long getLastChangeTimestamp() {
        return timeOfLastChange;
    }

    /**
     * Return the observation indexed by path, either a IState or ISubject. If not found return null with no
     * error.
     * 
     * @param path
     * @return
     */
    @Override
    public IObservation get(String path) {
        return subject == null ? null : ((Subject) subject).get(path);
    }

    @Override
    public ISubject getSubject() {
        return subject;
    }

    @Override
    public IContext observe() {

        if (isEmpty()) {
            return this;
        }

        ITask task = observeAsynchronous();
        return task.finish();
    }

    @Override
    public List<ITask> getTasks() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public void persist(File file, String path, IMedia.Type mediaType, Object... options)
            throws ThinklabException {
        if (path != null) {
            IObservation obs = this.get(path);
            VisualizationFactory.get().persist(obs, file, mediaType, options);
        } else {

            mediaType = IMedia.Type.DATA;
            File tempDir = Files.createTempDir();

            for (IState s : subject.getStates()) {
                if (s.isSpatiallyDistributed()) {
                    File outfile = new File(tempDir + File.separator
                            + ((Observable) s.getObservable()).getExportFileName()
                            + ".tif");
                    VisualizationFactory.get().persist(s, outfile, mediaType, options);
                }
            }

            ZipUtils.zip(file, tempDir, false, false);
        }

    }

    @Override
    public IContext resetContext(boolean reset) {
        this.reset = reset;
        return this;
    }

    public IDirectObserver getRootSubjectGenerator() {
        return rootObserver;
    }

    public void setBreakpoint(IObservation obs) {
        if (obs instanceof State) {
            breakpoints.add(((State) obs).getInternalId());
        }
    }

    public boolean breakpointReached(Collection<IObservation> modifiedObservations) {

        if (breakpoints.size() > 0) {
            for (IObservation o : modifiedObservations) {
                if (o instanceof State && breakpoints.contains(((State) o).getInternalId())) {
                    return true;
                }
            }
        }
        return false;
    }

    public void waitForResume() {

        pause(true);

        for (;;) {
            if (!isPaused()) {
                return;
            }
            try {
                Thread.sleep(300);
            } catch (InterruptedException e) {
                // nah
            }
        }
    }

    @Override
    public IContext run() throws ThinklabException {
        ITask task = runAsynchronous();
        return task.finish();
    }

    IObservation getParent(IObservation observation) {
        if (observation instanceof State) {
            return ((State) observation).getContext();
        } else if (observation instanceof DirectObservation) {
            return ((DirectObservation) observation).getContextSubject();
        }
        return null;
    }

    String getId(IObservation observation) {
        if (observation instanceof State) {
            return ((State) observation).getInternalId();
        } else if (observation instanceof DirectObservation) {
            return ((DirectObservation) observation).getInternalID();
        }
        return null;
    }

    @Override
    public String getPathFor(IObservation observation) {

        IObservation parent = getParent(observation);
        String ret = parent == null ? "" : getId(observation);

        // we don't want the root ID in the path.
        while (parent != null && getParent(parent) != null) {
            ret = getId(parent) + "/" + ret;
            parent = getParent(parent);
        }

        return "/" + ret;
    }

    @Override
    public ISession getSession() {
        return session;
    }
}
