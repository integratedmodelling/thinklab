/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.engine.runtime;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.integratedmodelling.api.monitoring.IMonitor;
import org.integratedmodelling.api.monitoring.Messages;
import org.integratedmodelling.api.runtime.ISession;
import org.integratedmodelling.api.runtime.ITask;
import org.integratedmodelling.common.monitoring.Notification;
import org.integratedmodelling.lang.IRemoteSerializable;

public abstract class AbstractBaseTask extends Thread implements ITask, IRemoteSerializable {

    static Long ___ID = 1L;

    private static long nextId() {
        synchronized (___ID) {
            return ___ID++;
        }
    }

    protected volatile ITask.Status _status = Status.RUNNING;
    protected long                  _taskId;
    protected IMonitor              _monitor;
    protected String                _description;
    protected ISession              _session;
    protected long                  _startTime;
    protected long                  _endTime;

    protected AbstractBaseTask(IMonitor monitor) {
        _session = monitor.getSession();
        _taskId = nextId();
        _monitor = monitor;
        _description = "";
        _startTime = new Date().getTime();
    }

    @Override
    public final long getTaskId() {
        return _taskId;
    }

    @Override
    public String getCommand() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public final String getDescription() {
        return _description;
    }

    @Override
    public final void interrupt() {
        _monitor.stop(this);
        synchronized (_status) {
            _status = Status.INTERRUPTED;
        }
        _endTime = new Date().getTime();
        _monitor.send(new Notification(Messages.TASK_INTERRUPTED, _session, this));

    }

    protected Object finish(Object ret) {
        for (;;) {
            synchronized (_status) {
                if (_status == Status.FINISHED) {
                    break;
                }
                try {
                    Thread.sleep(200);
                } catch (InterruptedException e) {
                }
            }
        }
        return ret;
    }

    @Override
    public Map<String, Object> adapt() {

        Map<String, Object> ret = new HashMap<String, Object>();

        ret.put("status", _status.toString());
        ret.put("id", _taskId);
        ret.put("description", _description);
        ret.put("start-time", _startTime);
        ret.put("end-time", _endTime);
        // ret.put("temporal", _runTask ? "true" : "false");
        ret.put("session-id", _session == null ? null : _session.getId());
        // if (_context != null) {
        // if (_isInitial) {
        // ret.put("context", _context.adapt());
        // } else {
        // ret.put("contextId", _context.getId());
        // }
        // }
        return ret;
    }

    @Override
    public final Status getStatus() {
        synchronized (_status) {
            return _status;
        }
    }

    @Override
    public final long getStartTime() {
        return _startTime;
    }

    @Override
    public final long getEndTime() {
        return _endTime;
    }

    @Override
    public final String getSessionId() {
        return _session == null ? null : _session.getId();
    }

}
