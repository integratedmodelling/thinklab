/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.engine.runtime;

import java.util.Date;
import java.util.List;
import java.util.Map;

import org.integratedmodelling.api.knowledge.IConcept;
import org.integratedmodelling.api.knowledge.IProperty;
import org.integratedmodelling.api.modelling.IActiveSubject;
import org.integratedmodelling.api.modelling.IDirectObserver;
import org.integratedmodelling.api.modelling.IKnowledgeObject;
import org.integratedmodelling.api.modelling.IModel;
import org.integratedmodelling.api.modelling.IObservable;
import org.integratedmodelling.api.modelling.ISubject;
import org.integratedmodelling.api.monitoring.IMonitor;
import org.integratedmodelling.api.monitoring.Messages;
import org.integratedmodelling.api.runtime.IContext;
import org.integratedmodelling.api.runtime.ITask;
import org.integratedmodelling.common.configuration.KLAB;
import org.integratedmodelling.common.monitoring.Notification;
import org.integratedmodelling.common.owl.Knowledge;
import org.integratedmodelling.common.utils.CamelCase;
import org.integratedmodelling.common.utils.MiscUtilities;
import org.integratedmodelling.common.vocabulary.NS;
import org.integratedmodelling.common.vocabulary.Observable;
import org.integratedmodelling.engine.KLABEngine;
import org.integratedmodelling.engine.introspection.CallTracer;
import org.integratedmodelling.engine.modelling.monitoring.Monitor;
import org.integratedmodelling.engine.modelling.runtime.Subject;
import org.integratedmodelling.engine.scripting.ModelProxy;
import org.integratedmodelling.exceptions.ThinklabException;
import org.integratedmodelling.exceptions.ThinklabValidationException;

import com.ibm.icu.text.NumberFormat;

public class Task extends AbstractBaseTask {

    static Long ___ID = 1L;

    private static long nextId() {
        synchronized (___ID) {
            return ___ID++;
        }
    }

    Context _context;
    Object  _observable;

    volatile ITask.Status  _status   = Status.RUNNING;
    private boolean        _optional = false;
    private boolean        _isInitial;
    private List<String>   _scenarios;
    private boolean        _reset;
    private boolean        _runTask;                  // if true, this task will run a previously initialized
                                                      // context.
    private IActiveSubject _target;

    Task(Context context, IMonitor monitor, boolean reset, List<String> scenarios) {
        super(monitor);
        _context = context;
        _session = context.session;
        _isInitial = _context.subject == null;
        _scenarios = scenarios;
        _reset = reset;
        _description = "observing " + _context.getName();
        _target = (IActiveSubject) context.getSubject();
    }

    Task(Context context, IMonitor monitor, boolean reset, List<String> scenarios, Object observable,
            ISubject targetSubject) {
        this(context, monitor, reset, scenarios);
        _target = (IActiveSubject) targetSubject;
        _observable = observable;
        _description = _observable + " in " + _context.getName();
    }

    /*
    * WHEN THIS IS USED, run() will run the temporal actions.
    */
    public Task(Context context, IMonitor monitor) {
        super(monitor);
        _context = context;
        _session = context.session;
        _runTask = true;
        _target = (IActiveSubject) context.getSubject();
        _description = "temporal contextualization of " + _context.getName();
    }

    @Override
    public String getCommand() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public IContext finish() {
        return _context;
    }

    @Override
    public Map<String, Object> adapt() {

        Map<String, Object> ret = super.adapt();
        ret.put("temporal", _runTask ? "true" : "false");
        if (_context != null) {
            if (_isInitial) {
                ret.put("context", _context.adapt());
            } else {
                ret.put("contextId", _context.getId());
            }
        }
        return ret;
    }

    /*
     * FIXME/TODO: the following are awkward because they keep referencing members of Context. The whole Task
     * class should be a member of Context - let it sleep for now.
     * 
     * (non-Javadoc)
     * 
     * @see java.lang.Thread#run()
     */
    @Override
    public void run() {

        /*
         * use a task monitor with this task's ID after resolution.
         */
        IMonitor monitor = new Monitor(_taskId, _session, _context.getReport());

        try {
            if (_context.isDeferred) {
                _context.resolveObservable();
                _target = (IActiveSubject) _context.getSubject();
            }

            _target.setMonitor(monitor);

            if (_runTask) {

                monitor.info("running temporal transitions", null);
                ((Subject) _target).setTaskId(_taskId);
                _target.contextualize();

            } else {

                /*
                 * establish the context - either a previous one or a new one if we're observing a new subject -
                 * and notify the listener.
                 */
                if (_observable == null) {
                    observeSubject(monitor);
                } else {
                    /*
                     * ensure strings and the like are resolved to something valid.
                     */
                    _observable = resolveObservable(_observable);
                    observeInContext(monitor);
                }
            }
        } catch (Exception e) {
            synchronized (_status) {
                _status = Status.ERROR;
            }
            monitor.send(new Notification(Messages.TASK_FAILED, _session, this, MiscUtilities
                    .throwableToString(e)));
            // monitor.error(e);
        } finally {
            if (_status != Status.ERROR) {
                monitor.send(new Notification(Messages.TASK_FINISHED, _session, this, _context));
            }
            _endTime = new Date().getTime();
            synchronized (_status) {
                if (_status != Status.ERROR) {
                    _status = Status.FINISHED;
                }
            }
        }
    }

    private void observeSubject(IMonitor monitor) throws ThinklabException {

        try {
            ((Subject) _target).setContext(_context);
            _target.setMonitor(monitor);
            _context.coverage = _target.initialize(_context.resolutionContext, monitor);
            _context.status = Context.INITIALIZED;

            if (_context.coverage.isEmpty()) {
                monitor.error("subject initialization failed: empty coverage");
            }
        } catch (Throwable e) {
            monitor.error(e);
        }
    }

    private Task observeInContext(IMonitor monitor) throws ThinklabException {

        _target.setMonitor(monitor);
        IObservable observable = makeObservable(_observable);

        if (observable.getModel() != null && observable.getModel().getErrorCount() > 0) {
            // TODO find where the errors are stored and report them. They're in _errors but no way to get
            // them out.
            monitor.error("model " + observable.getModel().getId() + " has runtime errors");
            throw new ThinklabException("cannot observe model " + observable.getModel().getId()
                    + ": runtime errors");
        }

        if (_reset) {
            monitor.info("resetting context", null);
            ((Subject) _target).removeStates();
            _context.coverage = _target.initialize(_context.resolutionContext, monitor);
            _context.status = Context.INITIALIZED;
        }

        _context.coverage = _target.observe(observable, _scenarios, _optional, monitor);

        if (!_context.coverage.isEmpty()) {
            monitor.info("observation of " + observable.getFormalName() + " in " + _context.subject.getId()
                    + " covers "
                    + NumberFormat.getPercentInstance().format(_context.coverage.getCoverage()), null);
        } else {
            monitor.warn("observation of " + observable.getFormalName() + " in "
                    + _context.subject.getId() + " is empty");
        }

        return null;
    }

    Object resolveObservable(Object observable) throws ThinklabException {

        CallTracer.indent("resolveObservable()", this, observable);
        Object ret = null;

        if (observable instanceof String) {
            if (observable.toString().contains(":")) {
                ret = Knowledge.parse(observable.toString());
                if (ret instanceof IProperty) {
                    throw new ThinklabValidationException("relationships cannot be observed directly: "
                            + ret);
                }
            } else {
                ret = KLAB.MMANAGER.findModelObject(observable.toString());
            }

        } else if (observable instanceof IKnowledgeObject) {
            ret = KLABEngine.c(((IKnowledgeObject) observable).getName());
        } else if (observable instanceof IConcept || observable instanceof IModel
                || observable instanceof IDirectObserver) {
            ret = observable;
        } else if (observable instanceof ModelProxy) {
            ret = ((ModelProxy) observable).getModel();
        }

        if (ret == null) {
            CallTracer.msg("ERROR: unable to observe the given observable parameter.");
            CallTracer.unIndent();
            throw new ThinklabValidationException("unable to observe " + observable);
        }

        CallTracer.msg("returning valid observable: " + CallTracer.detailedDescription(ret));
        CallTracer.unIndent();
        return ret;
    }

    private IObservable makeObservable(Object observable) {

        CallTracer.indent("makeObservable()", this, observable);
        IObservable result;

        if (observable instanceof IModel) {
            result = new Observable((IModel) observable, ((IModel) observable).getId());
            CallTracer.msg("got Observable result for IModel parameter: "
                    + CallTracer.detailedDescription(result));
            CallTracer.unIndent();
            return result;
        } else if (observable instanceof IConcept) {
            result = new Observable((IConcept) observable, NS.getObservationTypeFor((IConcept) observable),
            // TODO use current subject type for inherency
            CamelCase.toLowerCase(((IConcept) observable).getLocalName(), '-'));
            CallTracer.msg("got Observable result for IConcept parameter: "
                    + CallTracer.detailedDescription(result));
            CallTracer.unIndent();
            return result;
        }

        CallTracer.msg("parameter was not IConcept or IModel. returning null.");
        CallTracer.unIndent();
        return null;
    }

    @Override
    public IContext getContext() {
        return _context;
    }

}
