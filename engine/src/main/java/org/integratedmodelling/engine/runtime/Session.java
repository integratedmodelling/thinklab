/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.engine.runtime;

import java.io.IOException;
import java.io.InputStream;
import java.io.PrintStream;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Date;
import java.util.Deque;
import java.util.HashMap;
import java.util.List;
import java.util.Properties;
import java.util.UUID;

import org.apache.commons.io.input.NullInputStream;
import org.apache.commons.io.output.NullOutputStream;
import org.integratedmodelling.api.auth.IUser;
import org.integratedmodelling.api.modelling.INamespace;
import org.integratedmodelling.api.monitoring.IMonitor;
import org.integratedmodelling.api.monitoring.INotification;
import org.integratedmodelling.api.runtime.IContext;
import org.integratedmodelling.api.runtime.ISession;
import org.integratedmodelling.api.runtime.ITask;
import org.integratedmodelling.common.auth.RESTUser;
import org.integratedmodelling.common.configuration.KLAB;
import org.integratedmodelling.common.kim.KIMModelManager;
import org.integratedmodelling.engine.modelling.monitoring.Monitor;
import org.integratedmodelling.exceptions.ThinklabException;
import org.integratedmodelling.exceptions.ThinklabRuntimeException;

public class Session implements ISession {

    private static final int MAX_DEFAULT_CONTEXTS = 10;

    // String workspace = null;
    // INamespace namespace = null;

    /*
     * monitors. TODO remove when contexts are operational.
     */
    HashMap<Long, IMonitor> _taskMonitors = new HashMap<Long, IMonitor>();
    IMonitor                _monitor      = new Monitor(-1L, this);
    IUser                   _user         = new RESTUser();

    /*
     * default in/out is from/to null streams.
     * TODO use logging system.
     */
    protected InputStream _in  = new NullInputStream(12000);
    protected PrintStream _out = new PrintStream(new NullOutputStream());

    long _lastAccess = new Date().getTime();

    /*
     * context stack. We keep as much as practical. TODO there should be no need to
     * keep monitors when this is fully operational.
     */
    Deque<IContext> _contexts = new ArrayDeque<IContext>(MAX_DEFAULT_CONTEXTS);

    /*
     * notifications set by monitors in model executions
     */
    ArrayList<INotification> _notifications = new ArrayList<INotification>();
    INamespace               _namespace;

    String id = UUID.randomUUID().toString();

    static long __ID = 0L;

    private synchronized static long nextId() {
        return __ID++;
    }

    Properties properties = new Properties();

    /**
     * Anonymous session.
     */
    public Session() {
    }

    /**
     * User session. Assuming this is created AFTER user was authenticated successfully.
     * 
     * @param user
     */
    public Session(IUser user) {
        _user = user;
    }

    /* (non-Javadoc)
     * @see org.integratedmodelling.ima.core.ISession#getSessionID()
     */
    @Override
    public String getId() {
        return id;
    }

    @Override
    public void notify(INotification notification) {
        synchronized (_notifications) {
            _notifications.add(notification);
        }
    }

    @Override
    public List<INotification> getNotifications(boolean clear) {

        List<INotification> ret = new ArrayList<INotification>();
        synchronized (_notifications) {

            for (INotification n : _notifications)
                ret.add(n);
            if (clear)
                _notifications = new ArrayList<INotification>();
        }
        return ret;
    }

    public void registerTaskMonitor(long taskId, IMonitor monitor) {
        _taskMonitors.put(taskId, monitor);
    }

    public IMonitor getTaskMonitor(int taskId) {
        return _taskMonitors.get(taskId);
    }

    public INamespace getNamespace() {

        if (_namespace == null) {
            try {
                _namespace = ((KIMModelManager) KLAB.MMANAGER).getUserNamespace(this);
            } catch (ThinklabException e) {
                throw new ThinklabRuntimeException(e);
            }
        }
        return _namespace;
    }

    public IContext pushContext(IContext context) {
        if (_contexts.size() == MAX_DEFAULT_CONTEXTS) {
            _contexts.pollLast();
        }
        _contexts.push(context);
        return context;
    }

    @Override
    public IContext getContext() {
        return _contexts.peek();
    }

    @Override
    public List<IContext> getContexts() {
        return new ArrayList<IContext>(_contexts);
    }

    @Override
    public IContext getContext(long id) {
        for (IContext c : _contexts) {
            if (c.getId() == id) {
                return c;
            }
        }
        return null;
    }

    @Override
    public List<ITask> getTasks() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public void setMonitor(IMonitor monitor) {
        _monitor = monitor;
    }

    @Override
    public IMonitor getMonitor() {
        return _monitor;
    }

    @Override
    public PrintStream out() {
        return _out;
    }

    @Override
    public InputStream in() {
        return _in;
    }

    @Override
    public IUser getUser() {
        return _user;
    }

    @Override
    public void close() throws IOException {
        // TODO Auto-generated method stub
    }

    /**
     * record the current time as the last time of access.
     */
    public void ping() {
        _lastAccess = new Date().getTime();
    }

    public long getLastAccess() {
        return _lastAccess;
    }
}
