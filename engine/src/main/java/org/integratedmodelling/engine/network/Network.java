/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.engine.network;

import java.util.HashMap;
import java.util.Map;

import org.integratedmodelling.api.auth.IUser;
import org.integratedmodelling.api.configuration.IConfiguration;
import org.integratedmodelling.api.data.IRankingScale;
import org.integratedmodelling.api.metadata.IModelMetadata;
import org.integratedmodelling.api.metadata.IObservationMetadata;
import org.integratedmodelling.api.modelling.IClassification;
import org.integratedmodelling.api.modelling.IClassifier;
import org.integratedmodelling.api.modelling.IDirectObserver;
import org.integratedmodelling.api.modelling.IObservable;
import org.integratedmodelling.api.modelling.IRelationship;
import org.integratedmodelling.api.modelling.IStructure;
import org.integratedmodelling.api.modelling.IUnit;
import org.integratedmodelling.api.monitoring.INotification;
import org.integratedmodelling.api.network.IComponent;
import org.integratedmodelling.api.network.INode;
import org.integratedmodelling.api.services.IPrototype;
import org.integratedmodelling.common.auth.RESTUser;
import org.integratedmodelling.common.classification.Classification;
import org.integratedmodelling.common.classification.Classifier;
import org.integratedmodelling.common.command.Prototype;
import org.integratedmodelling.common.command.ServiceManager;
import org.integratedmodelling.common.components.Component;
import org.integratedmodelling.common.configuration.KLAB;
import org.integratedmodelling.common.data.RankingScale;
import org.integratedmodelling.common.engine.JSONdeserializer;
import org.integratedmodelling.common.kim.KIMDirectObserver;
import org.integratedmodelling.common.model.runtime.Structure;
import org.integratedmodelling.common.monitoring.EngineNotification;
import org.integratedmodelling.common.vocabulary.Observable;
import org.integratedmodelling.common.vocabulary.ObservationMetadata;
import org.integratedmodelling.common.vocabulary.Unit;
import org.integratedmodelling.engine.modelling.kbox.ModelData;
import org.integratedmodelling.engine.modelling.remote.RemoteServiceCallFunction;
import org.integratedmodelling.engine.modelling.runtime.Relationship;

/*
 * TODO add polling of inactive components; active components are pinged just before any call, so
 * if necessary they put themselves offline when they are called.
 * 
 * 
 * @author Ferd
 *
 */
public class Network extends org.integratedmodelling.common.network.Network {

    private static boolean     superUserTested;
    private static IUser       superUser;
    private Map<String, INode> nodesByKey = new HashMap<>();

    static {
        /*
         * set up for all objects marshalled through JSON
         */
        JSONdeserializer.register(IPrototype.class, Prototype.class);
        JSONdeserializer.register(INotification.class, EngineNotification.class);
        JSONdeserializer.register(IObservable.class, Observable.class);
        JSONdeserializer.register(IComponent.class, Component.class);
        JSONdeserializer.register(IClassification.class, Classification.class);
        JSONdeserializer.register(IClassifier.class, Classifier.class);
        JSONdeserializer.register(IUnit.class, Unit.class);
        JSONdeserializer.register(IRankingScale.class, RankingScale.class);
        JSONdeserializer.register(IModelMetadata.class, ModelData.class);
        JSONdeserializer.register(IStructure.class, Structure.class);
        JSONdeserializer.register(IRelationship.class, Relationship.class);
        JSONdeserializer.register(IDirectObserver.class, KIMDirectObserver.class);
        JSONdeserializer.register(IObservationMetadata.class, ObservationMetadata.class);
    }

    @Override
    public boolean initialize() {

        if (super.initialize()) {
            for (INode node : getNodes()) {
                for (IPrototype p : node.getServices()) {
                    if (p.isPublished()) {
                        ServiceManager.get()
                                .registerRemoteServiceAccessor(p, RemoteServiceCallFunction.class);
                    }
                }
                nodesByKey.put(node.getKey(), node);
            }
            return true;
        }
        return false;

    }

    /**
     * Get a configured in superuser. This is based on storing its credentials in the configuration. It should be
     * based on the server cert file in the future, and when that is done, we can create it at network initialization.
     *
     * @return
     */
    public static IUser getSuperuser() {

        if (!superUserTested) {

            superUserTested = true;
            if (KLAB.CONFIG.getProperties().containsKey(IConfiguration.THINKLAB_SUPERUSER_ID_PROPERTY)
                    && KLAB.CONFIG.getProperties()
                            .containsKey(IConfiguration.THINKLAB_SUPERUSER_PASSWORD_PROPERTY)
                    &&
                    KLAB.NETWORK.getAuthenticationEndpoint() != null) {

                superUser = new RESTUser(KLAB.NETWORK.getAuthenticationEndpoint(), KLAB.CONFIG.getProperties()
                        .getProperty(IConfiguration.THINKLAB_SUPERUSER_ID_PROPERTY).trim(), KLAB.CONFIG
                                .getProperties()
                                .getProperty(IConfiguration.THINKLAB_SUPERUSER_PASSWORD_PROPERTY).trim());

                if (superUser.isAnonymous()) {
                    superUser = null;
                }
            }
        }

        return superUser;
    }

    public INode getNodeByKey(String string) {
        return nodesByKey.get(string);
    }

}
