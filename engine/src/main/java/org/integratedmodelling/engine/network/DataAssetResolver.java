/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.engine.network;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Map;

import javax.annotation.Nullable;
import javax.imageio.ImageIO;

import org.apache.jcs.JCS;
import org.apache.jcs.access.exception.CacheException;
import org.integratedmodelling.api.auth.IUser;
import org.integratedmodelling.api.data.IDataAsset;
import org.integratedmodelling.api.monitoring.IMonitor;
import org.integratedmodelling.api.network.INode;
import org.integratedmodelling.common.auth.AuthClient;
import org.integratedmodelling.common.auth.RESTUser;
import org.integratedmodelling.common.configuration.KLAB;
import org.integratedmodelling.common.data.Datarecord;
import org.integratedmodelling.common.network.Endpoints;
import org.integratedmodelling.common.utils.Path;
import org.integratedmodelling.exceptions.ThinklabException;
import org.integratedmodelling.exceptions.ThinklabIOException;

/**
 * Static methods to resolve and cache data content from asset URNs.
 * 
 * NOTE one day we will want to move to URN formats that are more in line with modern
 * data curation standards (see the old LSID and the debate about those) and think about
 * discovery services and the like.
 * 
 * @author Ferd
 *
 */
public class DataAssetResolver {

    /**
     * If data record is in cache, return it; otherwise ask it to primary server and store it. This one 
     * uses the cache while getDataAsset only deals with primary sources.
     * 
     * @param urn
     * @return
     */
    public static Datarecord getDatarecord(String urn, @Nullable IMonitor monitor) {

        JCS dataCache = null;
        try {
            dataCache = getDataCache();
        } catch (ThinklabException e) {
            // continue without cache
        }

        Datarecord ret = null;
        if (dataCache != null) {

            Map<?, ?> map = (Map<?, ?>) dataCache.get(urn);
            if (map != null) {
                ret = new Datarecord(map);
                KLAB.info("resolved urn " + urn + " from cached records");
            } else {
                ret = (Datarecord) getDataAsset(urn, monitor);
            }

            if (map == null && ret != null) {
                try {
                    dataCache.put(urn, ret.adapt());
                } catch (CacheException e) {
                    KLAB.warn("unexpected error saving " + urn + " to data cache");
                }
            }
        }

        return ret;
    }

    /**
     * Resolve the URN based on the node prefix. Only retrieve primary data - use getDatarecord to use cache and ensure
     * it's a Datarecord.
     * 
     *  Possible use cases:
     *      1. We're a personal engine and the URN is on our primary server. Spare a trip and retrieve directly using our auth token.
     *      2. We're a personal engine and the URN is on another node. Use the node get-asset to negotiate authorization at the node, authorize
     *          our user with our auth token (seen previousy at /connect).
     *      3. We're a node engine and the URN is on our node. Retrieve directly from auth endpoint using superuser method (later certfile).
     *      4. We're a node engine and the URN is on another node. Retrieve from remote engine using get-asset and our node key as authorization.
     *  
     * @param urn
     * @return
     */
    public static IDataAsset getDataAsset(String urn, @Nullable IMonitor monitor) {

        IDataAsset ret = null;
        String nodeId = Path.getFirst(urn, ":");
        INode node = nodeId == KLAB.NAME ? null : KLAB.NETWORK.getNode(nodeId);

        if (KLAB.NETWORK.isPersonal() && node != null) {

            if (node.isPrimaryNode()
                    /*FIXME remove this hack when all certificates are hostname-based */ || node
                            .getUrl().equals("http://150.241.131.4/tl0")) {

                /*
                 * use auth services on primary node directly. This will be the use case for all
                 * users until we have more primary nodes.
                 */
                KLAB.info("attempting direct resolution of URN " + urn + " from primary node");
                try {
                    ret = ((RESTUser) KLAB.NETWORK.getUser()).getAuthClient()
                            .getDatasource(urn, Endpoints.AUTH_GET_DATASOURCE_ENGINE);
                } catch (Exception e) {
                    KLAB.error("error resolving datasource: " + e.getMessage());
                }
            } else {
                try {
                    KLAB.info("attempting direct resolution of URN " + urn + " from node " + nodeId);
                    ret = (IDataAsset) node.get(Endpoints.GET_ASSET, "urn", urn);
                } catch (ThinklabException e) {
                    // authorization denied or other error. Just return null.
                    KLAB.warn("urn resolution failed for " + urn);
                }
            }

        } else {

            if (nodeId.equals(KLAB.NAME)) {
                /*
                 * same node: use superuser/cert file authentication on own auth endpoint
                 */
                IUser su = Network.getSuperuser();
                if (su != null) {
                    KLAB.info("attempting resolution of URN " + urn + " from local service");
                    AuthClient client = new AuthClient(KLAB.NETWORK.getAuthenticationEndpoint(), su);
                    ret = client.getDatasource(urn, Endpoints.AUTH_GET_DATASOURCE_USER);
                }

            } else if (node != null) {
                try {
                    KLAB.info("attempting resolution of URN " + urn + " from node " + nodeId);
                    ret = (IDataAsset) node.get(Endpoints.GET_ASSET, "urn", urn);
                } catch (ThinklabException e) {
                    // authorization denied or other error. Just return null.
                    KLAB.warn("indirect urn resolution failed for " + urn);
                }
            }
        }

        if (ret != null) {
            KLAB.info("URN " + urn + " resolved successfully");
            // Env.logger.info("" + ret);
        } else {
            KLAB.warn("URN " + urn + " was not resolved");
        }

        return ret;

    }

    static JCS _dataCache;
    static JCS _imageCache;
    static JCS _wcsCache;
    static JCS _wfsCache;

    static JCS getDataCache() throws ThinklabException {

        if (_dataCache == null) {
            try {
                _dataCache = JCS.getInstance("data");
            } catch (CacheException e) {
                throw new ThinklabIOException(e);
            }
        }
        return _dataCache;
    }

    /**
    * Retrieves a BufferedImage from the passed cache; return null if not there.
     * @throws ThinklabIOException 
    *
    * @throws ImageNotFoundException
    * @throws CacheException
    * @throws IOException
    */
    public static BufferedImage getImage(String key, JCS imageCache) throws ThinklabIOException {
        BufferedImage img = null;
        byte[] imageAsBytes = (byte[]) imageCache.get(key);
        if (imageAsBytes != null) {
            // Convert the byte array back to the bufferedimage
            InputStream in = new ByteArrayInputStream(imageAsBytes);
            try {
                img = javax.imageio.ImageIO.read(in);
            } catch (IOException e) {
                throw new ThinklabIOException(e);
            }
        }
        return img;
    }

    /**
    * Save passed image to cache with given key. If it's already there, just return it.
     * @throws ThinklabIOException 
    *
    * @throws ImageNotFoundException
    * @throws CacheException
    * @throws IOException
    */
    public static BufferedImage loadImage(String key, BufferedImage img, JCS imageCache)
            throws ThinklabIOException {
        try {
            if (img != null) {
                // Convert the image to bytes, BufferedImages cannot be put into the cache
                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                ImageIO.write(img, "png", baos);
                byte[] bytesOut = baos.toByteArray();
                imageCache.put(key, bytesOut);
            }
        } catch (Exception e) {
            throw new ThinklabIOException(e);
        }
        return img;
    }

}
