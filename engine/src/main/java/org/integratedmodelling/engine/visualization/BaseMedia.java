/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.engine.visualization;

import java.awt.image.BufferedImage;
import java.io.File;

import javax.activation.MimeType;

import org.integratedmodelling.api.modelling.IExtent;
import org.integratedmodelling.api.modelling.IState;
import org.integratedmodelling.api.modelling.visualization.IColormap;
import org.integratedmodelling.api.modelling.visualization.ILegend;
import org.integratedmodelling.api.modelling.visualization.IMedia;
import org.integratedmodelling.api.modelling.visualization.IStateVisualization;
import org.integratedmodelling.collections.Pair;
import org.integratedmodelling.common.utils.image.ImageUtil;
import org.integratedmodelling.common.visualization.ContourPlot;
import org.integratedmodelling.engine.geospace.extents.Grid;
import org.integratedmodelling.engine.geospace.extents.SpaceExtent;
import org.integratedmodelling.engine.geospace.interfaces.IGridMask;
import org.integratedmodelling.engine.visualization.geospace.GeoImageFactory;
import org.integratedmodelling.exceptions.ThinklabException;
import org.integratedmodelling.exceptions.ThinklabUnsupportedOperationException;
import org.integratedmodelling.utils.image.processing.ImageProc;

public abstract class BaseMedia implements IMedia {

    private static final int COVERAGE_MAP = 0;
    private static final int CONTOUR_MAP  = 0;
    protected File           _file        = null;
    protected int            _type        = -1;
    protected MimeType       _mimeType    = null;
    protected ILegend        _legend      = null;
    protected IColormap      _colormap    = null;
    protected IState         _state;
    protected int            _h;
    protected int            _w;

    double[]                 _ddata       = null;
    int[]                    _data        = null;

    protected BaseMedia(File file, IState state, int[] data, double[] ddata, ILegend legend,
            IColormap colormap, int type, MimeType mimeType, int width, int height) {
        _file = file;
        _type = type;
        _mimeType = mimeType;
        _legend = legend;

        if (colormap == null)
            System.out.println("what?");

        _colormap = colormap;
        _state = state;
        _data = data;
        _ddata = ddata;
        _h = height;
        _w = width;
    }

    public int getType() {
        return _type;
    }

    @Override
    public MimeType getMIMEType() {
        return _mimeType;
    }

    @Override
    public ILegend getLegend() {
        return _legend;
    }

    public IColormap getColormap() {
        return _colormap;
    }

    private String getFileBaseName(File directory, String extension) {

        String base = _state.getObservable().getType().toString().replace(':', '_').replace('.', '_')
                .toLowerCase();

        String ret = base;
        int n = 0;
        File bfile = new File(directory + File.separator + base + "." + extension);

        while (bfile.exists()) {
            ret = base + (n++);
            bfile = new File(directory + File.separator + ret + "." + extension);
        }

        return ret;
    }

    public String makeMap(File directory, int[] data, int viewportWidth, int viewportHeight, int flags)
            throws ThinklabException {

        String ret = getFileBaseName(directory, "png") + ".png";

        Pair<Integer, Integer> xy = GeoImageFactory.getPlotSize(viewportWidth, viewportHeight,
                _state.getSpace());

        IExtent spaceExt = _state.getSpace();

        if (spaceExt instanceof SpaceExtent && ((SpaceExtent) spaceExt).isGrid()) {

            Grid space = ((SpaceExtent) spaceExt).getGrid();
            IGridMask mask = space.getActivationLayer();

            if ((flags & COVERAGE_MAP) != 0) {

                File ofile = new File(directory + File.separator + ret);

                if ((flags & IStateVisualization.OPAQUE_BACKGROUND) != 0) {

                    ImageUtil
                            .createImageFile(ImageUtil.upsideDown(data, space.getXCells()),
                                    space.getXCells(), xy.getFirst(), xy.getSecond(), getColormap(),
                                    ofile.toString());

                } else if ((flags & IStateVisualization.EARTH_IMAGE_BACKGROUND) != 0) {

                    BufferedImage bim = GeoImageFactory.get().getRasterImagery(space.getEnvelope(),
                            xy.getFirst(), xy.getSecond(), data, space.getXCells(), getColormap());
                    ImageUtil.saveImage(bim, ofile.toString());
                }

            } else if ((flags & CONTOUR_MAP) != 0) {

                int cols = space.getXCells();
                int rows = space.getYCells();

                double[][] plotdata = new double[rows][cols];

                if (_ddata != null) {

                    for (int row = 0; row < rows; row++) {
                        for (int col = 0; col < cols; col++) {
                            double d = _ddata[space.getOffset(col, row)];
                            boolean active = mask == null || mask.isActive(space.getOffset(col, row));
                            plotdata[rows - row - 1][col] = (!active || Double.isNaN(d)) ? 0.0 : d;
                        }
                    }
                }

                File ofile = new File(directory + File.separator + ret);

                if ((flags & IStateVisualization.OPAQUE_BACKGROUND) != 0) {

                    ContourPlot.createPlot(xy.getFirst(), xy.getSecond(),
                            ImageProc.gaussianSmooth0(plotdata, 1.8)).save(ofile.toString());

                } else if ((flags & IStateVisualization.OPAQUE_BACKGROUND) != 0) {

                    BufferedImage bim = ContourPlot.createPlot(xy.getFirst(), xy.getSecond(),
                            ImageProc.gaussianSmooth0(plotdata, 1.8));

                    bim = GeoImageFactory.get().paintOverImagery(space.getEnvelope(), xy.getFirst(),
                            xy.getSecond(), bim, space.getXCells(), getColormap());

                    ImageUtil.saveImage(bim, ofile.toString());
                }
            }
        } else {

            throw new ThinklabUnsupportedOperationException(
                    "support for visualization of non-grid spatial data is still in the works");
        }

        return ret;
    }

}
