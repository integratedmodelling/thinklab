/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.engine.visualization;

import java.io.File;
import java.io.IOException;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.io.FileUtils;
import org.integratedmodelling.api.knowledge.IConcept;
import org.integratedmodelling.api.modelling.IClassification;
import org.integratedmodelling.api.modelling.IMeasuringObserver;
import org.integratedmodelling.api.modelling.IObservation;
import org.integratedmodelling.api.modelling.IScale;
import org.integratedmodelling.api.modelling.IScale.Locator;
import org.integratedmodelling.api.modelling.IState;
import org.integratedmodelling.api.modelling.ISubject;
import org.integratedmodelling.api.modelling.visualization.IMedia;
import org.integratedmodelling.api.modelling.visualization.IViewport;
import org.integratedmodelling.api.modelling.visualization.IVisualizationFactory;
import org.integratedmodelling.common.data.IndexedCategoricalDistribution;
import org.integratedmodelling.common.states.State;
import org.integratedmodelling.common.visualization.Viewport;
import org.integratedmodelling.engine.geospace.Geospace;
import org.integratedmodelling.engine.time.Time;
import org.integratedmodelling.exceptions.ThinklabException;
import org.integratedmodelling.exceptions.ThinklabIOException;
import org.integratedmodelling.exceptions.ThinklabRuntimeException;
import org.restlet.data.MediaType;

public class VisualizationFactory extends
        org.integratedmodelling.common.visualization.VisualizationFactory implements
        IVisualizationFactory {

    private static VisualizationFactory _this = null;

    public static VisualizationFactory get() {
        if (_this == null) {
            _this = new VisualizationFactory();
        }
        return _this;
    }

    public double[] getStateDataAsNumbers(IState state, Iterable<Locator> locators) {

        double[] ret = new double[(int) state.getValueCount()];
        IClassification classification = getClassification(state);
        List<IScale.Locator> ls = new ArrayList<>();
        for (IScale.Locator l : locators) {
            ls.add(l);
        }

        for (int n : state.getScale().getIndex(ls.toArray(new Locator[ls.size()]))) {

            /**
             * If a specific extent offset is needed, e.g. the offset in space, the following can be used, which
             * will return the spatial offset for the given overall offset. The reason for not having specific
             * getSpatialOffset() or getTemporalOffset is that there can be additional extents beyond time and space,
             * although these are not used at the moment. 
             */
            int i = state.getScale().getExtentOffset(state.getScale().getSpace(), n);
            Double d = Double.NaN;
            Object o = ((State) state).getValue(n);

            if (o instanceof Number) {
                d = ((Number) o).doubleValue();
            } else if (o instanceof IndexedCategoricalDistribution) {
                d = ((IndexedCategoricalDistribution) o).getMean();
            } else if (o instanceof Boolean) {
                d = ((Boolean) o) ? 1.0 : 0.0;
            } else if (o instanceof IConcept) {
                if (classification != null) {
                    d = classification.getNumericCode((IConcept) o);
                }
            } else if (o != null) {
                throw new ThinklabRuntimeException("internal: unexpected state value in VisualizationFactory.getStateData");
            }

            ret[i] = d;
        }

        return ret;
    }

    public double[] getStateDataAsNumbers(IState state) {

        double[] ret = new double[(int) state.getValueCount()];
        IClassification classification = getClassification(state);

        for (int i = 0; i < state.getValueCount(); i++) {

            Double d = Double.NaN;
            Object o = ((State) state).getValue(i);

            if (o instanceof Number) {
                d = ((Number) o).doubleValue();
            } else if (o instanceof IndexedCategoricalDistribution) {
                d = ((IndexedCategoricalDistribution) o).getMean();
            } else if (o instanceof Boolean) {
                d = ((Boolean) o) ? 1.0 : 0.0;
            } else if (o instanceof IConcept) {
                if (classification != null) {
                    d = classification.getNumericCode((IConcept) o);
                }
            } else if (o != null) {
                throw new ThinklabRuntimeException("internal: unexpected state value in VisualizationFactory.getStateData");
            }

            ret[i] = d;
        }

        return ret;
    }

    public float[] getStateDataAsFloats(IState state) {

        float[] ret = new float[(int) state.getValueCount()];
        for (int i = 0; i < state.getValueCount(); i++) {

            Double d = Double.NaN;
            Object o = ((State) state).getValue(i);

            if (o instanceof Number) {
                d = ((Number) o).doubleValue();
            } else if (o instanceof IndexedCategoricalDistribution) {
                d = ((IndexedCategoricalDistribution) o).getMean();
            } else if (o instanceof Boolean) {
                d = ((Boolean) o) ? 1.0 : 0.0;
            } else if (o != null) {
                throw new ThinklabRuntimeException("internal: unexpected state value in VisualizationFactory.getStateData");
            }

            ret[i] = d.floatValue();
        }

        return ret;
    }

    @Override
    public IMedia getMedia(IObservation observation, IScale.Index index, IViewport viewport, String mimeType,
            Map<String, Object> options) {

        MediaType type = new MediaType(mimeType);

        if (type.getParent().equals(MediaType.IMAGE_ALL)) {
            return new ImageMedia(observation, index, viewport, type, options);
        }
        return null;
    }

    public void persist(IObservation obs, File file, IMedia.Type mediaType, Object... options)
            throws ThinklabException {

        /*
         * parse back options. TODO may assume that anything else is a key,value pair for
         * the media that take it, but for now let's skip the painful coding.
         */
        List<IScale.Locator> locators = new ArrayList<>();
        Viewport viewport = null;

        for (Object o : options) {
            if (o instanceof IScale.Locator) {
                locators.add((Locator) o);
            } else if (o instanceof Viewport) {
                viewport = (Viewport) o;
            }
        }

        switch (mediaType) {
        case DATA:
            if (obs instanceof IState) {
                if (obs.getScale().isSpatiallyDistributed()) {
                    Geospace.get().persistState((IState) obs, file, locators);
                } else if (obs.getScale().isTemporallyDistributed()) {
                    Time.get().persistState((IState) obs, file, locators);
                }

            } else if (obs instanceof ISubject) {
                if (obs.getScale().isSpatiallyDistributed()) {
                    Geospace.get().persistSubject((ISubject) obs, file);
                }
            }
            break;
        case VIDEO:
            if (obs instanceof IState && obs.getScale().isSpatiallyDistributed()
                    && obs.getScale().isTemporallyDistributed()) {
                VideoMedia vm = new VideoMedia(obs, viewport, null);
                try {
                    FileUtils.copyFile(vm.getFile(), file);
                } catch (IOException e) {
                    throw new ThinklabIOException(e);
                }
            }
            break;
        case FILE:
            if (obs instanceof ISubject) {
                /*
                 * TODO ZIP file with all states of subject
                 */
            }
            break;
        case IMAGE:
            break;
        }

    }

    public Map<?, ?> describeValue(IState state, Object o) {

        HashMap<String, Object> ret = new HashMap<String, Object>();
        if (o == null || (o instanceof Double && Double.isNaN((Double) o))) {
            o = "No data";
        } else if (o instanceof IndexedCategoricalDistribution) {
            ret.put("distribution", ((IndexedCategoricalDistribution) o).getData());
            ret.put("ranges", ((IndexedCategoricalDistribution) o).getRanges());
            ret.put("uncertainty", ((IndexedCategoricalDistribution) o).getUncertainty());
            o = "m=" +
                    NumberFormat.getInstance().format(((IndexedCategoricalDistribution) o).getMean())
                    + ", s=" +
                    NumberFormat.getInstance().format(((IndexedCategoricalDistribution) o).getUncertainty());
        } else if (o instanceof Boolean) {
            o = state.getObservable().getType().getLocalName() + ((Boolean) o ? " present" : " absent");
        } else if (o instanceof IConcept) {
            o = ((IConcept) o).getLocalName();
        } else if (o instanceof Number) {
            o = NumberFormat.getInstance().format(o);
        } else {
            o = o.toString();
        }

        if (state.getObserver() instanceof IMeasuringObserver) {
            o = o + " " + ((IMeasuringObserver) state.getObserver()).getUnit();
        }

        ret.put("value", o);
        return ret;
    }

}
