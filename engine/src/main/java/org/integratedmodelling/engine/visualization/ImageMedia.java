/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.engine.visualization;

import java.awt.Image;
import java.util.Map;

import javax.activation.MimeType;
import javax.activation.MimeTypeParseException;

import org.integratedmodelling.api.modelling.IObservation;
import org.integratedmodelling.api.modelling.IScale;
import org.integratedmodelling.api.modelling.IState;
import org.integratedmodelling.api.modelling.ISubject;
import org.integratedmodelling.api.modelling.visualization.IColormap;
import org.integratedmodelling.api.modelling.visualization.IImageMedia;
import org.integratedmodelling.api.modelling.visualization.IImageViewport;
import org.integratedmodelling.api.modelling.visualization.ILegend;
import org.integratedmodelling.api.modelling.visualization.IMedia;
import org.integratedmodelling.api.modelling.visualization.IViewport;
import org.integratedmodelling.api.monitoring.IMonitor;
import org.integratedmodelling.api.space.ISpatialExtent;
import org.integratedmodelling.api.time.ITemporalExtent;
import org.integratedmodelling.common.utils.image.ImageUtil;
import org.integratedmodelling.common.visualization.ContourPlot;
import org.integratedmodelling.engine.geospace.extents.Grid;
import org.integratedmodelling.engine.geospace.extents.SpaceExtent;
import org.integratedmodelling.engine.geospace.interfaces.IGridMask;
import org.integratedmodelling.engine.geospace.literals.ShapeValue;
import org.integratedmodelling.engine.visualization.geospace.GeoImageFactory;
import org.integratedmodelling.exceptions.ThinklabException;
import org.integratedmodelling.exceptions.ThinklabRuntimeException;
import org.integratedmodelling.utils.image.processing.ImageProc;
import org.restlet.data.MediaType;

public class ImageMedia implements IImageMedia {

    public static final int COVERAGE_MAP           = 0;
    public static final int EARTH_IMAGE_BACKGROUND = 1;
    public static final int CONTOUR_MAP            = 2;

    private double          SMOOTH_FACTOR          = 1.8;

    IImageViewport          _viewport;
    IObservation            _observation;
    IScale.Index            _index;

    private IColormap       _colormap;
    // private StateClass _type;
    private ILegend         _legend;
    private IMonitor        _monitor;

    /*
     * true if no-data appear in the data.
     */
    boolean                 _hasNull               = false;

    // private IClassification _classificati on;
    // private int[] _data;
    // private double[] _ddata;

    // default for spatial maps
    int                     _mapType               = COVERAGE_MAP;

    // /*
    // * data to match visualized _data to their meaning. Only set
    // * if data are categorized.
    // */
    // private HashMap<Object, Integer> _values = null;

    // /*
    // * number of values (excluding the null)
    // */
    // private boolean _needsZeroInColormap;
    // private int _offset;
    // private int _levels;
    private MimeType        _mtype;

    public ImageMedia(IObservation observation, IScale.Index index, IViewport viewport, MediaType type,
            Map<String, Object> options) {
        _viewport = (IImageViewport) viewport;
        _observation = observation;
        _index = index;
        try {
            _mtype = new MimeType(type.getName());
        } catch (MimeTypeParseException e) {
            throw new ThinklabRuntimeException(e);
        }
        // if (observation instanceof State) {
        // analyze((State) observation);
        // }

        if (options != null) {
            if (options.containsKey("image-type")) {
                String tp = options.get("image-type").toString();
                if (tp.equals("coverage")) {
                    _mapType = COVERAGE_MAP;
                } else if (tp.equals("contour")) {
                    _mapType = CONTOUR_MAP;
                }
            }
        }
    }

    @Override
    public MimeType getMIMEType() {
        return _mtype;
    }

    @Override
    public ILegend getLegend() {
        return _legend;
    }

    @Override
    public IColormap getColormap() {
        if (_colormap == null && _observation instanceof IState) {
            _colormap = VisualizationFactory.getColormap((IState) _observation, _index);
        }
        return _colormap;
    }

    @Override
    public IViewport getViewport() {
        return _viewport;
    }

    @Override
    public Image getImage() throws ThinklabException {

        ISpatialExtent space = _observation.getScale().getSpace();
        ITemporalExtent time = _observation.getScale().getTime();

        if (_observation instanceof ISubject) {

            if (space != null) {
                return getSatelliteImage(space);
            }

        } else if (_observation instanceof IState) {

            if (space != null) {
                return makeMap(space, VisualizationFactory.getDisplayData((IState) _observation, _index, false), _mapType);
            }
            if (time != null) {

            }
        }

        return null;
    }

    @Override
    public IMedia scale(IViewport viewport) {
        // TODO Auto-generated method stub
        return null;
    }

    public Image getSatelliteImage(ISpatialExtent spaceExt) throws ThinklabException {
        ShapeValue shape = ((SpaceExtent) spaceExt).getShape();
        return GeoImageFactory.get()
                .getImagery(shape.getEnvelope(), shape, _viewport.getWidth(), _viewport.getHeight(), 0);
    }

    public Image makeMap(ISpatialExtent spaceExt, int[] data, int type) throws ThinklabException {

        if (data == null) {
            return null;
        }
        int n = 0;
        for (int z : data) {
            if (z > 0) {
                n++;
            }
        }
        if (spaceExt instanceof SpaceExtent && ((SpaceExtent) spaceExt).isGrid()) {

            Grid space = ((SpaceExtent) spaceExt).getGrid();
            IGridMask mask = space.getActivationLayer();

            int[] xy = _viewport.getSizeFor(space.getXCells(), space.getYCells());

            if (type == COVERAGE_MAP) {

                return ImageUtil
                        .createImage(ImageUtil.upsideDown(data, space.getXCells()), space.getXCells(), xy[0], xy[1], getColormap());

            } else if (type == EARTH_IMAGE_BACKGROUND) {

                return GeoImageFactory
                        .get()
                        .getRasterImagery(space.getEnvelope(), xy[0], xy[1], data, space.getXCells(), getColormap());

            } else if (type == CONTOUR_MAP) {

                int cols = space.getXCells();
                int rows = space.getYCells();

                double[][] plotdata = new double[rows][cols];

                if (data != null) {

                    for (int row = 0; row < rows; row++) {
                        for (int col = 0; col < cols; col++) {
                            double d = data[space.getOffset(col, row)];
                            boolean active = mask == null || mask.isActive(space.getOffset(col, row));
                            plotdata[rows - row - 1][col] = (!active || Double.isNaN(d)) ? 0.0 : d;
                        }
                    }
                }

                return ContourPlot.createPlot(xy[0], xy[1], ImageProc
                        .gaussianSmooth0(plotdata, SMOOTH_FACTOR));

            }
        }
        return null;
    }

}
