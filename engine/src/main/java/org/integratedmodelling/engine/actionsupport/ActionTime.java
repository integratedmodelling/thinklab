package org.integratedmodelling.engine.actionsupport;

import org.integratedmodelling.api.time.ITemporalExtent;

/**
 * Wrapper for the spatial extent to use within actions. 
 * 
 * @author ferdinando.villa
 *
 */
public class ActionTime {

    ITemporalExtent time;

    public ActionTime(ITemporalExtent time) {
        this.time = time;
    }

    public double getLinearResolution() {
        return 0;
    }

    public double getArea() {
        return 0;
    }

}
