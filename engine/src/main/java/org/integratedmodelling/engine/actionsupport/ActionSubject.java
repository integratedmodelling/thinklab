package org.integratedmodelling.engine.actionsupport;

import java.util.ArrayList;
import java.util.List;

import org.integratedmodelling.api.knowledge.IConcept;
import org.integratedmodelling.api.modelling.IDirectObservation;
import org.integratedmodelling.api.modelling.IExtent;
import org.integratedmodelling.api.modelling.IScale;
import org.integratedmodelling.common.utils.NameGenerator;
import org.integratedmodelling.common.vocabulary.NS;
import org.integratedmodelling.common.vocabulary.Observable;
import org.integratedmodelling.engine.modelling.runtime.Scale;
import org.integratedmodelling.engine.modelling.runtime.Subject;
import org.integratedmodelling.exceptions.ThinklabException;
import org.integratedmodelling.exceptions.ThinklabValidationException;

public class ActionSubject {

    private Subject subject;

    public ActionSubject(Subject subject) {
        this.subject = subject;
    }

    /**
     * Create another observation of the passed type. Pass any extent, state, or a name 
     * string.
     * 
     * @param concept
     * @param state
     * @throws ThinklabException 
     */
    public IDirectObservation create(IConcept concept, Object... state) throws ThinklabException {

        IScale scale = null;
        IDirectObservation ret = null;

        /*
         * first scan state to check if we have extents
         */
        List<IExtent> extents = new ArrayList<>();
        List<ActionState> states = new ArrayList<>();
        String name = null;

        for (Object o : state) {
            if (o instanceof IExtent) {
                extents.add((IExtent) o);
            } else if (o instanceof String) {
                name = (String) o;
            } else if (o instanceof ActionState) {
                states.add((ActionState) o);
            }
        }

        if (name == null) {
            name = concept.getLocalName() + "_" + NameGenerator.shortUUID();
        }

        /*
         * TODO must negotiate default time extents with current transition. Events
         * get a single transition lifetime by default; subjects and processes inherit
         * from the context.
         */

        if (extents.size() > 0) {
            scale = new Scale(extents);
        }

        if (NS.isThing(concept)) {
            ret = subject.newSubject(new Observable(concept), scale, name, null);
        } else if (NS.isEvent(concept)) {
            ret = subject.newEvent(new Observable(concept), scale, name);
        } else if (NS.isProcess(concept)) {
            ret = subject.newProcess(new Observable(concept), scale, name);
        } else {
            throw new ThinklabValidationException("cannot create a direct observation of " + concept);
        }

        /*
         * create any states
         */
        for (ActionState s : states) {
            s._addState(ret);
        }

        return ret;
    }
}
