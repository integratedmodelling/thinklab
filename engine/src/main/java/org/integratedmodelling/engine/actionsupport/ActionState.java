package org.integratedmodelling.engine.actionsupport;

import org.integratedmodelling.api.knowledge.IConcept;
import org.integratedmodelling.api.modelling.IDirectObservation;

/**
 * Proxy for a state that will be created wherever it's referenced. Can be passed to
 * subject.create() to make states for new subjects. Returned by <script>.state().
 * 
 * @author ferdinando.villa
 *
 */
public class ActionState {

    IConcept concept;
    Object   object;

    public ActionState(IConcept concept, Object value) {
        this.concept = concept;
        this.object = value;
    }

    void _addState(IDirectObservation ret) {
        // TODO Auto-generated method stub

    }

}
