package org.integratedmodelling.engine.actionsupport;

import org.integratedmodelling.api.space.ISpatialExtent;
import org.integratedmodelling.engine.geospace.extents.SpaceExtent;

/**
 * Wrapper for the spatial extent to use within actions. All 
 * values are in the standard SI units (meters).
 * 
 * @author ferdinando.villa
 *
 */
public class ActionSpace {

    SpaceExtent space;
    int         offset = -1;

    double cellWidthM  = Double.NaN;
    double cellHeightM = Double.NaN;

    public ActionSpace(SpaceExtent space) {
        this.space = space;
        if (space.getGrid() != null) {
            cellWidthM = space.getGrid().getCellWidthMeters();
            cellHeightM = space.getGrid().getCellHeightMeters();
        }
    }

    public ActionSpace(SpaceExtent space, int offset) {
        this(space);
        this.offset = offset;
    }

    public double getLinearResolution() {
        return cellWidthM;
    }

    public double getCellArea() {
        return 0;
    }

    /**
     * Return an extent made from a random point within this space.
     * 
     * @return
     */
    public ISpatialExtent getRandomLocation() {
        return null;
    }

    /**
     * Return an extent made from a random polygon within this space.
     * 
     * @return
     */
    public ISpatialExtent getRandomPolygon() {
        return null;
    }

}
