/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.engine.modelling.functions.random;

import java.util.ArrayList;
import java.util.Map;

import org.integratedmodelling.api.knowledge.IConcept;
import org.integratedmodelling.api.knowledge.IExpression;
import org.integratedmodelling.api.lang.IList;
import org.integratedmodelling.api.modelling.IAction;
import org.integratedmodelling.api.monitoring.IMonitor;
import org.integratedmodelling.api.services.annotations.Prototype;
import org.integratedmodelling.common.thinkql.CodeExpression;
import org.integratedmodelling.common.vocabulary.NS;
import org.integratedmodelling.engine.KLABEngine;
import org.integratedmodelling.engine.modelling.datasources.RandomSelectDSActuator;
import org.integratedmodelling.exceptions.ThinklabException;
import org.integratedmodelling.exceptions.ThinklabValidationException;

@Prototype(id = "rand.select", args = {
        "values",
        Prototype.LIST,
        "distribution",
        Prototype.LIST,
        "# seed",
        Prototype.INT }, returnTypes = {
                NS.DATASOURCE,
                NS.STATE_CONTEXTUALIZER })
public class SELECT extends CodeExpression implements IExpression {

    @Override
    public Object eval(Map<String, Object> parameters, IMonitor monitor, IConcept... context)
            throws ThinklabException {

        Object[] distribution = null;
        Object[] values = null;

        if (parameters.containsKey("values")) {
            Object _vals = parameters.get("values");
            if (!(_vals instanceof IList)) {
                throw new ThinklabValidationException("values in a rand.select function must be a list");
            }

            Object[] vals = new Object[((IList) _vals).length()];
            int i = 0;
            for (Object o : ((IList) _vals)) {
                vals[i] = o;
                i++;
            }
            values = vals;
        } else {
            throw new ThinklabValidationException("rand.select must contains a 'values' list to choose from");
        }

        if (parameters.containsKey("distribution")) {
            Object _dist = parameters.get("distribution");
            if (!(_dist instanceof IList)) {
                throw new ThinklabValidationException("distribution in a rand.select function must be a list of floating point numbers");
            }

            int i = 0;
            Object[] vals = new Object[((IList) _dist).length()];
            for (Object o : ((IList) _dist)) {
                if (context != null && context[0].is(KLABEngine.c(NS.DATASOURCE)) && !(o instanceof Number)) {
                    throw new ThinklabValidationException("distribution in a rand.select datasource must be a list of floating point numbers");
                }
                vals[i] = o;
                i++;
            }

            distribution = vals;
        }

        long seed = 0;

        if (parameters.containsKey("seed")) {
            seed = (long) Double.parseDouble(parameters.get("seed").toString());
        }

        /*
         * uniform dist if not specified
         */
        if (distribution == null) {
            distribution = new Object[values.length];
            for (int i = 0; i < distribution.length; i++) {
                distribution[i] = 1.0 / distribution.length;
            }
        }

        if (distribution.length != values.length) {
            throw new ThinklabValidationException("distribution and values in rand.select must have the same number of items");
        }

        if (context == null || context[0].is(KLABEngine.c(NS.DATASOURCE))) {
            // datasource
            return new RandomSelectDSActuator(seed, values, distribution);
        }

        // accessor
        return new RandomSelectDSActuator(new ArrayList<IAction>(), null, seed, values, distribution);
    }

}
