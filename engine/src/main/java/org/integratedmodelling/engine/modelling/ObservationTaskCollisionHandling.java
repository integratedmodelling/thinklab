/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.engine.modelling;

import org.integratedmodelling.api.modelling.IDirectActiveObservation;
import org.integratedmodelling.api.modelling.agents.ICollision;
import org.integratedmodelling.api.modelling.agents.IObservationController;
import org.integratedmodelling.api.modelling.agents.IObservationGraphNode;
import org.integratedmodelling.api.modelling.scheduling.ITransition;
import org.integratedmodelling.exceptions.ThinklabException;

public class ObservationTaskCollisionHandling extends ObservationTask {

    // default generated ID
    private static final long serialVersionUID = 1L;
    private final ICollision  collision;

    public ObservationTaskCollisionHandling(IDirectActiveObservation subject, ICollision collision,
            IObservationGraphNode parentAgentState, IObservationController controller) {
        super(subject, collision.getCollisionTime(), parentAgentState, controller);
        this.collision = collision;
    }

    @Override
    public ITransition run() throws ThinklabException {
        // TODO terminate agent-state at collision time
        // TODO recursively invalidate dependent agent-states
        // TODO create new tasks to re-observe invalidated agent-states (influential relationships only)
        // TODO create new tasks to observe the collided agents
        return null;
    }

    public ICollision getCollision() {
        return collision;
    }
}
