/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.engine.modelling;

import org.integratedmodelling.api.modelling.IDirectActiveObservation;
import org.integratedmodelling.api.modelling.agents.IObservationController;
import org.integratedmodelling.api.modelling.agents.IObservationGraphNode;
import org.integratedmodelling.api.modelling.agents.IObservationTask;
import org.integratedmodelling.api.modelling.scheduling.ITransition;
import org.integratedmodelling.api.time.ITimeInstant;
import org.integratedmodelling.engine.introspection.CallTracer;
import org.integratedmodelling.exceptions.ThinklabException;

public class ObservationTask implements IObservationTask {
    // default generated id
    private static final long                serialVersionUID = 1L;
    protected final IDirectActiveObservation subject;
    protected final ITimeInstant             observationTime;
    private boolean                          valid            = true;
    private long                             startedAt;

    private IObservationGraphNode            parentNode       = null;
    protected final IObservationController   controller;

    public ObservationTask(IDirectActiveObservation subject, ITimeInstant observationTime,
            IObservationController controller) {
        this(subject, observationTime, null, controller);
    }

    public ObservationTask(IDirectActiveObservation subject, ITimeInstant observationTime,
            IObservationGraphNode parentNode, IObservationController controller) {
        this.subject = subject;
        this.observationTime = observationTime;
        this.parentNode = parentNode;
        this.controller = controller;
    }

    /**
     * provide temporal ordering according to each observation START time, and by END time secondarily
     */
    @Override
    public int compareTo(IObservationTask other) {
        return observationTime.compareTo(other.getObservationTime());
    }

    /**
     * TODO need to take the subject's state forward in time from its current state(s) to its state(s) as of
     * the next time step (this includes finding out what that time step is)
     */
    @Override
    public ITransition run() throws ThinklabException {
        CallTracer.indent("run()", this);
        // tell the agent (via the subjectObserver) to make the next state transition
        // NOTE: this should also set the agent to its new state (won't be done explicitly anywhere else)
        ITransition transition = subject.performTemporalTransitionFromCurrentState();

        // add a furtherObservations item for the next time period, if the observer survives.
        if (transition.agentSurvives()) {
            IObservationTask subsequentTaskForThisAgent = new ObservationTask(subject, transition
                    .getTime().getEnd(), controller);
            transition.addFurtherObservationTask(subsequentTaskForThisAgent);
        }

        CallTracer.unIndent();
        return transition;
    }

    @Override
    public ITimeInstant getObservationTime() {
        return observationTime;
    }

    @Override
    public void startedWorkAt(long systemTimeMilliseconds) {
        startedAt = systemTimeMilliseconds;
    }

    @Override
    public long getStartTime() {
        return startedAt;
    }

    @Override
    public void setInvalid() {
        valid = false;
    }

    @Override
    public boolean isValid() {
        return valid;
    }

    @Override
    public IObservationGraphNode getParentNode() {
        return parentNode;
    }

    @Override
    public void setParentNode(IObservationGraphNode node) {
        this.parentNode = node;
    }

    @Override
    public IDirectActiveObservation getSubject() {
        return subject;
    }

    @Override
    public String toString() {
        return getClass().getSimpleName() + " for " + subject + " at " + observationTime;
    }
}
