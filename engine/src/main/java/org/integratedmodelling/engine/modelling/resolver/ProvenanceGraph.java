/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.engine.modelling.resolver;

import java.util.ArrayList;
import java.util.HashMap;

import org.integratedmodelling.api.knowledge.IConcept;
import org.integratedmodelling.api.knowledge.IExpression;
import org.integratedmodelling.api.knowledge.IProperty;
import org.integratedmodelling.api.lang.IMetadataHolder;
import org.integratedmodelling.api.metadata.IMetadata;
import org.integratedmodelling.api.modelling.IConditionalObserver;
import org.integratedmodelling.api.modelling.ICoverage;
import org.integratedmodelling.api.modelling.IDataSource;
import org.integratedmodelling.api.modelling.IModel;
import org.integratedmodelling.api.modelling.IObjectSource;
import org.integratedmodelling.api.modelling.IObservable;
import org.integratedmodelling.api.modelling.IObserver;
import org.integratedmodelling.api.modelling.IScale;
import org.integratedmodelling.api.modelling.IState;
import org.integratedmodelling.api.modelling.resolution.IProvenance;
import org.integratedmodelling.api.modelling.runtime.IActiveDataSource;
import org.integratedmodelling.api.modelling.runtime.IActiveObserver;
import org.integratedmodelling.api.modelling.runtime.IActuator;
import org.integratedmodelling.api.monitoring.IMonitor;
import org.integratedmodelling.common.HashableObject;
import org.integratedmodelling.common.data.Edge;
import org.integratedmodelling.common.kim.KIMObserver;
import org.integratedmodelling.common.metadata.Metadata;
import org.integratedmodelling.common.model.actuators.RedirectingStateActuator;
import org.integratedmodelling.common.states.State;
import org.integratedmodelling.common.utils.NameGenerator;
import org.integratedmodelling.common.visualization.GraphVisualization;
import org.integratedmodelling.common.visualization.GraphVisualization.GraphAdapter;
import org.integratedmodelling.exceptions.ThinklabException;
import org.integratedmodelling.exceptions.ThinklabInternalErrorException;
import org.integratedmodelling.utils.graph.GraphViz;
import org.integratedmodelling.utils.graph.GraphViz.NodePropertiesProvider;
import org.jgrapht.graph.DefaultDirectedGraph;

/**
 * The model strategy produced by the ModelResolver. Individual models are linked by
 * edges that contain the description of what they are doing in relation to each other.
 * 
 * TODO must incorporate a trace of the decisions made by the resolver in the metadata for
 * each model or each link (easier).
 * 
 * TODO add introspection and metadata retrieval methods.
 * 
 * TODO either adapt or write an adapter for OPM 
 * 
 * @author Ferd
 *
 */
public class ProvenanceGraph extends
        DefaultDirectedGraph<ProvenanceGraph.ProvenanceNode, ProvenanceGraph.DependencyEdge>implements
        IProvenance {

    HashMap<Object, ProvenanceNode> _nodes = new HashMap<Object, ProvenanceGraph.ProvenanceNode>();
    IMonitor                        _monitor;

    public static class ProvenanceNode extends HashableObject implements IMetadataHolder {

        /*
         * only one of these is non-null at any time.
         */
        public IModel        model;
        public IObserver     observer;
        public IDataSource   datasource;
        public IState        state;
        public IMetadata     metadata;
        public IObjectSource objectsource;

        ICoverage coverage;

        // public boolean isEntryPoint;

        /*
         * methods below are for the compiler.
         */
        public IObserver getObserver() {
            if (observer != null) {
                return observer;
            }
            if (model != null) {
                return model.getObserver();
            }
            if (state != null) {
                return state.getObserver();
            }

            return null;
        }

        public IActuator getAccessor(IScale context, IMonitor monitor) throws ThinklabException {
            // CallTracer.indent("getAccessor()", this, context);
            // CallTracer.unIndent();
            IActuator ret = null;
            if (model != null) {
                ret = model.getActuator(monitor);
                if (ret != null) {
                    return ret;
                }
                if (model.getObserver() != null) {
                    if (model.getObserver().isComputed() || ((KIMObserver) model.getObserver()).hasActuator()) {
                        return ((IActiveObserver) model.getObserver()).getComputingAccessor(monitor);
                    } else if (model.getObserver() instanceof IConditionalObserver) {
                        return ((IActiveObserver) model.getObserver()).getMediator(null, monitor);
                    } else {
                        throw new ThinklabInternalErrorException("asked for a mediator in a context where the mediated object is unknown");
                    }
                }
            }
            if (observer != null) {
                if (observer.isComputed() || ((KIMObserver) observer).hasActuator()) {
                    return ((IActiveObserver) observer).getComputingAccessor(monitor);
                } else if (observer instanceof IConditionalObserver) {
                    return ((IActiveObserver) observer).getMediator(null, monitor);
                } else {
                    throw new ThinklabInternalErrorException("asked for a mediator in a context where the mediated object is unknown");
                }
            }
            if (state != null) {
                return new RedirectingStateActuator((State) state, monitor);
            }
            if (datasource != null) {
                return ((IActiveDataSource) datasource).getAccessor(context, observer, monitor);
            }

            return null;
        }

        public void setCoverage(ICoverage coverage) {
            this.coverage = coverage;
        }

        public Object getObject() {

            if (model != null) {
                return model;
            }
            if (observer != null) {
                return observer;
            }
            if (state != null) {
                return state;
            }
            if (datasource != null) {
                return datasource;
            }

            return null;
        }

        public static String describeNode(ProvenanceNode n) {

            if (n.model != null) {
                return "m " + n.model.getName();
            } else if (n.observer != null) {
                return "o " + n.observer;
            } else if (n.state != null) {
                return "s " + n.state;
            } else if (n.datasource != null) {
                return "d " + n.datasource;
            }

            return "<unknown node>";
        }

        @Override
        public String toString() {
            return "[" + describeNode(this) + "]";
        }

        @Override
        public IMetadata getMetadata() {
            return metadata == null ? new Metadata() : metadata;
        }
    }

    public static class DependencyEdge extends Edge {

        public static final int DEPENDENCY = 0;
        public static final int MEDIATE_TO = 1;

        // link that transfers data from a datasource to the state of the
        // observer that interprets it.
        public static final int INTERPRET_AS = 2;

        /*
         * the link between an observer and the model it is defined for. It implies only 
         * choice, not transformation
         */
        public static final int DEFINE_STATE = 3;

        /*
         * link between an unresolved observer and an external model that resolves it.
         * Behaves like mediate_to.
         */
        public static final int RESOLVES = 4;

        /*
         * link between a conditional observer and its conditions. The link
         * contains an expression index that sets the order of consideration
         * of the observers and matches the expressions in the conditional
         * observer.
         */
        public static final int CONDITIONAL_DEPENDENCY = 5;

        public DependencyEdge(int type, String formalName, IObservable observable) {
            this.formalName = formalName;
            this.type = type;
            this.observable = observable;
        }

        private static final long serialVersionUID = 2366743581134478147L;

        public String      formalName     = null;
        public IObservable observable     = null;
        public IProperty   property;
        public int         type;
        public double      coverage;
        public int         conditionIndex = -1;
        public IExpression condition;

        @Override
        public boolean equals(Object edge) {
            return edge instanceof ProvenanceGraph.DependencyEdge
                    && this.getSource().equals(((ProvenanceGraph.DependencyEdge) edge).getSource())
                    && this.getTarget().equals(((ProvenanceGraph.DependencyEdge) edge).getTarget())
                    && type == ((ProvenanceGraph.DependencyEdge) edge).type;
        }

        public ProvenanceNode getTargetNode() {
            return (ProvenanceNode) super.getTarget();
        }

        public ProvenanceNode getSourceNode() {
            return (ProvenanceNode) super.getSource();
        }

        public String describeType() {

            if (conditionIndex >= 0) {
                return "#" + conditionIndex + (condition == null ? "" : (" " + condition + "?"));
            }

            switch (type) {
            case DependencyEdge.DEPENDENCY:
                return formalName == null || formalName.isEmpty() ? ("provides " + property) : formalName;
            case DependencyEdge.MEDIATE_TO:
                return "mediate";
            case DependencyEdge.INTERPRET_AS:
                return "interpreted as";
            case DependencyEdge.DEFINE_STATE:
                return "defines";
            case DependencyEdge.RESOLVES:
                return "resolves";
            }

            return "";
        }

        @Override
        public String toString() {
            return getSourceNode() + " -- " + describeType() + " -> " + getTargetNode();
        }
    }

    private static final long serialVersionUID = -5836939340704909163L;

    public ProvenanceGraph(IMonitor monitor) {
        super(ProvenanceGraph.DependencyEdge.class);
        _monitor = monitor;
    }

    /**
     * Wrap the passed object into a node. If we have seen that object before, just 
     * return the correspondent node. Return the node but do not add it to the
     * graph.
     * 
     * @param o
     */
    public ProvenanceNode getNode(Object o) {

        ProvenanceNode ret;

        if (_nodes.containsKey(o)) {
            ret = _nodes.get(o);
        } else {
            ret = new ProvenanceNode();

            if (o instanceof IModel) {
                ret.model = (IModel) o;
            } else if (o instanceof IObserver) {
                ret.observer = (IObserver) o;
            } else if (o instanceof IDataSource) {
                ret.datasource = (IDataSource) o;
            } else if (o instanceof IObjectSource) {
                ret.objectsource = (IObjectSource) o;
            } else if (o instanceof IState) {
                ret.state = (IState) o;
            }

            _nodes.put(o, ret);
        }
        return ret;
    }

    public void add(ProvenanceNode n) {
        addVertex(n);
    }

    // DEBUG - remove
    public boolean hasDatasource() {
        for (ProvenanceNode n : vertexSet()) {
            if (n.datasource != null) {
                return true;
            }
        }
        return false;
    }

    /**
     * Merge in everything in the passed graph. If there is a node in each graph for a 
     * the same object, use the same node in the merged result.
     * 
     * @param g
     */
    public void merge(ProvenanceGraph g) {

        for (ProvenanceNode n : g.vertexSet()) {
            if (!hasNode(n.getObject())) {
                addVertex(n);
                _nodes.put(n.getObject(), n);
            }
        }
        for (DependencyEdge d : g.edgeSet()) {
            ProvenanceNode source = getNode(d.getSourceNode().getObject());
            ProvenanceNode target = getNode(d.getTargetNode().getObject());
            addEdge(source, target, d);
        }
    }

    public ProvenanceNode get(Object o) {
        return _nodes.get(o);
    }

    public String dump() {

        GraphViz ziz = new GraphViz();
        ziz.loadGraph(this, new NodePropertiesProvider<ProvenanceNode, DependencyEdge>() {

            @Override
            public int getNodeWidth(ProvenanceNode o) {
                return 40;
            }

            @Override
            public String getNodeId(ProvenanceNode o) {

                String id = "?";

                if (o.model != null) {

                    id = o.model.getId();
                    if (id == null || NameGenerator.isGenerated(id)) {
                        id = o.toString();
                    }
                } else if (o.state != null) {
                    id = o.state.toString();
                } else if (o.observer != null) {
                    id = o.observer.toString();
                } else if (o.datasource != null) {
                    id = o.datasource.toString();
                }

                return id + " (" + o.hashCode() + ")";
            }

            @Override
            public int getNodeHeight(ProvenanceNode o) {
                return 20;
            }

            @Override
            public String getNodeShape(ProvenanceNode o) {

                if (o.model != null) {
                    return (o.model.getObserver() == null ? BOX3D : BOX);
                }
                return BOX;
            }

            @Override
            public String getEdgeColor(DependencyEdge de) {
                switch (de.type) {
                case DependencyEdge.DEPENDENCY:
                    return "black";
                case DependencyEdge.MEDIATE_TO:
                    return "blue";
                case DependencyEdge.INTERPRET_AS:
                    return "red";
                case DependencyEdge.DEFINE_STATE:
                    return "green";
                case DependencyEdge.CONDITIONAL_DEPENDENCY:
                    return "brown";
                }
                return "black";
            }

            private String edgeType(DependencyEdge e) {
                return e.describeType();
            }

            @Override
            public String getEdgeLabel(DependencyEdge de) {
                return edgeType(de);
            }

        }, false);

        return ziz.getDotSource();
    }

    /**
     * Remove all disconnected nodes. Only happens when there were errors in model resolution, still we
     * may want to look at what was actually done for debugging.
     * 
     * Only for non-trivial structures: if there is only one node, leave it.
     */
    public void cleanup() {

        if (this.vertexSet().size() <= 1) {
            return;
        }

        ArrayList<ProvenanceNode> disconnected = new ArrayList<ProvenanceNode>();
        for (ProvenanceNode n : this.vertexSet()) {
            if (this.edgesOf(n).size() == 0) {
                disconnected.add(n);
            }
        }

        for (ProvenanceNode n : disconnected) {
            this.removeVertex(n);
        }
    }

    @Override
    public IMetadata collectMetadata(Object node) {

        Metadata ret = new Metadata();
        return collectMetadataInternal(node, ret);
    }

    private IMetadata collectMetadataInternal(Object o, Metadata dest) {

        if (o instanceof IMetadataHolder) {
            IMetadata md = ((IMetadataHolder) o).getMetadata();
            for (String k : md.getKeys()) {
                if (dest.get(k) == null) {
                    dest.put(k, md.get(k));
                }
            }
        }

        if (o instanceof IModel) {
            collectMetadataInternal(((IModel) o).getObservables().get(0).getType(), dest);
        } else if (o instanceof IObserver) {
            collectMetadataInternal(((KIMObserver) o).getTopLevelModel(), dest);
        } else if (o instanceof IState) {
            collectMetadataInternal(((IState) o).getObserver(), dest);
            collectMetadataInternal(((IState) o).getObservable().getType(), dest);
        } else if (o instanceof IConcept) {
            for (IConcept c : ((IConcept) o).getParents()) {
                collectMetadataInternal(c, dest);
            }
        }

        return dest;
    }

    public GraphVisualization visualize() {

        GraphVisualization ret = new GraphVisualization();
        ret.adapt(this, new GraphAdapter<ProvenanceNode, DependencyEdge>() {

            @Override
            public String getNodeType(ProvenanceNode o) {
                if (o.model != null) {
                    return o.model.getObserver() == null ? "amodel" : "dmodel";
                } else if (o.observer != null) {
                    return "observer";
                } else if (o.datasource != null) {
                    return "datasource";
                } else if (o.state != null) {
                    return "state";
                }
                return "node";
            }

            @Override
            public String getNodeId(ProvenanceNode o) {
                return "" + o.hashCode();
            }

            @Override
            public String getNodeLabel(ProvenanceNode o) {

                String id = "?";

                if (o.model != null) {

                    id = o.model.getName();
                    if (id == null) {
                        id = o.toString();
                    }
                } else if (o.state != null) {
                    id = o.state.toString();
                } else if (o.observer != null) {
                    id = o.observer.toString();
                } else if (o.datasource != null) {
                    id = o.datasource.toString();
                }

                return id;
            }

            @Override
            public String getNodeDescription(ProvenanceNode o) {
                // TODO Auto-generated method stub
                return null;
            }

            @Override
            public String getEdgeType(DependencyEdge o) {
                // TODO Auto-generated method stub
                return null;
            }

            @Override
            public String getEdgeId(DependencyEdge o) {
                return "" + o.hashCode();
            }

            @Override
            public String getEdgeLabel(DependencyEdge o) {
                return o.describeType();
            }

            @Override
            public String getEdgeDescription(DependencyEdge o) {
                // TODO Auto-generated method stub
                return null;
            }

        });

        return ret;

    }

    @Override
    public boolean isEmpty() {
        return vertexSet().size() == 0;
    }

    public boolean hasNode(Object o) {
        return _nodes.get(o) != null && vertexSet().contains(_nodes.get(o));
    }

    /**
     * Create a link between two node, if necessary switching the nodes so that we ensure that
     * no object is represented by more than one node. With the current logics it should be largely
     * unnecessary, but for now it can stay.
     * 
     * @param source
     * @param target
     * @param dlink
     */
    public void link(ProvenanceNode source, ProvenanceNode target, DependencyEdge dlink) {
        ProvenanceNode s = getNode(source.getObject());
        ProvenanceNode t = getNode(target.getObject());
        addVertex(s);
        addVertex(t);
        addEdge(s, t, dlink);

    }
}
