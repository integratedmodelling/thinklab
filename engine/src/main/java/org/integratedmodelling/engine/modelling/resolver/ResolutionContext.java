/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.engine.modelling.resolver;

import java.text.NumberFormat;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.integratedmodelling.api.knowledge.IConcept;
import org.integratedmodelling.api.knowledge.IExpression;
import org.integratedmodelling.api.knowledge.IKnowledge;
import org.integratedmodelling.api.knowledge.IProperty;
import org.integratedmodelling.api.metadata.IMetadata;
import org.integratedmodelling.api.metadata.IModelMetadata;
import org.integratedmodelling.api.modelling.ICoverage;
import org.integratedmodelling.api.modelling.IDataSource;
import org.integratedmodelling.api.modelling.IDependency;
import org.integratedmodelling.api.modelling.IDirectObservation;
import org.integratedmodelling.api.modelling.IModel;
import org.integratedmodelling.api.modelling.IModelObject;
import org.integratedmodelling.api.modelling.INamespace;
import org.integratedmodelling.api.modelling.IObjectSource;
import org.integratedmodelling.api.modelling.IObservable;
import org.integratedmodelling.api.modelling.IObserver;
import org.integratedmodelling.api.modelling.IScale;
import org.integratedmodelling.api.modelling.IState;
import org.integratedmodelling.api.modelling.ISubject;
import org.integratedmodelling.api.modelling.resolution.IModelPrioritizer;
import org.integratedmodelling.api.modelling.resolution.IProvenance;
import org.integratedmodelling.api.modelling.resolution.IResolutionContext;
import org.integratedmodelling.api.modelling.resolution.IResolutionStrategy;
import org.integratedmodelling.api.modelling.runtime.IDirectActuator;
import org.integratedmodelling.api.modelling.scheduling.ITransition;
import org.integratedmodelling.api.monitoring.IMonitor;
import org.integratedmodelling.api.monitoring.ITaskIntrospector.Info;
import org.integratedmodelling.api.project.IProject;
import org.integratedmodelling.api.time.ITemporalExtent;
import org.integratedmodelling.collections.Pair;
import org.integratedmodelling.common.configuration.KLAB;
import org.integratedmodelling.common.kim.KIMModel;
import org.integratedmodelling.common.kim.KIMNamespace;
import org.integratedmodelling.common.metadata.Metadata;
import org.integratedmodelling.common.utils.Dummy;
import org.integratedmodelling.common.vocabulary.NS;
import org.integratedmodelling.common.vocabulary.Observable;
import org.integratedmodelling.engine.KLABEngine;
import org.integratedmodelling.engine.introspection.CallTracer;
import org.integratedmodelling.engine.modelling.kbox.ModelKbox;
import org.integratedmodelling.engine.modelling.resolver.ProvenanceGraph.DependencyEdge;
import org.integratedmodelling.engine.modelling.resolver.ProvenanceGraph.ProvenanceNode;
import org.integratedmodelling.engine.modelling.runtime.Subject;
import org.integratedmodelling.exceptions.ThinklabException;
import org.integratedmodelling.exceptions.ThinklabValidationException;
import org.integratedmodelling.lang.LogicalConnector;

/*
 * resolution context of object X contains a scale, the link X will be target of in the provenance diagram,
 * and optionally the semantic context of the resolution, i.e. the property that represents the link and if
 * that isn't null, the concept it links to in that capacity.
 * 
 * TODO rename all the methods that return sub-contexts to something sensible and address any redundancy or
 * stupidity in them.
 */
public class ResolutionContext implements IResolutionContext {

    enum Type {
        ROOT,
        SUBJECT,
        OBSERVABLE,
        MODEL,
        OBSERVER,
        DEPENDENCY,
        CONDITIONAL_DEPENDENCY,
        DATASOURCE,
        OBJECTSOURCE,
        GROUP
    };

    Type               type;
    IDirectObservation subject;
    IObservable        observable;
    IProperty          property;
    IObserver          observer;
    IModel             model;
    IDataSource        datasource;
    IObjectSource      objectsource;
    ISubject           contextSubject;
    IDependency        dependency;
    // mandatorily not null
    IMonitor           monitor;

    // this only exists for a context handling a direct observable.
    IDirectActuator<?> actuator;

    // the scale we keep harmonizing as we resolve; exclude time given that we resolve for initialization
    IScale scale;

    // the time scale, if any, so we can tune the model retrieval for currency and coverage.
    ITemporalExtent time;

    // the link we will use to link to root if resolved
    DependencyEdge link;
    // the node we get for our target when resolution is accepted by the resolver, and the root node for
    // our children contexts.

    ProvenanceNode  node;
    // provenance graph that gets built along the resolution chain, passed around to children
    ProvenanceGraph modelGraph;

    // TODO fill these in
    Collection<String> nsWhitelist = null;
    Collection<String> nsBlacklist = null;

    // scenarios are defined externally on the root context and passed around unmodified
    HashSet<String> scenarios = null;

    // attributes extracted from the context, used when the kbox is consulted for ranking of options.
    Set<IConcept> attributes = null;

    // the namespace of reference for the context (the one we're resolving into at the
    // moment - model that has dependencies or root direct observer).
    private INamespace resolutionNamespace;

    // these are not dealt with directly, but the resolver may add metadata, in which
    // case they're passed on to the provenance nodes when they're built.
    IMetadata metadata = null;

    /*
     * this should only be true when we're resolving a conditional branch. It will color all nodes as
     * conditional unless they have been seen in an unconditional branch too.
     */
    boolean isOptional;

    /**
     *  If this is true, the context has been created by an explicit observation action from the
     *  outside of the context, and it should not propagate the model to its parent. 
     */
    boolean isDirect = false;

    /*
     * A mismatch between attribute semantics may make a context unsatisfiable, which prunes
     * a resolution subtree instantly.
     */
    boolean isUnsatisfiable = false;

    /*
     * connector for multiple coverage merges in resolution of dependencies. Default is
     * AND, but some contexts (namely conditional dependencies) will use OR. 
     */
    LogicalConnector connector = LogicalConnector.INTERSECTION;

    /*
     *  model cache, passed around; holds node->model correspondences as they're accepted.
     *  We hold all the resolved observables with the respective model and coverage.
     */
    HashMap<IObservable, IModel> models;

    /**
     * Here we keep those direct observables for which we could not find a model, but we can
     * still resolve.
     */
    HashSet<IObservable> noModelsFor;

    /*
     *  model cache, as before but separate for instantiators of direct observables (the
     *  observable is the same, so we must keep these separate).
     */
    HashMap<IObservable, IModel> instantiatorModels;

    /*
     * coverage cache for models. We just pass this around.
     */
    HashMap<IModel, ICoverage> coverages;

    /*
     * overall coverage, updated by the resolver after each step.
     */
    ICoverage coverage;

    /*
     * our parent node which will accept() us if the merged coverage is sufficient.
     */
    ResolutionContext parent;

    /*
     * if we're representing a conditional dependency, the condition that comes with it (may be 
     * null even for a conditional dependency).
     */
    IExpression condition;

    /*
     * if we're representing a conditional dependency, the order of definition of the
     * condition.
     */
    int conditionIndex = -1;

    /*
     * cache of model IDs being resolved to avoid infinite loops when a model matches
     * its own observable.
     */
    HashSet<IModelObject> resolving = new HashSet<>();

    /*
     * The final product of the resolution is a resolution strategy that will be
     * executed to initialize the subject if the resulting coverage is sufficient.
     */
    ResolutionStrategy resolutionStrategy;

    /*
     * we may be resolving during a transition. For now this is only in for semantic suppor but
     * always null - will be used later.
     */
    ITransition transition;

    /**
     * If true, we're looking to observe an observable for an already instantiated direct
     * observation, as opposed to observing the direct observable in a context. Any
     * matching model found down the resolution chain must be of the explanatory
     * kind - i.e. not an instantiator ('each').
     */
    boolean isExplanatoryModel = false;

    /**
     * If not null, we're resolving a dependency with the 'as <trait>' clause, which
     * requires us to merge this trait with the observable of any observation made under
     * this branch.
     */
    IConcept interpretAs = null;

    // created on demand and not communicated.
    IModelPrioritizer<IModelMetadata> prioritizer = null;

    /*
     * ONLY for "child" contexts - automatically sets the parent context and
     * prepares a new provenance graph, to be merged with the parent's at finish() if coverage
     * is sufficient. Copies all common info and leave everything else undefined.
     * 
     * Constructor using this must set the link and add the provenance node as 
     * the context's node, then link it as necessary. Each context can only have
     * one root node.
     */
    private ResolutionContext(ResolutionContext ctx, Type type, IDirectObservation subject) {

        CallTracer.indent("" + type);

        this.subject = subject;

        this.models = new HashMap<>(ctx.models);
        this.noModelsFor = new HashSet<>(ctx.noModelsFor);
        this.instantiatorModels = new HashMap<>(ctx.instantiatorModels);

        if (ctx.attributes != null) {
            this.attributes = new HashSet<>(ctx.attributes);
        }

        this.modelGraph = new ProvenanceGraph(ctx.monitor);
        this.scenarios = ctx.scenarios;
        this.nsWhitelist = ctx.nsWhitelist;
        this.nsBlacklist = ctx.nsBlacklist;
        this.scale = ctx.scale;
        this.monitor = ctx.monitor;
        this.resolutionNamespace = ctx.resolutionNamespace;
        // this.tasks = ctx.tasks;
        this.coverages = ctx.coverages;
        this.isOptional = ctx.isOptional;
        this.resolving.addAll(ctx.resolving);
        this.time = ctx.time;
        this.isExplanatoryModel = ctx.isExplanatoryModel;
        this.transition = ctx.transition;

        // the model and context subject stay unless redefined later
        this.model = ctx.model;
        this.parent = ctx;
        this.type = type;
        this.contextSubject = ctx.contextSubject;
        this.interpretAs = ctx.interpretAs;

        this.resolutionStrategy = new ResolutionStrategy(this.subject);
    }

    public ICoverage finish(ICoverage coverage) throws ThinklabException {
        this.coverage = coverage;
        return finish();
    }

    /**
     * Call after resolution of this context to merge in results into the father context. If the
     * merged coverage is acceptable, merge provenance and model cache and link the root node to
     * the father's.
     * 
     * @param ctx
     * @return
     * @throws ThinklabException 
     */
    @Override
    public ICoverage finish() throws ThinklabException {

        if (coverage == null) {

            // we're a group with no members or something that hasn't accepted or resolved anything.
            // Also happens for previous model observers. Needs checking.
            boolean startWithFullCoverage = type == Type.MODEL ||
                    type == Type.SUBJECT ||
                    type == Type.OBSERVER ||
                    (type == Type.DEPENDENCY && NS.isThing(dependency.getObservable()));

            coverage = startWithFullCoverage ? new Coverage(scale, 1.0) : Coverage.EMPTY;
        }

        if (coverage.isEmpty()) {
            // an empty optional dependency is OK, doesn't alter the parent's coverage
            if (isOptional) {
                if (dependency == null) {
                    // monitor.warn("there are unsatisfied optional dependencies");
                } else {
                    monitor.warn("optional dependency on " + dependency.getObservable().getType()
                            + " is unsatisfied");
                }
                CallTracer.unIndent();
                return coverage;
            }
            CallTracer.unIndent();
            return coverage;
        }

        CallTracer.unIndent();
        return parent.accept(this);

    }

    /**
     * Root resolution context. All others should be created using public methods.
     * @param subject
     * @param scenarios
     * @return
     */
    public static ResolutionContext root(ISubject subject, /* FIXME no need for this */ISubject contextSubject, IMonitor monitor, Collection<String> scenarios) {
        ResolutionContext ret = new ResolutionContext(subject, contextSubject, monitor, scenarios);
        ((Subject) subject).setPrimary(true);
        ret.type = Type.ROOT;
        return ret;
    }

    public ResolutionContext(IDirectObservation subject, ISubject contextSubject, IMonitor monitor,
            Collection<String> scenarios) {
        this.models = new HashMap<>();
        this.instantiatorModels = new HashMap<>();
        this.coverages = new HashMap<>();
        this.noModelsFor = new HashSet<>();
        this.monitor = monitor;
        this.contextSubject = contextSubject;
        this.scenarios = new HashSet<>();
        if (scenarios != null) {
            for (String s : scenarios) {
                this.scenarios.add(s);
            }
        }
        this.modelGraph = new ProvenanceGraph(monitor);
        this.resolutionStrategy = new ResolutionStrategy(subject);
    }

    /**
     * NOT USED FOR RESOLUTION: only for searching upon request from a foreign engine, within the
     * query service. It will make up a non-existing namespace just to carry the criteria around.
     * 
     * @param types
     * @param observationType
     * @param contextType
     * @param traits
     * @param scenarios
     * @param criteria
     */
    public ResolutionContext(IKnowledge subjectType, IScale scale, List<IKnowledge> types,
            boolean isInstantiator,
            IConcept observationType,
            IConcept contextType,
            IConcept inherentType,
            int detailLevel,
            List<IKnowledge> traits, String[] scenarios, IMetadata criteria) {

        this.scale = scale;
        this.isExplanatoryModel = !isInstantiator;
        this.noModelsFor = new HashSet<>();

        /*
         * make up a fake namespace (with a name that won't conflict and the passed resolution criteria)
         */
        this.resolutionNamespace = new KIMNamespace(ModelKbox.DUMMY_NAMESPACE_ID);
        ((KIMNamespace) resolutionNamespace).setResolutionCriteria(criteria);

        this.subject = Dummy.subject(subjectType, scale, this.resolutionNamespace);

        /*
         * make up an observable from the passed data
         */
        this.observable = new Observable();
        ((Observable) observable).setObservedType(types.get(0));
        ((Observable) observable).setObservationType(observationType);
        ((Observable) observable).setContextType(contextType);
        ((Observable) observable).setInherentType(inherentType);
        ((Observable) observable).setDetailLevel(detailLevel);

        /*
         * assign traits (attributes) and scenarios
         */
        if (traits.size() > 0) {
            this.attributes = new HashSet<>();
            for (IKnowledge t : traits) {
                this.attributes.add((IConcept) t);
            }
        }

        this.scenarios = new HashSet<>();
        if (scenarios != null) {
            for (String s : scenarios) {
                this.scenarios.add(s);
            }
        }
    }

    @Override
    public Set<IKnowledge> getObservableClosure(IObservable observable) throws ThinklabException {

        Set<IKnowledge> observables = new HashSet<IKnowledge>();
        if (this.observable != null && this.property != null
                && this.observable.getType() instanceof IConcept) {
            for (IConcept cc : this.observable.getTypeAsConcept().getPropertyRange(this.property)) {
                observables.add(cc);
            }
        } else {
            observables.add(observable.getType());
        }

        if (observables.size() == 0) {
            observables.add(observable.getType());
        }

        return observables;
    }

    @Override
    public IScale getScale() {
        return scale;
    }

    @Override
    public Collection<String> getScenarios() {
        return scenarios;
    }

    public IObservable getObservable() {
        return observable;
    }

    @Override
    public IModel getModel() {
        return model;
    }

    @Override
    public INamespace getResolutionNamespace() {
        return resolutionNamespace;
        // INamespace ns = getPrimaryModelNamespace();
        // return ns == null ? getContextNamespace() : ns;
    }

    // private INamespace getContextNamespace() {
    // if (subject != null && ((Subject) subject).isPrimary()) {
    // return subject.getNamespace();
    // }
    // return null;
    // }
    //
    // private INamespace getPrimaryModelNamespace() {
    // if (model != null && ((KIMModel) model).isPrimary()) {
    // return model.getNamespace();
    // }
    // return null;
    // }

    @Override
    public void setMonitor(IMonitor monitor) {
        this.monitor = monitor;
    }

    @Override
    public IMonitor getMonitor() {
        return monitor;
    }

    @Override
    public IDirectObservation getSubject() {
        return subject;
    }

    @Override
    public IProperty getPropertyContext() {
        return property;
    }

    @Override
    public void forceScale(IScale scale) {
        this.scale = scale;
    }

    @Override
    public IProvenance getProvenance() {
        return modelGraph;
    }

    @Override
    public ICoverage getCoverage() {
        return coverage;
    }

    @Override
    public ISubject getContextSubject() {
        return contextSubject;
    }

    @Override
    public void setMetadata(String key, Object value) {
        if (metadata == null) {
            metadata = new Metadata();
        }
        metadata.put(key, value);
    }

    @Override
    public ResolutionContext forObservable(IObservable observable) {

        ResolutionContext ret = new ResolutionContext(this, Type.OBSERVABLE, subject);
        ret.observable = observable;
        ret.attributes = mergeTraits(this.attributes, observable);

        if (NS.isQuality(observable) || NS.isTrait(observable)) {
            // force explanation, which may be false from upstream context.
            ret.isExplanatoryModel = true;
        }

        trace(ret);

        return ret;
    }

    @Override
    public ResolutionContext forObservable(IObservable observable, Collection<String> scenarios) {

        ResolutionContext ret = new ResolutionContext(this, Type.OBSERVABLE, subject);
        ret.observable = observable;
        if (scenarios != null && scenarios.size() > 0) {
            ret.scenarios.clear();
            ret.scenarios.addAll(scenarios);
        }
        ret.attributes = mergeTraits(this.attributes, observable);
        // needs this as it's an entry point for the resolver and the subject wasn't known.
        ret.resolutionStrategy = new ResolutionStrategy(subject);

        if (NS.isQuality(observable) || NS.isTrait(observable)) {
            // force explanation, which may be false from upstream context.
            ret.isExplanatoryModel = true;
        }

        trace(ret);

        return ret;
    }

    /*
     * add any traits that weren't there already, update those that were there. If any
     * traits are incompatible, set the context to unsatisfiable.
     */
    private Set<IConcept> mergeTraits(Set<IConcept> oldt, IObservable observable) {

        Set<IConcept> ret = new HashSet<IConcept>();
        try {

            Pair<IConcept, Collection<IConcept>> ast = NS.separateAttributes(observable.getType());
            for (IConcept attribute : ast.getSecond()) {
                IConcept baset = NS.getBaseParentTrait(attribute);
                if (oldt != null) {
                    for (IConcept c : oldt) {
                        if (!c.equals(attribute) && c.is(baset)) {
                            isUnsatisfiable = true;
                            return ret;
                        }
                    }
                }
                /*
                 * it gets here only if oldt didn't have it or had exactly the same.
                 */
                ret.add(attribute);
            }

            /*
             * anything not added that was in oldt is safe for addition at this point
             */
            if (oldt == null) {
                return ret.isEmpty() ? null : ret;
            }
            ret.addAll(oldt);

        } catch (ThinklabValidationException e) {

            // better empty than wrong
            ret.clear();
        }

        return ret;
    }

    @Override
    public ResolutionContext forDependency(IDependency dependency, INamespace namespace) {
        ResolutionContext ret = new ResolutionContext(this, Type.DEPENDENCY, subject);
        ret.model = this.model;
        ret.observer = this.observer;
        ret.datasource = this.datasource;
        ret.dependency = dependency;
        ret.property = dependency.getProperty();
        ret.isOptional = this.isOptional || dependency.isOptional();
        ret.interpretAs = dependency.reinterpretAs();
        ret.resolutionNamespace = namespace;
        // no link - this will collect node and link from downstream and pass them on to the parent.

        trace(ret);

        return ret;
    }

    private static void trace(ResolutionContext ret) {

        if (ret.node != null) {
            CallTracer.msg("N: " + ret.node);
        }
        if (ret.link != null) {
            CallTracer.msg("L: " + ret.link.describeType());
        }

    }

    private boolean isObserverContext() {
        if (this.type.equals(Type.OBSERVER))
            return true;
        if (this.type.equals(Type.MODEL) || this.type.equals(Type.DEPENDENCY))
            return false;
        return parent == null ? false : parent.isObserverContext();
    }

    @Override
    public ResolutionContext forModel(IModel model) {

        ResolutionContext ret = new ResolutionContext(this, Type.MODEL, subject);
        ret.model = model;
        ret.resolutionNamespace = model.getNamespace();
        ret.node = ret.modelGraph.getNode(model);
        ret.attributes = this.attributes == null ? null : mergeTraits(this.attributes, model.getObservable());
        ret.resolving.add(model);

        /*
         * get the actuator here so we can pass it to the resolver when looking up 
         * dependencies and give it a chance to define contextual dependencies.
         */
        if (NS.isDirect(model)) {
            try {
                ret.actuator = (IDirectActuator<?>) model.getActuator(monitor);
            } catch (ThinklabException e) {
                // just null.
            }
        }

        // if data model, link to parent is "provides"; otherwise we are an entry point and we don't want a
        // link. Dependency must be completed upstream by adding formal name and conditions if any.
        if (model.getObserver() != null) {
            ret.link = new DependencyEdge(isObserverContext() ? DependencyEdge.RESOLVES
                    : DependencyEdge.DEPENDENCY, "", model.getObservable());
        }

        trace(ret);

        return ret;
    }

    @Override
    public ResolutionContext forSubject(IDirectObservation subject, Collection<String> scenarios) {
        ResolutionContext ret = forSubject(subject);
        ret.isDirect = true;
        if (scenarios != null && scenarios.size() > 0) {
            ret.scenarios.clear();
            ret.scenarios.addAll(scenarios);
        }
        return ret;
    }

    @Override
    public ResolutionContext forSubject(IDirectObservation subject) {

        ResolutionContext ret = new ResolutionContext(this, Type.SUBJECT, subject);

        // null in root context
        ret.contextSubject = (ISubject) this.subject;
        ret.scale = subject.getScale().getSubscale(KLABEngine.c(NS.TIME_DOMAIN), 0);

        ret.time = subject.getScale().getTime();
        // TODO check the namespace that gets here
        ret.resolutionNamespace = subject.getNamespace();
        ret.attributes = mergeTraits(this.attributes, subject.getObservable());
        ret.resolutionStrategy._subject = (Subject) subject;
        ret.resolutionStrategy._csubject = ret.contextSubject;

        // whatever is in there, we reuse; use the observer's observable, which is what was
        // matched, not the dependency's. Use the explanatory model cache, not the instantiators.
        for (IState s : subject.getStates()) {
            ret.models.put(s.getObserver().getObservable(), new StateModel(s.getObservable(), s, subject
                    .getNamespace()));
        }

        /*
         * FIXME/TODO this needs to use the CONTEXT model states also, knowing they may have different scale so
         * using a scale mediator model initialized with the state.
         */
        // no node - we use the subject model as the entry point for the workflow.

        trace(ret);

        return ret;
    }

    @Override
    public ResolutionContext forObserver(IObserver observer, IDataSource modelDatasource) {
        ResolutionContext ret = new ResolutionContext(this, Type.OBSERVER, subject);
        ret.model = this.model;
        ret.observer = observer;
        ret.datasource = modelDatasource;
        ret.node = ret.modelGraph.getNode(observer);
        ret.attributes = mergeTraits(this.attributes, observer.getObservable());

        /*
         * FIXME the following is no longer true - observers such as distance have no mediated or
         * datasource but may have coverage. For now just adapted the Workflow to force-compile in
         * any computed observer, but coverage may be inaccurate.
         */
        
        // no need for coverage, observer always has a datasource or a mediated observer.
        // observers define the state of models.
        ret.link = new DependencyEdge(DependencyEdge.DEFINE_STATE, "", observer.getObservable());

        trace(ret);

        return ret;
    }

    @Override
    public ResolutionContext forDatasource(IDataSource datasource) throws ThinklabException {

        ResolutionContext ret = new ResolutionContext(this, Type.DATASOURCE, subject);
        ret.datasource = datasource;
        ret.model = this.model;
        ret.observer = this.observer;

        ret.node = ret.modelGraph.getNode(datasource);
        // non-conditional node with no further resolution, so it doesn't go through finish() and
        // gets added here.
        ret.modelGraph.add(ret.node);

        // datasources are interpreted by observers.
        ret.link = new DependencyEdge(DependencyEdge.INTERPRET_AS, "", observer.getObservable());

        // initial coverage is the intersection of the datasource with the scale. We use the model
        // to capture any other restriction from the model or the namespace.
        ret.coverage = new Coverage(scale, 1.0);
        IScale modelScale = model.getCoverage();
        ret.coverage = ret.coverage.and(new Coverage(modelScale));

        // boolean wasthere = false;
        // if (((Monitor) monitor).getSpatialDisplay() == null) {
        // ((Monitor) monitor).setSpatialDisplay((SpaceExtent) ((Coverage) ret.coverage)
        // .getOriginalExtent(Env.c(NS.SPACE_DOMAIN)));
        // } else {
        // wasthere = true;
        // }
        // ((Monitor) monitor).getSpatialDisplay()
        // .add(((SpaceExtent) ((Coverage) ret.coverage).getCurrentExtent(Env.c(NS.SPACE_DOMAIN)))
        // .getShape());
        //
        // if (!wasthere) {
        // ((Monitor) monitor).getSpatialDisplay().show();
        // }
        trace(ret);

        return ret;
    }

    @Override
    public ResolutionContext forObjectSource(IObjectSource objectsource) throws ThinklabException {

        final ResolutionContext ret = new ResolutionContext(this, Type.OBJECTSOURCE, subject);
        ret.objectsource = objectsource;
        ret.model = model;
        ret.observer = observer;

        ret.node = ret.modelGraph.getNode(objectsource);
        ret.node.model = model;

        // non-conditional node with no further resolution, so it doesn't go through finish() and
        // gets added here.
        ret.modelGraph.add(ret.node);

        // coverage is the intersection of the datasource with the scale. We use the model to capture
        // any other restrictions from the model or the namespace.
        ret.coverage = new Coverage(scale);
        ret.coverage = ret.coverage.and(new Coverage(model.getCoverage()));

        trace(ret);

        return ret;
    }

    @Override
    public ResolutionContext forMediatedObserver(IObserver observer) {
        ResolutionContext ret = new ResolutionContext(this, Type.OBSERVER, subject);
        ret.model = model;
        ret.observer = observer;
        ret.datasource = datasource;
        ret.link = new DependencyEdge(DependencyEdge.MEDIATE_TO, "", observer.getObservable());
        ret.observable = observer.getObservable();
        ret.node = ret.modelGraph.getNode(observer);

        trace(ret);

        return ret;
    }

    @Override
    public ResolutionContext forCondition(IExpression expression, int n) {

        /*
         * FIXME - this should merely color the downstream dependency link with a
         * conditional flavor and pass the expression and index, otherwise works like 
         * a dependency, so it should have no link and node. and the condition/index
         * should be in the context.
         */
        ResolutionContext ret = new ResolutionContext(this, Type.CONDITIONAL_DEPENDENCY, subject);
        ret.model = model;
        ret.observer = observer;
        ret.condition = expression;
        ret.conditionIndex = n;

        trace(ret);

        return ret;
    }

    @Override
    public ResolutionContext forGroup(LogicalConnector connector) {
        ResolutionContext ret = new ResolutionContext(this, Type.GROUP, subject);
        ret.connector = connector;
        ret.coverage = (connector.equals(LogicalConnector.INTERSECTION) ? new Coverage(ret.scale, 1.0)
                : Coverage.EMPTY);

        // act as a worker for the parent, so take its node to link to each child that satisfies
        // the rule for group inclusion.
        ret.node = this.node;

        // will use the child link to the parent node; we have no link of our own, but will link
        // the parent's to the child's at each accept()

        trace(ret);

        return ret;
    }

    public boolean hasNoModel(IObservable observable) {
        return noModelsFor.contains(observable);
    }

    public void proceedWithNoModel(IObservable observable) {
        noModelsFor.add(observable);
    }

    @Override
    public IModel getModelFor(IObservable observable, boolean matchInstantiators) {

        /*
         * TODO/CHECK
         * this will not match, e.g., an indirect observation with a measurement - which may be a problem
         * although usually only for manually observed concepts.
         */
        return matchInstantiators ? instantiatorModels.get(observable) : models.get(observable);

    }

    @Override
    public ICoverage accept(IResolutionContext chld) throws ThinklabException {

        ResolutionContext child = (ResolutionContext) chld;
        boolean noOp = false;
        boolean linkToParentSubject = this.type == Type.MODEL && NS.isProcess(this.model.getObservable());
        ProvenanceNode source = linkToParentSubject ? getParentNode() : this.node;
        ProvenanceNode target = child.node;
        DependencyEdge dlink = linkToParentSubject
                ? new DependencyEdge(DependencyEdge.DEPENDENCY, "", child.observable)
                : child.link;

        /*
         * determine coverage
         */
        if (type.equals(Type.OBSERVABLE)) {

            /*
             * child can only be a model
             */
            if (coverage == null) {

                coverage = child.coverage;

                // may have been seen before
                if (!getModelCache(isExplanatoryModel).containsValue(child.model)) {

                    /*
                     * don't bore the adorable user with something they will ask you about every 
                     * time they see it and never understand once.
                     */
                    if (!(child.model instanceof StateModel)) {
                        monitor.info(child.model.getName() + " satisfies "
                                + NumberFormat.getPercentInstance().format(coverage.getCoverage()) + " of "
                                + this.observable.getType() + " in " + subject.getId(), null);
                    }
                    /*
                     * store in cache
                     */
                    getModelCache(isExplanatoryModel).put(this.observable, child.model);
                    coverages.put(child.model, coverage);
                }

            } else {

                /*
                 * the child is a resolved model. Check if it adds enough coverage to be useful; if not, do nothing.
                 */
                double tcov = coverage.getCoverage();
                ICoverage cv = coverage.or(child.coverage);
                double additional = cv.getCoverage() - tcov;
                if (additional > 0) {

                    monitor.info(child.model.getName() + " adds "
                            + NumberFormat.getPercentInstance().format(additional) + " of "
                            + this.observable.getType(), null);

                    coverage = cv;
                    // link up the model into a common one.
                    if (node == null) {
                        node = child.node;
                    } else {

                        KLAB.info("MUST MERGE MODELS " + node.model + " WITH " + child.model);
                        /*
                         * TODO
                         * substitute the model in the node with the merged model for the
                         * additional coverage.
                         */
                    }

                    /*
                     * TODO store merged model in cache in lieu of previous
                     * store in cache
                     */

                } else {
                    noOp = true;
                }

            }

        } else if (type.equals(Type.GROUP)) {

            /*
             * merge according to the connector we are using
             */
            if (connector.equals(LogicalConnector.INTERSECTION)) {
                coverage = coverage.and(child.coverage);
            } else {
                coverage = coverage.or(child.coverage);
            }

        } else {
            coverage = child.coverage;
        }

        /*
         * merge the child's context if we're using it within this subject. Of course
         * we don't merge anything if we're resolving a child subject - we should, though,
         * link the provenance info in some way.
         */
        if (!coverage.isEmpty()) {

            for (IObservable o : child.models.keySet()) {
                if (!(child.models.get(o) instanceof StateModel)) {
                    models.put(o, child.models.get(o));
                }
            }
            instantiatorModels.putAll(child.instantiatorModels);
            noModelsFor.addAll(child.noModelsFor);

            if (child.type == Type.SUBJECT) {

                if (model == null) {
                    model = child.model;
                }
                /*
                 * TODO link provenance to parent context
                 */
                // child.resolutionStrategy.execute(transition);

            } else {

                // if (!isDirect) {

                modelGraph.merge(child.modelGraph);
                resolutionStrategy.merge(child.resolutionStrategy);

                if (!noOp) {
                    if (source != null) {
                        modelGraph.add(source);
                    }
                    if (source != null && target != null && dlink != null) {
                        modelGraph.link(target, source, dlink);
                        CallTracer.msg("Linked " + dlink.describeType());
                    }

                    /*
                     * if we're in a group and we have all the coverage we need, we also want to merge the found models with our own
                     * cache, so the next dependency can find them.
                     * 
                     * HMMM may not work - what happens to the next in line for the same observable in a union? Must be done when coverage is sufficient
                     */
                    // if (parent != null && parent.type.equals(Type.GROUP)) {
                    // parent.models.putAll(models);
                    // }
                }

                /*
                 * float these if we're just a linkpoint to the parent.
                 */
                if (node == null && link == null) {

                    node = child.node;
                    link = child.link;

                    /*
                     * "color" the link if our context contains anything relevant to it.
                     */
                    if (link != null) {
                        if (dependency != null) {
                            link.formalName = dependency.getFormalName();
                            link.property = dependency.getProperty();
                        }
                        link.condition = condition;
                        link.conditionIndex = conditionIndex;
                    }
                }
                // }

                /*
                 * inherit the model if we have resolved a new one in the child and we are still
                 * resolving.
                 */
                if (model == null /* && !isDirect */) {
                    model = child.model;
                }

                /*
                 * float the scale (only to the root context) or harmonize it with 
                 */
                if (scale == null) {
                    scale = child.scale;
                } else {

                    /*
                     * TODO harmonize the common scale with that of the object we have just resolved.
                     */
                    scale = scale.harmonize(child.scale);
                }
            }
        }

        /*
         * if we've been given metadata by the resolver, transfer them to
         * the node we're handling.
         */
        if (metadata != null && node != null) {
            node.getMetadata().merge(metadata, true);
        }

        return coverage;

    }

    private HashMap<IObservable, IModel> getModelCache(boolean isExplanatoryModel) {
        return isExplanatoryModel ? models : instantiatorModels;
    }

    /**
     * Get the first parent node available. Used when we must connect a process model's dependencies to
     * the subject they inhere to.
     * 
     * @return
     */
    private ProvenanceNode getParentNode() {
        ResolutionContext ctx = parent;
        while (ctx != null && ctx.node == null) {
            ctx = ctx.parent;
        }
        return ctx == null ? null : ctx.node;
    }

    public boolean isRoot() {
        return type.equals(Type.ROOT);
    }

    @Override
    public String toString() {
        return modelGraph.vertexSet().size() + "#{" + type + " # "
                + (coverage == null ? "null" : coverage.getCoverage()) + " # " + subject
                + "/" + model + "/" + observer + "}";
    }

    public boolean isRootSubjectObservable() {
        return type.equals(Type.OBSERVABLE) && parent.type.equals(Type.SUBJECT)
                && parent.parent.type.equals(Type.ROOT);
    }

    @Override
    public Set<IConcept> getTraits() {
        // don't return null, make it easy for your friends.
        return attributes == null ? new HashSet<IConcept>() : attributes;
    }

    /*
     * resolve the current context with the model passed, which is already in the
     * provenance graph.
     */
    public ICoverage resolve(IModel model) throws ThinklabException {
        coverage = coverages.get(model);
        modelGraph.add(modelGraph.getNode(model));
        return finish();
    }

    @Override
    public boolean isOptional() {
        return isOptional;
    }

    public boolean isResolving(IModel m) {
        return resolving.contains(m);
    }

    public void reset() {

        for (ResolutionContext c = this; c != null; c = c.parent) {
            c.modelGraph = new ProvenanceGraph(monitor);
            for (IState s : subject.getStates()) {
                c.models.put(s.getObserver().getObservable(), new StateModel(s.getObservable(), s, subject
                        .getNamespace()));
            }
        }
    }

    @Override
    public IResolutionStrategy getResolutionStrategy() {

        /*
         * called when this is used after resolution, so set the scale, monitor and coverage
         * in the returned strategy.
         * 
         * TODO this would also be a good time to send notifications documenting
         * the full strategy.
         */
        if (resolutionStrategy._coverage == null) {
            resolutionStrategy._coverage = this.coverage;
            resolutionStrategy._scale = this.scale;
            resolutionStrategy._csubject = this.contextSubject;
        }
        return resolutionStrategy;
    }

    @Override
    public void defineWorkflow() throws ThinklabException {

        IDirectObservation subject = getSubject();
        IProvenance provenance = getProvenance();
        IModel model = getModel();
        IMonitor monitor = getMonitor();

        if (!provenance.isEmpty()) {
            monitor.getTaskIntrospector().informationAvailable(null, Info.PROVENANCE_GRAPH, provenance);
            Workflow workflow = new Workflow(subject, model, provenance, monitor, getCoverage(), ((Subject) subject)
                    .getContext().getId());
            monitor.getTaskIntrospector().informationAvailable(null, Info.DATAFLOW_GRAPH, workflow);
            resolutionStrategy.addStep(provenance, workflow);
        }
    }

    @Override
    public void acceptModel() {
        if (model instanceof KIMModel
                && (((KIMModel) model).hasActuator() || ((KIMModel) model).getActions().size() > 0
                        || ((KIMModel) model).hasObjectSource())) {
            resolutionStrategy.addStep(model, interpretAs);
        }
    }

    /*
     * change the default context from seeking instantiators to seeking
     * explanatory models.
     */
    public ResolutionContext forExplanatoryModel() {
        isExplanatoryModel = true;
        return this;
    }

    @Override
    public boolean isForInstantiation() {
        return !isExplanatoryModel;
    }

    /**
     * Calls the resolution strategy's execute method and sets up the results in the
     * context caches, so that previous resolutions do not need to be attempted again.
     * 
     * @throws ThinklabException
     */
    @Override
    public void execute(ITransition transition) throws ThinklabException {
        getResolutionStrategy().execute(transition, this);
    }

    @Override
    public IModelPrioritizer<IModelMetadata> getPrioritizer() {
        if (prioritizer == null) {
            prioritizer = new ModelPrioritizer(this);
        }
        return prioritizer;
    }

    @Override
    public int getNamespaceDistance(INamespace ns) {

        if (ns == null) {
            return -1;
        }

        int ret = 0;
        if (!this.resolutionNamespace.getId().equals(ns.getId())) {
            ResolutionContext rc = parent;
            while (rc != null) {
                ret++;
                if (rc.resolutionNamespace != null && rc.resolutionNamespace.getId().equals(ns.getId())) {
                    return ret;
                }
                rc = rc.parent;
            }
        }

        return -1;
    }

    @Override
    public int getProjectDistance(IProject ns) {

        if (ns == null) {
            return -1;
        }

        if (this.resolutionNamespace.getProject() != null
                && this.resolutionNamespace.getProject().getId().equals(ns.getId())) {
            return 0;
        }

        int ret = 0;
        if (this.resolutionNamespace.getProject() == null
                || !this.resolutionNamespace.getProject().getId().equals(ns.getId())) {
            ResolutionContext rc = parent;
            while (rc != null) {
                ret++;
                if (rc.resolutionNamespace != null && rc.resolutionNamespace.getProject() != null
                        && rc.resolutionNamespace.getProject().getId().equals(ns.getId())) {
                    return ret;
                }
                rc = rc.parent;
            }
        }

        return -1;
    }

}
