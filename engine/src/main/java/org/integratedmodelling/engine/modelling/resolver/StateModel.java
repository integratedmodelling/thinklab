/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.engine.modelling.resolver;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import org.integratedmodelling.api.knowledge.IConcept;
import org.integratedmodelling.api.knowledge.IKnowledge;
import org.integratedmodelling.api.knowledge.IProperty;
import org.integratedmodelling.api.metadata.IMetadata;
import org.integratedmodelling.api.modelling.IAction;
import org.integratedmodelling.api.modelling.IAnnotation;
import org.integratedmodelling.api.modelling.IDataSource;
import org.integratedmodelling.api.modelling.IDependency;
import org.integratedmodelling.api.modelling.IDirectObservation;
import org.integratedmodelling.api.modelling.IModel;
import org.integratedmodelling.api.modelling.IModelObject;
import org.integratedmodelling.api.modelling.INamespace;
import org.integratedmodelling.api.modelling.IObjectSource;
import org.integratedmodelling.api.modelling.IObservable;
import org.integratedmodelling.api.modelling.IObserver;
import org.integratedmodelling.api.modelling.IScale;
import org.integratedmodelling.api.modelling.IState;
import org.integratedmodelling.api.modelling.runtime.IActiveDataSource;
import org.integratedmodelling.api.modelling.runtime.IActuator;
import org.integratedmodelling.api.modelling.scheduling.ITransition;
import org.integratedmodelling.api.modelling.storage.IDataset;
import org.integratedmodelling.api.monitoring.IMonitor;
import org.integratedmodelling.collections.Pair;
import org.integratedmodelling.common.kim.KIMObserver;
import org.integratedmodelling.common.model.actuators.StateActuator;
import org.integratedmodelling.common.states.State;
import org.integratedmodelling.common.states.StateView;
import org.integratedmodelling.exceptions.ThinklabException;

/**
 * Wraps a previously computed IState into a IModel that can be used to resolve a dependency for something
 * that was computed earlier.
 * 
 * @author Ferd
 * 
 */
public class StateModel implements IModel {

    IObservable       _observable;
    IState            _state;
    IActiveDataSource _datasource;
    INamespace        _namespace;
    IObserver         _observer;

    class StateDatasource implements IActiveDataSource {

        class StateDSActuator extends StateActuator {

            int _idx = 0;

            public StateDSActuator(IMonitor monitor) {
                super(new ArrayList<IAction>(), monitor);
            }

            @Override
            public void process(int stateIndex, ITransition transition) throws ThinklabException {
                _idx = stateIndex;
            }

            @Override
            public Object getValue(String outputKey) {
                return _state.getValue(_idx);
            }

            @Override
            public String toString() {
                return "<StateDSActuator " + _state.getObservable() + ">";
            }

            // @Override
            public Collection<IState> createStates(IDirectObservation context, IModel model, IObserver obs, ProvenanceGraph provenance, IDataset dataset) {
                /*
                 * only called on a view
                 */
                _states = new ArrayList<>();
                _states.add(_state);
                return _states;
            }

        }

        @Override
        public IMetadata getMetadata() {
            return _state.getMetadata();
        }

        @Override
        public IActuator getAccessor(IScale context, IObserver contextObserver, IMonitor monitor)
                throws ThinklabException {
            return new StateDSActuator(monitor);
        }

        @Override
        public IScale getCoverage() {
            return _state.getScale();
        }

        @Override
        public String toString() {
            return "Previous observation of " + _state.getObservable().getType();
        }

        @Override
        public boolean isAvailable() {
            return true;
        }
    }

    public StateModel(IObservable o, IState s, INamespace namespace) {
        _observable = o;
        _state = s;
        _datasource = new StateDatasource();
        _namespace = namespace;
        /*
         * if we don't copy the observer, the dataflow will compile itself until the end of all heaps.
         */
        _observer = ((KIMObserver) _state.getObserver()).copy();
    }

    @Override
    public List<IDependency> getDependencies() {
        return new ArrayList<IDependency>();
    }

    @Override
    public boolean hasActionsFor(IConcept observable, IConcept domainConcept) {
        return false;
    }

    @Override
    public IScale getCoverage() {
        return _state.getScale();
    }

    @Override
    public List<IAction> getActions() {
        return new ArrayList<IAction>();
    }

    @Override
    public boolean isComputed() {
        return false;
    }

    @Override
    public IObservable getObservable() {
        return _observable;
    }

    @Override
    public String getId() {
        return ((State) _state).getInternalId();
    }

    @Override
    public boolean isPrivate() {
        return false;
    }

    @Override
    public boolean isInactive() {
        return false;
    }

    @Override
    public Collection<IAnnotation> getAnnotations() {
        return new ArrayList<IAnnotation>();
    }

    @Override
    public int getFirstLineNumber() {
        return 0;
    }

    @Override
    public int getLastLineNumber() {
        return 0;
    }

    @Override
    public INamespace getNamespace() {
        return _namespace;
    }

    @Override
    public String getName() {
        return "previous observation";
    }

    @Override
    public IMetadata getMetadata() {
        return _state.getMetadata();
    }

    @Override
    public List<IObservable> getObservables() {
        return Collections.singletonList(_observable);
    }

    @Override
    public IDataSource getDatasource(IMonitor monitor) {
        return _datasource;
    }

    @Override
    public IObjectSource getObjectSource(IMonitor monitor) {
        return null;
    }

    @Override
    public IObserver getObserver() {
        return _observer;
    }

    @Override
    public boolean isResolved() {
        return true;
    }

    @Override
    public String toString() {
        return "<StateModel " + _state.getObservable() + ">";
    }

    public boolean isView() {
        return _state instanceof StateView;
    }

    @Override
    public IKnowledge getType() {
        return _state.getType();
    }

    @Override
    public boolean isInstantiator() {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public int getErrorCount() {
        // TODO Auto-generated method stub
        return 0;
    }

    @Override
    public int getWarningCount() {
        // TODO Auto-generated method stub
        return 0;
    }

    @Override
    public boolean isFirstClass() {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public boolean isDeprecated() {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public Collection<IModelObject> getChildren() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public IModelObject getParent() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public Object getInlineValue() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public IActuator getActuator(IMonitor monitor) throws ThinklabException {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public Collection<Pair<String, IObserver>> getAttributeObservers() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public Collection<Pair<String, IProperty>> getAttributeMetadata() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public boolean isAvailable() {
        return true;
    }

}
