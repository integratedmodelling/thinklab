/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.engine.modelling.runtime;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import org.integratedmodelling.api.knowledge.IProperty;
import org.integratedmodelling.api.modelling.IExtent;
import org.integratedmodelling.api.modelling.IModel;
import org.integratedmodelling.api.modelling.INamespace;
import org.integratedmodelling.api.modelling.IObservable;
import org.integratedmodelling.api.modelling.IRelationship;
import org.integratedmodelling.api.modelling.IScale;
import org.integratedmodelling.api.modelling.IState;
import org.integratedmodelling.api.modelling.IStructure;
import org.integratedmodelling.api.modelling.ISubject;
import org.integratedmodelling.api.monitoring.IMonitor;
import org.integratedmodelling.common.configuration.KLAB;
import org.integratedmodelling.common.states.States;
import org.integratedmodelling.common.utils.MapUtils;
import org.integratedmodelling.common.utils.NameGenerator;
import org.integratedmodelling.common.vocabulary.NS;
import org.integratedmodelling.common.vocabulary.Observable;
import org.integratedmodelling.exceptions.ThinklabException;
import org.integratedmodelling.exceptions.ThinklabValidationException;
import org.integratedmodelling.lang.IRemoteSerializable;

public class Relationship extends DirectObservation implements IRelationship, IRemoteSerializable {

    protected String  internalID = NameGenerator.shortUUID();
    ArrayList<IState> states     = new ArrayList<>();
    IModel            model      = null;
    ISubject          source;
    ISubject          target;

    public Relationship(IStructure structure, IObservable observable, IScale scale, INamespace namespace,
            ISubject source, ISubject target, String name, IMonitor monitor) {
        super(observable, scale, namespace, name, monitor);
        this.source = source;
        this.target = target;
    }

    public void addState(IState state) {
        states.add(state);
    }

    @Override
    public boolean equals(Object obj) {
        return obj instanceof Relationship && ((Relationship) obj).internalID.equals(internalID);
    }

    @Override
    public int hashCode() {
        return internalID.hashCode();
    }

    @Override
    public Object adapt() {
        return MapUtils.of("source", getSource().getId(), "target", getTarget()
                .getId(), "observable", getObservable(), "scale", scale, "states", getStates(), "name", name, "internal-id", internalID
        // TODO scale
        // TODO namespace
        );
    }

    @Override
    public ISubject getSource() {
        return source;
    }

    @Override
    public ISubject getTarget() {
        return target;
    }

    @Override
    public Map<IProperty, IState> getObjectStateCopy() {
        HashMap<IProperty, IState> result = new HashMap<IProperty, IState>();
        for (IState state : states) {
            if (!(state instanceof IExtent)) {
                result.put(KLAB.p(NS.PART_OF), state);
            }
        }
        return result;
    }

    @Override
    public Collection<IState> getStates() {
        return states;
    }

    @Override
    public String getId() {
        return name;
    }

    @Override
    public IState getStaticState(IObservable obs) throws ThinklabException {

        for (IState s : states) {
            if (obs.equals(s.getObservable())) {
                return s;
            }
        }

        if (!NS.isQuality(obs)) {
            throw new ThinklabValidationException("cannot create a state for a non-quality: "
                    + obs.getType());
        }

        if (obs.getInherentType() == null) {
            obs = ((Observable) obs).withInherentType(this.getObservable().getType());
        }

        IState ret = States.createStatic(obs, this);
        states.add(ret);
        return ret;
    }

    @Override
    public IState getState(IObservable obs, Object... data) throws ThinklabException {

        for (IState s : states) {
            if (obs.equals(s.getObservable())) {
                return s;
            }
        }

        if (!NS.isQuality(obs)) {
            throw new ThinklabValidationException("cannot create a state for a non-quality: "
                    + obs.getType());
        }

        if (obs.getInherentType() == null) {
            obs = ((Observable) obs).withInherentType(this.getObservable().getType());
        }

        IState ret = States.create(obs, this);
        states.add(ret);
        return ret;
    }

    @Override
    public void link(IStructure structure, Map<String, ISubject> subCatalog) {
        // TODO Auto-generated method stub

    }
}
