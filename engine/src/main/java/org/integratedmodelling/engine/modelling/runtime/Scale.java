/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.engine.modelling.runtime;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.integratedmodelling.api.knowledge.IConcept;
import org.integratedmodelling.api.modelling.IExtent;
import org.integratedmodelling.api.modelling.IScale;
import org.integratedmodelling.api.modelling.ITopologicallyComparable;
import org.integratedmodelling.api.space.ISpatialExtent;
import org.integratedmodelling.api.time.ITemporalExtent;
import org.integratedmodelling.collections.MultidimensionalCursor;
import org.integratedmodelling.collections.MultidimensionalCursor.StorageOrdering;
import org.integratedmodelling.common.model.runtime.AbstractScale;
import org.integratedmodelling.engine.geospace.Geospace;
import org.integratedmodelling.engine.geospace.extents.SpaceExtent;
import org.integratedmodelling.engine.time.Time;
import org.integratedmodelling.engine.time.extents.RegularTemporalGrid;
import org.integratedmodelling.exceptions.ThinklabException;
import org.integratedmodelling.exceptions.ThinklabRuntimeException;
import org.integratedmodelling.exceptions.ThinklabValidationException;
import org.integratedmodelling.lang.IRemoteSerializable;
import org.integratedmodelling.lang.LogicalConnector;

public class Scale extends AbstractScale implements IScale, IRemoteSerializable {

    List<IExtent>                  _extents      = new ArrayList<IExtent>();
    long                           _multiplicity = 0;
    int                            _sIndex       = -1;
    int                            _tIndex       = -1;
    private MultidimensionalCursor _cursor;

    // originalCursor != null means we derive from a previous scale and are representing one slice of it...
    private MultidimensionalCursor originalCursor = null;
    // ... identified by this offset...
    private int                    sliceOffset    = -1;
    // ... along this dimension
    private int                    sliceDimension = -1;

    public Scale(IExtent... topologies) throws ThinklabException {
        for (IExtent e : topologies) {
            mergeExtent(e, true);
        }
    }

    public Scale(Collection<IExtent> topologies) throws ThinklabException {
        this(topologies.toArray(new IExtent[topologies.size()]));
    }

    private Scale(IExtent[] topologies, MultidimensionalCursor cursor, int sliceExtentIndex,
            int sliceExtentOffset) throws ThinklabException {

        originalCursor = cursor;
        sliceDimension = sliceExtentIndex;
        sliceOffset = sliceExtentOffset;

        for (IExtent e : topologies) {
            mergeExtent(e, true);
        }
    }

    public Scale() {
        // TODO Auto-generated constructor stub
    }

    @Override
    public IExtent getExtent(int index) {
        return _extents.get(index);
    }

    @Override
    public Iterator<IExtent> iterator() {
        return _extents.iterator();
    }

    @Override
    public long getMultiplicity() {
        return _multiplicity;
    }

    @Override
    public boolean contains(IScale scale) throws ThinklabException {

        if (!hasSameExtents(scale)) {
            return false;
        }

        for (IExtent e : _extents) {
            if (!e.contains(((Scale) scale).getExtent(e.getDomainConcept()))) {
                return false;
            }
        }
        return true;
    }

    @Override
    public boolean overlaps(IScale scale) throws ThinklabException {

        if (!hasSameExtents(scale)) {
            return false;
        }

        for (IExtent e : _extents) {
            if (!e.overlaps(((Scale) scale).getExtent(e.getDomainConcept()))) {
                return false;
            }
        }
        return true;
    }

    @Override
    public boolean intersects(IScale scale) throws ThinklabException {

        if (!hasSameExtents(scale)) {
            return false;
        }

        for (IExtent e : _extents) {
            if (!e.intersects(((Scale) scale).getExtent(e.getDomainConcept()))) {
                return false;
            }
        }
        return true;
    }

    @Override
    public IScale intersection(IScale scale) throws ThinklabException {

        if (!hasSameExtents(scale)) {
            return null;
        }

        Scale ret = new Scale();
        for (IExtent e : _extents) {
            ret.mergeExtent(e.intersection(((Scale) scale).getExtent(e.getDomainConcept())), false);
        }

        return ret;
    }

    @Override
    public IScale union(IScale scale) throws ThinklabException {

        if (!hasSameExtents(scale)) {
            return null;
        }

        Scale ret = new Scale();
        for (IExtent e : _extents) {
            ret.mergeExtent(e.union(((Scale) scale).getExtent(e.getDomainConcept())), false);
        }

        return ret;
    }

    @Override
    public ITemporalExtent getTime() {
        return (ITemporalExtent) (_tIndex < 0 ? null : _extents.get(_tIndex));
    }

    @Override
    public ISpatialExtent getSpace() {
        return (ISpatialExtent) (_sIndex < 0 ? null : _extents.get(_sIndex));
    }

    public void mergeExtent(IExtent extent, boolean force) throws ThinklabException {

        IExtent merged = null;
        int i = 0;
        for (IExtent e : _extents) {
            if (e.getDomainConcept().equals(extent.getDomainConcept())) {
                merged = e.merge(extent, force);
                break;
            }
            i++;
        }

        if (merged != null) {
            _extents.add(i, merged);
        } else {
            _extents.add(extent);
        }

        sort();
    }

    /**
     * Return a collection of scales with multiplicity 1, one per each combination of the extent states we
     * represent.
     *
     * @return
     * @throws ThinklabException
     */
    public Collection<IScale> disaggregate() throws ThinklabException {

        ArrayList<IScale> ret = new ArrayList<IScale>();

        int[] dims = new int[_extents.size()];
        for (int i = 0; i < dims.length; i++) {
            dims[i] = (int) _extents.get(i).getMultiplicity();
        }

        MultidimensionalCursor cursor = new MultidimensionalCursor();
        cursor.defineDimensions(dims);

        for (int i = 0; i < cursor.getMultiplicity(); i++) {
            IExtent[] exts = new IExtent[dims.length];
            int[] idx = cursor.getElementIndexes(i);
            for (int j = 0; j < exts.length; j++) {
                exts[j] = _extents.get(j).getExtent(idx[j]);
            }
            ret.add(new Scale(exts));
        }

        return ret;

    }

    private void sort() {

        ArrayList<IExtent> order = new ArrayList<IExtent>(_extents);

        /*
         * Is it fair to think that if two extent concepts have an ordering relationship, they should know
         * about each other? So that we can implement the ordering as a relationship between extent
         * observation classes?
         *
         * For now, all we care about is that time, if present, comes first. We just check for that using the
         * ontology name, which of course sucks.
         */
        Collections.sort(order, new Comparator<IExtent>() {

            @Override
            public int compare(IExtent o1, IExtent o2) {
                // neg if o1 < o2
                boolean o1t = o1.getDomainConcept().getConceptSpace().equals("time");
                boolean o2t = o2.getDomainConcept().getConceptSpace().equals("time");
                if (o1t && !o2t) {
                    return -1;
                }
                if (!o1t && o2t) {
                    return 1;
                }
                return 0;
            }
        });

        _multiplicity = 1L;
        int idx = 0;
        for (IExtent e : order) {

            if (e.getMultiplicity() == INFINITE) {
                _multiplicity = INFINITE;
            }
            if (e.getDomainConcept().equals(Time.domainConcept()))
                _tIndex = idx;
            if (e.getDomainConcept().equals(Geospace.get().SpatialDomain()))
                _sIndex = idx;

            if (_multiplicity != INFINITE)
                _multiplicity *= e.getMultiplicity();

            idx++;
        }

        // better safe than sorry. Only time can be infinite so this should be pretty safe as long as
        // the comparator above works.
        if (_multiplicity == INFINITE && _extents.get(0).getMultiplicity() != INFINITE) {
            throw new ThinklabRuntimeException("internal error: infinite dimension not the first in scale");
        }

        // recompute strided offsets for quick extent access
        _cursor = new MultidimensionalCursor(StorageOrdering.ROW_FIRST);
        int[] dims = new int[_multiplicity == INFINITE ? _extents.size() - 1 : _extents.size()];
        int n = 0;
        for (int i = _multiplicity == INFINITE ? 1 : 0; i < _extents.size(); i++) {
            dims[n++] = (int) _extents.get(i).getMultiplicity();
        }
        _cursor.defineDimensions(dims);
        _extents = order;
    }

    /*
     * quick access to "current" T state index for given offset - not in the API for now.
     */
    public int getTimeIndex(int globalIndex) {
        return _tIndex == -1 ? -1 : _cursor.getElementIndexes(globalIndex)[_tIndex];
    }

    /*
     * quick access to "current" S state index for given offset - not in the API for now.
     */
    public int getSpaceIndex(int globalIndex) {
        return _sIndex == -1 ? -1 : _cursor.getElementIndexes(globalIndex)[_sIndex];
    }

    /*
     * quick access to "current" arbitrary state index for given offset - not in the API for now.
     */
    public int[] getExtentIndex(int globalIndex) {
        return _cursor.getElementIndexes(globalIndex);
    }

    /*
         * true if the passed scale has the same extents as we do.
         */
    boolean hasSameExtents(IScale scale) {

        for (IExtent e : scale) {
            if (getExtent(e.getDomainConcept()) == null) {
                return false;
            }
        }

        for (IExtent e : _extents) {
            if (((Scale) scale).getExtent(e.getDomainConcept()) == null) {
                return false;
            }
        }
        return true;
    }

    /*
     * get the extent with the passed domain concept
     */
    @Override
    public IExtent getExtent(IConcept domainConcept) {
        for (IExtent e : _extents) {
            if (e.getDomainConcept().equals(domainConcept)) {
                return e;
            }
        }
        return null;
    }

    // /**
    // * Scan all extents and return the properties and values, if any, that describe their coverage for
    // search
    // * and retrieval of compatible extents.
    // *
    // * It works by asking each extent for its storage metadata and returning any metadata that is indexed by
    // a
    // * known property and points to a topologically comparable object.
    // *
    // * Relies on the fact that each extent has only one topologically comparable storage metadata. Throws an
    // * unchecked exception if not so.
    // *
    // * @return
    // * @throws ThinklabException
    // */
    // public List<Pair<IProperty, ITopologicallyComparable<?>>> getCoverageProperties(IMonitor monitor)
    // throws ThinklabException {
    // ArrayList<Pair<IProperty, ITopologicallyComparable<?>>> ret = new ArrayList<Pair<IProperty,
    // ITopologicallyComparable<?>>>();
    // for (IExtent ext : _extents) {
    // int ncov = 0;
    // if (ext instanceof IStorageMetadataProvider) {
    // Metadata md = new Metadata();
    // ((IStorageMetadataProvider) ext).addStorageMetadata(md, monitor);
    // for (String pid : md.getKeys()) {
    // if (Thinklab.get().getProperty(pid) != null
    // && md.get(pid) instanceof ITopologicallyComparable<?>) {
    //
    // if (ncov > 0) {
    //
    // /*
    // * this is an obscure one for sure, but it should not really happen unless the
    // * implementation is screwed up and untested.
    // */
    // throw new ThinklabRuntimeException(
    // "internal: extent provides more than one topologically comparable storage metadata");
    // }
    //
    // ret.add(new Pair<IProperty, ITopologicallyComparable<?>>(Thinklab.p(pid),
    // (ITopologicallyComparable<?>) md.get(pid)));
    // ncov++;
    // }
    // }
    // }
    // }
    // return ret;
    // }

    //
    // /**
    // * Return the scale without time, self if we don't see time.
    // *
    // * @return
    // */
    // @Override
    // public IScale getNonDynamicScale() {
    //
    // if (getTime() == null) {
    // return this;
    // }
    //
    // int i = 0;
    // IExtent[] exts = new IExtent[_extents.size() - 1];
    // for (IExtent e : _extents) {
    // if (e.getDomainConcept().equals(Time.TIME_DOMAIN)) {
    // continue;
    // }
    // exts[i++] = e;
    // }
    // try {
    // return new Scale(exts);
    // } catch (ThinklabException e1) {
    // // shouldn't happen if we get as far as this.
    // throw new ThinklabRuntimeException(e1);
    // }
    // }

    @Override
    public int getExtentCount() {
        return _extents.size();
    }

    public List<IExtent> getExtents() {
        return _extents;
    }

    /**
     * Return the proportion of coverage of the extent that is covered the least by the corresponding extent
     * in the passed scale.
     *
     * @param context
     * @return
     */
    public double getCoverage(IScale context) {
        // TODO Auto-generated method stub
        return 1.0;
    }

    /**
     * Return the proportion of coverage that the passed scale would add to the coverage of our own extents.
     *
     * @param mcov
     * @return
     */
    public double getAdditionalCoverage(Scale mcov) {
        // TODO Auto-generated method stub
        return 1.0;
    }

    @Override
    public ITopologicallyComparable<IScale> union(ITopologicallyComparable<?> other)
            throws ThinklabException {

        if (!(other instanceof Scale)) {
            throw new ThinklabValidationException(other + " intersected with a Scale");
        }

        return merge((Scale) other, LogicalConnector.UNION, true);
    }

    @Override
    public ITopologicallyComparable<IScale> intersection(ITopologicallyComparable<?> other)
            throws ThinklabException {

        if (!(other instanceof Scale)) {
            throw new ThinklabValidationException(other + " intersected with a Scale");
        }

        return merge((Scale) other, LogicalConnector.INTERSECTION, true);
    }

    @Override
    public double getCoveredExtent() {
        /*
         * TODO multiply extents of extents.
         */
        return 1;
    }

    @Override
    public IScale merge(IScale scale, LogicalConnector how, boolean adopt) throws ThinklabException {

        Scale other = (Scale) scale;
        Scale ret = new Scale();
        ArrayList<IExtent> common = new ArrayList<IExtent>();
        HashSet<IConcept> commonConcepts = new HashSet<IConcept>();

        for (IExtent e : this) {
            if (other.getExtent(e.getDomainConcept()) != null) {
                common.add(e);
                commonConcepts.add(e.getDomainConcept());
            } else {
                ret.mergeExtent(e, true);
            }
        }

        if (adopt) {
            for (IExtent e : other) {
                if (adopt && ret.getExtent(e.getDomainConcept()) == null
                        && !commonConcepts.contains(e.getDomainConcept())) {
                    ret.mergeExtent(e, true);
                }
            }
        }

        for (IExtent e : common) {
            IExtent oext = other.getExtent(e.getDomainConcept());
            IExtent merged = null;
            if (how.equals(LogicalConnector.INTERSECTION)) {
                merged = e.intersection(oext);
            } else if (how.equals(LogicalConnector.UNION)) {
                merged = e.union(oext);
            } else {
                throw new ThinklabValidationException("extents are being merged with illegal operator" + how);
            }
            ret.mergeExtent(merged, true);
        }

        return ret;
    }

    @Override
    public String toString() {
        String ss = "";
        for (IExtent e : _extents) {
            ss += "<" + e.getDomainConcept() + " # " + e.getMultiplicity() + ">";
        }
        return "Scale #" + _extents.size() + " " + ss;
    }

    @Override
    public boolean isEmpty() {

        for (IExtent e : _extents) {
            if (e.isEmpty()) {
                return true;
            }
        }
        return false;
    }

    @Override
    public Object adapt() {
        Map<String, Object> ret = new HashMap<String, Object>();
        ret.put("extents", new ArrayList<IExtent>(_extents));
        ret.put("multiplicity", _multiplicity);
        return ret;
    }

    @Override
    public MultidimensionalCursor getCursor() {
        return _cursor;
    }

    @Override
    public IScale harmonize(IScale scale) throws ThinklabException {
        // TODO Auto-generated method stub
        return scale;
    }

    @Override
    public IScale getSubscale(IConcept extent, int offset) {

        int oridx = -1;
        ArrayList<IExtent> exts = new ArrayList<>();
        for (int i = 0; i < _extents.size(); i++) {
            if (_extents.get(i).getDomainConcept().equals(extent)) {
                oridx = i;
                continue;
            }
            exts.add(_extents.get(i));
        }

        if (oridx < 0) {
            return this;
        }

        try {
            return new Scale(exts.toArray(new IExtent[exts.size()]), getCursor(), oridx, offset);
        } catch (ThinklabException e1) {
            // should never happen since we build it with previously accepted extents.
            throw new ThinklabRuntimeException(e1);
        }
    }

    @Override
    public long getOriginalOffset(long subscaleOffset) {

        if (originalCursor == null) {
            return subscaleOffset;
        }

        int[] slcofs = getCursor().getElementIndexes((int) subscaleOffset);
        int[] orgofs = new int[originalCursor.getDimensionsCount()];
        int on = 0;
        for (int i = 0; i < orgofs.length; i++) {
            orgofs[i] = i == sliceDimension ? sliceOffset : slcofs[on++];
        }
        return originalCursor.getElementOffset(orgofs);
    }

    /**
     * Take a scale from any origin and return another whose extents are 
     * guaranteed to be the engine's implementation.
     * 
     * @param scale
     * @return
     * @throws ThinklabException 
     */
    public static IScale sanitize(IScale scale) throws ThinklabException {

        ArrayList<IExtent> extents = new ArrayList<>();
        for (IExtent e : scale) {
            if (e instanceof ISpatialExtent) {
                extents.add(SpaceExtent.sanitize((ISpatialExtent) e));
            } else if (e instanceof ITemporalExtent) {
                extents.add(RegularTemporalGrid.sanitize((ITemporalExtent) e));
            } else {
                /*
                 * nothing yet, but if anything, they should be simple enough to
                 * have one implementation.
                 */
                extents.add(e);
            }
        }
        return new Scale(extents.toArray(new IExtent[extents.size()]));
    }

    public static IScale substituteExtent(IScale scale, IExtent extent) throws ThinklabException {

        List<IExtent> exts = new ArrayList<>();
        for (IExtent e : scale) {
            if (e.getDomainConcept().equals(extent.getDomainConcept())) {
                exts.add(extent);
            } else {
                exts.add(e);
            }
        }
        return new Scale(exts.toArray(new IExtent[exts.size()]));
    }
}
