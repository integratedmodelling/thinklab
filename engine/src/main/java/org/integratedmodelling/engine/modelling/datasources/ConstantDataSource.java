/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.engine.modelling.datasources;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;

import org.integratedmodelling.api.metadata.IMetadata;
import org.integratedmodelling.api.modelling.IAction;
import org.integratedmodelling.api.modelling.IDirectObservation;
import org.integratedmodelling.api.modelling.IModel;
import org.integratedmodelling.api.modelling.IObservable;
import org.integratedmodelling.api.modelling.IObserver;
import org.integratedmodelling.api.modelling.IObservingObject;
import org.integratedmodelling.api.modelling.IScale;
import org.integratedmodelling.api.modelling.IState;
import org.integratedmodelling.api.modelling.resolution.IProvenance;
import org.integratedmodelling.api.modelling.runtime.IActiveDataSource;
import org.integratedmodelling.api.modelling.runtime.IActuator;
import org.integratedmodelling.api.modelling.runtime.IRawActuator;
import org.integratedmodelling.api.modelling.storage.IDataset;
import org.integratedmodelling.api.monitoring.IMonitor;
import org.integratedmodelling.common.HashableObject;
import org.integratedmodelling.common.kim.KIMObservable;
import org.integratedmodelling.common.metadata.Metadata;
import org.integratedmodelling.common.model.actuators.StateActuator;
import org.integratedmodelling.common.states.State;
import org.integratedmodelling.common.vocabulary.NS;
import org.integratedmodelling.engine.modelling.runtime.Scale;
import org.integratedmodelling.exceptions.ThinklabException;

/**
 * A datasource that returns the same object no matter what.
 * 
 * @author Ferd
 * 
 */
public class ConstantDataSource extends HashableObject implements IActiveDataSource {

    private Object    _state    = null;
    private IMetadata _metadata = new Metadata();

    public class ConstantActuator extends StateActuator implements IRawActuator {

        public ConstantActuator(List<IAction> actions, IMonitor monitor) {
            super(actions, monitor);
        }

        @Override
        public Object getValue(String outputKey) {
            return _state;
        }

        @Override
        public String getDatasourceLabel() {
            return "[constant: " + _state + "]";
        }

        @Override
        public String toString() {
            return _state == null ? "null" : _state.toString();
        }

        @Override
        public Collection<IState> createStates(IDirectObservation context, IModel model, IObserver obs, IProvenance provenance, IDataset dataset) {

            this.context = context;

            /**
             * Add metadata from context once for all - process() will ensure these are
             * available to the actions.
             */
            if (context != null && contextMetadata == null) {
                contextMetadata = new HashMap<>();
                contextMetadata.put(IMetadata.IM_NAME, context.getId());
                for (String s : context.getMetadata().getKeys()) {
                    contextMetadata.put(s, context.getMetadata().get(s));
                }
            }

            if (_outputs.size() == 0)
                return null;

            
            _states = new ArrayList<>();
            
            for (String key : _outputs.keySet()) {

                IObserver observer = _outputs.get(key);

                /*
                 * label with the appropriate observable.
                 */
                IObservingObject pkey = observer;
                IObservable c = new KIMObservable(observer);
                IState ret = new State(_state, c, context.getScale(), dataset, hasTemporalActions(), context);
                String label = c.toString();

                ret.getMetadata().put(NS.DISPLAY_LABEL_PROPERTY, label);

                IMetadata md = provenance.collectMetadata(obs);
                ret.getMetadata().merge(md, false);

                _states.add(ret);
            }

            return _states;
        }

        
        /*
         * TODO override createStates() to create a simple state without storage.
         */
    }

    public ConstantDataSource(Object state) {
        _state = state;
    }

    @Override
    public IActuator getAccessor(IScale context, IObserver observer, IMonitor monitor)
            throws ThinklabException {
        return new ConstantActuator(new ArrayList<IAction>(), monitor);
    }

    @Override
    public IMetadata getMetadata() {
        return _metadata;
    }

    @Override
    public IScale getCoverage() {
        return new Scale();
    }

    @Override
    public String toString() {
        return _state.toString();
    }

    @Override
    public boolean isAvailable() {
        return true;
    }

}
