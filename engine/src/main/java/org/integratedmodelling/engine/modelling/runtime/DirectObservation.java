/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.engine.modelling.runtime;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

import org.integratedmodelling.api.knowledge.IKnowledge;
import org.integratedmodelling.api.knowledge.IProperty;
import org.integratedmodelling.api.metadata.IMetadata;
import org.integratedmodelling.api.modelling.IAction;
import org.integratedmodelling.api.modelling.IDirectActiveObservation;
import org.integratedmodelling.api.modelling.IDirectObservation;
import org.integratedmodelling.api.modelling.IModel;
import org.integratedmodelling.api.modelling.INamespace;
import org.integratedmodelling.api.modelling.IObservable;
import org.integratedmodelling.api.modelling.IProcess;
import org.integratedmodelling.api.modelling.IScale;
import org.integratedmodelling.api.modelling.IState;
import org.integratedmodelling.api.modelling.agents.IAgentState;
import org.integratedmodelling.api.modelling.agents.ICollision;
import org.integratedmodelling.api.modelling.agents.IObservationController;
import org.integratedmodelling.api.modelling.agents.IObservationGraphNode;
import org.integratedmodelling.api.modelling.resolution.IResolutionContext;
import org.integratedmodelling.api.modelling.scheduling.ITransition;
import org.integratedmodelling.api.monitoring.IMonitor;
import org.integratedmodelling.api.runtime.IContext;
import org.integratedmodelling.api.time.ITemporalExtent;
import org.integratedmodelling.api.time.ITimePeriod;
import org.integratedmodelling.collections.Pair;
import org.integratedmodelling.common.configuration.KLAB;
import org.integratedmodelling.common.kim.KIMAction;
import org.integratedmodelling.common.metadata.Metadata;
import org.integratedmodelling.common.model.runtime.Transition;
import org.integratedmodelling.common.states.States;
import org.integratedmodelling.common.utils.NameGenerator;
import org.integratedmodelling.common.vocabulary.NS;
import org.integratedmodelling.common.vocabulary.Observable;
import org.integratedmodelling.engine.introspection.CallTracer;
import org.integratedmodelling.engine.modelling.AgentState;
import org.integratedmodelling.engine.modelling.TemporalCausalGraph;
import org.integratedmodelling.engine.modelling.resolver.ResolutionContext;
import org.integratedmodelling.exceptions.ThinklabException;
import org.integratedmodelling.exceptions.ThinklabValidationException;

/**
 * Holds the methods that interface each directly observed object (subject/agent, process, event) with
 * the multiagent/multiscale contextualizer.
 * 
 * Moved all methods from Subject to here so that Process and Event can react to transitions.
 * 
 * @author Ferd
 * @author Luke
 *
 */
public abstract class DirectObservation implements IDirectActiveObservation {

    protected boolean                                                              _initialized               = false;
    // index value of _scale.getTime().getExtent(i) corresponding to agent's temporal location
    protected int                                                                  currentTemporalExtentIndex = 0;
    protected ITimePeriod                                                          currentTimePeriodCache     = null;
    protected TemporalCausalGraph<IDirectActiveObservation, IObservationGraphNode> causalGraph;

    // set from Context at each new resolution.
    protected IContext                           _context    = null;
    protected long                               _taskId     = -1;
    protected INamespace                         namespace;
    protected IObservable                        observable;
    protected IScale                             scale;
    protected String                             name;
    protected IMonitor                           monitor;
    protected IMetadata                          metadata    = new Metadata();
    protected IResolutionContext                 _resolutionContext;
    protected ITransition                        lastTransitionSeen;
    protected ArrayList<Pair<IProperty, IState>> _states     = new ArrayList<>();
    /*
     * internal ID for observation paths
     */
    protected String                             _internalID = NameGenerator
            .shortUUID();

    private List<DirectObservation>      _directObservations = new ArrayList<>();
    IDirectObservation                   _contextSubject     = null;
    ArrayList<Pair<IProperty, IProcess>> _processes          = new ArrayList<>();

    // we store compiled versions of any actions coming from the model.
    List<IAction> initActions = new ArrayList<>();
    List<IAction> timeActions = new ArrayList<>();
    boolean       actionsStored;

    protected DirectObservation(IObservable observable, IScale scale, INamespace namespace, String name,
            IMonitor monitor) {
        this.scale = scale;
        this.observable = observable;
        this.namespace = namespace;
        this.name = name;
        this.monitor = monitor;
    }

    @Override
    public IModel getModel() {
        return _resolutionContext == null ? null : _resolutionContext.getModel();
    }

    public void setResolutionContext(ResolutionContext resolutionContext) {
        _resolutionContext = resolutionContext;
    }

    public IResolutionContext getResolutionContext() {
        return _resolutionContext;
    }

    @Override
    public IMetadata getMetadata() {
        return metadata;
    }

    public void prepareForTransition(ITransition transition) {

        if (lastTransitionSeen == null || transition.getTimeIndex() > lastTransitionSeen.getTimeIndex()) {
            lastTransitionSeen = transition;
            for (IState state : getStates()) {
                state.getStorage().flush(transition);
                States.setChanged(state, false);
            }
        }
    }

    public void addProcess(IProcess process) throws ThinklabException {
        IProperty p = NS.getPropertyFor(process, getObservable(), getNamespace());
        _processes.add(new Pair<IProperty, IProcess>(p, process));
        addDirectObservation((DirectObservation) process);
    }

    /**
     * Re-evaluate states given the new temporal location of this agent. The agent has access to the current
     * states during execution of this method because they have not yet changed. The agent is expected to
     * update the state values and set them in the result.
     *
     * this method is intended to be overridden by subclasses as appropriate.
     * @param timePeriod
     *
     * @return
     */
    @Override
    public ITransition reEvaluateStates(ITimePeriod timePeriod) {
        // TODO either call resolve() somehow, or mimic its behavior
        CallTracer.indent("reEvaluateStates()", this, timePeriod);
        Map<IProperty, IState> currentStates = getObjectStateCopy();

        IAgentState agentState = new AgentState(this, timePeriod, currentStates);

        // TODO this is the suburban version: nothing ever happens.
        ITransition noTransition = new Transition(getScale(), agentState, true);

        CallTracer.unIndent();
        return noTransition;
    }

    @Override
    public ICollision detectCollision(IObservationGraphNode myAgentState, IObservationGraphNode otherAgentState) {
        // TODO not going to implement this. Collision detection is extremely difficult.
        return null;
    }

    @Override
    public boolean doesThisCollisionAffectYou(IAgentState agentState, ICollision collision) {
        // TODO not going to implement this. Collision detection is extremely difficult.
        return false;
    }

    @Override
    public IState getState(IObservable obs, Object... data) throws ThinklabException {

        for (Pair<IProperty, IState> s : _states) {
            /*
             * Only compare observable and observation: the inherent type has been set to equal the subject's, so
             * it may differ without incompatibility.
             * FIXME - should be OK if the observable doesn't have it but not OK if it does and it's not the 
             * same type (using is()).
             */
            if (((Observable) obs).equalsWithoutInherency(s.getSecond().getObservable())) {
                return s.getSecond();
            }
        }
        if (!NS.isQuality(obs)) {
            throw new ThinklabValidationException("cannot create a state for a non-quality: "
                    + obs.getType());
        }

        if (obs.getInherentType() == null) {
            obs = ((Observable) obs).withInherentType(this.getObservable().getType());
        }

        IState ret = States.create(obs, this);
        // FIXME remove this - we do not need a data property until we need it.
        _states.add(new Pair<IProperty, IState>(NS
                .getPropertyFor(ret, this.getObservable(), getNamespace()), ret));
        return ret;
    }

    @Override
    public IState getStaticState(IObservable obs) throws ThinklabException {

        for (Pair<IProperty, IState> s : _states) {
            /*
             * Only compare observable and observation: the inherent type has been set to equal the subject's, so
             * it may differ without incompatibility.
             * FIXME - should be OK if the observable doesn't have it but not OK if it does and it's not the 
             * same type (using is()).
             */
            if (((Observable) obs).equalsWithoutInherency(s.getSecond().getObservable())) {
                return s.getSecond();
            }
        }
        if (!NS.isQuality(obs)) {
            throw new ThinklabValidationException("cannot create a state for a non-quality: "
                    + obs.getType());
        }

        if (obs.getInherentType() == null) {
            obs = ((Observable) obs).withInherentType(this.getObservable().getType());
        }

        IState ret = States.createStatic(obs, this);
        // FIXME remove this - we do not need a data property until we need it.
        _states.add(new Pair<IProperty, IState>(NS
                .getPropertyFor(ret, this.getObservable(), getNamespace()), ret));
        return ret;
    }

    public String getInternalID() {
        return _internalID;
    }

    public IDirectObservation getContextSubject() {
        return _contextSubject;
    }

    public void setContextSubject(IDirectObservation subject) {
        _contextSubject = subject;
        if (subject instanceof Subject) {
            _context = ((Subject) subject).getContext();
        }
    }

    /**
     * Return all direct observations in order of dependency (stated order
     * in model).
     * 
     * @return
     */
    public List<DirectObservation> getDirectObservations() {
        return _directObservations;
    }

    /*
     * called to add a direct observation to the observation graph. The order
     * of addition will determine the order of contextualization.
     */
    protected void addDirectObservation(DirectObservation obs) {
        _directObservations.add(obs);
        /*
         * TODO sort them: processes first (in order of declaration), then subjects and relationships in order of declarations.
         */
    }

    /**
     * Recursively add the subject and all its dependencies to the observation graph (which is contained in
     * the controller parameter). Keep a list of what's been added already, to speed up performance, avoid
     * infinite recursion, and avoid duplicating the initial observation tasks for the agents.
     * 
     * @param subjectObserver
     * @param simulationTimePeriod
     * @param controller
     * @param alreadyAdded
     */
    protected void addSubjectToObservationGraph(DirectObservation subject, ITimePeriod simulationTimePeriod, IObservationController controller, HashSet<IDirectObservation> alreadyAdded) {
        CallTracer.indent("addSubjectToObservationGraph()", this, subject, simulationTimePeriod);

        if (alreadyAdded == null) {
            alreadyAdded = new HashSet<IDirectObservation>();
        }
        if (alreadyAdded.contains(subject)) {
            CallTracer.msg("subject has already been added! returning without doing any work.");
        } else {

            alreadyAdded.add(subject);

            // recursively add each other subject to the controller's causal graph
            // NOTE: do this first so that the observation queue causes dependencies to be observed before
            // dependents.
            // This would work either way, but this way doesn't cause sleep/wait at the start of an
            // observation.
            CallTracer.msg("adding subject's children...");

            for (DirectObservation child : subject.getDirectObservations()) {
                addSubjectToObservationGraph(child, simulationTimePeriod, controller, alreadyAdded);
            }

            CallTracer.msg("adding subject...");
            ITimePeriod agentCreationTimePeriod = simulationTimePeriod;
            ITemporalExtent subjectTime = subject.getScale().getTime();
            if (subjectTime != null) {
                // agent has its own temporal scale, so don't inherit the default. Start with extent 0.
                agentCreationTimePeriod = subjectTime.getExtent(0).collapse();
            }
            AgentState initialState = new AgentState(subject, agentCreationTimePeriod, ((IDirectActiveObservation) subject)
                    .getObjectStateCopy());
            controller.createAgent(subject, initialState, agentCreationTimePeriod, null, null, true);
        }
        CallTracer.unIndent();
    }

    /**
     * Default implementation for Subjects (non-agents) without any explicit view of time. This means that
     * they can be re-evaluated over time by repeating the same observations, but they do not perform any
     * logic or application of rules based on states or state transitions.
     * @throws ThinklabException 
     */
    @Override
    public ITransition performTemporalTransitionFromCurrentState() throws ThinklabException {
        ITransition result;
        CallTracer.indent("performTemporalTransitionFromCurrentState()", this);

        // set the clock forward
        if (moveToNextTimePeriod() == null) {
            // we went beyond the agent's last temporal extent, so the agent dies
            result = new Transition(getScale(), null, false);
        } else {
            // TODO re-evaluate states by repeating the observations.
            result = reEvaluateStates(getCurrentTimePeriod());
        }

        CallTracer.unIndent();
        return result;
    }

    protected ITimePeriod moveToNextTimePeriod() {

        if (getCurrentTimePeriod() == null) {
            // if we're already beyond the valid index numbers, then don't increment any more.
            return null;
        }

        // set the clock forward & invalidate the cache
        currentTemporalExtentIndex++;
        currentTimePeriodCache = null;

        // reload the cache and validate that we're still within the subject's temporal scale
        ITimePeriod result = getCurrentTimePeriod();
        if (result == null) {
            // TODO either extend the scale or the agent must die
            // currently, the agent dies by default
        }

        return result;
    }

    /**
     * get the TemporalExtent (which is also a TimePeriod) of the agent's current temporal location. Will
     * return null if the index has gone beyond the agent's temporal scale.
     * 
     * @return
     */
    protected ITimePeriod getCurrentTimePeriod() {
        if (currentTimePeriodCache == null) {
            ITemporalExtent time = getScale().getTime();
            if (time != null) {
                // FIXME check the -1 - not sure yet
                if (currentTemporalExtentIndex >= (time.getMultiplicity()) - 1)
                    return null;
                ITemporalExtent extent = time.getExtent(currentTemporalExtentIndex);
                currentTimePeriodCache = extent.collapse();
            }
        }
        return currentTimePeriodCache;
    }

    public TemporalCausalGraph<IDirectActiveObservation, IObservationGraphNode> getCausalGraph() {
        return causalGraph;
    }

    public boolean isInitialized() {
        return _initialized;
    }

    @Override
    public void setMonitor(IMonitor monitor) {
        this.monitor = monitor;
    }

    @Override
    public IMonitor getMonitor() {
        return monitor;
    }

    @Override
    public INamespace getNamespace() {
        return namespace;
    }

    @Override
    public IObservable getObservable() {
        return observable;
    }

    @Override
    public IScale getScale() {
        return scale;
    }

    @Override
    public IKnowledge getType() {
        return getObservable().getType();
    }

    private void storeActions() {
        if (!actionsStored) {
            actionsStored = true;
            if (getModel() != null) {
                for (IAction a : getModel().getActions()) {
                    if (a.getDomain().isEmpty()) {
                        initActions.add(((KIMAction) a).compileFor(this, getModel()));
                    } else if (a.getDomain().contains(KLAB.c(NS.TIME_DOMAIN))) {
                        timeActions.add(((KIMAction) a).compileFor(this, getModel()));
                    }
                }
            }
        }
    }

    public void execActions(ITransition transition) throws ThinklabException {

        if (!actionsStored) {
            storeActions();
        }

        if (transition == null) {
            for (IAction a : initActions) {
                ((KIMAction) a).execute(_contextSubject, this, transition, monitor);
            }
        } else {
            for (IAction a : timeActions) {
                ((KIMAction) a).execute(_contextSubject, this, transition, monitor);
            }
        }
    }
}
