/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.engine.modelling;

import org.integratedmodelling.api.modelling.IDirectActiveObservation;
import org.integratedmodelling.api.modelling.agents.ICollision;
import org.integratedmodelling.api.modelling.agents.IObservationController;
import org.integratedmodelling.api.modelling.agents.IObservationGraphNode;
import org.integratedmodelling.api.modelling.scheduling.ITransition;
import org.integratedmodelling.api.time.ITimeInstant;
import org.integratedmodelling.engine.introspection.CallTracer;
import org.integratedmodelling.exceptions.ThinklabException;

public class ObservationTaskCollisionDetection extends ObservationTask {
    // default generated ID
    private static final long           serialVersionUID = 1L;
    private final IObservationGraphNode node1;
    private final IObservationGraphNode node2;

    public ObservationTaskCollisionDetection(ITimeInstant observationTime, IObservationGraphNode node1,
            IObservationGraphNode node2, IObservationGraphNode parentNode, IObservationController controller) {
        // null SubjectObserver because it's actually not used during collision detection (Subjects do it
        // directly)
        super(null, observationTime, parentNode, controller);
        this.node1 = node1;
        this.node2 = node2;
    }

    /**
     * NOTE: overriding run() also prevents NPE in superclass (where subjectObserver == null)
     */
    @Override
    public ITransition run() throws ThinklabException {
        CallTracer.indent("run()", this, node1, node2);
        // Necessary steps:
        // get the state distribution function for each agent which is valid @ observationTime
        // ask each agent to determine whether collision will happen
        ICollision collision1 = ((IDirectActiveObservation) (node1.getAgentState().getSubject()))
                .detectCollision(
                        node1, node2);
        ICollision collision2 = ((IDirectActiveObservation) (node1.getAgentState().getSubject()))
                .detectCollision(
                        node2, node1);
        // take the first-occurring collision (if any), because the earlier collision might influence the
        // subsequent agent state(s)
        // and therefore the later collision would have to be re-computed anyway.
        ICollision collision;
        if (collision1 == null) {
            collision = collision2;
        } else if (collision2 == null) {
            collision = collision1;
        } else if (collision1.getCollisionTime().compareTo(collision1.getCollisionTime()) < 0) {
            // collision 1 happens earlier
            collision = collision1;
        } else {
            // collision 2 happens earlier
            collision = collision2;
        }

        if (collision != null) {
            controller.collide(node1, node2, collision);
        }

        CallTracer.unIndent();
        return null;
    }

    /**
     * Override this to prevent NPE in superclass (where subjectObserver == null)
     */
    @Override
    public String toString() {
        return getClass().getSimpleName() + " for " + node1 + " and " + node2 + " at " + observationTime;
    }
}
