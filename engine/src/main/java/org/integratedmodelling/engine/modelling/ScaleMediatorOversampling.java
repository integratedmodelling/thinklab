/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.engine.modelling;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Random;

import org.integratedmodelling.api.knowledge.IProperty;
import org.integratedmodelling.api.metadata.IMetadata;
import org.integratedmodelling.api.modelling.IDirectActiveObservation;
import org.integratedmodelling.api.modelling.IScale;
import org.integratedmodelling.api.modelling.IState;
import org.integratedmodelling.api.modelling.agents.IObservationGraphNode;
import org.integratedmodelling.api.space.ISpatialExtent;
import org.integratedmodelling.api.time.ITimeInstant;
import org.integratedmodelling.engine.modelling.ScaleMediatorMetadata.ScaleMediatorTag;
import org.integratedmodelling.exceptions.ThinklabResourceNotFoundException;

import com.infomatiq.jsi.rtree.RTree;

import gnu.trove.procedure.TIntProcedure;

/**
 * Linear translation can handle any arbitrary number of dimensions. Translations are done one dimension at a
 * time.
 * 
 * @author luke
 * 
 */
public class ScaleMediatorOversampling extends ScaleMediator {

    protected static final IMetadata metadata                = new ScaleMediatorMetadata(ScaleMediatorTag.linear, ScaleMediatorTag.oneDimensional);

    private static final int         RANDOM_SAMPLES_PER_CELL = 10;
    private static final Random      random                  = new Random(System.currentTimeMillis());

    public ScaleMediatorOversampling(IDirectActiveObservation agent, ObservationController controller)
            throws ThinklabResourceNotFoundException {
        super(agent, controller);
    }

    class FeatureFinder {
        RTree                           tree;
        int                             idx;
        private com.infomatiq.jsi.Point point;

        FeatureFinder(RTree tree, double[] xy) {
            this.tree = tree;
            this.point = new com.infomatiq.jsi.Point((float) xy[0], (float) xy[1]);
        }

        int find() {

            tree.nearest(point, new TIntProcedure() {

                @Override
                public boolean execute(int arg0) {
                    idx = arg0;
                    return true;
                }
            }, Float.POSITIVE_INFINITY);

            return idx;
        }
    }

    @Override
    protected Map<IProperty, IState> generateTargetScale(SubjectObservation observation, IScale targetScale) {
        Map<IProperty, IState> result = new HashMap<IProperty, IState>();

        Map<IProperty, IState> observedStates = observation.agentStateNode.getAgentState().getStates();
        ISpatialExtent targetSpace = targetScale.getSpace();

        // find the overlapping observedStates for each extent in the target scale
        // and populate targetState with each Monte-Carlo-computed value
        IState observedState;
        ISpatialExtent targetExtent;
        IProperty observedProperty;
        for (Entry<IProperty, IState> observedPropertyState : observedStates.entrySet()) {
            observedProperty = observedPropertyState.getKey();
            observedState = observedPropertyState.getValue();
            RTree index = observation.getSpatialIndex(observedProperty);

            // TODO remove cast (see comments in StateFactory.getEmptyClone())
            IState targetState = StateFactory.getEmptyClone(observedState, targetScale);

            int nearestRectangle;
            float randomX, randomY;

            for (int i = 0; i < targetSpace.getValueCount(); i++) {
                targetExtent = targetSpace.getExtent(i);

                /*
                 * TODO add polygon intelligence. This just uses the rectangular bounding box, and finds the
                 * "nearest" (which I'm sure means "nearest center"). This is fine for a Monte Carlo algorithm
                 * in general, but may run into trouble with strange boundary shapes, such as crescents or
                 * wholly surrounded regions.
                 */
                double sum = 0;
                for (int j = 0; j < RANDOM_SAMPLES_PER_CELL; j++) {
                    randomX = random.nextFloat() * (float) (targetExtent.getMaxX() - targetExtent.getMinX())
                            + (float) targetExtent.getMinX();
                    randomY = random.nextFloat() * (float) (targetExtent.getMaxY() - targetExtent.getMinY())
                            + (float) targetExtent.getMinY();
//                    Point point = new Point(randomX, randomY);
                    nearestRectangle = new FeatureFinder(index, new double[]{randomX, randomY}).find();
                    // TODO how to enforce that the object state holds scalar values?
                    sum += (Double) observedState.getValue(nearestRectangle);
                }

                Double monteCarloValue = sum / RANDOM_SAMPLES_PER_CELL;
                targetState.getStorage().set(i, monteCarloValue); // won't work with non-scalar values
                                                                  // (see
                                                                  // comments
                // above and @ getEmptyClone())
            }

            result.put(observedProperty, targetState);
        }

        observation.outputStates.put(targetScale, result);
        return result;
    }

    @Override
    public void invalidate(ITimeInstant interruptTime) {
        // TODO Auto-generated method stub

    }

    @Override
    public void notify(IObservationGraphNode node) {
        // TODO Auto-generated method stub

    }
}
