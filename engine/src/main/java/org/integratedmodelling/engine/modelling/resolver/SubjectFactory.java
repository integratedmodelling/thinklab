/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.engine.modelling.resolver;

import java.lang.reflect.Constructor;
import java.util.ArrayList;
import java.util.List;

import org.integratedmodelling.api.knowledge.IProperty;
import org.integratedmodelling.api.metadata.IMetadata;
import org.integratedmodelling.api.modelling.IModel;
import org.integratedmodelling.api.modelling.INamespace;
import org.integratedmodelling.api.modelling.IObjectSource;
import org.integratedmodelling.api.modelling.IObservable;
import org.integratedmodelling.api.modelling.IObserver;
import org.integratedmodelling.api.modelling.IReifiableObject;
import org.integratedmodelling.api.modelling.IScale;
import org.integratedmodelling.api.modelling.ISubject;
import org.integratedmodelling.api.monitoring.IMonitor;
import org.integratedmodelling.collections.Pair;
import org.integratedmodelling.common.configuration.KLAB;
import org.integratedmodelling.common.states.State;
import org.integratedmodelling.common.utils.CamelCase;
import org.integratedmodelling.engine.modelling.runtime.Subject;
import org.integratedmodelling.exceptions.ThinklabException;
import org.integratedmodelling.exceptions.ThinklabInternalErrorException;
import org.integratedmodelling.exceptions.ThinklabRuntimeException;

/**
 * Reimplemented to use the annotation-driven mechanism used for other things in Thinklab. Just tag
 * the subject class to associate with a concept with the annotation @SubjectType(NS.CONCEPT_TO_ASSOCIATE).
 * This will register the class with the Model Manager and the class will be associated intelligently
 * (meaning, the reasoner will be used so derived concept will be associated to the class that is 
 * semantically closest).
 *
 * TODO this could (should?) use the default ISemanticObject mechanism in Thinklab (works exactly the same and adds
 * bi-directional extraction/injection of formal semantics from/to Java objects) but this way it's 
 * simpler and likely works out of the box.
 * 
 * @author Luke
 * @author Ferd
 */
public class SubjectFactory {

    /**
     * The main entry point.
     *
     * @param observable
     * @param namespace
     * @param scale
     * @param newSubjectId
     * @return
     * @throws ThinklabException
     */
    public static Subject getSubjectByMetadata(IObservable observable, INamespace namespace, IScale scale, String newSubjectId, IMonitor monitor)
            throws ThinklabException {

        Subject result;
        Constructor<?> constructor;

        Class<?> agentClass = KLAB.MMANAGER.getSubjectClass(observable.getTypeAsConcept());
        if (agentClass == null) {
            agentClass = Subject.class;
        }

        try {
            constructor = agentClass.getConstructor(IObservable.class, IScale.class, /* Object.class, */
            INamespace.class, String.class, IMonitor.class);
        } catch (Exception e) {
            throw new ThinklabInternalErrorException("No viable constructor found for Java class '"
                    + agentClass.getCanonicalName() + "' for agent type '" + observable.getFormalName()
                    + "'");
        }

        try {
            result = (Subject) constructor.newInstance(observable, scale, /* null,*/
            namespace, newSubjectId, monitor);
        } catch (Exception e) {
            throw new ThinklabRuntimeException("Unable to generate new instance of Java class '"
                    + agentClass.getCanonicalName() + "' for agent type '" + observable.getFormalName()
                    + "'");
        }

        return result;

    }

    /**
     * Create all subjects from an object source in a given scale.
     * 
     * @param model
     * @param scale
     * @return
     * @throws ThinklabException
     */
    public static List<ISubject> createSubjects(IModel model, IObjectSource objectSource, IScale scale, ISubject contextSubject, IMonitor monitor)
            throws ThinklabException {
        ArrayList<ISubject> ret = new ArrayList<ISubject>();

        if (objectSource != null) {
            int i = 1;
            for (IReifiableObject o : objectSource.getObjects(scale)) {
                String name = CamelCase.toLowerCase(model.getObservable().getType().getLocalName(), '-')
                        + "-" + i++;
                IObservable observable = model.getObservable();
                IScale oScale = o.getScale(scale);
                if (!oScale.isConsistent()) {
                    monitor.warn("inconsistent or empty scale for new object " + name + " (id = " + o.getId()
                            + "): ignored");
                    continue;
                }

                Subject subject = getSubjectByMetadata(observable, model
                        .getNamespace(), oScale, name, monitor);

                for (Pair<String, IObserver> at : model.getAttributeObservers()) {
                    Object att = o.getAttributeValue(at.getFirst(), at.getSecond());
                    if (att != null) {
                        // FIXME see what to do with dynamic states. ALSO should enable expressions and 'on
                        // definition' ....
                        subject.addState(new State(att, at.getSecond().getObservable(), oScale, false));
                    }
                }

                for (Pair<String, IProperty> at : model.getAttributeMetadata()) {
                    if (at.getSecond().toString().equals(IMetadata.IM_NAME)) {
                        Object att = o.getAttributeValue(at.getFirst(), null);
                        if (att != null) {
                            subject.setId(att.toString());
                        }
                    } else {
                        subject.getMetadata().put(at.getSecond().toString(), at.getFirst());
                    }
                }

                subject.setContextSubject(contextSubject);

                /*
                 * TODO model may have actions that we need to call and schedule for transitions
                 */
                ret.add(subject);
            }
        }

        return ret;
    }
}
