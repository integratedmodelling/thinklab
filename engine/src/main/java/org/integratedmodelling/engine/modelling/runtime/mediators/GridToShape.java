/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.engine.modelling.runtime.mediators;

import java.util.ArrayList;
import java.util.List;

import org.integratedmodelling.api.knowledge.IConcept;
import org.integratedmodelling.api.modelling.IExtent;
import org.integratedmodelling.api.modelling.IObservable;
import org.integratedmodelling.api.modelling.IScale;
import org.integratedmodelling.api.modelling.IScale.Locator;
import org.integratedmodelling.api.modelling.IState;
import org.integratedmodelling.api.space.IGrid;
import org.integratedmodelling.api.space.IGrid.Cell;
import org.integratedmodelling.api.space.IShape;
import org.integratedmodelling.api.space.ISpatialExtent;
import org.integratedmodelling.collections.Pair;
import org.integratedmodelling.common.space.IGeometricShape;
import org.integratedmodelling.common.space.SpaceLocator;
import org.integratedmodelling.exceptions.ThinklabException;
import org.integratedmodelling.exceptions.ThinklabRuntimeException;

public class GridToShape extends AbstractMediator implements IState.Mediator {

    private Iterable<Pair<Cell, Double>> cellCoverage;
    Aggregation                          aggregation;
    double                               total = 0.0;

    public GridToShape(IObservable observable, IExtent from, IExtent to, IConcept trait) {

        super(observable.getObserver(), trait);

        aggregation = MediationOperations.getAggregator(observable);
        IGrid g1 = ((ISpatialExtent) from).getGrid();
        IShape sh = ((ISpatialExtent) to).getShape();
        try {
            this.cellCoverage = MediationOperations
                    .getCoveredCells(g1, (IGeometricShape) sh, aggregation != Aggregation.SUM);
        } catch (ThinklabException e) {
            throw new ThinklabRuntimeException(e);
        }

        if (aggregation == Aggregation.SUM) {
            /*
             * compute stats for faster propagation
             */
            for (Pair<Cell, Double> tc : cellCoverage) {
                total += tc.getSecond();
            }
        }
    }

    @Override
    public Object mediateTo(Object value, int index) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public Aggregation getAggregation() {
        return aggregation;
    }

    /*
     * in this one, the index is always 0
     * (non-Javadoc)
     * @see org.integratedmodelling.api.modelling.IState.Mediator#mediateFrom(int)
     */
    @Override
    public Object mediateFrom(IState originalState, IScale.Locator... otherLocators) {
        List<Pair<Integer, Double>> ret = new ArrayList<>();
        for (Pair<Cell, Double> zio : cellCoverage) {

            /*
             * TODO must use space locators and whatever else
             */
            ret.add(new Pair<Integer, Double>(zio.getFirst().getOffsetInGrid(), zio.getSecond()));
        }
        return ret;
    }

    @Override
    public List<Locator> getLocators(int index) {

        List<Locator> ret = new ArrayList<>();
        for (Pair<Cell, Double> zio : cellCoverage) {
            SpaceLocator loc = SpaceLocator.get(zio.getFirst().getX(), zio.getFirst().getY());
            loc.setWeight(zio.getSecond());
            ret.add(loc);
        }
        return ret;
    }

}
