/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.engine.modelling.kbox;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.Map;

import org.integratedmodelling.api.metadata.IModelMetadata;
import org.integratedmodelling.api.modelling.IModel;
import org.integratedmodelling.api.modelling.IModelObject;
import org.integratedmodelling.api.modelling.INamespace;
import org.integratedmodelling.api.modelling.IObserver;
import org.integratedmodelling.api.modelling.resolution.IModelPrioritizer;
import org.integratedmodelling.api.monitoring.IMonitor;
import org.integratedmodelling.api.monitoring.INotification;
import org.integratedmodelling.api.network.INode;
import org.integratedmodelling.api.project.IProject;
import org.integratedmodelling.collections.ImmutableList;
import org.integratedmodelling.common.configuration.KLAB;
import org.integratedmodelling.common.kim.KIMModel;
import org.integratedmodelling.common.kim.KIMObserver;
import org.integratedmodelling.common.utils.StringUtils;
import org.integratedmodelling.engine.introspection.DataRecorder;
import org.integratedmodelling.exceptions.ThinklabRuntimeException;

/**
 * Result of a query. The get(n) method returns a model in order of insertion; the iterator returns
 * RANKED models. Model retrieval is lazy and the collection only holds a small amount of ranked metadata 
 * until a model is wanted. The model retriever should (eventually) be able to integrate models from 
 * any server, local or remote, creating the final models based on the server ID in model data.
 * 
 * @author ferdinando.villa
 *
 */
public class ModelQueryResult extends ImmutableList<IModel> {

    IModelPrioritizer<IModelMetadata> _comparator;
    ArrayList<ModelData>              _modelData = new ArrayList<ModelData>();
    boolean                           _sorted    = false;
    IMonitor                          _monitor;

    public class It implements Iterator<IModel> {

        Iterator<ModelData> _it;

        It() {
            if (!_sorted) {
                Collections.sort(_modelData, _comparator);
                _sorted = true;

                // add ranking metadata to provenance. TODO we may want to condition this to an option.
                addMetadata();

                // if debugging, print out the ranked alternatives.
                if (KLAB.CONFIG.getNotificationLevel() == INotification.DEBUG) {
                    if (_modelData.size() > 0) {
                        DataRecorder.debug("---- SCORES ------");
                        int n = 1;
                        for (ModelData md : _modelData) {
                            DataRecorder.debug(describeRanks(md, 2, n++));
                        }
                        DataRecorder.debug("------------------");
                    } else {
                        DataRecorder.debug("No results");
                    }
                }
            }
            _it = _modelData.iterator();
        }

        @Override
        public boolean hasNext() {
            return _it.hasNext();
        }

        @Override
        public IModel next() {
            return getModel(_it.next());
        }

        @Override
        public void remove() {
            throw new ThinklabRuntimeException("remove() in ObservationQueryResult iterator is unsupported");
        }
    }

    private IModel getModel(ModelData md) {

        IModelObject ret = null;

        if (md == null)
            return null;

        if (md.serverId != null && KLAB.MMANAGER.findModelObject(md.name) == null) {
            // load remote projects if necessary. After the call, the object should be available locally.
            IProject remoteProject = KLAB.PMANAGER.getProject(md.projectId);
            if (remoteProject == null) {
                INode node = KLAB.NETWORK.getNode(md.serverId);
                if (node != null) {
                    try {
                        remoteProject = node.importProject(md.projectId);
                        KLAB.PMANAGER.loadProject(remoteProject.getId(), KLAB.MFACTORY.getRootParsingContext());
                    } catch (Exception e) {
                        throw new ThinklabRuntimeException(e);
                    }
                } else {
                    throw new ThinklabRuntimeException("node " + md.serverId
                            + " returned from remote query has become inaccessible");
                }
            }
        }

        INamespace ns = KLAB.MMANAGER.getNamespace(md.namespaceId);
        if (ns != null)
            ret = ns.getModelObject(md.id);
        if (!(ret instanceof IModel))
            return null;

        if (md.dereifyingAttribute != null) {
            IObserver obs = ((KIMModel) ret).getAttributeObserver(md.dereifyingAttribute);
            if (obs != null) {
                ret = new KIMModel((KIMObserver) obs, (KIMModel) ret, md.dereifyingAttribute, _monitor);
            } else {
                return null;
            }
        }

        return (IModel) ret;
    }

    public void addMetadata() {
        // TODO Auto-generated method stub
    }

    public String describeRanks(ModelData md, int indent, int n) {

        String ret = "";
        String filler = StringUtils.spaces(indent);

        ret += filler + StringUtils.rightPad(n + ".", 4) + md.name + " ["
                + (md.serverId == null ? "local" : md.serverId) + "]\n";
        Map<String, Object> ranks = _comparator.getRanks(md);
        for (String s : _comparator.listCriteria()) {
            ret += filler + "  " + StringUtils.rightPad(s, 25) + " "
                    + ranks.get(s) + "\n";
        }

        return ret;
    }

    public ModelQueryResult(IModelPrioritizer<IModelMetadata> prioritizer, IMonitor monitor) {
        _comparator = prioritizer;
        _monitor = monitor;
    }

    @Override
    public boolean contains(Object arg0) {
        throw new ThinklabRuntimeException("contains() in ObservationQueryResult is unsupported");
    }

    @Override
    public IModel get(int arg0) {
        return getModel(_modelData.get(arg0));
    }

    @Override
    public Iterator<IModel> iterator() {
        return new It();
    }

    @Override
    public int size() {
        return _modelData.size();
    }

    @Override
    public Object[] toArray() {
        throw new ThinklabRuntimeException("toArray() in ObservationQueryResult is unsupported");
    }

    @Override
    public <T> T[] toArray(T[] arg0) {
        throw new ThinklabRuntimeException("toArray() in ObservationQueryResult is unsupported");
    }

    public void addModelData(ModelData md) {
        _modelData.add(md);
        _sorted = false;
    }

}
