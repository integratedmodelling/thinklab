/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.engine.modelling.resolver;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

import org.integratedmodelling.api.knowledge.IConcept;
import org.integratedmodelling.api.modelling.IActiveSubject;
import org.integratedmodelling.api.modelling.ICoverage;
import org.integratedmodelling.api.modelling.IDirectObservation;
import org.integratedmodelling.api.modelling.IModel;
import org.integratedmodelling.api.modelling.IObservable;
import org.integratedmodelling.api.modelling.IProcess;
import org.integratedmodelling.api.modelling.IRelationship;
import org.integratedmodelling.api.modelling.IScale;
import org.integratedmodelling.api.modelling.ISubject;
import org.integratedmodelling.api.modelling.resolution.IProvenance;
import org.integratedmodelling.api.modelling.resolution.IResolutionContext;
import org.integratedmodelling.api.modelling.resolution.IResolutionStrategy;
import org.integratedmodelling.api.modelling.resolution.IWorkflow;
import org.integratedmodelling.api.modelling.runtime.IDirectActuator;
import org.integratedmodelling.api.modelling.runtime.IEventActuator;
import org.integratedmodelling.api.modelling.runtime.IProcessActuator;
import org.integratedmodelling.api.modelling.runtime.ISubjectActuator;
import org.integratedmodelling.api.modelling.scheduling.ITransition;
import org.integratedmodelling.common.configuration.KLAB;
import org.integratedmodelling.common.kim.KIMModel;
import org.integratedmodelling.common.vocabulary.NS;
import org.integratedmodelling.common.vocabulary.Observable;
import org.integratedmodelling.common.vocabulary.ObservationMetadata;
import org.integratedmodelling.engine.modelling.kbox.ObservationKbox;
import org.integratedmodelling.engine.modelling.runtime.DirectObservation;
import org.integratedmodelling.engine.modelling.runtime.Process;
import org.integratedmodelling.engine.modelling.runtime.Subject;
import org.integratedmodelling.exceptions.ThinklabException;

public class ResolutionStrategy implements IResolutionStrategy {

    List<Step>        _steps = new ArrayList<Step>();
    ICoverage         _coverage;
    DirectObservation _subject;
    ISubject          _csubject;
    List<String>      _scenarios;
    IScale            _scale;

    public static class ResolutionStep implements Step {

        IProvenance        provenance;
        IWorkflow          workflow;
        IModel             model;
        IDirectActuator<?> accessor;

        // if this isn't null, this step will be a database search for subjects of
        // this type in the scale/scenario combination of interest.
        IConcept subjectType;
        /*
         * if this isn't null, it's a trait that must be added to the main observable of
         * any direct observation created in this step. Only applicable for model and
         * subject steps.
         */
        IConcept interpretAs;

        public ResolutionStep(IProvenance provenance, IWorkflow workflow) {
            this.provenance = provenance;
            this.workflow = workflow;
        }

        public ResolutionStep(IModel model, IConcept interpretAs) {
            this.model = model;
            this.interpretAs = interpretAs;
        }

        public ResolutionStep(IConcept subjectType, IConcept interpretAs) {
            this.subjectType = subjectType;
            this.interpretAs = interpretAs;
        }

        @Override
        public IModel getModel() {
            return model;
        }

        @Override
        public IProvenance getProvenance() {
            return provenance;
        }

        @Override
        public IWorkflow getWorkflow() {
            return workflow;
        }

    }

    public ResolutionStrategy(IDirectObservation subject) {
        _subject = (DirectObservation) subject;
    }

    @Override
    public int getStepCount() {
        return _steps.size();
    }

    @Override
    public ICoverage getCoverage() {
        return _coverage;
    }

    public void addStep(IProvenance provenance, IWorkflow workflow) {
        _steps.add(new ResolutionStep(provenance, workflow));
    }

    public void addStep(IModel model, IConcept interpretAs) {
        _steps.add(new ResolutionStep(model, interpretAs));
    }

    public void addStep(IConcept subjectType, IConcept interpretAs) {
        _steps.add(new ResolutionStep(subjectType, interpretAs));
    }

    @Override
    public Iterator<Step> iterator() {
        return _steps.iterator();
    }

    @Override
    public Step getStep(int i) {
        return _steps.get(i);
    }

    @Override
    public boolean execute(ITransition transition, IResolutionContext context) throws ThinklabException {

        for (Step rs : _steps) {

            ResolutionStep s = (ResolutionStep) rs;

            /*
             * subject dependencies are only handled at initialization; afterwards it's the 
             * processes and subject's tasks to create them.
             */
            if (s.subjectType != null && transition == null) {

                int objs = 0;
                for (ObservationMetadata sg : ObservationKbox.get()
                        .query(Collections.singleton(s.subjectType), _scale, _scenarios, false)) {
                    // IDirectObserver sobs = SubjectObserver.sanitize();
                    // TODO sanitization should be unnecessary as the scale is built in the call
                    ISubject subj = (ISubject) KLAB.MFACTORY
                            .createSubject(sg.getSubjectObserver(), _subject, _subject.getMonitor());
                    ((DirectObservation) subj).setContextSubject(_subject);
                    if (((IActiveSubject) subj)
                            .initialize(_subject.getResolutionContext(), _subject.getMonitor())
                            .isEmpty()) {
                        _subject.getMonitor().warn("cannot resolve dependent subject "
                                + subj.getId());
                    } else {
                        ((Subject) subj).setObservable(((Observable) subj.getObservable())
                                .withInterpretingTrait(s.interpretAs));
                        // FIXME! need a relationship - must be found or specified. Then both the
                        // subject and the relationship must be resolved.
                        ((ISubject) _subject).getStructure()
                                .link((ISubject) _subject, subj, (IRelationship) null);
                    }
                    objs++;
                }

                KLAB.info(objs + " objects of type " + s.subjectType + " retrieved from network search");

            }

            // if there is a workflow, run it for the current transition.
            if (s.getWorkflow() != null) {
                if (!s.getWorkflow().run(transition))
                    return false;
            }

            if (s.getModel() != null) {

                // if there is an object source and we're initializing, create and initialize objects
                if (transition == null && ((KIMModel) s.model).hasObjectSource()) {

                    int n = 0;
                    for (ISubject subj : SubjectFactory
                            .createSubjects(s.model, s.model
                                    .getObjectSource(_subject
                                            .getMonitor()), _scale, (ISubject) _subject, _subject
                                                    .getMonitor())) {

                        // add it to establish relationship, then resolve it
                        // FIXME! need a relationship - must be found or specified. Then both the
                        // subject and the relationship must be resolved.
                        ((ISubject) _subject).getStructure()
                                .link((ISubject) _subject, subj, (IRelationship) null);
                        Resolver resolver = new Resolver(_subject.getResolutionContext());
                        if (resolver.resolve(subj, _subject.getMonitor()).isEmpty()) {
                            _subject.getMonitor().warn("cannot resolve dependent subject " + subj.getId());
                        } else {

                            ((Subject) subj).setObservable(((Observable) subj.getObservable())
                                    .withInterpretingTrait(s.interpretAs));
                            /*
                             * TODO schedule?
                             */
                            ((Subject) subj).execActions(transition);

                        }
                        n++;
                    }
                    _subject.getMonitor().info("created " + n + " " + s.model.getObservable().getType()
                            + " subjects", null);
                }

                if (transition == null && s.accessor == null) {
                    s.accessor = (IDirectActuator<?>) s.model.getActuator(_subject.getMonitor());
                }

                // if there is an accessor, run it
                if (s.accessor != null) {

                    if (transition == null) {

                        s.accessor.notifyModel(s.model);
                        for (IObservable observable : s.model.getObservables()) {
                            if (NS.isQuality(observable) || NS.isTrait(observable)) {
                                s.accessor.notifyExpectedOutput(observable, observable
                                        .getObserver(), observable.getFormalName());
                            }
                        }

                        /*
                         * if the model is a process model, create the process and insert it in the 
                         * observation schedule.
                         */
                        if (s.accessor instanceof IProcessActuator) {
                            /*
                             * TODO use subject factory and an annotation mechanism like for subjects to
                             * select the implementation.
                             */
                            IProcess process = new Process(s.model, _subject, (IProcessActuator) s.accessor, _subject
                                    .getMonitor());
                            process = ((IProcessActuator) s.accessor)
                                    .initialize(process, _subject, context, _subject.getMonitor());
                            ((Process) process).setObservable(((Observable) process.getObservable())
                                    .withInterpretingTrait(s.interpretAs));
                            _subject.addProcess(process);

                        } else if (s.accessor instanceof IEventActuator) {

                            /*
                             * 
                             * TODO event models should have a chance to create and process an event at 
                             * transitions only, initialization should not be passed anything.
                             */

                        } else {
                            _subject = (Subject) ((ISubjectActuator) s.accessor)
                                    .initialize((ISubject) _subject, _csubject, context, _subject
                                            .getMonitor());
                        }

                        /*
                         * if this was a subject model, the accessor may have created subjects,
                         * which we need to resolve in the main subject's context.
                         * 
                         * TODO this must resolve and initialize both subjects and relationships.
                         * CHECK is the subject being inserted in the observation graph?
                         * 
                         */
                        if (NS.isThing(s.model.getObservable())) {
                            for (ISubject subj : ((ISubject) _subject).getSubjects()) {

                                if (!((Subject) subj).isInitialized()) {
                                    ((Subject) subj).setContextSubject(_subject);
                                    if (((IActiveSubject) subj)
                                            .initialize(_subject.getResolutionContext(), _subject
                                                    .getMonitor())
                                            .isEmpty()) {
                                        _subject.getMonitor().warn("cannot resolve dependent subject "
                                                + subj.getId());
                                    } else {

                                        ((Subject) subj).setObservable(((Observable) subj.getObservable())
                                                .withInterpretingTrait(s.interpretAs));

                                        /*
                                         * TODO schedule?
                                         */

                                        ((Subject) subj).execActions(transition);

                                    }
                                }
                            }
                        }

                    }

                    /*
                     * run actions if any
                     */
                    ((Subject) _subject).execActions(transition);
                }
            }
        }

        return false;

    }

    public void merge(ResolutionStrategy strategy) {
        for (Step s : strategy) {
            _steps.add(s);
        }
    }
}
