/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
//package org.integratedmodelling.engine.modelling.functions;
//
//import java.util.Map;
//
//import org.integratedmodelling.api.knowledge.IConcept;
//import org.integratedmodelling.api.knowledge.IExpression;
//import org.integratedmodelling.api.lang.IList;
//import org.integratedmodelling.api.modelling.parsing.IFunctionCall;
//import org.integratedmodelling.api.project.IProject;
//import org.integratedmodelling.api.services.annotations.Prototype;
//import org.integratedmodelling.common.vocabulary.NS;
//import org.integratedmodelling.engine.Thinklab;
//import org.integratedmodelling.exceptions.ThinklabException;
//import org.integratedmodelling.exceptions.ThinklabValidationException;
//import org.integratedmodelling.list.PolyList;
//
///*
// * Creates the proper observable for the uncertainty of another. Provided to
// * avoid the need for a complicated instance definition when uncertainty is wanted
// * from a model.
// * 
// * FIXME this must return a MODEL with the appropriate observable.
// */
//@Prototype(
//        id = "uncertainty",
//        args = { IFunctionCall.DEFAULT_PARAMETER_NAME, Prototype.TEXT },
//        returnTypes = { NS.MODEL })
//public class UNCERTAINTY implements IExpression {
//
//    IProject _project;
//
//    @Override
//    public Object eval(Map<String, Object> parameters, IConcept... context) throws ThinklabException {
//
//        /*
//         * TODO make list and return the instantiated semantic object. Modeling
//         * ontology still needs the concepts.
//         * 
//         * The argument may be a concept or a list, which will be matched to
//         * another observable in the observable list of a model.
//         */
//        Object obs = parameters.get(IFunctionCall.DEFAULT_PARAMETER_NAME);
//
//        if (obs instanceof String) {
//            if (((String) obs).trim().startsWith("(")) {
//                obs = PolyList.parse(obs.toString());
//            } else {
//                obs = PolyList.list(Thinklab.c(obs.toString()));
//            }
//        } else if (obs instanceof IConcept) {
//            obs = PolyList.list(obs);
//        } else if (!(obs instanceof IList)) {
//            throw new ThinklabValidationException(
//                    "observable for uncertainty function unrecognized: should be a concept or an instance list");
//        }
//
//        return PolyList.list(NS.UNCERTAINTY, PolyList.list(NS.HAS_OBSERVABLE, obs));
//    }
//
//    @Override
//    public void setProjectContext(IProject project) {
//        /*
//         * not necessary for now, but we may want to condition the display of
//         * uncertainty to some project-level configuration.
//         */
//        _project = project;
//    }
//
// }
