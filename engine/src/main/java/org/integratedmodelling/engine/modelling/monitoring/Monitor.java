/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.engine.modelling.monitoring;

import java.util.Date;

import org.integratedmodelling.api.monitoring.IMonitor;
import org.integratedmodelling.api.monitoring.INotification;
import org.integratedmodelling.api.monitoring.IReport;
import org.integratedmodelling.api.monitoring.ITaskIntrospector;
import org.integratedmodelling.api.monitoring.Messages;
import org.integratedmodelling.api.runtime.ISession;
import org.integratedmodelling.api.runtime.ITask;
import org.integratedmodelling.common.configuration.KLAB;
import org.integratedmodelling.common.monitoring.Notification;
import org.integratedmodelling.common.utils.MiscUtilities;
import org.integratedmodelling.engine.geospace.extents.SpaceExtent;
import org.integratedmodelling.engine.geospace.utils.SpatialDisplay;
import org.integratedmodelling.engine.modelling.resolver.ProvenanceGraph;
import org.integratedmodelling.engine.modelling.resolver.Workflow;
import org.integratedmodelling.engine.runtime.Session;

/**
 * Helper to simplify handling of multiple listeners and two-way communication with 
 * running tasks.
 * 
 * @author Ferd
 *
 */
public class Monitor implements IMonitor {

    long              _taskId;
    ISession          _session;
    boolean           _stop         = false;
    boolean           _hasErrors    = false;
    ITaskIntrospector _introspector = new Introspector();
    IReport           _report;

    /*
     * for debugging: instantiated when setSpatialDisplay(Object) is called.
     */
    private SpatialDisplay spatialDisplay = null;

    class Introspector implements ITaskIntrospector {

        @Override
        public void informationAvailable(ITask task, Info type, Object content) {

            Date date = new Date();

            switch (type) {
            case DATAFLOW_GRAPH:
                send(new Notification(getTaskId(), date
                        .getTime(), Messages.CURRENT_WORKFLOW_GRAPH, getSession(), ((Workflow) content)
                                .visualize().toString()));
                break;
            case PROVENANCE_GRAPH:
                send(new Notification(getTaskId(), date
                        .getTime(), Messages.CURRENT_PROVENANCE_GRAPH, getSession(), ((ProvenanceGraph) content)
                                .visualize().toString()));
                break;
            }

        }
    }

    /*
     * TODO set in constructor to user-overriddable default.
     */
    int _logLevel = INotification.INFO;

    public Monitor(long taskId, ISession session) {
        _taskId = taskId;
        _session = session;
        if (_session != null) {
            ((Session) _session).registerTaskMonitor(taskId, this);
        }
    }

    public Monitor(long taskId, ISession session, IReport report) {
        this(taskId, session);
        _report = report;
    }

    public void setReport(IReport report) {
        _report = report;
    }

    @Override
    public long getTaskId() {
        return _taskId;
    }

    static Throwable getRootCause(Throwable t) {
        if (t.getCause() == null)
            return t;
        return getRootCause(t.getCause());
    }

    /*
     * TODO see if we want to float this to the interface.
     */
    @Override
    public ISession getSession() {
        return _session;
    }

    private String toText(Object o) {

        String s = null;
        if (o instanceof String) {
            s = (String) o;
        } else if (o instanceof Throwable) {
            o = getRootCause((Throwable) o);
            s = ((Throwable) o).getMessage() == null ? MiscUtilities.getExceptionPrintout((Throwable) o)
                    : ((Throwable) o).getMessage();
        } else if (o != null) {
            s = o.toString();
        }
        return s;
    }

    @Override
    public void warn(Object o) {
        if (_logLevel >= INotification.WARNING && _session != null)
            _session.notify(new Notification(_taskId, INotification.WARNING, toText(o), new Date()
                    .getTime(), _session
                            .getId()));
    }

    @Override
    public void info(Object o, String infoClass) {
        if (_logLevel >= INotification.INFO && _session != null)
            _session.notify(new Notification(_taskId, INotification.INFO, toText(o), new Date()
                    .getTime(), _session
                            .getId()));
    }

    @Override
    public void error(Object o) {
        if (_logLevel >= INotification.ERROR && _session != null)
            _session.notify(new Notification(_taskId, INotification.ERROR, toText(o), new Date()
                    .getTime(), _session
                            .getId()));
        _hasErrors = true;
    }

    // monitor for an arbitrary task - FIXME having the task id in the monitor is probably FUBAR design
    public void warn(int task, Object o) {
        KLAB.warn(o);
        if (_logLevel >= INotification.WARNING && _session != null)
            _session.notify(new Notification(task, INotification.WARNING, toText(o), new Date()
                    .getTime(), _session
                            .getId()));
    }

    public void info(int task, Object o, String infoClass) {
        if (_logLevel >= INotification.INFO && _session != null)
            _session.notify(new Notification(task, INotification.INFO, toText(o), new Date()
                    .getTime(), _session
                            .getId()));
    }

    public void error(int task, Object o) {
        KLAB.error(o);
        if (_logLevel >= INotification.ERROR && _session != null)
            _session.notify(new Notification(task, INotification.ERROR, toText(o), new Date()
                    .getTime(), _session
                            .getId()));
        _hasErrors = true;
    }

    @Override
    public void debug(Object o) {
        if (_logLevel >= INotification.DEBUG)
            _session.notify(new Notification(_taskId, INotification.DEBUG, toText(o), new Date()
                    .getTime(), _session
                            .getId()));
    }

    @Override
    public void stop(Object o) {
        _stop = true;
    }

    @Override
    public boolean isStopped() {
        return _stop;
    }

    /**
     * Use for debugging only: won't send anything to client but will create graphical displays
     * from the engine.
     * 
     * @param space
     */
    public void setSpatialDisplay(SpaceExtent space) {
        spatialDisplay = new SpatialDisplay(space);
    }

    public SpatialDisplay getSpatialDisplay() {
        return spatialDisplay;
    }

    @Override
    public void addProgress(int steps, String description) {
        // TODO Auto-generated method stub

    }

    @Override
    public void send(INotification notification) {

        ((Notification) notification).setTaskId(_taskId);

        if (_session != null)
            _session.notify(notification);

        if (notification.getMessage().equals("stop")) {
            _stop = true;
        } /* TODO etc */
    }

    @Override
    public void setLogLevel(int level) {
        _logLevel = level;
    }

    @Override
    public int getDefaultLogLevel() {
        // TODO tie to session/user preferences
        return INotification.INFO;
    }

    @Override
    public boolean hasErrors() {
        return _hasErrors;
    }

    @Override
    public void defineProgressSteps(int n) {
        // TODO Auto-generated method stub

    }

    @Override
    public ITaskIntrospector getTaskIntrospector() {
        return _introspector;
    }

    @Override
    public void report(String markdown, Object... attachments) {
        // TODO Auto-generated method stub
        if (_report != null) {
            if (attachments != null) {
                for (Object o : attachments) {
                    _report.addAttachment(o);
                }
            }
            _report.writeln(markdown);
        }
    }
}
