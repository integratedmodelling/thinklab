/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.engine.modelling.runtime;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import org.integratedmodelling.api.knowledge.IConcept;
import org.integratedmodelling.api.knowledge.IProperty;
import org.integratedmodelling.api.metadata.IMetadata;
import org.integratedmodelling.api.modelling.ICoverage;
import org.integratedmodelling.api.modelling.IDirectActiveObservation;
import org.integratedmodelling.api.modelling.IEvent;
import org.integratedmodelling.api.modelling.IExtent;
import org.integratedmodelling.api.modelling.INamespace;
import org.integratedmodelling.api.modelling.IObservable;
import org.integratedmodelling.api.modelling.IObservation;
import org.integratedmodelling.api.modelling.IProcess;
import org.integratedmodelling.api.modelling.IScale;
import org.integratedmodelling.api.modelling.IScale.Locator;
import org.integratedmodelling.api.modelling.IState;
import org.integratedmodelling.api.modelling.IStructure;
import org.integratedmodelling.api.modelling.ISubject;
import org.integratedmodelling.api.modelling.agents.IObservationController;
import org.integratedmodelling.api.modelling.agents.IObservationGraphNode;
import org.integratedmodelling.api.modelling.agents.IObservationWorker;
import org.integratedmodelling.api.modelling.resolution.IResolutionContext;
import org.integratedmodelling.api.modelling.resolution.IResolutionStrategy;
import org.integratedmodelling.api.modelling.scheduling.ITransition;
import org.integratedmodelling.api.modelling.storage.IDataset;
import org.integratedmodelling.api.monitoring.IMonitor;
import org.integratedmodelling.api.runtime.IContext;
import org.integratedmodelling.api.time.ITemporalExtent;
import org.integratedmodelling.api.time.ITimePeriod;
import org.integratedmodelling.collections.Pair;
import org.integratedmodelling.common.model.runtime.Structure;
import org.integratedmodelling.common.model.runtime.Transition;
import org.integratedmodelling.common.states.State;
import org.integratedmodelling.common.storage.MemoryDataset;
import org.integratedmodelling.common.storage.NetCDFdataset;
import org.integratedmodelling.common.utils.MapUtils;
import org.integratedmodelling.common.vocabulary.NS;
import org.integratedmodelling.engine.introspection.CallTracer;
import org.integratedmodelling.engine.modelling.ObservationController;
import org.integratedmodelling.engine.modelling.ObservationWorker;
import org.integratedmodelling.engine.modelling.TemporalCausalGraph;
import org.integratedmodelling.engine.modelling.interfaces.IExtendedSubject;
import org.integratedmodelling.engine.modelling.monitoring.Monitor;
import org.integratedmodelling.engine.modelling.resolver.Coverage;
import org.integratedmodelling.engine.modelling.resolver.Resolver;
import org.integratedmodelling.exceptions.ThinklabException;
import org.integratedmodelling.lang.IRemoteSerializable;

public class Subject extends DirectObservation implements ISubject, IExtendedSubject, Cloneable,
        IRemoteSerializable {

    ArrayList<Pair<IProperty, ISubject>> _subjects = new ArrayList<>();

    IDataset  _backingDataset = null;
    Structure _structure;
    boolean   isPrimary;

    public Subject(IObservable type, IScale scale, INamespace namespace, String name, IMonitor monitor) {
        super(type, scale, namespace, name, monitor);
        _structure = new Structure(this);
    }

    @Override
    public Collection<IState> getStates() {
        ArrayList<IState> ret = new ArrayList<IState>();
        for (Pair<IProperty, IState> pd : _states) {
            ret.add(pd.getSecond());
        }
        return ret;
    }

    public boolean isPrimary() {
        return isPrimary;
    }

    @Override
    public Collection<IState> getNonRawStates() {

        HashMap<IConcept, IState> ret = new HashMap<IConcept, IState>();
        for (Pair<IProperty, IState> pd : _states) {

            IConcept key = (IConcept) pd.getSecond().getObservable().getType();

            /*
             * only substitute an existing one if the one in the result set is raw. When only a raw one is
             * available, use it.
             */
            if (ret.containsKey(key)) {
                if (((State) (ret.get(key))).isRaw()) {
                    ret.put(key, pd.getSecond());
                }
            } else {
                ret.put(key, pd.getSecond());
            }
        }
        return ret.values();

    }

    @Override
    public Map<IProperty, IState> getObjectStateCopy() {
        HashMap<IProperty, IState> result = new HashMap<IProperty, IState>();
        IState state;
        for (Pair<IProperty, IState> pd : _states) {
            state = pd.getSecond();
            if (!(state instanceof IExtent)) {
                result.put(pd.getFirst(), state);
            }
        }
        return result;
    }

    @Override
    public void addState(IState s) throws ThinklabException {
        CallTracer.msg("addState(" + s + ")", this);
        IProperty p = NS.getPropertyFor(s, getObservable(), getNamespace());
        _states.add(new Pair<IProperty, IState>(p, s));
    }

    @Override
    public ISubject clone() {

        Subject ret = new Subject(getObservable(), getScale(), getNamespace(), name, monitor);
        ret._states.addAll(_states);
        ret._subjects.addAll(_subjects);

        return ret;
    }

    @Override
    public Collection<ISubject> getSubjects() {

        // FIXME this should already be ISubject and we should just return the vertexSet
        ArrayList<ISubject> ret = new ArrayList<ISubject>();
        for (IObservation o : _structure.vertexSet()) {
            if (!o.equals(this)) {
                ret.add((ISubject) o);
            }
        }
        return ret;
    }

    /*
     * use with great caution. Only for adjustments in reinterpretation through traits.
     */
    public void setObservable(IObservable obs) {
        observable = obs;
    }

    public IObservation get(String path) {

        if (path.startsWith("/")) {
            path = path.substring(1);
        }
        String[] s = path.split("\\/");
        IObservation ret = this;

        for (String id : s) {

            for (IState state : getStates()) {
                // states have no children
                if (((State) state).getInternalId().equals(id))
                    return state;
            }
            for (ISubject subject : getSubjects()) {
                if (((Subject) subject)._internalID.equals(id))
                    ret = subject;
            }
        }

        return ret;
    }

    public void removeStates() {
        _states.clear();
    }

    @Override
    public String toString() {
        return "S" + getObservable() + ": " + getId();
    }

    @Override
    public ICoverage observe(IObservable observable, Collection<String> scenarios, boolean isOptional, IMonitor monitor)
            throws ThinklabException {

        ICoverage ret = Coverage.EMPTY;

        if (_initialized) {
            try {
                Resolver resolver = new Resolver(_resolutionContext, scenarios);
                ret = resolver.resolve(observable, monitor, this);
            } catch (Exception e) {
                monitor.error(e);
                throw e;
            }
        }

        return ret;
    }

    @Override
    public ICoverage initialize(IResolutionContext context, IMonitor monitor)
            throws ThinklabException {

        ICoverage ret = Coverage.EMPTY;
        try {
            Resolver resolver = new Resolver(context);
            ret = resolver.resolve(this, monitor);
        } catch (Exception e) {
            monitor.error(e);
        } /* finally {
            _dependencies.clear();
          } */
        _initialized = true;
        return ret;
    }

    @Override
    public void contextualize() throws ThinklabException {

        if (!_initialized) {
            // if (initialize(new ArrayList<String>()).isEmpty())
            // return;
        }

        CallTracer.indent("run()", this);
        ITemporalExtent time = getScale().getTime();
        if (time == null) {
            // the subject doesn't proceed through time
            CallTracer.msg("scale for root subject is atemporal");
        } else {
            CallTracer.msg("scale for root subject contains time dimension.");

            CallTracer.msg("creating ObservationController...");
            causalGraph = new TemporalCausalGraph<IDirectActiveObservation, IObservationGraphNode>();
            IObservationController controller = new ObservationController(causalGraph, monitor, time
                    .getEnd());

            // add the root subject (and its children) to the controller's causal graph
            // insert the FIRST time period of the temporal extent
            ITimePeriod simulationTimePeriod = time.collapse();
            addSubjectToObservationGraph(this, simulationTimePeriod, controller, null);

            // TODO for now, only create one observation worker for single-threaded demonstration
            IObservationWorker worker = new ObservationWorker(controller, monitor, ((Monitor) monitor)
                    .getSession(), _context, _taskId);
            try {
                worker.run();
            } catch (Throwable e) {
                monitor.error(e);
            }
            // TODO should this return the controller? or root subject?
        }
    }

    public IDataset requireBackingDataset() throws ThinklabException {

        if (_backingDataset == null) {
            if (!this.getScale().isSpatiallyDistributed()) {
                _backingDataset = new MemoryDataset(this.getScale());
            } else {
                _backingDataset = new NetCDFdataset(this.getScale());
            }
        }
        return _backingDataset;
    }

    public void setContext(IContext context) {
        _context = context;
    }

    public IContext getContext() {
        return _context;
    }

    @Override
    public IDataset getBackingDataset() {
        return _backingDataset;
    }

    @Override
    public IResolutionStrategy getResolutionStrategy() {
        return _resolutionContext == null ? null : _resolutionContext.getResolutionStrategy();
    }

    @Override
    public Collection<IProcess> getProcesses() {
        ArrayList<IProcess> ret = new ArrayList<IProcess>();
        for (Pair<IProperty, IProcess> pd : _processes) {
            ret.add(pd.getSecond());
        }
        return ret;
    }

    @Override
    public ITransition performTemporalTransitionFromCurrentState() throws ThinklabException {

        ITransition result;

        // set the clock forward
        if (moveToNextTimePeriod() == null) {
            // subject terminated
            result = new Transition(scale, null, false);
        } else {
            result = reEvaluateStates(getCurrentTimePeriod());
            // exec any transition-dependent workflows. TODO this call should be skipped if there's nothing to
            // do.
            if (result.agentSurvives() && _resolutionContext != null) {
                prepareForTransition(result);
                _resolutionContext.execute(result);
            }
        }
        return result;
    }

    @Override
    public Object adapt() {
        return MapUtils.of("scale", scale, "structure", (((Structure) getStructure()).hasChanged()
                ? getStructure()
                : null), "states", getStates(), "processes", getProcesses(), "name", name, "internal-id", _internalID, "observable", observable);
    }

    public void setTaskId(long taskId) {
        _taskId = taskId;
    }

    public void setBreakpoint(IObservation obs, boolean b) {
        // TODO Auto-generated method stub
        System.out.println("xx");
    }

    @Override
    public ISubject getSubject(IObservable obs) throws ThinklabException {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public IEvent getEvent(IObservable obs) throws ThinklabException {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public IProcess getProcess(IObservable obs) throws ThinklabException {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public IStructure getStructure(Locator... locators) {
        return _structure;
    }

    @Override
    public ISubject newSubject(IObservable observable, IScale scale, String name, IProperty relationship)
            throws ThinklabException {

        ISubject ret = new Subject(observable, scale, getNamespace(), name, monitor);
        ((Subject) ret)._context = this._context;
        ((Subject) ret)._contextSubject = this;

        _structure.addSubject(ret);
        addDirectObservation((DirectObservation) ret);

        // /*
        // * FIXME this MUST be a DirectObservation but for now it's not (it's in commons). Fix this
        // * for relationships to be added to the observation graph, and remove the check.
        // */
        // if (rel instanceof DirectObservation) {
        // addDirectObservation((DirectObservation) rel);
        // }
        return ret;
    }

    @Override
    public IProcess newProcess(IObservable observable, IScale scale, String name) throws ThinklabException {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public IEvent newEvent(IObservable observable, IScale scale, String name) throws ThinklabException {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public String getId() {
        return name;
    }

    @Override
    public IMetadata getMetadata() {
        return metadata;
    }

    public void setId(String id) {
        name = id;
    }

    @Override
    public Collection<IEvent> getEvents() {
        return new ArrayList<IEvent>();
    }

    public void setPrimary(boolean b) {
        isPrimary = b;
    }

}
