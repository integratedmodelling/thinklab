/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.engine.modelling.bayes;

import java.io.File;

import org.integratedmodelling.common.configuration.KLAB;
import org.integratedmodelling.common.utils.MiscUtilities;
import org.integratedmodelling.engine.modelling.bayes.gn.GenieBayesianNetwork;
import org.integratedmodelling.engine.modelling.bayes.rw.RiskwizBayesianNetwork;
import org.integratedmodelling.exceptions.ThinklabException;

/**
 * Serves bayesian network objects of the type set through the global parameters in
 * plugin configuration. 
 * 
 * @author Ferdinando
 *
 */
public class BayesianFactory {

    public static final String BAYESIAN_ENGINE_PROPERTY = "thinklab.bayesian.engine";

    private static BayesianFactory _this = null;

    private enum BType {
        RISKWIZ,
        GENIE
    };

    BType btype = BType.GENIE;

    private BayesianFactory() {

        // check if the bayesian engine was set in startup options first - these override plugin options
        String engine = System.getProperty(BAYESIAN_ENGINE_PROPERTY);

        // then in plugin config, which takes over
        if (engine == null) {
            engine = KLAB.CONFIG.getProperties().getProperty(BAYESIAN_ENGINE_PROPERTY);
        }

        if (engine != null) {

            if (engine.equals("riskwiz")) {
                btype = BType.RISKWIZ;
            } else if (engine.equals("genie")) {
                btype = BType.GENIE;
            }

            KLAB.info("bayesian engine set to " + engine);

        }
    }

    public static BayesianFactory get() {
        if (_this == null)
            _this = new BayesianFactory();
        return _this;
    }

    public IBayesianNetwork createBayesianNetwork(String resourceId) throws ThinklabException {

        File input = MiscUtilities.resolveUrlToFile(resourceId);
        IBayesianNetwork ret = null;

        if (btype.equals(BType.RISKWIZ)) {
            ret = new RiskwizBayesianNetwork(input);
        } else if (btype.equals(BType.GENIE)) {
            ret = new GenieBayesianNetwork(input);
        }

        return ret;
    }

}
