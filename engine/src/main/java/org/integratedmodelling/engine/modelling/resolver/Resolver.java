/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.engine.modelling.resolver;

import java.util.Collection;
import java.util.List;

import org.integratedmodelling.api.knowledge.IExpression;
import org.integratedmodelling.api.knowledge.ISemantic;
import org.integratedmodelling.api.modelling.IActiveSubject;
import org.integratedmodelling.api.modelling.IConditionalObserver;
import org.integratedmodelling.api.modelling.ICoverage;
import org.integratedmodelling.api.modelling.IDependency;
import org.integratedmodelling.api.modelling.IDerivedObserver;
import org.integratedmodelling.api.modelling.IDirectObservation;
import org.integratedmodelling.api.modelling.IMediatingObserver;
import org.integratedmodelling.api.modelling.IModel;
import org.integratedmodelling.api.modelling.INamespace;
import org.integratedmodelling.api.modelling.IObjectSource;
import org.integratedmodelling.api.modelling.IObservable;
import org.integratedmodelling.api.modelling.IObserver;
import org.integratedmodelling.api.modelling.IState;
import org.integratedmodelling.api.modelling.ISubject;
import org.integratedmodelling.api.modelling.resolution.IResolutionContext;
import org.integratedmodelling.api.modelling.resolution.ISubjectResolver;
import org.integratedmodelling.api.monitoring.IMonitor;
import org.integratedmodelling.collections.Pair;
import org.integratedmodelling.common.kim.KIMModel;
import org.integratedmodelling.common.kim.KIMObserver;
import org.integratedmodelling.common.kim.KIMObservingObject;
import org.integratedmodelling.common.states.States;
import org.integratedmodelling.common.vocabulary.NS;
import org.integratedmodelling.common.vocabulary.Observable;
import org.integratedmodelling.engine.modelling.kbox.ModelKbox;
import org.integratedmodelling.engine.modelling.runtime.DirectObservation;
import org.integratedmodelling.engine.modelling.runtime.Subject;
import org.integratedmodelling.exceptions.ThinklabException;
import org.integratedmodelling.lang.LogicalConnector;

/**
 * A resolver has a top-level resolution context, set to ROOT if it's a top resolver, or to the
 * context that resolved the subject if it's called within one.
 * 
 * @author Ferd
 *
 */
public class Resolver extends BaseResolver implements ISubjectResolver {

    IResolutionContext context;
    Collection<String> scenarios = null;

    public Resolver(IResolutionContext context) {
        this.context = context;
    }

    /*
     * this constructor overrides the scenarios in the original context.
     */
    public Resolver(IResolutionContext context, Collection<String> scenarios) {
        this.context = context;
        if (scenarios != null) {
            this.scenarios = scenarios;
        }
    }

    /*
     * Entry point when a subject is initialized from a context subject's resolution strategy.
     * 
     * (non-Javadoc)
     * @see org.integratedmodelling.api.modelling.resolution.ISubjectResolver#resolve(org.integratedmodelling.api.modelling.ISubject, org.integratedmodelling.api.monitoring.IMonitor)
     */
    @Override
    public ICoverage resolve(IDirectObservation subject, IMonitor monitor) throws ThinklabException {
        ResolutionContext ctx = (ResolutionContext) context.forSubject(subject, scenarios);
        ctx.setMonitor(monitor);
        ((DirectObservation) subject).setResolutionContext(ctx);
        ICoverage ret = resolve(subject, ctx);
        if (ret != null) {
            ctx.execute(null);
        }
        return ret;
    }

    /*
     * Entry point when an observable is resolved from a subject's observe().
     * 
     * (non-Javadoc)
     * @see org.integratedmodelling.api.modelling.resolution.ISubjectResolver#resolve(org.integratedmodelling.api.modelling.IObservable, org.integratedmodelling.api.monitoring.IMonitor)
     */
    @Override
    public ICoverage resolve(IObservable observable, IMonitor monitor, ISubject contextSubject)
            throws ThinklabException {
        ResolutionContext ctx = (ResolutionContext) context.forObservable(observable, scenarios);
        if (NS.isThing(observable)) {
            ctx.contextSubject = contextSubject;
        }
        ctx.setMonitor(monitor);
        ICoverage ret = resolve(observable, ctx);
        if (ret != null && !ret.isEmpty()) {
            if (ctx.getModel() != null && (NS.isQuality(ctx.getModel().getObservable())
                    || NS.isTrait(ctx.getModel().getObservable()))) {
                ctx.defineWorkflow();
            }
            ctx.execute(null);
        }
        return ret;
    }

    private ICoverage resolve(IModel model, ResolutionContext context) throws ThinklabException {

        if (context.isUnsatisfiable)
            return Coverage.EMPTY;

        if (context.monitor.hasErrors()) {
            return Coverage.EMPTY;
        }

        if (model.getObserver() != null) {
            if (resolve(model.getObserver(), context
                    .forObserver(model.getObserver(), model.getDatasource(context.monitor)))
                            .isEmpty()) {
                return Coverage.EMPTY;
            }
        }

        /*
         * resolve all model dependencies, contextualizing at every group if the model is
         * a subject model. This will call finish() on the context passed.
         */
        ICoverage ret = resolveDependencies(model, context, model.getNamespace());

        if (ret.isEmpty())
            return ret;

        IObjectSource objectSource = model.getObjectSource(context.getMonitor());
        if (objectSource != null) {
            context.forObjectSource(objectSource)
                    .finish();
        }

        if (!context.monitor.hasErrors()) {
            context.acceptModel();
            /*
             * NOW if this is a process model, we must create one process for this models's coverage and schedule it for 
             * execution.
             */
        }

        return context.finish();

    }

    /*
     * resolves dependencies for a model, subject or observer, also ensuring that any
     * semantic-induced dependencies are in.
     */
    private ICoverage resolveDependencies(ISemantic observation, ResolutionContext context, INamespace namespace)
            throws ThinklabException {

        if (context.isUnsatisfiable)
            return Coverage.EMPTY;

        ICoverage ret = Coverage.FULL(context.getScale());

        for (List<IDependency> dg : groupDependencies(observation, context)) {

            ResolutionContext cd = context.forGroup(LogicalConnector.INTERSECTION);
            ret = cd.getCoverage();
            boolean quality = false;
            for (IDependency d : dg) {
                if (NS.isQuality(d.getObservable()) || NS.isTrait(d.getObservable())) {
                    quality = true;
                }
                if (resolve(d, cd.forDependency(d, namespace)).isEmpty()) {
                    return Coverage.EMPTY;
                }
            }

            if (quality && ret.isRelevant()) {
                cd.defineWorkflow();
            }
            ret = cd.finish();
        }

        return ret;
    }

    private ICoverage resolve(IObserver observer, ResolutionContext context) throws ThinklabException {

        if (context.isUnsatisfiable)
            return Coverage.EMPTY;

        if (observer instanceof IMediatingObserver
                && ((IMediatingObserver) observer).getMediatedObserver() != null) {
            if (resolve(((IMediatingObserver) observer).getMediatedObserver(), context
                    .forMediatedObserver(((IMediatingObserver) observer).getMediatedObserver())).isEmpty()) {
                return Coverage.EMPTY;
            }
        } else if (observer instanceof IConditionalObserver) {

            int i = 0;
            ResolutionContext cd = context.forGroup(LogicalConnector.UNION);
            for (Pair<IModel, IExpression> observedModelPair : ((IConditionalObserver) observer)
                    .getModels()) {
                IModel model = observedModelPair.getFirst();
                IExpression expression = observedModelPair.getSecond();
                resolve(model, cd.forCondition(expression, i++));
            }
            cd.finish();

            /*
             * model dependencies are only relevant when we have conditional observers; we 
             * link them to the observer.
             */
            IResolutionContext dc = context.forGroup(LogicalConnector.INTERSECTION);
            for (IDependency d : ((KIMModel) context.model).getAllDependencies(context)) {
                if (resolve(d, (ResolutionContext) dc.forDependency(d, observer.getModel().getNamespace()))
                        .isEmpty())
                    break;
            }
            dc.finish();

        } else if (!((KIMObservingObject) observer).hasActuator() && !observer.isComputed()) {
            /*
             * not mediating, not computing and with no user-defined accessor: if we have been passed a
             * datasource to interpret do that, otherwise we must resolve our final observer externally.
             */
            if (context.datasource != null) {
                if (context.forDatasource(context.datasource).finish().isEmpty())
                    return Coverage.EMPTY;
            } else {

                IObservable observable = observer.getObservable();
                if (resolve(observable, context.forObservable(observable)).isEmpty()) {

                    if (observer instanceof IDerivedObserver) {
                        /*
                         * try the indirect way.
                         */
                        if (resolveIndirect((IDerivedObserver) observer, context).isEmpty()) {
                            return Coverage.EMPTY;
                        }
                    } else {
                        return Coverage.EMPTY;
                    }
                }
            }
        }

        List<IDependency> alldeps = ((KIMObserver) observer).getAllDependencies(context);
        if (alldeps.size() > 0) {
            IResolutionContext dc = context.forGroup(LogicalConnector.INTERSECTION);
            for (IDependency d : alldeps) {
                if (resolve(d, (ResolutionContext) dc.forDependency(d, observer.getNamespace()))
                        .isEmpty())
                    break;
            }
            dc.finish();
        }

        return context.finish();
    }

    private ICoverage resolveIndirect(IDerivedObserver observer, ResolutionContext context2) {
        // TODO try the alternative observable/accessor.
        return Coverage.EMPTY;
    }

    private ICoverage resolve(IDirectObservation subject, ResolutionContext context)
            throws ThinklabException {

        if (context.isUnsatisfiable)
            return Coverage.EMPTY;

        if (!((DirectObservation) subject).isInitialized()) {

            ICoverage cov = resolve(subject.getObservable(), context.forObservable(subject.getObservable())
                    .forExplanatoryModel());

            if (cov.isEmpty()) {
                context.getMonitor().error("cannot resolve subject " + subject.getId());
                return cov;
            }
        }

        ICoverage ret = resolveDependencies(subject, context, subject.getNamespace());

        if (ret.isEmpty())
            return ret;

        return context.finish();
    }

    private ICoverage resolve(IDependency dependency, ResolutionContext context) throws ThinklabException {

        if (context.isUnsatisfiable)
            return Coverage.EMPTY;

        IObservable observable = dependency.getObservable();
        ICoverage cov = null;

        if (dependency.getContextModel() != null) {
            // TODO - resolve the context model, then set the result in the context for the
            // following resolution. The next step will necessarily be an object resolution.
        }

        if (observable.getModel() != null) {
            cov = resolve(observable.getModel(), context.forModel(observable.getModel()));
        } else {
            cov = resolve(observable, context.forObservable(observable));
        }

        if (cov.isEmpty() && !context.isOptional()) {
            context.getMonitor().error("mandatory dependency on " + observable.getType() + " is unresolved");
            context.coverage = Coverage.EMPTY;
        }

        return context.finish();
    }

    /*
     * this will be passed a context.forObservable(), which automatically only merges in models that cover
     * enough more context to be relevant, and merges them into one conditional model at finish().
     */
    private ICoverage resolve(IObservable observable, ResolutionContext context) throws ThinklabException {

        if (context.isUnsatisfiable)
            return Coverage.EMPTY;

        ICoverage cov = Coverage.EMPTY;

        if (observable.getModel() != null) {
            cov = resolve(observable.getModel(), context.forModel(observable.getModel()));
        } else {

            IModel previous = context.getModelFor(observable, !context.isExplanatoryModel);
            if (previous != null) {
                /*
                 * TODO we must not re-accept this, but tell the workflow to use the results of its
                 * previous run.
                 */
                cov = resolve(previous, context.forModel(previous));
                if (cov != null) {
                    cov = context.finish();
                }
            } else {

                /*
                 * if we're resolving a subject, coverage is full if we don't find any model. Any other
                 * situation, including processes and events, must have a model.
                 */
                cov = (NS.isThing(observable) && context.isExplanatoryModel)
                        ? Coverage.FULL(context.getScale()) : Coverage.EMPTY;

                /*
                 * lookup model for this observable. ModelKbox will do all the
                 * ranking and sorting as long as its own iterator is used. 
                 */
                boolean found = false;

                /*
                 * we go through search only if we have not already seen the observable in a context
                 * where we have recorded that we can proceed with no model.
                 */
                if (!(context.isExplanatoryModel && context.hasNoModel(observable))) {

                    /*
                     * lookup state for same model in parent contexts. If found, create 
                     * a state view, which may (some day) be intelligent enough to make further 
                     * observations to adapt to different scales, and just resolve a state model
                     * using it.
                     */
                    if (context.contextSubject != null
                            && (NS.isQuality(observable) || NS.isTrait(observable))) {

                        // ensure we have one. Will not create NetCDFs unless there is raster space.
                        ((Subject) context.subject).requireBackingDataset();
                        IState prevobs = States
                                .findView(context.contextSubject, observable, (IActiveSubject) context.subject);

                        if (prevobs != null) {
                            StateModel sm = new StateModel(observable, prevobs, context
                                    .getResolutionNamespace());
                            cov = resolve(sm, context.forModel(sm));
                            if (cov.isComplete()) {
                                return context.finish();
                            }
                        }
                    }

                    /*
                     * found or not, we want to also check the database and network
                     * for subjects if the observable is a subject and we don't know already which
                     * subject we are resolving (i.e., we're observing the concept within another
                     * context). Add a custom step to that extent to the resolution strategy.
                     */
                    if (NS.isThing(observable)
                            && (!context.isExplanatoryModel || ((Observable) observable).isInstantiator())) {
                        context.resolutionStrategy
                                .addStep(observable.getTypeAsConcept(), context.interpretAs);
                    }

                    for (IModel m : ModelKbox.get().query(observable, context)) {

                        if (m == null) {
                            continue;
                        }

                        found = true;

                        if (context.isResolving(m)) {
                            continue;
                        }

                        if (!m.isAvailable()) {
                            context.monitor.warn("model " + m.getName() + " is unavailable");
                            continue;
                        }

                        cov = resolve(m, context.forModel(m));

                        if (cov.isComplete()) {
                            return context.finish();
                        }
                    }

                    /*
                     * ensure DB search is done even if no models are found in this situation.
                     */
                    if (NS.isThing(observable) && !context.isExplanatoryModel) {
                        cov = context.finish(Coverage.FULL(context.scale));
                    }

                    /*
                     * if we have not found a model but we're continuing, record the incident so
                     * we don't have to query again for the next. We record this in the parent
                     * or it won't be transferred, as finish() is only called when we have a
                     * complete context.
                     */
                    if (!found && !cov.isEmpty()) {
                        context.parent.proceedWithNoModel(observable);
                    }
                }

            }
        }

        return cov;
    }
}
