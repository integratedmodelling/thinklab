/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.engine.modelling;

import org.integratedmodelling.api.modelling.agents.IAgentState;
import org.integratedmodelling.api.modelling.agents.ICollision;
import org.integratedmodelling.api.time.ITimeInstant;

/**
 * this represents an interruption of an agent-state which is caused by some
 * outside force. Agents change their own states by their own decisions; collisions cause states to change
 * without agents' decisions.
 * 
 * Also, collisions do not have to respect the colliding agents' schedules. They can happen at any time, and
 * so they have the ability to alter the agents' temporal scales.
 * 
 * Sometimes the causingAgent/impactedAgent don't matter and sometimes they do. When they do, the "causing"
 * agent is the one taking action. For instance, if the collision represents a message being sent, the
 * causingAgent is the sender and the impactedAgent is the recipient.
 * 
 * @author luke
 * 
 */
public class Collision implements ICollision {
    private final ITimeInstant collisionTime;
    private final IAgentState causingAgentState;
    private final IAgentState impactedAgentState;

    public Collision(ITimeInstant collisionTime, IAgentState causingAgentState, IAgentState impactedAgentState) {
        this.collisionTime = collisionTime;
        this.causingAgentState = causingAgentState;
        this.impactedAgentState = impactedAgentState;
    }

    @Override
    public ITimeInstant getCollisionTime() {
        return collisionTime;
    }

    @Override
    public IAgentState getCausingAgent() {
        return causingAgentState;
    }

    @Override
    public IAgentState getImpactedAgent() {
        return impactedAgentState;
    }
}
