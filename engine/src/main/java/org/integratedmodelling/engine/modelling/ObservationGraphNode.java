/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.engine.modelling;

import java.util.Collection;
import java.util.HashSet;

import org.integratedmodelling.api.modelling.agents.IAgentState;
import org.integratedmodelling.api.modelling.agents.IObservationGraphNode;
import org.integratedmodelling.api.modelling.agents.IObservationTask;
import org.integratedmodelling.api.modelling.agents.IScaleMediator;
import org.integratedmodelling.api.modelling.scheduling.ITransition;

public class ObservationGraphNode implements IObservationGraphNode {

    private final IAgentState state;
    private final IObservationTask taskCreatingThisNodeState;
    private final ITransition transitionFromPreviousNodeState;
    private Collection<IScaleMediator> subscribers; // ScaleMediators which have actually READ this node's state

    // TODO think of how to clean this up over time. Once processing proceeds BEYOND the end time of an agent-state,
    // then the causal links from that agent-state could be deleted
    private final HashSet<IObservationTask> tasksCausedByThisNodeState = new HashSet<IObservationTask>();

    public ObservationGraphNode(IAgentState initialState, IObservationTask taskCreatingThisNodeState) {
        this(initialState, taskCreatingThisNodeState, null);
    }

    public ObservationGraphNode(IAgentState initialState, IObservationTask taskCreatingThisNodeState,
            ITransition transitionFromPreviousNodeState) {
        this.state = initialState;
        this.taskCreatingThisNodeState = taskCreatingThisNodeState;
        this.transitionFromPreviousNodeState = transitionFromPreviousNodeState;
    }

    @Override
    public IAgentState getAgentState() {
        return state;
    }

    @Override
    public ITransition getTransitionFromPrevious() {
        return transitionFromPreviousNodeState;
    }

    @Override
    public IObservationTask getTaskCreatingThisNodeState() {
        return taskCreatingThisNodeState;
    }

    @Override
    public void addTaskCausedByThisNodeState(IObservationTask task) {
        tasksCausedByThisNodeState.add(task);
    }

    @Override
    public Collection<IObservationTask> getTasksCausedByThisNodeState() {
        return tasksCausedByThisNodeState;
    }

    @Override
    public Collection<IScaleMediator> getSubscribers() {
        return subscribers;
    }

    @Override
    public String toString() {
        return state.toString();
    }

    @Override
    public boolean canCollideWithAnything() {
        return true;
    }
}
