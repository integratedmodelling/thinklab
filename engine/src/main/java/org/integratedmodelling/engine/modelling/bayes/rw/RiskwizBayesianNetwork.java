/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.engine.modelling.bayes.rw;

import java.io.File;
import java.io.FileInputStream;
import java.util.Vector;

import org.integratedmodelling.engine.modelling.bayes.IBayesianInference;
import org.integratedmodelling.engine.modelling.bayes.IBayesianNetwork;
import org.integratedmodelling.exceptions.ThinklabException;
import org.integratedmodelling.exceptions.ThinklabIOException;
import org.integratedmodelling.riskwiz.bn.BeliefNetwork;
import org.integratedmodelling.riskwiz.domain.Domain;
import org.integratedmodelling.riskwiz.domain.LabelDomain;
import org.integratedmodelling.riskwiz.io.genie.GenieReader;

public class RiskwizBayesianNetwork implements IBayesianNetwork {

    BeliefNetwork bn = null;

    public RiskwizBayesianNetwork(File in) throws ThinklabIOException {

        /*
         * TODO support other formats based on file extension, or create a new
         * GenericReader.
         */
        GenieReader r = new GenieReader();
        try {
            this.bn = r.load(new FileInputStream(in));
        } catch (Exception e) {
            throw new ThinklabIOException(e);
        }

    }

    @Override
    public IBayesianInference getInference() {
        return new RiskwizBayesianInference(bn);
    }

    @Override
    public int getNodeCount() {
        return bn.getNodeNumber();
    }

    @Override
    public String[] getAllNodeIds() {
        return bn.getNodeNames();
    }

    @Override
    public int getOutcomeCount(String nodeId) {
        return bn.getBeliefNode(nodeId).getCount();
    }

    @Override
    public String getOutcomeId(String nodeId, int outcomeIndex) {
        return getOutcomeIds(nodeId)[outcomeIndex];
    }

    @Override
    public String[] getParentIds(String nodeId) {
        return bn.getParents(nodeId);
    }

    @Override
    public String[] getChildIds(String nodeId) {
        return bn.getChildren(nodeId);
    }

    @Override
    public String[] getOutcomeIds(String nodeId) {

        String[] ret = null;
        Domain domain = bn.getBeliefNode(nodeId).getDomain();
        if (domain instanceof LabelDomain) {
            Vector<String> cc = ((LabelDomain) domain).getStates();
            ret = new String[cc.size()];
            int i = 0;
            for (String c : cc)
                ret[i++] = c;
        }
        return ret;
    }

    @Override
    public String getName() {
        return bn.getName();
    }

    @Override
    public IBayesianNetwork train(File observations, String method) throws ThinklabException {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public void write(File modelFile) throws ThinklabIOException {
        // TODO Auto-generated method stub

    }

    @Override
    public boolean isLeaf(String nodeId) {
        // TODO Auto-generated method stub
        String[] ids = getChildIds(nodeId);
        return ids == null || ids.length == 0;
    }

}
