/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.engine.modelling.resolver;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.integratedmodelling.api.configuration.IConfiguration;
import org.integratedmodelling.api.knowledge.IExpression;
import org.integratedmodelling.api.modelling.IActiveSubject;
import org.integratedmodelling.api.modelling.IClassifyingObserver;
import org.integratedmodelling.api.modelling.ICoverage;
import org.integratedmodelling.api.modelling.IDirectObservation;
import org.integratedmodelling.api.modelling.IModel;
import org.integratedmodelling.api.modelling.IObservable;
import org.integratedmodelling.api.modelling.IObserver;
import org.integratedmodelling.api.modelling.IScale;
import org.integratedmodelling.api.modelling.IState;
import org.integratedmodelling.api.modelling.ISubject;
import org.integratedmodelling.api.modelling.resolution.IProvenance;
import org.integratedmodelling.api.modelling.resolution.IWorkflow;
import org.integratedmodelling.api.modelling.runtime.IActiveDataSource;
import org.integratedmodelling.api.modelling.runtime.IActiveObserver;
import org.integratedmodelling.api.modelling.runtime.IActuator;
import org.integratedmodelling.api.modelling.runtime.IStateActuator;
import org.integratedmodelling.api.modelling.runtime.ISwitchingActuator;
import org.integratedmodelling.api.modelling.scheduling.ITransition;
import org.integratedmodelling.api.modelling.storage.IDataset;
import org.integratedmodelling.api.monitoring.IMonitor;
import org.integratedmodelling.common.HashableObject;
import org.integratedmodelling.common.configuration.KLAB;
import org.integratedmodelling.common.data.Edge;
import org.integratedmodelling.common.kim.KIMObserver;
import org.integratedmodelling.common.model.actuators.Actuator;
import org.integratedmodelling.common.model.actuators.StateActuator;
import org.integratedmodelling.common.visualization.GraphVisualization;
import org.integratedmodelling.common.visualization.GraphVisualization.GraphAdapter;
import org.integratedmodelling.common.vocabulary.NS;
import org.integratedmodelling.engine.KLABEngine;
import org.integratedmodelling.engine.introspection.CallTracer;
import org.integratedmodelling.engine.modelling.resolver.ProvenanceGraph.DependencyEdge;
import org.integratedmodelling.engine.modelling.resolver.ProvenanceGraph.ProvenanceNode;
import org.integratedmodelling.engine.modelling.resolver.Workflow.DataPath;
import org.integratedmodelling.engine.modelling.resolver.Workflow.ProcessingStep;
import org.integratedmodelling.engine.modelling.resolver.Workflow.ProcessingStep.Cond;
import org.integratedmodelling.engine.modelling.resolver.Workflow.ProcessingStep.OutputAction;
import org.integratedmodelling.engine.modelling.runtime.Subject;
import org.integratedmodelling.exceptions.ThinklabException;
import org.integratedmodelling.utils.graph.GraphViz;
import org.integratedmodelling.utils.graph.GraphViz.NodePropertiesProvider;
import org.jgrapht.alg.CycleDetector;
import org.jgrapht.graph.DefaultDirectedGraph;
import org.jgrapht.traverse.TopologicalOrderIterator;

/**
 * An acyclic dataflow built from the ProvenanceGraph of a resolved model.
 * 
 * Iterates the processing steps in order of dependency.
 * 
 * @author Ferd
 */
public class Workflow extends DefaultDirectedGraph<ProcessingStep, DataPath>
        implements Iterable<ProcessingStep>, IWorkflow {

    private static final long serialVersionUID = 2348908259284218130L;

    public static final int INITIALIZATION_SCHEDULE = 0;
    public static final int TIME_SCHEDULE           = 1;

    private int lastRegister = 0;

    IDirectObservation        _subject;
    IModel                    _rootModel;
    ProvenanceGraph           _modelGraph;
    IScale                    _scale;
    IMonitor                  _monitor;
    ArrayList<ProcessingStep> _entryPoints = new ArrayList<ProcessingStep>();
    long                      _contextId;

    // the overall coverage of the observation we're performing. Used to pass to the states.
    final ICoverage _coverage;

    HashMap<ProvenanceNode, ProcessingStep> steps = new HashMap<ProvenanceNode, ProcessingStep>();

    private final Collection<IState> _states    = new ArrayList<IState>();
    private Object[]                 _registers = null;
    private List<ISubject>           _subjects  = new ArrayList<ISubject>();

    /*
     * flags that decide which states to generate. Default is only directly resolved nodes and
     * those that have dependencies. Not used at the moment (I doubt anyone will take the trouble
     * of customizing this). Complete checkStorage() to support fully.
     */
    boolean storeRaw          = Boolean
            .parseBoolean(KLAB.CONFIG
                    .getProperties()
                    .getProperty(IConfiguration.STORE_RAW_DATA_PROPERTY, "false"));
    boolean storeIntermediate = Boolean
            .parseBoolean(KLAB.CONFIG
                    .getProperties()
                    .getProperty(IConfiguration.STORE_INTERMEDIATE_DATA_PROPERTY, "true"));
    boolean storeConditional  = Boolean
            .parseBoolean(KLAB.CONFIG
                    .getProperties()
                    .getProperty(IConfiguration.STORE_CONDITIONAL_DATA_PROPERTY, "false"));
    boolean storeMediated     = Boolean
            .parseBoolean(KLAB.CONFIG
                    .getProperties()
                    .getProperty(IConfiguration.STORE_MEDIATED_DATA_PROPERTY, "false"));

    public Workflow(IDirectObservation subject, IModel rootModel, IProvenance modelGraph, IMonitor monitor,
            ICoverage coverage, long contextId) throws ThinklabException {
        super(DataPath.class);
        _rootModel = rootModel;
        _modelGraph = (ProvenanceGraph) modelGraph;
        _subject = subject;
        _scale = subject.getScale();
        _monitor = monitor;
        _coverage = coverage;
        _contextId = contextId;
        compile();
        optimize();
    }

    private void optimize() {

        /*
         * TODO more checks. For now just removes precomputed nodes if they're not involved in anything.
         */
        ArrayList<ProcessingStep> trivials = new ArrayList<Workflow.ProcessingStep>();
        for (ProcessingStep s : this) {
            if (s.model instanceof StateModel &&
                    !((StateModel) s.model).isView()
                    && this.outgoingEdgesOf(s).size() == 0
                    && this.incomingEdgesOf(s).size() == 0) {
                trivials.add(s);
            }
        }

        for (ProcessingStep s : trivials) {
            this.removeVertex(s);
        }
    }

    @Override
    public boolean addEdge(ProcessingStep stepFrom, ProcessingStep stepTo, DataPath edge) {
        // CallTracer.indent("addEdge()", this, stepFrom, stepTo, edge);
        /*
         * every edge except dependency has no formal name set in, and the accessors may not either.
         */
        if (stepFrom.actuator.getName() == null && stepFrom.observer != null) {
            ((Actuator) (stepFrom.actuator)).setName(stepFrom.observer.getId());
        }
        if (stepTo.actuator.getName() == null) {
            ((Actuator) (stepTo.actuator)).setName(stepTo.observer.getId());
        }
        if (edge.formalName == null || edge.formalName.isEmpty()) {
            edge.formalName = stepTo.actuator.getName();
        }

        // CallTracer.unIndent();
        return super.addEdge(stepFrom, stepTo, edge);
    }

    private void compile() throws ThinklabException {

        /*
         * should return null when called at this level.
         */
        compileInternal(_modelGraph.get(_rootModel), null, _scale, _modelGraph);

        // /*
        // * since now, we may be compiling a partial workflow just for one observation, so the
        // * workflow may not contain the whole provenance up to the subject. Also, the model
        // * for the subject is now optional. So this may happen.
        // */
        // if (_entryPoints.isEmpty()) {
        //
        for (ProcessingStep s : this) {
            Set<DataPath> oe = outgoingEdgesOf(s);
            if (oe.size() == 0) {
                _entryPoints.add(s);
            }
        }
    }

    private ProcessingStep newStep(IActuator actuator, IObserver observer, ProvenanceNode node, ProvenanceGraph graph) {

        ProcessingStep ret = new ProcessingStep(actuator, observer);
        addVertex(ret);
        ret.model = node.model;
        ret.observer = observer;
        steps.put(node, ret);
        ret.stored = checkStorage(node, graph);
        return ret;
    }

    /**
     * We only get datasource and observer nodes - those that can produce a value. The
     * datasource ones we store iif we want raw data. The observer ones only if they
     * are the target of a resolution from a model and they define the state of
     * another.
     * 
     * @param node
     * @param graph
     * @return
     */
    private boolean checkStorage(ProvenanceNode node, ProvenanceGraph graph) {

        /*
         * optional = one of the optional sources for a conditional accessor
         */
        boolean isRaw = false;
        boolean hasDirectDependency = false;
        boolean definesState = false;
        boolean wasResolved = false;
        boolean ret = true;
        boolean interpretAs = false;

        for (DependencyEdge d : graph.outgoingEdgesOf(node)) {
            if (d.type == DependencyEdge.INTERPRET_AS) {
                isRaw = true;
            }
            if (d.type == DependencyEdge.DEFINE_STATE) {
                // sends input to model
                definesState = true;
            }
        }

        for (DependencyEdge d : graph.incomingEdgesOf(node)) {
            if (d.type == DependencyEdge.DEPENDENCY && d.conditionIndex < 0) {
                hasDirectDependency = true;
            }
            if (d.type == DependencyEdge.RESOLVES) {
                // was resolved, i.e. doesn't just interpret a datasource.
                wasResolved = true;
            }
            if (d.type == DependencyEdge.INTERPRET_AS) {
                // interprets a datasource.
                interpretAs = true;
            }
        }

        ret = (definesState /* && (wasResolved || interpretAs)*/) /* || hasDirectDependency */
                || (storeRaw && isRaw);

        /*
         * whatever happened, we don't want to store previous states again.
         */
        if (node.model instanceof StateModel) {
            ret = false;
        }

        return ret;

    }

    private ProcessingStep compileInternal(ProvenanceNode node, IObserver contextObserver, IScale context, ProvenanceGraph provenanceGraph)
            throws ThinklabException {

        CallTracer.indent("compileInternal()", this, node, contextObserver, context);
        ProcessingStep result = null;

        // FIXME patch - may happen if one dep is a process or subject.
        if (node == null)
            return null;

        if (steps.containsKey(node)) {
            CallTracer.msg("node has already been processed. Returning previous ProcessingStep...");
            result = steps.get(node);
        } else if (node.datasource != null) {
            /*
             * we add the observer and model later, from the observer accessor upstream.
             */
            CallTracer
                    .msg("node has already designated a datasource. Generating step from datasource.getAccessor()...");
            // result = newStep(node.datasource.getAccessor(context, contextObserver, _monitor), null, node);
            result = newStep(((IActiveDataSource) node.datasource)
                    .getAccessor(context, node.observer, _monitor), node.observer, node, provenanceGraph);
        } else if (provenanceGraph.incomingEdgesOf(node).size() > 0) {

            CallTracer.msg("node does not exist and has no datasource defined but has inputs...");

            IObserver observer = node.getObserver();
            IModel model = node.model;

            for (ProvenanceGraph.DependencyEdge edge : provenanceGraph.incomingEdgesOf(node)) {

                IActuator actuator = null;
                boolean childIsEntryPoint = false;
                ProcessingStep child = compileInternal(edge
                        .getSourceNode(), node.observer, context, provenanceGraph);

                if (child == null) {

                    /*
                     * No datasource at the end of the line. If we have a computing observer 
                     * without a datasource, compile it in.
                     */
                    if (observer != null && observer.isComputed()) {
                        if (result == null) {
                            result = newStep(((KIMObserver) observer)
                                    .getComputingAccessor(_monitor), observer, node, provenanceGraph);
                        }
                        // addEdge(node, result, new DataPath(edge));
                    }

                    continue;
                }

                CallTracer.msg("switching on edge.type: " + edge.type);

                switch (edge.type) {
                case DependencyEdge.DEPENDENCY:
                case DependencyEdge.CONDITIONAL_DEPENDENCY:

                    childIsEntryPoint = node.model != null && node.model.getObserver() == null;

                    if (childIsEntryPoint) {
                        // CallTracer.msg("node.model has no observer. adding entry point for child: " +
                        // child);
                        // _entryPoints.add(child);
                    } else {

                        if (result == null) {
                            result = newStep(node
                                    .getAccessor(context, _monitor), observer, node, provenanceGraph);
                        }
                        CallTracer.msg("adding dependency edge for child: " + child);
                        addEdge(child, result, new DataPath(edge));
                    }
                    break;

                case DependencyEdge.MEDIATE_TO:
                case DependencyEdge.RESOLVES:

                    /*
                     * we must have an observer to get here. Get the observer from the child and call
                     * getMediator(); if the result is null (no mediation needed) we fall through to using the
                     * child.
                     */
                    if (child.observer != null) {
                        actuator = ((IActiveObserver) node.observer).getMediator(child.observer, _monitor);
                    }

                    if (actuator != null) {

                        /*
                         * If we do mediate, the mediated gets the same name as our own accessor.
                         */
                        CallTracer.msg("got mediated dependency using accessor: " + actuator);
                        result = newStep(actuator, observer, node, provenanceGraph);
                        addEdge(child, result, new DataPath(edge));
                        break;
                    }
                    // else fall through

                case DependencyEdge.INTERPRET_AS:

                    if (!((IActiveObserver) observer).canInterpretDirectly(child.actuator)) {
                        /*
                         * we are reinterpreting a datasource value through actions. Just put in a bare
                         * accessor with nothing but the actions.
                         */
                        CallTracer.msg("cannot interpret child directly. Creating interpreter...");
                        result = newStep(((IActiveObserver) observer)
                                .getInterpreter((IStateActuator) child.actuator, _monitor), observer, node, provenanceGraph);
                        addEdge(child, result, new DataPath(edge));
                        break;
                    }
                    // else fall through

                case DependencyEdge.DEFINE_STATE:
                    CallTracer.msg("using normal pass-through dependency...");
                    result = child;
                    break;
                }

                if (child.observer == null)
                    child.observer = observer;
                if (child.model == null)
                    child.model = model;

            }

            /*
             * only store states for non-branch accessors TODO shouldn't store state if implied in conditional
             */
            if (result != null && node.model != null && node.getObserver() != null) {
                notifyObservables(node, result);
            }

        } else if (node.observer != null
                && (node.observer.isComputed() || ((KIMObserver) (node.observer)).hasActuator())) {
            /*
             * we add the observer and model later, from the observer accessor upstream.
             */
            IActuator act = ((KIMObserver) node.observer).hasActuator() ? node.observer.getActuator(_monitor)
                    : ((KIMObserver) node.observer).getComputingAccessor(_monitor);

            /*
             * the context is not set here, so do it.
             */
            ((Actuator) act).setContext(_subject);

            CallTracer.msg("node has no dependencies or datasource but produces data through an accessor.");
            result = newStep(act, node.observer, node, provenanceGraph);
        }

        if (result != null && result.actuator != null && result.actuator.getName() == null
                && result.observer != null) {
            // happens when there is only one node. FIXME should be written cleanly, one day.
            ((Actuator) (result.actuator)).setName(result.observer.getId());
        }

        CallTracer.msg("result: " + result);
        CallTracer.unIndent();
        return result;
    }

    private void notifyObservables(ProvenanceNode node, ProcessingStep step) {

        ArrayList<IObservable> observables = new ArrayList<IObservable>();
        ArrayList<String> observableNames = new ArrayList<String>();
        ArrayList<IObserver> observers = new ArrayList<IObserver>();

        /*
         * use the top model to get at the intended observables.
         */
        IModel tmodel = node.model; // ((Model)(node.model)).getTopLevelModel();

        for (int i = 0; i < tmodel.getObservables().size(); i++) {

            IObservable observable = tmodel.getObservables().get(i);
            String name = i == 0 ? step.actuator.getName() : observable.getFormalName();
            IObserver observer = observable.getObserver();

            observables.add(observable);
            observableNames.add(name);
            observers.add(observer);
        }

        if (observables.size() > 0) {
            step.observables = observables;
            step.observableNames = observableNames;
            step.observers = observers;
        }
    }

    public class DataPath extends Edge {

        /*
         * each data path is for ONE observable and between two accessors. The same accessor may appear as the
         * target of more than one path. Each accessor has a name which it is known to itself with, and is the
         * ID of the model it comes from or it represents (in the case of datasource accessors). Each path has
         * a formal name that the other accessor is known to it with. Paths act as name translators when
         * connections are made. When an accessor reinterprets a datasource (which only happens with actions
         * or mediations) the path has the same formal name as the target accessor's. Otherwise the dependency
         * name is used as the formal name.
         */
        public DataPath(DependencyEdge edge) {

            isMediation = edge.type == DependencyEdge.MEDIATE_TO;

            if ((isConditional = (conditionIndex >= 0))) {
                conditionIndex = edge.conditionIndex;
            }

            formalName = edge.formalName;
            observable = edge.observable;
            register = lastRegister++;
        }

        private static final long serialVersionUID = 2366743581134478147L;

        public boolean     isMediation    = false;
        public String      formalName     = null;
        public IObservable observable     = null;
        public boolean     isConditional;
        public int         conditionIndex = -1;

        @Override
        public String toString() {
            return getSource() + " -- " + (isMediation ? "m/" : "")
                    + (isConditional ? ("c/" + conditionIndex) + "/" : "") + formalName + " --> "
                    + getTarget();
        }

        public IStateActuator getSourceAccessor() {
            return (IStateActuator) ((ProcessingStep) (this.getSource())).actuator;
        }

        public IStateActuator getTargetAccessor() {
            return (IStateActuator) ((ProcessingStep) (this.getTarget())).actuator;
        }

        public ProcessingStep getSourceStep() {
            return (ProcessingStep) (this.getSource());
        }

        public ProcessingStep getTargetStep() {
            return (ProcessingStep) (this.getTarget());
        }

        /*
         * each dependency gets its own register automatically
         */
        public int register = -1;

        @Override
        public boolean equals(Object edge) {
            return edge instanceof DataPath && this.getSource().equals(((DataPath) edge).getSource())
                    && this.getTarget().equals(((DataPath) edge).getTarget())
                    && isMediation == ((DataPath) edge).isMediation;
        }
    }

    /*
     * compilation element - the workflow is made of these. It holds the accessor (the processing step) and
     * the observer that provides its observation semantics. Observable is only set in it when we want to
     * create a state.
     */
    public static class ProcessingStep extends HashableObject {

        public IModel model;

        public ProcessingStep(IActuator actuator, IObserver observer) {
            this.actuator = actuator;
            this.observer = observer;
        }

        public IObserver observer;
        public IActuator actuator;

        @Override
        public String toString() {
            return "<" + actuator.getName() + ">";
        }

        public boolean isConditional;

        /*
         * all actions to be done as output after accessor was called - set to register if >= 0, save to state
         * with given name if state != null.
         */
        static class OutputAction {
            String formalName;
            int    register = -1;
        }

        ArrayList<OutputAction> outputs = new ArrayList<OutputAction>();

        /*
         * if conditional, this structure tells us how to process the switch instead of calling process().
         */
        static class Cond {
            int            index;
            IExpression    expression;
            ProcessingStep target;
            String         formalName;
        }

        List<Cond> conditionals = null;

        /*
         * TODO this would be lots simpler and faster if we used concepts, but that creates endless
         * repercussions at the moment.
         */
        public List<IObservable> observables;
        public List<String>      observableNames;
        public List<IObserver>   observers;

        public boolean            stored;
        public Collection<IState> states;
        public boolean            isDynamic;
    }

    @Override
    public Iterator<ProcessingStep> iterator() {

        CycleDetector<ProcessingStep, DataPath> cycleDetector = new CycleDetector<ProcessingStep, DataPath>(this);

        if (cycleDetector.detectCycles()) {

            Iterator<ProcessingStep> iterator;
            Set<ProcessingStep> cycleVertices;
            Set<ProcessingStep> subCycle;
            ProcessingStep cycle;

            // TODO leave but report an internal error and
            // check what is happening. The point here is that we should
            // distinguish cycles that are semantic contradiction from those
            // that can be seen as representational artifacts once temporal dynamics is
            // factored in.
            KLAB.warn("Cycles detected in workflow.");

            // Get all vertices involved in cycles.
            cycleVertices = cycleDetector.findCycles();

            // Loop through vertices trying to find disjoint cycles.
            while (!cycleVertices.isEmpty()) {

                // Get a vertex involved in a cycle.
                iterator = cycleVertices.iterator();
                cycle = iterator.next();

                // Get all vertices involved with this vertex.
                subCycle = cycleDetector.findCyclesContainingVertex(cycle);
                for (ProcessingStep sub : subCycle) {
                    // Remove vertex so that this cycle is not encountered
                    // again.
                    // cycleVertices.remove(sub);
                }
            }
        }

        return new TopologicalOrderIterator<Workflow.ProcessingStep, Workflow.DataPath>(this);
    }

    public Collection<IState> getStates() throws ThinklabException {
        return _states;
    }

    public String dump() {

        GraphViz ziz = new GraphViz();
        ziz.loadGraph(this, new NodePropertiesProvider<ProcessingStep, DataPath>() {

            @Override
            public int getNodeWidth(ProcessingStep o) {
                return 40;
            }

            @Override
            public String getNodeId(ProcessingStep ps) {
                return ps.actuator + " (" + ps.hashCode() + ")";
            }

            @Override
            public int getNodeHeight(ProcessingStep o) {
                return 20;
            }

            @Override
            public String getNodeShape(ProcessingStep o) {
                return o.actuator instanceof ISwitchingActuator ? DIAMOND
                        : (o.actuator.getActions().size() > 0 ? COMPONENT : BOX);
            }

            @Override
            public String getEdgeColor(DataPath e) {
                DataPath de = e;
                // TODO put a switch here will you
                return
                // de.isInitialization ? "red" :
                (de.isMediation ? "blue" : (de.isConditional ? "yellow" : "black"));
            }

            @Override
            public String getEdgeLabel(DataPath e) {
                return (e.observable == null ? "" : e.observable.getLocalName())
                        + (e.isConditional ? (" #" + e.conditionIndex) : "");
            }

        }, false);

        return ziz.getDotSource();
    }

    /**
     * Run the workflow once per context state, before time exists, using pull strategy. This produce initial
     * states for all states. Schedules are computed after this has run from 0 to context multiplicity, driven
     * by observe() in ResolvedSubject.
     * 
     * If anything bad happens, use logging in the session/subject to communicate it and return false instead
     * of throwing exceptions.
     * 
     */
    public boolean runAt(int contextIndex, ITransition transition) {

        if (this.vertexSet().isEmpty())
            return true;

        HashSet<ProcessingStep> computed = new HashSet<ProcessingStep>();
        for (ProcessingStep entry : _entryPoints) {
            if (!compute(entry, contextIndex, transition, computed))
                return false;
        }
        return true;
    }

    /**
     * Run the workflow for the passed transition, already determined by the scheduler to be relevant for it.
     * 
     * If anything bad happens, use logging in the session/subject to communicate it and return false instead
     * of throwing exceptions.
     */
    @Override
    public boolean run(ITransition transition) {

        if (this.vertexSet().isEmpty())
            return true;

        if (transition == null) {

            IScale scale = _scale.getSubscale(KLABEngine.c(NS.TIME_DOMAIN), 0);

            // initialize for all context states. The simple loop is fine here.
            if (preprocess()) {

                if (scale.getMultiplicity() > 1) {
                    _monitor.info("initializing " + scale.getMultiplicity() + " states", null);
                }

                // main initialization loop.
                // TODO parallelize. With the current time/space, we should be able to just use
                // Parallel.for, but the issue is whether the external accessors are reentrant
                // or not.
                for (int i : _scale.getIndex(transition)) {
                    // for (int i = 0; i < scale.getMultiplicity(); i++) {
                    // }
                    if (_monitor.isStopped()) {
                        _monitor.warn("initialization interrupted by user");
                        return false;
                    }
                    runAt(i, transition);
                }

                /*
                 * set the states in the subject and it's all done.
                 */
                try {
                    for (IState s : getStates()) {
                        ((IActiveSubject) _subject).addState(s);
                    }
                } catch (ThinklabException e) {
                    _monitor.error(e);
                    return false;
                }
            }
        } else {

            // HashSet<ProcessingStep> computed = new HashSet<ProcessingStep>();
            for (int i : _scale.getIndex(transition)) {
                // for (int i = 0; i < scale.getMultiplicity(); i++) {
                // }
                if (_monitor.isStopped()) {
                    _monitor.warn("initialization interrupted by user");
                    return false;
                }
                runAt(i, transition);
            }

        }

        return true;
    }

    /*
     * called once before initialize() and possibly run() are called. Call every notifier in accessors and
     * communicate all relevant contextual information. If anything bad happens, use logging in the
     * session/subject to communicate it and return false instead of throwing exceptions.
     */
    public boolean preprocess() {

        if (this.vertexSet().isEmpty())
            return true;

        HashSet<ProcessingStep> computed = new HashSet<ProcessingStep>();
        HashSet<IActuator> visualized = new HashSet<IActuator>();
        for (ProcessingStep entry : _entryPoints) {
            if (!notify(entry, computed, visualized))
                return false;
        }

        _registers = new Object[lastRegister];

        /*
         * create states for anything we want stored.
         */
        for (ProcessingStep step : vertexSet()) {
            defineStates(step);
        }

        if (_monitor != null) {
            // no need to continue
            if (_monitor.hasErrors()) {
                return false;
            }
            _monitor.debug("preprocessing finished");
        }

        return true;
    }

    private boolean notify(ProcessingStep step, HashSet<ProcessingStep> computed, HashSet<IActuator> visualized) {

        if (computed.contains(step))
            return true;
        computed.add(step);

        IStateActuator acc = (IStateActuator) step.actuator;
        step.isConditional = acc instanceof ISwitchingActuator;

        try {
            acc.notifyObserver(step.model == null ? step.observer.getObservable()
                    : step.model.getObservable(), step.observer);

            IModel tmodel = ((KIMObserver) (step.observer)).getTopLevelModel();
            if (tmodel != null && tmodel.getObservables().size() > 1) {
                for (int i = 1; i < tmodel.getObservables().size(); i++) {
                    IObservable oob = tmodel.getObservables().get(i);
                    OutputAction out = new OutputAction();
                    out.formalName = oob.getFormalName();
                    acc.notifyExpectedOutput(oob, oob.getObserver(), oob.getFormalName());
                    step.outputs.add(out);
                }
            }

        } catch (Exception e) {
            return onException(e);
        }

        /*
         * notify all inputs
         */
        for (DataPath d : this.incomingEdgesOf(step)) {

            ProcessingStep src = this.getEdgeSource(d);
            notify(src, computed, visualized);

            try {
                if (d.isConditional) {

                    ((ISwitchingActuator) acc).notifyConditionalAccessor(src.actuator, d.conditionIndex);

                    Cond cond = new Cond();
                    cond.target = src;
                    cond.index = d.conditionIndex;
                    cond.formalName = src.actuator.getName();

                    if (step.conditionals == null) {
                        step.conditionals = new ArrayList<Cond>();
                    }
                    step.conditionals.add(cond);

                } else {
                    acc.notifyDependency(d.observable, ((KIMObserver) src.observer)
                            .getRepresentativeObserver(), (d.formalName == null
                                    ? src.actuator.getName() : d.formalName), d.isMediation);
                }
            } catch (ThinklabException e) {
                return onException(e);
            }
        }

        /*
         * record all outputs needed by other steps
         */
        for (DataPath d : this.outgoingEdgesOf(step)) {

            OutputAction out = new OutputAction();
            out.register = d.register;
            out.formalName = step.actuator.getName();
            step.outputs.add(out);
        }

        /*
         * extract and store any explicit observables that we haven't already used as part of the model
         * dataflow. This can only happen if the accessor is a computing one.
         * 
         * FIXME/CHECK - is this ever called? See same logics above.
         */
        // if (step.observables != null) {
        // for (int i = 0; i < step.observables.size(); i++) {
        //
        // IObservable o = step.observables.get(i);
        //
        // OutputAction out = new OutputAction();
        // out.formalName = step.observableNames.get(i);
        // IObserver observer = step.observers.get(i);
        //
        // try {
        // ((IStateAccessor) (step.accessor)).notifyExpectedOutput(o,
        // getRepresentativeObserver(observer), out.formalName);
        // } catch (ThinklabException e) {
        // onException(e);
        // }
        //
        // step.outputs.add(out);
        // }
        // }

        /*
         * if we have conditionals, sort them in order of definition.
         */
        if (step.conditionals != null) {
            Collections.sort(step.conditionals, new Comparator<Cond>() {
                @Override
                public int compare(Cond o1, Cond o2) {
                    return o1.index - o2.index;
                }
            });
        }

        return true;
    }

    private void defineStates(ProcessingStep step) {

        // force storage if we only have this step.
        if (this.vertexSet().size() == 1 || _entryPoints.contains(step)) {
            step.stored = true;
        }

        if (step.stored) {

            IDataset ds = null;
            try {
                ds = ((Subject) _subject).requireBackingDataset();
            } catch (ThinklabException e) {
                _monitor.error("I/O error: cannot create backing dataset for subject " + _subject.getId());
                return;
            }

            /**
             * This is the one that we actually asked for in a dependency, not the one
             * that interprets whatever we linked it to. One problem is that for bare classifications,
             * it is an incomplete classification, so for now we patch it as seen below.
             */
            IObserver actualObserver = step.observers == null ? null : step.observers.get(0);

            /**
             * FIXME this should be set at resolution when the classifications are matched, and the
             * following should be unnecessary. Matching classifications should check the types and
             * when we grow up, mediate them.
             */
            if (actualObserver instanceof IClassifyingObserver
                    && ((IClassifyingObserver) actualObserver).getClassification() == null) {
                actualObserver = step.observer;
            }

            step.states = ((StateActuator) (step.actuator))
                    .createStates(_subject, step.model, actualObserver, _modelGraph, ds);
            if (step.states != null) {
                for (IState state : step.states) {
                    _states.add(state);
                    if (state.getStorage().isDynamic()) {
                        step.isDynamic = true;
                    }
                }
            }
        }
    }

    private boolean onException(Exception e) {
        _monitor.error(e);
        return false;
    }

    private boolean compute(ProcessingStep step, int contextIndex, ITransition transition, HashSet<ProcessingStep> computed) {

        if (computed.contains(step))
            return true;

        if (!_scale.isCovered(contextIndex)) {
            return true;
        }

        computed.add(step);

        /**
         * Steps that do not compute dynamic data will not be run at transitions. Data may still
         * change due to external action.
         */
        if (transition != null && !step.isDynamic) {
            return true;
        }

        ((IStateActuator) (step.actuator)).reset();

        /*
         * compute all non-conditional connections and retrieve the observable
         */
        for (DataPath in : this.incomingEdgesOf(step)) {

            if (in.isConditional)
                continue;

            if (compute(in.getSourceStep(), contextIndex, transition, computed)) {
                ((IStateActuator) (step.actuator)).setValue(in.formalName, _registers[in.register]);
            }
        }

        /*
         * process
         */
        try {
            if (step.isConditional) {

                /*
                 * ask accessor to compute each condition in order.
                 */
                ISwitchingActuator switcher = (ISwitchingActuator) step.actuator;
                String id = step.actuator.getName();
                boolean ok = false;
                Object value = null;
                for (Cond cond : step.conditionals) {

                    if (switcher.isNullCondition(cond.index)) {

                        /*
                         * compute the accessor, get the value and stop when it's not null. If so have the
                         * step update all other states and break the loop.
                         */
                        if (compute(cond.target, contextIndex, transition, computed)) {
                            value = ((IStateActuator) (cond.target.actuator)).getValue(cond.formalName);
                            ok = value != null;
                        }

                    } else if (switcher.getConditionValue(cond.index)) {
                        if (compute(cond.target, contextIndex, transition, computed)) {
                            value = ((IStateActuator) (cond.target.actuator)).getValue(cond.formalName);
                        }
                        ok = true;
                    }

                    if (ok) {

                        /*
                         * TODO properly handle side effects of the winning accessor.
                         */
                        switcher.setValue(id, value);
                        break;
                    }

                }

            } else {
                ((IStateActuator) (step.actuator)).process(contextIndex, null);
            }
        } catch (ThinklabException e) {
            return onException(e);
        }

        /*
         * do what we need to do with the output
         */
        for (OutputAction action : step.outputs) {

            Object out = ((IStateActuator) (step.actuator)).getValue(action.formalName);
            if (action.register >= 0) {
                _registers[action.register] = out;
            }
        }

        if (step.stored) {
            ((StateActuator) (step.actuator)).updateStates(contextIndex, transition);
        }

        return true;
    }

    public GraphVisualization visualize() {

        /*
         * FIXME add total coverage from _coverage
         */
        GraphVisualization ret = new GraphVisualization();
        ret.adapt(this, new GraphAdapter<ProcessingStep, DataPath>() {

            @Override
            public String getNodeType(ProcessingStep o) {
                return "accessor";
            }

            @Override
            public String getNodeId(ProcessingStep o) {
                return "" + o.hashCode();
            }

            @Override
            public String getNodeLabel(ProcessingStep o) {
                return o.actuator.toString();
            }

            @Override
            public String getNodeDescription(ProcessingStep o) {
                // TODO Auto-generated method stub
                return null;
            }

            @Override
            public String getEdgeType(DataPath o) {
                // TODO Auto-generated method stub
                return null;
            }

            @Override
            public String getEdgeId(DataPath o) {
                return "" + o.hashCode();
            }

            @Override
            public String getEdgeLabel(DataPath de) {
                return (de.observable == null ? "" : de.observable.getLocalName())
                        + (de.isConditional ? (" #" + de.conditionIndex) : "");
            }

            @Override
            public String getEdgeDescription(DataPath o) {
                // TODO Auto-generated method stub
                return null;
            }
        });

        return ret;
    }

    public List<ISubject> getGeneratedSubjects() {
        return _subjects;
    }
}
