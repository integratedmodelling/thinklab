/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.engine.modelling.runtime.mediators;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import org.integratedmodelling.api.knowledge.IConcept;
import org.integratedmodelling.api.metadata.IMetadata;
import org.integratedmodelling.api.modelling.IClassifyingObserver;
import org.integratedmodelling.api.modelling.INumericObserver;
import org.integratedmodelling.api.modelling.IObserver;
import org.integratedmodelling.api.modelling.IPresenceObserver;
import org.integratedmodelling.api.modelling.IState;
import org.integratedmodelling.collections.Pair;
import org.integratedmodelling.common.HashableObject;
import org.integratedmodelling.common.data.TableFactory.MaxAggregator;
import org.integratedmodelling.common.data.TableFactory.MeanAggregator;
import org.integratedmodelling.common.data.TableFactory.MinAggregator;
import org.integratedmodelling.common.data.TableFactory.SumAggregator;
import org.integratedmodelling.common.vocabulary.NS;

/**
 * Holds aggregation methods for all mediators to use.
 * 
 * @author ferdinando.villa
 *
 */
public abstract class AbstractMediator extends HashableObject implements IState.Mediator {

    private IObserver observer;
    private IConcept  dataReductionTrait;

    protected AbstractMediator(IObserver observer, IConcept dataReductionTrait) {
        this.observer = observer;
        this.dataReductionTrait = dataReductionTrait;
    }

    @Override
    public Object reduce(Collection<Pair<Object, Double>> toReduce, IMetadata metadata) {

        if (observer instanceof INumericObserver) {
            return reduceNumbers(toReduce, metadata);
        } else if (observer instanceof IClassifyingObserver) {
            return reduceConcepts(toReduce, metadata);
        } else if (observer instanceof IPresenceObserver) {
            return reduceBooleans(toReduce, metadata);
        }

        return null;
    }

    /*
     * CHECK - TODO: this mediator takes as true anything that mediates at least
     * one true value with a weight > 0. We should enable configuration with truth
     * values too.
     */
    private Object reduceBooleans(Collection<Pair<Object, Double>> toReduce, IMetadata metadata) {

        double truth = Double.NaN;
        int total = 0, tottrue = 0;
        boolean ret = false;
        for (Pair<Object, Double> p : toReduce) {
            if (p.getFirst() instanceof Boolean && ((Boolean) p.getFirst()) && p.getSecond() > 0) {
                ret = true;
                tottrue++;
            }
            total++;
        }

        metadata.put(SPACE_TOTAL_VALUES, total);
        metadata.put(SPACE_VALUE_SUM, tottrue);

        return ret;
        // return (truth / total) > .5;
    }

    /*
     * TODO
     * this one should produce a distribution, although I'm not sure the state upstream is prepared
     * to handle it.
     */
    private Object reduceConcepts(Collection<Pair<Object, Double>> toReduce, IMetadata metadata) {

        Map<IConcept, Integer> counts = new HashMap<>();

        return null;
    }

    private Object reduceNumbers(Collection<Pair<Object, Double>> toReduce, IMetadata metadata) {

        Aggregation agg = getAggregation();
        boolean extensive = agg == Aggregation.SUM;
        if (NS.synchronize() && dataReductionTrait != null) {
            if (dataReductionTrait.is(NS.AVERAGE_TRAIT)) {
                agg = Aggregation.AVERAGE;
                extensive = false;
            } else if (dataReductionTrait.is(NS.MAXIMUM_TRAIT)) {
                agg = Aggregation.MAX;
            } else if (dataReductionTrait.is(NS.MINIMUM_TRAIT)) {
                agg = Aggregation.MIN;
            }
        }

        ArrayList<Object> vals = new ArrayList<>();
        for (Pair<Object, Double> zio : toReduce) {
            if (zio.getFirst() instanceof Number) {
                double d = ((Number) zio.getFirst()).doubleValue();
                if (!Double.isNaN(d)) {
                    vals.add(extensive ? d * zio.getSecond() : d);
                }
            }
        }

        switch (agg) {
        case SUM:
            return new SumAggregator().aggregate(vals);
        case AVERAGE:
            return new MeanAggregator().aggregate(vals);
        case MIN:
            return new MinAggregator().aggregate(vals);
        case MAX:
            return new MaxAggregator().aggregate(vals);
        }

        return null;
    }
}
