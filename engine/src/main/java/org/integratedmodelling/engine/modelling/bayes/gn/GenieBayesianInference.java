/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.engine.modelling.bayes.gn;

import java.util.HashMap;
import java.util.Map;

import org.integratedmodelling.engine.modelling.bayes.IBayesianInference;
import org.integratedmodelling.exceptions.ThinklabException;

import smile.Network;

public class GenieBayesianInference implements IBayesianInference {

    Network network = null;

    public GenieBayesianInference(Network prototype) {
        this.network = prototype;
    }

    @Override
    public void run() {
        network.updateBeliefs();
    }

    @Override
    public void setEvidence(String node, String outcome) throws ThinklabException {
        this.network.setEvidence(node, outcome);
    }

    @Override
    public double getMarginal(String node, String outcome) {
        double ret = 0;
        double[] vals = network.getNodeValue(node);
        int i = 0;
        for (String s : network.getOutcomeIds(node)) {
            if (s.equals(outcome)) {
                ret = vals[i];
                break;
            }
            i++;
        }
        return ret;
    }

    @Override
    public Map<String, Double> getMarginals(String node) {

        HashMap<String, Double> result = new HashMap<String, Double>(network.getOutcomeCount(node));
        double[] vals = network.getNodeValue(node);
        int i = 0;
        for (String s : network.getOutcomeIds(node)) {
            result.put(s, vals[i++]);
        }
        return result;
    }

    @Override
    public void clearEvidence() {
        network.clearAllEvidence();
    }

    @Override
    public double[] getMarginalValues(String node) {
        return network.getNodeValue(node);
    }

}
