/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.engine.modelling.remote;

import java.util.HashMap;
import java.util.Map;

import org.integratedmodelling.api.modelling.ISubject;
import org.integratedmodelling.api.modelling.scheduling.ITransition;
import org.integratedmodelling.api.network.INode;
import org.integratedmodelling.api.services.IPrototype;
import org.integratedmodelling.common.command.ServiceManager;
import org.integratedmodelling.common.configuration.KLAB;
import org.integratedmodelling.common.model.actuators.BaseStateActuator;
import org.integratedmodelling.common.network.Node;
import org.integratedmodelling.exceptions.ThinklabException;

public class RemoteStateActuator extends BaseStateActuator {

    ISubject            context;
    IPrototype          prototype;
    Map<String, Object> parameters;
    private Map<?, ?>   result;
    private INode       node;

    public RemoteStateActuator(IPrototype prototype, Map<String, Object> parameters) {
        this.prototype = prototype;
        this.parameters = new HashMap<String, Object>(parameters);
        this.node = KLAB.NETWORK.getNodeProviding(prototype.getId());
    }

    @Override
    public void process(int stateIndex, ITransition transition) throws ThinklabException {
        // TODO Auto-generated method stub
        this.result = (Map<?, ?>) ((Node) node)
                .call(ServiceManager
                        .get()
                        .getStateAccessorCall(prototype, context, parameters, transition, stateIndex, monitor));

        /*
         * TODO set result from map
         */
    }

    @Override
    public void setValue(String inputKey, Object value) {

        /*
         * set keys/values into parameters
         * TODO callee must know how to call other expected outputs - override parent's func
         */
    }

    @Override
    public Object getValue(String outputKey) {
        if (outputKey.equals(getName())) {
            return result.get("result");
        }
        return result.get(outputKey);
    }

    @Override
    public void reset() {
        // TODO Auto-generated method stub

    }

}
