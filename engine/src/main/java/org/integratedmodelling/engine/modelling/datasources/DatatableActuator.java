/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.engine.modelling.datasources;

import java.util.ArrayList;

import org.integratedmodelling.api.data.IAggregator;
import org.integratedmodelling.api.data.IColumn;
import org.integratedmodelling.api.data.ITable;
import org.integratedmodelling.api.knowledge.IExpression;
import org.integratedmodelling.api.modelling.IAction;
import org.integratedmodelling.api.modelling.scheduling.ITransition;
import org.integratedmodelling.common.model.actuators.StateActuator;
import org.integratedmodelling.exceptions.ThinklabException;
import org.integratedmodelling.exceptions.ThinklabValidationException;

public class DatatableActuator extends StateActuator {

    ITable      _table;
    IExpression _rowSelector;
    IAggregator _operation;
    IColumn     _column;
    Object      _value;

    public DatatableActuator(ArrayList<IAction> actions, ITable table, IExpression rowSelector,
            IAggregator operation, String column) throws ThinklabValidationException {

        super(actions, null);
        _table = table;
        _column = table.getColumn(column);
        _rowSelector = rowSelector;
        _operation = operation;
        if (_column == null) {
            throw new ThinklabValidationException("table column " + column + " does not exist");
        }
    }

    @Override
    public void processState(int stateIndex, ITransition transition) throws ThinklabException {

        /*
         * scan rows; if there is a selector, filter them through it; keep IDs of valid rows.
         * Use copy of _parameters plus all values of columns at each point.
         */
        if (_rowSelector == null) {
            // aggregation of all rows. Table is read-only so we only need to do this
            // once.
            if (_value == null) {
                _value = _operation.aggregate(_column.getValues());
            }
        } else {
            /*
             * apply aggregator to rows that match the filter.
             */
            if (_value == null || _parameters.size() > 0) {
                _value = _operation
                        .aggregate(_table.lookup(_column.getName(), _rowSelector, _parameters, getMonitor()));
            }
        }

        /*
         * set value to resulting aggregation
         */
        for (String s : getOutputKeys()) {
            setValue(s, _value);
        }

//        super.process(stateIndex, transition);
    }

}
