/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.engine.modelling;

import org.integratedmodelling.api.modelling.agents.IObservationController;
import org.integratedmodelling.api.modelling.agents.IObservationTask;
import org.integratedmodelling.api.modelling.agents.IObservationWorker;
import org.integratedmodelling.api.modelling.scheduling.ITransition;
import org.integratedmodelling.api.monitoring.IMonitor;
import org.integratedmodelling.api.monitoring.Messages;
import org.integratedmodelling.api.runtime.IContext;
import org.integratedmodelling.api.runtime.ISession;
import org.integratedmodelling.common.monitoring.Notification;
import org.integratedmodelling.engine.introspection.CallTracer;
import org.integratedmodelling.engine.runtime.Context;
import org.integratedmodelling.exceptions.ThinklabException;

/**
 * A worker node which performs the tasks of observing agent-state intervals.
 *
 * Agents progress through time in a linear fashion, and their dependencies all point into the immediate past.
 * This will generate agent states for given time intervals based on these dependencies, which then allow
 * further observations to be made.
 *
 * @author luke
 *
 */
public class ObservationWorker implements IObservationWorker {

    private final IObservationController controller;
    private final IMonitor               monitor;
    private final ISession               session;
    private final long                   taskId;
    private final IContext               context;

    public ObservationWorker(IObservationController controller, IMonitor monitor, ISession session,
            IContext context,
            long taskId) {
        this.controller = controller;
        this.monitor = monitor;
        this.session = session;
        this.taskId = taskId;
        this.context = context;
    }

    @Override
    public void run() throws ThinklabException {

        CallTracer.indent("run()", this);

        IObservationTask task = controller.getNext();

        while (task != null) {

            ITransition result = task.run();

            // null result will happen when ...well... nothing happens.
            // (like a collision detection task that doesn't detect anything)
            if (result != null) {
                controller.setResult(task, result);
                if (result.getAgentState() != null && result.getAgentState().getTimePeriod() != null) {

                    /*
                     * Only report transitions affecting the primary subject.
                     * TODO this may need to scan a list of monitored
                     * subjects so we can implement breakpoints and the like.
                     */
                    if (task.getSubject().equals(context.getSubject())) {
                        monitor.send(new Notification(taskId, Messages.TIME_TRANSITION, session, context,
                                result.getTime().getStart().getMillis(),
                                result.getTime().getEnd().getMillis(),
                                result.getTimeIndex()));
                    }

                    /*
                     * check breakpoints for subject and pause if the transition has modified any of
                     * them.
                     */
                    if (((Context) context).breakpointReached(result.getModifiedObservations())) {
                        ((Context) context).waitForResume();
                    }

                }
            }
            task = controller.getNext();
        }

        /*
         * remove all breakpoints for this subject.
         */
        CallTracer.unIndent();
    }
}
