/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.engine.modelling.datasources;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import org.integratedmodelling.api.metadata.IMetadata;
import org.integratedmodelling.api.modelling.IAction;
import org.integratedmodelling.api.modelling.IObserver;
import org.integratedmodelling.api.modelling.IScale;
import org.integratedmodelling.api.modelling.runtime.IActiveDataSource;
import org.integratedmodelling.api.modelling.runtime.IActuator;
import org.integratedmodelling.api.modelling.scheduling.ITransition;
import org.integratedmodelling.api.monitoring.IMonitor;
import org.integratedmodelling.collections.NumericInterval;
import org.integratedmodelling.common.metadata.Metadata;
import org.integratedmodelling.common.model.actuators.StateActuator;
import org.integratedmodelling.engine.modelling.runtime.Scale;
import org.integratedmodelling.exceptions.ThinklabException;
import org.integratedmodelling.exceptions.ThinklabRuntimeException;

/**
 * A datasource that returns the same object no matter what. Also works as an accessor so it can be parameterized
 * appropriately.
 * 
 * @author Ferd
 *
 */
public class RandomSelectDSActuator extends StateActuator implements IActiveDataSource {

    // private DistributionValue _state = null;
    private IMetadata _metadata = new Metadata();
    private Object[]  _values;
    private Object[]  _distribution;
    Random            _rand     = new Random();

    boolean isAccessor = false;

    public RandomSelectDSActuator(List<IAction> actions, IMonitor monitor, long seed, Object[] values,
            Object[] dist) {
        super(actions, monitor);
        _values = values;
        _distribution = dist;
        isAccessor = true;
        if (seed != 0) {
            _rand.setSeed(seed);
        }
    }

    public RandomSelectDSActuator(long seed, Object[] values, Object[] dist) {
        super(new ArrayList<IAction>(), null);
        _values = values;
        _distribution = dist;
        if (seed != 0) {
            _rand.setSeed(seed);
        }
    }

    @Override
    public void processState(int stateIndex, ITransition transition) throws ThinklabException {

        for (String s : getOutputKeys()) {
            setValue(s, draw());
        }
        super.processState(stateIndex, transition);
    }

    @Override
    public Object getValue(String outputKey) {
        return _parameters.containsKey(outputKey) ? _parameters.get(outputKey) : draw();
    }

    private Object draw() {

        double[] dd = new double[_values.length];
        double sum = 0.0;

        for (int i = 0; i < _values.length; i++) {
            double d = 0.0;
            if (!(_distribution[i] instanceof Number)) {
                Object o = this._parameters.get(_distribution[i].toString());
                if (!(o instanceof Number)) {
                    RuntimeException e = new ThinklabRuntimeException("cannot compute value of "
                            + _distribution[i] + ": " + o + " is not a number");
                    if (monitor != null) {
                        monitor.error(e);
                    }
                    throw e;
                }
                d = ((Number) o).doubleValue();
                if (Double.isNaN(d)) {
                    return null;
                }
            } else {
                d = ((Number) _distribution[i]).doubleValue();
            }

            sum += d;
            dd[i] = sum;
        }

        double thr = _rand.nextDouble() * sum;
        for (int i = 0; i < _values.length; i++) {
            if (thr <= dd[i]) {
                return processValue(_values[i]);
            }
        }

        return null;
    }

    private Object processValue(Object object) {

        if (object instanceof NumericInterval) {
            // choose randomly
            NumericInterval ni = (NumericInterval) object;
            object = ni.getMinimumValue()
                    + (_rand.nextDouble() * (ni.getMaximumValue() - ni.getMinimumValue()));
        }
        return object;
    }

    @Override
    public IActuator getAccessor(IScale context, IObserver observer, IMonitor monitor)
            throws ThinklabException {
        return this;
    }

    @Override
    public IMetadata getMetadata() {
        return _metadata;
    }

    @Override
    public IScale getCoverage() {
        return new Scale();
    }

    @Override
    public String toString() {
        /*
         * FIXME do better
         */
        return "[random.select]";
    }

    @Override
    public boolean isAvailable() {
        return true;
    }
}
