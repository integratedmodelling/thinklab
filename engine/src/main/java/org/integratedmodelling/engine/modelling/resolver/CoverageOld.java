/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
// package org.integratedmodelling.engine.modelling.resolver;
//
// import java.util.ArrayList;
//
// import org.integratedmodelling.api.knowledge.IProperty;
// import org.integratedmodelling.api.modelling.ICoverage;
// import org.integratedmodelling.api.modelling.IExtent;
// import org.integratedmodelling.api.modelling.IScale;
// import org.integratedmodelling.api.modelling.ITopologicallyComparable;
// import org.integratedmodelling.collections.Pair;
// import org.integratedmodelling.collections.Triple;
// import org.integratedmodelling.engine.modelling.runtime.Scale;
// import org.integratedmodelling.exceptions.ThinklabException;
// import org.integratedmodelling.exceptions.ThinklabRuntimeException;
//
// @Deprecated
// public class CoverageOld implements ICoverage {
//
// /*
// * do not accept a model unless its coverage is greater than this.
// */
// private static double MIN_MODEL_COVERAGE = 0.01;
//
// /*
// * default: we accept models if they cover at least an additional 20% of the whole context
// */
// private static double MIN_TOTAL_COVERAGE = 0.20;
//
// /*
// * default: we stop adding models when we cover at least 95% of the whole context.
// */
// private static double MIN_REQUIRED_COVERAGE = 0.95;
//
// IScale _scale;
// double _coverage = 0.0;
//
// public static final CoverageOld EMPTY = new CoverageOld(null, 0.0);
//
// public static ICoverage FULL(IScale scale) {
// return new CoverageOld(scale, 1.0);
// }
//
// /*
// * the extents are initialized from the scale and change by either setting the
// * coverage's metadata or by and/or-ing with another coverage.
// */
// ArrayList<Triple<IExtent, ITopologicallyComparable<?>, Double>> _current = new ArrayList<Triple<IExtent,
// ITopologicallyComparable<?>, Double>>();
//
// /**
// * Create coverage initialized at 0
// * @param scale
// */
// public CoverageOld(IScale scale) {
// this(scale, 0.0);
// }
//
// @Override
// public boolean isEmpty() {
// return _coverage < MIN_MODEL_COVERAGE;
// }
//
// @Override
// public boolean isRelevant() {
// return _coverage > MIN_TOTAL_COVERAGE;
// }
//
// @Override
// public boolean isComplete() {
// return _coverage >= MIN_REQUIRED_COVERAGE;
// }
//
// public static void setMinimumModelCoverage(double d) {
// MIN_TOTAL_COVERAGE = d;
// }
//
// public static void setMinimumTotalCoverage(double d) {
// MIN_MODEL_COVERAGE = d;
// }
//
// public static void setSufficientTotalCoverage(double d) {
// MIN_REQUIRED_COVERAGE = d;
// }
//
// /**
// * Create the coverage of scale 'toCover' created by adding scale
// * 'covering'.
// *
// * @param toCover
// * @param covering
// * @throws ThinklabException
// */
// public CoverageOld(IScale toCover, IScale covering) throws ThinklabException {
// this(toCover);
// double co = 1.0;
// for (Triple<IExtent, ITopologicallyComparable<?>, Double> tc : _current) {
// IExtent its = ((Scale) covering).getExtent(tc.getFirst().getDomainConcept());
// if (its != null) {
// Pair<ITopologicallyComparable<?>, Double> topo = tc.getFirst().checkCoverage(its);
// tc.setSecond(topo.getFirst());
// tc.setThird(topo.getSecond());
// if (topo.getSecond() < co)
// co = topo.getSecond();
// }
// }
// _coverage = co;
//
// }
//
// /**
// * Create coverage with predefined value
// * @param scale
// * @param coverage
// */
// public CoverageOld(IScale scale, double coverage) {
// try {
// _scale = scale == null ? null : Scale.sanitize(scale);
// } catch (ThinklabException e1) {
// throw new ThinklabRuntimeException(e1.getMessage());
// }
// if (_scale != null) {
// for (IExtent e : _scale) {
// _current.add(new Triple<IExtent, ITopologicallyComparable<?>, Double>(e, e
// .getExtent(), coverage));
// }
// }
// _coverage = coverage;
// }
//
// public CoverageOld(ArrayList<Triple<IExtent, ITopologicallyComparable<?>, Double>> sav) {
// _current = sav;
// for (Triple<IExtent, ITopologicallyComparable<?>, Double> tc : _current) {
// if (tc.getThird() > _coverage) {
// _coverage = tc.getThird();
// }
// }
// }
//
// private CoverageOld(CoverageOld coverage) {
//
// for (Triple<IExtent, ITopologicallyComparable<?>, Double> tc : coverage._current) {
// _current.add(new Triple<IExtent, ITopologicallyComparable<?>, Double>(tc));
// }
// _coverage = coverage._coverage;
// _scale = coverage._scale;
// }
//
// /*
// * AND the coverage with the passed one. If there is no difference in the result,
// * return this; otherwise return a new coverage.
// */
// @Override
// public CoverageOld and(ICoverage coverage) throws ThinklabException {
//
// if (coverage == null || coverage.getCoverage().equals(0)) {
//
// /*
// * empty coverage
// */
// return new CoverageOld(_scale);
//
// } else {
//
// /*
// * work on a copy
// */
// ArrayList<Triple<IExtent, ITopologicallyComparable<?>, Double>> sav = new ArrayList<Triple<IExtent,
// ITopologicallyComparable<?>, Double>>();
// for (Triple<IExtent, ITopologicallyComparable<?>, Double> tc : _current) {
// sav.add(new Triple<IExtent, ITopologicallyComparable<?>, Double>(tc));
// }
// if (andCoverage(((CoverageOld) coverage)._current, sav) != 0)
// return new CoverageOld(sav);
// }
//
// return this;
//
// }
//
// /*
// * OR the coverage with the passed one, only accepting the passed one as a contributor if
// * it determines a net improvement in coverage of at least minAcceptedImprovement. If the
// * coverage is not accepted, return this (getCoverage() will return the same value); otherwise
// * return a new coverage.
// */
// public CoverageOld or(ICoverage coverage, double minIncrementAccepted) throws ThinklabException {
//
// if (coverage != null) {
// if (((CoverageOld) coverage)._coverage == 1.0) {
// return new CoverageOld((CoverageOld) coverage);
// } else {
//
// /*
// * work on a copy
// */
// ArrayList<Triple<IExtent, ITopologicallyComparable<?>, Double>> sav = new ArrayList<Triple<IExtent,
// ITopologicallyComparable<?>, Double>>();
// for (Triple<IExtent, ITopologicallyComparable<?>, Double> tc : _current) {
// sav.add(new Triple<IExtent, ITopologicallyComparable<?>, Double>(tc));
// }
// double cov = orCoverage(((CoverageOld) coverage)._current, sav);
// if (cov >= minIncrementAccepted) {
// return new CoverageOld(sav);
// }
// }
//
// }
//
// return this;
// }
//
// /*
// * OR the coverage with the passed one, only accepting the passed one as a contributor if
// * it determines a net improvement in coverage of at least minAcceptedImprovement. If the
// * coverage is not accepted, return this (getCoverage() will return the same value); otherwise
// * return a new coverage.
// */
// public CoverageOld orIfRelevant(ICoverage coverage) throws ThinklabException {
// return or(coverage, MIN_TOTAL_COVERAGE);
// }
//
// /*
// * OR the coverage with the passed one, only accepting the passed one as a contributor if
// * it determines a net improvement in coverage of at least minAcceptedImprovement. If the
// * coverage is not accepted, return this (getCoverage() will return the same value); otherwise
// * return a new coverage.
// */
// @Override
// public CoverageOld or(ICoverage coverage) throws ThinklabException {
// return or(coverage, 0);
// }
//
// private double andCoverage(ArrayList<Triple<IExtent, ITopologicallyComparable<?>, Double>> md,
// ArrayList<Triple<IExtent, ITopologicallyComparable<?>, Double>> coverage)
// throws ThinklabException {
//
// double best = 0.0;
//
// for (int i = 0; i < coverage.size(); i++) {
//
// Triple<IExtent, ITopologicallyComparable<?>, Double> tc = coverage.get(i);
// IProperty p = tc.getFirst().getCoverageProperty();
// ITopologicallyComparable<?> cov = null;
// for (Triple<IExtent, ITopologicallyComparable<?>, Double> oc : md) {
// if (oc.getFirst().getCoverageProperty().equals(p)) {
// cov = oc.getSecond();
// }
// }
//
// /*
// * If the model does not have the extent or it's empty, no cookie anyway.
// */
// if (cov == null) {
// coverage.get(i).setSecond(null);
// coverage.get(i).setThird(0.0);
// } else {
//
// /*
// * if we had no previous value, the AND can only return nothing.
// */
// ITopologicallyComparable<?> prev = tc.getSecond();
// if (prev != null) {
//
// Pair<ITopologicallyComparable<?>, Double> newcov = tc.getFirst().checkCoverage(cov);
// if (newcov.getSecond() > 0.0) {
//
// /*
// * intersect them
// */
// ITopologicallyComparable<?> ncov = cov.intersection(newcov.getFirst());
//
// double c1 = ncov.getCoveredExtent();
// double c2 = newcov.getFirst().getCoveredExtent();
// double change = c1 / c2;
//
// coverage.get(i).setSecond(ncov);
// coverage.get(i).setThird(change);
//
// if (best < change)
// best = change;
//
// } else {
// coverage.get(i).setSecond(null);
// coverage.get(i).setThird(0.0);
// }
// }
// }
// }
//
// /*
// * return highest change among all extents.
// */
// return best;
//
// }
//
// Triple<IExtent, ITopologicallyComparable<?>, Double> getRecord(IExtent ext) {
// for (Triple<IExtent, ITopologicallyComparable<?>, Double> tc : _current) {
// if (tc.getFirst().getCoverageProperty().equals(ext.getCoverageProperty())) {
// return tc;
// }
// }
// return null;
// }
//
// /*
// * add the metadata to the extents in coverage using the passed logical connector (AND or OR). Return the
// * highest coverage change obtained in the process.
// *
// */
// private double orCoverage(ArrayList<Triple<IExtent, ITopologicallyComparable<?>, Double>> md,
// ArrayList<Triple<IExtent, ITopologicallyComparable<?>, Double>> coverage)
// throws ThinklabException {
//
// double improvement = 0.0;
// double best = 0.0;
//
// for (int i = 0; i < coverage.size(); i++) {
//
// Triple<IExtent, ITopologicallyComparable<?>, Double> tc = coverage.get(i);
// IProperty p = tc.getFirst().getCoverageProperty();
//
// ITopologicallyComparable<?> cov = null;
// for (Triple<IExtent, ITopologicallyComparable<?>, Double> oc : md)
// if (oc.getFirst().getCoverageProperty().equals(p))
// cov = oc.getSecond();
//
// /*
// * If the model does not have the extent, leave coverage at 1.
// * This is a second choice model that with the search strategy in ModelQuery should only
// * show up here if no models with all extents were found.
// */
// if (cov != null) {
//
// /*
// * if we had a previous value, merge it with the current and try that;
// * otherwise use the new one alone
// */
// ITopologicallyComparable<?> prev = tc.getSecond();
// if (prev != null) {
// cov = prev.union(cov);
// }
// Pair<ITopologicallyComparable<?>, Double> newcov = tc.getFirst().checkCoverage(cov);
// improvement = newcov.getSecond() - tc.getThird();
// cov = newcov.getFirst();
//
// /*
// * if enough improvement, make new extents current
// */
// if (improvement != 0) {
// coverage.get(i).setSecond(cov);
// coverage.get(i).setThird(newcov.getSecond());
//
// if (best < improvement)
// best = improvement;
// }
// }
// }
//
// /*
// * return highest change among all extents.
// */
// return best;
// }
//
// @Override
// public Double getCoverage() {
// return _coverage;
// }
//
// @Override
// public String toString() {
// return "coverage (" + _current.size() + " ext) = " + _coverage;
// }
//
// }
