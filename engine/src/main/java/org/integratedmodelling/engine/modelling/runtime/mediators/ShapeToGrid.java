/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.engine.modelling.runtime.mediators;

import java.util.Collection;
import java.util.List;

import org.integratedmodelling.api.metadata.IMetadata;
import org.integratedmodelling.api.modelling.IExtent;
import org.integratedmodelling.api.modelling.IObservable;
import org.integratedmodelling.api.modelling.IScale.Locator;
import org.integratedmodelling.api.modelling.IState;
import org.integratedmodelling.api.space.ISpatialExtent;
import org.integratedmodelling.collections.Pair;
import org.integratedmodelling.engine.geospace.extents.SpaceExtent;

public class ShapeToGrid implements IState.Mediator {

    public ShapeToGrid(IExtent from, IExtent to) {
        // TODO Auto-generated constructor stub
    }

    public ShapeToGrid(IObservable observable, ISpatialExtent asExtent, SpaceExtent spaceExtent) {
        // TODO Auto-generated constructor stub
    }

    @Override
    public Object mediateTo(Object value, int index) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public Aggregation getAggregation() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public Object mediateFrom(IState originalState, Locator... otherLocators) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public List<Locator> getLocators(int index) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public Object reduce(Collection<Pair<Object, Double>> toReduce, IMetadata metadata) {
        // TODO Auto-generated method stub
        return null;
    }

}
