/*******************************************************************************
 *  Copyright (C) 2007, 2014:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.engine.modelling.random;

import umontreal.iro.lecuyer.probdist.Distribution;
import umontreal.iro.lecuyer.probdist.DistributionFactory;
import umontreal.iro.lecuyer.randvar.RandomVariateGen;
import umontreal.iro.lecuyer.rng.LFSR113;
import umontreal.iro.lecuyer.rng.RandomStream;

/**
 * Easy to use distribution for random value extraction. Use with one of the provided string 
 * constants. See <a href="http://statistik.wu-wien.ac.at/unuran/doc/unuran.html#KeysDistr">here</a> 
 * for details on the initialization parameters.
 * 
 * Proxies an SSJ object quite literally.
 * 
 * @author Ferd
 *
 */
public class DistributionValue {

    /*
     * TODO finish distributions
     */
    RandomStream stream = new LFSR113();

    //	public final static String  ANDERSON_DARLING = "";
    public final static String BETA = "beta";
    //	public final static String	BETA_SYMMETRIC = "";
    public final static String CAUCHY = "cauchy";
    public final static String CHI = "chi";
    public final static String CHI_SQUARE = "chisquare";
    //	public final static String	CRAMER_VONMISES = "";
    //	public final static String	ERLANG = "";
    public final static String EXPONENTIAL = "exponential";
    //	public final static String	EXTREME_VALUE = "";
    //	public final static String	FATIGUE_LIFE = "";
    public final static String FISHER_F = "F";
    //	public final static String	FOLDED_NORMAL = "";
    public final static String GAMMA = "gamma";
    public final static String HALF_NORMAL = "";
    //	public final static String	HYPERBOLIC_SECANT = "";
    //	public final static String	INVERSE_GAUSSIAN = "";
    //	public final static String	KOLMOGOROV_SMIRNOV = "";
    public final static String LAPLACE = "laplace";
    public final static String LOGISTIC = "logistic";
    //	public final static String	LOG_LOGISTIC = "";
    public final static String LOG_NORMAL = "lognormal";
    //	public final static String	NAKAGAMI = "";
    public final static String NORMAL = "normal";
    //	public final static String	NORMAL_INVERSE = "";
    public final static String PARETO = "pareto";
    //	public final static String	PASCAL = "";
    //	public final static String	PEARSON5 = "";
    //	public final static String	PEARSON6 = "";
    //	public final static String	PIECEWISE_LINEAR_EMPIRICAL = "";
    public final static String POWER = "powerexponential";
    public final static String RAYLEIGHT = "rayleigh";
    public final static String STUDENT = "student";
    public final static String TRIANGULAR = "triangular";
    //	public final static String	TRUNCATED = "";
    public final static String UNIFORM = "uniform";
    //	public final static String	WATSON_G = "";
    //	public final static String	WATSON_U = "";
    public final static String WEIBULL = "weibull";
    public final static String BINOMIAL = "binomial";
    public final static String GEOMETRIC = "geometric";
    public final static String HYPERGEOMETRIC = "hypergeometric";
    public final static String LOGARITHMIC = "logarithmic";
    public final static String NEGATIVE_BINOMIAL = "negativebiniomal";
    public final static String POISSON = "poisson";

    private Distribution distribution = null;
    private RandomVariateGen genN;
    private String name;

    public DistributionValue(String distribution, double... parameters) {

        String dp = "";
        for (double d : parameters) {
            dp += (dp.isEmpty() ? "" : ", ") + d;
        }
        this.name = distribution + "(" + dp + ")";
        this.distribution = DistributionFactory.getDistribution(name);
        this.genN = new RandomVariateGen(stream, this.distribution);
    }

    public double draw() {
        return genN.nextDouble();
    }

    public Distribution getDistribution() {
        return distribution;
    }

    public double getMean() {
        return distribution.getMean();
    }

    public double getStandardDeviation() {
        return distribution.getStandardDeviation();
    }

    public double getVariance() {
        return distribution.getVariance();
    }

    public String getName() {
        return name;
    }

    @Override
    public String toString() {
        return distribution.toString();
    }

}
