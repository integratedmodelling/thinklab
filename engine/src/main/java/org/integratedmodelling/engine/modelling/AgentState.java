/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.engine.modelling;

import java.util.Map;

import org.integratedmodelling.api.knowledge.IProperty;
import org.integratedmodelling.api.modelling.IDirectActiveObservation;
import org.integratedmodelling.api.modelling.IState;
import org.integratedmodelling.api.modelling.agents.IAgentState;
import org.integratedmodelling.api.time.ITimeInstant;
import org.integratedmodelling.api.time.ITimePeriod;
import org.integratedmodelling.engine.time.literals.PeriodValue;
import org.integratedmodelling.exceptions.ThinklabException;

/**
 * a temporary class to describe what is being called "agent-state" in the multiple-scale paper. It represents
 * a state (which can be a constant value, temporal function, or probability over either) valid for some time
 * period.
 *
 * @author luke
 *
 */
public class AgentState implements IAgentState {
    private final IDirectActiveObservation subject;
    private final ITimePeriod              timePeriod;
    private final Map<IProperty, IState>   states;

    public AgentState(IDirectActiveObservation subject, ITimePeriod timePeriod,
            Map<IProperty, IState> states) {
        this.subject = subject;
        this.timePeriod = timePeriod;
        this.states = states;
    }

    @Override
    public IDirectActiveObservation getSubject() {
        return subject;
    }

    @Override
    public ITimePeriod getTimePeriod() {
        return timePeriod;
    }

    @Override
    public Map<IProperty, IState> getStates() {
        return states;
    }

    /**
     * see comments on IAgentState.terminateEarly()
     * @throws ThinklabException
     */
    @Override
    public IAgentState terminateEarly(ITimeInstant interruptTime) throws ThinklabException {
        ITimePeriod shorterTimePeriod = new PeriodValue(timePeriod.getStart().getMillis(),
                interruptTime.getMillis());
        AgentState result = new AgentState(subject, shorterTimePeriod, states);
        return result;
    }

    @Override
    public String toString() {
        return "AgentState[" + subject.toString() + " at " + timePeriod.toString() + "]";
    }
}
