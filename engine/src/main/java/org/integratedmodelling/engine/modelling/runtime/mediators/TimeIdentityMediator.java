/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.engine.modelling.runtime.mediators;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.integratedmodelling.api.metadata.IMetadata;
import org.integratedmodelling.api.modelling.IScale.Locator;
import org.integratedmodelling.api.modelling.IState;
import org.integratedmodelling.collections.Pair;
import org.integratedmodelling.common.time.TimeLocator;

public class TimeIdentityMediator implements IState.Mediator {

    @Override
    public Object mediateTo(Object value, int index) {
        return value;
    }

    @Override
    public Aggregation getAggregation() {
        return Aggregation.NONE;
    }

    @Override
    public Object mediateFrom(IState originalState, Locator... locators) {
        return originalState.getValue(originalState.getScale().getIndex(locators).iterator().next());
    }

    @Override
    public List<Locator> getLocators(int index) {
        List<Locator> ret = new ArrayList<>();
        ret.add(TimeLocator.get(index));
        return ret;
    }

    @Override
    public Object reduce(Collection<Pair<Object, Double>> toReduce, IMetadata metadata) {
        return toReduce.iterator().next().getFirst();
    }
}
