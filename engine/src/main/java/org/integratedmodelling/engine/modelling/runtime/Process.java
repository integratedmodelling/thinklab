/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.engine.modelling.runtime;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import org.integratedmodelling.api.knowledge.IProperty;
import org.integratedmodelling.api.metadata.IMetadata;
import org.integratedmodelling.api.modelling.IActiveProcess;
import org.integratedmodelling.api.modelling.IDirectObservation;
import org.integratedmodelling.api.modelling.IModel;
import org.integratedmodelling.api.modelling.IObservable;
import org.integratedmodelling.api.modelling.IState;
import org.integratedmodelling.api.modelling.runtime.IProcessActuator;
import org.integratedmodelling.api.modelling.scheduling.ITransition;
import org.integratedmodelling.api.monitoring.IMonitor;
import org.integratedmodelling.common.model.runtime.Transition;
import org.integratedmodelling.common.utils.CamelCase;
import org.integratedmodelling.exceptions.ThinklabException;

public class Process extends DirectObservation implements IActiveProcess {

    IProcessActuator   _actuator;
    IModel             _model;
    IDirectObservation _subject;
    String             _name;

    public Process(IModel model, IDirectObservation subject, IProcessActuator accessor, IMonitor monitor) {
        /*
         * TODO processes must have their own scale eventually.
         */
        super(model.getObservable(), subject.getScale(), model.getNamespace(), subject.getId() + "_"
                + model.getObservable().getLocalName(), monitor);
        _model = model;
        _actuator = accessor;
        _subject = subject;
        _name = CamelCase.toLowerCase(getObservable().getLocalName(), '-');
    }

    @Override
    public IDirectObservation getSubject() {
        return _subject;
    }

    public IModel getModel() {
        return _model;
    }

    @Override
    public Map<IProperty, IState> getObjectStateCopy() {
        return new HashMap<IProperty, IState>();
    }

    @Override
    public ITransition performTemporalTransitionFromCurrentState() throws ThinklabException {

        ITransition result;

        // set the clock forward
        if (moveToNextTimePeriod() == null) {
            // process has terminated
            result = new Transition(scale, null, false);
        } else {

            result = reEvaluateStates(getCurrentTimePeriod());
            if (result.agentSurvives() && _actuator != null) {
                /*
                 * ensure the subject that contains the states we see has been updated to
                 * receive a new transition. This may be called multiple times for the same
                 * transition (by all direct observations inherent to this subject).
                 */
                ((Subject) _subject).prepareForTransition(result);
                _actuator.processTransition(result, monitor);
            }

        }
        return result;
    }

    @Override
    public String toString() {
        return "<process " + observable + ">";
    }

    @Override
    public IMetadata getMetadata() {
        return metadata;
    }

    @Override
    public String getId() {
        return _name;
    }

    /**
     * use with great caution. Only for reinterpretation.
     * @param observable
     */
    public void setObservable(IObservable observable) {
        this.observable = observable;
    }

    @Override
    public Collection<IState> getStates() {
        // TODO Auto-generated method stub
        return null;
    }

}
