/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.engine.modelling.resolver;

import java.util.ArrayList;
import java.util.List;

import org.integratedmodelling.api.knowledge.IConcept;
import org.integratedmodelling.api.modelling.ICoverage;
import org.integratedmodelling.api.modelling.IExtent;
import org.integratedmodelling.api.modelling.IScale;
import org.integratedmodelling.api.modelling.ITopologicallyComparable;
import org.integratedmodelling.exceptions.ThinklabException;
import org.integratedmodelling.exceptions.ThinklabRuntimeException;

public class Coverage implements ICoverage {

    /*
     * do not accept a model unless its coverage is greater than this.
     */
    private static double MIN_MODEL_COVERAGE = 0.01;

    /*
     * default: we accept models if they cover at least an additional 20% of the whole context
     */
    private static double MIN_TOTAL_COVERAGE = 0.20;

    /*
     * default: we stop adding models when we cover at least 95% of the whole context.
     */
    private static double MIN_REQUIRED_COVERAGE = 0.95;

    public static final Coverage EMPTY = new Coverage(null, 0.0);

    public static ICoverage FULL(IScale scale) {
        return new Coverage(scale, 1.0);
    }

    class CExt {
        IConcept                    domain;
        ITopologicallyComparable<?> original;
        ITopologicallyComparable<?> current;
        double                      coverage;

        CExt(IConcept domain, ITopologicallyComparable<?> original, ITopologicallyComparable<?> current,
                double coverage) {
            this.domain = domain;
            this.original = original;
            this.current = current;
            this.coverage = coverage;
        }
    }

    /*
     * For each extent of the original scale this contains:
     * 1. the original, unmodified extent which was defined as covered or uncovered at the
     *    constructor;
     * 2. the currently covered extent;
     * 3. the proportion of coverage for this extent combination.
     *
     * The total coverage is the product of all coverages.
     */
    List<CExt> current  = new ArrayList<>();
    double     coverage = 0.0;

    /**
     * Create coverage initialized at 1
     * @param scale
     */
    public Coverage(IScale scale) {
        this(scale, 1.0);
    }

    @Override
    public boolean isEmpty() {
        return coverage < MIN_MODEL_COVERAGE;
    }

    @Override
    public boolean isRelevant() {
        return coverage > MIN_TOTAL_COVERAGE;
    }

    @Override
    public boolean isComplete() {
        return coverage >= MIN_REQUIRED_COVERAGE;
    }

    public static void setMinimumModelCoverage(double d) {
        MIN_TOTAL_COVERAGE = d;
    }

    public static void setMinimumTotalCoverage(double d) {
        MIN_MODEL_COVERAGE = d;
    }

    public static void setSufficientTotalCoverage(double d) {
        MIN_REQUIRED_COVERAGE = d;
    }

    /**
     * Create coverage with predefined value (either 0 or 1). 
     * 
     * @param scale
     * @param coverage
     */
    public Coverage(IScale scale, double coverage) {
        if (coverage != 0 && coverage != 1) {
            throw new ThinklabRuntimeException("API misuse: coverage can only be initially fully covered or uncovered");
        }
        this.coverage = coverage;
        if (scale != null) {
            for (IExtent e : scale) {
                ITopologicallyComparable<?> orig = e.getExtent();
                ITopologicallyComparable<?> curr = coverage > 0 ? orig : null;
                current.add(new CExt(e.getDomainConcept(), orig, curr, coverage));
            }
        }
    }

    private Coverage() {
    }

    @Override
    public String toString() {
        return "coverage (" + current.size() + " ext) = " + coverage;
    }

    @Override
    public Double getCoverage() {
        return coverage;
    }

    @Override
    public ICoverage or(ICoverage coverage) throws ThinklabException {

        Coverage ret = new Coverage();
        ret.coverage = 1.0;

        for (CExt my : current) {
            for (CExt his : ((Coverage) coverage).current) {
                if (his.domain.equals(my.domain)) {
                    /*
                     * recompute current using the other's 
                     */
                    ITopologicallyComparable<?> other = his.current == null ? his.original : his.current;
                    ITopologicallyComparable<?> current = my.current.union(other);
                    double ncoverage = current.getCoveredExtent() / my.original.getCoveredExtent();

                    /*
                     * only add it if the increment is enough to justify
                     */
                    if ((ncoverage - my.coverage) >= MIN_REQUIRED_COVERAGE) {
                        ret.coverage *= ncoverage;
                        ret.current.add(new CExt(my.domain, my.original, current, ncoverage));
                    } else {
                        ret.coverage *= my.coverage;
                        ret.current.add(new CExt(my.domain, my.original, my.current, my.coverage));
                    }
                }
            }
        }
        return ret;
    }

    @Override
    public ICoverage and(ICoverage coverage) throws ThinklabException {

        Coverage ret = new Coverage();
        ret.coverage = 1.0;

        for (CExt my : current) {
            for (CExt his : ((Coverage) coverage).current) {
                if (his.domain.equals(my.domain)) {
                    ITopologicallyComparable<?> other = his.current == null ? his.original : his.current;
                    ITopologicallyComparable<?> current = my.current.intersection(other);
                    double ncoverage = current.getCoveredExtent() / my.original.getCoveredExtent();
                    ret.coverage *= ncoverage;
                    ret.current.add(new CExt(my.domain, my.original, current, ncoverage));
                }
            }
        }
        return ret;
    }

    /**
     * for debugging
     * 
     * @param c
     * @return
     */
    public ITopologicallyComparable<?> getCurrentExtent(IConcept c) {
        for (CExt cc : current) {
            if (cc.domain.equals(c)) {
                return cc.current;
            }
        }
        return null;
    }

    /**
     * for debugging
     * 
     * @param c
     * @return
     */
    public ITopologicallyComparable<?> getOriginalExtent(IConcept c) {
        for (CExt cc : current) {
            if (cc.domain.equals(c)) {
                return cc.current;
            }
        }
        return null;
    }

}
