/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.engine.modelling.functions;

import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Map;

import org.integratedmodelling.api.data.IAggregator;
import org.integratedmodelling.api.data.ITable;
import org.integratedmodelling.api.data.ITableSet;
import org.integratedmodelling.api.knowledge.IConcept;
import org.integratedmodelling.api.knowledge.IExpression;
import org.integratedmodelling.api.modelling.IAction;
import org.integratedmodelling.api.modelling.INamespace;
import org.integratedmodelling.api.monitoring.IMonitor;
import org.integratedmodelling.api.services.annotations.Prototype;
import org.integratedmodelling.common.data.TableFactory;
import org.integratedmodelling.common.thinkql.CodeExpression;
import org.integratedmodelling.common.thinkql.GroovyExpression;
import org.integratedmodelling.common.vocabulary.NS;
import org.integratedmodelling.engine.modelling.datasources.DatatableActuator;
import org.integratedmodelling.exceptions.ThinklabException;
import org.integratedmodelling.exceptions.ThinklabValidationException;

@Prototype(
        id = "data.tabular",
        args = {
                "file",
                Prototype.TEXT,
                "# sheet",
                Prototype.TEXT,
                "# column",
                Prototype.TEXT,
                "# row-selector",
                Prototype.TEXT,
                "# aggregation",
                Prototype.TEXT },
        returnTypes = { NS.STATE_CONTEXTUALIZER })
public class DATATABLE extends CodeExpression implements IExpression {

    @Override
    public Object eval(Map<String, Object> parameters, IMonitor monitor, IConcept... context)
            throws ThinklabException {

        ITable table = null;
        IExpression rowSelector = null;
        IAggregator operation = null;
        String column = null;

        if (parameters.containsKey("file")) {
            File file = new File((getProject() == null ? "" : (getProject().getLoadPath() + File.separator))
                    + parameters.get("file"));
            ITableSet tableSet = TableFactory.open(file);

            if (tableSet != null) {
                String sheet = null;
                if (parameters.containsKey("table-name")) {
                    sheet = parameters.get("table-name").toString();
                }
                if (sheet != null) {
                    table = tableSet.getTable(sheet);
                } else {
                    Collection<ITable> tables = tableSet.getTables();
                    if (tables.size() > 0) {
                        table = tables.iterator().next();
                    }
                }

                if (table == null) {
                    throw new ThinklabValidationException("data.tabular: cannot find "
                            + (sheet == null ? "default" : sheet) + " table");
                }
            }
        }
        if (parameters.containsKey("column")) {
            column = parameters.get("column").toString();
        }
        if (parameters.containsKey("row-selector")) {
            rowSelector = new GroovyExpression(parameters.get("row-selector").toString());
        }
        if (parameters.containsKey("aggregation")) {
            String op = parameters.get("aggregation").toString();
            operation = TableFactory.getAggregator(op);
            if (operation == null) {
                throw new ThinklabValidationException("data.tabular: cannot find operation " + op);
            }
        }

        // average is the default aggregation
        if (operation == null) {
            operation = TableFactory.getAggregator("mean");
        }

        return new DatatableActuator(new ArrayList<IAction>(), table, rowSelector, operation, column);
    }

    @Override
    public INamespace getNamespace() {
        // TODO Auto-generated method stub
        return null;
    }

}
