/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.engine;

import java.io.File;
import java.lang.annotation.Annotation;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.jcs.JCS;
import org.integratedmodelling.Version;
import org.integratedmodelling.api.annotations.SubjectType;
import org.integratedmodelling.api.auth.IResourceCatalog;
import org.integratedmodelling.api.auth.IUser;
import org.integratedmodelling.api.configuration.IConfiguration;
import org.integratedmodelling.api.knowledge.IConcept;
import org.integratedmodelling.api.knowledge.IProperty;
import org.integratedmodelling.api.metadata.IModelMetadata;
import org.integratedmodelling.api.metadata.IObservationMetadata;
import org.integratedmodelling.api.modelling.IDirectObserver;
import org.integratedmodelling.api.modelling.IObservable;
import org.integratedmodelling.api.modelling.ISubject;
import org.integratedmodelling.api.modelling.resolution.IResolutionContext;
import org.integratedmodelling.api.monitoring.IMonitor;
import org.integratedmodelling.api.monitoring.IProjectLifecycleListener;
import org.integratedmodelling.api.network.INode;
import org.integratedmodelling.api.project.IProject;
import org.integratedmodelling.api.rest.IRESTController;
import org.integratedmodelling.api.runtime.ITask;
import org.integratedmodelling.api.services.IPrototype;
import org.integratedmodelling.api.services.annotations.Prototype;
import org.integratedmodelling.common.beans.Status;
import org.integratedmodelling.common.command.ServiceCall;
import org.integratedmodelling.common.command.ServiceManager;
import org.integratedmodelling.common.components.ComponentManager;
import org.integratedmodelling.common.configuration.Configuration;
import org.integratedmodelling.common.configuration.KLAB;
import org.integratedmodelling.common.kim.KIMModelManager;
import org.integratedmodelling.common.network.Endpoints;
import org.integratedmodelling.common.project.ProjectManager;
import org.integratedmodelling.common.utils.ClassUtils;
import org.integratedmodelling.common.utils.ClassUtils.AnnotationVisitor;
import org.integratedmodelling.common.utils.Lock;
import org.integratedmodelling.common.vocabulary.NS;
import org.integratedmodelling.common.vocabulary.ObservationMetadata;
import org.integratedmodelling.engine.modelling.kbox.ModelKbox;
import org.integratedmodelling.engine.modelling.kbox.ObservationKbox;
import org.integratedmodelling.engine.network.Network;
import org.integratedmodelling.engine.rest.RESTManager;
import org.integratedmodelling.engine.rest.RESTResourceHandler;
import org.integratedmodelling.exceptions.ThinklabException;
import org.integratedmodelling.exceptions.ThinklabIOException;
import org.restlet.resource.ServerResource;

public class KLABNodeEngine implements INode {

    // TODO these should go in NS
    public static IConcept DOUBLE;
    public static IConcept BOOLEAN;
    public static IConcept TEXT;
    public static IConcept LONG;
    public static IConcept INTEGER;
    public static IConcept FLOAT;
    public static IConcept NUMBER;
    public static IConcept THING;
    public static IConcept NOTHING;

    public static IProperty CLASSIFICATION_PROPERTY;
    public static IProperty ABSTRACT_PROPERTY;
    protected long bootTime;
    Lock           lock;

    /*
     * prevent BS warnings from various libs.
     */
    static {
        System.setProperty("com.sun.media.jai.disableMediaLib", "true");
    }

    public KLABNodeEngine() throws ThinklabException {

        KLAB.PMANAGER = new ProjectManager();
        KLAB.ENGINE = this;

        KLAB.PMANAGER.addListener(new IProjectLifecycleListener() {

            @Override
            public void projectUnregistered(IProject project) {
            }

            @Override
            public void projectRegistered(IProject project) {
            }

            @Override
            public void projectPropertiesModified(IProject project, File file) {
            }

            @Override
            public void onReload(boolean full) {
                try {
                    ObservationKbox.get().reindexLocalObservations();
                } catch (ThinklabException e) {
                    KLAB.warn("reindexing of observations failed: " + e.getCause().getMessage());
                }
            }

            @Override
            public void namespaceModified(String ns, IProject project) {
            }

            @Override
            public void namespaceDeleted(String ns, IProject project) {
            }

            @Override
            public void namespaceAdded(String ns, IProject project) {
            }

            @Override
            public void fileModified(IProject project, File file) {
            }

            @Override
            public void fileDeleted(IProject project, File file) {
            }

            @Override
            public void fileCreated(IProject project, File file) {
            }
        });

        /*
         * needs to be defined for JCS config to work properly.
         */
        if (System.getProperty(IConfiguration.THINKLAB_WORK_DIRECTORY_PROPERTY) == null) {
            System.setProperty(IConfiguration.THINKLAB_WORK_DIRECTORY_PROPERTY, ".tl");
        }

        JCS.setConfigFilename("/org/integratedmodelling/cache.ccf");

        KLAB.MMANAGER = new KIMModelManager();
        KLAB.KM = new KnowledgeManager();
        KLAB.NETWORK = new Network();
        KLAB.CMANAGER = new ComponentManager();
        KLAB.MFACTORY = new ModelFactory();
    }

    public Status getEngineStatus() {
        
        Status ret = new Status();
        
        Runtime runtime = Runtime.getRuntime();
        String buildInfo = "development version";
        if (!Version.VERSION_BUILD.equals("VERSION_BUILD")) {
            buildInfo = " build " + Version.VERSION_BUILD + " (" + Version.VERSION_BRANCH + " "
                    + Version.VERSION_DATE + ")";
        }
        ret.setVersion(new Version().toString());
        ret.setBuildInfo(buildInfo);
        ret.setBootTime(KLABNodeEngine.get().getBootTime());
        ret.setTotalMemory(runtime.totalMemory() / 1048576);
        ret.setFreeMemory(runtime.freeMemory() / 1048576);
        ret.setAvailableProcessors(runtime.availableProcessors());
        
        return ret;
    }
    
    /*
     * (non-Javadoc)
     * 
     * @see
     * org.integratedmodelling.thinklab.plugin.IThinklabPlugin#getClassLoader()
     */
    public ClassLoader getClassLoader() {
        return this.getClass().getClassLoader();
    }

    protected final void startup() throws ThinklabException {

        KLAB.CONFIG = new Configuration(false);

        String buildInfo = "";
        if (!Version.VERSION_BUILD.equals("VERSION_BUILD")) {
            buildInfo = " build " + Version.VERSION_BUILD + " (" + Version.VERSION_BRANCH + " "
                    + Version.VERSION_DATE + ")";
        }

        String vdesc = new Version().toString() + buildInfo;

        KLAB.info("Thinklab engine [" + vdesc + "] booting on " + new Date());

        bootTime = new Date().getTime();

        ((KnowledgeManager) (KLAB.KM)).initialize();

        INTEGER = KLAB.KM.getConcept(NS.INTEGER);
        FLOAT = KLAB.KM.getConcept(NS.FLOAT);
        TEXT = KLAB.KM.getConcept(NS.TEXT);
        LONG = KLAB.KM.getConcept(NS.LONG);
        DOUBLE = KLAB.KM.getConcept(NS.DOUBLE);
        NUMBER = KLAB.KM.getConcept(NS.NUMBER);
        BOOLEAN = KLAB.KM.getConcept(NS.BOOLEAN);

        CLASSIFICATION_PROPERTY = KLAB.KM.getProperty(NS.CLASSIFICATION_PROPERTY);
        ABSTRACT_PROPERTY = KLAB.KM.getProperty(NS.IS_ABSTRACT);

        visitAnnotations();

        /*
         * and finally the projects if a startup directory has been mentioned in
         * the properties. This should only happen in node servers and not in
         * modeling servers.
         */
        if (KLAB.CONFIG.getProperties().getProperty(IConfiguration.THINKLAB_PROJECT_DIR_PROPERTY) != null) {
            File deploypath = new File(KLAB.CONFIG.getProperties()
                    .getProperty(IConfiguration.THINKLAB_PROJECT_DIR_PROPERTY));
            KLAB.PMANAGER.registerProjectDirectory(deploypath);
        }

        lock = new Lock(".lck");

        /**
         * Initialize the network.
         */
        ((Network) (KLAB.NETWORK)).initialize();

        KLAB.CMANAGER.initialize(KLAB.CONFIG.getDataPath("components"), KLAB.NETWORK, KLAB.NETWORK.getUser());

        /*
         * link components after network init, so that we can load projects from
         * the network. If the engine is local, we let the client explicitly do
         * so after the synchronized projects have been registered, so that the
         * components can refer to them.
         */
        if (!KLAB.NETWORK.isPersonal()) {

            /*
             * if we serve content from any projects, load them.
             */
            if (KLAB.CONFIG.getProperties()
                    .getProperty(IConfiguration.THINKLAB_ASSETS_DIR_PROPERTY) != null) {
                File pdir = new File(KLAB.CONFIG.getProperties()
                        .getProperty(IConfiguration.THINKLAB_ASSETS_DIR_PROPERTY));
                KLAB.PMANAGER.registerProjectDirectory(pdir);
                for (String p : KLAB.NETWORK.getResourceCatalog().getContentProjectIds()) {
                    KLAB.PMANAGER.loadProject(p, KLAB.MFACTORY.getRootParsingContext());
                }
            }

            /*
             * in personal engines, no need to keep reallocating connections in what can
             * potentially be a slow operation.
             */
            if (KLAB.NETWORK.isPersonal()) {
                ModelKbox.get().getDatabase().preallocateConnection();
                ObservationKbox.get().getDatabase().preallocateConnection();
            }

            KLAB.CMANAGER.link();

            /*
             * in personal engines, no need to keep reallocating connections in what can
             * potentially be a slow operation.
             */
            if (KLAB.NETWORK.isPersonal()) {
                ModelKbox.get().getDatabase().deallocateConnection();
                ObservationKbox.get().getDatabase().deallocateConnection();
            }
        }
    }

    private void visitAnnotations() throws ThinklabException {

        ClassUtils.visitAnnotations("org.integratedmodelling", Prototype.class, new AnnotationVisitor() {
            @Override
            public void visit(Annotation acls, Class<?> target) throws ThinklabException {
                ServiceManager.get().processPrototypeDeclaration((Prototype) acls, target);
            }
        });

        ClassUtils
                .visitAnnotations("org.integratedmodelling", RESTResourceHandler.class, new AnnotationVisitor() {
                    @Override
                    public void visit(Annotation acls, Class<?> target) throws ThinklabException {
                        registerRESTResource(target, (RESTResourceHandler) acls);
                    }
                });

        ClassUtils.visitAnnotations("org.integratedmodelling", SubjectType.class, new AnnotationVisitor() {
            @Override
            public void visit(Annotation acls, Class<?> target) throws ThinklabException {
                registerSubjectClass(target, (SubjectType) acls);
            }
        });

    }

    @SuppressWarnings("unchecked")
    private void registerRESTResource(Class<?> cls, RESTResourceHandler annotation) throws ThinklabException {

        String path = annotation.id();
        String description = annotation.description();
        String argument = annotation.arguments();
        String options = annotation.options();
        RESTManager.get()
                .registerService(path, (Class<? extends ServerResource>) cls, description, argument, options);
    }

    @SuppressWarnings("unchecked")
    private void registerSubjectClass(Class<?> cls, SubjectType annotation) throws ThinklabException {

        String concept = annotation.value();
        KLAB.MMANAGER.registerSubjectClass(concept, (Class<? extends ISubject>) cls);
    }

    public ClassLoader swapClassloader() {
        ClassLoader clsl = Thread.currentThread().getContextClassLoader();
        Thread.currentThread().setContextClassLoader(getClassLoader());
        return clsl;
    }

    public void resetClassLoader(ClassLoader clsl) {
        Thread.currentThread().setContextClassLoader(clsl);
    }

    public URL getResourceURL(String resource) throws ThinklabIOException {
        return getResourceURL(resource, null);
    }

    public URL getResourceURL(String resource, IProject plugin) throws ThinklabIOException {

        URL ret = null;

        try {
            File f = new File(resource);

            if (f.exists()) {
                ret = f.toURI().toURL();
            } else if (resource.contains("://")) {
                ret = new URL(resource);
            } else {
                ret = getClassLoader().getResource(resource);
            }
        } catch (MalformedURLException e) {
            throw new ThinklabIOException(e);
        }

        return ret;
    }

    /**
     * Return the only instance of Thinklab, your favourite knowledge manager.
     * 
     * @return
     */
    public static KLABNodeEngine get() {
        return (KLABNodeEngine) KLAB.ENGINE;
    }

    /**
     * Entry point in Thinklab: call boot() before you do anything. Calling more
     * than once without calling shutdown() has no effect.
     * 
     * @throws ThinklabException
     */
    public static void boot() throws ThinklabException {
        if (KLAB.ENGINE == null) {
            KLAB.ENGINE = new KLABNodeEngine();
        }
        ((KLABNodeEngine) (KLAB.ENGINE)).startup();
    }

    /**
     * You must call shutdown() when you're done to ensure integrity of data and
     * everything. This said, I always abort applications without getting there
     * and not much happens.
     */
    public static void shutdown() {

        KLAB.info("Thinklab shutting down");
        ((KnowledgeManager) (KLAB.KM)).shutdown();

    }
    
    public void shutdown(final int seconds) throws ThinklabException {

        new Thread() {

            @Override
            public void run() {

                int status = 0;
                if (seconds > 0) {
                    try {
                        sleep(seconds * 1000);
                    } catch (InterruptedException e) {
                        status = 255;
                    }
                }

                if (lock.isLocked()) {
                    lock.unlock();
                }

                System.exit(status);

            }
        }.start();
    }

    public long getBootTime() {
        return bootTime;
    }

    public Lock getLock() {
        return lock;
    }

    @Override
    public void rescanClasspath() throws ThinklabException {

        ClassUtils.visitAnnotations("org.integratedmodelling", Prototype.class, new AnnotationVisitor() {
            @Override
            public void visit(Annotation acls, Class<?> target) throws ThinklabException {
                if (!ServiceManager.get().hasPrototype(((Prototype) acls).id())) {
                    ServiceManager.get().processPrototypeDeclaration((Prototype) acls, target);
                }
            }
        });

        ClassUtils.visitAnnotations("org.integratedmodelling", SubjectType.class, new AnnotationVisitor() {
            @Override
            public void visit(Annotation acls, Class<?> target) throws ThinklabException {
                registerSubjectClass(target, (SubjectType) acls);
            }
        });

        ClassUtils
                .visitAnnotations("org.integratedmodelling", org.integratedmodelling.api.plugin.Component.class, new AnnotationVisitor() {
                    @Override
                    public void visit(Annotation acls, Class<?> target) throws ThinklabException {

                        if (KLAB.CMANAGER
                                .getComponent(((org.integratedmodelling.api.plugin.Component) acls)
                                        .id()) == null) {
                            KLAB.CMANAGER
                                    .register((org.integratedmodelling.api.plugin.Component) acls, target);

                            /*
                             * in personal engines, no need to keep reallocating connections in what can
                             * potentially be a slow operation.
                             */
                            if (KLAB.NETWORK.isPersonal()) {
                                ModelKbox.get().getDatabase().preallocateConnection();
                                ObservationKbox.get().getDatabase().preallocateConnection();
                            }

                            KLAB.CMANAGER.link();

                            /*
                             * in personal engines, no need to keep reallocating connections in what can
                             * potentially be a slow operation.
                             */
                            if (KLAB.NETWORK.isPersonal()) {
                                ModelKbox.get().getDatabase().deallocateConnection();
                                ObservationKbox.get().getDatabase().deallocateConnection();
                            }
                        }
                    }
                });
    }

    @Override
    public String getName() {
        return KLAB.NAME;
    }

    @Override
    public String getUrl() {
        return KLAB.NETWORK.getUrl();
    }

    @Override
    public Map<String, Object> getStatus() {
        return null;
    }

    @Override
    public boolean isRunning() {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public Collection<IPrototype> getServices() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public Collection<IPrototype> getFunctions() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public boolean providesComponent(String id) {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public boolean providesService(String id) {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public IPrototype getServicePrototype(String id) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public IPrototype getFunctionPrototype(String id) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public String submitObservation(IDirectObserver observer, boolean store) throws ThinklabException {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public List<IObservationMetadata> queryObservations(String text, boolean localOnly)
            throws ThinklabException {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public IDirectObserver retrieveObservation(String observationid, String nodeId) throws ThinklabException {

        IDirectObserver ret = null;

        ObservationMetadata omd = ObservationKbox.get().retrieveByName(observationid);

        if (omd != null) {
            return omd.getSubjectObserver();
        }

        /*
        * broadcast call for search function on network.
        */
        if (KLAB.NETWORK.isPersonal() && KLAB.NETWORK.providesComponent("im.search")) {

            if (nodeId != null) {
                ret = KLAB.NETWORK.getNode(nodeId).retrieveObservation(observationid, nodeId);
            } else {

                ServiceCall scl = ServiceManager
                        .getServiceCall(Endpoints.RETRIEVE_OBSERVATION, "observation-name", observationid);

                Object mdd = KLAB.NETWORK.broadcast(scl.post(), scl.getMonitor());
                if (mdd instanceof Collection<?>) {
                    for (Object md : ((Collection<?>) mdd)) {
                        if (md instanceof ObservationMetadata) {
                            omd = (ObservationMetadata) md;
                            break;
                        }
                    }
                }
            }
        }

        if (omd != null && ret == null) {
            ret = omd.getSubjectObserver();
        }

        return ret;
    }

    @Override
    public List<IModelMetadata> queryModels(IObservable observable, IResolutionContext context)
            throws ThinklabException {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public ITask setupComponent(String componentId) throws ThinklabException {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public void removeObservations(Collection<String> observationNames) throws ThinklabException {
        // TODO Auto-generated method stub

    }

    @Override
    public void setMonitor(IMonitor monitor) {
    }

    @Override
    public IMonitor getMonitor() {
        return KLAB.CMANAGER.getMonitor();
    }

    @Override
    public Object get(String endpoint, Object... args) throws ThinklabException {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public Object post(String endpoint, Object... args) throws ThinklabException {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public IUser getUser() {
        return KLAB.NETWORK.getUser();
    }

    @Override
    public boolean responds() {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public String connect() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public IRESTController forNode(String url, String key) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public IProject importProject(String projectId) throws ThinklabException {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public Set<String> getSubmittingGroupSet() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public String getId() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public boolean isPrimaryNode() {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public IResourceCatalog getResourceCatalog() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public boolean isActive() {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public String getKey() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public String getDescription() {
        // TODO Auto-generated method stub
        return null;
    }


}
