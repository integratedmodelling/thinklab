/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.engine.rest.resources;

import java.io.File;
import java.util.Map;

import org.integratedmodelling.api.runtime.ISession;
import org.integratedmodelling.common.configuration.KLAB;
import org.integratedmodelling.common.utils.MiscUtilities;
import org.integratedmodelling.engine.KLABEngine;
import org.integratedmodelling.engine.rest.BaseRESTService;
import org.integratedmodelling.engine.rest.RESTManager;
import org.integratedmodelling.exceptions.ThinklabAuthenticationException;
import org.integratedmodelling.exceptions.ThinklabException;
import org.integratedmodelling.exceptions.ThinklabResourceNotFoundException;
import org.restlet.data.Disposition;
import org.restlet.data.MediaType;
import org.restlet.representation.FileRepresentation;

/**
 * Send a file to the server using a valid handle returned previously. Only
 * files created in the workspace of the same session will be sent.
 * 
 * @author ferdinando.villa
 *
 */
public class FileSendService extends BaseRESTService {

    @Override
    public Object process(Map<String, String> entity, Map<String, Object> authInfo) throws ThinklabException {

        File file = retrieveFile(getArgument("handle"), (ISession) authInfo.get(AUTHORIZED_SESSION));
        String extension = MiscUtilities.getFileExtension(file.toString());
        MediaType mt = KLABEngine.get().getMetadataService().getMediaType(extension);
        FileRepresentation rep = new FileRepresentation(file, mt);
        Disposition disp = new Disposition(Disposition.TYPE_ATTACHMENT);
        disp.setFilename(file.getName());
        disp.setSize(file.length());
        rep.setDisposition(disp);
        return rep;
    }

    private File retrieveFile(String string, ISession session) throws ThinklabException {

        boolean ok = string.startsWith(session == null ? "uploads" : session.getId());
        if (!ok) {
            ok = RESTManager.get().checkResourceAuthorization(string);
        }

        if (!ok)
            throw new ThinklabAuthenticationException("send: trying to access a non-existing or unauthorized resource");

        File sdir = new File(KLAB.CONFIG.getScratchArea() + File.separator + "rest/tmp" + File.separator
                + string);

        if (!sdir.exists())
            throw new ThinklabResourceNotFoundException("send: trying to access nonexistent file " + string);

        return sdir;
    }

}
