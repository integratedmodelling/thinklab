/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
//package org.integratedmodelling.engine.rest;
//
//import java.util.HashMap;
//import java.util.Iterator;
//import java.util.Map;
//
//import org.integratedmodelling.api.auth.IUser;
//import org.integratedmodelling.api.auth.IUserAssets;
//import org.integratedmodelling.api.network.INetwork;
//import org.integratedmodelling.api.network.INode;
//import org.integratedmodelling.auth.data.Datarecord;
//import org.integratedmodelling.common.configuration.Env;
//import org.integratedmodelling.common.utils.Escape;
//import org.integratedmodelling.common.utils.Path;
//
//import us.monoid.json.JSONObject;
//import us.monoid.web.Resty;
//
//public class RESTHelper {
//
//    public static String getURLForPrimaryServerCommand(String serverURL, String command, Object... arguments) {
//
//        String url = serverURL + "/" + command;
//        if (arguments != null && arguments.length > 0) {
//            for (int i = 0; i < arguments.length; i++)
//                url += (i == 0 ? "?" : "&") + Escape.forURL(arguments[i].toString()) + "="
//                        + Escape.forURL(arguments[++i].toString());
//        }
//        return url;
//    }
//
//    @SuppressWarnings("unchecked")
//    public static <T> T getAsset(String urn, IUser user, Class<T> cls) {
//
//        if (cls.equals(Datarecord.class)) {
//
//            String url =
//                    getURLForPrimaryServerCommand(user.getServerURL(), "get-asset",
//                            "asset", urn,
//                            "type", IUserAssets.TYPE_DATA_RECORD,
//                            "key", user.getSecurityKey());
//            try {
//                JSONObject ret = new Resty().json(url).object();
//                Map<String, Object> data = new HashMap<String, Object>();
//
//                if (ret.get("urn") == null)
//                    return null;
//
//                for (Iterator<String> it = ret.keys(); it.hasNext();) {
//                    String k = it.next();
//                    data.put(k, ret.get(k));
//                }
//                return (T) new Datarecord(data);
//
//            } catch (Exception e) {
//                return null;
//            }
//
//        }
//
//        return null;
//    }
//
//    @SuppressWarnings("unchecked")
//    public static <T> T getAsset(String urn, INetwork network, Class<T> cls) {
//
//        if (cls.equals(Datarecord.class)) {
//
//            /*
//             * find the node resolving the URN
//             */
//            String nodeId = Path.getFirst(urn, ":");
//            if (nodeId.equals(Env.NAME)) {
//                /*
//                 * self! just get the asset from the database.
//                 */
//                // Env.logger.info("serving asset URN " + urn + " from local database");
//                return (T) Datarecord.retrieve(urn);
//            }
//
//            INode node = network.getNode(nodeId);
//            if (node == null) {
//                Env.logger.error("network has no active node to resolve URN " + urn);
//                return null;
//            }
//
//            String url =
//                    getURLForPrimaryServerCommand(node.getUrl(), "get-asset",
//                            "asset", urn,
//                            "type", IUserAssets.TYPE_DATA_RECORD,
//                            "server-key", node.getKey());
//            try {
//                JSONObject ret = new Resty().json(url).object();
//                Map<String, Object> data = new HashMap<String, Object>();
//
//                if (ret.get("urn") == null)
//                    return null;
//
//                for (Iterator<String> it = ret.keys(); it.hasNext();) {
//                    String k = it.next();
//                    data.put(k, ret.get(k));
//                }
//                return (T) new Datarecord(data);
//
//            } catch (Exception e) {
//                return null;
//            }
//
//        }
//
//        return null;
//    }
// }
