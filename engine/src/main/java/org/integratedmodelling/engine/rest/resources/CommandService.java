/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.engine.rest.resources;

import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import javax.imageio.ImageIO;

import org.integratedmodelling.api.auth.IUser;
import org.integratedmodelling.api.modelling.visualization.IImageMedia;
import org.integratedmodelling.api.monitoring.IMonitor;
import org.integratedmodelling.api.network.INode;
import org.integratedmodelling.api.runtime.ISession;
import org.integratedmodelling.api.services.IPrototype;
import org.integratedmodelling.api.services.IServiceCall;
import org.integratedmodelling.api.services.types.IAdminService;
import org.integratedmodelling.api.services.types.IEngineService;
import org.integratedmodelling.api.services.types.ILocalService;
import org.integratedmodelling.api.services.types.IPublicService;
import org.integratedmodelling.api.services.types.ISessionService;
import org.integratedmodelling.api.services.types.IUserService;
import org.integratedmodelling.common.command.ServiceCall;
import org.integratedmodelling.common.command.ServiceManager;
import org.integratedmodelling.common.configuration.KLAB;
import org.integratedmodelling.common.model.remote.ModelService;
import org.integratedmodelling.common.network.Endpoints;
import org.integratedmodelling.engine.modelling.monitoring.Monitor;
import org.integratedmodelling.engine.network.Network;
import org.integratedmodelling.engine.rest.BaseRESTService;
import org.integratedmodelling.engine.rest.FileMedia;
import org.integratedmodelling.engine.runtime.Session;
import org.integratedmodelling.exceptions.ThinklabAuthorizationException;
import org.integratedmodelling.exceptions.ThinklabException;
import org.integratedmodelling.exceptions.ThinklabIOException;
import org.integratedmodelling.exceptions.ThinklabUnsupportedOperationException;
import org.integratedmodelling.exceptions.ThinklabValidationException;
import org.restlet.data.Disposition;
import org.restlet.data.MediaType;
import org.restlet.representation.ByteArrayRepresentation;
import org.restlet.representation.FileRepresentation;

/**
 * Execute a language statement and optionally produce a visualization and/or dataset
 * for the results.
 * 
 * @author ferdinando.villa
 *
 */
public class CommandService extends BaseRESTService {

    static Set<String> unloggedCommands = new HashSet<>();
    static {
        /*
         * add commands whose ID is in here will not get logged. Maybe one day we'll want to 
         * specify those that should be instead.
         */
        unloggedCommands.add(Endpoints.GET_NOTIFICATIONS);
        unloggedCommands.add(Endpoints.GET_NODE_CAPABILITIES);
        unloggedCommands.add(Endpoints.IDENTIFY);
    }

    public CommandService() {
    }

    @Override
    public Object process(Map<String, String> arguments, Map<String, Object> authInfo)
            throws ThinklabException {

        String pdefs = "";

        IPrototype prototype = ServiceManager.get().getPrototype(getServiceName());
        if (prototype == null) {
            throw new ThinklabValidationException("invalid service call: " + getServiceName());
        }

        String username = "anonymous";
        if (authInfo.containsKey(Endpoints.USERNAME_INFO_HEADER)) {
            username = authInfo.get(Endpoints.USERNAME_INFO_HEADER).toString();
        }
        ISession session = (ISession) authInfo.get(BaseRESTService.AUTHORIZED_SESSION);
        IUser user = (IUser) authInfo.get(BaseRESTService.AUTHORIZED_USER);

        if (username.equals("anonymous") && authInfo.containsKey(Endpoints.CALLING_ENGINE_KEY_HEADER)) {
            INode caller = ((Network) KLAB.NETWORK)
                    .getNodeByKey(authInfo.get(Endpoints.CALLING_ENGINE_KEY_HEADER).toString());
            username = caller == null ? "<E>" : caller.getId();
        }

        /*
         * final set of verifications. If we get here all authentications are OK, but we need to
         * ensure that we have them at all. Any of these being OK is enough to pass the request
         * through.
         */
        boolean engOK = true, admOK = true, usrOK = true, sesOK = true, locOK = true, pubOK = true;
        int errors = 0, metConditions = 0;
        if (IEngineService.class.isAssignableFrom(prototype.getExecutorClass())) {
            // must have engine authorization
            if (!authInfo.containsKey(Endpoints.RECEIVING_ENGINE_KEY_HEADER)) {
                engOK = false;
                errors++;
            } else {
                metConditions++;
                pdefs += (pdefs.isEmpty() ? "" : " ") + "ENG";
            }
        }
        if (IAdminService.class.isAssignableFrom(prototype.getExecutorClass())) {
            // must have admin authorization or be on a personal engine
            if (!(KLAB.NETWORK.isPersonal() || authInfo
                    .containsKey(Endpoints.ADMIN_AUTHORIZATION_HEADER))) {
                admOK = false;
                errors++;
            } else {
                metConditions++;
                pdefs += (pdefs.isEmpty() ? "" : " ") + "ADM";

            }

        }
        if (IUserService.class.isAssignableFrom(prototype.getExecutorClass())) {
            // must have user
            if (!authInfo.containsKey(AUTHORIZED_USER)) {
                usrOK = false;
                errors++;
            } else {
                metConditions++;
                pdefs += (pdefs.isEmpty() ? "" : " ") + "USR";

            }
        }
        if (ISessionService.class.isAssignableFrom(prototype.getExecutorClass())) {
            // must have session
            if (!authInfo.containsKey(AUTHORIZED_SESSION)) {
                sesOK = false;
                errors++;
            } else {
                metConditions++;
                pdefs += (pdefs.isEmpty() ? "" : " ") + "SES";
            }
        }
        if (ILocalService.class.isAssignableFrom(prototype.getExecutorClass())) {
            // must be on a personal engine
            if (!KLAB.NETWORK.isPersonal()) {
                locOK = false;
                errors++;
            } else {
                metConditions++;
                pdefs += (pdefs.isEmpty() ? "" : " ") + "LOC";
            }
        }
        if (IPublicService.class.isAssignableFrom(prototype.getExecutorClass())) {
            // must be on a public engine
            if (KLAB.NETWORK.isPersonal()) {
                // TODO throw the exception instead
                pubOK = false;
                errors++;
            } else {
                metConditions++;
                pdefs += (pdefs.isEmpty() ? "" : " ") + "PUB";
            }
        }

        /*
         * continue only if at least one condition was met or there was no condition to meet.
         */
        if (metConditions == 0 && errors > 0) {

            if (!engOK) {
                throw new ThinklabAuthorizationException(prototype.getId()
                        + ": this service requires engine authorization");

            }
            if (!admOK) {
                throw new ThinklabAuthorizationException(prototype.getId()
                        + ": this service requires administrator authorization");

            }
            if (!usrOK) {
                throw new ThinklabAuthorizationException(prototype.getId()
                        + ": this service can only be executed by a logged in user");

            }
            if (!sesOK) {
                throw new ThinklabAuthorizationException(prototype.getId()
                        + ": this service can only be executed within an authorized user session");

            }
            if (!locOK) {
                throw new ThinklabAuthorizationException(prototype.getId()
                        + ": this service can only be executed on a local engine");

            }
            if (!pubOK) {
                throw new ThinklabAuthorizationException(prototype.getId()
                        + ": this service can only be executed on a public Thinklab node");
            }
        }

        if (session != null) {
            /*
             * record last use time for reaper
             */
            ((Session) session).ping();
        }

        IServiceCall command = ServiceManager
                .parseCall(getServiceName(), arguments, new Monitor(-1, session), session);

        // do not log transition modeling to avoid log explosion.
        if (!unloggedCommands.contains(command.getPrototype().getId())
                && !command.has(ModelService.TRANSITION_ARGUMENT)) {

            // command privileges from headers
            String kdef = pdefs.isEmpty() ? "" : (" [" + pdefs + "]");

            /*
             * log the request nicely.
             */
            KLAB.info((username.equals("anonymous") ? "" : ("[" + username + "] ")) + command
                    // + " <" + this.getRequest().getHostRef().getHostIdentifier() + ">"
                    + kdef);
        }

        ((ServiceCall) command).setUser(user);
        ((ServiceCall) command).setGroups((Set<String>) authInfo.get(BaseRESTService.REQUESTER_GROUPS));
        ((ServiceCall) command).setRoles((Set<String>) authInfo.get(BaseRESTService.REQUESTER_ROLES));
        if (authInfo.containsKey(Endpoints.CALLING_ENGINE_KEY_HEADER)) {
            ((ServiceCall) command).setCallerAuthorizationKey(authInfo
                    .get(Endpoints.CALLING_ENGINE_KEY_HEADER).toString());
        }
        if (authInfo.containsKey(Endpoints.RECEIVING_ENGINE_KEY_HEADER)) {
            ((ServiceCall) command).setEngineAuthorization(true);
        }

        return processResult(command.execute(), command.getMonitor());
    }

    private Object processResult(Object result, IMonitor monitor) throws ThinklabException {

        if (result instanceof IImageMedia) {
            BufferedImage image = null;
            Image img = ((IImageMedia) result).getImage();

            if (img instanceof BufferedImage) {
                image = (BufferedImage) img;

                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                try {
                    ImageIO.write(image, "png", baos);
                } catch (IOException e) {
                    throw new ThinklabIOException(e);
                }
                // TODO generalize
                return new ByteArrayRepresentation(baos.toByteArray(), new MediaType("image/png"));
            } else {
                throw new ThinklabUnsupportedOperationException("media contain an image that is not buffered");
            }

        } else if (result instanceof BufferedImage) {

            BufferedImage image = (BufferedImage) result;

            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            try {
                ImageIO.write(image, "png", baos);
            } catch (IOException e) {
                throw new ThinklabIOException(e);
            }
            // TODO generalize
            return new ByteArrayRepresentation(baos.toByteArray(), new MediaType("image/png"));

        } else if (result instanceof FileMedia) {

            FileMedia fm = (FileMedia) result;
            FileRepresentation rep = new FileRepresentation(fm.file, fm.type);
            Disposition disp = new Disposition(Disposition.TYPE_ATTACHMENT);
            disp.setFilename(fm.file.getName());
            disp.setSize(fm.file.length());
            rep.setDisposition(disp);
            return rep;

        }

        return result;
    }
}
