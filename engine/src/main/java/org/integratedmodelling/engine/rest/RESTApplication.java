/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.engine.rest;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;

import org.integratedmodelling.api.services.IPrototype;
import org.integratedmodelling.common.command.ServiceManager;
import org.integratedmodelling.common.configuration.KLAB;
import org.integratedmodelling.engine.rest.resources.CommandService;
import org.integratedmodelling.engine.rest.resources.FileReceiveService;
import org.integratedmodelling.engine.rest.resources.FileSendService;
import org.integratedmodelling.engine.rest.resources.PingService;
import org.restlet.Application;
import org.restlet.Restlet;
import org.restlet.data.Protocol;
import org.restlet.resource.Directory;
import org.restlet.routing.Router;
import org.restlet.routing.Template;

/**
 * The REST application that exposes all thinklab commands through the REST API
 * 
 * @author ferdinando.villa
 */
public class RESTApplication extends Application {

    ArrayList<CommandService>  _cservices         = new ArrayList<CommandService>();
    HashMap<String, Directory> _accessDirectories = new HashMap<>();
    Router                     _router;

    @Override
    public Restlet createInboundRoot() {

        getConnectorService().getClientProtocols().add(Protocol.FILE);

        _router = new Router(getContext());

        /*
         * root entry point is a "ping" service that returns Thinklab and server stats.
         */
        _router.attach("/", PingService.class);

        // /*
        // * register authentication service, which gives you a session with privileges depending on
        // * user properties. Will use whatever authentication manager is installed.
        // */
        // _router.attach("/authenticate", AuthenticateService.class);

        /*
         * receive uploaded files, return handle
         */
        _router.attach("/receive", FileReceiveService.class);

        /*
         * send uploaded files to client using download handle
         */
        _router.attach("/send", FileSendService.class);

        // /*
        // * remote project management
        // */
        // _router.attach("/project", ProjectService.class);

        /*
         * notification services (also control running tasks)
         */
        // _router.attach("/notifications", GetNotificationsService.class);

        // attach each prototype that is not a function.
        for (final IPrototype d : ServiceManager.get().getPrototypes()) {
            _router.attach("/" + d.getId(), CommandService.class);
        }

        /*
         * configure an entry point per installed command
         */
        for (String path : RESTManager.get().getPaths()) {
            _router.attach("/" + path, RESTManager.get().getResourceForPath(path), Template.MODE_STARTS_WITH);
            KLAB.info("REST command " + path + " registered");
        }

        /*
         * expose logs - TODO see if we want this to continue. At the moment shows auth tokens to the whole world, so
         * certainly not in this form.
         */
        mapDirectoryToKey(KLAB.CONFIG.getDataPath("log"), "logs");

        return _router;
    }

    /**
     * Map a filesystem directory to a string path. Used to give users temporary access to resources when
     * they log in, by mapping asset directories to privately shared keys.
     * 
     * @param directory
     * @param key
     */
    public void mapDirectoryToKey(File directory, String key) {
        Directory dir = new Directory(getContext(), directory.toURI().toString());
        _router.attach("/" + key, dir, Template.MODE_STARTS_WITH);
        _accessDirectories.put(key, dir);
    }

    /**
     * Unmap a previously mapped directory.
     * 
     * @param key
     */
    public void unmapDirectory(String key) {
        if (_accessDirectories.containsKey(key)) {
            _router.detach(_accessDirectories.get(key));
            _accessDirectories.remove(key);
        }
    }
}
