/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.engine.rest.resources;

import java.io.File;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.integratedmodelling.api.runtime.ISession;
import org.integratedmodelling.collections.Pair;
import org.integratedmodelling.common.configuration.KLAB;
import org.integratedmodelling.engine.rest.BaseRESTService;
import org.integratedmodelling.engine.rest.JSONserializer;
import org.integratedmodelling.exceptions.ThinklabException;
import org.integratedmodelling.exceptions.ThinklabIOException;
import org.restlet.data.MediaType;
import org.restlet.ext.fileupload.RestletFileUpload;
import org.restlet.representation.Representation;
import org.restlet.resource.Post;

/**
 * Receive a file from the client and return an handle that can be used later
 * to communicate the file location for services that will use it.
 * 
 * @author ferdinando.villa
 *
 */
public class FileReceiveService extends BaseRESTService {

    @Override
    @Post
    public Representation servicePOST(Representation entity) throws Exception {

        ISession session = null;// getSession();

        if (entity != null) {

            if (MediaType.MULTIPART_FORM_DATA.equals(entity.getMediaType(), true)) {

                // 1/ Create a factory for disk-based file items
                DiskFileItemFactory factory = new DiskFileItemFactory();
                factory.setSizeThreshold(1000240);

                // 2/ Create a new file upload handler based on the Restlet
                // FileUpload extension that will parse Restlet requests and
                // generates FileItems.
                RestletFileUpload upload = new RestletFileUpload(factory);
                List<FileItem> items;

                // 3/ Request is parsed by the handler which generates a
                // list of FileItems
                items = upload.parseRequest(getRequest());

                // save each file
                ArrayList<String> done = new ArrayList<String>();
                for (final Iterator<FileItem> it = items.iterator(); it.hasNext();) {
                    FileItem fi = it.next();
                    Pair<File, String> filename = getFileName(fi.getName(), session == null ? "uploads"
                            : session.getId());
                    fi.write(filename.getFirst());
                    done.add(filename.getSecond());
                }

                if (done.size() > 0) {
                    return JSONserializer.wrap(done.toArray(new String[done.size()]));
                } else {
                    return JSONserializer.wrap(JSONserializer
                            .wrapException(new ThinklabIOException("file upload failed: no file received")));
                }
            }
        }
        return JSONserializer.wrap(JSONserializer
                .wrapException(new ThinklabIOException("file upload: not a multipart request")));

    }

    @Override
    protected Object process(Map<String, String> arguments, Map<String, Object> authInfo)
            throws ThinklabException {
        // TODO Auto-generated method stub
        KLAB.error("process() called! With " + arguments.size() + " args...");
        return null;
    }

}
