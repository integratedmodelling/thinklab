/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.engine.rest.resources;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.integratedmodelling.Version;
import org.integratedmodelling.api.engine.IEngine;
import org.integratedmodelling.common.configuration.KLAB;
import org.integratedmodelling.engine.KLABEngine;
import org.integratedmodelling.engine.rest.BaseRESTService;

/**
 * Returns server status information. Bound to the root service URL and used as an
 * handshaking command ("ping") to ensure that server is alive and well.
 * 
 * @author ferdinando.villa
 *
 */
public class PingService extends BaseRESTService {

    @Override
    public Object process(Map<String, String> entity, Map<String, Object> authInfo) {

        Map<String, Object> ret = new HashMap<>();
        Runtime runtime = Runtime.getRuntime();
        String buildInfo = "development version";
        if (!Version.VERSION_BUILD.equals("VERSION_BUILD")) {
            buildInfo = " build " + Version.VERSION_BUILD + " (" + Version.VERSION_BRANCH + " "
                    + Version.VERSION_DATE + ")";
        }
        ret.put(IEngine.VERSION_STRING, new Version().toString());
        ret.put(IEngine.VERSION_INFO_STRING, buildInfo);
        ret.put(IEngine.BOOT_TIME_MS, KLABEngine.get().getBootTime());
        ret.put(IEngine.TOTAL_MEMORY_MB, runtime.totalMemory() / 1048576);
        ret.put(IEngine.FREE_MEMORY_MB, runtime.freeMemory() / 1048576);
        ret.put(IEngine.AVAILABLE_PROCESSORS, runtime.availableProcessors());

        // human-readable for debugging
        ret.put("up-since", new Date(KLABEngine.get().getBootTime()).toString());

        /*
         * flag changes in the network. 
         * TODO there is no polling any more (just on-demand retrieval), so it may make little sense for now.
         */
        if (KLAB.NETWORK != null && KLAB.NETWORK.hasChanged()) {
            ret.put("network-changed", "true");
        }

        return ret;
    }
}
