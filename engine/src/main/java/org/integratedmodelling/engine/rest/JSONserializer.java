/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.engine.rest;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang.exception.ExceptionUtils;
import org.integratedmodelling.api.lang.IList;
import org.integratedmodelling.api.metadata.IModelMetadata;
import org.integratedmodelling.api.modelling.ILanguageObject;
import org.integratedmodelling.api.modelling.IObservable;
import org.integratedmodelling.api.modelling.IRelationship;
import org.integratedmodelling.api.modelling.IState;
import org.integratedmodelling.api.modelling.ISubject;
import org.integratedmodelling.api.runtime.ITask;
import org.integratedmodelling.common.utils.MiscUtilities;
import org.integratedmodelling.lang.IRemoteSerializable;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.restlet.data.CharacterSet;
import org.restlet.ext.json.JsonRepresentation;
import org.restlet.representation.Representation;

/**
 * Wrap anything into the screwy org.json objects that restlet likes before they're passed back to the client. Use 
 * conventions to provide Thinklab-aware type annotations so that we can easily reconstruct marshalled objects 
 * at client side. Believe it or not, it's quite a bit less work and coding than using actual POJO marshalling 
 * through more proper technology.
 * 
 * The client counterpart is the JSON class in the client package.
 * 
 * @author Ferd
 *
 */
public class JSONserializer {

    static Set<Class<?>> recognizedInterfaces = new HashSet<>();

    static {
        recognizedInterfaces.add(ISubject.class);
        recognizedInterfaces.add(IRelationship.class);
        recognizedInterfaces.add(IState.class);
        recognizedInterfaces.add(ITask.class);
        recognizedInterfaces.add(IModelMetadata.class);
        recognizedInterfaces.add(IObservable.class);
    }

    /*
     * FIXME this is NOT a good strategy. Must explicitly annotate starting from the class. May work for now as the number
     * of classes marshalled so far is limited and probably near-final. But we must use some decent mechanism asap. For what 
     * it is now, a simple switch at both ends would be much cleaner.
     * 
     * @param o
     * @return
     */
    static String getAPIInterface(Object o) {

        for (Class<?> i : o.getClass().getInterfaces()) {
            if (i.getCanonicalName().startsWith("org.integratedmodelling.api")
                    /* hack */ && !i.equals(ILanguageObject.class)) {
                return i.getCanonicalName();
            }
        }

        for (Class<?> cl : recognizedInterfaces) {
            if (cl.isAssignableFrom(o.getClass())) {
                return cl.getCanonicalName();
            }
        }

        return null;
    }

    static public Representation wrap(Object o) throws JSONException {

        if (o instanceof Representation) {
            return (Representation) o;
        }

        Object r = wrapInternal(o);
        JSONObject ret = null;

        /*
         * repackage any non-map object into a map according to conventions.
         */
        if (r instanceof JSONObject) {
            ret = (JSONObject) r;
        } else {

            ret = new JSONObject();

            if (r == null) {
                ret.put("_itype", "_null");
            } else if (r instanceof JSONArray) {
                ret.put("_itype", "_array");
                ret.put("_ivalue", r);
            } else if (r instanceof String) {
                ret.put("_itype", "_string");
                ret.put("_ivalue", r);
            } else if (r instanceof Number) {
                ret.put("_itype", (r instanceof Integer || r instanceof Long) ? "_int" : "_double");
                ret.put("_ivalue", r);
            }
        }

        JsonRepresentation jr = new JsonRepresentation(ret);
        jr.setCharacterSet(CharacterSet.UTF_8);
        return jr;
    }

    @SuppressWarnings({ "unchecked", "rawtypes" })
    static private Object wrapInternal(Object o) throws JSONException {

        Object ret = null;

        if (o instanceof IRemoteSerializable) {
            Object orig = o;
            o = ((IRemoteSerializable) o).adapt();
            if (o instanceof Map<?, ?>) {
                String interf = getAPIInterface(orig);
                if (interf != null) {
                    // copy map in case we're using an immutable one.
                    o = new HashMap<Object, Object>((Map<?, ?>) o);
                    ((Map) o).put("_otype", interf);
                }
            }
        }

        if (o == null) {

            ret = JSONObject.NULL;

        } else if (o instanceof Map<?, ?>) {

            ret = new JSONObject();
            for (Object k : ((Map<?, ?>) o).keySet()) {
                if (k == null || ((Map<?, ?>) o).get(k) == null) {
                    // happens at times when remote service is restarting
                    continue;
                }
                put((JSONObject) ret, k.toString(), ((Map<?, ?>) o).get(k));
            }

        } else if (o instanceof List<?>) {

            ret = new JSONArray();
            for (Object e : ((List<?>) o)) {
                add((JSONArray) ret, e);
            }

        } else if (o instanceof Set<?>) {

            ret = new JSONArray();
            for (Object e : ((Set<?>) o)) {
                add((JSONArray) ret, e);
            }

        } else if (o.getClass().isArray()) {

            ret = new JSONArray();
            for (Object e : (Object[]) o) {
                add((JSONArray) ret, e);
            }

        } else if (o instanceof IList) {

            ret = new JSONArray();
            for (Object e : (IList) o) {
                add((JSONArray) ret, e);
            }

        } else if (o instanceof String || o instanceof Number) {
            return o;
        }

        return ret;
    }

    /*
     * stuck with idiotic org.json, which only uses literal polymorphic constructors and 
     * provides an idiotic one with Object arg that stringifies everything.
     */
    private static void put(JSONObject ret, String key, Object o) throws JSONException {

        if (o == null) {
            ret.put(key, JSONObject.NULL);
        } else if (o instanceof List<?>) {
            ret.put(key, wrapInternal(o));
        } else if (o instanceof Map<?, ?>) {
            ret.put(key, wrapInternal(o));
        } else if (o instanceof String) {
            ret.put(key, o);
        } else if (o instanceof Number) {
            ret.put(key, o);
        } else if (o instanceof IRemoteSerializable) {
            ret.put(key, wrapInternal(o));
        }
    }

    // never seen a worse API before.
    private static void add(JSONArray ret, Object o) throws JSONException {

        if (o == null) {
            ret.put(JSONObject.NULL);
        } else if (o instanceof List<?>) {
            ret.put(wrapInternal(o));
        } else if (o instanceof Map<?, ?>) {
            ret.put(wrapInternal(o));
        } else if (o instanceof String) {
            ret.put(o);
        } else if (o instanceof Number) {
            ret.put(o);
        } else if (o instanceof IRemoteSerializable) {
            ret.put(wrapInternal(o));
        }

    }

    public static JSONObject wrapException(Throwable e) {

        JSONObject ret = new JSONObject();
        try {
            ret.put("_type", "_exception");
            ret.put("_exceptionClass", e.getClass().getCanonicalName());
            ret.put("_exceptionMessage", e.getMessage() == null ? ExceptionUtils.getRootCauseMessage(e) : e
                    .getMessage());
            ret.put("_exceptionText", MiscUtilities.throwableToString(e));
        } catch (JSONException e1) {
            // give me a break
        }
        return ret;
    }
}
