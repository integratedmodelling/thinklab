/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.engine.services.administration;

import java.util.ArrayList;

import org.integratedmodelling.api.network.IComponent;
import org.integratedmodelling.api.network.INode;
import org.integratedmodelling.api.services.IServiceCall;
import org.integratedmodelling.api.services.annotations.Execute;
import org.integratedmodelling.api.services.annotations.Prototype;
import org.integratedmodelling.api.services.types.IAdminService;
import org.integratedmodelling.common.configuration.KLAB;
import org.integratedmodelling.common.network.Endpoints;
import org.integratedmodelling.exceptions.ThinklabException;
import org.integratedmodelling.exceptions.ThinklabResourceNotFoundException;

@Prototype(
        id = Endpoints.ADMIN_COMPONENTS,
        description = "submit component commands to network",
        args = {
                "? e|engine",
                Prototype.TEXT,
                "id",
                Prototype.TEXT
        })
public class Component implements IAdminService {

    @Execute(command = "setup")
    public Object setup(IServiceCall call) throws ThinklabException {

        ArrayList<Object> ret = new ArrayList<Object>();
        String id = call.get("id").toString();
        String engine = call.has("engine") ? call.get("engine").toString() : null;

        KLAB.info("calling setup for component " + id + " on "
                        + (engine == null ? "all connected nodes providing it"
                                : engine));

        if (engine != null && engine.equals("local")) {

            IComponent component = KLAB.CMANAGER.getComponent(id);
            if (component != null) {
                ((org.integratedmodelling.common.components.Component) component).setup();
            } else {
                throw new ThinklabResourceNotFoundException("component " + id
                        + " is not installed in local engine");
            }
        } else {

            for (INode node : KLAB.NETWORK.getNodes()) {
                if (node.providesComponent(id)
                        && (engine == null ? true : engine.equals(node.getId()))) {
                    ret.add(node.getId()
                            + ": "
                            + node.setupComponent(id));
                }
            }
        }

        return ret;

    }
}
