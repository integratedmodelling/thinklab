/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.engine.services.introspection;

import org.integratedmodelling.api.services.IServiceCall;
import org.integratedmodelling.api.services.annotations.Execute;
import org.integratedmodelling.api.services.annotations.Prototype;
import org.integratedmodelling.api.services.types.IAdminService;
import org.integratedmodelling.common.vocabulary.NS;
import org.integratedmodelling.engine.introspection.CallTracer;
import org.integratedmodelling.exceptions.ThinklabException;

/**
 * Trace tools. Subcommands:
 * 
 * calls - use CallTracer to document resolution.
 * queries - quick summary of recent model queries and their results (use 'q' command for details)
 * 
 * @author Ferd
 *
 */
@Prototype(id = "trace")
public class Trace implements IAdminService {

    @Execute(command = "calls")
    public Object execute(IServiceCall command)
            throws ThinklabException {
        CallTracer.dump(command.getSession().out());
        return null;
    }

    @Execute(command = "derived")
    public Object derived(IServiceCall command)
            throws ThinklabException {
        NS.dumpDerived(command.getSession().out());
        return null;
    }
}
