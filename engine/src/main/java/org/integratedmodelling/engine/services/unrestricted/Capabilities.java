/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.engine.services.unrestricted;

import java.util.ArrayList;
import java.util.HashMap;

import org.integratedmodelling.Version;
import org.integratedmodelling.api.engine.IEngine;
import org.integratedmodelling.api.services.IPrototype;
import org.integratedmodelling.api.services.IServiceCall;
import org.integratedmodelling.api.services.annotations.Execute;
import org.integratedmodelling.api.services.annotations.Prototype;
import org.integratedmodelling.common.command.ServiceManager;
import org.integratedmodelling.common.network.Endpoints;
import org.integratedmodelling.engine.KLABEngine;

/**
 * Public service returning full server capabilities.
 * 
 * @author ferdinando.villa
 *
 */
@Prototype(id = Endpoints.CAPABILITIES)
public class Capabilities {

    @Execute
    public Object getCapabilities(IServiceCall command) {

        HashMap<String, Object> oret = new HashMap<>();
        Runtime runtime = Runtime.getRuntime();

        /*
         * same stuff as Ping
         */
        String buildInfo = "";
        if (!Version.VERSION_BUILD.equals("VERSION_BUILD")) {
            buildInfo = " build " + Version.VERSION_BUILD + " (" + Version.VERSION_BRANCH + " "
                    + Version.VERSION_DATE + ")";
        }
        oret.put(IEngine.VERSION_STRING, new Version().toString() + buildInfo);
        oret.put(IEngine.BOOT_TIME_MS, KLABEngine.get().getBootTime());
        oret.put(IEngine.TOTAL_MEMORY_MB, runtime.totalMemory() / 1048576);
        oret.put(IEngine.FREE_MEMORY_MB, runtime.freeMemory() / 1048576);
        oret.put(IEngine.AVAILABLE_PROCESSORS, runtime.availableProcessors());

        /*
         * the actual capabilities
         */
        oret.put("functions", new ArrayList<IPrototype>(ServiceManager.get().getFunctionPrototypes()));
        oret.put("services", new ArrayList<IPrototype>(ServiceManager.get().getPrototypes()));

        return oret;
    }
}
