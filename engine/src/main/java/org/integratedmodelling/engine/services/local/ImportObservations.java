/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.engine.services.local;

import java.io.File;

import org.integratedmodelling.api.services.IServiceCall;
import org.integratedmodelling.api.services.annotations.Execute;
import org.integratedmodelling.api.services.annotations.Prototype;
import org.integratedmodelling.api.services.types.ISessionService;
import org.integratedmodelling.common.configuration.KLAB;
import org.integratedmodelling.common.network.Endpoints;
import org.integratedmodelling.engine.geospace.coverage.vector.VectorCoverage;
import org.integratedmodelling.exceptions.ThinklabAuthorizationException;
import org.integratedmodelling.exceptions.ThinklabException;
import org.integratedmodelling.exceptions.ThinklabResourceNotFoundException;
import org.integratedmodelling.exceptions.ThinklabUnsupportedOperationException;

/**
 * Take an observation created externally; optionally store it (in the system namespace) and/or observe it, as passed
 * or with forced spatio/temporal representation.
 * 
 * @author ferdinando.villa
 *
 */
@Prototype(id = Endpoints.IMPORT_OBSERVATIONS,
        args = {
                "source", Prototype.TEXT,
                "? s|simplify", Prototype.FLOAT,
                "? p|perimeter", Prototype.BOOLEAN,
                "? c|convex-hull", Prototype.BOOLEAN,
        })
public class ImportObservations implements ISessionService {

    @Execute
    public Object execute(IServiceCall command) throws ThinklabException {

        if (!KLAB.NETWORK.isPersonal()) {
            throw new ThinklabAuthorizationException("observations can only be imported by personal engines");
        }

        File f = new File(command.get("source").toString());

        if (!f.exists()) {
            throw new ThinklabResourceNotFoundException("resource " + f + " does not exist in the filesystem");
        }

        /*
         * TODO when it's worth it, we may want to use a plug-in system with components to 
         * recognize extensions, as we used to have.
         */
        if (f.toString().endsWith(".shp")) {
            return importShapefile(f);
        }

        throw new ThinklabUnsupportedOperationException("resource " + f
                + " cannot be imported: format not recognized");

    }

    private Object importShapefile(File f) throws ThinklabException {
        return VectorCoverage.readFeatures(f);
    }
}
