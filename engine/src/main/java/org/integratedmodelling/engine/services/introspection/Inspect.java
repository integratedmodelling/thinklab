/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.engine.services.introspection;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.integratedmodelling.api.knowledge.IConcept;
import org.integratedmodelling.api.network.IComponent;
import org.integratedmodelling.api.network.INode;
import org.integratedmodelling.api.services.IPrototype;
import org.integratedmodelling.api.services.IServiceCall;
import org.integratedmodelling.api.services.annotations.Execute;
import org.integratedmodelling.api.services.annotations.Prototype;
import org.integratedmodelling.common.configuration.KLAB;
import org.integratedmodelling.common.data.Datarecord;
import org.integratedmodelling.common.kim.KIMModelManager;
import org.integratedmodelling.common.utils.FileUtils;
import org.integratedmodelling.common.utils.StringUtils;
import org.integratedmodelling.common.vocabulary.NS;
import org.integratedmodelling.common.vocabulary.NS.RoleDescriptor;
import org.integratedmodelling.common.vocabulary.ObservationMetadata;
import org.integratedmodelling.engine.geospace.Geospace;
import org.integratedmodelling.engine.modelling.kbox.ModelData;
import org.integratedmodelling.engine.modelling.kbox.ModelKbox;
import org.integratedmodelling.engine.modelling.kbox.ObservationKbox;
import org.integratedmodelling.engine.network.DataAssetResolver;
import org.integratedmodelling.exceptions.ThinklabException;
import org.integratedmodelling.exceptions.ThinklabIOException;
import org.opengis.referencing.crs.CoordinateReferenceSystem;

/**
 * Trace tools. Subcommands:
 * 
 * calls - use CallTracer to document resolution.
 * queries - quick summary of recent model queries and their results (use 'q' command for details)
 * 
 * @author Ferd
 *
 */
@Prototype(
        id = "inspect",
        args = {
                "# arg0",
                Prototype.TEXT,
                "# arg1",
                Prototype.TEXT,
                "? rn|remove-newlines",
                Prototype.NONE,
                "? o|output",
                Prototype.TEXT })
public class Inspect {

    @Execute(command = "network")
    public Object execute(IServiceCall command)
            throws ThinklabException {

        List<String> ret = new ArrayList<>();

        ret.add("--- NETWORK NODES -----");

        for (INode n : KLAB.NETWORK.getNodes()) {

            ret.add("NODE " + n.getId());
            for (IPrototype p : n.getServices()) {
                ret.add("   Provides " + p);
            }
            for (String p : n.getSubmittingGroupSet()) {
                ret.add("   Can submit: " + p);
            }
            for (IComponent c : n.getResourceCatalog()
                    .getAuthorizedComponents(KLAB.NETWORK.getUser() == null ? null : KLAB.NETWORK.getUser()
                            .getGroups())) {
                ret.add("   Component: " + c);
            }

            ret.add("--------");
        }

        return ret;
    }

    @Execute(command = "derived")
    public Object derived(IServiceCall command)
            throws ThinklabException {
        NS.dumpDerived(command.getSession().out());
        return null;
    }

    @Execute(command = "urn", requires = { "arg0" })
    public Object testUrn(IServiceCall command)
            throws ThinklabException {
        Datarecord dr = DataAssetResolver.getDatarecord(command.get("arg0").toString(), command.getMonitor());
        command.getSession().out().println(dr == null ? "resolution failed" : dr.toString());
        return null;
    }

    @Execute(command = "projection", requires = { "arg0" })
    public Object testProjection(IServiceCall command)
            throws ThinklabException {
        CoordinateReferenceSystem crs = Geospace.getCRSFromID(command.get("arg0").toString());
        if (crs == null) {
            return "projection " + command.getString("arg0") + " is unknown";
        }
        return command.has("remove-newlines")
                ? StringUtils.pack(crs.toWKT().replace("\n", " ").replace("\r", " "))
                : crs.toWKT();
    }

    @Execute(command = "models")
    public Object models(IServiceCall command)
            throws ThinklabException {
        List<ModelData> ret = new ArrayList<>();
        for (ModelData md : ModelKbox.get().retrieveAll()) {
            ret.add(md);
        }
        return ret;
    }

    @Execute(command = "observations")
    public Object observations(IServiceCall command)
            throws ThinklabException {
        List<ObservationMetadata> ret = new ArrayList<>();
        for (ObservationMetadata md : ObservationKbox.get().retrieveAll()) {
            ret.add(md);
        }
        return ret;
    }

    @Execute(command = "ontologies")
    public Object exportOntologies(IServiceCall command)
            throws ThinklabException {
        NS.dumpOntologies(command.has("output") ? new File(command.get("output").toString()) : KLAB.CONFIG
                .getDataPath("outputs"));
        return null;
    }

    /**
     * Get a CSV file (or just JSON output) describing all the concepts known to the engine.
     * 
     * @param command
     * @return
     * @throws ThinklabException
     */
    @Execute(command = "concepts")
    public Object exportConcepts(IServiceCall command)
            throws ThinklabException {

        String namespace = null;
        if (command.has("arg0")) {
            namespace = command.getString("arg0");
        }

        List<String> ret = NS.dumpConcepts(namespace);

        if (ret != null && command.has("output")) {
            try {
                FileUtils.writeLines(new File(command.get("output").toString()), ret);
            } catch (IOException e) {
                throw new ThinklabIOException(e);
            }
        }
        return ret;
    }

    @Execute(command = "exports")
    public Object listExports(IServiceCall command)
            throws ThinklabException {
        List<String> ret = ((KIMModelManager) KLAB.MMANAGER).dumpExports();
        if (ret != null && command.has("output")) {
            try {
                FileUtils.writeLines(new File(command.get("output").toString()), ret);
            } catch (IOException e) {
                throw new ThinklabIOException(e);
            }
        }
        return ret;
    }

    @Execute(command = "level", requires = { "arg0", "arg1" })
    public Object getLevel(IServiceCall command)
            throws ThinklabException {
        int ret = -1;
        IConcept c = KLAB.c(command.getString("arg0"));
        if (c != null) {
            ret = NS.getDetailLevel(c, command.getString("arg1"));
        }
        return ret;
    }

    @Execute(command = "roles")
    public Object roles(IServiceCall command)
            throws ThinklabException {
        List<Map<?, ?>> ret = new ArrayList<>();
        for (RoleDescriptor rd : NS.getRoles()) {
            ret.add(rd.dump());
        }
        return ret;
    }
}
