/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.engine.services.unrestricted;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.integratedmodelling.Version;
import org.integratedmodelling.api.engine.IEngine;
import org.integratedmodelling.api.network.INode;
import org.integratedmodelling.api.services.IServiceCall;
import org.integratedmodelling.api.services.annotations.Execute;
import org.integratedmodelling.api.services.annotations.Prototype;
import org.integratedmodelling.common.configuration.KLAB;
import org.integratedmodelling.common.network.Endpoints;
import org.integratedmodelling.common.network.Network;
import org.integratedmodelling.common.utils.SetUtils;
import org.integratedmodelling.common.utils.StringUtils;
import org.integratedmodelling.engine.KLABEngine;
import org.integratedmodelling.exceptions.ThinklabException;

/**
 * Public service returning only server ID, version and authentication endpoint.
 * 
 * @author ferdinando.villa
 *
 */
@Prototype(id = Endpoints.IDENTIFY)
public class Identify {

    @Execute
    public Object getCapabilities(IServiceCall command) {

        HashMap<String, Object> oret = new HashMap<>();
        Runtime runtime = Runtime.getRuntime();

        /*
         * same stuff as Ping
         */
        String buildInfo = "";
        if (!Version.VERSION_BUILD.equals("VERSION_BUILD")) {
            buildInfo = " build " + Version.VERSION_BUILD + " (" + Version.VERSION_BRANCH + " "
                    + Version.VERSION_DATE + ")";
        }

        oret.put(IEngine.VERSION_STRING, new Version().toString() + buildInfo);
        oret.put(IEngine.BOOT_TIME_MS, KLABEngine.get().getBootTime());
        oret.put(IEngine.TOTAL_MEMORY_MB, runtime.totalMemory() / 1048576);
        oret.put(IEngine.FREE_MEMORY_MB, runtime.freeMemory() / 1048576);
        oret.put(IEngine.AVAILABLE_PROCESSORS, runtime.availableProcessors());

        if (KLAB.NETWORK.getAuthenticationEndpoint() != null) {
            oret.put(IEngine.AUTH_ENDPOINT, KLAB.NETWORK.getAuthenticationEndpoint());
        }

        if (command.hasEngineAuthorization() || KLAB.NETWORK.isPersonal()) {

            boolean useGroups = false; // TODO

            List<String> network = new ArrayList<>();

            /*
             * if we have been passed our own server authentication key, respond with unrestricted node list complete with
             * nodes harvested from the network. If we also have a user authentication key, check that we have been connected to a
             * and respond with filtered network list. Receiving end will call getServices() on each node to update services.
             */
            Set<String> nids = new HashSet<>();
            nids.add(KLAB.NAME);
            network.add(KLAB.NAME + "|" + KLAB.NETWORK.getUrl() + "|" + KLAB.NETWORK.getKey());

            String relationships = "";

            for (String s : ((Network) KLAB.NETWORK).getConnectedNodeNames()) {

                INode n = KLAB.NETWORK.getNode(s);

                if (n == null || !n.isActive()) {
                    continue;
                }

                if (useGroups
                        && !SetUtils.containsAny(n.getResourceCatalog().getAuthorizedGroups(), command
                                .getRequesterGroups())) {
                    continue;
                }

                if (command.getCallerAuthorizationKey() != null
                        && n.getKey().equals(command.getCallerAuthorizationKey())) {
                    // harmless, but avoids unnecessary web traffic.
                    continue;
                }

                relationships += (relationships.isEmpty() ? "" : "|") + KLAB.NAME + "," + s;

                if (!nids.contains(n.getId())) {
                    nids.add(n.getId());
                    network.add(n.getId() + "|" + n.getUrl() + "|" + n.getKey());
                }
                try {
                    Object o = n.get(Endpoints.IDENTIFY);
                    if (o instanceof Map && ((Map<?, ?>) o).containsKey("network")) {
                        List<?> nodes = (List<?>) ((Map<?, ?>) o).get("network");
                        for (int i = 0; i < nodes.size(); i++) {
                            if (i == nodes.size() - 1) {
                                relationships += ((relationships.isEmpty()
                                        || nodes.get(i).toString().isEmpty()) ? "" : "|")
                                        + nodes.get(i).toString();
                            } else {
                                String rnl = nodes.get(i).toString();
                                String[] ss = rnl.split("\\|");
                                if (!nids.contains(ss[0])) {
                                    nids.add(ss[0]);
                                    network.add(rnl);
                                }
                            }
                        }
                    }
                } catch (ThinklabException e) {
                    KLAB.warn("error calling identify on node " + n.getId(), e);
                    // just don't.
                }
            }

            if (network.size() > 0) {
                network.add(relationships);
            }
            oret.put("network", network);
        }

        if (!KLAB.NETWORK.getResourceCatalog().getAuthorizedGroups().isEmpty()) {
            oret.put(IEngine.AUTHORIZED_GROUPS, StringUtils.join(KLAB.NETWORK.getResourceCatalog()
                    .getAuthorizedGroups(), ","));
        }

        return oret;
    }
}
