/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.engine.services.local;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import org.integratedmodelling.Version;
import org.integratedmodelling.api.engine.IEngine;
import org.integratedmodelling.api.monitoring.IMonitor;
import org.integratedmodelling.api.monitoring.INotification;
import org.integratedmodelling.api.services.IServiceCall;
import org.integratedmodelling.api.services.annotations.Execute;
import org.integratedmodelling.api.services.annotations.Prototype;
import org.integratedmodelling.api.services.types.ILocalService;
import org.integratedmodelling.common.monitoring.Notification;
import org.integratedmodelling.common.network.Endpoints;
import org.integratedmodelling.engine.KLABEngine;
import org.integratedmodelling.engine.runtime.Session;
import org.integratedmodelling.exceptions.ThinklabException;

@Prototype(id = Endpoints.GET_NOTIFICATIONS,
        args = {
                "# send", Prototype.TEXT,
                "# clear", Prototype.BOOLEAN,
                "# start", Prototype.INT,
                "# taskId", Prototype.INT
        })
public class GetNotifications implements ILocalService {

    @Execute
    public Object authenticate(IServiceCall call) throws ThinklabException {

        Map<String, Object> ret = new HashMap<>();
        Runtime runtime = Runtime.getRuntime();
        ret.put(IEngine.VERSION_STRING, Version.CURRENT);
        ret.put(IEngine.BOOT_TIME_MS, KLABEngine.get().getBootTime());
        ret.put(IEngine.TOTAL_MEMORY_MB, (runtime.totalMemory() / 1048576l));
        ret.put(IEngine.FREE_MEMORY_MB, (runtime.freeMemory() / 1048576l));
        ret.put(IEngine.AVAILABLE_PROCESSORS, runtime.availableProcessors());

        /*
         * time running
         */
        long bt = new Date().getTime() - KLABEngine.get().getBootTime();
        String ut = String.format(
                "%02d:%02d min",
                TimeUnit.MILLISECONDS.toMinutes(bt),
                TimeUnit.MILLISECONDS.toSeconds(bt)
                        - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(bt)));
        ret.put(IEngine.UPTIME_TEXT, ut);

        boolean clear = Boolean.parseBoolean(call.getString("clear", "false"));
        int start = Integer.parseInt(call.getString("start", "0"));
        String message = call.getString("send");

        if (call.getSession() != null) {

            if (message != null) {
                int taskId = Integer.parseInt(call.getString("taskId", "-1"));
                IMonitor monitor = ((Session) call.getSession()).getTaskMonitor(taskId);
                if (monitor != null) {
                    monitor.send(new Notification(message, call.getSession()));
                }

            } else {

                List<INotification> notifications = call.getSession().getNotifications(clear);

                if (start > 0)
                    notifications = notifications.subList(start, notifications.size());

                ret.put("notifications", notifications);
            }
        }
        return ret;

    }
}
