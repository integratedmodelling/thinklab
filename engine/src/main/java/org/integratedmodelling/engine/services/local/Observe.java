/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.engine.services.local;

import java.util.ArrayList;
import java.util.List;

import org.integratedmodelling.api.modelling.IActiveSubject;
import org.integratedmodelling.api.modelling.IExtent;
import org.integratedmodelling.api.modelling.IObservation;
import org.integratedmodelling.api.modelling.ISubject;
import org.integratedmodelling.api.runtime.IContext;
import org.integratedmodelling.api.runtime.ITask;
import org.integratedmodelling.api.services.IServiceCall;
import org.integratedmodelling.api.services.annotations.Execute;
import org.integratedmodelling.api.services.annotations.Prototype;
import org.integratedmodelling.api.services.types.ISessionService;
import org.integratedmodelling.common.model.runtime.Space;
import org.integratedmodelling.common.model.runtime.Time;
import org.integratedmodelling.common.network.Endpoints;
import org.integratedmodelling.engine.runtime.Context;
import org.integratedmodelling.engine.runtime.Session;
import org.integratedmodelling.exceptions.ThinklabException;
import org.integratedmodelling.exceptions.ThinklabValidationException;

@Prototype(
        id = Endpoints.OBSERVE,
        args = {
                "observable",
                Prototype.TEXT,
                "# target",
                Prototype.TEXT,
                "? c|context",
                Prototype.INT,
                "? s|scenario",
                Prototype.TEXT,
                "? r|reset",
                "true|false",
                "? a|asynchronous",
                "true|false",
                "# force-space", Prototype.TEXT,
                "# force-time", Prototype.TEXT },
        argDescriptions = {
                "what to observe",
                "path to context subject for observation (if empty, observable must be a subject observer)",
                "context (must be passed if path is not empty)",
                "scenario(s)",
                "reset context to pristine",
                "run asynchronously",
                "spatial scaling to force (subjects only)",
                "temporal scaling to force (subjects only)" })
public class Observe implements ISessionService {

    static final String PREVIOUS_CONTEXT_ID  = "_previous_context";
    static final String PREVIOUS_OBSERVER_ID = "_previous_observer";

    @Execute
    public Object execute(IServiceCall command) throws ThinklabException {

        String observableName = command.get("observable").toString();
        String pathToTarget = command.has("target") ? command.get("target").toString() : null;
        List<String> scenarios = new ArrayList<String>();
        boolean reset = command.has("reset")
                && Boolean.parseBoolean(command.get("reset").toString());

        if (command.has("scenario")) {
            for (String s : command.get("scenario").toString().split(",")) {
                scenarios.add(s);
            }
        }

        ArrayList<IExtent> forcings = new ArrayList<>();
        if (command.has("force-space")) {
            forcings.add(new Space(command.get("force-space").toString()));
        }
        if (command.has("force-time")) {
            forcings.add(new Time(command.get("force-time").toString()));
        }

        long context = -1l;
        if (command.has("context")) {
            context = Long.parseLong(command.get("context").toString());
        }

        if (pathToTarget != null && context < 0) {
            throw new ThinklabValidationException("illegal observe call: no path to target and no context identified.");
        }

        ITask ret = null;
        if (context < 0) {

            IContext ctx = Context
                    .createDeferred(observableName, command.getSession(), command.getMonitor(), forcings);
            ((Session) command.getSession()).pushContext(ctx);
            if (scenarios.size() > 0) {
                ctx = ctx.inScenario(scenarios.toArray(new String[scenarios.size()]));
            }

            ret = ctx.observeAsynchronous();

        } else {

            IContext ctx = ((Session) command.getSession()).getContext(context);
            if (ctx == null) {
                throw new ThinklabValidationException("context " + context + " does not exist");
            }
            if (reset) {
                ctx = ctx.resetContext(reset);
            }
            if (scenarios.size() > 0) {
                ctx = ctx.inScenario(scenarios.toArray(new String[scenarios.size()]));
            }
            IObservation subject = ctx.get(pathToTarget);
            if (!(subject instanceof IActiveSubject)) {
                throw new ThinklabValidationException("observe: observation target identified is not a subject: "
                        + subject);
            }

            ret = ((Context) ctx).observeAsynchronous(observableName, (ISubject) subject);
        }

        return ret;
    }
}
