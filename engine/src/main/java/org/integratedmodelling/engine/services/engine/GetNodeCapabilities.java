/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.engine.services.engine;

import java.util.ArrayList;

import org.integratedmodelling.api.network.IComponent;
import org.integratedmodelling.api.project.IProject;
import org.integratedmodelling.api.services.IPrototype;
import org.integratedmodelling.api.services.IServiceCall;
import org.integratedmodelling.api.services.annotations.Execute;
import org.integratedmodelling.api.services.annotations.Prototype;
import org.integratedmodelling.common.configuration.KLAB;
import org.integratedmodelling.common.network.Endpoints;
import org.integratedmodelling.common.utils.MapUtils;
import org.integratedmodelling.exceptions.ThinklabException;

/**
 * Return all the public services that this engine provides and the projects it distributes authoritatively. If 
 * a group list is passed, only include those that are available to those groups.
 * 
 * @author Ferd
 *
 */
@Prototype(id = Endpoints.GET_NODE_CAPABILITIES)
public class GetNodeCapabilities {

    @Execute
    public Object getServices(IServiceCall call) throws ThinklabException {

        /*
         * all authorizations OK, return own public capabilities and list of nodes we know.
         */
        ArrayList<IPrototype> services = new ArrayList<>();
        ArrayList<String> projects = new ArrayList<>();
        ArrayList<IComponent> components = new ArrayList<>();
        ArrayList<String> submitters = new ArrayList<>();

        // Env.logger.info("requesting groups are " + call.getRequesterGroups());

        for (IComponent component : KLAB.NETWORK.getResourceCatalog()
                .getAuthorizedComponents(call.isFromKnownEngine() ? null : call.getRequesterGroups())) {

            // Env.logger.info("authorizing component " + component.getId());

            components.add(component);
            for (IPrototype srv : component.getAPI()) {
                /*
                 * return both published and unpublished - the requesting engine will make sure only published
                 * services are seen, but admins should be able to call the unpublished ones.
                 */
                services.add(srv);
            }
        }

        for (String s : KLAB.NETWORK.getAuthorizedSubmitters()) {
            submitters.add(s);
        }

        for (IProject project : KLAB.NETWORK.getResourceCatalog()
                .getAuthorizedProjects(call.isFromKnownEngine() ? null : call.getRequesterGroups())) {

            // Env.logger.info("authorizing project: " + project.getId());

            projects.add(project.getId());
        }

        return MapUtils
                .of("components", components, "services", services, "projects", projects, "submitters", submitters);
    }
}
