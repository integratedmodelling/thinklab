/*******************************************************************************
 *  Copyright (C) 2007, 2014:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.engine.services.commandline;

import org.integratedmodelling.api.services.IServiceCall;
import org.integratedmodelling.api.services.annotations.Execute;
import org.integratedmodelling.api.services.annotations.Prototype;
import org.integratedmodelling.engine.rest.RESTManager;
import org.integratedmodelling.exceptions.ThinklabException;

/**
 * Start and stop the REST service.
 * 
 * @author ferdinando.villa
 *
 */

@Prototype(id = "rest", args = { "? p|port", Prototype.INT, "? s|server", Prototype.NONE })
public class Rest {

    @Execute(command = "start")
    public Object start(IServiceCall command) throws ThinklabException {

        String ept = System.getenv("THINKLAB_REST_PORT");
        if (ept == null)
            ept = "8182";

        String ctx = System.getenv("THINKLAB_REST_CONTEXT");
        if (ctx == null)
            ctx = "rest";

        Integer port = Integer.parseInt(ept);

        if (command.has("port"))
            port = ((Number) (command.get("port"))).intValue();

        RESTManager.get().start(ctx, port);

        if (command.has("server")) {
            while (true) {
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    break;
                }
            }
        }

        return null;
    }

    @Execute(command = "start")
    public Object stop(IServiceCall command) throws ThinklabException {

        String ept = System.getenv("THINKLAB_REST_PORT");
        if (ept == null)
            ept = "8182";

        String ctx = System.getenv("THINKLAB_REST_CONTEXT");
        if (ctx == null)
            ctx = "rest";

        Integer port = Integer.parseInt(ept);

        if (command.has("port"))
            port = ((Number) (command.get("port"))).intValue();

        RESTManager.get().stop(port);

        return null;
    }

    @Execute(command = "restart")
    public Object execute(IServiceCall command) throws ThinklabException {

        stop(command);
        try {
            Thread.sleep(500);
        } catch (InterruptedException e) {
            // come on
        }

        return start(command);
    }

}
