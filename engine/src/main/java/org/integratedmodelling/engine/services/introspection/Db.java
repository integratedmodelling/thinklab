/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/

//package org.integratedmodelling.engine.services.introspection;
//
//import java.util.ArrayList;
//
//import org.integratedmodelling.api.modelling.storage.IModelData;
//import org.integratedmodelling.api.services.IServiceCall;
//import org.integratedmodelling.api.services.annotations.Execute;
//import org.integratedmodelling.api.services.annotations.Prototype;
//import org.integratedmodelling.engine.modelling.kbox.ModelKbox;
//import org.integratedmodelling.exceptions.ThinklabException;
//
///**
// * Database inspector. Subcommands:
// * 
// * stored  - list all objects stored in this session and their ID.
// * rm <id> - remove object with given ID.
// * find <type> - list all objects of given type.
// * 
// * @author Ferd
// *
// */
//@Prototype(
//        id = "db",
//        args = { "# arg", Prototype.TEXT })
//public class Db  implements IAdminService {
//
//    @Execute(command = "list")
//    public Object stored(IServiceCall command) throws ThinklabException {
//
//        ArrayList<String> ret = new ArrayList<>();
//        for (IModelData md : ModelKbox.get().retrieveAll()) {
//            ret.add("---------------\n");
//            ret.add("" + md);
//        }
//
//        return ret; // DataRecorder.get().list(NeoKBox.STORAGE_DEBUG_ID);
//
//    }
//
//    @Execute(command = "find")
//    public Object find(IServiceCall command) throws ThinklabException {
//
//        String c = command.get("arg").toString();
//        // IKbox kbox = Thinklab.get().requireKbox("thinklab");
//        // command.getSession().out().println(kbox.query(Queries.select(Thinklab.c(c))).toString());
//
//        return null;
//    }
//
//    @Execute(command = "rm")
//    public Object rm(IServiceCall command) throws ThinklabException {
//
//        int n = Integer.parseInt(command.get("arg").toString());
//
//        // IKbox kbox = Thinklab.get().requireKbox("thinklab");
//        // kbox.remove(n);
//
//        return null;
//    }
//
// }
