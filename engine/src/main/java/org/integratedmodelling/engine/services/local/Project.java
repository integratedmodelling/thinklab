/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.engine.services.local;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.UUID;

import org.integratedmodelling.api.modelling.INamespace;
import org.integratedmodelling.api.monitoring.IMonitor;
import org.integratedmodelling.api.monitoring.Messages;
import org.integratedmodelling.api.project.IProject;
import org.integratedmodelling.api.runtime.IContext;
import org.integratedmodelling.api.services.IServiceCall;
import org.integratedmodelling.api.services.annotations.Execute;
import org.integratedmodelling.api.services.annotations.Prototype;
import org.integratedmodelling.api.services.types.IEngineService;
import org.integratedmodelling.api.services.types.ILocalService;
import org.integratedmodelling.common.configuration.KLAB;
import org.integratedmodelling.common.monitoring.Notification;
import org.integratedmodelling.common.network.Endpoints;
import org.integratedmodelling.common.utils.MiscUtilities;
import org.integratedmodelling.common.utils.ZipUtils;
import org.integratedmodelling.engine.modelling.kbox.ModelKbox;
import org.integratedmodelling.engine.modelling.kbox.ObservationKbox;
import org.integratedmodelling.engine.modelling.monitoring.Monitor;
import org.integratedmodelling.engine.rest.FileMedia;
import org.integratedmodelling.engine.rest.RESTManager;
import org.integratedmodelling.engine.runtime.AbstractBaseTask;
import org.integratedmodelling.exceptions.ThinklabException;
import org.integratedmodelling.exceptions.ThinklabIOException;
import org.integratedmodelling.exceptions.ThinklabResourceNotFoundException;
import org.restlet.data.MediaType;

@Prototype(
        id = Endpoints.PROJECT,
        args = {
                "# plugin",
                Prototype.TEXT,
                "# load",
                Prototype.BOOLEAN,
                "# directory",
                Prototype.TEXT,
                "# reload",
                Prototype.BOOLEAN,
        })
public class Project implements ILocalService, IEngineService {

    @Execute(command = "deploy")
    public Object deploy(IServiceCall call) throws ThinklabException {

        String pluginId = call.getString("plugin");
        boolean load = call.has("load") && (Boolean) call.get("load");
        IMonitor monitor = call.getSession() == null ? KLAB.CMANAGER.getMonitor()
                : call.getSession().getMonitor();

        File archive = call.getFile();
        KLAB.info("archive " + archive + " received for project " + pluginId);
        IProject project = KLAB.PMANAGER
                .deployProject(pluginId, archive.toString(), monitor);

        if (load && project != null) {

            KLAB.PMANAGER.unloadProject(pluginId);
            KLAB.info("loading deployed project " + pluginId);
            KLAB.PMANAGER.loadProject(project.getId(), KLAB.MFACTORY.getRootParsingContext());
            KLAB.info("finished loading project " + pluginId);
        }

        return null;
    }

    @Execute(command = "list")
    public Object list(IServiceCall call) throws ThinklabException {
        ArrayList<IProject> ret = new ArrayList<>();
        for (IProject p : KLAB.PMANAGER.getProjects()) {
            ret.add(p);
        }
        return ret;
    }

    @Execute(command = "undeploy")
    public Object undeploy(IServiceCall call) throws ThinklabException {
        String pluginId = call.getString("plugin");
        KLAB.info("undeploying project " + pluginId);
        KLAB.PMANAGER.undeployProject(pluginId);
        return null;
    }

    @Execute(command = "pack")
    public Object pack(IServiceCall call) throws ThinklabException {

        /*
         * make an archive from the project and return the handle
         */
        String pluginId = call.getString("plugin");
        IProject tp = KLAB.PMANAGER.getProject(pluginId);
        if (tp == null)
            throw new ThinklabResourceNotFoundException("project " + pluginId + " does not exist");

        File tempFile = null;
        try {
            tempFile = File.createTempFile("prj", "zip");
            ZipUtils.zip(tempFile, tp.getLoadPath(), true, false);
            return new FileMedia(tempFile, MediaType.APPLICATION_ZIP);
        } catch (IOException e) {
            throw new ThinklabIOException(e);
        }
    }

    @Execute(command = "register")
    public Object register(IServiceCall call) throws ThinklabException {

        /*
         * notify that a project is waiting in a particular directory. Use instead of deploy in embedded
         * servers where the server filesystem is available to the client, or for special purposes.
         * Admits several directories separated by commas.
         */
        for (String s : call.getString("directory").split(",")) {

            File file = new File(s);
            if (!file.exists() || !file.isDirectory())
                throw new ThinklabResourceNotFoundException("directory " + file
                        + " not found on filesystem");

            String projectId = MiscUtilities.getFileName(s);
            KLAB.info("registering project " + projectId + " from " + file);
            KLAB.PMANAGER.registerProject(file);
        }

        return null;

    }

    @Execute(command = "load")
    public Object load(IServiceCall call) throws ThinklabException {

        boolean nochange = call.getString("reload", "true").equals("false");
        String pluginId = call.getString("plugin");
        IMonitor monitor = call.getSession() == null ? KLAB.CMANAGER.getMonitor()
                : call.getSession().getMonitor();

        if (pluginId == null) {
            /*
             * load asynchronously and let the client figure out whether to wait or not.
             */
            ProjectLoadTask task = new ProjectLoadTask(monitor, !nochange);
            task.start();
            return task;

        }

        IProject project = KLAB.PMANAGER.getProject(pluginId);
        if (project == null) {
            throw new ThinklabResourceNotFoundException("project " + pluginId + " not registered");
        }

        if (nochange && ((org.integratedmodelling.common.project.Project) project).isLoaded()) {
            KLAB.info("project " + pluginId + " unchanged");
        } else {
            KLAB.info("loading project " + pluginId);
            KLAB.PMANAGER.loadProject(pluginId, KLAB.MFACTORY.getRootParsingContext());
        }

        return null;
    }

    @Execute(command = "init-components")
    public Object initComponents(IServiceCall call) throws ThinklabException {

        KLAB.CMANAGER.link();
        return null;
    }

    @Execute(command = "export")
    public Object export(IServiceCall call) throws ThinklabException {

        String pluginId = call.getString("plugin");
        HashMap<String, Object> ret = new HashMap<>();

        IProject project = KLAB.PMANAGER.getProject(pluginId);
        if (project == null) {
            throw new ThinklabResourceNotFoundException("project " + pluginId + " not registered");
        }

        String accessKey = UUID.randomUUID().toString();
        RESTManager.get().getApplication().mapDirectoryToKey(project.getLoadPath(), accessKey);

        /*
         * prepare URL with server key and export the necessary ninfo:
         *  required projects list
         *  namespace list
         *  list of relative URLs to binary assets, packed if necessary
         *  list of relative URLs to data assets, packed if necessary
         */
        ArrayList<String> nss = new ArrayList<>();
        ArrayList<String> pss = new ArrayList<>();

        for (INamespace ns : project.getNamespaces()) {
            nss.add(ns.getId());
        }
        for (IProject p : project.getPrerequisites()) {
            pss.add(p.getId());
        }

        ret.put("access-key", accessKey);
        ret.put("namespaces", nss);
        ret.put("prerequisites", pss);
        ret.put("data-assets", ((org.integratedmodelling.common.project.Project) project)
                .getDataAssetPaths());
        ret.put("binary-assets", ((org.integratedmodelling.common.project.Project) project)
                .getBinaryAssetPaths());
        ret.put("src-path", ((org.integratedmodelling.common.project.Project) project).getRelativeSrcPath());
        ret.put("lib-path", ((org.integratedmodelling.common.project.Project) project).getRelativeLibPath());

        return ret;
    }

    public class ProjectLoadTask extends AbstractBaseTask {

        boolean forceReload = false;

        public ProjectLoadTask(IMonitor monitor, boolean forceReload) {
            super(monitor);
            _monitor = new Monitor(getTaskId(), monitor.getSession());
            this.forceReload = forceReload;
            this._description = "Refreshing projects";
        }

        @Override
        public IContext getContext() {
            // TODO Auto-generated method stub
            return null;
        }

        @Override
        public IContext finish() {
            return (IContext) finish(null);
        }

        @Override
        public void run() {

            _monitor.send(new Notification(Messages.TASK_STARTED, _session, this));

            try {

                /*
                 * in personal engines, no need to keep reallocating connections in what can
                 * potentially be a slow operation.
                 */
                if (KLAB.NETWORK.isPersonal()) {
                    ModelKbox.get().getDatabase().preallocateConnection();
                    ObservationKbox.get().getDatabase().preallocateConnection();
                }

                KLAB.PMANAGER.load(forceReload, KLAB.MFACTORY.getRootParsingContext());

                /*
                 * restore multi-user even in personal engines as we may be using this from
                 * concurrent clients.
                 */
                if (KLAB.NETWORK.isPersonal()) {
                    ModelKbox.get().getDatabase().deallocateConnection();
                    ObservationKbox.get().getDatabase().deallocateConnection();
                }

            } catch (Exception e) {
                synchronized (_status) {
                    _status = Status.ERROR;
                }
                _monitor.send(new Notification(Messages.TASK_FAILED, _session, this, MiscUtilities
                        .throwableToString(e)));
            } finally {
                if (_status != Status.ERROR) {
                    _monitor.send(new Notification(Messages.TASK_FINISHED, _session, this));
                }
                _endTime = new Date().getTime();
                synchronized (_status) {
                    if (_status != Status.ERROR) {
                        _status = Status.FINISHED;
                    }
                }
            }

        }
    }

}
