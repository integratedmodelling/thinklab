/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.engine.services.engine;

import java.util.ArrayList;
import java.util.List;

import org.integratedmodelling.api.knowledge.IConcept;
import org.integratedmodelling.api.modelling.IScale;
import org.integratedmodelling.api.services.IServiceCall;
import org.integratedmodelling.api.services.annotations.Execute;
import org.integratedmodelling.api.services.annotations.Prototype;
import org.integratedmodelling.api.services.types.IEngineService;
import org.integratedmodelling.api.services.types.ISessionService;
import org.integratedmodelling.common.model.runtime.Scale;
import org.integratedmodelling.common.network.Endpoints;
import org.integratedmodelling.engine.KLABEngine;
import org.integratedmodelling.engine.modelling.kbox.ObservationKbox;
import org.integratedmodelling.exceptions.ThinklabException;

/**
 * Either the query string or the three other args must be passed.
 * 
 * @author ferdinando.villa
 *
 */
@Prototype(id = Endpoints.QUERY_OBSERVATIONS,
        args = {
                "# query-string", Prototype.TEXT,
                "# local-only", Prototype.BOOLEAN,
                "# type", Prototype.CONCEPT,
                "# scenarios", Prototype.TEXT,
                "# scale", Prototype.TEXT
        })
public class QueryObservations implements IEngineService, ISessionService {

    @Execute
    public Object execute(IServiceCall command) throws ThinklabException {

        boolean localOnly = command.has("local-only") && command.getString("local-only").equals("true");

        if (command.has("query-string")) {
            return ObservationKbox.get().query(command.getString("query-string"), localOnly);
        }

        List<String> scenarios = new ArrayList<>();
        if (command.has("scenarios")) {
            for (String s : command.get("scenario").toString().split(",")) {
                scenarios.add(s);
            }
        }

        IScale scale = new Scale(command.get("scale").toString());
        List<IConcept> concepts = splitKnowledge(command.get("type").toString());
        return ObservationKbox.get().query(concepts, scale, scenarios, localOnly);
    }

    private List<IConcept> splitKnowledge(String string) {
        List<IConcept> ret = new ArrayList<>();
        for (String s : string.split(",")) {
            IConcept c = KLABEngine.get().getConcept(s);
            if (c != null) {
                ret.add(c);
            }
        }
        return ret;
    }
}
