/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.engine.services.local;

import java.util.Collection;

import org.integratedmodelling.api.runtime.ISession;
import org.integratedmodelling.api.services.IServiceCall;
import org.integratedmodelling.api.services.annotations.Execute;
import org.integratedmodelling.api.services.annotations.Prototype;
import org.integratedmodelling.api.services.types.ILocalService;
import org.integratedmodelling.common.configuration.KLAB;
import org.integratedmodelling.common.network.Endpoints;
import org.integratedmodelling.common.network.Network;
import org.integratedmodelling.common.utils.MapUtils;
import org.integratedmodelling.engine.rest.RESTManager;
import org.integratedmodelling.exceptions.ThinklabAuthorizationException;
import org.integratedmodelling.exceptions.ThinklabException;

/**
 * Authenticate is a local service (only runs on a personal engine) that will let a client in and create
 * a session ID for further operations (ISessionService) as long as the username that is being authenticated
 * is the same as the one the engine is initialized with.
 * 
 * @author ferdinando.villa
 *
 */
@Prototype(id = Endpoints.AUTHENTICATE,
        args = { "username", Prototype.TEXT })
public class Authenticate implements ILocalService {

    @Execute
    public Object authenticate(IServiceCall call) throws ThinklabException {

        if (!KLAB.NETWORK.getUser().getUsername().equals(call.getString("username"))) {
            throw new ThinklabAuthorizationException("cannot authorize " + call.getString("username")
                    + ": personal engine is locked by a different user");
        }
        Collection<String> sessions = RESTManager.get().getSessionIdsForUser(KLAB.NETWORK.getUser());
        ISession session = RESTManager.get().createRESTSession(KLAB.NETWORK.getUser());
        return MapUtils
                .of("session", session.getId(), "sessions", sessions, "network", ((Network) KLAB.NETWORK)
                        .getStructureAsList());
    }
}
