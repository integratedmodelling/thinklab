/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.engine.services.engine;

import org.integratedmodelling.api.auth.IUser;
import org.integratedmodelling.api.services.IServiceCall;
import org.integratedmodelling.api.services.annotations.Execute;
import org.integratedmodelling.api.services.annotations.Prototype;
import org.integratedmodelling.common.configuration.KLAB;
import org.integratedmodelling.common.network.Endpoints;
import org.integratedmodelling.engine.rest.RESTManager;
import org.integratedmodelling.exceptions.ThinklabAuthorizationException;
import org.integratedmodelling.exceptions.ThinklabException;

/**
 * Called by a personal engine that wants to get on the network through us. The user has been authenticated by
 * the authentication service and its (temporary) auth token is sent along for double-checking with the authentication
 * endpoint, which we have sent to the client before. If the credentials match, we send back the network 
 * filtered according to the user's groups.
 * 
 * @author Ferd
 *
 */
@Prototype(id = Endpoints.CONNECT, args = { "username", Prototype.TEXT, "key", Prototype.TEXT })
public class Connect {

    @Execute
    public Object connect(IServiceCall call) throws ThinklabException {

        IUser user = RESTManager.get().checkAuthentication(call.getString("username"), call.getString("key"));
        if (user == null) {
            throw new ThinklabAuthorizationException("cannot authenticate connection: access to network denied");
        }

        /*
         * TODO/FIXME should return a time-limited key tied to a specific view of the network and temporary - not the official
         * server key. All servers in the network should do the same, which makes it a bit complicated for now.
         */
        return KLAB.NETWORK.getKey();
    }
}
