/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.engine.services.engine;

import org.integratedmodelling.api.knowledge.IConcept;
import org.integratedmodelling.api.modelling.IDirectObserver;
import org.integratedmodelling.api.modelling.INamespace;
import org.integratedmodelling.api.services.IServiceCall;
import org.integratedmodelling.api.services.annotations.Execute;
import org.integratedmodelling.api.services.annotations.Prototype;
import org.integratedmodelling.api.services.types.IEngineService;
import org.integratedmodelling.api.services.types.ISessionService;
import org.integratedmodelling.common.configuration.KLAB;
import org.integratedmodelling.common.kim.KIMNamespace;
import org.integratedmodelling.common.kim.ModelFactory;
import org.integratedmodelling.common.model.runtime.Scale;
import org.integratedmodelling.common.network.Endpoints;
import org.integratedmodelling.engine.modelling.kbox.ObservationKbox;
import org.integratedmodelling.exceptions.ThinklabException;

/**
 * Take an observation created externally; optionally store it (in the system namespace) and/or observe it, as passed
 * or with forced spatio/temporal representation.
 * 
 * @author ferdinando.villa
 *
 */
@Prototype(
        id = Endpoints.SUBMIT_OBSERVATION,
        args = {
                "type",
                Prototype.CONCEPT,
                "id",
                Prototype.TEXT,
                "scale",
                Prototype.TEXT,
                "# store",
                Prototype.BOOLEAN
        })
public class SubmitObservation implements IEngineService, ISessionService {

    @Execute
    public Object execute(IServiceCall command) throws ThinklabException {

        Scale scale = new Scale(command.getString("scale"));
        IConcept concept = (IConcept) command.get("type");
        String id = command.getString("id");
        boolean store = command.has("store") && command.get("store").toString().equals("true");

        INamespace localNamespace = KLAB.MMANAGER.getLocalNamespace();
        IDirectObserver observer = ModelFactory.createDirectObserver(concept, id, localNamespace, scale);
        ((KIMNamespace) localNamespace).addModelObject(observer);

        if (store) {
            ObservationKbox.get().store(observer);
            ObservationKbox.get().reindexLocalObservations();
        }

        return observer.getName();

    }
}
