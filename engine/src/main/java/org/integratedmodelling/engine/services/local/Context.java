/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.engine.services.local;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.integratedmodelling.api.metadata.IMetadata;
import org.integratedmodelling.api.modelling.IObservation;
import org.integratedmodelling.api.modelling.IScale;
import org.integratedmodelling.api.modelling.IScale.Locator;
import org.integratedmodelling.api.modelling.IState;
import org.integratedmodelling.api.modelling.ISubject;
import org.integratedmodelling.api.modelling.visualization.IMedia;
import org.integratedmodelling.api.runtime.IContext;
import org.integratedmodelling.api.runtime.ISession;
import org.integratedmodelling.api.services.IServiceCall;
import org.integratedmodelling.api.services.annotations.Execute;
import org.integratedmodelling.api.services.annotations.Prototype;
import org.integratedmodelling.api.services.types.ILocalService;
import org.integratedmodelling.api.services.types.ISessionService;
import org.integratedmodelling.common.model.runtime.Scale;
import org.integratedmodelling.common.network.Endpoints;
import org.integratedmodelling.common.visualization.ColorMap;
import org.integratedmodelling.common.visualization.Histogram;
import org.integratedmodelling.common.visualization.Viewport;
import org.integratedmodelling.engine.modelling.runtime.Subject;
import org.integratedmodelling.engine.rest.RESTManager;
import org.integratedmodelling.engine.visualization.VisualizationFactory;
import org.integratedmodelling.exceptions.ThinklabException;
import org.integratedmodelling.lang.IRemoteSerializable;

/**
 * Inspection and maintenance of computed or computing contexts. Used by remote GUIs to
 * visualize ongoing results of computations or to retrieve previously serialized
 * contexts.
 * 
 * API:
 *
 *  cmd=load <serialized context id>  // load context from user repository
 *      -> a serialized context descriptor with the ID
 *  cmd=remove ctx=<id>
 *      -> removes the context from the active list, freeing the memory
 *  cmd=persist ctx=<id> remove=<boolean>
 *      -> persists the context and optionally removes it. Returns
 *         the descriptor of the serialized context (TBD including 
 *         permanent ID).
 *  cmd=list
 *      -> two lists of numeric IDs: one tagged by "active"
 *         containing descriptors of "live" contexts in order of 
 *         creation, and one tagged by "inactive" containing bare
 *         descriptions of contexts that have timed out (structure
 *         TBD) and including the location of the context in the
 *         user repository if it was persisted.
 *         Live contexts should also contain their current time to 
 *         expiration.
 *  cmd=reset ctx=<id>
 *      -> reset the context by re-observing the root subject and 
 *         return its structure.
 *  cmd=structure path=<path> ctx=<id>
 *      -> context (sub-contexts and states). Each object is a
 *         map with path, name, type and auxiliary info. TBD. For
 *         states, the descriptor should contain the definition
 *         of the representative observer. For sub-contexts, the
 *         same structure is given, so the output is in fact a 
 *         tree. The root context path is identified by the slash
 *         argument.
 *  cmd=run ctx=<id>
 *      -> start temporal transitions. Immediately return ok/error
 *         status while simulation is ongoing. Do nothing if finished. 
 *  cmd=step ctx=<id>
 *      -> run the next temporal transitions. Immediately return 
 *         ok/error status while simulation is ongoing. Do nothing 
 *         if finished.
 *  cmd=status ctx=<id> [after=<timestamp>]
 *      -> a map containing structure (indexed by "structure"), 
 *         and the current status of the context. Also return a "timestamp"
 *         with the time of last context change. If after is
 *         passed, return a no-op with "ignore=true" unless the
 *         last change in the context happened after the passed
 *         timestamp.
 *  cmd=get-media path=<path> ctx=<id> type=<MIMEtype> index=<index> [attribute=<field> [pN=<parameter value>]*] [break=<true|false>]
 *      -> descriptor for the visualization of the object 
 *         identified by <path> at context <index>. Result will have the MIME
 *         type requested. Optionally the request may specify an attribute of the
 *         object, such as the workflow graph for a task, the legend
 *         or the colormap, and add parameters (starting at p0) to further specialize the target
 *         or the result.
 *         If break == true, set a breakpoint on this object, pausing temporal contexualization when it changes until 
 *         run is called again.
 *  cmd=get-data-summary path=<path> ctx=<id> type=<MIMEtype> [attribute=<field> [pN=<parameter value>]*]
 *      -> descriptor for the statistical summary of the object 
 *         identified by <path> at context <index> including an histogram of the data, data
 *         type, color map, aggregated value and any other information relevant to content. 
 *  cmd=get-data-summary path=<path> ctx=<id> type=<MIMEtype> [attribute=<field> [pN=<parameter value>]*]
 *      -> data content of object 
 *         identified by <path> at context <index> including an histogram of the data and
 *         any other information relevant to content. 
 *  cmd=get-data path=<path> ctx=<id>
 *      -> descriptor for the visualization of the object 
 *         identified by <path>. 
 *  cmd=export ctx=<id> [path=<path> format=<format-id> {file=<file> | remote=<true|false>}]
 *      -> handle to the exported version of the given path, using
 *         the default format unless one is provided. If file is given, persist to
 *         local file. Create a dataset for a subject (or if path is not passed), 
 *         an appropriate media (default or format-id) if a state.
 *  cmd=image path=<path> ctx=<id> x=<columns> y=<rows>
 *      -> an image representing the current values of the state
 *         identified by <path> fitting the passed dimensions.
 *  cmd=history path=<path> ctx=<id>
 *      -> a list of chronological steps describing each observation
 *         made in this context. Each step contains the observed
 *         objects, the provenance and dataflow diagrams, and the
 *         start/end time. TBD logging etc.
 *  cmd=breakpoint 
 *      -> pause and restart contextualization based on value of "action" parameter. Normally
 *         only used to restart after a breakpoint was triggered by get-media.
 *  
 * TODO/CHECK: added ILocalService so it can be used by JS-driven geomapper without having
 * to worry about headers for now. 
 *  
 * @author ferdinando.villa
 *
 */
@Prototype(
        id = Endpoints.CONTEXT,
        args = {
                "# stateindex",
                Prototype.INT,
                "ctx",
                Prototype.INT,
                "# path",
                Prototype.TEXT,
                "# session",
                Prototype.TEXT,
                "# remove",
                Prototype.TEXT,
                "# format", Prototype.TEXT,
                "# x", Prototype.INT,
                "# y", Prototype.INT,
                "# after", Prototype.INT,
                "# viewport", Prototype.TEXT,
                "# break", Prototype.BOOLEAN,
                "# action", "stop|start",
                "# index", Prototype.TEXT,
                "# type", Prototype.TEXT,
                "# file", Prototype.TEXT,
                "# remote", Prototype.TEXT })
public class Context implements ILocalService, ISessionService {

    @Execute(command = "status")
    public Object status(IServiceCall command) throws ThinklabException {

        org.integratedmodelling.engine.runtime.Context context = null;
        int ctx = -1;
        if (command.has("ctx")) {
            ctx = ((Number) (command.get("ctx"))).intValue();
            for (IContext c : command.getSession().getContexts()) {
                if (((org.integratedmodelling.engine.runtime.Context) c).getId() == ctx) {
                    context = (org.integratedmodelling.engine.runtime.Context) c;
                    break;
                }
            }
        }

        Map<String, Object> r = new HashMap<String, Object>();
        r.put("structure", getStructure((Subject) context.getSubject(), true));
        r.put("current", context.getCurrentTimeIndex());
        r.put("timestamp", context.getLastChangeTimestamp());
        r.put("id", context.getId());
        r.put("name", context.getName());
        return r;
    }

    @Execute(command = "persist", requires = { "file", "type" })
    public Object persist(IServiceCall command) throws ThinklabException {

        org.integratedmodelling.engine.runtime.Context context = null;
        int ctx = -1;
        if (command.has("ctx")) {
            ctx = ((Number) (command.get("ctx"))).intValue();
            for (IContext c : command.getSession().getContexts()) {
                if (((org.integratedmodelling.engine.runtime.Context) c).getId() == ctx) {
                    context = (org.integratedmodelling.engine.runtime.Context) c;
                    break;
                }
            }
        }

        String path = null;
        List<Object> options = new ArrayList<>();

        if (command.has("path")) {
            path = command.get("path").toString();
        }
        if (command.has("viewport")) {
            options.add(Viewport.fromString(command.get("viewport").toString()));
        }
        if (command.has("index")) {
            for (IScale.Locator locator : Scale.parseLocators(command.get("index").toString())) {
                options.add(locator);
            }
        }
        IMedia.Type type = IMedia.Type.valueOf(command.get("type").toString());

        context.persist(new File(command.get("file").toString()), path, type, options.toArray());

        return null;
    }

    @Execute(command = "structure")
    public Object structure(IServiceCall command) throws ThinklabException {

        Object ret = null;
        org.integratedmodelling.engine.runtime.Context context = null;
        int ctx = -1;
        if (command.has("ctx")) {
            ctx = ((Number) (command.get("ctx"))).intValue();
            for (IContext c : command.getSession().getContexts()) {
                if (((org.integratedmodelling.engine.runtime.Context) c).getId() == ctx) {
                    context = (org.integratedmodelling.engine.runtime.Context) c;
                    break;
                }
            }
        }
        String path = null;
        if (command.has("path")) {
            path = command.get("path").toString();
        }

        if (path == null)
            path = "/";
        IObservation subject = context.get(path);
        if (subject instanceof IRemoteSerializable) {
            ret = ((IRemoteSerializable) subject).adapt();
        }

        return ret;
    }

    @Execute(command = "run")
    public Object run(IServiceCall command) throws ThinklabException {

        org.integratedmodelling.engine.runtime.Context context = null;
        int ctx = -1;
        if (command.has("ctx")) {
            ctx = ((Number) (command.get("ctx"))).intValue();
            for (IContext c : command.getSession().getContexts()) {
                if (((org.integratedmodelling.engine.runtime.Context) c).getId() == ctx) {
                    context = (org.integratedmodelling.engine.runtime.Context) c;
                    break;
                }
            }
        }
        String path = null;
        if (command.has("path")) {
            path = command.get("path").toString();
        }

        if (path == null)
            path = "/";

        return context.runAsynchronous();
    }

    @Execute(command = "breakpoint", requires = { "ctx", "action" })
    public Object breakpoint(IServiceCall command) throws ThinklabException {

        org.integratedmodelling.engine.runtime.Context context = null;
        int ctx = -1;
        ctx = ((Number) (command.get("ctx"))).intValue();
        for (IContext c : command.getSession().getContexts()) {
            if (((org.integratedmodelling.engine.runtime.Context) c).getId() == ctx) {
                context = (org.integratedmodelling.engine.runtime.Context) c;
                break;
            }
        }
        if (context != null) {
            context.pause(command.get("action").equals("stop"));
        }
        return null;
    }

    @Execute(command = "step")
    public Object step(IServiceCall command) throws ThinklabException {

        org.integratedmodelling.engine.runtime.Context context = null;
        int ctx = -1;
        if (command.has("ctx")) {
            ctx = ((Number) (command.get("ctx"))).intValue();
            for (IContext c : command.getSession().getContexts()) {
                if (((org.integratedmodelling.engine.runtime.Context) c).getId() == ctx) {
                    context = (org.integratedmodelling.engine.runtime.Context) c;
                    break;
                }
            }
        }
        String path = null;
        if (command.has("path")) {
            path = command.get("path").toString();
        }

        if (path == null)
            path = "/";

        /*
         * TODO step hostia.
         */

        return null;
    }

    @Execute(command = "get-media", requires = { "index", "viewport", "type" })
    public Object getMedia(IServiceCall command) throws ThinklabException {

        Object ret = null;
        org.integratedmodelling.engine.runtime.Context context = null;
        int ctx = -1;
        ISession session = command.getSession();
        if (session == null && command.has("session")) {
            /*
             * temporary: should come from headers, but this will be issued by the JS in geomapper which
             * I will fix for my 99th birthday.
             */
            session = RESTManager.get().getSession(command.getString("session"));
        }
        if (command.has("ctx")) {
            ctx = ((Number) (command.get("ctx"))).intValue();
            for (IContext c : session.getContexts()) {
                if (((org.integratedmodelling.engine.runtime.Context) c).getId() == ctx) {
                    context = (org.integratedmodelling.engine.runtime.Context) c;
                    break;
                }
            }
        }
        String path = null;
        if (command.has("path")) {
            path = command.get("path").toString();
        }

        IObservation obs = context.get(path);
        String mType = command.get("type").toString();
        String attr = command.has("attribute") ? command.get("attribute").toString() : null;
        Viewport viewport = Viewport.fromString(command.get("viewport").toString());
        Locator[] locators = Scale.parseLocators(command.get("index").toString());

        /*
         * TODO it gets messy to send this from the GUI, so for now just assume that
         * every time we get the media we also want to set a breakpoint. The context will
         * figure out what to do.
         */
        if (command.has("break") && (Boolean) command.get("break")) {
            context.setBreakpoint(obs);
        }

        if (attr == null) {
            return getMedia(obs, mType, locators, viewport);
        }
        /*
         * TODO handle attribute and its parameters.
         */
        return ret;
    }

    @Execute(command = "get-data-summary", requires = { "index" })
    public Object getDataSummary(IServiceCall command) throws ThinklabException {

        org.integratedmodelling.engine.runtime.Context context = null;
        int ctx = -1;
        if (command.has("ctx")) {
            ctx = ((Number) (command.get("ctx"))).intValue();
            for (IContext c : command.getSession().getContexts()) {
                if (((org.integratedmodelling.engine.runtime.Context) c).getId() == ctx) {
                    context = (org.integratedmodelling.engine.runtime.Context) c;
                    break;
                }
            }
        }
        String path = null;
        if (command.has("path")) {
            path = command.get("path").toString();
        }

        Map<String, Object> r = new HashMap<String, Object>();
        IObservation obs = context.get(path);
        Locator[] locators = Scale.parseLocators(command.get("index").toString());
        if (obs instanceof IState) {
            IScale.Index ind = obs.getScale().getIndex(locators);
            Histogram histogram = VisualizationFactory.getHistogram((IState) obs, ind, 10);
            ColorMap colormap = (ColorMap) VisualizationFactory.getColormap((IState) obs, ind);
            if (histogram != null)
                r.put(IMetadata.STATE_HISTOGRAM, histogram.toString());
            if (colormap != null)
                r.put(IMetadata.STATE_COLORMAP, colormap.toString());
            return r;
        }

        return null;
    }

    @Execute(command = "get-data", requires = { "index", "viewport", "type" })
    public Object getData(IServiceCall command) throws ThinklabException {

        org.integratedmodelling.engine.runtime.Context context = null;
        int ctx = -1;
        if (command.has("ctx")) {
            ctx = ((Number) (command.get("ctx"))).intValue();
            for (IContext c : command.getSession().getContexts()) {
                if (((org.integratedmodelling.engine.runtime.Context) c).getId() == ctx) {
                    context = (org.integratedmodelling.engine.runtime.Context) c;
                    break;
                }
            }
        }
        String path = null;
        if (command.has("path")) {
            path = command.get("path").toString();
        }

        IObservation obs = context.get(path);
        String mType = command.get("type").toString();
        String attr = command.has("attribute") ? command.get("attribute").toString() : null;
        Viewport viewport = Viewport.fromString(command.get("viewport").toString());
        Locator[] locators = Scale.parseLocators(command.get("index").toString());
        if (attr == null) {
            return getMedia(obs, mType, locators, viewport);
        }

        return null;
    }

    @Execute(command = "get-value")
    public Object getValue(IServiceCall command) throws ThinklabException {

        org.integratedmodelling.engine.runtime.Context context = null;
        int ctx = -1;
        if (command.has("ctx")) {
            ctx = ((Number) (command.get("ctx"))).intValue();
            for (IContext c : command.getSession().getContexts()) {
                if (((org.integratedmodelling.engine.runtime.Context) c).getId() == ctx) {
                    context = (org.integratedmodelling.engine.runtime.Context) c;
                    break;
                }
            }
        }
        String path = null;
        if (command.has("path")) {
            path = command.get("path").toString();
        }

        IObservation obs = context.get(path);
        // String mType = command.getArgumentAsString("type");
        // int[] index = NumberUtils.fromString(command.getArgumentAsString("index"));
        int idx = ((Number) (command.get("stateindex"))).intValue();
        if (obs instanceof IState) {
            return VisualizationFactory.get().describeValue((IState) obs, ((IState) obs).getValue(idx));
        }

        /*
         * TODO handle attribute and its parameters.
         */

        return null;
    }

    private Object getMedia(IObservation obs, String mType, Locator[] index, Viewport viewport) {
        return VisualizationFactory.get()
                .getMedia(obs, obs.getScale().getIndex(index), viewport, mType, null);
    }

    /**
     * Return just the id, observable and list of states and subjects for each
     * subject. States are described only as observables. Scale is included
     * optionally so we only ask for it once.
     * 
     * @param s
     * @return
     */
    Map<?, ?> getStructure(Subject s, boolean addScale) {

        Map<Object, Object> ret = new HashMap<Object, Object>();

        ArrayList<Object> states = new ArrayList<Object>();
        for (IState st : s.getStates()) {
            if (st instanceof IRemoteSerializable) {
                states.add(((IRemoteSerializable) st).adapt());
            }
        }

        ArrayList<Object> subjects = new ArrayList<Object>();
        for (ISubject st : s.getSubjects()) {
            subjects.add(getStructure((Subject) st, addScale));
        }

        ret.put("id", s.getId());
        ret.put("observable", ((IRemoteSerializable) (s.getObservable())).adapt());
        ret.put("internal-id", s.getInternalID());
        ret.put("states", states);
        ret.put("subjects", subjects);

        if (addScale) {
            ret.put("scale", ((IRemoteSerializable) (s.getScale())).adapt());
        }

        return ret;
    }

    Object serialize(Object o) {
        if (o instanceof IRemoteSerializable) {
            return ((IRemoteSerializable) o).adapt();
        }
        return null;
    }

}
