/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.engine.services.local;

import java.io.File;

import org.integratedmodelling.api.project.IProject;
import org.integratedmodelling.api.services.IServiceCall;
import org.integratedmodelling.api.services.annotations.Execute;
import org.integratedmodelling.api.services.annotations.Prototype;
import org.integratedmodelling.api.services.types.ISessionService;
import org.integratedmodelling.common.configuration.KLAB;
import org.integratedmodelling.engine.scripting.ScriptEngine;
import org.integratedmodelling.exceptions.ThinklabException;
import org.integratedmodelling.exceptions.ThinklabResourceNotFoundException;

@Prototype(
        id = "run",
        args = {
                "script",
                Prototype.TEXT,
                "# project",
                Prototype.TEXT,
                "? c|context",
                Prototype.INT,
                "? s|scenario",
                Prototype.TEXT })
public class Run implements ISessionService {

    @Execute
    public Object execute(IServiceCall command) throws ThinklabException {

        IProject project = KLAB.PMANAGER.getProject(command.get("project").toString());
        File f = null;
        if (project != null) {
            f = new File(project.getLoadPath() + File.separator + ".scripts" + File.separator
                    + command.get("script") + ".groovy");
        }

        if (f == null || !f.exists()) {
            throw new ThinklabResourceNotFoundException("script " + command.get("script")
                    + " in project " + command.get("project"));
        }

        return ScriptEngine.execute(f);
    }

}
