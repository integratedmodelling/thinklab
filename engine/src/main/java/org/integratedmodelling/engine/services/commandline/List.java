/*******************************************************************************
 *  Copyright (C) 2007, 2014:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.engine.services.commandline;
///**
// * Copyright 2011 The ARIES Consortium (http://www.ariesonline.org) and
// * www.integratedmodelling.org. 
//
//   This file is part of Thinklab.
//
//   Thinklab is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License as published
//   by the Free Software Foundation, either version 3 of the License,
//   or (at your option) any later version.
//
//   Thinklab is distributed in the hope that it will be useful, but
//   WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//   General Public License for more details.
//
//   You should have received a copy of the GNU General Public License
//   along with Thinklab.  If not, see <http://www.gnu.org/licenses/>.
// */
//package org.integratedmodelling.thinklab.commandline.commands;
//
//import org.integratedmodelling.exceptions.ThinklabException;
//import org.integratedmodelling.exceptions.ThinklabValidationException;
//import org.integratedmodelling.lang.SemanticType;
//import org.integratedmodelling.thinklab.Thinklab;
//import org.integratedmodelling.thinklab.api.knowledge.IConcept;
//import org.integratedmodelling.thinklab.api.knowledge.IOntology;
//import org.integratedmodelling.thinklab.api.knowledge.IProperty;
//import org.integratedmodelling.thinklab.api.knowledge.ISemanticObject;
//import org.integratedmodelling.thinklab.api.knowledge.kbox.IKbox;
//import org.integratedmodelling.thinklab.api.runtime.ISession;
//import org.integratedmodelling.thinklab.command.Command;
//import org.integratedmodelling.thinklab.command.CommandManager;
//import org.integratedmodelling.thinklab.interfaces.commands.ICommandHandler;
//import org.integratedmodelling.thinklab.interfaces.commands.IListingProvider;
//
//public class List implements ICommandHandler {
//
//    enum listmode {
//        DESCRIPTIVE,
//        LIST
//    };
//
//    void listOntology(IOntology ont, ISession session) throws ThinklabException {
//
//        session.println("Listing ontology " + ont);
//
//        session.println("\nConcepts:");
//        //		for (IConcept i : ont.getConcepts()) {
//        //			session.getOutputStream().println("\t" + i.toString()
//        //					+ ":\t" + i.getLabel() + "\t" + i.getDescription());
//        //		}
//        //		session.getOutputStream().println("\nProperties:");
//        //		for (IProperty i : ont.getProperties()) {
//        //			session.getOutputStream().println("\t" + i.toString()
//        //					+ ":\t" + i.getLabel() + "\t" + i.getDescription());
//        //		}
//        //		session.getOutputStream().println("\nInstances:");
//        //		for (IInstance i : ont.getInstances()) {
//        //			session.getOutputStream().println("\t" + i.toString()
//        //					+ ":\t" + i.getLabel() + "\t" + i.getDescription());
//        //		}
//    }
//
//    //
//    //	void listIndividual(IInstance c, listmode mode,
//    //			ISession session) throws ThinklabException {
//    //
//    //		if (mode == listmode.DESCRIPTIVE) {
//    //			session.getOutputStream().println("Instance URI is " + c.getURI());
//    //
//    //			session.getOutputStream().println("list representation:\n"
//    //					+ PolyList.prettyPrint(c.conceptualize().asList()));
//    //
//    //			session.getOutputStream().println(c.getDescription());
//    //
//    //			if (c.getImplementation() == null) {
//    //				session.getOutputStream().println("has no implementation");
//    //			} else {
//    //				session.getOutputStream().println("has implementation of class "
//    //						+ c.getImplementation().getClass().toString());
//    //			}
//    //
//    //			for (IRelationship r : c.getRelationships()) {
//    //				session.getOutputStream().println("  " + r.toString());
//    //			}
//    //
//    //		} else if (mode == listmode.LIST) {
//    //			session.getOutputStream().println(PolyList.prettyPrint(c.conceptualize().asList()));
//    //		}
//    //	}
//
//    void listConcept(IConcept c, listmode l, ISession session) throws ThinklabException {
//
//        session.println("Concept URI is " + c.getURI());
//
//        //		session.getOutputStream().println(c.getDescription());
//
//        session.println("  Properties:");
//        for (IProperty r : c.getAllProperties()) {
//            session.println("    " + r);
//        }
//
//    }
//
//    void listProperty(IProperty p, listmode l, ISession outputWriter) {
//
//        outputWriter.println("Property URI is " + p.getURI());
//        //		outputWriter.getOutputStream().println(p.getDescription());
//
//        outputWriter.println("  Domain:");
//        outputWriter.println("    " + p.getDomain());
//
//        outputWriter.println("  Range:");
//        for (IConcept c : p.getRange()) {
//            outputWriter.println("    " + c);
//        }
//    }
//
//    void listKBox(IKbox kbox, String kbname, ISession outputWriter) throws ThinklabException {
//
//        outputWriter.println("Listing contents of kBox " + kbname);
//
//        java.util.List<ISemanticObject<?>> result = kbox.query(null);
//
//        if (result.size() > 0) {
//
//            for (int i = 0; i < result.size(); i++) {
//
//                outputWriter.println(i + ". " + result.get(i));
//            }
//        }
//
//        outputWriter.println("total: " + result.size());
//    }
//
//    public Object execute(Command command, ISession session) throws ThinklabException {
//
//        String subject = command.getArgumentAsString("subject");
//        String item = null;
//
//        if (!command.getArgumentAsString("item").equals("__NONE__"))
//            item = command.getArgumentAsString("item");
//
//        listmode mode = listmode.LIST;
//
//        if (command.hasOption("lf"))
//            mode = listmode.LIST;
//
//        if (subject == null || subject.equals("__NONE__")) {
//
//            session.println("Listing session contents: \n");
//            int c = 0;
//            session.println(c + " objects");
//            return null;
//        }
//
//        /*
//         * check if we're invoking one of the installed listing providers	
//         */
//        IListingProvider prov = item == null ? CommandManager.get().getListingProvider(subject)
//                : CommandManager.get().getItemListingProvider(subject);
//
//        if (prov != null) {
//
//            if (item == null) {
//                int n = 0;
//                for (Object o : prov.getListing()) {
//                    session.println("  " + o);
//                    n++;
//                }
//                session.println(n + " " + subject);
//
//            } else {
//                for (Object o : prov.getSpecificListing(item)) {
//                    session.println(o.toString());
//                }
//            }
//
//            return null;
//        }
//
//        /*
//         * default topics
//         */
//        if ("ontologies".equals(subject)) {
//
//            for (IOntology o : Thinklab.get().getOntologies(true)) {
//                session.println(o.getConceptSpace() + ":\t" + o.getURI());
//            }
//
//        } else if ("kboxes".equals(subject)) {
//
//            //			for (String kb : KBoxManager.get().getInstalledKboxes()) {
//            //				session.getOutputStream().println(MiscUtilities.getURLBaseName(kb)
//            //						+ ":\t" + kb);
//            //			}
//
//            //			for (String kb : session.getLocalKBoxes()) {
//            //				session.getOutputStream().println(kb + " (local)");
//            //			}
//
//        } else {
//
//            if (subject.startsWith("#")) {
//
//                //				/* looking for an instance in current session */
//                //				ISemanticObject obj = null; // session.retrieveObject(subject.substring(1));
//                //
//                //				if (obj == null) {
//                //					session
//                //							.getOutputStream().println("nothing known about " + subject);
//                //				} else {
//                //					listIndividual(obj, listmode.DESCRIPTIVE, session);
//                //				}
//
//            } else if (SemanticType.validate(subject)) {
//
//                //				IInstance inst = null;
//                IConcept conc = null;
//                IProperty prop = null;
//
//                /* concept? */
//                if ((conc = Thinklab.get().getConcept(subject)) != null)
//                    listConcept(conc, mode, session);
//                //				/* individual? */
//                //				else if ((inst = KnowledgeManager.get().retrieveInstance(t)) != null)
//                //					listIndividual(inst, mode, session);
//                /* property? */
//                else if ((prop = Thinklab.get().getProperty(subject)) != null)
//                    listProperty(prop, mode, session);
//                else
//                    throw new ThinklabValidationException(subject);
//
//            } else {
//
//                /* see if subject is an ontology */
//                IOntology o = null; // Thinklab.get().getKnowledgeRepository().retrieveOntology(
//                //subject);
//                if (o != null)
//                    listOntology(o, session);
//                else {
//                    //					/* see if it is a kbox URL or name */
//                    //					IKBox kbox = session.retrieveKBox(subject);
//                    //
//                    //					if (kbox != null) {
//                    //						listKBox(kbox, subject, session);
//                    //					} else {
//                    //						session.getOutputStream().println("nothing known about "
//                    //								+ subject);
//                    //					}
//                }
//            }
//        }
//
//        return null;
//    }
//
// }
