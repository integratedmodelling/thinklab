/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.engine;

import java.lang.reflect.Constructor;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.integratedmodelling.api.errormanagement.ICompileError;
import org.integratedmodelling.api.errormanagement.ICompileInfo;
import org.integratedmodelling.api.errormanagement.ICompileWarning;
import org.integratedmodelling.api.knowledge.ICodeExpression;
import org.integratedmodelling.api.lang.IParsingContext;
import org.integratedmodelling.api.modelling.IClassification;
import org.integratedmodelling.api.modelling.IClassifyingObserver;
import org.integratedmodelling.api.modelling.IDataSource;
import org.integratedmodelling.api.modelling.IDirectActiveObservation;
import org.integratedmodelling.api.modelling.IDirectObserver;
import org.integratedmodelling.api.modelling.IExtent;
import org.integratedmodelling.api.modelling.IFunctionCall;
import org.integratedmodelling.api.modelling.IModel;
import org.integratedmodelling.api.modelling.IModelObject;
import org.integratedmodelling.api.modelling.INamespace;
import org.integratedmodelling.api.modelling.INumericObserver;
import org.integratedmodelling.api.modelling.IObservable;
import org.integratedmodelling.api.modelling.IObserver;
import org.integratedmodelling.api.modelling.IPresenceObserver;
import org.integratedmodelling.api.modelling.IScale;
import org.integratedmodelling.api.modelling.storage.IDataset;
import org.integratedmodelling.api.modelling.storage.IStorage;
import org.integratedmodelling.api.monitoring.IKnowledgeLifecycleListener;
import org.integratedmodelling.api.monitoring.IMonitor;
import org.integratedmodelling.api.space.ISpatialExtent;
import org.integratedmodelling.api.space.ISpatialIndex;
import org.integratedmodelling.common.configuration.KLAB;
import org.integratedmodelling.common.kim.KIMContext;
import org.integratedmodelling.common.kim.KIMDirectObserver;
import org.integratedmodelling.common.kim.KIMFunctionCall;
import org.integratedmodelling.common.project.RemoteProject;
import org.integratedmodelling.common.storage.BooleanStorage;
import org.integratedmodelling.common.storage.ConceptStorage;
import org.integratedmodelling.common.storage.NumberStorage;
import org.integratedmodelling.common.vocabulary.Observable;
import org.integratedmodelling.engine.actionsupport.ActionSpace;
import org.integratedmodelling.engine.actionsupport.ActionSubject;
import org.integratedmodelling.engine.geospace.extents.SpaceExtent;
import org.integratedmodelling.engine.geospace.extents.SpatialIndex;
import org.integratedmodelling.engine.modelling.datasources.ConstantDataSource;
import org.integratedmodelling.engine.modelling.kbox.ModelKbox;
import org.integratedmodelling.engine.modelling.kbox.ObservationKbox;
import org.integratedmodelling.engine.modelling.runtime.Scale;
import org.integratedmodelling.engine.modelling.runtime.Subject;
import org.integratedmodelling.exceptions.ThinklabException;
import org.integratedmodelling.exceptions.ThinklabInternalErrorException;
import org.integratedmodelling.exceptions.ThinklabRuntimeException;

public class ModelFactory extends org.integratedmodelling.common.kim.ModelFactory {

    class EngineKIMContext extends KIMContext {

        @Override
        protected KIMContext newInstance() {
            return new EngineKIMContext();
        }

        @Override
        public void onError(ICompileError error) {
            KLAB.error(error.toString());
        }

        @Override
        protected void onWarning(ICompileWarning error) {
            // Env.logger.warn(error.toString());
        }

        @Override
        protected void onInfo(ICompileInfo error) {
            // Env.logger.info(error.toString());
        }
    }

    class KnowledgeListener implements IKnowledgeLifecycleListener {

        Map<String, Integer> recheckModelNS       = new HashMap<>();
        Map<String, Integer> recheckObservationNS = new HashMap<>();

        @Override
        public void objectDefined(IModelObject object) {

            Integer storingNamespace = recheckModelNS.get(object.getNamespace().getId());
            Integer storingObservation = recheckObservationNS.get(object.getNamespace().getId());

            if (object instanceof IModel && !object.isInactive() && storingNamespace != null
                    && storingNamespace > 0) {

                /*
                 * do not store anything coming from remote projects - just use them when they're
                 * online.
                 */
                if (!(object.getNamespace().getProject() instanceof RemoteProject)) {

                    boolean store = true;

                    try {
                        if (storingNamespace == 2) {
                            store = ModelKbox.get().hasModel(object.getName());
                        }
                        if (store) {
                            ModelKbox.get().store(object);
                        }
                    } catch (ThinklabException e) {
                        KLAB.error("error storing model: " + e);
                    }

                }
            }

            if (object instanceof IDirectObserver && !object.isInactive() && storingObservation != null
                    && storingObservation > 0) {

                /*
                 * goes in the searchable observation kbox. We piggyback on the model
                 * kbox to handle the namespace synchronization.
                 */
                if (!(object.getNamespace().getProject() instanceof RemoteProject)) {

                    boolean store = true;

                    try {
                        if (storingObservation == 2) {
                            store = ObservationKbox.get().hasObservation(object.getName());
                        }
                        if (store) {
                            ObservationKbox.get().store(object);
                        }
                    } catch (ThinklabException e) {
                        KLAB.error("error storing observation: " + e);
                    }
                }

            }
        }

        @Override
        public void namespaceDeclared(INamespace namespace) {

            if (!namespace.hasErrors()) {

                try {
                    int cmodel = ModelKbox.get().removeIfOlder(namespace);
                    if (cmodel > 0) {
                        recheckModelNS.put(namespace.getId(), cmodel);
                    }
                } catch (Exception e) {

                }

                try {
                    int cobser = ObservationKbox.get().removeIfOlder(namespace);
                    if (cobser > 0) {
                        recheckObservationNS.put(namespace.getId(), cobser);
                    }
                } catch (Exception e) {

                }
            }
        }

        @Override
        public void namespaceDefined(INamespace namespace) {

            Integer storingNamespace = recheckModelNS.remove(namespace.getId());
            Integer storingObservation = recheckObservationNS.remove(namespace.getId());

            if (storingNamespace != null && storingNamespace > 0
                    && !(namespace.getProject() instanceof RemoteProject)) {

                try {
                    ModelKbox.get().store(namespace);
                } catch (Exception e) {
                    KLAB.error("error storing knowledge", e);
                }
            }

            if (storingObservation != null && storingObservation > 0
                    && !(namespace.getProject() instanceof RemoteProject)) {

                try {
                    ObservationKbox.get().store(namespace);
                } catch (Exception e) {
                    KLAB.error("error storing knowledge", e);
                }
            }
        }
    }

    @Override
    public IParsingContext getRootParsingContext() {
        return new EngineKIMContext();
    }

    public ModelFactory() {
        addKnowledgeLifecycleListener(new KnowledgeListener());
    }

    @Override
    public IStorage<?> createStorage(IObserver observer, IScale scale, IDataset dataset, boolean isDynamic) {

        IObservable observable = new Observable((Observable) (observer.getModel() == null
                ? observer.getObservable()
                : observer.getModel().getObservable()));

        ((Observable) observable).setObserver(observer);

        if (observer instanceof INumericObserver
                && ((INumericObserver) observer).getDiscretization() == null) {
            return new NumberStorage(observable, scale, ((INumericObserver) observer)
                    .getDiscretization(), dataset, isDynamic);
        } else
            if (observer instanceof IClassifyingObserver || (observer instanceof INumericObserver
                    && ((INumericObserver) observer).getDiscretization() != null)) {
            IClassification classif = (observer instanceof IClassifyingObserver)
                    ? ((IClassifyingObserver) observer).getClassification()
                    : ((INumericObserver) observer).getDiscretization(); // FIXME this latter won't be called
            return new ConceptStorage(observable, scale, dataset, isDynamic, classif);
        } else if (observer instanceof IPresenceObserver) {
            return new BooleanStorage(observable, scale, dataset, isDynamic);
        }

        throw new ThinklabRuntimeException("don't know how to create storage for observer " + observer);
    }

    @Override
    public IDirectActiveObservation createSubject(IDirectObserver observer, IDirectActiveObservation context, IMonitor monitor)
            throws ThinklabException {
        return getSubjectByMetadata(observer.getObservable(), observer.getNamespace(), observer
                .getCoverage(), observer.getId(), monitor);
    }

    @Override
    public IScale createScale(Collection<IFunctionCall> definition) {
        try {
            List<IExtent> exts = new ArrayList<>();
            for (IFunctionCall f : definition) {
                Object o = callFunction(f, KLAB.CMANAGER.getMonitor(), null);
                if (!(o instanceof IExtent)) {
                    throw new ThinklabRuntimeException("function call " + f + " did not produce an extent");
                }
                if (o instanceof ICodeExpression) {
                    ((ICodeExpression) o).setNamespace(((KIMFunctionCall) f).getNamespace());
                }
                exts.add((IExtent) o);
            }
            return new Scale(exts);
        } catch (ThinklabException e) {
            throw new ThinklabRuntimeException(e);
        }
    }

    /**
    * Return a new subject generator whose scale has the passed forcings applied.
    *
    * @param observer
    * @param forcings
    * @return
    * @throws ThinklabException
    */
    public static IDirectObserver forceScale(IDirectObserver observer, ArrayList<IExtent> forcings)
            throws ThinklabException {
        IScale oldScale = Scale.sanitize(observer.getCoverage());
        List<IExtent> forcedExtents = org.integratedmodelling.common.model.runtime.Scale
                .forceExtents(oldScale, forcings.toArray(new IExtent[forcings.size()]));
        IScale newScale = new Scale(forcedExtents.toArray(new IExtent[forcedExtents.size()]));
        return ModelFactory.createDirectObserver(observer.getObservable().getTypeAsConcept(), observer
                .getId(), observer.getNamespace(), newScale);
    }

    public static IDirectObserver sanitizeDirectObserver(IDirectObserver observer) {

        return new KIMDirectObserver((KIMDirectObserver) observer);

        // ret.observable = observer.getObservable();
        // ret.setLineNumbers(observer.getFirstLineNumber(), observer.getLastLineNumber());
        // ret.setNamespace((INamespaceDefinition) observer.getNamespace());
        // ret.setPrivate(observer.isPrivate());
        // ret.setInactive(observer.isInactive());
        // ret.setId(observer.getId());
        // ret.observableDefinition = ReferenceList.list(observer.getObservable().getTypeAsConcept());
        // try {
        // ret.scale = Scale.sanitize(observer.getScale());
        // } catch (ThinklabException e1) {
        // throw new ThinklabRuntimeException(e1);
        // }
        //
        // return ret;
    }

    /**
     * The main entry point.
     *
     * @param observable
     * @param namespace
     * @param scale
     * @param newSubjectId
     * @return
     * @throws ThinklabException
     */
    public static Subject getSubjectByMetadata(IObservable observable, INamespace namespace, IScale scale, String newSubjectId, IMonitor monitor)
            throws ThinklabException {

        Subject result;
        Constructor<?> constructor;

        Class<?> agentClass = KLAB.MMANAGER.getSubjectClass(observable.getTypeAsConcept());
        if (agentClass == null) {
            agentClass = Subject.class;
        }

        try {
            constructor = agentClass.getConstructor(IObservable.class, IScale.class, /* Object.class, */
            INamespace.class, String.class, IMonitor.class);
        } catch (Exception e) {
            throw new ThinklabInternalErrorException("No viable constructor found for Java class '"
                    + agentClass.getCanonicalName() + "' for agent type '" + observable.getFormalName()
                    + "'");
        }

        try {
            result = (Subject) constructor.newInstance(observable, scale, /* null,*/
            namespace, newSubjectId, monitor);
        } catch (Exception e) {
            throw new ThinklabRuntimeException("Unable to generate new instance of Java class '"
                    + agentClass.getCanonicalName() + "' for agent type '" + observable.getFormalName()
                    + "'");
        }

        return result;

    }

    @Override
    public IDataSource createConstantDataSource(Object inlineValue) {
        return new ConstantDataSource(inlineValue);
    }

    @Override
    public Object wrapForAction(Object object) {
        if (object instanceof Subject) {
            return new ActionSubject((Subject) object);
        } else if (object instanceof SpaceExtent) {
            return new ActionSpace((SpaceExtent) object);
        }
        return super.wrapForAction(object);
    }

    @Override
    public ISpatialIndex getSpatialIndex(ISpatialExtent space) {
        return new SpatialIndex(space);
    }

}
