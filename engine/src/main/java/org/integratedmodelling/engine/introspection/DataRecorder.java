/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.engine.introspection;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.integratedmodelling.engine.commandline.GraphicalShell;

/**
 * Used only when debug configuration is activated (by putting thinklab.debug = true in properties). Records
 * stuff. Debug command can show the stuff recorded.
 * 
 * Basically a super-collection with a flexible add/retrieval interface.
 * 
 * @author Ferd
 *
 */
public class DataRecorder {

    private static DataRecorder _this;

    HashMap<String, Object>     _data     = new HashMap<String, Object>();
    ArrayList<Object>           _infos    = new ArrayList<Object>();
    ArrayList<Object>           _warnings = new ArrayList<Object>();
    ArrayList<Object>           _errors   = new ArrayList<Object>();
    GraphicalShell              _shell    = null;

    private DataRecorder() {
    }

    public void setShell(GraphicalShell shell) {
        _shell = shell;
    }

    public static DataRecorder get() {
        if (_this == null) {
            _this = new DataRecorder();
        }
        return _this;
    }

    public List<Object> getErrors() {
        return _errors;
    }

    public List<Object> getWarnings() {
        return _warnings;
    }

    public List<Object> getInfos() {
        return _infos;
    }

    public void record(Object o, String oclass) {
        _data.put(oclass, o);
    }

    /**
     * Interpret class as a list and add the object to it
     * @param o
     * @param oclass
     * @param onTop
     */
    @SuppressWarnings("unchecked")
    public void add(Object o, String oclass, boolean onTop) {
        Object dt = _data.get(oclass);
        if (dt == null) {
            dt = new LinkedList<Object>();
            _data.put(oclass, dt);
        }
        if (onTop) {
            ((LinkedList<Object>) dt).addFirst(o);
        } else {
            ((LinkedList<Object>) dt).addLast(o);
        }
    }

    @SuppressWarnings("unchecked")
    public void put(Object o, String key, String oclass) {
        Object dt = _data.get(oclass);
        if (dt == null) {
            dt = new HashMap<String, Object>();
            _data.put(oclass, dt);
        }
        ((HashMap<String, Object>) dt).put(key, o);
    }

    @SuppressWarnings("unchecked")
    public List<Object> list(String oclass) {

        Object dt = _data.get(oclass);

        List<Object> ret = null;
        if (dt == null) {
            ret = new ArrayList<Object>();
        } else if (dt instanceof List<?>) {
            ret = (List<Object>) dt;
        } else if (dt instanceof Map<?, ?>) {
            ret = new ArrayList<Object>();
            for (Object o : ((Map<String, Object>) dt).values())
                ret.add(o);
        } else {
            ret = new ArrayList<Object>();
            ret.add(dt);
        }

        return ret;
    }

    public void clear(String oclass) {
        _data.remove(oclass);
    }

    public Object get(String key, String oclass) {

        Object dt = _data.get(oclass);
        return dt == null ? null : (dt instanceof Map<?, ?> ? ((Map<?, ?>) dt).get(key) : null);
    }

    @SuppressWarnings("unchecked")
    public Map<String, Object> map(String oclass) {

        Object dt = _data.get(oclass);

        Map<String, Object> ret = null;
        if (dt == null) {
            ret = new HashMap<String, Object>();
        } else if (dt instanceof List<?>) {
            ret = new HashMap<String, Object>();
            int i = 0;
            for (Object o : ((List<Object>) dt)) {
                ret.put("" + i++, o);
            }
        } else if (dt instanceof Map<?, ?>) {
            ret = (Map<String, Object>) dt;
        } else {
            ret = new HashMap<String, Object>();
            ret.put("0", dt);
        }

        return ret;
    }

    /*
     * log to connected console
     */
    public void info(Object s) {
        _infos.add(s);
        if (_shell != null) {
            _shell.info(s.toString());
        }
    }

    /*
     * log to connected console
     */
    public void warn(Object s) {
        _warnings.add(s);
        if (_shell != null) {
            _shell.warn(s.toString());
        }
    }

    /*
     * log to connected console
     */
    public void error(Object s) {
        _errors.add(s);
        if (_shell != null) {
            _shell.error(s.toString());
        }
    }

    public static void debug(Object s) {
        if (get()._shell != null /* && Thinklab.get().getNotificationLevel() == INotification.DEBUG*/) {
            get()._shell.info(s.toString());
        }
    }
}
