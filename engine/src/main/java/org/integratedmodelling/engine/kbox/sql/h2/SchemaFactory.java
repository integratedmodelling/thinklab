/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.engine.kbox.sql.h2;

import java.lang.reflect.Field;
import java.lang.reflect.ParameterizedType;
import java.util.Map;

import net.jodah.typetools.TypeResolver;

import org.hsqldb.lib.Collection;
import org.integratedmodelling.collections.Pair;
import org.integratedmodelling.collections.Triple;
import org.integratedmodelling.common.utils.TypedMap;
import org.integratedmodelling.engine.geospace.literals.ShapeValue;
import org.integratedmodelling.engine.kbox.sql.h2.H2Kbox.Schema;
import org.integratedmodelling.engine.kbox.sql.h2.schema.CollectionSchema;
import org.integratedmodelling.engine.kbox.sql.h2.schema.CompoundSchema;
import org.integratedmodelling.engine.kbox.sql.h2.schema.MapSchema;
import org.integratedmodelling.engine.kbox.sql.h2.schema.NumberSchema;
import org.integratedmodelling.engine.kbox.sql.h2.schema.PairSchema;
import org.integratedmodelling.engine.kbox.sql.h2.schema.SpaceSchema;
import org.integratedmodelling.engine.kbox.sql.h2.schema.StringSchema;
import org.integratedmodelling.engine.kbox.sql.h2.schema.TripleSchema;

public class SchemaFactory {

    static TypedMap<Schema> schemata = new TypedMap<>();

    static {

        /*
         * TODO add other predefined schemata for concepts, time extents and space extents
         */
        schemata.put(String.class, new StringSchema());
        schemata.put(Integer.class, new NumberSchema<Integer>(Integer.class));
        schemata.put(Long.class, new NumberSchema<Long>(Long.class));
        schemata.put(Double.class, new NumberSchema<Double>(Double.class));
        schemata.put(Float.class, new NumberSchema<Float>(Float.class));
        schemata.put(ShapeValue.class, new SpaceSchema());
    }

    static public Schema getSchema(Field field) {
        if (field.getType().isArray()) {
            return new CollectionSchema(field.getType(), field.getType().getComponentType());
        } else if (Collection.class.isAssignableFrom(field.getType())) {
            ParameterizedType collectionType = (ParameterizedType) field.getGenericType();
            return new CollectionSchema(field.getType(), (Class<?>) collectionType.getActualTypeArguments()[0]);
        } else if (Map.class.isAssignableFrom(field.getType())) {
            Class<?>[] clss = TypeResolver.resolveRawArguments(Map.class, field.getType());
            return new MapSchema(field.getType(), clss[0], clss[1]);
        } else if (Map.class.isAssignableFrom(Pair.class)) {
            ParameterizedType collectionType = (ParameterizedType) field.getGenericType();
            return new PairSchema(field.getType(), (Class<?>) collectionType.getActualTypeArguments()[0], (Class<?>) collectionType
                    .getActualTypeArguments()[1]);
        } else if (Map.class.isAssignableFrom(Triple.class)) {
            ParameterizedType collectionType = (ParameterizedType) field.getGenericType();
            return new TripleSchema(field.getType(), (Class<?>) collectionType.getActualTypeArguments()[0], (Class<?>) collectionType
                    .getActualTypeArguments()[1], (Class<?>) collectionType
                    .getActualTypeArguments()[2]);
        }
        return getSchema(field.getType());

    }

    static public Schema getSchema(Class<?> cls) {

        if (schemata.containsKey(cls)) {
            return schemata.get(cls);
        }

        H2Schema ret = new CompoundSchema(cls);
        schemata.put(cls, ret);
        return ret;
    }

}
