/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.engine.kbox.sql.h2;

import java.io.File;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.integratedmodelling.api.monitoring.IMonitor;
import org.integratedmodelling.api.persistence.IKbox;
import org.integratedmodelling.api.persistence.IQuery;
import org.integratedmodelling.engine.kbox.sql.SQL;
import org.integratedmodelling.engine.modelling.monitoring.Monitor;
import org.integratedmodelling.engine.runtime.Session;
import org.integratedmodelling.exceptions.ThinklabException;
import org.integratedmodelling.exceptions.ThinklabRuntimeException;

/**
 * Poor-man hybernate with Thinklab-specialized query language, aware of semantics and time/space,
 * using assumptions and conventions to allow IKbox clean model with little or no configuration.
 * 
 * @author ferdinando.villa
 *
 */
public class H2Kbox implements IKbox {

    protected H2Database               database;
    protected Serializer               serializer;
    protected Deserializer             deserializer;
    protected QueryBuilder             queryBuilder;
    protected IMonitor                 monitor;
    protected Schema                   schema;

    private static Map<String, H2Kbox> kboxes = new HashMap<>();

    protected static IKbox set(String name, H2Kbox kbox) {
        kboxes.put(name, kbox);
        return kbox;
    }

    protected static IKbox get(String name) {
        if (kboxes.containsKey(name)) {
            return kboxes.get(name);
        }
        // TODO check if we should use a common session. Remember these may be used concurrently.
        H2Kbox kbox = new H2Kbox(name, new Monitor(-1, new Session()));
        kboxes.put(name, kbox);
        return kbox;
    };

    public H2Kbox(String name, IMonitor monitor) {
        database = H2Database.get(name);
        queryBuilder = new H2QueryBuilder();
        this.monitor = monitor;
    }

    public H2Kbox(String name, File directory, IMonitor monitor) {
        database = H2Database.get(directory, name);
        queryBuilder = new H2QueryBuilder();
        this.monitor = monitor;
    }

    @Override
    public List<Object> query(IQuery query) throws ThinklabException {
        return querySql(queryBuilder.getSQL(query));
    }

    /**
     * Call the H2 recover tool. 
     * @throws ThinklabException
     */
    public void recover() throws ThinklabException {
        database.recover();
    }

    public List<Object> querySql(String query) throws ThinklabException {

        final List<Object> ret = deserializer instanceof DeferredDeserializer ? new H2Result(this, monitor)
                : new ArrayList<Object>();

        deserializer.setKbox(this);
        database.query(query, new SQL.ResultHandler() {

            @Override
            public void onRow(ResultSet rs) {
                if (deserializer instanceof DeferredDeserializer) {
                    try {
                        ((DeferredDeserializer) deserializer).addId(rs.getLong(H2Schema.FIELD_PKEY));
                    } catch (SQLException e) {
                        throw new ThinklabRuntimeException(e);
                    }
                } else {
                    ret.add(((DirectDeserializer) deserializer).deserialize(rs));
                }
            }

            @Override
            public void nResults(int nres) {
            }
        });

        return ret;
    }

    @Override
    public long store(Object o) throws ThinklabException {
        return database.storeObject(o, 0l, serializer, monitor);
    }

    @Override
    public Object retrieve(long id) throws ThinklabException {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public void remove(long id) throws ThinklabException {
        // TODO Auto-generated method stub

    }

    @Override
    public void remove(IQuery query) throws ThinklabException {
        // TODO Auto-generated method stub

    }

    @Override
    public void clear() throws ThinklabException {
        // TODO Auto-generated method stub

    }

    protected void setSerializer(Serializer serializer) {
        this.serializer = serializer;
    }

    protected void setDeserializer(Deserializer deserializer) {
        this.deserializer = deserializer;
    }

    protected void setQueryBuilder(QueryBuilder querybuilder) {
        this.queryBuilder = querybuilder;
    }

    protected void setSchema(Class<?> cls, Schema schema) {
        SchemaFactory.schemata.put(cls, schema);
    }

    // TODO these interfaces should go in implementation.

    /**
     * Turns an object into SQL instructions to store it.
     * 
     * @author ferdinando.villa
     *
     */
    public static interface Serializer {

        /**
         * 
         * @param o
         * @param schema
         * @param primaryKey
         * @param foreignKey
         * @return
         */
        String serialize(Object o, Schema schema, long primaryKey, long foreignKey);
    }

    /**
     * Turns a SQL result into an object. Only a tag interface: the actual
     * working API is in the two derived ones. 
    
     * @author ferdinando.villa
     *
     */
    public static abstract interface Deserializer {

        void setKbox(IKbox h2Kbox);

    }

    /**
     * Builds the object directly in the query result list. Use when building the
     * objects is fast and cheap.
     * 
     * @author ferdinando.villa
     *
     */
    public static interface DirectDeserializer extends Deserializer {

        /**
         * Passed a result set localized on a valid match. Just get your
         * things from it and return - its lifetime is controlled outside.
         * Should return null in error.
         * 
         * @param rs
         * @return
         */
        Object deserialize(ResultSet rs);

    }

    /**
     * Turns a database unique ID into an object. Use when building objects
     * is complex, requiring more than one query, or when performance is
     * an issue when creating objects. In this case, the query strategy will
     * return the OID field for all matches to it, and the serializer will have
     * to behave like a list that lazily extracts the objects when get() is
     * called.
     * 
     * @author ferdinando.villa
     *
     */
    public static interface DeferredDeserializer extends Deserializer, List<Object> {
        void addId(long id);
    }

    public static interface QueryBuilder {

        String getSQL(IQuery query);

    }

    public static interface Schema {

        /**
         * Return non-null if this schema creates a whole table.
         * @return
         */
        String getCreateSQL();

        /**
         * Return non-null if this schema is only for a field.
         * @return
         */
        String getFieldName();

        /**
         * Same 
         * @return
         */
        String getFieldType();

    }

}
