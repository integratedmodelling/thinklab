package org.integratedmodelling.engine.mca;

import org.integratedmodelling.api.knowledge.IKnowledge;
import org.integratedmodelling.api.modelling.IDirectObservation;

public interface IAlternative {

    boolean isDistributed();

    double getValueOf(IKnowledge k, int offset, IDirectObservation offsetContext);

    boolean hasCriterion(IKnowledge observable);

    IDirectObservation getSubject();

    /**
     * If the alternative is not distributed, this will return the subject ID, otherwise some progressive
     * string made from it.
     * 
     * @return
     */
    String getId();

}
