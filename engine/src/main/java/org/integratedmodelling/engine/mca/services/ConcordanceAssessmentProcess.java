/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.engine.mca.services;

import java.util.HashMap;
import java.util.Map;

import org.integratedmodelling.api.modelling.IDirectObservation;
import org.integratedmodelling.api.modelling.IModel;
import org.integratedmodelling.api.modelling.IObservable;
import org.integratedmodelling.api.modelling.IObservation;
import org.integratedmodelling.api.modelling.IProcess;
import org.integratedmodelling.api.modelling.IScale;
import org.integratedmodelling.api.modelling.IState;
import org.integratedmodelling.api.modelling.resolution.IResolutionContext;
import org.integratedmodelling.api.modelling.runtime.IProcessContextualizer;
import org.integratedmodelling.api.modelling.scheduling.ITransition;
import org.integratedmodelling.api.monitoring.IMonitor;
import org.integratedmodelling.api.project.IProject;
import org.integratedmodelling.api.services.annotations.Prototype;
import org.integratedmodelling.common.vocabulary.NS;
import org.integratedmodelling.engine.mca.Assessment;
import org.integratedmodelling.engine.mca.MCA;
import org.integratedmodelling.engine.mca.MCAComponent;
import org.integratedmodelling.exceptions.ThinklabException;

/**
 * Any MCA-driven decision process that makes choices based on evaluating criteria. Will look for
 * pairwise and/or absolute values for all the inputs that have a criterion role, and produce a
 * priority ranking for them.
 * 
 * @author ferdinando.villa
 *
 */
@Prototype(
        id = "mca.concordance-assessment",
        componentId = "im.mca",
        args = {
                "# levels",
                Prototype.INT,
                "# method",
                "PROMETHEE|EVAMIX|ELECTRE3"
        },
        returnTypes = { NS.PROCESS_CONTEXTUALIZER })
public class ConcordanceAssessmentProcess implements IProcessContextualizer {

    private IObservable        concordance;
    private String             concordanceId;

    private MCA.Method         method  = MCA.Method.EVAMIX;
    private IScale             scale;
    private boolean            canDispose;
    private IResolutionContext resolutionContext;
    private IDirectObservation context;
    private IProcess           process;
    private IMonitor           monitor;
    private Assessment         assessment;
    private int                nLevels = 5;

    @Override
    public boolean canDispose() {
        return canDispose;
    }

    @Override
    public void setContext(Map<String, Object> parameters, IModel model, IProject project) {
        if (parameters.containsKey("method")) {
            switch (parameters.get("method").toString().toUpperCase()) {
            case "EVAMIX":
                method = MCA.Method.EVAMIX;
                break;
            case "ELECTRE3":
                method = MCA.Method.ELECTRE3;
                break;
            case "PROMETHEE":
                method = MCA.Method.PROMETHEE;
                break;
            }
        }
        if (parameters.containsKey("levels")) {
            this.nLevels = ((Number) parameters.get("levels")).intValue();
        }
    }

    @Override
    public Map<String, IObservation> initialize(IProcess process, IDirectObservation context, IResolutionContext resolutionContext, Map<String, IObservable> expectedInputs, Map<String, IObservable> expectedOutputs, IMonitor monitor)
            throws ThinklabException {

        this.context = context;
        this.process = process;
        this.scale = context.getScale();
        this.monitor = monitor;

        MCAComponent.NS.synchronize();
        
        this.resolutionContext = resolutionContext;

        canDispose = !this.scale.isTemporallyDistributed();
        
        /*
        * lookup the concordance output observable
        */
        for (String s : expectedOutputs.keySet()) {
            if (expectedOutputs.get(s).is(MCAComponent.NS.CONCORDANCE)) {
                this.concordance = expectedOutputs.get(s);
                this.concordanceId = s;
            }
        }

        Map<String, IObservation> ret = new HashMap<>();

        this.assessment = new Assessment(process, context, concordance, resolutionContext, monitor);

        if (assessment.isError()) {
            return ret;
        }

        this.assessment.setDiscretizationLevel(nLevels);


        return assessment.compute(null);
    }

    @Override
    public Map<String, IObservation> compute(ITransition transition, Map<String, IState> inputs)
            throws ThinklabException {

        canDispose = transition.isLast();
        return assessment.compute(transition);

    }

    private boolean inputsHaveChanged(Map<String, IState> inputs) {
        // TODO Auto-generated method stub
        return false;
    }

    private boolean criteriaHaveChanged(Map<String, IState> inputs) {
        // TODO Auto-generated method stub
        return false;
    }

}
