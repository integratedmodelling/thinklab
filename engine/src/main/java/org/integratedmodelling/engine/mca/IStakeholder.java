package org.integratedmodelling.engine.mca;

import java.util.List;

import org.integratedmodelling.api.modelling.IDirectObservation;
import org.integratedmodelling.api.modelling.scheduling.ITransition;
import org.integratedmodelling.exceptions.ThinklabException;

public interface IStakeholder {

    IDirectObservation getSubject();
    
    /**
     * Return all alternatives retained, making sure everything is initialized.
     * @return
     */
    List<IAlternative> getAlternatives(ITransition transition);

    void rankAlternatives(ITransition transition) throws ThinklabException;

    boolean canValue(IAlternative alternative);

}
