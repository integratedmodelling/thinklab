/*******************************************************************************
 *  Copyright (C) 2007, 2014:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.engine.geospace.coverage.vector;

import java.io.IOException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

import org.geotools.data.DataStore;
import org.geotools.data.DataStoreFinder;
import org.geotools.data.wfs.WFSDataStoreFactory;
import org.integratedmodelling.common.utils.NetUtilities;
import org.integratedmodelling.exceptions.ThinklabException;
import org.integratedmodelling.exceptions.ThinklabIOException;

public class WFSCoverage extends AbstractVectorCoverage {

    public static final int TIMEOUT     = 100000;
    public static final int BUFFER_SIZE = 512;

    DataStore               store       = null;

    public WFSCoverage(URL service, String id, String valueField, String filter) throws ThinklabException {
        super(service, id, valueField, filter);
    }

    @Override
    protected DataStore getDataStore() throws ThinklabException {

        if (store == null) {

            Integer wfsTimeout = TIMEOUT;
            Integer wfsBufsize = BUFFER_SIZE;

            // if we don't do this, it will take the first layer in WFS
            coverageId = layerName;

            Map<Object, Object> connectionParameters = new HashMap<Object, Object>();
            connectionParameters.put(WFSDataStoreFactory.URL.key, sourceUrl
                    + "?REQUEST=getCapabilities&VERSION=1.1.0");
            connectionParameters.put(WFSDataStoreFactory.TIMEOUT.key, wfsTimeout);
            connectionParameters.put(WFSDataStoreFactory.BUFFER_SIZE.key, wfsBufsize);

            if (!NetUtilities.urlResponds(this.sourceUrl)) {
                throw new ThinklabIOException("connection to WFS host failed for layer " + this.coverageId);
            }

            try {
                store = DataStoreFinder.getDataStore(connectionParameters);
            } catch (IOException e) {
                throw new ThinklabIOException(e);
            }
        }

        return store;
    }

}
