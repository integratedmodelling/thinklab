/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.engine.geospace.gis;

import java.util.ArrayList;

import org.geotools.feature.FeatureCollection;
import org.geotools.geometry.Envelope2D;
import org.integratedmodelling.api.modelling.IState;
import org.integratedmodelling.engine.geospace.coverage.raster.AbstractRasterCoverage;
import org.integratedmodelling.engine.geospace.coverage.raster.RasterCoverage;
import org.integratedmodelling.engine.geospace.coverage.vector.VectorCoverage;
import org.integratedmodelling.engine.geospace.extents.Grid;
import org.integratedmodelling.engine.visualization.VisualizationFactory;
import org.integratedmodelling.exceptions.ThinklabException;
import org.opengis.feature.simple.SimpleFeature;
import org.opengis.feature.simple.SimpleFeatureType;

/**
 * A Thinklab-aware vectorizer that produces a Thinklab coverage and optionally, an observation
 * structure with the recognizer features. Can eventually be used as a datasource or kbox.
 * 
 * @author Ferdinando Villa
 *
 */
public class ThinklabVectorizer {

    public static VectorCoverage vectorize(AbstractRasterCoverage rCoverage, Grid extent)
            throws ThinklabException {

        ArrayList<Double> nans = new ArrayList<Double>();
        nans.add(Double.NaN);
        nans.add(0.0);

        // cross fingers
        Envelope2D bounds = new Envelope2D(extent.getEnvelope());
        FeatureCollection<SimpleFeatureType, SimpleFeature> features = null;
        // try {
        // // features =
        // // RasterToVectorProcess.process(rCoverage.getCoverage(), 0, bounds, nans, null);
        // } catch (ProcessException e) {
        // throw new ThinklabValidationException(e);
        // }

        return null;
    }

    /*
     * It's actually an "objectify" function. The features should have their state as an
     * attribute.
     * 
     * expects fully categorized states.
     * TODO different polygons for different states if necessary, according to metadata. Should
     * refuse to vectorize anything continuous.
     */
    public static VectorCoverage vectorize(IState state, Grid extent) throws ThinklabException {

        return vectorize(new RasterCoverage(state.getObservable().getType().toString() + "_objects", extent,
                VisualizationFactory.get().getStateDataAsNumbers(state)), extent);
    }

}
