/*******************************************************************************
 *  Copyright (C) 2007, 2014:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.engine.geospace.gis;

import java.awt.image.RenderedImage;

import javax.media.jai.iterator.RandomIter;
import javax.media.jai.iterator.RandomIterFactory;

import org.geotools.coverage.grid.GridCoverage2D;
import org.geotools.feature.FeatureIterator;
import org.geotools.geometry.jts.ReferencedEnvelope;
import org.integratedmodelling.api.modelling.IObserver;
import org.integratedmodelling.api.monitoring.IMonitor;
import org.integratedmodelling.engine.geospace.Geospace;
import org.integratedmodelling.engine.geospace.coverage.raster.DummyActivationLayer;
import org.integratedmodelling.engine.geospace.coverage.raster.RasterActivationLayer;
import org.integratedmodelling.engine.geospace.coverage.raster.RasterCoverage;
import org.integratedmodelling.engine.geospace.coverage.vector.AbstractVectorCoverage;
import org.integratedmodelling.engine.geospace.extents.Grid;
import org.integratedmodelling.engine.geospace.interfaces.IGridMask;
import org.integratedmodelling.engine.geospace.literals.ShapeValue;
import org.integratedmodelling.exceptions.ThinklabException;
import org.integratedmodelling.exceptions.ThinklabValidationException;
import org.opengis.feature.simple.SimpleFeature;

public class ThinklabRasterizer {

    /**
     * Convert the passed vector coverage into a raster coverage that adopts the 
     * passed extent.
     * 
     * @param vCoverage
     * @param extent
     * @return
     */
    public static RasterCoverage rasterize(AbstractVectorCoverage vCoverage, String valueId, float noData, Grid extent, IObserver observer, String valueDefault, IMonitor monitor, boolean isWFS)
            throws ThinklabException {

        if (extent.getCRS() != null)
            vCoverage = (AbstractVectorCoverage) vCoverage.requireMatch(extent, observer, monitor, false);

        GridCoverage2D coverage = null;
        FeatureRasterizer rasterizer = new FeatureRasterizer(extent, noData, (valueId == null ? null
                : vCoverage.getAttributeDescriptor(valueId)), monitor);

        FeatureIterator<SimpleFeature> iterator = null;
        try {

            ReferencedEnvelope dataEnvelope = null;

            try {
                dataEnvelope = extent.getEnvelope().transform(vCoverage.getCoordinateReferenceSystem(), true);
            } catch (Exception e) {
                throw new ThinklabValidationException(e);
            }

            iterator = vCoverage.getFeatureIterator(extent.getEnvelope(), valueId);

            coverage = rasterizer.rasterize(vCoverage.getLayerName() + "_" + (valueId == null ? "" : valueId)
                    + "_raster", iterator, valueId, observer, valueDefault, dataEnvelope,
                    // FIXME this MAY work for WFS in the current implementation, but how the hell do I know.
                    Geospace.getCRSIdentifier(dataEnvelope.getCoordinateReferenceSystem(), true)
                            .equals("EPSG:4326")
                            && isWFS);

        } finally {
            if (iterator != null)
                iterator.close();
        }

        RasterCoverage ret = new RasterCoverage(vCoverage.getLayerName() + "_"
                + (valueId == null ? "" : valueId) + "_raster", coverage);

        if (rasterizer.getClassification() != null) {
            ret.setClassMappings(rasterizer.getClassification());
        }

        return ret;
    }

    private static IGridMask rasterizeShape(ShapeValue shape, Grid grid, int value) throws ThinklabException {

        if (grid.getXCells() * grid.getYCells() < 16) {
            return new DummyActivationLayer(grid);
        }

        RasterActivationLayer ret = (RasterActivationLayer) createMask(grid);
        GridCoverage2D coverage = null;

        ret.deactivate();
        FeatureRasterizer rasterizer = new FeatureRasterizer(grid, 0.0f, null, null);
        coverage = rasterizer.rasterize(shape, value);

        /*
         * turn coverage into mask
         */
        RenderedImage image = coverage.getRenderedImage();
        RandomIter itera = RandomIterFactory.create(image, null);

        for (int i = 0; i < grid.getCellCount(); i++) {

            int[] xy = grid.getXYOffsets(i);

            if (itera.getSampleDouble(xy[0], xy[1], 0) > 0.0) {
                ret.activate(xy[0], xy[1]);
            }
        }
        return ret;
    }

    public static IGridMask createMask(Grid grid) {
        RasterActivationLayer ret = new RasterActivationLayer(grid.getXCells(), grid
                .getYCells(), false, grid);
        ret.setCRS(grid.getCRS());
        return ret;
    }

    public static IGridMask createMask(ShapeValue shape, Grid grid) throws ThinklabException {
        return rasterizeShape(shape, grid, 1);
    }

    public static IGridMask addToMask(ShapeValue shape, IGridMask mask) throws ThinklabException {
        mask.or(rasterizeShape(shape, mask.getGrid(), 1));
        return mask;
    }

}
