/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.engine.geospace.datasources;

import java.io.File;

import org.geotools.coverage.grid.io.AbstractGridCoverage2DReader;
import org.geotools.coverage.grid.io.AbstractGridFormat;
import org.geotools.coverage.grid.io.GridFormatFinder;
import org.integratedmodelling.api.metadata.IMetadata;
import org.integratedmodelling.common.metadata.Metadata;
import org.integratedmodelling.engine.geospace.coverage.ICoverage;
import org.integratedmodelling.engine.geospace.coverage.raster.RasterCoverage;
import org.integratedmodelling.exceptions.ThinklabException;
import org.integratedmodelling.exceptions.ThinklabUnsupportedOperationException;

public class RasterFileDataSource extends RegularRasterGridDataSource {

    AbstractGridFormat _format   = null;
    File               _file     = null;
    IMetadata          _metadata = new Metadata();

    public RasterFileDataSource(String id, String file) throws ThinklabUnsupportedOperationException {

        _id = id;
        File f = new File(file);
        if (f.exists()) {
            _file = f;
            try {
                _format = GridFormatFinder.findFormat(file);
            } catch (Throwable e) {

                /*
                 * stupid SPI throws exceptions when optional formats are not available 
                 * because we haven't bought ARC. Just ignore, and _format will be null if
                 * any real problem happened.
                 */
            }
        }

        if (_format == null) {
            throw new ThinklabUnsupportedOperationException("file " + file
                    + " cannot be read or does not have a supported raster format");
        }
    }

    @Override
    protected ICoverage readData() throws ThinklabException {

        if (_coverage == null) {
            AbstractGridCoverage2DReader reader = _format.getReader(_file);
            try {
                return new RasterCoverage(_id, reader.read(null));
            } catch (Exception e) {
                throw new ThinklabUnsupportedOperationException("file " + _file
                        + " cannot be read or does not have a supported raster format");
            }
        }
        return _coverage;
    }

    @Override
    public boolean isAvailable() {
        return _file.exists() && _file.canRead();
    }
}
