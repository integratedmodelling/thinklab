/*******************************************************************************
 *  Copyright (C) 2007, 2014:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.engine.geospace.coverage;

import java.io.File;

import org.geotools.geometry.jts.ReferencedEnvelope;
import org.integratedmodelling.api.modelling.IObserver;
import org.integratedmodelling.api.monitoring.IMonitor;
import org.integratedmodelling.engine.geospace.extents.Area;
import org.integratedmodelling.exceptions.ThinklabException;
import org.integratedmodelling.exceptions.ThinklabValidationException;
import org.opengis.referencing.crs.CoordinateReferenceSystem;

public interface ICoverage {

    /**
     * Return the value associated with the n-th subdivision according to the SubdivisionOrdering associated with
     * the coverage. The value should be what the passed conceptual model wants.
     * 
     * @param subdivisionOrder
     * @return 
     * @throws ThinklabValidationException 
     * @throws ThinklabException 
     */
    public abstract Object getSubdivisionValue(int subdivisionOrder, Area extent)
            throws ThinklabValidationException;

    public abstract String getSourceUrl();

    public abstract String getCoordinateReferenceSystemCode() throws ThinklabException;

    public abstract CoordinateReferenceSystem getCoordinateReferenceSystem();

    public abstract String getLayerName();

    /**
     * Return a coverage that matches the passed raster extent, warping if necessary. Return self if
     * the match is there, a new one otherwise. If the coverage cannot be adjusted, throw an exception.
     * 
     * @param arealExtent the extent that this coverage needs to cover.
     * @param observer the type of observation we're expected to make with this transformation
     * @param _monitor report info and stuff here
     * @param allowClassChange if true, we can generate a coverage of a different class (e.g. a raster
     *        instead of a vector); otherwise we must stay our type, and throw an exception if impossible.
     * @return a new coverage or self. It should never return null.
     */
    public abstract ICoverage requireMatch(Area arealExtent, IObserver observer, IMonitor monitor,
            boolean allowClassChange) throws ThinklabException;

    /**
     * Write the coverage to a suitable GIS format, determined by the extension in the
     * requested file name.
     * 
     * @param f file to write to.
     * @throws ThinklabException
     */
    public void write(File f) throws ThinklabException;

    /**
     * 
     * @return
     */
    public abstract ReferencedEnvelope getEnvelope();

}
