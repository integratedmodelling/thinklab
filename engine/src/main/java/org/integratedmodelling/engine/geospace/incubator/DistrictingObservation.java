/*******************************************************************************
 *  Copyright (C) 2007, 2014:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.engine.geospace.incubator;

/**
 * A districting observation transforms a gridded context into a spatial classification, and
 * turns the states of its dependencies into a distribution of state values for each class.
 *  
 * @author Ferdinando
 *
// */
//@InstanceImplementation(concept=Geospace.CLASSIFIED_GRID)
//public class DistrictingObservation extends Observation {
//
//	private String method = "k-means";
//	private int initialK;
//	private double stoppingThreshold = 0.2;
//	private double varianceRatio = 9.0;
//	private double membershipRatio = 0.1;
//	private double separationRatio = 0.25;
//
//	@Override
//	protected IConceptualModel createMissingConceptualModel()
//			throws ThinklabException {
//		return 
//			this.method .equals("k-means") ?
//					new ClusteringRasterModel(getObservable(), initialK) :
//					new ClusteringRasterModel(
//							getObservable(),
//							initialK, stoppingThreshold, varianceRatio, 
//							membershipRatio, separationRatio);
//	}
//
//	@Override
//	public void initialize(IInstance i) throws ThinklabException {
//		
//		super.initialize(i);
//		
//		/*
//		 * recover clustering parameters, "infer" method
//		 */
//		initialK = i.get("geospace:hasInitialK").asNumber().asInteger();
//		
//		IValue st = i.get("geospace:hasStoppingThreshold");
//		IValue vr = i.get("geospace:hasVarianceRatio");
//		IValue mr = i.get("geospace:hasMembershipRatio");
//		IValue sr = i.get("geospace:hasSeparationRatio");
//		
//		if (st != null) {
//			stoppingThreshold = st.asNumber().asDouble();
//			method = "isodata";
//		}
//		if (vr != null) {
//			varianceRatio = st.asNumber().asDouble();
//			method = "isodata";
//		}
//		if (mr != null) {
//			membershipRatio = st.asNumber().asDouble();
//			method = "isodata";
//		}
//		if (sr != null) {
//			separationRatio = st.asNumber().asDouble();
//			method = "isodata";
//		}
//		
//	}
//
//}
