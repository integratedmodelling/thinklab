/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.engine.geospace.datasources;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.integratedmodelling.api.metadata.IMetadata;
import org.integratedmodelling.api.modelling.IAction;
import org.integratedmodelling.api.modelling.IExtent;
import org.integratedmodelling.api.modelling.IObserver;
import org.integratedmodelling.api.modelling.IScale;
import org.integratedmodelling.api.modelling.runtime.IActiveDataSource;
import org.integratedmodelling.api.modelling.runtime.IActuator;
import org.integratedmodelling.api.modelling.runtime.IRawActuator;
import org.integratedmodelling.api.modelling.scheduling.ITransition;
import org.integratedmodelling.api.monitoring.IMonitor;
import org.integratedmodelling.api.monitoring.IMonitorable;
import org.integratedmodelling.common.HashableObject;
import org.integratedmodelling.common.configuration.KLAB;
import org.integratedmodelling.common.data.AbstractTableSet;
import org.integratedmodelling.common.metadata.Metadata;
import org.integratedmodelling.common.model.actuators.StateActuator;
import org.integratedmodelling.engine.geospace.Geospace;
import org.integratedmodelling.engine.geospace.coverage.ICoverage;
import org.integratedmodelling.engine.geospace.coverage.raster.AbstractRasterCoverage;
import org.integratedmodelling.engine.geospace.coverage.raster.RasterCoverage;
import org.integratedmodelling.engine.geospace.extents.Grid;
import org.integratedmodelling.engine.geospace.extents.SpaceExtent;
import org.integratedmodelling.engine.geospace.literals.ShapeValue;
import org.integratedmodelling.engine.modelling.runtime.Scale;
import org.integratedmodelling.exceptions.ThinklabException;
import org.integratedmodelling.exceptions.ThinklabRuntimeException;
import org.integratedmodelling.exceptions.ThinklabValidationException;

public abstract class RegularRasterGridDataSource extends HashableObject implements IActiveDataSource,
        IMonitorable {

    protected ICoverage _coverage     = null;
    protected Grid      _finalExtent  = null;
    private IMetadata   _metadata     = new Metadata();
    protected IMonitor  _monitor;
    protected String    _id;
    int                 _errors       = 0;
    Map<?, ?>           _valueMapping = null;

    /*
     * set in specialized constructor when we supply it with a coverage that is
     * already matched to the final context of use. This happens when a datasource
     * is created to provide an accessor for another that has transformed the
     * original coverage.
     */
    private boolean _preMatched = false;

    public RegularRasterGridDataSource() {
    }

    @Override
    public IMetadata getMetadata() {
        return _metadata;
    }

    public RegularRasterGridDataSource(ICoverage coverage, Grid Grid) {
        this._coverage = coverage;
        this._finalExtent = Grid;
        this._preMatched = true;
    }

    public void setValueMapping(Map<?, ?> mapping) {
        _valueMapping = mapping;
    }

    @Override
    public void setMonitor(IMonitor monitor) {
        _monitor = monitor;
    }

    @Override
    public IMonitor getMonitor() {
        return _monitor;
    }

    public Object getValue(int index) {

        if (!((AbstractRasterCoverage) _coverage).isLoaded()) {
            try {
                ((RasterCoverage) (this._coverage)).loadData();
            } catch (ThinklabException e1) {
                throw new ThinklabRuntimeException(e1);
            }
        }

        try {

            Object ret = _coverage.getSubdivisionValue(index, _finalExtent);
            if (!(ret instanceof Number))
                return ret;
            ret = ((Number) ret).doubleValue();

            /*
             * TODO/FIXME - this is already done in the coverage, although differently (just
             * comparing the value) - will never get here as it will be already a Double.NaN
             * if the same nodata values of the coverage are used.
             */
            double[] nd = ((AbstractRasterCoverage) _coverage).getNodataValue();
            if (nd != null && ret != null && (ret instanceof Double) && !Double.isNaN((Double) ret)) {
                for (double d : nd) {
                    if (((Double) ret).equals(d)) {
                        ret = Double.NaN;
                        break;
                    }
                }
            }
            return _valueMapping == null ? ret : _valueMapping.get(AbstractTableSet.sanitizeKey(ret));

        } catch (ThinklabException e) {
            throw new ThinklabRuntimeException(e);
        }
    }

    @Override
    public IActuator getAccessor(IScale context, IObserver observer, IMonitor monitor)
            throws ThinklabException {

        if (_coverage == null) {
            _coverage = readData();
        }

        if (!_preMatched)
            _finalExtent = getFinalExtent(context);

        return new RasterGridActuator(new ArrayList<IAction>(), observer, monitor);
    }

    /*
     * -------------------------------------------------------------------------------------------
     * read the coverage
     * -------------------------------------------------------------------------------------------
     */

    /**
     * Do whatever is needed to instantiate _coverage.
     * @throws ThinklabException
     */
    protected abstract ICoverage readData() throws ThinklabException;

    /**
     * Return the final grid extent implied by the context. It should also validate
     * the context, ensuring we don't want multiplicity in domains where we cannot
     * provide it.
     * 
     * @param context
     * @return
     */
    protected Grid getFinalExtent(IScale context) throws ThinklabException {

        IExtent space = context.getSpace();

        if (!(space instanceof SpaceExtent && ((SpaceExtent) space).isGrid())) {
            throw new ThinklabValidationException("cannot compute a raster datasource in a non-grid context");
        }

        // if we got here, the resolver wanted us, so let's not take responsibility for this.
        // if (space.getMultiplicity() != context.getMultiplicity()) {
        // throw new ThinklabValidationException(
        // "extents requested to raster datasource span more domains than space");
        // }

        return ((SpaceExtent) space).getGrid();
    }

    /*
     * -------------------------------------------------------------------------------------------------
     * simple accessor
     * -------------------------------------------------------------------------------------------------
     */
    class RasterGridActuator extends StateActuator implements IRawActuator {

        public RasterGridActuator(List<IAction> actions, IObserver observer, IMonitor monitor) {
            super(actions, monitor);
            _observer = observer;
        }

        boolean   isFirst = true;
        int       _idx;
        IObserver _observer;

        @Override
        public String toString() {
            return "[raster " + _coverage.getLayerName() + "]";
        }

        @Override
        public void process(int stateIndex, ITransition transition) throws ThinklabException {
            _idx = stateIndex;
        }

        @Override
        public Object getValue(String outputKey) {
            if (isFirst && !_preMatched) {
                try {
                    KLAB.info(_coverage.getEnvelope() + " -> " + _finalExtent);
                    _coverage = _coverage.requireMatch(_finalExtent, _observer, monitor, false);
                    KLAB.info("match achieved for " + _coverage.getLayerName());
                } catch (ThinklabException e) {
                    throw new ThinklabRuntimeException("error retrieving data for " + outputKey + ": "
                            + e.getMessage());
                }
                isFirst = false;
            }

            return RegularRasterGridDataSource.this.getValue(_idx);
        }

        @Override
        public String getName() {
            return _coverage.getLayerName();
        }

        @Override
        public String getDatasourceLabel() {
            return "[raw data from: " + _coverage.getLayerName() + "]";
        }

    }

    @Override
    public IScale getCoverage() {

        Scale ret = new Scale();

        try {
            _coverage = readData();
        } catch (ThinklabException e) {
            KLAB.warn("raster data source couldn't be read: " + e.getMessage());
            if (_monitor != null && _errors == 0) {
                _monitor.error("raster data source " + _id + " is inaccessible");
                _errors++;
            }
            throw new ThinklabRuntimeException(e);
        }

        try {
            ShapeValue shape = new ShapeValue(_coverage.getEnvelope());
            shape = shape.transform(Geospace.get().getDefaultCRS());
            int x = ((AbstractRasterCoverage) _coverage).getXCells();
            int y = ((AbstractRasterCoverage) _coverage).getYCells();
            ret.mergeExtent(new SpaceExtent(shape, x, y), true);
        } catch (ThinklabException e) {
            // shouldn't happen - just in case
            if (_monitor != null && _errors == 0) {
                _monitor.error("raster data source " + _id + " is corrupted");
                _errors++;
            }
            throw new ThinklabRuntimeException(e);
        }

        return ret;
    }

}
