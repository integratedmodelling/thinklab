/*******************************************************************************
 *  Copyright (C) 2007, 2014:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.engine.geospace.districting.utils;

import java.util.ArrayList;

import org.integratedmodelling.engine.geospace.exceptions.ThinklabDistrictingException;

public class DistrictingResults {

    private int[] typeset;
    private ArrayList<Integer> pointsPerCluster;
    private ArrayList<Double>[] centroids;
    private ArrayList<Double>[] stdevs;
    private int iterations;
    private int initialK;
    private int finalK;
    private double[] datasetVariance;
    private String[] variableNames;

    public void setTypeset(int[] t) {
        typeset = t;
    }

    public void setPointsPerCluster(ArrayList<Integer> p) {
        pointsPerCluster = p;
    }

    public void setCentroids(ArrayList<Double>[] c) {
        centroids = c;
    }

    public void setStdevs(ArrayList<Double>[] s) {
        stdevs = s;
    }

    public void setIterations(int i) {
        iterations = i;
    }

    public void setInitialK(int k) {
        initialK = k;
    }

    public void setFinalK(int k) {
        finalK = k;
    }

    public void setDatasetVariance(double[] d) {
        datasetVariance = d;
    }

    public void setVariableNames(String[] v) throws ThinklabDistrictingException {
        if (v.length != centroids.length) {
            throw new ThinklabDistrictingException("Invalid variable names list: must have same length "
                    + "as number of variables in districting results.");
        }
        variableNames = v;
    }

    public int[] getTypeset() {
        return typeset;
    }

    public ArrayList<Integer> getPointsPerCluster() {
        return pointsPerCluster;
    }

    public ArrayList<Double>[] getCentroids() {
        return centroids;
    }

    public double[] getCentroids(int districtIndex) {

        double[] ret = new double[centroids.length];

        for (int i = 0; i < centroids.length; i++)
            ret[i] = getCentroids()[i].get(districtIndex);

        return ret;
    }

    public ArrayList<Double>[] getStdevs() {
        return stdevs;
    }

    public int getIterations() {
        return iterations;
    }

    public int getInitialK() {
        return initialK;
    }

    public int getFinalK() {
        return finalK;
    }

    public double[] getDatasetVariance() {
        return datasetVariance;
    }

    public String[] getVariableNames() {
        return variableNames;
    }

}
