/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.engine.geospace.gis;

import java.awt.image.RenderedImage;

import javax.media.jai.iterator.RandomIter;
import javax.media.jai.iterator.RandomIterFactory;

import org.geotools.coverage.grid.GridCoverage2D;
import org.integratedmodelling.api.modelling.IDirectObservation;
import org.integratedmodelling.api.modelling.IObservable;
import org.integratedmodelling.api.modelling.IObserver;
import org.integratedmodelling.api.modelling.IScale;
import org.integratedmodelling.common.states.State;
import org.integratedmodelling.engine.geospace.extents.Grid;
import org.integratedmodelling.engine.geospace.extents.SpaceExtent;
import org.integratedmodelling.exceptions.ThinklabRuntimeException;

/**
 * Wraps a GridCoverage2D into a IState to enable switching back and forth without
 * copying data.
 * 
 * TODO unused, but should be used.
 * 
 * @author Ferd
 *
 */
public class Coverage2DState extends State {

    GridCoverage2D              _coverage;
    private final Grid          _grid;
    private final RenderedImage _image;
    private final RandomIter    _itera;

    public Coverage2DState(GridCoverage2D coverage, IObservable observable, IScale scale, IObserver observer,
            IDirectObservation context) {

        // FIXME see what to do with dynamic state
        super(observable, scale, false, context);
        SpaceExtent ext = (SpaceExtent) scale.getSpace();

        if (ext.getGrid() == null) {
            throw new ThinklabRuntimeException("cannot return a raster layer from a non-grid extent");
        }

        _grid = ext.getGrid();
        _image = coverage.getRenderedImage();
        _itera = RandomIterFactory.create(_image, null);
        _coverage = coverage;
    }

    @Override
    public Object getValue(int contextIndex) {
        int[] xy = _grid.getXYOffsets(contextIndex);
        return _itera.getSampleDouble(xy[0], xy[1], 0);
    }

    @Override
    public long getValueCount() {
        return (_grid.getXCells() * _grid.getYCells());
    }

    public GridCoverage2D getCoverage() {
        return _coverage;
    }

}
