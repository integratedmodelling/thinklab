/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
//package org.integratedmodelling.engine.geospace.gis.operators;
//
//import org.integratedmodelling.api.modelling.IModel;
//import org.integratedmodelling.api.modelling.IObservable;
//import org.integratedmodelling.api.modelling.IObserver;
//import org.integratedmodelling.api.modelling.IState;
//import org.integratedmodelling.api.modelling.ISubject;
//import org.integratedmodelling.api.monitoring.IMonitor;
//import org.integratedmodelling.common.states.State;
//import org.integratedmodelling.engine.geospace.gis.GISOperations;
//import org.integratedmodelling.engine.modelling.runtime.Subject;
//import org.integratedmodelling.exceptions.ThinklabException;
//
//import es.unex.sextante.core.OutputFactory;
//import es.unex.sextante.core.OutputObjectsSet;
//import es.unex.sextante.core.ParametersSet;
//import es.unex.sextante.dataObjects.IRasterLayer;
//import es.unex.sextante.geotools.GTOutputFactory;
//import es.unex.sextante.gridTools.rasterBuffer.RasterBufferAlgorithm;
//import es.unex.sextante.outputs.Output;
//
//public class RBufferAccessor extends BaseGISActuator {
//
//    /*
//     * TODO use Sextante's, but the values in it are identical and screwed.
//     */
//    public static final int FIXED_DISTANCE                    = 0;
//    public static final int USE_CELL_VALUE_AS_BUFFER_DISTANCE = 1;
//
//    double                  _distance;
//    int                     _method                           = FIXED_DISTANCE;
//    IObservable             _outputConcept;
//    IObserver               _outputObserver;
//
//    public RBufferAccessor(double distance, int method) {
//        _distance = distance;
//    }
//
//    @Override
//    public ISubject initialize(ISubject subject, ISubject context, IMonitor monitor)
//            throws ThinklabException {
//
//        RasterBufferAlgorithm alg = new RasterBufferAlgorithm();
//        ParametersSet parms = alg.getParameters();
//
//        /*
//         * there should be just one non-raw input 
//         */
//        IState input = null;
//        for (IState s : getInputStates()) {
//            if (!((State) s).isRaw()) {
//                input = s;
//                break;
//            }
//        }
//
//        if (input == null) {
//            monitor.error("cannot determine input state for raster buffering");
//            return null;
//        } else {
//            monitor.info("distance buffering executing on " + input.getObservable().getType(),
//                    GISOperations.INFO_CLASS);
//        }
//
//        /*
//         * TODO translate distance to the appropriate measurement for the
//         * projection. 
//         */
//        IRasterLayer inp = getRaster(input);
//
//        try {
//            parms.getParameter(RasterBufferAlgorithm.METHOD).setParameterValue(new Integer(_method));
//            parms.getParameter(RasterBufferAlgorithm.DIST).setParameterValue(new Double(_distance));
//            parms.getParameter(RasterBufferAlgorithm.INPUT).setParameterValue(inp);
//
//            OutputFactory outputFactory = new GTOutputFactory();
//            alg.execute(getTaskMonitor(), outputFactory);
//
//            OutputObjectsSet outputs = alg.getOutputObjects();
//            Output slope = outputs.getOutput(RasterBufferAlgorithm.BUFFER_LAYER);
//            publishRasterAsState((IRasterLayer) slope.getOutputObject(), _outputConcept, _outputObserver, (Subject) subject);
//
//        } catch (Exception e) {
//            monitor.error(e);
//        }
//        return subject;
//    }
//
//    @Override
//    public void notifyExpectedOutput(IObservable observable, IObserver observer, String name) {
//        _outputConcept = observable;
//        _outputObserver = observer;
//    }
//
//    @Override
//    public void notifyModel(IModel model) {
//        // TODO Auto-generated method stub
//
//    }
//
// }
