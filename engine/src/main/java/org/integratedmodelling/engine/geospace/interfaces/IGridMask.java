/*******************************************************************************
 *  Copyright (C) 2007, 2014:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.engine.geospace.interfaces;

import org.integratedmodelling.collections.Pair;
import org.integratedmodelling.engine.geospace.extents.Grid;
import org.integratedmodelling.exceptions.ThinklabValidationException;

public interface IGridMask {

    public abstract void intersect(IGridMask other) throws ThinklabValidationException;

    public abstract void or(IGridMask other) throws ThinklabValidationException;

    public abstract Pair<Integer, Integer> getCell(int index);

    public abstract boolean isActive(int linearIndex);

    public abstract boolean isActive(int x, int y);

    public abstract void activate(int x, int y);

    public abstract void deactivate(int x, int y);

    public abstract int totalActiveCells();

    public abstract int nextActiveOffset(int fromOffset);

    public abstract int[] nextActiveCell(int fromX, int fromY);

    public abstract Pair<Integer, Integer> nextActiveCell(int fromOffset);

    public abstract Grid getGrid();

    public abstract void invert();

    /**
     * Set every flag to false;
     */
    public abstract void deactivate();

    /**
     * Set every flag to true;
     */
    public abstract void activate();

}
