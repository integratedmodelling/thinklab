package org.integratedmodelling.engine.geospace.coverage.raster;

import org.integratedmodelling.collections.Pair;
import org.integratedmodelling.engine.geospace.extents.Grid;
import org.integratedmodelling.engine.geospace.interfaces.IGridMask;
import org.integratedmodelling.exceptions.ThinklabValidationException;

public class DummyActivationLayer implements IGridMask {

    boolean isActive = true;
    Grid    grid;

    public DummyActivationLayer(Grid grid) {
        this.grid = grid;
    }

    @Override
    public void intersect(IGridMask other) throws ThinklabValidationException {
    }

    @Override
    public void or(IGridMask other) throws ThinklabValidationException {
    }

    @Override
    public Pair<Integer, Integer> getCell(int index) {
        return null;
    }

    @Override
    public boolean isActive(int linearIndex) {
        return isActive;
    }

    @Override
    public boolean isActive(int x, int y) {
        return isActive;
    }

    @Override
    public void activate(int x, int y) {
        isActive = true;
    }

    @Override
    public void deactivate(int x, int y) {
        isActive = false;
    }

    @Override
    public int totalActiveCells() {
        return 0;
    }

    @Override
    public int nextActiveOffset(int fromOffset) {
        // TODO Auto-generated method stub
        return 0;
    }

    @Override
    public int[] nextActiveCell(int fromX, int fromY) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public Pair<Integer, Integer> nextActiveCell(int fromOffset) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public Grid getGrid() {
        // TODO Auto-generated method stub
        return grid;
    }

    @Override
    public void invert() {
        // TODO Auto-generated method stub

    }

    @Override
    public void deactivate() {
        // TODO Auto-generated method stub

    }

    @Override
    public void activate() {
        // TODO Auto-generated method stub

    }
}
