/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.engine.geospace.gis.functions;

import java.util.HashMap;
import java.util.Map;

import org.integratedmodelling.api.modelling.IDirectObservation;
import org.integratedmodelling.api.modelling.IModel;
import org.integratedmodelling.api.modelling.IObservable;
import org.integratedmodelling.api.modelling.IObservation;
import org.integratedmodelling.api.modelling.IProcess;
import org.integratedmodelling.api.modelling.IState;
import org.integratedmodelling.api.modelling.resolution.IResolutionContext;
import org.integratedmodelling.api.modelling.runtime.IProcessContextualizer;
import org.integratedmodelling.api.modelling.scheduling.ITransition;
import org.integratedmodelling.api.monitoring.IMonitor;
import org.integratedmodelling.api.project.IProject;
import org.integratedmodelling.api.services.annotations.Prototype;
import org.integratedmodelling.common.vocabulary.GeoNS;
import org.integratedmodelling.common.vocabulary.NS;
import org.integratedmodelling.engine.geospace.gis.SextanteOperations;
import org.integratedmodelling.exceptions.ThinklabException;
import org.integratedmodelling.exceptions.ThinklabValidationException;

import es.unex.sextante.core.OutputFactory;
import es.unex.sextante.core.OutputObjectsSet;
import es.unex.sextante.core.ParametersSet;
import es.unex.sextante.dataObjects.IRasterLayer;
import es.unex.sextante.geotools.GTOutputFactory;
import es.unex.sextante.hydrology.slopeLength.SlopeLengthAlgorithm;
import es.unex.sextante.outputs.Output;

@Prototype(
        id = "gis.slope-length",
        returnTypes = { NS.PROCESS_CONTEXTUALIZER })
public class SlopeLength implements IProcessContextualizer {

    IProject project;

    @Override
    public boolean canDispose() {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public void setContext(Map<String, Object> parameters, IModel model, IProject project) {
        this.project = project;
    }

    @Override
    public Map<String, IObservation> initialize(IProcess process, IDirectObservation context, IResolutionContext resolutionContext, Map<String, IObservable> expectedInputs, Map<String, IObservable> expectedOutputs, IMonitor monitor)
            throws ThinklabException {

        GeoNS.synchronize();

        Map<String, IObservation> ret = new HashMap<>();

        SlopeLengthAlgorithm alg = new SlopeLengthAlgorithm();
        ParametersSet parms = alg.getParameters();
        IState elevation = null;

        /*
         * TODO we should also check that the model does not expect anything but aspect and the core
         * process.
         */
        String oName = null;
        IObservable output = null;
        for (String n : expectedOutputs.keySet()) {
            if (expectedOutputs.get(n).is(GeoNS.SLOPE_LENGTH)) {
                oName = n;
                output = expectedOutputs.get(n);
            }
        }

        if (output == null) {
            throw new ThinklabValidationException("slope length GIS computation: model has no usable concept in outputs");
        }

        for (IState st : context.getStates()) {
            if (st.getObservable().is(GeoNS.ELEVATION)) {
                elevation = st;
            }
        }

        if (elevation == null) {
            throw new ThinklabValidationException("slope length GIS computation: DEM not found in observed dependencies");
        }

        /*
         * units based on the output observer 
         */
        IRasterLayer dem = SextanteOperations.getInputAsRaster(elevation);
        
        try {
            parms.getParameter(SlopeLengthAlgorithm.DEM).setParameterValue(dem);

            OutputFactory outputFactory = new GTOutputFactory();
            alg.execute(SextanteOperations.getTaskMonitor(monitor), outputFactory);

            OutputObjectsSet outputs = alg.getOutputObjects();
            Output computed = outputs.getOutput(SlopeLengthAlgorithm.SLOPELENGTH);
            IState aspect = null;

            /*
             * result is only dynamic if elevation is.
             */
            if (elevation.isTemporallyDistributed()) {
                aspect = SextanteOperations.getStateFromRaster(output, context, (IRasterLayer) computed
                        .getOutputObject());
            } else {
                aspect = SextanteOperations.getStaticStateFromRaster(output, context, (IRasterLayer) computed
                        .getOutputObject());
            }

            ret.put(oName, aspect);

        } catch (Exception e) {
            monitor.error(e);
        }

        return ret;
    }

    @Override
    public Map<String, IObservation> compute(ITransition transition, Map<String, IState> inputs)
            throws ThinklabException {
        // TODO replay if the DEM has changed.
        return null;
    }

}
