/*******************************************************************************
 *  Copyright (C) 2014, 2015:
 *  
 *    - Ioannis N. Athanasiadis <ioannis@athanasiadis.info>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *    You should have received a copy of the Affero General Public License
 *    along with this program; if not, write to the Free Software
 *    Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *    The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.engine.geospace.georss;

import java.util.Map;

import org.integratedmodelling.api.knowledge.IConcept;
import org.integratedmodelling.api.knowledge.IExpression;
import org.integratedmodelling.api.monitoring.IMonitor;
import org.integratedmodelling.api.services.annotations.Prototype;
import org.integratedmodelling.common.thinkql.CodeExpression;
import org.integratedmodelling.common.vocabulary.NS;
import org.integratedmodelling.exceptions.ThinklabException;

/**
 * 
 * @author Ioannis N. Athanasiadis <ioannis@athanasiadis.info>
 * @since 2014
 */
@Prototype(
		id = "georss",
		args = {
				"url", Prototype.TEXT,  // location of the georss
				"# node",Prototype.TEXT, // nodename(s) to parse for value(s) i.e. <category label="Magnitude" term="Magnitude 6"/>
				// if missing we are checking for presence only
				"# default-value", Prototype.TEXT, // if missing what the default value should be
				"# extract", Prototype.TEXT, // in case we need some RegEx to extract values
				"# limit", Prototype.INT, // Limit of number of entries to read
		},
		returnTypes = { NS.DATASOURCE, NS.OBJECTSOURCE })

public class GeoRSS extends CodeExpression implements IExpression {


	@Override
	public Object eval(Map<String, Object> parameters, IMonitor monitor, IConcept... context)
			throws ThinklabException {

		String url = parameters.containsKey("url") ? parameters.get("url").toString()  : "";
		String node = parameters.containsKey("node") ? parameters.get("node").toString()  : "";
		String defaultvalue = parameters.containsKey("default-value") ? parameters.get("default-value").toString() : "";
		String extract = parameters.containsKey("extract") ? parameters.get("extract").toString() : "";
		String limit = parameters.containsKey("limit") ? parameters.get("limit").toString() : "";

		if(url!="")
			return new GeoRSSDataSource(url, node, defaultvalue, extract, limit);
		return null;
	}



}


