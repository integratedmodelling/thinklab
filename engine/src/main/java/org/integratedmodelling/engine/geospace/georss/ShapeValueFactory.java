/*******************************************************************************
 *  Copyright (C) 2014, 2015:
 *  
 *    - Ioannis N. Athanasiadis <ioannis@athanasiadis.info>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *    You should have received a copy of the Affero General Public License
 *    along with this program; if not, write to the Free Software
 *    Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *    The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.engine.geospace.georss;

import org.integratedmodelling.engine.geospace.literals.ShapeValue;
import org.opengis.referencing.crs.CoordinateReferenceSystem;

import com.rometools.modules.georss.geometries.AbstractGeometry;
import com.rometools.modules.georss.geometries.Envelope;
import com.rometools.modules.georss.geometries.LineString;
import com.rometools.modules.georss.geometries.LinearRing;
import com.rometools.modules.georss.geometries.Point;
import com.rometools.modules.georss.geometries.Polygon;
import com.rometools.modules.georss.geometries.PositionList;
import com.vividsolutions.jts.geom.Coordinate;
import com.vividsolutions.jts.geom.GeometryFactory;

/**
 * This helper class provides with a collection of methods to match 
 * Rome geometries to geotools/jts geometries
 * 
 * 
 * @author Ioannis N. Athanasiadis <ioannis@athanasiadis.info>
 * @since 2014
 */
public class ShapeValueFactory {
	static GeometryFactory gf = new GeometryFactory();


	public static ShapeValue create(AbstractGeometry geometry, CoordinateReferenceSystem crs) {
		if(geometry instanceof Point){
	    	return new ShapeValue(transform((Point) geometry), crs);
		} else if (geometry instanceof LineString){
			return new ShapeValue(transform((LineString) geometry), crs);
		} else if (geometry instanceof Polygon){
			return new ShapeValue(transform((Polygon) geometry), crs);
		} else if (geometry instanceof Envelope){
			return new ShapeValue(transform((Envelope) geometry), crs);

		}
		
		return null;
	}
	
	
	private static com.vividsolutions.jts.geom.Polygon transform(Envelope env) {
		Coordinate[] coordinates = {
                new Coordinate(env.getMinLongitude(), env.getMinLatitude()),
                new Coordinate(env.getMaxLongitude(), env.getMinLatitude()),
                new Coordinate(env.getMaxLongitude(), env.getMaxLatitude()),
                new Coordinate(env.getMinLongitude(), env.getMaxLatitude()),
                new Coordinate(env.getMinLongitude(), env.getMinLatitude()),
		};

		return gf.createPolygon(coordinates);
	}


	static com.vividsolutions.jts.geom.Point transform(com.rometools.modules.georss.geometries.Point p){
		return gf.createPoint(new Coordinate( p.getPosition().getLongitude(), p.getPosition().getLatitude()));
	}

	private static Coordinate[] getCoordinates(PositionList list){
		Coordinate[] seq = new Coordinate[list.size()];
		for(int i=0;i<list.size();i++){
			seq[i] = new Coordinate(list.getLongitude(i),list.getLatitude(i));				
		}
		return seq;
	}

	public static com.vividsolutions.jts.geom.LineString transform(com.rometools.modules.georss.geometries.LineString line){
		PositionList list = line.getPositionList();
		return  gf.createLineString(getCoordinates(list));
	}
	

	public static com.vividsolutions.jts.geom.LinearRing transform(com.rometools.modules.georss.geometries.LinearRing ring){
		PositionList list = ring.getPositionList();	
		return  gf.createLinearRing(getCoordinates(list));		
	}

	public static com.vividsolutions.jts.geom.LinearRing[] transform(com.rometools.modules.georss.geometries.LinearRing[] ring){
		com.vividsolutions.jts.geom.LinearRing[] c = new  com.vividsolutions.jts.geom.LinearRing[ring.length];
		for(int i = 0 ; i< ring.length; i++){
			c[i] = transform(ring[i]);
		}
		return c;
	}
	
	public static com.vividsolutions.jts.geom.Polygon transform(com.rometools.modules.georss.geometries.Polygon polygon){
		com.vividsolutions.jts.geom.LinearRing shell = transform((LinearRing) polygon.getExterior());
		com.vividsolutions.jts.geom.LinearRing[] holes = transform((LinearRing[]) polygon.getInterior().toArray());
		return gf.createPolygon(shell, holes);
	}
}

