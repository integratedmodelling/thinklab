/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.engine.geospace.gis;

import java.awt.image.RenderedImage;

import javax.media.jai.iterator.RandomIter;
import javax.media.jai.iterator.RandomIterFactory;

import org.geotools.coverage.grid.GridCoverage2D;
import org.integratedmodelling.api.modelling.IDirectObservation;
import org.integratedmodelling.api.modelling.IObservable;
import org.integratedmodelling.api.modelling.IScale;
import org.integratedmodelling.api.modelling.IState;
import org.integratedmodelling.api.modelling.ISubject;
import org.integratedmodelling.api.monitoring.IMonitor;
import org.integratedmodelling.common.states.State;
import org.integratedmodelling.common.states.States;
import org.integratedmodelling.common.vocabulary.NS;
import org.integratedmodelling.engine.geospace.coverage.raster.RasterCoverage;
import org.integratedmodelling.engine.geospace.extents.Grid;
import org.integratedmodelling.engine.geospace.extents.SpaceExtent;
import org.integratedmodelling.exceptions.ThinklabException;
import org.integratedmodelling.exceptions.ThinklabValidationException;
import org.jgrasstools.gears.libs.monitor.IJGTProgressMonitor;

import es.unex.sextante.core.ITaskMonitor;
import es.unex.sextante.dataObjects.IRasterLayer;
import es.unex.sextante.geotools.GTRasterLayer;

public class GISOperations {

    public final static String  INFO_CLASS                 = "GIS_PROCESS";
    private static final double BASIN_IMPORTANCE_THRESHOLD = 0.05;
    private static final int    WATERSHEDS_TO_EXTRACT      = 1;

    public static class TaskMonitor implements ITaskMonitor, IJGTProgressMonitor {

        IMonitor       _monitor;
        private String _name;

        public TaskMonitor(IMonitor monitor) {
            _monitor = monitor;
        }

        @Override
        public void setProgress(int iStep) {
            _monitor.addProgress(iStep, "GIS processing");
        }

        @Override
        public void setProgress(int step, int totalNumberOfSteps) {
            _monitor.defineProgressSteps(totalNumberOfSteps);
            _monitor.addProgress(step, "GIS processing");
        }

        @Override
        public void setProgressText(String sText) {
            // if (sText != null && !sText.trim().isEmpty())
            // _monitor.info(sText, INFO_CLASS);
        }

        @Override
        public boolean isCanceled() {
            return _monitor.isStopped();
        }

        @Override
        public void close() {
        }

        @Override
        public void setDeterminate(boolean bDeterminate) {
        }

        @Override
        public void setProcessDescription(String sDescription) {
        }

        @Override
        public void setDescriptionPrefix(String sPrefix) {
        }

        // JGT methods
        @Override
        public void beginTask(String name, int totalWork) {
            _name = name;
            _monitor.defineProgressSteps(totalWork);
            // _monitor.info("beginning " + name, INFO_CLASS);
        }

        @Override
        public void message(String message) {
            // _monitor.info(message, INFO_CLASS);
        }

        @Override
        public void errorMessage(String message) {
            _monitor.error(message);
        }

        @Override
        public void done() {
            // _monitor.info("finished " + _name, INFO_CLASS);
        }

        @Override
        public void internalWorked(double work) {
            // TODO Auto-generated method stub

        }

        @Override
        public void setCanceled(boolean value) {
            if (value) {
                _monitor.stop(null);
            }
        }

        @Override
        public void setTaskName(String name) {
            _name = name;
        }

        @Override
        public void subTask(String name) {
        }

        @Override
        public void worked(int work) {
            _monitor.addProgress(work, "");
        }

        @Override
        public <T> T adapt(Class<T> adaptee) {
            // TODO Auto-generated method stub
            return null;
        }

        @Override
        public void exceptionThrown(String message) {
            _monitor.error(message);
        }

        @Override
        public void onModuleExit() {
            // TODO Auto-generated method stub

        }
    }

    public static IRasterLayer stateToRaster(IState state) throws ThinklabException {

        GTRasterLayer ret = null;
        RasterCoverage coverage = new RasterCoverage(state);
        ret = new GTRasterLayer();
        ret.create(coverage.getCoverage());
        return ret;
    }

    /**
     * Turn a Thinklab state into a Geotools GridCoverage2D.
     * 
     * @param state
     * @return
     * @throws ThinklabException
     */
    public static GridCoverage2D stateToCoverage(IState state) throws ThinklabException {
        RasterCoverage coverage = new RasterCoverage(state);
        return coverage.getCoverage();
    }

    /**
     * Turn a Thinklab state into a Geotools GridCoverage2D.
     * 
     * @param state
     * @return
     * @throws ThinklabException
     */
    public static GridCoverage2D stateToCoverage(IState state, Iterable<IScale.Locator> locators)
            throws ThinklabException {
        RasterCoverage coverage = new RasterCoverage(state, locators);
        return coverage.getCoverage();
    }

    public static IState rasterToState(IRasterLayer layer, IObservable observable, ISubject context)
            throws ThinklabException {

        GTRasterLayer l = (GTRasterLayer) layer;
        SpaceExtent ext = (SpaceExtent) context.getScale().getSpace();

        if (ext.getGrid() == null) {
            throw new ThinklabValidationException("cannot return a raster layer from a non-grid extent");
        }

        Grid grid = ext.getGrid();
        double[] data = new double[grid.getCellCount()];

        for (int i = 0; i < grid.getCellCount(); i++) {
            int[] xy = grid.getXYOffsets(i);
            data[i] = l.getCellValueAsDouble(xy[0], xy[1]);
        }

        // FIXME see what to do with dynamic state
        return new State(data, observable, context.getScale(), false);
    }

    /**
     * Get a static state built from the passed layer.
     * 
     * @param layer
     * @param observable
     * @param context
     * @param label
     * @return
     * @throws ThinklabException
     */
    public static IState coverageToState(GridCoverage2D layer, IObservable observable, IDirectObservation context,
            String label) throws ThinklabException {

        SpaceExtent ext = (SpaceExtent) context.getScale().getSpace();
        if (ext.getGrid() == null) {
            throw new ThinklabValidationException("cannot return a raster layer from a non-grid extent");
        }

        Grid grid = ext.getGrid();
        double[] data = new double[grid.getCellCount()];
        RenderedImage image = layer.getRenderedImage();
        RandomIter itera = RandomIterFactory.create(image, null);

        IState ret = context.getStaticState(observable);

        for (int i = 0; i < grid.getCellCount(); i++) {
            int[] xy = grid.getXYOffsets(i);
            States.set(ret, itera.getSampleDouble(xy[0], xy[1], 0), i);
        }

        ret.getMetadata().put(NS.DISPLAY_LABEL_PROPERTY, label);

        return ret;
    }
}
