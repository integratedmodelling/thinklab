/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
//package org.integratedmodelling.engine.geospace.gis.operators;
//
//import org.integratedmodelling.api.modelling.IModel;
//import org.integratedmodelling.api.modelling.IObservable;
//import org.integratedmodelling.api.modelling.IObserver;
//import org.integratedmodelling.api.modelling.IState;
//import org.integratedmodelling.api.modelling.ISubject;
//import org.integratedmodelling.api.monitoring.IMonitor;
//import org.integratedmodelling.exceptions.ThinklabException;
//
//import es.unex.sextante.core.OutputFactory;
//import es.unex.sextante.core.ParametersSet;
//import es.unex.sextante.dataObjects.IRasterLayer;
//import es.unex.sextante.geotools.GTOutputFactory;
//import es.unex.sextante.gridAnalysis.accCost.AccCostAlgorithm;
//
//public class AccumulatedCostAccessor extends BaseGISActuator {
//
//    public final static int EUCLIDEAN  = AccCostAlgorithm.EUCLIDEAN;
//    public final static int CHESSBOARD = AccCostAlgorithm.CHESSBOARD;
//    public final static int MANHATTAN  = AccCostAlgorithm.MANHATTAN;
//    public final static int CHAMFER    = AccCostAlgorithm.CHAMFER;
//    public final static int WINDOW5X5  = AccCostAlgorithm.WINDOW5X5;
//
//    IObservable             _outputConcept;
//    IObserver               _outputObserver;
//
//    public AccumulatedCostAccessor(int distanceType) {
//    }
//
//    @Override
//    public ISubject initialize(ISubject subject, ISubject context, IMonitor monitor)
//            throws ThinklabException {
//
//        AccCostAlgorithm alg = new AccCostAlgorithm();
//        ParametersSet parms = alg.getParameters();
//
//        // if (getInput(Thinklab.c(Geospace.NS.ELEVATION)) == null) {
//        // _monitor.error(new
//        // ThinklabValidationException("slope length: DEM not found in observed dependencies"));
//        // }
//
//        IState input = getInputStates().iterator().hasNext() ? getInputStates().iterator().next() : null;
//        if (input != null) {
//            monitor.error("cannot find input layer");
//            return subject;
//        }
//        IRasterLayer inp = getRaster(input);
//
//        try {
//            // parms.getParameter(AccCostAlgorithm.USETHRESHOLD).
//            // setParameterValue(new Boolean(_useThreshold));
//            // parms.getParameter(AccCostAlgorithm.THRESHOLD).
//            // setParameterValue(new Double(_threshold));
//            // parms.getParameter(AccCostAlgorithm.DEM).
//            // setParameterValue(dem);
//
//            OutputFactory outputFactory = new GTOutputFactory();
//            alg.execute(getTaskMonitor(), outputFactory);
//
//            // OutputObjectsSet outputs = alg.getOutputObjects();
//            // Output slope = outputs.getOutput(SlopeLengthAlgorithm.SLOPELENGTH);
//            // publishRasterAsState((IRasterLayer) slope.getOutputObject(),
//            // _outputConcept, _outputObserver);
//
//        } catch (Exception e) {
//            monitor.error(e);
//        }
//
//        return subject;
//    }
//
//    @Override
//    public void notifyExpectedOutput(IObservable observable, IObserver observer, String name) {
//        _outputConcept = observable;
//        _outputObserver = observer;
//    }
//
//    @Override
//    public void notifyModel(IModel model) {
//        // TODO Auto-generated method stub
//
//    }
//
// }
