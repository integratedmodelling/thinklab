/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.engine.geospace.extents;

import java.util.BitSet;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.geotools.geometry.jts.ReferencedEnvelope;
import org.integratedmodelling.api.knowledge.IConcept;
import org.integratedmodelling.api.knowledge.IKnowledge;
import org.integratedmodelling.api.knowledge.IProperty;
import org.integratedmodelling.api.metadata.IMetadata;
import org.integratedmodelling.api.modelling.IExtent;
import org.integratedmodelling.api.modelling.IObservable;
import org.integratedmodelling.api.modelling.IObserver;
import org.integratedmodelling.api.modelling.IScale;
import org.integratedmodelling.api.modelling.IScale.Index;
import org.integratedmodelling.api.modelling.IScale.Locator;
import org.integratedmodelling.api.modelling.ITopologicallyComparable;
import org.integratedmodelling.api.modelling.storage.IStorage;
import org.integratedmodelling.api.space.IShape;
import org.integratedmodelling.api.space.ISpatialExtent;
import org.integratedmodelling.api.space.ISpatialIndex;
import org.integratedmodelling.api.space.ITessellation;
import org.integratedmodelling.api.time.ITemporalExtent;
import org.integratedmodelling.collections.Pair;
import org.integratedmodelling.collections.Path;
import org.integratedmodelling.common.configuration.KLAB;
import org.integratedmodelling.common.model.runtime.Space;
import org.integratedmodelling.common.space.IGeometricShape;
import org.integratedmodelling.common.space.SpaceLocator;
import org.integratedmodelling.common.vocabulary.NS;
import org.integratedmodelling.engine.KLABEngine;
import org.integratedmodelling.engine.geospace.Geospace;
import org.integratedmodelling.engine.geospace.coverage.raster.RasterActivationLayer;
import org.integratedmodelling.engine.geospace.coverage.vector.VectorCoverage;
import org.integratedmodelling.engine.geospace.literals.ShapeValue;
import org.integratedmodelling.engine.modelling.runtime.mediators.GridToShape;
import org.integratedmodelling.engine.modelling.runtime.mediators.ShapeToGrid;
import org.integratedmodelling.exceptions.ThinklabException;
import org.integratedmodelling.exceptions.ThinklabInternalErrorException;
import org.integratedmodelling.exceptions.ThinklabRuntimeException;
import org.integratedmodelling.exceptions.ThinklabUnsupportedOperationException;
import org.integratedmodelling.exceptions.ThinklabValidationException;
import org.integratedmodelling.lang.IRemoteSerializable;
import org.opengis.referencing.crs.CoordinateReferenceSystem;

import com.vividsolutions.jts.geom.Geometry;

/**
 * Slated to substitute all other spatial extents - defines itself to the optimal
 * representation by merging other partially specified ones.
 * 
 * @author Ferd
 *
 */
public class SpaceExtent implements ISpatialExtent, Iterable<ShapeValue>,
        IRemoteSerializable, IGeometricShape {

    ShapeValue                _shape          = null;
    Grid                      _grid           = null;
    ReferencedEnvelope        _envelope       = null;
    CoordinateReferenceSystem _crs            = null;
    VectorCoverage            _features       = null;
    double                    _gridResolution = -1.0;
    ISpatialIndex             _index          = null;
    int                       _extentType     = INCONSISTENT;

    // the four states this can be in
    /**
     * Extent has only been set with info that require a merge before
     * we can establish what the usable extent will be.
     */
    public static final int   INCONSISTENT    = -1;

    /**
     * extent represents a shape location (may be single, multiple or a point) of
     * multiplicity 1.
     */
    public static final int   SINGLE_SHAPE    = 0;

    /**
     * Extent represents more than one shape, with one state per shape.
     */
    public static final int   MULTIPLE_SHAPE  = 1;

    /**
     * Extent represents a grid, possibly with a shape that doesn't cover it
     * entirely.
     */
    public static final int   GRID            = 2;

    class Tessellation implements ITessellation {

        @Override
        public Iterator<IShape> iterator() {
            // TODO return a shape iterator built upon the _features field.
            return null;
        }

    }

    /*
     * cloning constructor
     */
    private SpaceExtent(SpaceExtent extent) {
        _shape = extent._shape;
        _grid = extent._grid;
        _envelope = extent._envelope;
        _crs = extent._crs;
        _features = extent._features;
        _extentType = extent._extentType;
    }

    // @Override
    public BitSet getMask() {
        return _grid == null ? null : ((RasterActivationLayer) (_grid.activationLayer));
    }

    @Override
    public IKnowledge getType() {
        return getObservable().getType();
    }

    public SpaceExtent(ShapeValue shape) {
        _shape = shape;
        _crs = shape.getCRS();
        _envelope = shape.getEnvelope();
        _extentType = SINGLE_SHAPE;
    }

    public SpaceExtent(Grid grid) {
        _grid = grid;
        _shape = grid.getShape();
        _envelope = grid.getEnvelope();
        _extentType = GRID;
        _crs = grid.crs;
    }

    public SpaceExtent(ShapeValue shape, int x, int y) throws ThinklabException {
        // TODO no activation layer
        _grid = new Grid(shape, x, y, true);
        _shape = shape;
        _envelope = shape.getEnvelope();
        _extentType = GRID;
    }

    public SpaceExtent(VectorCoverage vector) {
        _features = vector;
        _extentType = MULTIPLE_SHAPE;
    }

    public SpaceExtent() {
        _extentType = INCONSISTENT;
    }

    public SpaceExtent(double resolution) {
        _gridResolution = resolution;
        _extentType = INCONSISTENT;
        // TODO review - this is a forcing.
    }

    @Override
    public IExtent merge(IExtent extent, boolean force) throws ThinklabException {

        if (extent instanceof Space && force) {
            SpaceExtent ret = new SpaceExtent(this);
            if (!((Space) extent).isForcing()) {
                throw new ThinklabUnsupportedOperationException("cannot merge non-forcing foreign spatial extent in SpatialExtent");
            }
            ret._grid = new Grid(getShape(), Grid.parseResolution(((Space) extent).getForcingDefinition()));
            return ret;
        }

        if (!(extent instanceof SpaceExtent)) {
            throw new ThinklabValidationException("space extent cannot merge non-space extent");
        }

        SpaceExtent ret = new SpaceExtent(this);
        SpaceExtent oth = (SpaceExtent) extent;

        /*
         * TODO figure out mandatory vs. not. These are all false, which probably
         * shouldn't be - either pass to merge or be smarter.
         */
        if (oth._grid != null) {
            ret.set(oth._grid, force);
        } else if (oth._features != null) {
            ret.set(oth._features, oth._shape, force);
        } else if (oth._shape != null) {
            ret.set(oth._shape, force);
        } else if (oth._gridResolution > 0.0) {
            ret.setGridResolution(oth._gridResolution, force);
        }

        return ret;
    }

    /*
     * constructor & merger w/ multiple shapes + optional boundary. If mandatory,
     * we HAVE to have these, and incompatible previous specs are an error.
     */
    public void set(VectorCoverage vector, ShapeValue shape, boolean mandatory)
            throws ThinklabException {

        if (_extentType == INCONSISTENT) {

            if (_gridResolution > 0.0) {

                /*
                 * make grid; subset to union of shapes
                 */

            } else {
                _features = vector;
                _extentType = MULTIPLE_SHAPE;
            }
        } else if (_extentType == GRID) {

            /*
             * TODO
             * error if mandatory, else activate the grid
             */
        }

        if (shape != null) {

            /*
             * intersect existing shape if any
             */
            if (_shape != null) {
                _shape = _shape.intersection(shape);
            } else {
                _shape = shape;
            }

        }
    }

    /*
     * constructor & merger w/ grid + optional boundary. If mandatory,
     * we HAVE to have these, and incompatible previous specs are an error.
     */
    public void set(Grid grid, boolean mandatory) throws ThinklabException {

        if (_extentType == INCONSISTENT) {
            _grid = grid;
        } else if (!mandatory) {

            throw new ThinklabValidationException("conflicting spatial extent merge: grid -> "
                    + getTypeLabel());

        } else {

            /*
             * TODO - we want to force a grid.
             */
        }
    }

    private String getTypeLabel() {

        switch (_extentType) {
        case INCONSISTENT:
            return "inconsistent";
        case GRID:
            return "grid";
        case SINGLE_SHAPE:
            return "single shape";
        case MULTIPLE_SHAPE:
            return "multiple shape";
        }
        return null;
    }

    /*
     * constructor & merger w/ single shape. If mandatory,
     * we HAVE to have these, and incompatible previous specs are an error.
     */
    public void set(ShapeValue shape, boolean mandatory) throws ThinklabException {

        /*
         * TODO lots to do here - particularly, if we force a new shape on a grid, must recompute
         * multiplicity etc. The new shape may be larger if this is called by union() (which does
         * not happen at the moment, but intersection() can).
         */

        if (_extentType == INCONSISTENT) {

            _shape = shape;
            if (_gridResolution > 0.0) {
                _grid = new Grid(shape, _gridResolution);
                _extentType = GRID;
            }

        } else if (_extentType == GRID) {

            /*
             * mandatory? subset the grid : complain
             */
            if (mandatory) {
                _grid.createActivationLayer(shape);
            } else {
                throw new ThinklabValidationException("conflicting spatial extent merge: shape -> grid");
            }

        } else if (_extentType == SINGLE_SHAPE) {

            /*
             * if mandatory intersect shapes; if not, complain
             */
            if (mandatory) {
                _shape = _shape.intersection(shape);
            } else {
                throw new ThinklabValidationException("conflicting spatial extent merge: shape -> shape");
            }

        } else if (_extentType == MULTIPLE_SHAPE) {

            /*
             * filter the shapes that intersect the given one and set them to
             * the intersection.
             */
            _features = intersectFeatures(_features, shape);
        }
    }

    private VectorCoverage intersectFeatures(VectorCoverage _features2, ShapeValue shape) {
        throw new ThinklabRuntimeException("unsupported shape intersection");
    }

    /*
     * Set the grid resolution and become a grid if we have a boundary. If mandatory,
     * we HAVE to have this, and incompatible previous specs are an error.
     */
    public void setGridResolution(double res, boolean mandatory) throws ThinklabException {

        _gridResolution = res;

        /*
         * adopt, adapt and improve
         */
        if (_extentType == GRID) {

            /*
             * resample if mandatory, complain otherwise
             */
        } else if (_extentType == SINGLE_SHAPE) {

            /*
             * make grid
             */
            _grid = new Grid(_shape, _gridResolution);
            _extentType = GRID;

        } else if (_extentType == MULTIPLE_SHAPE) {

            /*
             * make grid with union
             */
        }

    }

    @Override
    public boolean isConsistent() {
        return _extentType != INCONSISTENT && !getShape().isEmpty() && getShape().isValid();
    }

    @Override
    public Type getGeometryType() {
        return getShape().getGeometryType();
    }

    @Override
    public long getValueCount() {
        return getMultiplicity();
    }

    @Override
    public boolean isSpatiallyDistributed() {
        return getValueCount() > 1;
    }

    @Override
    public boolean isTemporallyDistributed() {
        return false;
    }

    @Override
    public IObserver getObserver() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public long getMultiplicity() {

        if (_grid != null) {
            return _grid.getCellCount();
        }

        // TODO the rest

        return 1;
    }

    @Override
    public IExtent intersection(IExtent other) throws ThinklabException {

        if (!(other instanceof SpaceExtent)) {
            throw new ThinklabValidationException("non-spatial extent being intersected with a spatial one");
        }

        SpaceExtent oth = (SpaceExtent) other;
        ShapeValue mys = getShape();
        ShapeValue its = oth.getShape();

        if (mys == null && its == null) {
            return new SpaceExtent();
        } else if (mys == null && its != null) {
            return new SpaceExtent(its);
        } else if (mys != null && its == null) {
            return new SpaceExtent(mys);
        }

        return new SpaceExtent(mys.intersection(its));
    }

    @Override
    public IExtent union(IExtent other) throws ThinklabException {

        if (!(other instanceof SpaceExtent)) {
            throw new ThinklabValidationException("non-spatial extent being intersected with a spatial one");
        }

        SpaceExtent oth = (SpaceExtent) other;
        ShapeValue mys = getShape();
        ShapeValue its = oth.getShape();

        if (mys == null && its == null) {
            return new SpaceExtent();
        } else if (mys == null && its != null) {
            return new SpaceExtent(its);
        } else if (mys != null && its == null) {
            return new SpaceExtent(mys);
        }

        return new SpaceExtent(mys.union(its));
    }

    @Override
    public boolean contains(IExtent o) throws ThinklabException {
        SpaceExtent e = (SpaceExtent) o;
        return _shape.contains(e._shape);
    }

    @Override
    public boolean overlaps(IExtent o) throws ThinklabException {
        SpaceExtent e = (SpaceExtent) o;
        return _shape.overlaps(e._shape);
    }

    @Override
    public boolean intersects(IExtent o) throws ThinklabException {
        SpaceExtent e = (SpaceExtent) o;
        return _shape.intersects(e._shape);
    }

    @Override
    public IConcept getDomainConcept() {
        return Geospace.get().SpatialDomain();
    }

    @Override
    public IProperty getDomainProperty() {
        return KLABEngine.p(NS.SPATIAL_EXTENT_PROPERTY);
    }

    @Override
    public IExtent collapse() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public ISpatialExtent getExtent(int stateIndex) {

        if (_grid != null) {
            return new SpaceExtent(_grid.getCellPolygon(stateIndex));
        }

        // TODO return n-th shape if vector coverage != null

        // TODO return self if index = 0 && it's a single shape.

        return null;
    }

    @Override
    public boolean isCovered(int stateIndex) {
        return _grid == null || _grid.isCovered(stateIndex);
    }

    @Override
    public Object getValue(int index) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public Iterator<ShapeValue> iterator() {
        // TODO Auto-generated method stub
        return null;
    }

    public boolean isGrid() {
        return _grid != null;
    }

    @Override
    public Grid getGrid() {
        return _grid;
    }

    @Override
    public ShapeValue getShape() {
        return _shape;
    }

    public ReferencedEnvelope getEnvelope() {
        return _envelope;
    }

    @Override
    public boolean isTemporal() {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public boolean isSpatial() {
        // TODO Auto-generated method stub
        return true;
    }

    @Override
    public ISpatialExtent getSpace() {
        return this;
    }

    @Override
    public ITemporalExtent getTime() {
        return null;
    }

    @Override
    public IProperty getCoverageProperty() {
        return KLABEngine.p(NS.GEOSPACE_HAS_SHAPE);
    }

    @Override
    public Pair<ITopologicallyComparable<?>, Double> checkCoverage(ITopologicallyComparable<?> obj)
            throws ThinklabException {

        if (_shape == null) {
            throw new ThinklabInternalErrorException("checking coverage on an indefinite spatial extent");
        }

        ShapeValue shape = null;
        if (obj instanceof SpaceExtent) {
            shape = ((SpaceExtent) obj).getShape();
        } else if (obj instanceof ShapeValue) {
            shape = (ShapeValue) obj;
        }

        if (shape == null) {
            throw new ThinklabInternalErrorException("checking spatial coverage of a non-spatial object");
        }

        ShapeValue common = _shape.intersection(shape);
        double prop = common.getGeometry().getArea() / _shape.getGeometry().getArea();

        return new Pair<ITopologicallyComparable<?>, Double>(common, prop);
    }

    @Override
    public ITopologicallyComparable<IExtent> union(ITopologicallyComparable<?> obj) throws ThinklabException {

        SpaceExtent ret = new SpaceExtent(this);

        if (_shape == null) {
            return ret;
        }

        ShapeValue shape = null;
        if (obj instanceof SpaceExtent) {
            shape = ((SpaceExtent) obj).getShape();
        } else if (obj instanceof ShapeValue) {
            shape = (ShapeValue) obj;
        }

        if (shape == null) {
            return new SpaceExtent();
        }

        ShapeValue common = _shape.union(shape);

        ret.set(common, true);

        return ret;
    }

    @Override
    public ITopologicallyComparable<IExtent> intersection(ITopologicallyComparable<?> obj)
            throws ThinklabException {

        SpaceExtent ret = new SpaceExtent(this);

        if (_shape == null) {
            return ret;
        }

        ShapeValue shape = null;
        if (obj instanceof SpaceExtent) {
            shape = ((SpaceExtent) obj).getShape();
        } else if (obj instanceof ShapeValue) {
            shape = (ShapeValue) obj;
        }

        if (shape == null) {
            return new SpaceExtent();
        }

        ShapeValue common = _shape.intersection(shape);

        return new SpaceExtent(common);
    }

    @Override
    public double getCoveredExtent() {
        return _shape == null ? 0 : _shape.getArea();
    }

    // @Override
    // public Class<?> getDataClass() {
    // return SpaceExtent.class;
    // }

    @Override
    public String toString() {
        return "S[" + _envelope + "]";
    }

    @Override
    public boolean isEmpty() {
        return _shape == null || _shape.getGeometry().isEmpty();
    }

    @Override
    public double getMinX() {
        return _envelope.getMinX();
    }

    @Override
    public double getMinY() {
        return _envelope.getMinY();
    }

    @Override
    public double getMaxX() {
        return _envelope.getMaxX();
    }

    @Override
    public double getMaxY() {
        return _envelope.getMaxY();
    }

    @Override
    public IScale getScale() {

        // TODO won't be called, but it should return the scale we're part of
        return null;
    }

    @Override
    public Object adapt() {

        Map<String, Object> ret = new HashMap<String, Object>();

        ret.put("multiplicity", getValueCount());
        ret.put("domain", getDomainConcept().toString());

        ret.put("minx", getEnvelope().getMinX());
        ret.put("miny", getEnvelope().getMinY());
        ret.put("maxx", getEnvelope().getMaxX());
        ret.put("maxy", getEnvelope().getMaxY());

        if (_grid != null) {
            ret.put("grid", _grid.adapt());
        }
        ShapeValue shape = getShape();
        if (shape != null) {
            ret.put("shape", shape.toString());
        }
        ret.put("crs", Geospace.getCRSIdentifier(_crs, true));
        return ret;
    }

    @Override
    public int[] getDimensionSizes() {

        if (_features != null) {
            // TODO number of features return new int[] {)
        } else if (_grid != null) {
            return new int[] { _grid.getXCells(), _grid.getYCells() };
        }
        return new int[] { 1 };
    }

    @Override
    public int[] getDimensionOffsets(int linearOffset, boolean rowFirst) {

        if (_features != null) {
            return new int[] { linearOffset };
        } else if (_grid != null) {
            if (rowFirst) {
                int[] xy = _grid.getXYOffsets(linearOffset);
                return new int[] { xy[1], xy[0] };
            }
            return _grid.getXYOffsets(linearOffset);
        }
        return new int[] { 0 };
    }

    @Override
    public ISpatialExtent getExtent() {
        return _shape == null ? null : _shape.asExtent();
    }

    @Override
    public String getCRSCode() {
        return Geospace.getCRSIdentifier(_crs, false);
    }

    @Override
    public ITessellation getTessellation() {
        return new Tessellation();
    }

    @Override
    public IStorage<?> getStorage() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public Iterator<?> iterator(Index index) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public IMetadata getMetadata() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public IObservable getObservable() {
        // TODO Auto-generated method stub
        return null;
    }

    //
    // @Override
    // public IConcept getDirectType() {
    // // TODO Auto-generated method stub
    // return null;
    // }
    //
    // @Override
    // public INamespace getNamespace() {
    // // TODO Auto-generated method stub
    // return null;
    // }
    //
    // @Override
    // public boolean is(Object other) {
    // // TODO Auto-generated method stub
    // return false;
    // }

    @Override
    public int locate(Locator locator) {
        if (locator instanceof SpaceLocator) {
            if (locator.isAll())
                return GENERIC_LOCATOR;
            if (_grid != null) {
                if (((SpaceLocator) locator).isLatLon()) {
                    return getGrid()
                            .getOffsetFromWorldCoordinates(((SpaceLocator) locator).lon, ((SpaceLocator) locator).lat);
                } else {
                    return getGrid().getOffset(((SpaceLocator) locator).x, ((SpaceLocator) locator).y);
                }
            }
        }
        return INAPPROPRIATE_LOCATOR;
    }

    @Override
    public Geometry getStandardizedGeometry() {
        return _shape.getStandardizedGeometry();
    }

    @Override
    public Geometry getGeometry() {
        return _shape.getGeometry();
    }

    public static IExtent sanitize(ISpatialExtent e) throws ThinklabException {
        if (e instanceof SpaceExtent) {
            return e;
        }
        SpaceExtent ret = null;
        String crsId = e.getCRSCode();
        ShapeValue shape = new ShapeValue(((IGeometricShape) e).getGeometry(), Geospace.getCRSFromID(crsId));
        if (e.getGrid() != null) {
            Grid grid = new Grid(shape, e.getGrid().getXCells(), e.getGrid().getYCells());
            ret = new SpaceExtent(grid);
        } else {
            ret = new SpaceExtent(shape);
        }
        return ret;
    }

    // public void parse(String string) {
    // // TODO Auto-generated method stub
    //
    // }

    @Override
    public String asText() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public int getSRID() {
        return Integer.parseInt(Path.getLast(Geospace.getCRSIdentifier(_crs, true), ':'));
    }

    @Override
    public Mediator getMediator(IExtent extent, IObservable observable, IConcept trait) {
        if (getGrid() != null && ((ISpatialExtent) extent).getGrid() == null
                && ((ISpatialExtent) extent).getMultiplicity() == 1) {
            return new GridToShape(observable, this, ShapeValue
                    .sanitize(((ISpatialExtent) extent).getShape()).asExtent(), trait);
        }
        if (getGrid() == null && ((ISpatialExtent) extent).getGrid() != null
                && getMultiplicity() == 1) {
            return new ShapeToGrid(observable, ShapeValue
                    .sanitize(((ISpatialExtent) extent).getShape()).asExtent(), this);
        }
        return null;
    }

    @Override
    public double getArea() {
        return _shape.getArea();
    }

    @Override
    public ISpatialIndex getIndex(boolean makeNew) {

        if (makeNew) {
            return KLAB.MFACTORY.getSpatialIndex(this);
        }

        if (this._index == null) {
            this._index = KLAB.MFACTORY.getSpatialIndex(this);
        }
        return this._index;

    }

    @Override
    public boolean isConstant() {
        return getMultiplicity() == 1;
    }

}
