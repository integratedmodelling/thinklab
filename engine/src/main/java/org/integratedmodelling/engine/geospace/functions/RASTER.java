/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.engine.geospace.functions;

import java.io.File;
import java.util.Map;

import org.integratedmodelling.api.knowledge.IConcept;
import org.integratedmodelling.api.knowledge.IExpression;
import org.integratedmodelling.api.monitoring.IMonitor;
import org.integratedmodelling.api.services.annotations.Prototype;
import org.integratedmodelling.common.thinkql.CodeExpression;
import org.integratedmodelling.common.utils.MiscUtilities;
import org.integratedmodelling.common.vocabulary.NS;
import org.integratedmodelling.engine.geospace.datasources.RasterFileDataSource;
import org.integratedmodelling.exceptions.ThinklabException;

@Prototype(id = "raster", args = {
        "# id",
        Prototype.TEXT,
        "file",
        Prototype.TEXT,
        "# table",
        Prototype.TEXT,
        "# key",
        Prototype.TEXT }, returnTypes = { NS.DATASOURCE })
public class RASTER extends CodeExpression implements IExpression {

    @Override
    public Object eval(Map<String, Object> parameters, IMonitor monitor, IConcept... context)
            throws ThinklabException {

        File input = new File(getProject().getLoadPath() + File.separator
                + parameters.get("file").toString());

        if (!input.exists()) {
            input = new File(parameters.get("file").toString());
        }

        if (input.exists()) {

            String id = parameters.containsKey("id") ? parameters.get("id").toString() : MiscUtilities
                    .getFileBaseName(input.toString());

            return new RasterFileDataSource(id, input.toString());
        }
        return null;
    }

}
