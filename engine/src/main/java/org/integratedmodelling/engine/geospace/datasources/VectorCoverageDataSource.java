/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.engine.geospace.datasources;

import java.util.ArrayList;
import java.util.List;

import org.geotools.feature.FeatureIterator;
import org.geotools.geometry.jts.ReferencedEnvelope;
import org.integratedmodelling.api.metadata.IMetadata;
import org.integratedmodelling.api.modelling.IExtent;
import org.integratedmodelling.api.modelling.IObserver;
import org.integratedmodelling.api.modelling.IReifiableObject;
import org.integratedmodelling.api.modelling.IScale;
import org.integratedmodelling.api.modelling.runtime.IActiveDataSource;
import org.integratedmodelling.api.modelling.runtime.IActiveObjectSource;
import org.integratedmodelling.api.modelling.runtime.IActuator;
import org.integratedmodelling.api.modelling.runtime.IStateActuator;
import org.integratedmodelling.api.monitoring.IMonitor;
import org.integratedmodelling.api.monitoring.IMonitorable;
import org.integratedmodelling.common.HashableObject;
import org.integratedmodelling.common.metadata.Metadata;
import org.integratedmodelling.common.vocabulary.NS;
import org.integratedmodelling.engine.geospace.Geospace;
import org.integratedmodelling.engine.geospace.coverage.ICoverage;
import org.integratedmodelling.engine.geospace.coverage.vector.AbstractVectorCoverage;
import org.integratedmodelling.engine.geospace.extents.Grid;
import org.integratedmodelling.engine.geospace.extents.SpaceExtent;
import org.integratedmodelling.engine.geospace.literals.ShapeValue;
import org.integratedmodelling.engine.modelling.runtime.Scale;
import org.integratedmodelling.exceptions.ThinklabException;
import org.integratedmodelling.exceptions.ThinklabRuntimeException;
import org.integratedmodelling.exceptions.ThinklabUnsupportedOperationException;
import org.opengis.feature.simple.SimpleFeature;

import com.vividsolutions.jts.geom.Geometry;

/**
 * @author Ferdinando
 */
public abstract class VectorCoverageDataSource extends HashableObject implements IActiveDataSource,
        IActiveObjectSource,
        IMonitorable {

    protected ICoverage coverage          = null;
    IMetadata           metadata          = new Metadata();
    IMonitor            _monitor;
    protected String    _id;
    // if not passed as an init parameter, this will stay null and we use defaults that
    // depend on the projection.
    Boolean             invertCoordinates = null;
    protected int       _errors           = 0;

    @Override
    public void setMonitor(IMonitor monitor) {
        _monitor = monitor;
    }

    @Override
    public IMonitor getMonitor() {
        return _monitor;
    }

    class ShapeObject implements IReifiableObject {

        SimpleFeature _feature;
        ShapeValue    _shape;
        IScale        _scale;

        public ShapeObject(SimpleFeature feature) {
            _feature = feature;
            _shape = new ShapeValue((Geometry) _feature.getDefaultGeometry(), coverage
                    .getCoordinateReferenceSystem());
        }

        @Override
        public IScale getScale(IScale parent) {

            if (_scale == null) {

                SpaceExtent pspace = (SpaceExtent) parent.getSpace();
                try {
                    _shape = _shape.transform(pspace.getShape().getCRS());
                    _shape = _shape.intersection(pspace.getShape());
                    SpaceExtent space = new SpaceExtent(_shape);
                    _scale = new Scale(new IExtent[] { space });
                } catch (ThinklabException e) {
                    throw new ThinklabRuntimeException(e);
                }
            }

            return _scale;
        }

        @Override
        public Object getAttributeValue(String attribute, IObserver observer) {

            Object ret = _feature.getAttribute(attribute);
            if (ret == null) {
                ret = _feature.getAttribute(attribute.toLowerCase());
            }
            if (ret == null) {
                return null;
            }
            if (observer != null && NS.isNumericTransformation(observer)) {
                ret = Double.parseDouble(ret.toString());
            }

            return ret;
        }

        @Override
        public String getId() {
            return _feature.getID();
        }

    }

    /**
     * This must ensure that coverage contains a valid vector coverage.
     * 
     * @throws ThinklabException
     */
    protected abstract void initialize() throws ThinklabException;

    @Override
    public IActuator getAccessor(IScale context, IObserver observer, IMonitor monitor)
            throws ThinklabException {

        initialize();

        SpaceExtent space = context.getSpace() instanceof SpaceExtent ? (SpaceExtent) (context.getSpace())
                : null;

        if (space != null && space.getGrid() != null) {

            ICoverage cov = coverage.requireMatch(space.getGrid(), observer, monitor, true);
            RegularRasterGridDataSource ds = new RegularRasterGridDataSource(cov, space.getGrid()) {

                IMetadata _metadata = new Metadata();

                @Override
                protected ICoverage readData() throws ThinklabException {
                    return _coverage;
                }

                @Override
                protected Grid getFinalExtent(IScale context) throws ThinklabException {
                    return _finalExtent;
                }

                @Override
                public IMetadata getMetadata() {
                    return _metadata;
                }

                @Override
                public boolean isAvailable() {
                    return true;
                }
            };

            return ds.getAccessor(context, observer, monitor);

        } else {

            /*
             * TODO accessor for vectors - may need to do the monster conversion if
             * the context's shapes are different
             */
            throw new ThinklabUnsupportedOperationException("vector accessors not there yet, please be patient");
        }
    }

    @Override
    public IMetadata getMetadata() {
        return metadata;
    }

    @Override
    public IScale getCoverage() {

        Scale ret = new Scale();

        try {
            initialize();
        } catch (ThinklabException e) {
            if (_monitor != null && _errors == 0) {
                _monitor.error("vector data source " + _id + " is inaccessible");
                _errors++;
            }
            return ret;
        }

        if (coverage == null) {
            if (_monitor != null && _errors == 0) {
                _monitor.error("vector data source " + _id + " is inaccessible");
                _errors++;
            }
            throw new ThinklabRuntimeException("cannot read vector data source");
        }

        try {
            ShapeValue shape = new ShapeValue(coverage.getEnvelope());
            shape = shape.transform(Geospace.get().getDefaultCRS());
            ret.mergeExtent(new SpaceExtent(shape), true);
        } catch (ThinklabException e) {
            if (_monitor != null && _errors == 0) {
                _monitor.error("vector data source " + _id + " is inaccessible");
                _errors++;
            }
            throw new ThinklabRuntimeException(e);
        }

        return ret;
    }

    @Override
    public List<IReifiableObject> getObjects(IScale scale) {

        IExtent space = scale.getSpace();
        if (!(space instanceof SpaceExtent)) {
            throw new ThinklabRuntimeException("cannot extract objects from a vector coverage in a non-spatial context");
        }

        ReferencedEnvelope env = ((SpaceExtent) space).getEnvelope();
        if (invertCoordinates != null && invertCoordinates) {
            env = new ReferencedEnvelope(env.getMinY(), env.getMaxY(), env.getMinX(), env.getMaxX(), env
                    .getCoordinateReferenceSystem());
        }
        ArrayList<IReifiableObject> ret = new ArrayList<IReifiableObject>();

        FeatureIterator<SimpleFeature> fit = null;
        try {
            initialize();
            fit = ((AbstractVectorCoverage) coverage).getFeatureIterator(env);
            while (fit.hasNext()) {
                ret.add(new ShapeObject(fit.next()));
            }

        } catch (ThinklabException e) {
            throw new ThinklabRuntimeException(e);
        } finally {
            if (fit != null) {
                fit.close();
            }
        }

        return ret;
    }

    /**
     * Extract the first shape as a shape value.
     * 
     * @return
     * @throws ThinklabException
     */
    public ShapeValue extractFirst() throws ThinklabException {

        FeatureIterator<SimpleFeature> fit = null;
        SimpleFeature feature = null;
        try {
            initialize();
            fit = ((AbstractVectorCoverage) coverage).getFeatureIterator(null);
            if (fit.hasNext()) {
                feature = fit.next();
            }
        } finally {
            if (fit != null) {
                fit.close();
            }
        }

        return new ShapeValue((Geometry) feature.getDefaultGeometry(), coverage
                .getCoordinateReferenceSystem());
    }

    @Override
    public IStateActuator getAccessor(String attribute, IObserver observer, IScale context, IMonitor monitor)
            throws ThinklabException {

        /*
         * if observer is presence, return rasterizing accessor with no attribute 
         */

        /*
         * else must find attribute with compatible observer and rasterize it, with
         * mediation if necessary.
         */
        // TODO Auto-generated method stub
        return null;
    }

}
