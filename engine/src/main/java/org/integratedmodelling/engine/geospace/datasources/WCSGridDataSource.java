/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.engine.geospace.datasources;

import java.util.Properties;

import org.integratedmodelling.engine.geospace.coverage.ICoverage;
import org.integratedmodelling.engine.geospace.coverage.raster.AbstractRasterCoverage;
import org.integratedmodelling.engine.geospace.coverage.raster.WCSCoverage;
import org.integratedmodelling.exceptions.ThinklabException;

public class WCSGridDataSource extends RegularRasterGridDataSource {

    private Properties _properties = new Properties();
    private String     _service;

    public WCSGridDataSource(String service, String id, double[] noData) throws ThinklabException {

        _service = service;
        _id = id;

        _properties.put(WCSCoverage.WCS_SERVICE_PROPERTY, _service);

        for (double d : noData) {
            if (!Double.isNaN(d)) {
                String s = _properties.getProperty(AbstractRasterCoverage.NODATA_PROPERTY, "");
                if (s.length() > 0)
                    s += ",";
                s += d;
                _properties.put(AbstractRasterCoverage.NODATA_PROPERTY, s);
            }
        }
    }

    @Override
    public String toString() {
        return "wcs [" + _id + "]";
    }

    @Override
    protected ICoverage readData() throws ThinklabException {

        if (this._coverage == null) {
            this._coverage = new WCSCoverage(_id, _properties, _monitor);
        }
        return this._coverage;
    }

    @Override
    public boolean isAvailable() {
        // TODO Auto-generated method stub
        return true;
    }

}
