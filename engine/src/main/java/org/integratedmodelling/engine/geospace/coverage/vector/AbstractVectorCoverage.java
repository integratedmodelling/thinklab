/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.engine.geospace.coverage.vector;

import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.net.URL;
import java.util.ArrayList;
import java.util.Iterator;

import org.apache.jcs.access.exception.CacheException;
import org.geotools.data.DataStore;
import org.geotools.data.FeatureSource;
import org.geotools.factory.CommonFactoryFinder;
import org.geotools.factory.GeoTools;
import org.geotools.feature.FeatureCollection;
import org.geotools.feature.FeatureIterator;
import org.geotools.filter.text.cql2.CQL;
import org.geotools.filter.text.cql2.CQLException;
import org.geotools.geometry.jts.ReferencedEnvelope;
import org.integratedmodelling.api.modelling.IObserver;
import org.integratedmodelling.api.monitoring.IMonitor;
import org.integratedmodelling.common.utils.NetUtilities;
import org.integratedmodelling.engine.KLABEngine;
import org.integratedmodelling.engine.geospace.Geospace;
import org.integratedmodelling.engine.geospace.coverage.ICoverage;
import org.integratedmodelling.engine.geospace.extents.Area;
import org.integratedmodelling.engine.geospace.extents.Grid;
import org.integratedmodelling.engine.geospace.gis.ThinklabRasterizer;
import org.integratedmodelling.engine.geospace.literals.ShapeValue;
import org.integratedmodelling.engine.modelling.kbox.ModelData;
import org.integratedmodelling.exceptions.ThinklabException;
import org.integratedmodelling.exceptions.ThinklabIOException;
import org.integratedmodelling.exceptions.ThinklabRuntimeException;
import org.integratedmodelling.exceptions.ThinklabUnsupportedOperationException;
import org.integratedmodelling.exceptions.ThinklabValidationException;
import org.opengis.feature.simple.SimpleFeature;
import org.opengis.feature.simple.SimpleFeatureType;
import org.opengis.feature.type.AttributeDescriptor;
import org.opengis.filter.Filter;
import org.opengis.filter.FilterFactory2;
import org.opengis.referencing.crs.CoordinateReferenceSystem;

import com.vividsolutions.jts.geom.Geometry;

public abstract class AbstractVectorCoverage implements ICoverage, Iterable<ShapeValue> {

    public static String ALL_ATTRIBUTES = "__ALL_ATTRIBUTES__";

    protected FeatureSource<SimpleFeatureType, SimpleFeature> featureSource = null;

    CoordinateReferenceSystem crs     = null;
    String                    crsCode = null;

    String coverageId = null;

    int attributeHandle = -1;

    protected String             layerName        = null;
    private String               valueField       = null;
    protected String             sourceUrl        = null;
    protected String             filterExpression = null;
    private String               valueDefault;
    protected ReferencedEnvelope envelope;

    private float fillValue = Float.NaN;

    AbstractVectorCoverage() {
    }

    protected FeatureSource<SimpleFeatureType, SimpleFeature> getFeatureSource() throws ThinklabException {
        if (featureSource == null) {
            try {
                featureSource = getDataStore().getFeatureSource(coverageId);
            } catch (IOException e) {
                throw new ThinklabIOException(e);
            }
        }
        return featureSource;
    }

    protected FeatureCollection<SimpleFeatureType, SimpleFeature> getFeatureCollection(CoordinateReferenceSystem desiredCRS)
            throws ThinklabException {

        FeatureCollection<SimpleFeatureType, SimpleFeature> featureCollection = null;

        try {
            // THIS WOULD WORK IF GEOTOOLS WORKED. WITH GT BUGS, THIS MESS IS ON ME ANYWAY.
            // if (envelope != null) {
            // DefaultQuery q = new DefaultQuery();
            // q.setCoordinateSystem(getFeatureSource().getSchema().getCoordinateReferenceSystem());
            // q.setCoordinateSystemReproject(desiredCRS);
            // featureCollection = getFeatureSource().getFeatures(q);
            // } else {
            featureCollection = getFeatureSource().getFeatures();
            // }
        } catch (IOException e) {
            throw new ThinklabIOException(e);
        }
        return featureCollection;
    }

    protected abstract DataStore getDataStore() throws ThinklabException;

    public AttributeDescriptor getAttributeDescriptor(String valueId) throws ThinklabException {
        return getFeatureSource().getSchema().getDescriptor(valueId);
    }

    static public class CachedDescriptor implements Serializable {

        public static final long serialVersionUID = 1453574401114688492L;
        public double            x1, x2, y1, y2;
        public String            srs;
        public String            coverageId;
    }

    class ShapeIterator implements Iterator<ShapeValue> {

        private FeatureIterator<SimpleFeature> it;

        ShapeIterator() throws ThinklabException {
            this.it = getFeatureIterator(null, (String[]) null);
        }

        @Override
        public boolean hasNext() {
            return it.hasNext();
        }

        @Override
        public ShapeValue next() {

            SimpleFeature feature = it.next();

            /*
             * TODO add all attributes as metadata
             */
            return new ShapeValue((Geometry) feature.getDefaultGeometry(), getCoordinateReferenceSystem());
        }

        @Override
        public void remove() {
            // come on
        }

    }

    public AbstractVectorCoverage(URL url, String layerName, String valueField, String filter)
            throws ThinklabException {

        this.valueField = (valueField == null || valueField.equals(ModelData.PRESENCE_ATTRIBUTE)) ? null
                : valueField;
        this.sourceUrl = url.toString();
        this.filterExpression = filter;
        this.layerName = layerName;

        if (!readFromCache()) {

            if (url.toString().startsWith("http:") && !NetUtilities.urlResponds(url.toString())) {
                throw new ThinklabIOException("connection to WFS host failed for layer " + layerName);
            }

            try {

                /*
                 * LEAVE this please - must set the coverageId before the next statement in WFS
                 */
                getDataStore();

                if (coverageId == null) {
                    coverageId = getDataStore().getTypeNames()[0];
                }

                envelope = new ReferencedEnvelope(getFeatureSource().getBounds());
                crs = getFeatureSource().getSchema().getCoordinateReferenceSystem();
                crsCode = Geospace.getCRSIdentifier(crs, false);

                if (this instanceof WFSCoverage && crsCode.equals("EPSG:4326")) {

                    /*
                     * invert the fucking envelope as WFS 1.1.0 is guaranteed to return it
                     * north-based.
                     */
                    envelope = new ReferencedEnvelope(envelope.getMinY(), envelope.getMaxY(), envelope
                            .getMinX(), envelope.getMaxX(), crs);
                }

            } catch (Exception e) {
                throw new ThinklabIOException(e);
            }

            saveToCache();
        }
    }

    private void saveToCache() throws ThinklabException {

        if (sourceUrl.startsWith("file:")) {
            return;
        }

        CachedDescriptor cd = new CachedDescriptor();
        String key = sourceUrl + "#" + layerName;

        cd.srs = crsCode;
        cd.x1 = envelope.getMinX();
        cd.x2 = envelope.getMaxX();
        cd.y1 = envelope.getMinY();
        cd.y2 = envelope.getMaxY();
        cd.coverageId = coverageId;

        // DataRecorder.get().error(layerName + " written: " + envelope);
        //
        try {
            Geospace.get().getWFSCache().put(key, cd);

        } catch (CacheException e) {
            throw new ThinklabIOException(e);
        }

    }

    private boolean readFromCache() throws ThinklabException {

        if (sourceUrl.startsWith("file:")) {
            return false;
        }

        String key = sourceUrl + "#" + layerName;

        CachedDescriptor cd = (CachedDescriptor) Geospace.get().getWFSCache().get(key);

        if (cd != null) {

            this.crsCode = cd.srs;
            this.crs = Geospace.getCRSFromID(cd.srs);
            this.envelope = new ReferencedEnvelope(cd.x1, cd.x2, cd.y1, cd.y2, crs);
            this.coverageId = cd.coverageId;
            return true;
        }
        return false;
    }

    @Override
    public String getCoordinateReferenceSystemCode() throws ThinklabValidationException {
        return crsCode;
    }

    /**
     * Create a new raster coverage for the passed extent and return it. It even sounds 
     * easy.
     * 
     * @param arealExtent
     * @return
     * @throws ThinklabException
     */
    public ICoverage convertToRaster(Grid arealExtent, IObserver observer, IMonitor monitor)
            throws ThinklabException {
        return ThinklabRasterizer
                .rasterize(this, valueField, this.fillValue, arealExtent, observer, valueDefault, monitor, (this instanceof WFSCoverage));
    }

    @Override
    public ICoverage requireMatch(Area arealExtent, IObserver observer, IMonitor monitor, boolean allowClassChange)
            throws ThinklabException {

        ICoverage ret = null;

        if (arealExtent instanceof Grid && allowClassChange) {

            if (monitor != null) {
                monitor.info("rasterizing " + layerName
                        + (valueField != null ? (" using attribute " + valueField) : "") + "...", null);
            }
            ret = convertToRaster((Grid) arealExtent, observer, monitor);

            // if (monitor != null) {
            // monitor.info("finished rasterizing " + layerName, null);
            // }

        } else {

            /*
             * TODO reproject and subset if the passed extent is different from ours
             */
            ret = this;
        }

        return ret;
    }

    @Override
    public Object getSubdivisionValue(int subdivisionOrder, Area extent) throws ThinklabValidationException {
        // TODO Auto-generated method stub
        return null;
    }

    public void setName(String covId) {
        layerName = covId;
    }

    public String[] getAttributeNames() throws ThinklabException {

        String[] ret = new String[getFeatureSource().getSchema().getAttributeCount()];
        int i = 0;

        for (AttributeDescriptor ad : getFeatureSource().getSchema().getAttributeDescriptors()) {
            ret[i++] = ad.getLocalName();
        }

        return ret;
    }

    @Override
    public ReferencedEnvelope getEnvelope() {
        return envelope;
    }

    @Override
    public Iterator<ShapeValue> iterator() {
        try {
            return new ShapeIterator();
        } catch (ThinklabException e) {
            throw new ThinklabRuntimeException(e);
        }
    };

    /**
     * 
     * @param envelope
     * @param attributes If we want the features to retain an attribute other than the
     *        geometry, pass it here
     * @return
     * @throws ThinklabException
     */
    public FeatureIterator<SimpleFeature> getFeatureIterator(ReferencedEnvelope envelope, String... attributes)
            throws ThinklabException {

        ClassLoader clsl = null;
        FeatureIterator<SimpleFeature> ret = null;

        try {

            // SPI be damned (BTW TODO - probably now unnecessary)
            clsl = KLABEngine.get().swapClassloader();

            if (envelope == null) {

                FeatureCollection<SimpleFeatureType, SimpleFeature> fc = getFeatureCollection(null);
                ret = fc.features();

            } else {

                /*
                 * get the envelope in the coverage's SRS before we do any transformation
                 */
                ReferencedEnvelope dEnvelope = null;
                try {
                    dEnvelope = envelope.transform(crs, true);
                } catch (Exception e) {
                    throw new ThinklabValidationException(e);
                }

                /*
                 * TODO/FIXME/CHECK
                 * 
                 * if the COVERAGE is in WGS84, WFS will want the bounding box in lat/lon order. How do we ensure which box we need to use?
                 * Datasource properties? For Geoserver, this is OK. For anything else, who knows. 
                 * 
                 * NOTE: SWAPPING ONLY in WFS. We force WFS protocol to 1.1.0 which SHOULD be guaranteed to swap. For
                 * inline data, Geoserver's setting of keeping XY consistent should get it done.
                 * 
                 */
                if (crsCode.equals("EPSG:4326")
                        && Geospace.getCRSIdentifier(envelope.getCoordinateReferenceSystem(), true)
                                .equals("EPSG:4326")
                        && this instanceof WFSCoverage) {
                    envelope = new ReferencedEnvelope(envelope.getMinY(), envelope.getMaxY(), envelope
                            .getMinX(), envelope.getMaxX(), envelope.getCoordinateReferenceSystem());

                    dEnvelope = envelope;
                }

                FeatureCollection<SimpleFeatureType, SimpleFeature> fc = getFeatureCollection(envelope
                        .getCoordinateReferenceSystem());

                /*
                 * query
                 */
                FilterFactory2 ff = CommonFactoryFinder.getFilterFactory2(GeoTools.getDefaultHints());
                String geomName = fc.getSchema().getGeometryDescriptor().getLocalName();
                Filter filter = ff.bbox(ff.property(geomName), dEnvelope);

                // READ ALL THE F'ING ATTRIBUTES. We only do this once, so we don't know what we'll need in
                // different models.
                // if (true /*attributes != null && attributes.length > 0 && attributes[0] != null
                // && attributes[0].equals(ALL_ATTRIBUTES)*/) {

                ArrayList<String> atn = new ArrayList<String>();
                for (AttributeDescriptor i : fc.getSchema().getAttributeDescriptors()) {
                    if (!i.getLocalName().equals(geomName))
                        atn.add(i.getLocalName());
                }

                attributes = atn.toArray(new String[atn.size()]);
                // }

                if (filterExpression != null) {
                    try {
                        Filter cql = CQL.toFilter(filterExpression);
                        filter = ff.and(filter, cql);
                    } catch (CQLException e) {
                        throw new ThinklabValidationException(e);
                    }
                }

                /*
                 * attributes to put in the query
                 */
                String[] attnames = new String[] { geomName };
                int ii = 0;
                for (String ss : attributes) {
                    if (ss != null)
                        ii++;
                }
                attnames = new String[ii + 1];
                attnames[0] = geomName;
                if (attributes != null) {
                    int i = 1;
                    for (String a : attributes) {
                        if (a != null)
                            attnames[i++] = a;
                    }
                }

                FeatureCollection<SimpleFeatureType, SimpleFeature> feat = fc.subCollection(filter);

                ret = feat.features();
            }

        } finally {
            KLABEngine.get().resetClassLoader(clsl);
        }

        return ret;
    }

    @Override
    public void write(File f) throws ThinklabException {

        if (!(f.toString().endsWith(".shp"))) {
            throw new ThinklabUnsupportedOperationException("vector coverage: writing: only shapefile format is supported for now");
        }
    }

    @Override
    public String getLayerName() {
        return layerName == null ? "no name" : layerName;
    }

    @Override
    public String getSourceUrl() {
        return sourceUrl;
    }

    @Override
    public CoordinateReferenceSystem getCoordinateReferenceSystem() {
        if (crs == null && crsCode != null) {
            crs = Geospace.getCRSFromID(crsCode);
        }
        return crs;
    }

}
