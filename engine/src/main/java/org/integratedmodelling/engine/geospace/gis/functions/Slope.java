/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.engine.geospace.gis.functions;

import java.util.HashMap;
import java.util.Map;

import org.integratedmodelling.api.modelling.IDirectObservation;
import org.integratedmodelling.api.modelling.IModel;
import org.integratedmodelling.api.modelling.IObservable;
import org.integratedmodelling.api.modelling.IObservation;
import org.integratedmodelling.api.modelling.IProcess;
import org.integratedmodelling.api.modelling.IState;
import org.integratedmodelling.api.modelling.resolution.IResolutionContext;
import org.integratedmodelling.api.modelling.runtime.IProcessContextualizer;
import org.integratedmodelling.api.modelling.scheduling.ITransition;
import org.integratedmodelling.api.monitoring.IMonitor;
import org.integratedmodelling.api.project.IProject;
import org.integratedmodelling.api.services.annotations.Prototype;
import org.integratedmodelling.common.vocabulary.GeoNS;
import org.integratedmodelling.common.vocabulary.NS;
import org.integratedmodelling.engine.geospace.gis.SextanteOperations;
import org.integratedmodelling.exceptions.ThinklabException;
import org.integratedmodelling.exceptions.ThinklabValidationException;

import es.unex.sextante.core.OutputFactory;
import es.unex.sextante.core.OutputObjectsSet;
import es.unex.sextante.core.ParametersSet;
import es.unex.sextante.dataObjects.IRasterLayer;
import es.unex.sextante.geotools.GTOutputFactory;
import es.unex.sextante.morphometry.slope.SlopeAlgorithm;
import es.unex.sextante.outputs.Output;

@Prototype(
        id = "gis.slope",
        args = { "# method", "burgess|bauer|tarboton|heerdegen|zevenberger|haralick|maximum-slope" },
        returnTypes = { NS.PROCESS_CONTEXTUALIZER })
public class Slope implements IProcessContextualizer {

    public final static int METHOD_MAXIMUM_SLOPE = SlopeAlgorithm.METHOD_MAXIMUM_SLOPE;
    public final static int METHOD_TARBOTON      = SlopeAlgorithm.METHOD_TARBOTON;
    public final static int METHOD_BURGESS       = SlopeAlgorithm.METHOD_BURGESS;
    public final static int METHOD_BAUER         = SlopeAlgorithm.METHOD_BAUER;
    public final static int METHOD_HEERDEGEN     = SlopeAlgorithm.METHOD_HEERDEGEN;
    public final static int METHOD_ZEVENBERGEN   = SlopeAlgorithm.METHOD_ZEVENBERGEN;
    public final static int METHOD_HARALICK      = SlopeAlgorithm.METHOD_HARALICK;

    public final static int UNITS_RADIANS    = SlopeAlgorithm.UNITS_RADIANS;
    public final static int UNITS_DEGREES    = SlopeAlgorithm.UNITS_DEGREES;
    public final static int UNITS_PERCENTAGE = SlopeAlgorithm.UNITS_PERCENTAGE;

    IProject project;
    int      method     = METHOD_HARALICK;
    boolean  canDispose = false;

    @Override
    public boolean canDispose() {
        return canDispose;
    }

    @Override
    public void setContext(Map<String, Object> parameters, IModel model, IProject project) {

        this.project = project;
        if (parameters.containsKey("method")) {

            String m = parameters.get("method").toString().toLowerCase();
            if (m.equals("burgess")) {
                method = METHOD_BURGESS;
            } else if (m.equals("bauer")) {
                method = METHOD_BAUER;
            } else if (m.equals("tarboton")) {
                method = METHOD_TARBOTON;
            } else if (m.equals("heerdegen")) {
                method = METHOD_HEERDEGEN;
            } else if (m.equals("zevenbergen")) {
                method = METHOD_ZEVENBERGEN;
            } else if (m.equals("haralick")) {
                method = METHOD_HARALICK;
            } else if (m.equals("maximum-slope")) {
                method = METHOD_MAXIMUM_SLOPE;
            }
        }

    }

    @Override
    public Map<String, IObservation> initialize(IProcess process, IDirectObservation context, IResolutionContext resolutionContext, Map<String, IObservable> expectedInputs, Map<String, IObservable> expectedOutputs, IMonitor monitor)
            throws ThinklabException {

        canDispose = !context.getScale().isTemporallyDistributed();

        GeoNS.synchronize();

        Map<String, IObservation> ret = new HashMap<>();

        SlopeAlgorithm alg = new SlopeAlgorithm();
        ParametersSet parms = alg.getParameters();
        IState elevation = null;

        /*
         * TODO we should also check that the model does not expect anything but aspect and the core
         * process.
         */
        String oName = null;
        IObservable output = null;
        for (String n : expectedOutputs.keySet()) {
            if (expectedOutputs.get(n).is(GeoNS.SLOPE)) {
                oName = n;
                output = expectedOutputs.get(n);
            }
        }

        if (output == null) {
            monitor.error(new ThinklabValidationException("slope GIS computation: model has no usable concept in outputs"));
        }

        for (IState st : context.getStates()) {
            if (st.getObservable().is(GeoNS.ELEVATION)) {
                elevation = st;
            }
        }

        if (elevation == null) {
            monitor.error(new ThinklabValidationException("aspect GIS computation: DEM not found in observed dependencies"));
        }

        /*
         * units based on the output observer 
         */
        int unit = SlopeAlgorithm.UNITS_DEGREES;

        IRasterLayer dem = SextanteOperations.getInputAsRaster(elevation);

        try {
            parms.getParameter(SlopeAlgorithm.UNITS).setParameterValue(new Integer(unit));
            parms.getParameter(SlopeAlgorithm.METHOD).setParameterValue(new Integer(method));
            parms.getParameter(SlopeAlgorithm.DEM).setParameterValue(dem);

            OutputFactory outputFactory = new GTOutputFactory();
            alg.execute(SextanteOperations.getTaskMonitor(monitor), outputFactory);

            OutputObjectsSet outputs = alg.getOutputObjects();
            Output slope = outputs.getOutput(SlopeAlgorithm.SLOPE);
            IState aspect = null;

            /*
             * result is only dynamic if elevation is.
             */
            if (elevation.isTemporallyDistributed()) {
                aspect = SextanteOperations.getStateFromRaster(output, context, (IRasterLayer) slope
                        .getOutputObject());
            } else {
                aspect = SextanteOperations.getStaticStateFromRaster(output, context, (IRasterLayer) slope
                        .getOutputObject());
            }

            ret.put(oName, aspect);

        } catch (Exception e) {
            monitor.error(e);
        }

        return ret;
    }

    @Override
    public Map<String, IObservation> compute(ITransition transition, Map<String, IState> inputs)
            throws ThinklabException {
        canDispose = transition.isLast();
        return null;
    }

}
