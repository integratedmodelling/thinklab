package org.integratedmodelling.thinklab.script

import org.integratedmodelling.engine.runtime.Context
import org.integratedmodelling.engine.scripting.ModelProxy

class ThinklabScriptBase extends Script {

    public ModelProxy measure(Object what, Object unit) {
        return null;
    }

    public ModelProxy rank(Object what) {
        return null;
    }

    public ModelProxy rank(Object what, double from, double to) {
        return null;
    }

    public ModelProxy presence(Object what) {
        return null;
    }

    public ModelProxy percentage(Object what) {
        return null;
    }

    public ModelProxy ratio(Object what) {
        return null;
    }

    public ModelProxy proportion(Object what) {
        return null;
    }

    public ModelProxy classify(Object what, Object ... classification) {
        return null;
    }

    public ModelProxy value(Object what, Object currency) {
        return null;
    }

    public Context observe(Object o) {
        def ret = new Context(getBinding().getVariable("_session"), getBinding().getVariable("_monitor"));
        return new org.integratedmodelling.thinklab.script.Context(ret.observe(o).finish());
    }

    @Override
    public Object run() {
        // TODO Auto-generated method stub
        return null;
    }
}
