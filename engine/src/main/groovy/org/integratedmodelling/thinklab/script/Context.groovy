package org.integratedmodelling.thinklab.script

class Context {

    org.integratedmodelling.engine.runtime.Context ctx;

    public Context(org.integratedmodelling.engine.runtime.Context ctx) {
        this.ctx = ctx
    }

    /*
     * context << observable observes it.
     */
    def leftShift(observable){

        this.ctx.observe(observable)

        /*
         * TODO
         * return all resulting new observations. No method for that right now.
         */
    }
}
