package org.integratedmodelling.thinklab.actions

import org.integratedmodelling.api.knowledge.IConcept
import org.integratedmodelling.common.vocabulary.Unit
import org.integratedmodelling.engine.KLABEngine

class ActionScript extends DefaultAction {

    class Quantity {

        Unit unit;
        Number n;

        Quantity(Number n, String unit) {
            this.n = n;
            this.unit = new Unit(unit);
        }

        public Object times(Object o) {

            if (o instanceof Number) {
                return new Quantity(o * n, unit);
            } else if (o instanceof Quantity) {
            }

            return Double.NaN;
        }
    }

    class NumVar {

        Number n;
        NumVar(Number n) {
            this.n = n;
        }
    }

    class ClassVar {

        IConcept c;
        ClassVar(String concept) {
            this.c = KLABEngine.c(concept);
        }

        public boolean is(Object o) {
            if (o instanceof ClassVar) {
                return c.is(((ClassVar)o).c);
            }
        }
    }

    public Quantity _q(Number n, String unit) {
        return new Quantity(n, unit);
    }
}
