package org.integratedmodelling.thinklab.actions

import org.integratedmodelling.api.knowledge.IConcept
import org.integratedmodelling.api.knowledge.IKnowledge
import org.integratedmodelling.api.knowledge.IProperty
import org.integratedmodelling.api.monitoring.IMonitor
import org.integratedmodelling.common.configuration.KLAB
import org.integratedmodelling.engine.actionsupport.ActionState
import org.jscience.physics.amount.Amount


//static UnitBinding binding = null;

/**
 * The base class for a Thinklab action. No space or time semantics built in.
 * 
 * @author Ferd
 *
 */
abstract class DefaultAction extends Script {

    static inited = false;
    UnitBinding binding;

    static class UnitBinding extends Binding {

        def getVariable(String symbol) {
            if (symbol == 'out') return System.out
            return Amount.valueOf(1, Unit.valueOf(symbol))
        }
    }

    DefaultAction() {
        if (!inited) {
            Number.metaClass.getProperty = { String symbol ->
                Amount.valueOf(delegate, Unit.valueOf(symbol))
            }

            // define opeartor overloading, as JScience doesn\'t use the same operation names as Groovy
            Amount.metaClass.static.valueOf = { Number number, String unit -> Amount.valueOf(number, Unit.valueOf(unit)) }
            Amount.metaClass.multiply = { Number factor -> delegate.times(factor) }
            Number.metaClass.multiply = { Amount amount -> amount.times(delegate) }
            Number.metaClass.div = { Amount amount -> amount.inverse().times(delegate) }
            Amount.metaClass.div = { Number factor -> delegate.divide(factor) }
            Amount.metaClass.div = { Amount factor -> delegate.divide(factor) }
            Amount.metaClass.power = { Number factor -> delegate.pow(factor) }
            Amount.metaClass.negative = { -> delegate.opposite() }

            // for unit conversions
            Amount.metaClass.to = { Amount amount -> delegate.to(amount.unit) }
            Amount.metaClass.to = { String unit -> delegate.to(Unit.valueOf(unit)) }

            binding = new UnitBinding();
        }
    }

    public class V {
    }

    protected void info(String text) {
        Object o = getBinding().getVariable("_monitor");
        if (o != null && o instanceof IMonitor) {
            ((IMonitor)o).info(text, "USERCODE");
        }
    }

    protected void warning(String text) {
        Object o = getBinding().getVariable("_monitor");
        if (o != null && o instanceof IMonitor) {
            ((IMonitor)o).warn(text);
        }
    }

    protected void error(String text) {
        Object o = getBinding().getVariable("_monitor");
        if (o != null && o instanceof IMonitor) {
            ((IMonitor)o).error(text);
        }
    }

    /*
     * window into the knowledge manager
     */
    protected IConcept _getConcept(String string) {
        return KLAB.c(string);
    }

    protected IProperty _getProperty(String string) {
        return KLAB.p(string);
    }

    protected IKnowledge _getKnowledge(String string) {
        return KLAB.k(string);
    }

    @Override
    public Object run() {
        // TODO Auto-generated method stub
        return super.run();
    }

    public ActionState state(IConcept concept, Object value) {
        return new ActionState(concept, value);
    }
}
