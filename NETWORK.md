* Network operations in Thinklab

Attempting to document the complicated dance that allows a user to connect to a personal engine, a personal engine to connect to its primary server (entry point on the Thinklab network), and the process of network building.

1. The im.cert user certificate at client side is read to instantiate a RESTUser (auth package). This logs on to the authentication service, which parses the certificate and returns the user profile, including groups and roles. An authentication token is established so that the user can make further calls to the authentication service. No Thinklab nodes are involved up to this point.

2. Concurrently, a personal engine is fired and initialized. After general initialization, the Network.initialize() method is called, which will operate in two alternative modes according to the certificatws found in configuration. If the engine does not find the server.cert certificate in the configuration directory, it operates as a personal engine and expects to find a im.cert (user certificate). If that is found, a RESTUser is instantiated and set as the "locking user" for the network (returned by Thinklab.getLockingUser()). The remaining authorization proceeds by connecting to the network through the sequence described in (5). After initialization, a personal server waits for authorization from the client (described in 4), which needs to send a matching authorization token to open a session.

3. At any Thinklab engine network node, a server is running and responding on a URL. The RESTUser contains the URL of one of these, which serves as its entry point to hop onto the Thinklab network. The server software is the same as the personal engine, potentially configured to server projects and components (see ENGINE_CONFIGURATION.md). It has a server.cert file in its configuration directory, which establishes its authentication tokens, official URL and a unique network name. It also has a number of certificates for other servers in <config>/network/*.cert; these are the server.cert files of trusted nodes that will be connected to at startup. A node is configured to serve different resources to different user groups, and may have a list of groups that are allowed to see it on the network at all. When the server has a server.cert file, it operates in public mode and its Env.NETWORK.isPersonal() method returns false.

4. The client communicates its user name, groupset and authorization token to the personal engine using Endpoints.AUTHORIZE. If the authorization tokens match, the personal engine opens a session and communicates its ID back to the client for future use. The same personal engine may open different simultaneous sessions for different clients with the same user. Sessions will contain the results of modeling tasks and are automatically deleted after a (long) period of inactivity.




* TODOs

1. Session id and auth token in client/personal-engine communcation should be sent using headers.
2. Network communication between public engines should use the same method and secure calls.