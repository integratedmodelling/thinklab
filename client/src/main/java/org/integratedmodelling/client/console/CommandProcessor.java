/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.client.console;

import java.lang.annotation.Annotation;

import org.integratedmodelling.api.services.IServiceCall;
import org.integratedmodelling.api.services.annotations.Prototype;
import org.integratedmodelling.client.Client;
import org.integratedmodelling.common.command.ServiceManager;
import org.integratedmodelling.common.utils.ClassUtils;
import org.integratedmodelling.common.utils.ClassUtils.AnnotationVisitor;
import org.integratedmodelling.exceptions.ThinklabException;

public class CommandProcessor extends com.eleet.dragonconsole.CommandProcessor {

    protected Console _console;
    private boolean   _commandsOk = false;

    public CommandProcessor(Console console) {
        _console = console;
        _commandsOk = visitAnnotations();
    }

    @Override
    public void processCommand(String input) {

        input = input.trim();

        if (input.equals("exit")) {
            System.exit(0);
        } else if (_commandsOk && input.length() > 0) {

            IServiceCall command = null;
            try {
                command = ServiceManager.get()
                        .parseCommandLine(input, Console.getClient().getMonitor(), Console
                                .getClient().getCurrentSession());

                boolean ok = command != null;
                if (command == null) {
                    _console.warning("Command '" + input + "' incorrect or unknown");
                } else {
                    try {
                        _console.echo("> " + input);
                        Object ret = command.execute();
                        _console.outputResult(input, ret);
                    } catch (Throwable e) {
                        ok = false;
                        _console.error(e);
                    }
                }
                _console.reportCommandResult(input, ok);

            } catch (ThinklabException e) {
                _console.error(e);
            }
        }

    }

    /**
     * Overrides the default output in CommandProcessor to determine if ANSI
     * Colors are processed or DCCC and converts accordingly.
     * @param s The String to output.
     */
    @Override
    public void output(String s) {
        if (getConsole().isUseANSIColorCodes())
            super.output(convertToANSIColors(s));
        else
            super.output(s);
    }

    private boolean visitAnnotations() {

        try {
            ClassUtils.visitAnnotations(Client.class.getPackage().getName(), Prototype.class,
                    new AnnotationVisitor() {
                        @Override
                        public void visit(Annotation acls, Class<?> target) throws ThinklabException {
                            registerCommand(target, (Prototype) acls);
                        }
                    });
        } catch (ThinklabException e) {
            return false;
        }

        return true;
    }

    protected void registerCommand(Class<?> target, Prototype command) {
        ServiceManager.get().processPrototypeDeclaration(command, target);
    }
}
