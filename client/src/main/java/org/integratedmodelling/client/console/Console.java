/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.client.console;

import java.io.File;
import java.util.Collection;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import org.integratedmodelling.Version;
import org.integratedmodelling.api.auth.IUser;
import org.integratedmodelling.api.engine.IEngine;
import org.integratedmodelling.api.modelling.INamespace;
import org.integratedmodelling.api.modelling.scheduling.ITransition;
import org.integratedmodelling.api.monitoring.ITaskIntrospector;
import org.integratedmodelling.api.project.IProject;
import org.integratedmodelling.api.runtime.IContext;
import org.integratedmodelling.api.runtime.ITask;
import org.integratedmodelling.api.ui.IBookmark;
import org.integratedmodelling.client.Client;
import org.integratedmodelling.exceptions.ThinklabException;

import com.eleet.dragonconsole.DragonConsoleFrame;

public class Console {

    static Client             _client;
    static DragonConsoleFrame _console;

    private class Listener implements Client.Listener {

        /*
         * start pretending the network isn't responding, so that the first notification will
         * turn the status to green.
         */
        boolean _netError = true;

        @Override
        public void warn(Object o, ITask task) {
            Console.this.warning(o);
        }

        @Override
        public void info(Object info, ITask task) {
            Console.this.info(info, null);
        }

        @Override
        public void error(Object o, ITask task) {
            Console.this.error(o);
        }

        @Override
        public void debug(Object o, ITask task) {
            // TODO Auto-generated method stub
        }

        @Override
        public void notifyUser(IUser user) {
            Console.this.info("Operating as " + user, null);
        }

        @Override
        public void notifyEngineBooting(IEngine server) {
            Console.this.info(server.getName() + " booting. Please wait...", null);
        }

        @Override
        public void notifyEngineOperational(IEngine server) {
            Console.this.info(server.getName() + " ready.", null);
            try {
                Client.get().reloadWorkspace(true);
            } catch (ThinklabException e) {
                this.error(e, null);
            }
        }

        @Override
        public void notifyEngineFailure(IEngine server) {
            Console.this.error(server.getName() + " failed.");
        }

        // @Override
        // public void notify(INotification notification, ITask task) {
        // Console.this.info((task.getTaskId() < 0 ? "* " : ("[" + task.getTaskId() + "] "))
        // + notification.getText(), task.getTaskId() < 0 ? "GENERAL" : "TASK");
        // }

        @Override
        public void notifyTaskStarted(ITask task) {
            Console.this.info("Task " + task.getTaskId() + " started.", null);
        }

        @Override
        public void notifyTaskFinished(ITask task) {
            Console.this.info("Task " + task.getTaskId() + " finished.", null);
        }

        @Override
        public void notifyTaskInterrupted(ITask task) {
            Console.this.warning("Task " + task.getTaskId() + " interrupted by user.");
        }

        @Override
        public void notifyContextAvailable(IContext context) {
            Console.this.info("Context " + context.getName() + " computed succesfully.", null);
        }

        @Override
        public void notifyTimeTransition(IContext context, ITransition transition) {
            Console.this.info("Context " + context.getId() + " reached time " + transition, null);
        }

        @Override
        public void notifyClientProperty(String key, String value) {
            _client.onPropertyChange(key, value);
        }

        @Override
        public void notifyNamespaceCreated(String namespaceId, IProject project) {
            // TODO Auto-generated method stub
            Console.this
                    .info("namespace " + namespaceId + " created in project " + project.getId(), null);
        }

        @Override
        public void notifyNamespaceDeleted(String namespaceId, IProject project) {
            // TODO Auto-generated method stub
            Console.this
                    .info("namespace " + namespaceId + " deleted in project " + project.getId(), null);
        }

        @Override
        public void notifyPropertiesChanged(IProject project, File file) {
        }

        @Override
        public void scenariosChanged(Set<INamespace> currentScenarios) {
        }

        @Override
        public void notifyEngineStatusUpdated(IEngine engine, Map<?, ?> status) {
        }

        @Override
        public void notifyEngineStopped(IEngine engine) {
            Console.this.info(engine.getName() + " stopped.", null);
        }

        @Override
        public ITaskIntrospector getTaskIntrospector() {
            return null;
        }

        @Override
        public void notifyNamespacesModified(Collection<INamespace> namespaces, INamespace root) {

            Console.this.info("namespace " + root + " modified:", null);
            for (INamespace n : namespaces) {
                Console.this.info("  " + n.getId() + " reloaded", null);
            }
        }

        @Override
        public void notifyComponentsInitialized(IEngine engine) {
            // TODO Auto-generated method stub

        }

        @Override
        public void notifyNetworkStatus(boolean urlResponds) {
            if (_netError && urlResponds) {
                Console.this.info("remote server is online", null);
                _netError = false;
            } else if (!_netError && !urlResponds) {
                Console.this.warning("remote server is offline");
                _netError = true;
            }
        }

        @Override
        public void notifyEngineInitializing(IEngine engine) {
            Console.this.info(engine.getName() + " initializing...", null);
        }

        @Override
        public void notifyBookmarkAdded(IBookmark bookmark) {
            // TODO Auto-generated method stub

        }
    }

    private boolean initializeClient() {
        _client = new Client(new Listener());
        _client.startNetworkPolling();
        return true;
    }

    public static Client getClient() {
        return _client;
    }

    /*
     * ask for a string and return the value. Requires a mess of reciprocal thread waiting, so will be done
     * when I feel like.
     */
    public String ask(String prompt) {
        String oldp = _console.console.getPrompt();
        _console.console.setPrompt(prompt + " ");
        // TODO oh screw it
        return "";
    }

    public void start() throws Exception {

        try {
            javax.swing.SwingUtilities.invokeAndWait(new Runnable() {
                @Override
                public void run() {
                    String buildInfo = "";
                    if (!Version.VERSION_BUILD.equals("VERSION_BUILD")) {
                        buildInfo = " build " + Version.VERSION_BUILD + " (" + Version.VERSION_BRANCH + " "
                                + Version.VERSION_DATE + ")";
                    }
                    _console = new DragonConsoleFrame("Thinklab client v" + new Version().toString()
                            + buildInfo, false, new CommandHistory());
                    _console.console.setCommandProcessor(new CommandProcessor(Console.this));
                    _console.console.append("ThinkLab client shell v" + new Version().toString() + "\n");
                    _console.console.append("Enter 'help' for a list of commands; 'exit' quits.\n\n");

                    if (initializeClient()) {
                        _console.setVisible(true);
                        new Thread() {
                            @Override
                            public void run() {
                                try {
                                    _client.boot();
                                } catch (ThinklabException e) {
                                    error(e);
                                }
                            }
                        }.start();
                    }
                }
            });
        } catch (Exception exc) {
            exc.printStackTrace();
        }
    }

    protected void error(Object e) {
        if (e instanceof Throwable) {
            /*
             * TODO log stack trace
             */
            e = ((Throwable) e).getMessage();
        } else {
            e = e.toString();
        }
        _console.console.append("&R-" + e + "\n");
    }

    protected void warning(Object e) {
        _console.console.append("&D-" + e + "\n");
    }

    protected void echo(Object e) {
        _console.console.append("&X-" + e + "\n");
        _console.console.repaint();
    }

    protected void info(Object e, String infoClass) {

        String color = "&l-";
        if (infoClass != null) {
            switch (infoClass) {
            case "TASK":
                color = "&g-";
                break;
            case "GENERAL":
                color = "&p-";
                break;
            }
        }

        _console.console.append(color + e + "\n");
    }

    protected void output(Object e) {
        _console.console.append("&w-" + e + "\n");
    }

    /**
     * Entry point for command line client.
     * 
     * @param args
     * @throws Exception
     */
    public static void main(String[] args) throws Exception {
        new Console().start();
    }

    public void outputResult(String input, Object ret) {

        if (ret == null) {
            return;
        }

        if (ret instanceof Map) {
            for (Object o : ((Map<?, ?>) ret).keySet()) {
                output(o + " = " + ((Map<?, ?>) ret).get(o));
            }
        } else if (ret instanceof Iterable) {
            for (Iterator<?> it = ((Iterable<?>) ret).iterator(); it.hasNext();) {
                output(it.next());
            }
        } else {
            output(ret);
        }
    }

    public void reportCommandResult(String input, boolean ok) {
        // add to history
        if (_console.console.getHistory() != null) {
            _console.console.getHistory().append(input);
        }
    }
}
