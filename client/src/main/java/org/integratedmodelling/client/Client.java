/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.client;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.input.NullInputStream;
import org.integratedmodelling.api.auth.IUser;
import org.integratedmodelling.api.engine.IEngine;
import org.integratedmodelling.api.engine.IModelingEngine;
import org.integratedmodelling.api.knowledge.IExpression;
import org.integratedmodelling.api.metadata.IObservationMetadata;
import org.integratedmodelling.api.modelling.IAnnotation;
import org.integratedmodelling.api.modelling.IConditionalObserver;
import org.integratedmodelling.api.modelling.IDependency;
import org.integratedmodelling.api.modelling.IDirectObserver;
import org.integratedmodelling.api.modelling.IExtent;
import org.integratedmodelling.api.modelling.ILanguageObject;
import org.integratedmodelling.api.modelling.IModel;
import org.integratedmodelling.api.modelling.IModelObject;
import org.integratedmodelling.api.modelling.INamespace;
import org.integratedmodelling.api.modelling.IObservable;
import org.integratedmodelling.api.modelling.IObservation;
import org.integratedmodelling.api.modelling.IObserver;
import org.integratedmodelling.api.modelling.IObservingObject;
import org.integratedmodelling.api.modelling.IScale;
import org.integratedmodelling.api.modelling.ISubject;
import org.integratedmodelling.api.modelling.scheduling.ITransition;
import org.integratedmodelling.api.monitoring.IKnowledgeLifecycleListener;
import org.integratedmodelling.api.monitoring.IMonitor;
import org.integratedmodelling.api.monitoring.INotification;
import org.integratedmodelling.api.monitoring.IProjectLifecycleListener;
import org.integratedmodelling.api.monitoring.ITaskIntrospector;
import org.integratedmodelling.api.monitoring.ITaskIntrospector.Info;
import org.integratedmodelling.api.monitoring.Messages;
import org.integratedmodelling.api.network.INode;
import org.integratedmodelling.api.project.IProject;
import org.integratedmodelling.api.runtime.IClient;
import org.integratedmodelling.api.runtime.IContext;
import org.integratedmodelling.api.runtime.ISession;
import org.integratedmodelling.api.runtime.ITask;
import org.integratedmodelling.api.runtime.IWorkspace;
import org.integratedmodelling.client.modelling.runtime.Context;
import org.integratedmodelling.client.modelling.runtime.ContextInternal;
import org.integratedmodelling.client.modelling.runtime.Subject;
import org.integratedmodelling.client.modelling.runtime.Task;
import org.integratedmodelling.client.web.WebServer;
import org.integratedmodelling.collections.Pair;
import org.integratedmodelling.common.Resources;
import org.integratedmodelling.common.auth.RESTUser;
import org.integratedmodelling.common.configuration.KLAB;
import org.integratedmodelling.common.controller.ContextViewer;
import org.integratedmodelling.common.editor.Bookmark;
import org.integratedmodelling.common.editor.BookmarkManager;
import org.integratedmodelling.common.engine.Engine;
import org.integratedmodelling.common.engine.LocalDevelopmentEngine;
import org.integratedmodelling.common.engine.NetworkedDistribution;
import org.integratedmodelling.common.engine.NetworkedDistribution.SyncListener;
import org.integratedmodelling.common.engine.RemotelySynchronizedLocalEngine;
import org.integratedmodelling.common.errormanagement.CompileWarning;
import org.integratedmodelling.common.indexing.KnowledgeIndex;
import org.integratedmodelling.common.kim.KIM;
import org.integratedmodelling.common.kim.KIMModelManager;
import org.integratedmodelling.common.kim.KIMNamespace;
import org.integratedmodelling.common.kim.ModelFactory;
import org.integratedmodelling.common.model.runtime.Transition;
import org.integratedmodelling.common.network.Network;
import org.integratedmodelling.common.owl.KnowledgeManager;
import org.integratedmodelling.common.project.ProjectManager;
import org.integratedmodelling.common.project.Workspace;
import org.integratedmodelling.common.utils.NetUtilities;
import org.integratedmodelling.common.visualization.GraphVisualization;
import org.integratedmodelling.exceptions.ThinklabException;
import org.integratedmodelling.exceptions.ThinklabIOException;
import org.integratedmodelling.exceptions.ThinklabRuntimeException;

/**
 * A client with all the functionalities and a command line parser, ready
 * to serve as a back-end for any CLI or GUI.
 * 
 * @author ferdinando.villa
 *
 */
public class Client implements IClient {

    public static final String ENGINE_CONTEXT_PATH = "rest";
    public static final int    ENGINE_PORT         = 8182;

    HashSet<String>              extendedCommands = new HashSet<>();
    HashMap<String, IAnnotation> deprecated       = new HashMap<>();
    HashMap<Integer, WebServer>  webServers       = new HashMap<>();

    private IMonitor                           monitor = new Monitor();
    private IModelingEngine                    engine;
    private RESTUser                           user;
    private NetworkedDistribution              serverDistribution;
    private Listener                           listener;
    private NetworkedDistribution.SyncListener syncListener;

    Set<INamespace> currentScenarios = new HashSet<INamespace>();

    // tracking client/server registration of projects
    Set<IProject>   notified         = new HashSet<IProject>();
    Set<String>     removed          = new HashSet<String>();
    Set<INamespace> toReindex        = new HashSet<INamespace>();
    ArrayList<File> syncProjectFiles = new ArrayList<>();

    private ClientRESTController tclient;

    private KnowledgeIndex index;

    /*
     * for the network polling service
     */
    ScheduledExecutorService executor                    = null;
    ScheduledFuture<?>       future                      = null;
    volatile Boolean         _monitoring                 = false;
    static long              POLLING_PERIOD_MILLISECONDS = 30000;

    Map<Long, Context>    contexts         = new HashMap<>();
    Map<String, ISession> sessions         = new HashMap<>();
    String                currentSessionId = null;

    ContextViewer contextViewer = null;
    // port for the viewer; set to anything other than default if needed.
    protected int viewerPort    = 0;

    static Client _this;

    /*
     * TODO move to session so that we don't need a singleton here.
     */
    public static Client get() {
        return _this;
    }

    public class Session implements ISession {

        String                  id;
        List<INotification>     notifications = new ArrayList<INotification>();
        List<Long>              _contextIds   = new ArrayList<Long>();
        List<Long>              _taskIds      = new ArrayList<Long>();
        HashMap<Long, IContext> _contexts     = new HashMap<>();
        HashMap<Long, ITask>    _tasks        = new HashMap<>();

        /*
         * FIXME - wrong: should be linked to the console
         */
        PrintStream _out = KLAB.getLoggerStream();
        InputStream _in  = new NullInputStream(512);

        Session(String id) {
            this.id = id;
        }

        @Override
        public void setMonitor(IMonitor monitor) {
        }

        @Override
        public IMonitor getMonitor() {
            return monitor;
        }

        @Override
        public PrintStream out() {
            return _out;
        }

        @Override
        public InputStream in() {
            return _in;
        }

        @Override
        public String getId() {
            return id;
        }

        @Override
        public IUser getUser() {
            return user;
        }

        @Override
        public void notify(INotification notification) {
            synchronized (notifications) {
                notifications.add(notification);
            }
        }

        @Override
        public List<INotification> getNotifications(boolean clear) {
            synchronized (notifications) {
                List<INotification> ret = new ArrayList<INotification>(notifications);
                if (clear) {
                    notifications.clear();
                }
                return ret;
            }
        }

        @Override
        public IContext getContext() {
            return _contextIds.size() == 0 ? null : _contexts.get(_contextIds.get(_contextIds.size() - 1));
        }

        @Override
        public List<IContext> getContexts() {
            ArrayList<IContext> ret = new ArrayList<IContext>();
            for (long l : _contextIds) {
                ret.add(_contexts.get(l));
            }
            return ret;
        }

        public void addContext(IContext context) {
            if (!_contexts.containsKey(context.getId())) {
                _contextIds.add(0, context.getId());
            }
            _contexts.put(context.getId(), context);
        }

        @Override
        public IContext getContext(long intValue) {
            return _contexts.get(intValue);
        }

        public ITask getTask(long taskId) {
            return _tasks.get(taskId);
        }

        public void addTask(ITask task) {
            if (!_tasks.containsKey(task.getTaskId())) {
                _taskIds.add(0, task.getTaskId());
            }
            _tasks.put(task.getTaskId(), task);
        }

        @Override
        public List<ITask> getTasks() {
            ArrayList<ITask> ret = new ArrayList<ITask>();
            for (long l : _taskIds) {
                ret.add(_tasks.get(l));
            }
            return ret;
        }

        @Override
        public void close() throws IOException {
            // TODO Auto-generated method stub
            // TODO logoff with engine
        }

    }

    class Monitor implements IMonitor {

        @Override
        public void warn(Object o) {
            listener.warn(o, null);
        }

        @Override
        public void info(Object info, String infoClass) {
            listener.info(info, null);
        }

        @Override
        public void error(Object o) {
            listener.error(o, null);
        }

        @Override
        public void debug(Object o) {
            listener.debug(o, null);
        }

        @Override
        public void send(INotification notification) {

            if (notification.getMessage() == null) {

                /*
                 * TODO not sure it should be here
                 */
                ITask task = null;
                if (notification.getSessionId() != null) {
                    Session session = Client.this.getSession(notification.getSessionId());
                    session.notify(notification);
                    task = session.getTask(notification.getTaskId());
                }

                switch (notification.getLevel()) {
                case INotification.DEBUG:
                    listener.debug(notification.getText(), task);
                    break;
                case INotification.INFO:
                    listener.info(notification.getText(), task);
                    break;
                case INotification.WARNING:
                    listener.warn(notification.getText(), task);
                    break;
                case INotification.ERROR:
                case INotification.FATAL:
                    listener.error(notification.getText(), task);
                    break;
                }

            } else {

                /*
                 * if it comes from the server, process it based on the object it's 
                 * defining, which will create client peers for anything that happened
                 * at the server side.
                 * 
                 * FIXME - use task finished to process any result (2nd parameter).
                 */
                notification = processEngineNotification(notification);
                Task task = null;
                switch (notification.getMessage()) {
                case Messages.TASK_STARTED:
                    listener.notifyTaskStarted((ITask) notification.get(0));
                    break;
                case Messages.TASK_FINISHED:
                    task = (Task) notification.get(0);
                    listener.notifyTaskFinished(task);
                    break;
                case Messages.TASK_INTERRUPTED:
                    task = (Task) notification.get(0);
                    listener.notifyTaskInterrupted(task);
                    break;
                case Messages.TASK_FAILED:
                    task = (Task) notification.get(0);
                    listener.notifyTaskFinished(task);
                    break;
                case Messages.TIME_TRANSITION:
                    IScale scale = ((IContext) notification.get(0)).getSubject().getScale();
                    ITransition transition = new Transition(scale, notification.getInt(3), true);
                    listener.notifyTimeTransition((IContext) notification.get(0), transition);
                    break;
                case Messages.ENGINE_BOOTING:
                    listener.notifyEngineBooting((IEngine) notification.get(0));
                    break;
                case Messages.ENGINE_AVAILABLE:
                    listener.notifyEngineOperational((IEngine) notification.get(0));
                    break;
                case Messages.ENGINE_STOPPED:
                    currentSessionId = null;
                    sessions.clear();
                    listener.notifyEngineStopped((IEngine) notification.get(0));
                    break;
                case Messages.ENGINE_EXCEPTION:
                    listener.notifyEngineFailure((IEngine) notification.get(0));
                    break;
                case Messages.ENGINE_STATUS:
                    Map<?, ?> map = (Map<?, ?>) notification.get(1);
                    listener.notifyEngineStatusUpdated((IEngine) notification.get(0), map);
                    // if (map.containsKey("network-changed")) {
                    // ((Network) Env.NETWORK).setChanged(true);
                    // }
                    break;

                // Introspection messages
                case Messages.CURRENT_PROVENANCE_GRAPH:

                    if (listener.getTaskIntrospector() != null) {
                        GraphVisualization gv = GraphVisualization.fromString(notification.get(0).toString());
                        gv.name = "Provenance (" + gv.vertexSet().size() + " nodes)";
                        task = (Task) (Client.this.getSession(notification.getSessionId()))
                                .getTask(notification.getTaskId());
                        listener.getTaskIntrospector().informationAvailable(task, Info.PROVENANCE_GRAPH, gv);
                    }
                    break;

                case Messages.CURRENT_WORKFLOW_GRAPH:
                    if (listener.getTaskIntrospector() != null) {
                        GraphVisualization df = GraphVisualization.fromString(notification.get(0).toString());
                        df.name = "Dataflow (" + df.vertexSet().size() + " nodes)";
                        task = (Task) (Client.this.getSession(notification.getSessionId()))
                                .getTask(notification.getTaskId());
                        listener.getTaskIntrospector().informationAvailable(task, Info.DATAFLOW_GRAPH, df);
                    }
                    break;
                }
            }
        }

        @Override
        public void stop(Object o) {
            // TODO Auto-generated method stub

        }

        @Override
        public boolean isStopped() {
            // TODO Auto-generated method stub
            return false;
        }

        @Override
        public void defineProgressSteps(int n) {
            // TODO Auto-generated method stub

        }

        @Override
        public void addProgress(int steps, String description) {
            // TODO Auto-generated method stub

        }

        @Override
        public void setLogLevel(int level) {
            // TODO Auto-generated method stub

        }

        @Override
        public int getDefaultLogLevel() {
            // TODO Auto-generated method stub
            return 0;
        }

        @Override
        public boolean hasErrors() {
            // TODO Auto-generated method stub
            return false;
        }

        @Override
        public long getTaskId() {
            // TODO Auto-generated method stub
            return -1;
        }

        @Override
        public ITaskIntrospector getTaskIntrospector() {
            // TODO Auto-generated method stub
            return null;
        }

        @Override
        public ISession getSession() {
            return Client.this.getSession(currentSessionId);
        }

        @Override
        public void report(String markdown, Object... attachments) {
            // TODO Auto-generated method stub

        }
    }

    /**
     * Add a monitor for notification of server status and spawn an appropriate
     * monitoring cycle. Monitor will get all notifications sent (as instances
     * of EngineNotification).
     * 
     * @param serverMonitor
     */
    public void startNetworkPolling() {

        if (future != null) {
            future.cancel(false);
            executor.shutdown();
        }

        executor = Executors.newScheduledThreadPool(24);
        future = executor
                .scheduleWithFixedDelay(new Runnable() {
                    @Override
                    public void run() {
                        synchronized (_monitoring) {
                            if (KLAB.NETWORK.getUrl() != null) {
                                listener.notifyNetworkStatus(NetUtilities
                                        .urlResponds(KLAB.NETWORK.getUrl() + "/"));
                            }
                        }
                    }
                }, 0, POLLING_PERIOD_MILLISECONDS, TimeUnit.MILLISECONDS);
    }

    public List<Context> getContexts() {

        List<Long> cids = new ArrayList<Long>();
        cids.addAll(contexts.keySet());
        Collections.sort(cids);
        List<Context> ret = new ArrayList<>();
        for (int i = cids.size() - 1; i >= 0; i--) {
            ret.add(contexts.get(cids.get(i)));
        }
        return ret;
    }

    public ContextViewer getContextViewer() {
        if (contextViewer == null) {
            contextViewer = new ContextViewer(viewerPort);
            try {
                contextViewer.start();
            } catch (ThinklabException e) {
                contextViewer = null;
                return null;
            }
            if (!contextViewer.startVisualizer(viewerPort)) {
                contextViewer = null;
                return null;
            }
        }
        return contextViewer;
    }

    /**
     * Launch viewer and if successful, show the passed context in it.
     * 
     * @param context
     * @return
     */
    public boolean showContext(IContext context) {
        if (getContextViewer() == null) {
            return false;
        }
        contextViewer.showContext(context);
        return true;

    }

    public boolean show(IObservation observation, IContext context, boolean setFocus) {
        if (contextViewer == null) {
            showContext(context);
        }
        contextViewer.showObservation(observation, true, setFocus);
        return true;
    }

    public INotification processEngineNotification(INotification notification) {

        if (notification != null && notification.getMessage() != null
                && !notification.getMessage().equals(Messages.ENGINE_STATUS)) {
            System.out.println("N: " + notification);
        }

        switch (notification.getMessage()) {

        case Messages.TASK_FAILED:
        case Messages.TASK_INTERRUPTED:
            ITask task = (ITask) notification.get(0);
            ((Task) task).setStatusFromMessage(notification.getMessage());
            engine.interruptTask(task.getTaskId());
            break;

        case Messages.TASK_FINISHED:
        case Messages.TASK_STARTED:
            Object arg = notification.size() > 1 ? notification.get(1) : null;
            task = (ITask) notification.get(0);
            ((Task) task).setStatusFromMessage(notification.getMessage());
            updateTask(notification.getSessionId(), task, arg);
            if (arg instanceof IContext) {
                registerContext((ContextInternal) arg).addTask((Task) task);
                listener.notifyContextAvailable(registerContext((IContext) arg));
            }
            break;

        case Messages.ENGINE_EXCEPTION:

            // TODO did nothing before.
            // engine.engineError(notification.getSessionId(), notification.size() > 0 ? notification.get(0)
            // : null);
            break;

        }

        return notification;
    }

    public void updateTask(String sessionId, ITask task, Object object) {

        Session session = getSession(sessionId);
        session.addTask(task);
        if (object instanceof IContext) {
            session.addContext((IContext) object);
        }
    }

    /**
     * Take the passed argument and if it's a context coming from the
     * engine, submit it to the correspondent client context, creating
     * if necessary; in all cases return the stable and unique context
     * instance for the client.
     * 
     * @param arg
     * @return
     */
    public Context registerContext(IContext arg) {

        if (arg instanceof Context) {
            return (Context) arg;
        }

        synchronized (contexts) {
            Context ret = contexts.get(arg.getId());
            if (ret == null) {
                ret = new Context((ContextInternal) arg);
                contexts.put(arg.getId(), ret);
            } else {
                ret.setContext((ContextInternal) arg);
            }
            return ret;
        }
    }

    public List<ITask> getTasks() {
        return getSession(currentSessionId).getTasks();
    }

    public Client(Listener listener) {
        _this = this;
        this.listener = listener;
    }

    public IMonitor getMonitor() {
        return monitor;
    }

    @Override
    public IModelingEngine getEngine() {
        return engine;
    }

    public void error(Throwable e) {
        listener.error(e, null);
    }

    public void info(String message) {
        listener.info(message, null);
    }

    public void warn(String message) {
        listener.warn(message, null);
    }

    /**
     * client boot phase 1: create the Env.XXX pieces, read configuration, read the upper ontologies
     * and ensure we have a sound Thinklab client environment. Build a network distribution for the
     * local engine and store it, but do not synchronize it yet.
    
     * @throws ThinklabException 
     */
    public void initialize() throws ThinklabException {

        KLAB.CLIENT = this;
        KLAB.PMANAGER = new ProjectManager();
        KLAB.MMANAGER = new KIMModelManager();
        KLAB.KM = new KnowledgeManager();
        KLAB.MFACTORY = new ModelFactory();

        if (KLAB.NETWORK == null) {
            KLAB.NETWORK = new Network();
        }
        serverDistribution = new NetworkedDistribution();
        serverDistribution.setListener(new SyncListener() {

            int n = 0;
            int d = 0;

            @Override
            public void transferFinished() {
                listener.info("Server synchronization complete", null);
            }

            @Override
            public void notifyDownloadCount(int downloadFilecount, int deleteFileCount) {
                n = downloadFilecount;
                d = deleteFileCount;
                listener
                        .info("Server synchronization: " + n + " files to download, " + d
                                + " to delete", null);
            }

            @Override
            public void beforeDownload(String file) {
                listener.info("downloading " + file + "...", null);
            }

            @Override
            public void beforeDelete(File localFile) {
                listener.info("deleting " + localFile + "...", null);
            }
        });

    }

    /**
     * client boot phase 2: authenticate and return the user we are. Synchronize any core projects
     * defined for our certificate.
     * 
     * Read the user certificate if it's there, and authenticate the user. If this step fails for
     * any reason, we alert the user if an invalid certificate was there, and move on with an
     * anonymous user.
     */
    public IUser authenticate() {

        File certificate = new File(KLAB.CONFIG.getDataPath() + File.separator + "im.cert");
        try {
            KLAB.CONFIG.getProperties()
                    .setProperty(IUser.THINKLAB_PUBLIC_KEY_PROPERTY, Resources.getEtcPath().toURI().toURL()
                            + "/pubring.gpg");
        } catch (Exception e1) {
            listener.error(e1, null);
        }

        try {
            this.user = new RESTUser(certificate);
            this.tclient = new ClientRESTController("http://127.0.0.1:" + ENGINE_PORT + "/"
                    + ENGINE_CONTEXT_PATH, user);
        } catch (Throwable e) {
            listener.error("Authentication failed: check access to network and certificate.", null);
            this.user = new RESTUser();
        }

        if (user.isAnonymous()) {
            listener.error("Invalid certificate or offline access: continuing anonymously.", null);
            user = new RESTUser();
        }

        KLAB.NAME = user.getUsername();

        /*
         * sync project assets. Workspace shouldn't really be null, but users are
         * creative and NPEs are ugly.
         */
        if (KLAB.WORKSPACE != null) {
            try {
                KLAB.WORKSPACE.synchronizeUserProjects(user, monitor);
            } catch (ThinklabException e) {
                listener.error(e, null);
            }
        }

        listener.notifyUser(user);

        return user;
    }

    /**
     * client boot phase 3: create the workspace and the synchronized knowledge and read it a into client-side model tree. 
     * Synchronize bookmarks and knowledge index. 
     * 
     * Errors and other source markers will be defined. If an engine hasn't been started, the functions will be unknown and
     * info markers will be displayed to notify that they cannot be validated.
     * @throws ThinklabException 
     */
    public List<INamespace> readWorkspace() throws ThinklabException {

        /*
         * If workspace hasn't already been set in the environment, see if we have one in properties.
         */
        if (KLAB.WORKSPACE == null) {
            String workspace = KLAB.CONFIG.getProperties().getProperty(KLAB.THINKLAB_WORKSPACE_PROPERTY);
            if (workspace != null) {
                File ws = new File(workspace);
                if (ws.exists() && ws.canWrite() && ws.canRead() && ws.isDirectory()) {
                    KLAB.WORKSPACE = new Workspace(ws);
                }
            }
        }

        /*
         * Setup model manager listener
         */
        KLAB.MFACTORY.addKnowledgeLifecycleListener(new IKnowledgeLifecycleListener() {

            @Override
            public void objectDefined(IModelObject o) {

                for (IModelObject object : KIM.getAllChildren(o)) {

                    /*
                    * TODO this should be done in the model manager, not here.
                    */
                    if (object.getAnnotations().size() > 0) {
                        processAnnotations(object);
                    }
                    for (Pair<Integer, String> zio : getDeprecation(object)) {
                        ((KIMNamespace) (object.getNamespace()))
                                .addWarning(new CompileWarning(object.getNamespace(), zio
                                        .getSecond(), zio.getFirst()));
                    }
                }
            }

            @Override
            public void namespaceDeclared(INamespace namespace) {
                BookmarkManager.get().removeAllForNamespace(namespace.getId());
            }

            @Override
            public void namespaceDefined(INamespace namespace) {
                // TODO Auto-generated method stub
            }
        });

        /*
         * read projects in current workspace.
         * TODO add previously synchronized ones unless already present in workspace.
         */
        List<INamespace> ret = new ArrayList<>();
        if (KLAB.WORKSPACE != null) {
            for (File pf : KLAB.WORKSPACE.getProjectLocations()) {
                KLAB.PMANAGER.registerProject(pf);
            }
            try {
                ret = KLAB.PMANAGER.load(false, KLAB.MFACTORY.getRootParsingContext());
            } catch (Throwable e) {
                KLAB.error(e);
            }
            listener.info("read " + ret.size() + " namespaces in " + KLAB.PMANAGER.getProjects().size()
                    + " projects.", null);
        }

        /*
         * setup project manager listener after first load.
         */
        KLAB.PMANAGER.addListener(new IProjectLifecycleListener() {

            @Override
            public void projectRegistered(IProject project) {
                try {
                    if (engine != null) {
                        engine.deployProject(project, true, false);
                    }
                } catch (ThinklabException e) {
                    Client.this.error(e);
                }

                /*
                 * only reload if the workspace didn't have it before. This skips the
                 * repeated load of projects we would load anyway.
                 */
                if (!KLAB.WORKSPACE.getProjectLocations().contains(project.getLoadPath())) {
                    ArrayList<INamespace> changed = new ArrayList<>();
                    try {
                        for (INamespace n : KLAB.PMANAGER
                                .load(false, KLAB.MFACTORY.getRootParsingContext())) {
                            changed.add(n);
                        }

                        if (changed.size() > 0) {
                            for (INamespace nss : changed) {
                                index.index(nss);
                            }
                            index.reindex();
                            listener.info(changed.size() + " namespaces reindexed", null);
                        }
                    } catch (ThinklabException e) {
                        Client.this.error(e);
                    }
                    listener.notifyNamespacesModified(changed, null);
                }
            }

            @Override
            public void namespaceDeleted(String ns, IProject project) {
                try {
                    KLAB.PMANAGER.load(false, KLAB.MFACTORY.getRootParsingContext());
                    BookmarkManager.get().removeAllForNamespace(ns);
                    listener.notifyNamespaceDeleted(ns, project);
                } catch (ThinklabException e) {
                    Client.this.error(e);
                }
            }

            @Override
            public void projectUnregistered(IProject project) {
                try {
                    if (engine != null) {
                        engine.undeployProject(project.getId());
                    }
                } catch (ThinklabException e) {
                    Client.this.error(e);
                }
                ArrayList<INamespace> changed = new ArrayList<>();
                try {
                    for (INamespace n : KLAB.PMANAGER.load(true, KLAB.MFACTORY.getRootParsingContext())) {
                        changed.add(n);
                    }

                    if (changed.size() > 0) {
                        for (INamespace nss : changed) {
                            index.index(nss);
                        }
                        index.reindex();
                        listener.info(changed.size() + " namespaces reindexed", null);
                    }
                } catch (ThinklabException e) {
                    Client.this.error(e);
                }
                listener.notifyNamespacesModified(changed, null);
            }

            @Override
            public void namespaceAdded(String ns, IProject project) {
                try {
                    for (INamespace n : KLAB.PMANAGER.load(false, KLAB.MFACTORY.getRootParsingContext())) {
                        listener.notifyNamespaceCreated(n.getId(), n.getProject());
                    }
                } catch (ThinklabException e) {
                    Client.this.error(e);
                }
            }

            @Override
            public void namespaceModified(String ns, IProject project) {
                ArrayList<INamespace> changed = new ArrayList<>();
                try {
                    for (INamespace n : KLAB.PMANAGER.load(false, KLAB.MFACTORY.getRootParsingContext())) {
                        changed.add(n);
                    }

                    if (changed.size() > 0) {
                        for (INamespace nss : changed) {
                            index.index(nss);
                        }
                        index.reindex();
                        listener.info(changed.size() + " namespaces reindexed", null);
                    }
                } catch (ThinklabException e) {
                    Client.this.error(e);
                }
                listener.notifyNamespacesModified(changed, KLAB.MMANAGER.getNamespace(ns));

            }

            @Override
            public void projectPropertiesModified(IProject project, File file) {

                ArrayList<INamespace> changed = new ArrayList<>();
                try {
                    for (INamespace n : KLAB.PMANAGER.load(true, KLAB.MFACTORY.getRootParsingContext())) {
                        changed.add(n);
                    }

                    if (changed.size() > 0) {
                        for (INamespace nss : changed) {
                            index.index(nss);
                        }
                        index.reindex();
                        listener.info(changed.size() + " namespaces reindexed", null);
                    }
                } catch (ThinklabException e) {
                    Client.this.error(e);
                }
                listener.notifyPropertiesChanged(project, file);
                listener.notifyNamespacesModified(changed, null);
            }

            @Override
            public void fileCreated(IProject project, File file) {
                // TODO notify
            }

            @Override
            public void fileDeleted(IProject project, File file) {
            }

            @Override
            public void fileModified(IProject project, File file) {
            }

            @Override
            public void onReload(boolean full) {
                // TODO Auto-generated method stub

            }
        });

        /*
         * open the knowledge index; record whether we need to reindex the whole thing.
         * TODO for now we just force full reindexing, just in case.
         */
        final boolean reindex = createKnowledgeIndex();

        if (reindex) {
            index.clear();
            for (INamespace ns : ret) {
                index.index(ns);
            }
            index.reindex();
        }

        try {
            // if anything happens here, we don't want it to stop the whole boot process.
            BookmarkManager.get().restoreBookmarks();
        } catch (Throwable e) {
            error(e);
        }

        return ret;
    }

    public List<INamespace> reloadWorkspace(boolean forceReload) throws ThinklabException {

        /*
         * TODO put this in a synchronized workspace job
         */

        List<INamespace> ret = new ArrayList<>();
        if (KLAB.WORKSPACE != null) {
            for (File pf : KLAB.WORKSPACE.getProjectLocations()) {
                KLAB.PMANAGER.registerProject(pf);
            }
            ret = KLAB.PMANAGER.load(forceReload, KLAB.MFACTORY.getRootParsingContext());
        }
        return ret;
    }

    /**
     * client boot phase 4: create the local engine, start it and have it log in to the network. Gather capabilities and 
     * service prototypes. Rebuild the model tree so that function calls can be validated.
     * 
     * Returns true unless an update was necessary, the engine was running when called, and attempts to stop it before
     * updating have failed.
     * 
     * @param forceSynchronization 
     * @throws ThinklabException 
     */
    public boolean startEngine(boolean forceSynchronization) throws ThinklabException {

        /*
         * get an appropriate local engine: development if available and not disabled, 
         * synchronized otherwise.
         * 
         * TODO configure context path and port through properties.
         */
        if (engine == null) {
            IModelingEngine localEngine = null;
            if (!LocalDevelopmentEngine.isAvailable()
                    || KLAB.getBooleanProperty("thinklab.client.nodev", false)) {
                /*
                 * we only sync if the feature has changed versions or there is no server. Sync will have
                 * no effect if we are using a local development distribution (set from properties).
                 */
                if (forceSynchronization || !serverDistribution.isComplete()) {
                    if (!syncServer(serverDistribution)) {
                        return false;
                    }
                }
                localEngine = new RemotelySynchronizedLocalEngine(tclient, serverDistribution, ENGINE_CONTEXT_PATH, ENGINE_PORT, monitor);
            } else {
                localEngine = new LocalDevelopmentEngine(tclient, monitor, ENGINE_CONTEXT_PATH, ENGINE_PORT);
            }

            KLAB.ENGINE = engine = localEngine;
        }

        /*
         * start default engine and open a session
         */
        if (engine.start()) {
            info("engine started: synchronizing local knowledge...");
            this.currentSessionId = engine.openSession();
            this.sessions.put(currentSessionId, new Session(currentSessionId));
            registerProjects();
            listener.notifyComponentsInitialized(engine);
            ((Engine) engine).startPolling(this.sessions.get(currentSessionId));
        }

        return true;
    }

    /**
     * Either boot() or its four independent phases should be called. In GUI environments, it may be
     * necessary to call the four phases in different order and/or in different lifecycle stages. 
     */
    @Override
    public void boot() throws ThinklabException {

        initialize();
        readWorkspace();
        authenticate();
        if (!startEngine(false)) {
            error(new ThinklabRuntimeException("Cannot upgrade the engine because it failed to stop before update. Please terminate the engine before attempting boot."));
            System.exit(0);
        }
        connectToThinklabNetwork();

    }

    public boolean connectToThinklabNetwork() {
        ((Network) KLAB.NETWORK).initialize();
        info("connected to " + KLAB.NETWORK.getNodes().size() + " active Thinklab nodes");
        user.setOnline(KLAB.NETWORK.isOnline() ? IUser.Status.ONLINE : IUser.Status.OFFLINE);
        return true;
    }

    /**
     * Call on a newly started engine.
     * @throws ThinklabException 
     */
    public void registerProjects() throws ThinklabException {
        for (IProject project : KLAB.PMANAGER.getProjects()) {
            engine.deployProject(project, true, false);
        }

        /*
         * send signal to load components and read capabilities again. Not necessarily 
         * elegant - should find a better way.
         */
        ((Engine) engine).initializeComponents();
    }

    @Override
    public void shutdown() {

    }

    protected void setSyncListener(NetworkedDistribution.SyncListener listener) {
        this.syncListener = listener;
    }

    protected boolean syncServer(final NetworkedDistribution sd) throws ThinklabException {

        /*
         * check if we have a new version or server is not available; if not, 
         * skip synchronization. We should be able to force server sync if requested
         * by user.
         */
        if (syncListener == null) {
            syncListener = new NetworkedDistribution.SyncListener() {

                int _total = 0;
                int _sofar = 0;

                @Override
                public void beforeDownload(String file) {
                    // _monitor.info("Updating server distribution: " + (_total - _sofar)
                    // + " files to download");
                    listener.info("downloading " + file, null);
                    _sofar++;
                    // monitor.worked(1);
                }

                @Override
                public void beforeDelete(File localFile) {
                    listener.info("deleting " + localFile, null);
                }

                @Override
                public void notifyDownloadCount(int downloadFilecount, int deleteFileCount) {
                    _total = downloadFilecount;
                    boolean ok = true;
                    if (downloadFilecount > 0) {
                        // ok = EmbeddedServer.ensureShutdown();
                    }
                    listener.info("Updating server distribution: " + downloadFilecount
                            + " files to download", null);
                }

                @Override
                public void transferFinished() {
                    listener.info("server distribution updated.", null);
                }
            };
        }
        if (!engine.stop()) {
            return false;
        }
        sd.setListener(syncListener);
        sd.sync();
        return true;
    }

    /**
     * Create the knowledge index; return true if there was no index, meaning we need
     * to reindex the full knowledge base.
     * 
     * @return
     * @throws ThinklabException
     */
    public boolean createKnowledgeIndex() throws ThinklabException {

        index = new KnowledgeIndex(getKnowledgeIndexName());
        index.addListener(new KnowledgeIndex.Listener() {

            @Override
            public void writeStarted(KnowledgeIndex knowledgeIndex, int ndocs) {
            }

            @Override
            public void onError(KnowledgeIndex knowledgeIndex, Throwable t) {
                error(t);
            }

            @Override
            public void indexReady(KnowledgeIndex knowledgeIndex) {
            }
        });

        return index.isNew();
    }

    /**
     * If this is changed to return a specific name, the knowledge index will be created there; 
     * by default, the index will be named thinklab. Having the same index means that two clients
     * cannot be used at the same time.
     * 
     * @return
     */
    protected String getKnowledgeIndexName() {
        return "thinklab";
    }

    /**
     * TODO make it part of the API after having extracted IKnowledgeIndex.
     * @return
     */
    public KnowledgeIndex getKnowledgeIndex() {
        return index;
    }

    @Override
    public void reindexKnowledge() {
        try {
            index.clear();
            int nn = 0;
            for (IProject p : KLAB.PMANAGER.getProjects()) {
                for (INamespace n : p.getNamespaces()) {
                    index.index(n);
                    nn++;
                }
            }
            index.reindex();

            /*
             * just notify if more than one namespace is reindexed. This is seen only when the GUI is
             * up, so after user action.
             */
            if (nn > 1) {
                listener.info((nn - 1) + " dependent namespaces were re-indexed", null);
            }
        } catch (ThinklabException e) {
            error(e);
        }

    }

    /**
     * Observe the given object at server side. If it's a subject, set the current subject to it. Task ID
     * generated incrementally.
     * 
     * @param o
     * @param currentTaskId
     * @param addToExisting
     * @return
     * @throws ThinklabException 
     */
    @Override
    public ITask observe(IDirectObserver observer, IExtent... forceScale) throws ThinklabException {

        if (observer.getNamespace().getId().equals(KLAB.MMANAGER.getLocalNamespace().getId())) {
            /*
             * TODO submit the object to the client first.
             */
            engine.submitObservation(observer, false);
        }

        ITask ret = engine.observe(observer, forceScale);
        ((Task) ret).setCommand(observer.getName());

        return ret;
    }

    @Override
    public Task observe(Object observable, ISubject context)
            throws ThinklabException {
        ITask ret = engine.observe(observable, context == null ? null
                : ((Subject) context).getContext(), context, getScenarios());
        ((Task) ret).setCommand(observable.toString());
        return (Task) ret;
    }

    public Collection<String> getScenarios() {
        List<String> ret = new ArrayList<>();
        for (INamespace n : currentScenarios) {
            ret.add(n.getId());
        }
        return ret;
    }

    public IContext getContext(long contextId) {
        if (contextId >= 0) {
            for (IContext ret : getContexts()) {
                if (ret.getId() == contextId)
                    return ret;
            }
        }
        return null;
    }

    protected void processAnnotations(IModelObject object) {

        for (IAnnotation a : object.getAnnotations()) {
            if (a.getId().equals("deprecated") || a.getId().equals("experimental")) {
                deprecated.put(object.getName(), a);
            }
        }
        BookmarkManager.get().processAnnotations(object, user);
    }

    /*
     * hard to resist returning something else.
     */
    public List<Pair<Integer, String>> getDeprecation(IModelObject object) {
        List<Pair<Integer, String>> ret = new ArrayList<Pair<Integer, String>>();
        getDeprecation(object, ret);
        return ret;
    }

    /*
     * TODO handle deprecated models and deprecated concept inheritance as well
     */
    private void getDeprecation(ILanguageObject object, List<Pair<Integer, String>> ret) {

        if (object instanceof IModel) {

            /*
             * check observables
             */
            for (IObservable obs : ((IModel) object).getObservables()) {
                if (obs != null && obs.getType() != null
                        && deprecated.containsKey(obs.getType().toString())) {
                    IAnnotation a = deprecated.get(obs.getType().toString());
                    ret.add(new Pair<Integer, String>(object.getFirstLineNumber(), "concept "
                            + obs.getType()
                            + " is "
                            + a.getId()
                            + (a.getParameters().get("value") == null ? "" : (": " + a.getParameters()
                                    .get("value").toString()))));
                }
            }

            /*
             * check observer
             */
            if (((IModel) object).getObserver() != null) {
                getDeprecation(((IModel) object).getObserver(), ret);
            }

        } else if (object instanceof IConditionalObserver) {

            /*
             * check every observable
             */
            for (Pair<IModel, IExpression> obs : ((IConditionalObserver) object).getModels()) {
                getDeprecation(obs.getFirst(), ret);
            }

        } else if (object instanceof IObserver) {

            /*
             * check observable
             */
            IObservable obs = ((IObserver) object).getObservable();
            if (obs != null && obs.getType() != null && deprecated.containsKey(obs.getType().toString())) {
                IAnnotation a = deprecated.get(obs.getType().toString());
                ret.add(new Pair<Integer, String>(object.getFirstLineNumber(), "concept "
                        + obs.getType()
                        + " is "
                        + a.getId()
                        + (a.getParameters().get("value") == null ? "" : (": " + a.getParameters()
                                .get("value").toString()))));
            }

        } else if (object instanceof IDependency) {

            /*
             * TODO check property
             */

            /*
             * check observable appropriately
             */
            IObservable obs = ((IDependency) object).getObservable();
            if (obs != null && obs.getType() != null && deprecated.containsKey(obs.getType().toString())) {
                IAnnotation a = deprecated.get(obs.getType().toString());
                ret.add(new Pair<Integer, String>(object.getFirstLineNumber(), "concept "
                        + obs.getType()
                        + " is "
                        + a.getId()
                        + (a.getParameters().get("value") == null ? "" : (": " + a.getParameters()
                                .get("value").toString()))));
            }
        }

        if (object instanceof IObservingObject) {

            /*
             * check dependencies
             */
            for (IDependency dep : ((IObservingObject) object).getDependencies()) {
                getDeprecation(dep, ret);
            }
        }
    }

    public Session getSession(String sessionId) {
        return (Session) sessions.get(sessionId);
    }

    public static Properties readProperties(File pfile) throws ThinklabIOException {
        Properties ret = new Properties();
        try (InputStream finp = new FileInputStream(pfile)) {
            ret.load(finp);
        } catch (Exception e) {
        }
        return ret;
    }

    public static void writeProperties(Properties properties, File pfile) {
        try (OutputStream finp = new FileOutputStream(pfile)) {
            properties.store(finp, null);
        } catch (Exception e) {
        }
    }

    /**
     * Return the value of property thinklab.client.<key>, or null if not there.
     * 
     * @param key
     * @return
     */
    public String getProperty(String key) {
        String prop = "thinklab.client." + key;
        return KLAB.CONFIG.getProperties().getProperty(prop);
    }

    /**
     * Set the thinklab.client.<key> property, persist it and call the callback for processing.
     * 
     * @param key
     * @param value
     */
    public void setProperty(String key, String value) {
        String prop = "thinklab.client." + key;
        KLAB.CONFIG.getProperties().setProperty(prop, value);
        KLAB.CONFIG.persistProperties();
        listener.notifyClientProperty(key, value);
    }

    /**
     * Intercept and adapt to any configuration change.
     * 
     * @param key
     * @param value
     */
    public void onPropertyChange(String key, String value) {

        if (key.equals("workspace")) {
            try {
                for (IProject p : KLAB.PMANAGER.getProjects()) {
                    KLAB.PMANAGER.unregisterProject(p.getId());
                }
                listener.info("registering workspace " + value, null);
                File workspace = new File(value);
                if (!workspace.exists() || !workspace.isDirectory()) {
                    listener.warn("workspace " + workspace + " does not exist or is not a directory", null);
                }
                KLAB.WORKSPACE = new Workspace(workspace);
                for (File pf : KLAB.WORKSPACE.getProjectLocations()) {
                    KLAB.PMANAGER.registerProjectDirectory(pf);
                }
                ArrayList<String> skip = new ArrayList<String>();
                for (IProject p : KLAB.PMANAGER.getProjects()) {
                    skip.add(p.getId());
                }
                // TODO check - did nothing before.
                // for (File projectDir : engine.synchronizeUserProjects(skip)) {
                // Env.PMANAGER.registerProject(projectDir);
                // // _syncProjectFiles.add(projectDir);
                // }
                listener.info("reading " + KLAB.PMANAGER.getProjects().size() + " local projects...", null);
                List<INamespace> nss = KLAB.PMANAGER.load(false, KLAB.MFACTORY.getRootParsingContext());
                listener.info("read " + nss.size() + " namespaces in " + KLAB.PMANAGER.getProjects().size()
                        + " projects.", null);
            } catch (ThinklabException e) {
                error(e);
            }
        }
    }

    @Override
    public IWorkspace getWorkspace() {
        return KLAB.WORKSPACE;
    }

    public void upgradeEngine() throws ThinklabException {
        // TODO do nothing unless we have a remotely synchronized engine somewhere.
        // TODO stop server if running
        serverDistribution.sync();
        // TODO restart if it was running, leaving in the same slot it was in.
    }

    @Override
    public void bookmark(IModelObject modelObject, String name, String description)
            throws ThinklabIOException {
        Bookmark bookmark = new Bookmark(modelObject, name, description);
        BookmarkManager.get().addPersistentBookmark(bookmark);
        listener.notifyBookmarkAdded(bookmark);
    }

    public void startWebServices(int port) {

        if (NetUtilities.portAvailable(port)) {

            File wdir = null;
            try {
                wdir = Resources.getWarPath();
            } catch (Exception e1) {
                error(new ThinklabIOException("could not find or extract web resources for map viewer."));
            }
            if (wdir == null || !(wdir.exists() && wdir.canRead() && wdir.isDirectory()))
                return;

            // System.out.println("starting web server on " + wdir);
            WebServer ws = new WebServer(port, wdir.toString());
            webServers.put(port, ws);
            try {
                ws.start();
                Thread.sleep(3000);
            } catch (Exception e) {
                webServers.remove(port);
            }
        }
    }

    public WebServer getWebServer(int port) {
        return webServers.get(port);
    }

    @Override
    public void run(File scriptFile) {
        // TODO Auto-generated method stub

    }

    @Override
    public Collection<INamespace> setScenario(INamespace element, boolean active) {

        if (active) {

            for (String s : element.getDisjointNamespaces()) {
                INamespace dn = KLAB.MMANAGER.getNamespace(s);
                if (dn != null)
                    currentScenarios.remove(dn);
            }
            currentScenarios.add(element);

        } else {
            currentScenarios.remove(element);
        }

        listener.scenariosChanged(currentScenarios);

        return currentScenarios;
    }

    @Override
    public void resetScenarios() {
        currentScenarios.clear();
        listener.scenariosChanged(currentScenarios);
    }

    @Override
    public List<INotification> getNotifications(ITask task) {
        List<INotification> ret = new ArrayList<>();
        for (INotification n : getSession(task.getSessionId()).getNotifications(false)) {
            if (n.getTaskId() == task.getTaskId()) {
                ret.add(n);
            }
        }
        return ret;
    }

    /**
     * Yup, a client has a user.
     * 
     * @return
     */
    @Override
    public IUser getUser() {
        return user;
    }

    @Override
    public ISession getCurrentSession() {
        return getSession(currentSessionId);
    }

    public List<Map<String, Object>> importData(File file) throws ThinklabException {
        return engine.importObservations(file);
    }

    public List<IObservationMetadata> queryObservations(String text) throws ThinklabException {
        return engine.queryObservations(text, false);
    }

    public IDirectObserver retrieveObservation(IObservationMetadata queryResult) throws ThinklabException {
        if (queryResult.getNodeId().equals(KLAB.NAME)) {
            return engine.retrieveObservation(queryResult.getName(), queryResult.getNodeId());
        }

        INode node = KLAB.NETWORK.getNode(queryResult.getNodeId());
        if (node != null) {
            return node.retrieveObservation(queryResult.getName(), queryResult.getNodeId());
        }

        return null;
    }

    public void stopEngine() throws ThinklabException {
        ((Engine) engine).pausePolling();
        engine.stop();
        sessions.clear();
        currentSessionId = null;
        info("engine stopped.");
    }
}
