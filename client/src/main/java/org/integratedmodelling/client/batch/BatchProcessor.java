/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.client.batch;

import java.io.File;
import java.util.Collection;
import java.util.Map;
import java.util.Set;

import org.integratedmodelling.api.auth.IUser;
import org.integratedmodelling.api.engine.IEngine;
import org.integratedmodelling.api.modelling.INamespace;
import org.integratedmodelling.api.modelling.scheduling.ITransition;
import org.integratedmodelling.api.monitoring.ITaskIntrospector;
import org.integratedmodelling.api.project.IProject;
import org.integratedmodelling.api.runtime.IClient;
import org.integratedmodelling.api.runtime.IContext;
import org.integratedmodelling.api.runtime.ITask;
import org.integratedmodelling.api.ui.IBookmark;
import org.integratedmodelling.client.Client;

/**
 * A special client intended to be instantiated and run once, then exit. Uses stdin/out for all
 * I/O and is optimized for running Groovy scripts. Client-launcher will instantiate one of these
 * and use it to serve any request if there are parameters on the command line.
 * 
 * @author Ferd
 *
 */
public class BatchProcessor {

    IClient client;

    class Listener implements IClient.Listener {
        @Override
        public void notifyUser(IUser user) {
            // TODO Auto-generated method stub

        }

        @Override
        public void debug(Object o, ITask task) {
            // TODO Auto-generated method stub

        }

        @Override
        public void error(Object error, ITask task) {
            // TODO Auto-generated method stub

        }

        @Override
        public void info(Object info, ITask task) {
            // TODO Auto-generated method stub

        }

        @Override
        public void warn(Object o, ITask task) {
            // TODO Auto-generated method stub

        }

        @Override
        public void notifyEngineBooting(IEngine engine) {
            // TODO Auto-generated method stub

        }

        @Override
        public void notifyEngineOperational(IEngine engine) {
            // TODO Auto-generated method stub

        }

        @Override
        public void notifyEngineFailure(IEngine engine) {
            // TODO Auto-generated method stub

        }

        @Override
        public void notifyTaskStarted(ITask task) {
            // TODO Auto-generated method stub

        }

        @Override
        public void notifyTaskFinished(ITask task) {
            // TODO Auto-generated method stub

        }

        @Override
        public void notifyTaskInterrupted(ITask task) {
            // TODO Auto-generated method stub

        }

        @Override
        public void notifyContextAvailable(IContext context) {
            // TODO Auto-generated method stub

        }

        @Override
        public void notifyTimeTransition(IContext context, ITransition transition) {
            // TODO Auto-generated method stub

        }

        @Override
        public void notifyClientProperty(String substring, String property) {
            // TODO Auto-generated method stub

        }

        @Override
        public void notifyNamespaceCreated(String namespaceId, IProject project) {
            // TODO Auto-generated method stub

        }

        @Override
        public void notifyNamespaceDeleted(String namespaceId, IProject project) {
            // TODO Auto-generated method stub

        }

        @Override
        public void notifyPropertiesChanged(IProject project, File file) {
            // TODO Auto-generated method stub

        }

        @Override
        public void scenariosChanged(Set<INamespace> currentScenarios) {
            // TODO Auto-generated method stub

        }

        @Override
        public void notifyEngineStatusUpdated(IEngine engine, Map<?, ?> status) {
            // TODO Auto-generated method stub

        }

        @Override
        public void notifyEngineStopped(IEngine engine) {
            // TODO Auto-generated method stub

        }

        @Override
        public ITaskIntrospector getTaskIntrospector() {
            // TODO Auto-generated method stub
            return null;
        }

        @Override
        public void notifyNamespacesModified(Collection<INamespace> namespaces, INamespace root) {
            // TODO Auto-generated method stub

        }

        @Override
        public void notifyComponentsInitialized(IEngine engine) {
            // TODO Auto-generated method stub

        }

        @Override
        public void notifyNetworkStatus(boolean urlResponds) {
            // TODO Auto-generated method stub

        }

        @Override
        public void notifyEngineInitializing(IEngine engine) {
            // TODO Auto-generated method stub

        }

        @Override
        public void notifyBookmarkAdded(IBookmark bookmark) {
            // TODO Auto-generated method stub

        }
    }

    public BatchProcessor() {
        this.client = new Client(new Listener());
    }

}
