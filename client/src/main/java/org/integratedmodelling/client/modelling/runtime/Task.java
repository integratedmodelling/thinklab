/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.client.modelling.runtime;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.integratedmodelling.api.engine.IEngine;
import org.integratedmodelling.api.monitoring.IMonitor;
import org.integratedmodelling.api.monitoring.IMonitorable;
import org.integratedmodelling.api.monitoring.INotification;
import org.integratedmodelling.api.monitoring.Messages;
import org.integratedmodelling.api.runtime.IContext;
import org.integratedmodelling.api.runtime.ITask;
import org.integratedmodelling.common.visualization.GraphVisualization;

public class Task implements ITask, IMonitorable {

    Context             _context;
    IEngine             _server;
    Status              _status;
    String              _command;
    String              _description;
    long                _id;
    long                _started;
    long                _finished;
    IMonitor            _monitor;
    String              _sessionId;
    boolean             _temporal;

    GraphVisualization  _provenance    = null;
    GraphVisualization  _workflow      = null;

    List<INotification> _notifications = new ArrayList<INotification>();

    private String checkNull(Object o) {
        return o == null ? null : o.toString();
    }

    public Task(long taskId) {
        _id = taskId;
        _description = "";
        _finished = new Date().getTime();
        _status = Status.FINISHED;
    }

    public Task(Map<?, ?> res) {
        _id = ((Number) res.get("id")).longValue();
        _command = checkNull(res.get("command"));
        _description = checkNull(res.get("description"));
        _status = Status.valueOf(res.get("status").toString());
        _started = ((Number) res.get("start-time")).longValue();
        _sessionId = checkNull(res.get("session-id"));
        _finished = ((Number) res.get("end-time")).longValue();
        _temporal = res.containsKey("temporal") && res.get("temporal").equals("true");
    }

    @Override
    public Status getStatus() {
        return _status;
    }

    public void setStatus(Status status) {
        _status = status;
    }

    @Override
    public long getTaskId() {
        return _id;
    }

    @Override
    public String getSessionId() {
        return _sessionId;
    }

    @Override
    public String getCommand() {
        return _command;
    }

    @Override
    public String getDescription() {
        return _description;
    }

    @Override
    public void interrupt() {
        // TODO send to server

    }

    public void setCommand(String command) {
        _command = command;
    }

    @Override
    public IContext getContext() {
        return _context;
    }

    @Override
    public IContext finish() {

        for (;;) {
            synchronized (_status) {
                if (_status == Status.FINISHED || _status == Status.ERROR) {
                    break;
                }
            }
            try {
                Thread.sleep(300);
            } catch (InterruptedException e) {
                _status = Status.INTERRUPTED;
                Thread.currentThread().interrupt();
            }
        }
        return _context;
    }

    @Override
    public long getStartTime() {
        return _started;
    }

    @Override
    public long getEndTime() {
        return _finished;
    }

    @Override
    public String toString() {
        return "[T] " + _description + " [id=" + _id + "; " + _status.name().toLowerCase() + "]";
    }

    @Override
    public void setMonitor(IMonitor monitor) {
        _monitor = monitor;
    }

    @Override
    public IMonitor getMonitor() {
        return _monitor;
    }

    /**
     * Check after a task reports finish or interruption - if true, we have
     * ran our temporal transitions, and no further observations should be allowed.
     * 
     * @return
     */
    public boolean isTemporal() {
        return _temporal;
    }

    public void setStatusFromMessage(String message) {

        switch (message) {
        case Messages.TASK_FAILED:
            _status = Status.ERROR;
            break;
        case Messages.TASK_FINISHED:
            _status = Status.FINISHED;
            break;
        case Messages.TASK_INTERRUPTED:
            _status = Status.INTERRUPTED;
            break;
        case Messages.TASK_STARTED:
            _status = Status.RUNNING;
            break;
        }

    }

    public void merge(Task task) {
        _finished = task._finished;
        _notifications.addAll(task._notifications);
    }
}
