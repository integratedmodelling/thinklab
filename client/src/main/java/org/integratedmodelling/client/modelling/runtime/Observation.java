/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.client.modelling.runtime;

import org.integratedmodelling.api.knowledge.IKnowledge;
import org.integratedmodelling.api.modelling.IDirectObservation;
import org.integratedmodelling.api.modelling.IObservable;
import org.integratedmodelling.api.modelling.IObservation;
import org.integratedmodelling.api.modelling.IProcess;
import org.integratedmodelling.api.modelling.IScale;
import org.integratedmodelling.api.modelling.IState;
import org.integratedmodelling.api.modelling.ISubject;

public abstract class Observation implements IObservation {

    protected IObservable        _observable;
    protected String             _path;
    protected String             _label;
    protected Context            _context;
    protected IDirectObservation _parent;
    protected IScale             _scale;

    /*
     * call it to establish the context on a top-level subject. Will set the context
     * and the parent subject on each child observation recursively.
     */
    public void contextualize(Context context) {
        contextualize(context, null);
    }

    @Override
    public IKnowledge getType() {
        return getObservable().getType();
    }

    protected void contextualize(Context context, ISubject parent) {

        _context = context;
        _parent = parent;
        if (this instanceof Subject) {
            for (ISubject s : ((Subject) this).getSubjects()) {
                ((Subject) s).contextualize(context, (ISubject) this);
            }
            for (IState s : ((Subject) this).getStates()) {
                ((State) s).contextualize(context, (ISubject) this);
            }
            for (IProcess s : ((Subject) this).getProcesses()) {
                ((Process) s).contextualize(context, (ISubject) this);
            }
        } /* else if (this instanceof State) {
            ((State) this).createViews();

          } */
    }

    @Override
    public IScale getScale() {
        return _scale;
    }

    /*
     * automatic unique ID to create unambiguous observation paths.
     */
    protected String _id;

    public String getPath() {

        Observation parent = getParentObservation();
        String ret = parent == null ? "" : getPathId();

        // we don't want the root ID in the path.
        while (parent != null && parent.getParentObservation() != null) {
            ret = parent.getPathId() + "/" + ret;
            parent = parent.getParentObservation();
        }

        return "/" + ret;
    }

    protected Observation getParentObservation() {
        if (this instanceof State)
            return (Observation) ((State) this).getSubject();
        if (this instanceof Subject)
            return (Observation) ((Subject) this).getParent();
        if (this instanceof Relationship)
            return (Observation) ((Relationship) this).getParent();
        return null;
    }

    @Override
    public IObservable getObservable() {
        return _observable;
    }

    public String getPathId() {
        return _id;
    }

    public String getLabel() {
        return _label;
    }

    @Override
    public boolean equals(Object obj) {
        return obj instanceof Observation && _id.equals(((Observation) obj)._id);
    }

    @Override
    public int hashCode() {
        return _id.hashCode();
    }

}
