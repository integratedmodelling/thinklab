/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.client.modelling.runtime;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.integratedmodelling.api.engine.IEngine;
import org.integratedmodelling.api.engine.IModelingEngine;
import org.integratedmodelling.api.metadata.IMetadata;
import org.integratedmodelling.api.modelling.IDirectObservation;
import org.integratedmodelling.api.modelling.IMeasuringObserver;
import org.integratedmodelling.api.modelling.IObservable;
import org.integratedmodelling.api.modelling.IObserver;
import org.integratedmodelling.api.modelling.IScale;
import org.integratedmodelling.api.modelling.IScale.Index;
import org.integratedmodelling.api.modelling.IScale.Locator;
import org.integratedmodelling.api.modelling.IState;
import org.integratedmodelling.api.modelling.scheduling.ITransition;
import org.integratedmodelling.api.modelling.storage.IStorage;
import org.integratedmodelling.api.modelling.visualization.IViewport;
import org.integratedmodelling.api.space.ISpatialExtent;
import org.integratedmodelling.api.time.ITemporalExtent;
import org.integratedmodelling.client.Client;
import org.integratedmodelling.common.configuration.KLAB;
import org.integratedmodelling.common.engine.JSONdeserializer;
import org.integratedmodelling.common.model.runtime.Scale;
import org.integratedmodelling.common.network.Endpoints;
import org.integratedmodelling.common.storage.AbstractStorage;
import org.integratedmodelling.common.utils.CamelCase;
import org.integratedmodelling.common.utils.StringUtils;
import org.integratedmodelling.common.visualization.ColorMap;
import org.integratedmodelling.common.visualization.Histogram;
import org.integratedmodelling.common.vocabulary.NS;
import org.integratedmodelling.common.vocabulary.Observable;
import org.integratedmodelling.exceptions.ThinklabException;
import org.integratedmodelling.exceptions.ThinklabRuntimeException;

/**
 * The client side of IState can have different views, one per visualizable data slice. The main IState contains
 * the IScale.Index that specifies the current default view. Any view below this correspond to a different index,
 * normally for dimensions other than space (visualized in one shot) or time (visualized one state at a time). If
 * there are views below it, these are still IStates with a different index; only the leaf views are visualizable.
 * 
 * 
 * @author Ferd
 *
 */
public class State extends Observation implements IState {

    List<State>    _views         = new ArrayList<State>();
    Histogram      _histogram;
    ColorMap       _colormap;
    String         _statsAcquired = null;
    IObserver      _observer;
    String         _mediaType;
    IStorage<?>    _storage;
    boolean        _isDynamic;
    boolean        _isConstant;
    private Object _latestValue;

    class DummyStorage extends AbstractStorage<Object> {

        public DummyStorage(IScale scale, boolean isDynamic) {
            super(scale, isDynamic);
        }

        @Override
        public Object get(int index) {
            return State.this.getValue(index);
        }

        @Override
        public Class<?> getDataClass() {
            return Object.class;
        }

        @Override
        public void flush(ITransition incomingTransition) {
        }

        @Override
        public double getMin() {
            return Double.NaN;
        }

        @Override
        public double getMax() {
            return Double.NaN;
        }

        @Override
        public Object getLatestAggregatedValue() {
            // TODO Auto-generated method stub
            return null;
        }

    }

    public State(Map<?, ?> map) {

        _observer = (IObserver) map.get("observer");
        _observable = (IObservable) map.get("observable");
        if (_observable.getObservationType().is(KLAB.c(NS.PRESENCE_OBSERVATION))
                || _observable.getObservationType().is(KLAB.c(NS.RATIO_OBSERVATION))
                || _observable.getObservationType().is(KLAB.c(NS.UNCERTAINTY_OBSERVATION))) {
            /*
             * these observers create concepts that usually have their observation type in the concept name.
             */
            _label = StringUtils.capitalize(CamelCase.toLowerCase(_observable.getType().getLocalName(), ' '));

        } else {
            _label = StringUtils.capitalize(
            /* CamelCase
            .toLowerCase(_observable.getObservationType().getLocalName(), ' ')
            + " of "
            + */CamelCase.toLowerCase(_observable.getType().getLocalName(), ' '));

            if (_observable.getTraitType() != null) {
                _label += " [by "
                        + StringUtils.capitalize(CamelCase.toLowerCase(_observable.getTraitType()
                                .getLocalName(), ' '))
                        + "]";
            } else if (_observable.getObserver() instanceof IMeasuringObserver) {
                _label += " [" + ((IMeasuringObserver) _observable.getObserver()).getUnit() + "]";
            }
        }
        if (map.containsKey("internal-id")) {
            _id = map.get("internal-id").toString();
        }
        if (map.containsKey("latest-value")) {
            _latestValue = map.get("latest-value");
        }

        /*
         * Point of view. TODO - smarter way to suggest that in a label?
         */
        if (((Observable) _observable).getObservingSubjectId() != null) {
            _label = ((Observable) _observable).getObservingSubjectId() + ": " + _label;
        }

        _isDynamic = map.containsKey("dynamic") && map.get("dynamic").equals("true");
        _isConstant = map.containsKey("constant") && map.get("constant").equals("true");
    }

    private void getStats(Iterable<Locator> locators) throws ThinklabException {

        String locationSig = Scale.locatorsAsText(locators);

        if (_statsAcquired == null || !_statsAcquired.equals(locationSig)) {

            _statsAcquired = locationSig;

            Map<?, ?> res = _context._engine
                    .getStatistics(_context.getId(), _context._lastUpdate, getPath(), locators);

            if (res.containsKey(IMetadata.STATE_HISTOGRAM)) {
                _histogram = Histogram.fromString(res.get(IMetadata.STATE_HISTOGRAM).toString());
            }
            if (res.containsKey(IMetadata.STATE_COLORMAP)) {
                _colormap = ColorMap.fromString(res.get(IMetadata.STATE_COLORMAP).toString());
            }
        }

    }

    public String getId() {
        return _id;
    }

    @Override
    public String getLabel() {
        if (_latestValue != null) {
            String ret = getObservable().getLocalName() + " = " + _latestValue;
            if (_observer instanceof IMeasuringObserver) {
                ret += " " + ((IMeasuringObserver) _observer).getUnit();
            }
            return ret;
        }
        return super.getLabel();
    }

    @Override
    public IScale getScale() {
        return _parent.getScale();
    }

    @Override
    public long getValueCount() {
        return getScale().getMultiplicity();
    }

    @Override
    public IObserver getObserver() {
        return _observer;
    }

    @Override
    public boolean isSpatiallyDistributed() {
        return getScale().getSpace() != null && getScale().getSpace().getMultiplicity() > 1;
    }

    @Override
    public boolean isTemporallyDistributed() {
        return getScale().getTime() != null && getScale().getTime().getMultiplicity() > 1;
    }

    @Override
    public ISpatialExtent getSpace() {
        return getScale().getSpace();
    }

    @Override
    public ITemporalExtent getTime() {
        return getScale().getTime();
    }

    public IDirectObservation getSubject() {
        return _parent;
    }

    @Override
    public boolean isTemporal() {
        return getTime() != null;
    }

    @Override
    public boolean isSpatial() {
        return getSpace() != null;
    }

    @Override
    public IMetadata getMetadata() {
        // TODO Auto-generated method stub
        return null;
    }

    public boolean isDynamic() {
        return _isDynamic;
    }

    public Object getLatestValue() {
        return _latestValue;
    }

    @Override
    public Object getValue(int index) {

        try {
            return _context._engine.getStateValue(_context.getId(), index, getPath());
        } catch (ThinklabException e) {
            throw new ThinklabRuntimeException(e);
        }
    }

    @Override
    public Iterator<Object> iterator(Index index) {
        // TODO Auto-generated method stub
        return null;
    }

    public IEngine getServer() {
        return getContext().getServer();
    }

    public Context getContext() {
        return Client.get().registerContext(((Subject) _parent).getContext());
    }

    /**
     * Return the server URL that will return the visualization for this state in the passed type. If 
     * the state cannot be visualized (e.g. because only its views are visualizable) return null without
     * error.
     * 
     * FIXME because we don't use the engine's controller, we need to manually pass the session and to leave
     * the context endpoing unprotected (so that the JS inside the geomapper's browser does not need to deal
     * with headers and stuff). No big deal, but we should really fix this. 
     * 
     * @return
     */
    public String getMediaURL(String mimeType, IViewport viewport, Iterable<Locator> locators) {
        Context context = (Context) ((Subject) _parent).getContext();
        IModelingEngine engine = KLAB.CLIENT.getEngine();
        return JSONdeserializer
                .getURL(engine.getUrl(), Endpoints.CONTEXT, "cmd", "get-media", "ctx", context
                        .getId(), "session", KLAB.CLIENT.getCurrentSession()
                                .getId(), "path", getPath(), "type", mimeType, "index", Scale
                                        .locatorsAsText(locators), "viewport", viewport.toString());
    }

    /**
     * Return the server URL that will return the value of this state at the given index. If 
     * the state cannot be visualized (e.g. because only its views are visualizable) return null without
     * error.
     *     
     * FIXME because we don't use the engine's controller, we need to manually pass the session and to leave
     * the context endpoing unprotected (so that the JS inside the geomapper's browser does not need to deal
     * with headers and stuff). No big deal, but we should really fix this. 
     * 
     * @return
     */
    public String getValueURL(Iterable<Locator> locators) {

        Context context = (Context) ((Subject) _parent).getContext();
        IModelingEngine engine = KLAB.CLIENT.getEngine();
        return JSONdeserializer
                .getURL(engine.getUrl(), Endpoints.CONTEXT, "cmd", "get-value", "ctx", context
                        .getId(), "session", KLAB.CLIENT.getCurrentSession()
                                .getId(), "path", getPath(), "index", Scale.locatorsAsText(locators));
    }

    /**
     * 
     * FIXME because we don't use the engine's controller, we need to manually pass the session and to leave
     * the context endpoing unprotected (so that the JS inside the geomapper's browser does not need to deal
     * with headers and stuff). No big deal, but we should really fix this. 
     * 
     * @param mimeType
     * @param viewport
     * @param locators
     * @return
     */
    public String getStatsURL(String mimeType, IViewport viewport, Iterable<Locator> locators) {
        Context context = (Context) ((Subject) _parent).getContext();
        IModelingEngine engine = KLAB.CLIENT.getEngine();
        return JSONdeserializer
                .getURL(engine.getUrl(), Endpoints.CONTEXT, "cmd", "get-data-summary", "ctx", context
                        .getId(), "session", KLAB.CLIENT.getCurrentSession()
                                .getId(), "path", getPath(), "index", Scale.locatorsAsText(locators));
    }

    /**
     * Return the views under this state. If views are present, this state cannot be visualized.
     * 
     * @return
     */
    public List<State> getViews() {
        return _views;
    }

    public Histogram getHistogram(Iterable<Locator> locators) throws ThinklabException {
        getStats(locators);
        return _histogram;
    }

    public ColorMap getColormap(Iterable<Locator> locators) throws ThinklabException {
        getStats(locators);
        return _colormap;
    }

    @Override
    public String toString() {
        return getLabel() + " (" + getObservable().getType() + ")";
    }

    @Override
    public IStorage<?> getStorage() {
        if (_storage == null) {
            _storage = new DummyStorage(getScale(), _isDynamic);
        }
        return _storage;
    }

    @Override
    public boolean isConstant() {
        return _isConstant;
    }

}
