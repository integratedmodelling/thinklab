/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.client.modelling.runtime;

import java.util.Collection;
import java.util.Map;

import org.integratedmodelling.api.knowledge.IProperty;
import org.integratedmodelling.api.metadata.IMetadata;
import org.integratedmodelling.api.modelling.IActiveProcess;
import org.integratedmodelling.api.modelling.IModel;
import org.integratedmodelling.api.modelling.INamespace;
import org.integratedmodelling.api.modelling.IObservable;
import org.integratedmodelling.api.modelling.IState;
import org.integratedmodelling.api.modelling.ISubject;
import org.integratedmodelling.api.modelling.agents.IAgentState;
import org.integratedmodelling.api.modelling.agents.ICollision;
import org.integratedmodelling.api.modelling.agents.IObservationGraphNode;
import org.integratedmodelling.api.modelling.scheduling.ITransition;
import org.integratedmodelling.api.monitoring.IMonitor;
import org.integratedmodelling.api.time.ITimePeriod;
import org.integratedmodelling.exceptions.ThinklabException;

public class Process extends Observation implements IActiveProcess {

    ISubject subject;
    String   name;

    @Override
    public IMetadata getMetadata() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public ICollision detectCollision(IObservationGraphNode myAgentState, IObservationGraphNode otherAgentState) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public boolean doesThisCollisionAffectYou(IAgentState agentState, ICollision collision) {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public ITransition reEvaluateStates(ITimePeriod timePeriod) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public ITransition performTemporalTransitionFromCurrentState() throws ThinklabException {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public Map<IProperty, IState> getObjectStateCopy() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public void setMonitor(IMonitor monitor) {
        // TODO Auto-generated method stub

    }

    @Override
    public IMonitor getMonitor() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public ISubject getSubject() {
        return subject;
    }

    @Override
    public INamespace getNamespace() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public String getId() {
        // TODO Auto-generated method stub
        return name;
    }

    @Override
    public Collection<IState> getStates() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public IState getState(IObservable observable, Object... data) throws ThinklabException {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public IState getStaticState(IObservable observable) throws ThinklabException {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public IModel getModel() {
        // TODO Auto-generated method stub
        return null;
    }

}
