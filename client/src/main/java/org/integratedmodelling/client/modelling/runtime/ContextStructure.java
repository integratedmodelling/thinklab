/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.client.modelling.runtime;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.integratedmodelling.api.knowledge.IKnowledge;
import org.integratedmodelling.api.modelling.IObservable;
import org.integratedmodelling.api.modelling.IObservation;
import org.integratedmodelling.api.modelling.IProcess;
import org.integratedmodelling.api.modelling.IRelationship;
import org.integratedmodelling.api.modelling.IScale;
import org.integratedmodelling.api.modelling.IState;
import org.integratedmodelling.api.modelling.ISubject;
import org.integratedmodelling.common.utils.NameGenerator;
import org.integratedmodelling.common.vocabulary.Observable;

/**
 * A more orderly view of a subject's structure made for display and browsing. Uses 
 * folders to group subjects and relationships meaningfully. Uses the client versions
 * of observations directly. Keeps track of parent-child relationships so we don't have to.
 * 
 * @author ferdinando.villa
 *
 */
public class ContextStructure {

    Subject             subject;
    Map<String, Object> parents = new HashMap<>();

    /*
     * a folder is an observation of a group of direct observations. It may 
     * hold relationships, in which case they may be of different types and
     * the observable will be null.
     */
    public class Folder extends ArrayList<IObservation> implements IObservation {

        private static final long serialVersionUID = -6948404167166780198L;
        IObservable               observable;
        String                    id;
        boolean                   isRelationship   = false;

        Folder(IKnowledge observable, ISubject parent) {
            if (observable == null) {
                this.isRelationship = true;
            } else {
                this.observable = new Observable(observable);
            }
            this.id = NameGenerator.shortUUID();
            parents.put(this.id, parent);
        }

        @Override
        public IObservable getObservable() {
            return observable;
        }

        public boolean isRelationshipFolder() {
            return isRelationship;
        }

        public String getId() {
            return id;
        }

        @Override
        public IScale getScale() {
            return null;
        }

        @Override
        public boolean equals(Object o) {
            return o instanceof Folder && ((Folder) o).id.equals(id);
        }

        @Override
        public int hashCode() {
            return id.hashCode();
        }

        @Override
        public IKnowledge getType() {
            // TODO Auto-generated method stub
            return null;
        }
    }

    public ContextStructure(Subject subject) {
        this.subject = subject;
        parents.put(subject.getPath(), this);
    }

    public IObservation[] getChildren(Object observation) {
        ArrayList<IObservation> ret = new ArrayList<>();

        if (observation instanceof ContextStructure) {
            return new IObservation[] { subject };
        } else if (observation instanceof IRelationship) {

            /*
             * add states
             */
            for (IState state : ((IRelationship) observation).getStates()) {
                parents.put(((State) state).getPath(), observation);
                ret.add(state);
            }

            /*
             * TODO processes
             */

        } else if (observation instanceof ISubject) {

            /*
             * accumulate subjects in catalog
             */
            Map<IKnowledge, List<ISubject>> catalog = new HashMap<>();
            for (ISubject s : ((ISubject) observation).getSubjects()) {
                List<ISubject> list = catalog.get(s.getObservable().getType());
                if (list == null) {
                    list = new ArrayList<ISubject>();
                    catalog.put(s.getObservable().getType(), list);
                }
                list.add(s);
            }

            /*
             * add states
             */
            for (IState state : ((ISubject) observation).getStates()) {
                parents.put(((State) state).getPath(), observation);
                ret.add(state);
            }

            /*
             * add folders with 2 subjects or more
             */
            for (IKnowledge key : catalog.keySet()) {
                if (catalog.get(key).size() > 1) {
                    Folder folder = new Folder(key, (ISubject) observation);
                    for (ISubject s : catalog.get(key)) {
                        parents.put(((Subject) s).getPath(), folder);
                        folder.add(s);
                    }
                    ret.add(folder);
                }
            }

            /*
             * add all lone subjects
             */
            for (IKnowledge key : catalog.keySet()) {
                if (catalog.get(key).size() == 1) {
                    parents.put(((Subject) catalog.get(key).get(0)).getPath(), observation);
                    ret.add(catalog.get(key).get(0));
                }
            }

            /*
             * add all relationships, in a folder if > 1
             */
            Set<IRelationship> relationships = ((ISubject) observation).getStructure().getRelationships();
            if (relationships.size() > 1) {
                Folder folder = new Folder(null, (ISubject) observation);
                for (IRelationship r : relationships) {
                    parents.put(((Relationship) r).getPath(), folder);
                    folder.add(r);
                }
                ret.add(folder);
            } else if (relationships.size() > 0) {
                IRelationship r = relationships.iterator().next();
                parents.put(((Relationship) r).getPath(), observation);
                ret.add(r);
            }

            /*
             * all processes individually
             */
            for (IProcess p : ((ISubject) observation).getProcesses()) {
                parents.put(((Process) p).getPath(), observation);
                ret.add(p);
            }

        } else if (observation instanceof Folder) {
            ret.addAll((Folder) observation);
        }

        return ret.toArray(new IObservation[ret.size()]);
    }

    public Object getParent(Object observation) {
        return parents.get(observation instanceof Folder ? ((Folder) observation).id
                : (observation instanceof ContextStructure ? null : ((Observation) observation).getPath()));
    }

    public boolean hasChildren(Object element) {
        if (element instanceof Folder) {
            return ((Folder) element).size() > 0;
        } else if (element instanceof ISubject) {
            return ((ISubject) element).getStates().size() > 0
                    || ((ISubject) element).getSubjects().size() > 0 ||
                    ((ISubject) element).getProcesses().size() > 0;
        } else if (element instanceof IRelationship) {
            return ((IRelationship) element).getStates().size() > 0 /* TODO processes */;
        } else if (element instanceof ContextStructure) {
            return hasChildren(subject);
        }
        return false;
    }
}
