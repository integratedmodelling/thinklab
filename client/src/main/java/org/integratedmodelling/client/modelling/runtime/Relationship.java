/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.client.modelling.runtime;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import org.integratedmodelling.api.metadata.IMetadata;
import org.integratedmodelling.api.modelling.INamespace;
import org.integratedmodelling.api.modelling.IObservable;
import org.integratedmodelling.api.modelling.IRelationship;
import org.integratedmodelling.api.modelling.IScale;
import org.integratedmodelling.api.modelling.IState;
import org.integratedmodelling.api.modelling.IStructure;
import org.integratedmodelling.api.modelling.ISubject;
import org.integratedmodelling.common.configuration.KLAB;
import org.integratedmodelling.common.model.runtime.Scale;
import org.integratedmodelling.common.model.runtime.Structure;
import org.integratedmodelling.common.utils.CamelCase;
import org.integratedmodelling.exceptions.ThinklabException;

public class Relationship extends Observation implements IRelationship {

    List<IState> states = new ArrayList<IState>();
    ISubject     source;
    ISubject     target;
    IStructure   structure;
    INamespace   namespace;
    String       name;
    ISubject     parent;

    // for map constructor; requires linking
    String sourceId = null;
    String targetId = null;

    public Relationship(Map<?, ?> map) {
        sourceId = map.get("source").toString();
        targetId = map.get("target").toString();
        if (map.containsKey("states")) {

            List<?> sts = (List<?>) map.get("states");
            for (Object o : sts) {
                states.add((IState) o);
                ((State) o)._parent = this;
            }
        }
        _scale = (Scale) map.get("scale");
        name = map.get("name").toString();
        _id = map.get("internal-id").toString();
        _observable = (IObservable) map.get("observable");
        _label = CamelCase.toLowerCase(_observable.getType().getLocalName(), ' ') + " " + sourceId + " to "
                + targetId;
    }

    public ISubject getParent() {
        return parent;
    }

    @Override
    public IScale getScale() {
        return _scale;
    }

    @Override
    public ISubject getSource() {
        return source;
    }

    @Override
    public ISubject getTarget() {
        return target;
    }

    @Override
    public INamespace getNamespace() {
        return namespace;
    }

    @Override
    public String getId() {
        return _id;
    }

    @Override
    public Collection<IState> getStates() {
        return states;
    }

    //
    @Override
    public void link(IStructure structure, Map<String, ISubject> subjects) {
        this.parent = structure.getSubject();
        this.source = subjects.get(sourceId);
        this.target = subjects.get(targetId);
        if (this.source != null && this.target != null) {
            ((Structure) structure).link(this.source, this.target, this);
        } else {
        	// TODO this should be an exception
        	KLAB.error("internal error: relationship could not be parsed: " + sourceId + " -> " + targetId);
        }
    }

    @Override
    public IState getState(IObservable observable, Object... data) throws ThinklabException {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public IState getStaticState(IObservable observable) throws ThinklabException {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public IMetadata getMetadata() {
        // TODO Auto-generated method stub
        return null;
    }

}
