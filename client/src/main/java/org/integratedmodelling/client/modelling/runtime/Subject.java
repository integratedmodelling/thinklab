/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.client.modelling.runtime;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Map;

import org.integratedmodelling.api.knowledge.IProperty;
import org.integratedmodelling.api.metadata.IMetadata;
import org.integratedmodelling.api.modelling.IEvent;
import org.integratedmodelling.api.modelling.INamespace;
import org.integratedmodelling.api.modelling.IObservable;
import org.integratedmodelling.api.modelling.IObservation;
import org.integratedmodelling.api.modelling.IProcess;
import org.integratedmodelling.api.modelling.IScale;
import org.integratedmodelling.api.modelling.IScale.Locator;
import org.integratedmodelling.api.modelling.IState;
import org.integratedmodelling.api.modelling.IStructure;
import org.integratedmodelling.api.modelling.ISubject;
import org.integratedmodelling.api.monitoring.IMonitor;
import org.integratedmodelling.api.runtime.IContext;
import org.integratedmodelling.collections.Path;
import org.integratedmodelling.common.model.runtime.Scale;
import org.integratedmodelling.common.model.runtime.Structure;
import org.integratedmodelling.common.states.States;
import org.integratedmodelling.common.utils.StringUtils;
import org.integratedmodelling.common.vocabulary.NS;
import org.integratedmodelling.exceptions.ThinklabException;
import org.integratedmodelling.exceptions.ThinklabValidationException;

public class Subject extends Observation implements ISubject {

	String _name;
	ArrayList<IState> _states = new ArrayList<IState>();
	ArrayList<IProcess> _processes = new ArrayList<IProcess>();
	private IMetadata _metadata;
	private IMonitor _monitor;
	private Structure _structure;

	public Subject(Map<?, ?> map) {

		_name = (String) map.get("name");
		_label = Path.getLast(_name, '.');
		if (map.get("scale") instanceof Map<?, ?>) {
			_scale = new Scale((Map<?, ?>) map.get("scale"));
		} else {
			_scale = (Scale) map.get("scale");
		}
		_id = map.get("internal-id").toString();
		_observable = (IObservable) map.get("observable");
		// Set<IObservable> obss = new HashSet<>();
		for (Object o : (Collection<?>) map.get("states")) {
			// /*
			// * FIXME (probably upstream): these come in more times than
			// necessary, the second time
			// * without data.
			// */
			// if (obss.contains(((IState) o).getObservable())) {
			// continue;
			// }
			_states.add((IState) o);
			// obss.add(((IState) o).getObservable());
		}
		for (Object o : (Collection<?>) map.get("processes")) {
			_processes.add((IProcess) o);
		}
		_structure = map.containsKey("structure") ? (Structure) map.get("structure") : new Structure(this);
		if (map.containsKey("structure")) {
			try {
				_structure.link(this);
			} catch (Throwable e) {
				System.out.println("hostia");
			}
		}
	}

	@Override
	public Collection<IEvent> getEvents() {
		return new ArrayList<IEvent>();
	}

	public IObservation get(String path) {

		if (path.equals("/")) {
			return this;
		}

		if (path.startsWith("/")) {
			path = path.substring(1);
		}
		String[] s = path.split("\\/");
		IObservation ret = this;

		for (String id : s) {

			for (IState state : getStates()) {
				// states have no children
				if (((State) state)._id.equals(id)) {
					return state;
				}
			}
			for (ISubject subject : getSubjects()) {
				if (((Subject) subject)._id.equals(id)) {
					ret = subject;
				}
			}
		}

		return ret;
	}

	@Override
	public IMetadata getMetadata() {
		return _metadata;
	}

	@Override
	public Collection<IState> getStates() {
		return _states;
	}

	@Override
	public Collection<ISubject> getSubjects() {
		ArrayList<ISubject> ret = new ArrayList<>();
		for (IObservation o : _structure.vertexSet()) {
			if (!o.equals(this)) {
				ret.add((ISubject) o);
			}
		}
		return ret;
	}

	@Override
	public String getId() {
		return _label;
	}

	public Object getParent() {
		return _parent;
	}

	public IContext getContext() {
		return _context;
	}

	@Override
	public IMonitor getMonitor() {
		return _monitor;
	}

	@Override
	public Collection<IProcess> getProcesses() {
		return _processes;
	}

	@Override
	public String toString() {
		return dump(0);
	}

	private String dump(int i) {

		String filler = StringUtils.spaces(i);
		String filled = StringUtils.spaces(i + 3);
		String ret = filler;
		ret += (i == 0 ? "[C] " : "[O] ") + _name + " (" + getObservable().getType() + ")\n";

		int nn = 1;
		if (_states.size() > 0) {
			ret += "\n";
			for (IState s : _states) {
				ret += filler + filled + " S" + nn + " " + s + "\n";
				nn++;
			}
		}

		nn = 1;
		if (_structure.getObservations().size() > 1) {
			ret += "\n";
			for (IObservation s : _structure.getObservations()) {
				if (!s.equals(this)) {
					ret += filler + filled + " O" + nn + " " + ((Subject) s).dump(i + 3);
					nn++;
				}
			}
		}

		nn = 0;
		// TODO relationships (not to self)

		return ret;
	}

	@Override
	public void setMonitor(IMonitor monitor) {
		_monitor = monitor;
	}

	@Override
	public IState getState(IObservable obs, Object... data) throws ThinklabException {

		for (IState s : _states) {
			if (obs.equals(s.getObservable())) {
				return s;
			}
		}
		if (!NS.isQuality(obs)) {
			throw new ThinklabValidationException("cannot create a state for a non-quality: " + obs.getType());
		}

		IState ret = States.create(obs, this);
		_states.add(ret);
		return ret;
	}

	@Override
	public ISubject getSubject(IObservable obs) throws ThinklabException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public IEvent getEvent(IObservable obs) throws ThinklabException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public IProcess getProcess(IObservable obs) throws ThinklabException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public IState getStaticState(IObservable observable) throws ThinklabException {
		// client states are all static
		return getState(observable);
	}

	@Override
	public IStructure getStructure(Locator... locators) {
		return _structure;
	}

	@Override
	public ISubject newSubject(IObservable observable, IScale scale, String name, IProperty relationship)
			throws ThinklabException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public IProcess newProcess(IObservable observable, IScale scale, String name) throws ThinklabException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public IEvent newEvent(IObservable observable, IScale scale, String name) throws ThinklabException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public INamespace getNamespace() {
		// TODO Auto-generated method stub
		return null;
	}
}
