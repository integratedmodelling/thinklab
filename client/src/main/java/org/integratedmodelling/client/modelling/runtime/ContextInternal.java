/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.client.modelling.runtime;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.integratedmodelling.api.engine.IModelingEngine;
import org.integratedmodelling.api.modelling.ICoverage;
import org.integratedmodelling.api.modelling.IObservation;
import org.integratedmodelling.api.modelling.ISubject;
import org.integratedmodelling.api.modelling.visualization.IMedia;
import org.integratedmodelling.api.monitoring.IMonitor;
import org.integratedmodelling.api.runtime.IContext;
import org.integratedmodelling.api.runtime.ISession;
import org.integratedmodelling.api.runtime.ITask;
import org.integratedmodelling.api.time.ITemporalExtent;
import org.integratedmodelling.client.Client;
import org.integratedmodelling.exceptions.ThinklabException;
import org.integratedmodelling.exceptions.ThinklabRuntimeException;

/**
 * Internal client side of a context. Multiple objects will be sent by the engine when the same context
 * is modified, so it's not reliable to keep state. Wrapped into the public client context for reliable
 * use.
 * 
 * @author Ferd
 * 
 */
public class ContextInternal implements IContext {

    /**
     * These are used to store and communicate user/GUI status. Putting them here allows
     * much cleaner code when the context is passed around any interface. TODO/FIXME use
     * proper setters and getters after this is wired in.
     */
    // current state at the top of the stack - set by double-clicking the observation tree.

    List<String>    _scenarios = new ArrayList<String>();

    // same at both sides, key to proper operation. Note: only valid within a server session - may want
    // to store the session ID too, although end of session should destroy all contexts.
    long            _contextId;

    Subject         _subject;
    String          _name;
    long            _lastUpdate;
    ISession        _session;
    IModelingEngine _engine;

    public ContextInternal(Map<?, ?> status) {
        initialize(status);
    }

    private void initialize(Map<?, ?> status) {

        _lastUpdate = (Long) status.get("timestamp");
        _session = Client.get().getSession(status.get("session").toString());
        _engine = Client.get().getEngine();
        // _timestep = ((Number) status.get("current")).intValue();
        _contextId = ((Number) status.get("context")).longValue();
        _name = status.get("name").toString();
        _subject = (Subject) status.get("subject");
        _scenarios = (List<String>) status.get("scenarios");
        ITemporalExtent time = _subject.getScale().getTime();
        // if (time != null && time.getMultiplicity() > 1) {
        // _timeResolution = (time.getEnd().getMillis() - time.getStart().getMillis())
        // / time.getMultiplicity();
        // }
        _subject.contextualize(Client.get().registerContext(this));
    }

    @Override
    public boolean isEmpty() {
        return _subject == null;
    }

    @Override
    public String getName() {
        return _name;
    }

    @Override
    public boolean isFinished() {
        throw new ThinklabRuntimeException("internal: function should not be called");
    }

    @Override
    public boolean isRunning() {
        throw new ThinklabRuntimeException("internal: function should not be called");
    }

    @Override
    public IContext observe() throws ThinklabException {
        return null;
    }

    @Override
    public ITask observeAsynchronous() throws ThinklabException {
        return null;
    }

    @Override
    public IContext inScenario(String... scenarios) {
        for (String s : scenarios) {
            _scenarios.add(s);
        }
        return this;
    }

    @Override
    public ITask runAsynchronous() throws ThinklabException {
        throw new ThinklabRuntimeException("internal: function should not be called");
    }

    @Override
    public ICoverage getCoverage() {
        throw new ThinklabRuntimeException("internal: function should not be called");
    }

    @Override
    public ISubject getSubject() {
        return _subject;
    }

    @Override
    public long getId() {
        return _contextId;
    }

    @Override
    public List<ITask> getTasks() {
        throw new ThinklabRuntimeException("internal: function should not be called");
    }

    @Override
    public IObservation get(String path) {
        return _subject == null ? null : _subject.get(path);
    }

    @Override
    public void persist(File file, String path, IMedia.Type mediaType, Object... options)
            throws ThinklabException {
        _engine.persistContext(file, _contextId, path, mediaType, options);

    }

    @Override
    public IContext resetContext(boolean reset) {
        throw new ThinklabRuntimeException("internal: function should not be called");
    }

    @Override
    public void setMonitor(IMonitor monitor) {
        _session.setMonitor(monitor);
    }

    @Override
    public IMonitor getMonitor() {
        return _session.getMonitor();
    }

    @Override
    public IContext run() throws ThinklabException {
        throw new ThinklabRuntimeException("internal: function should not be called");
    }

    @Override
    public String getPathFor(IObservation observation) {
        return ((Observation) observation).getPath();
    }

    @Override
    public ISession getSession() {
        return _session;
    }

}
