/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.client.modelling.runtime;

import java.io.File;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.Stack;

import org.integratedmodelling.api.engine.IEngine;
import org.integratedmodelling.api.engine.IModelingEngine;
import org.integratedmodelling.api.modelling.ICoverage;
import org.integratedmodelling.api.modelling.IObservation;
import org.integratedmodelling.api.modelling.ISubject;
import org.integratedmodelling.api.modelling.scheduling.ITransition;
import org.integratedmodelling.api.modelling.visualization.IMedia;
import org.integratedmodelling.api.monitoring.IMonitor;
import org.integratedmodelling.api.monitoring.Messages;
import org.integratedmodelling.api.runtime.IContext;
import org.integratedmodelling.api.runtime.ISession;
import org.integratedmodelling.api.runtime.ITask;
import org.integratedmodelling.api.runtime.ITask.Status;
import org.integratedmodelling.client.Client;
import org.integratedmodelling.common.model.runtime.Transition;
import org.integratedmodelling.common.monitoring.Notification;
import org.integratedmodelling.common.time.TimeLocator;
import org.integratedmodelling.exceptions.ThinklabException;
import org.joda.time.DateTime;

/**
 * This keeps the user state relative to a context and the way it is being accessed and used
 * as it computes and changes via subsequent observations. It will also track
 * changes in structure and allow to retrieve history and provenance across different
 * observations.
 * 
 * Exposes a number of public fields that UIs can use to track visualization and
 * simulated time. 
 * 
 * @author Ferd
 *
 */
public class Context implements IContext {

    public ContextInternal   context;

    public State             currentState = null;
    // manage the current layers shown and their display order
    public Set<String>       shown        = new HashSet<String>();
    public Stack<String>     showOrder    = new Stack<String>();
    // all the tasks seen by a context
    public ArrayList<ITask>  tasks        = new ArrayList<ITask>();
    public Set<Long>         taskIds      = new HashSet<Long>();
    // last task ended in error, no more actions possible besides new context.
    public boolean           error        = false;
    // current time locator, null at the beginning - set by user by clicking on the timebar.
    public TimeLocator       currentTimeLocator;
    public long              currentTime;
    public ITask             runningTask;

    Date                     creationTime = new Date();
    Boolean                  _reset       = false;
    boolean                  _error;
    boolean                  _interrupted;
    IModelingEngine          _engine;
    long                     _lastUpdate  = 0;
    boolean                  _finalized   = false;
    ITransition              _transition;
    int                      _clientTime  = -1;
    boolean                  _running     = false;
    ISession                 _session     = null;

    private ContextStructure structure;

    public Context(ContextInternal arg) {
        context = arg;
        _engine = arg._engine;
        if (arg.getSubject().getScale().getTime() != null) {
            currentTimeLocator = TimeLocator.initialization();
            currentTime = arg.getSubject().getScale().getTime().getStart().getMillis();
            _session = arg._session;
        }
    }

    public boolean isInterrupted() {
        return _interrupted;
    }

    public IEngine getServer() {
        return _engine;
    }

    public synchronized ITransition getCurrentTransition() {
        return _transition;
    }

    public void setContext(ContextInternal ctx) {
        synchronized (this) {
            diff(ctx, null);
            this.context = ctx;
            this.structure = new ContextStructure((Subject) ctx.getSubject());
        }
    }

    public void addTask(Task task) {

        task._context = this;
        if (task.getStatus() == Status.ERROR || task.getStatus() == Status.INTERRUPTED) {
            _error = task.getStatus() == Status.ERROR;
            _interrupted = task.getStatus() == Status.INTERRUPTED;
            _running = false;
        } else if (task.getStatus() == Status.FINISHED) {
            if (task.isTemporal()) {
                _running = false;
                _finalized = true;
            }
        }

        /*
         * TODO qua si attacca la provenance - BEFORE MERGE
         */

        updateTasks(task);
    }

    private void updateTasks(Task task) {
        for (int i = 0; i < tasks.size(); i++) {
            if (tasks.get(i).getTaskId() == task.getTaskId()) {
                ((Task) tasks.get(i)).merge(task);
                return;
            }
        }
        tasks.add(task);
    }

    private synchronized void diff(ContextInternal ctx, Transition transition) {
        // TODO compute history before context is reassigned
        _engine = ctx._engine;
        _lastUpdate = ctx._lastUpdate;
    }

    @Override
    public void setMonitor(IMonitor monitor) {
        synchronized (this) {
            context.setMonitor(monitor);
        }
    }

    @Override
    public IMonitor getMonitor() {
        synchronized (this) {
            return context.getMonitor();
        }
    }

    @Override
    public long getId() {
        synchronized (this) {
            return context.getId();
        }
    }

    @Override
    public boolean isEmpty() {
        synchronized (this) {
            return context.isEmpty();
        }
    }

    @Override
    public String getName() {
        synchronized (this) {
            return context.getName();
        }
    }

    public DateTime getCreationDate() {
        return new DateTime(creationTime.getTime());
    }

    @Override
    public boolean isFinished() {
        synchronized (this) {
            return _finalized;
        }
    }

    @Override
    public boolean isRunning() {
        return _running;
    }

    @Override
    public IContext inScenario(String... scenarios) {
        synchronized (this) {
            return context.inScenario(scenarios);
        }
    }

    @Override
    public IContext observe() throws ThinklabException {
        synchronized (this) {
            return context.observe();
        }
    }

    @Override
    public IContext run() throws ThinklabException {
        synchronized (this) {
            return context.run();
        }
    }

    @Override
    public ITask observeAsynchronous() throws ThinklabException {
        synchronized (this) {
            return context.observeAsynchronous();
        }
    }

    @Override
    public ITask runAsynchronous() throws ThinklabException {
        _running = true;
        ITask ret = _engine.contextualize(getId());
        Client.get().getMonitor().send(new Notification(Messages.TASK_STARTED, _session, ret));
        return ret;
    }

    @Override
    public ICoverage getCoverage() {
        synchronized (this) {
            return context.getCoverage();
        }
    }

    @Override
    public ISubject getSubject() {
        synchronized (this) {
            return context.getSubject();
        }
    }

    @Override
    public List<ITask> getTasks() {
        synchronized (this) {
            return tasks;
        }
    }

    @Override
    public IObservation get(String path) {
        synchronized (this) {
            return context.get(path);
        }
    }

    public synchronized void setLastTransition(ITransition transition) {
        this._transition = transition;
    }

    @Override
    public void persist(File file, String path, IMedia.Type mediaType, Object... options)
            throws ThinklabException {
        synchronized (this) {
            context.persist(file, path, mediaType, options);
        }
    }

    @Override
    public String toString() {
        return "[C] " + getName() + " [id=" + getId() + "]: " + getSubject().getStates().size() + " states, "
                + getSubject().getSubjects().size() + " subjects";
    }

    @Override
    public IContext resetContext(boolean reset) {
        synchronized (this) {
            return context.resetContext(reset);
        }
    }

    public boolean isError() {
        synchronized (this) {
            return error;
        }
    }

    public ContextStructure getStructure() {
        return structure;
    }

    @Override
    public String getPathFor(IObservation observation) {
        synchronized (this) {
            return context.getPathFor(observation);
        }
    }

    @Override
    public ISession getSession() {
        return _session;
    }
}
