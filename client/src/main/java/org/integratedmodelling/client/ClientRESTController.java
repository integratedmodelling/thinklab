/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.client;

import org.integratedmodelling.api.auth.IUser;
import org.integratedmodelling.api.data.IRankingScale;
import org.integratedmodelling.api.metadata.IObservationMetadata;
import org.integratedmodelling.api.modelling.IClassification;
import org.integratedmodelling.api.modelling.IClassifier;
import org.integratedmodelling.api.modelling.IClassifyingObserver;
import org.integratedmodelling.api.modelling.ICountingObserver;
import org.integratedmodelling.api.modelling.IDirectObserver;
import org.integratedmodelling.api.modelling.IDistanceObserver;
import org.integratedmodelling.api.modelling.IMeasuringObserver;
import org.integratedmodelling.api.modelling.IObservable;
import org.integratedmodelling.api.modelling.IPresenceObserver;
import org.integratedmodelling.api.modelling.IProbabilityObserver;
import org.integratedmodelling.api.modelling.IProportionObserver;
import org.integratedmodelling.api.modelling.IRankingObserver;
import org.integratedmodelling.api.modelling.IRatioObserver;
import org.integratedmodelling.api.modelling.IRelationship;
import org.integratedmodelling.api.modelling.IScale;
import org.integratedmodelling.api.modelling.IState;
import org.integratedmodelling.api.modelling.IStructure;
import org.integratedmodelling.api.modelling.ISubject;
import org.integratedmodelling.api.modelling.IUncertaintyObserver;
import org.integratedmodelling.api.modelling.IUnit;
import org.integratedmodelling.api.modelling.IValuingObserver;
import org.integratedmodelling.api.monitoring.INotification;
import org.integratedmodelling.api.network.IComponent;
import org.integratedmodelling.api.runtime.IContext;
import org.integratedmodelling.api.runtime.ITask;
import org.integratedmodelling.api.services.IPrototype;
import org.integratedmodelling.api.space.ISpatialExtent;
import org.integratedmodelling.api.time.ITemporalExtent;
import org.integratedmodelling.client.modelling.runtime.ContextInternal;
import org.integratedmodelling.client.modelling.runtime.Relationship;
import org.integratedmodelling.client.modelling.runtime.State;
import org.integratedmodelling.client.modelling.runtime.Subject;
import org.integratedmodelling.client.modelling.runtime.Task;
import org.integratedmodelling.common.classification.Classification;
import org.integratedmodelling.common.classification.Classifier;
import org.integratedmodelling.common.command.Prototype;
import org.integratedmodelling.common.components.Component;
import org.integratedmodelling.common.data.RankingScale;
import org.integratedmodelling.common.engine.JSONdeserializer;
import org.integratedmodelling.common.kim.KIMClassificationObserver;
import org.integratedmodelling.common.kim.KIMCountObserver;
import org.integratedmodelling.common.kim.KIMDirectObserver;
import org.integratedmodelling.common.kim.KIMDistanceObserver;
import org.integratedmodelling.common.kim.KIMMeasurementObserver;
import org.integratedmodelling.common.kim.KIMPresenceObserver;
import org.integratedmodelling.common.kim.KIMProbabilityObserver;
import org.integratedmodelling.common.kim.KIMProportionObserver;
import org.integratedmodelling.common.kim.KIMRankingObserver;
import org.integratedmodelling.common.kim.KIMRatioObserver;
import org.integratedmodelling.common.kim.KIMUncertaintyObserver;
import org.integratedmodelling.common.kim.KIMValuationObserver;
import org.integratedmodelling.common.model.runtime.Scale;
import org.integratedmodelling.common.model.runtime.Space;
import org.integratedmodelling.common.model.runtime.Structure;
import org.integratedmodelling.common.model.runtime.Time;
import org.integratedmodelling.common.monitoring.EngineNotification;
import org.integratedmodelling.common.network.RESTController;
import org.integratedmodelling.common.vocabulary.Observable;
import org.integratedmodelling.common.vocabulary.ObservationMetadata;
import org.integratedmodelling.common.vocabulary.Unit;

/**
 * 
 * @author ferdinando.villa
 *
 */
public class ClientRESTController extends RESTController {

    public ClientRESTController(String url, IUser user) {
        super(url, user);
    }

    public ClientRESTController(String url, String nodeKey) {
        super(url, nodeKey);
    }

    static {
        /*
         * set up for all objects marshalled through JSON
         */
        JSONdeserializer.register(IPrototype.class, Prototype.class);
        JSONdeserializer.register(IContext.class, ContextInternal.class);
        JSONdeserializer.register(ITask.class, Task.class);
        JSONdeserializer.register(INotification.class, EngineNotification.class);
        JSONdeserializer.register(ISubject.class, Subject.class);
        JSONdeserializer.register(IState.class, State.class);
        JSONdeserializer.register(ISpatialExtent.class, Space.class);
        JSONdeserializer.register(ITemporalExtent.class, Time.class);
        JSONdeserializer.register(IScale.class, Scale.class);
        JSONdeserializer.register(IObservable.class, Observable.class);
        JSONdeserializer.register(IComponent.class, Component.class);
        JSONdeserializer.register(IClassification.class, Classification.class);
        JSONdeserializer.register(IClassifier.class, Classifier.class);
        JSONdeserializer.register(IUnit.class, Unit.class);
        JSONdeserializer.register(IRankingScale.class, RankingScale.class);
        JSONdeserializer
                .register(IClassifyingObserver.class, KIMClassificationObserver.class);
        JSONdeserializer.register(ICountingObserver.class, KIMCountObserver.class);
        JSONdeserializer.register(IMeasuringObserver.class, KIMMeasurementObserver.class);
        JSONdeserializer.register(IPresenceObserver.class, KIMPresenceObserver.class);
        JSONdeserializer.register(IDistanceObserver.class, KIMDistanceObserver.class);
        JSONdeserializer.register(IProbabilityObserver.class, KIMProbabilityObserver.class);
        JSONdeserializer.register(IProportionObserver.class, KIMProportionObserver.class);
        JSONdeserializer.register(IRatioObserver.class, KIMRatioObserver.class);
        JSONdeserializer.register(IRankingObserver.class, KIMRankingObserver.class);
        JSONdeserializer.register(IUncertaintyObserver.class, KIMUncertaintyObserver.class);
        JSONdeserializer.register(IValuingObserver.class, KIMValuationObserver.class);
        JSONdeserializer.register(IStructure.class, Structure.class);
        JSONdeserializer.register(IRelationship.class, Relationship.class);
        JSONdeserializer.register(IDirectObserver.class, KIMDirectObserver.class);
        JSONdeserializer.register(IObservationMetadata.class, ObservationMetadata.class);
    }

}
