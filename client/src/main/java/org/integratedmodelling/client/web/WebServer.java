/*******************************************************************************
 *  Copyright (C) 2007, 2014:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.client.web;

import java.util.HashMap;

import org.integratedmodelling.exceptions.ThinklabException;
import org.integratedmodelling.exceptions.ThinklabInternalErrorException;
import org.integratedmodelling.exceptions.ThinklabRuntimeException;
import org.mortbay.jetty.Connector;
import org.mortbay.jetty.Server;
import org.mortbay.jetty.nio.SelectChannelConnector;
import org.mortbay.jetty.webapp.WebAppClassLoader;
import org.mortbay.jetty.webapp.WebAppContext;


public class WebServer {

    private Server server   = null;
    int            port     = 8881;
    String         host     = "127.0.0.1";
    String         context  = "";
    String         webSpace = System.getProperty("thinklab.webapp.dir", "./war");

    public WebServer(int webPort, String war) {
        port = webPort;
        webSpace = war;
    }

    public WebServer(String webHost, int webPort, String war, HashMap<String, String> parameters) {
        host = webHost;
        port = webPort;
        webSpace = war;
    }

    /**
     * The base domain URL that needs to be set in Access-Control-Allow-Origin headers for those pesky
     * AJAX clients.
     * @return
     */
    public String getHostUrl() {
        return "http://" + host + ":" + port;
    }

    /**
     * The URL to connect to.
     * 
     * @return
     */
    public String getBaseUrl() {
        return "http://" + host + ":" + port + "/" + context;
    }

    public String getUrl(String relativePath, String ... parameters) {
        String ret = "http://" + host + ":" + port + "/" + context;
        if (relativePath != null) {
            ret = ret + (ret.endsWith("/") ? "" : "/") + relativePath;
        }
        if (parameters != null) {
            ret += "?";
            for (int i = 0; i < parameters.length; i++) {
                ret += (i == 0 ? "" : "&") + parameters[i] + "=" + parameters[++i];
            }
        }
        return ret;
    }

    public void stop() {

        if (server != null)
            try {
                server.stop();
            } catch (Exception e) {
                throw new ThinklabRuntimeException(e);
            } finally {
                server = null;
            }
    }

    public Server start() throws ThinklabException {

        if (server != null)
            throw new ThinklabException("server is already active");

        Server serv = new Server();
        SelectChannelConnector connector = new SelectChannelConnector();
        connector.setHost(host);
        connector.setPort(port);
        serv.setConnectors(new Connector[] { connector });

        WebAppContext wah = new WebAppContext();
        wah.setContextPath(context);
        wah.setWar(webSpace);

        ClassLoader cl = this.getClass().getClassLoader();
        WebAppClassLoader wacl;
        try {
            wacl = new WebAppClassLoader(cl, wah);
            wah.setClassLoader(wacl);
            serv.addHandler(wah);
            serv.setStopAtShutdown(true);
            serv.start();
        } catch (Exception e) {
            throw new ThinklabInternalErrorException(e);
        }

        server = serv;

        return serv;
    }

}
