/// *******************************************************************************
// * Copyright (C) 2007, 2015:
// *
// * - Ferdinando Villa <ferdinando.villa@bc3research.org>
// * - integratedmodelling.org
// * - any other authors listed in @author annotations
// *
// * All rights reserved. This file is part of the k.LAB software suite,
// * meant to enable modular, collaborative, integrated
// * development of interoperable data and model components. For
// * details, see http://integratedmodelling.org.
// *
// * This program is free software; you can redistribute it and/or
// * modify it under the terms of the Affero General Public License
// * Version 3 or any later version.
// *
// * This program is distributed in the hope that it will be useful,
// * but without any warranty; without even the implied warranty of
// * merchantability or fitness for a particular purpose. See the
// * Affero General Public License for more details.
// *
// * You should have received a copy of the Affero General Public License
// * along with this program; if not, write to the Free Software
// * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
// * The license is also available at: https://www.gnu.org/licenses/agpl.html
// *******************************************************************************/
// package org.integratedmodelling.client;
//
// import org.integratedmodelling.api.IThinklab;
// import org.integratedmodelling.api.configuration.IConfiguration;
// import org.integratedmodelling.api.factories.IKnowledgeManager;
// import org.integratedmodelling.api.factories.IModelManager;
// import org.integratedmodelling.api.factories.IProjectManager;
// import org.integratedmodelling.common.configuration.KLAB;
//
/// **
// * Instantiates what we need from the common library. Client.boot() must be called before anything is done
// * at the client side.
// *
// * TODO check all these singletons one day and see if we can do without some or all.
// * TODO probably obsolete, as we now have a proper API for engines and engine clients.
// *
// * @author Ferd
// *
// */
// public class Thinklab implements IThinklab {
//
// boolean _locked = false;
//
// @Override
// public IKnowledgeManager getKnowledgeManager() {
// return KLAB.KM;
// }
//
// @Override
// public IProjectManager getProjectManager() {
// return KLAB.PMANAGER;
// }
//
// @Override
// public IConfiguration getConfiguration() {
// return KLAB.CONFIG;
// }
//
// @Override
// public IModelManager getModelManager() {
// return KLAB.MMANAGER;
// }
//
// // @Override
// // public void warn(String string) {
// // // TODO!
// // System.out.println(string);
// // }
// //
// // @Override
// // public void error(String string) {
// // // TODO!
// // System.out.println(string);
// // }
//
// // @Override
// // public void info(String string) {
// // // TODO!
// // System.out.println(string);
// // }
// //
// // @Override
// // public boolean isLocked() {
// // // a client is just as locked as its current server
// // return _locked;
// // }
//
// public static void shutdown() {
// }
//
// public static Thinklab get() {
// return (Thinklab) KLAB.ENGINE;
// }
//
// // @Override
// // public IUser getLockingUser() {
// // // TODO Auto-generated method stub
// // return null;
// // }
// //
// // // @Override
// // // public boolean lock(IUser user, boolean force) {
// // // // TODO Auto-generated method stub
// // // return false;
// // // }
// //
// // @Override
// // public boolean unlock() {
// // // TODO Auto-generated method stub
// // return false;
// // }
//
// // @Override
// // public Collection<IComponent> getComponents() {
// // // TODO Auto-generated method stub
// // return null;
// // }
// //
// // @Override
// // public IComponent getComponent(String id) {
// // // TODO Auto-generated method stub
// // return null;
// // }
//
// // @Override
// // public String getName() {
// // // TODO Auto-generated method stub
// // return null;
// // }
//
// @Override
// public void rescanClasspath() {
// // TODO Auto-generated method stub
//
// }
//
// }
