/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.client.commands;

import org.integratedmodelling.api.modelling.IDirectObserver;
import org.integratedmodelling.api.modelling.IModelObject;
import org.integratedmodelling.api.runtime.IContext;
import org.integratedmodelling.api.services.IServiceCall;
import org.integratedmodelling.api.services.annotations.Execute;
import org.integratedmodelling.api.services.annotations.Prototype;
import org.integratedmodelling.client.Client;
import org.integratedmodelling.common.configuration.KLAB;
import org.integratedmodelling.common.model.runtime.Space;
import org.integratedmodelling.common.model.runtime.Time;
import org.integratedmodelling.exceptions.ThinklabException;

@Prototype(
        id = "observe",
        description = "start an observation in the current context",
        args = {
                "what",
                Prototype.TEXT,
                "# what1",
                Prototype.TEXT,
                "# what2",
                Prototype.TEXT,
                "# what3",
                Prototype.TEXT,
                "# what4",
                Prototype.TEXT,
                "# what5",
                Prototype.TEXT,
                "# what6",
                Prototype.TEXT,
                "# what7",
                Prototype.TEXT,
                "? r|reset",
                Prototype.NONE,
                "? c|context",
                Prototype.INT
        },
        argDescriptions = {
                "what to observe",
                "what to observe (optional)",
                "what to observe (optional)",
                "what to observe (optional)",
                "what to observe (optional)",
                "what to observe (optional)",
                "what to observe (optional)",
                "what to observe (optional)",
                "reset context before observation",
                "choose the context to observe into" })
public class Observe {

    // current spatial-temporal forcings. TODO add subcommand to modify them.
    public static Space spatialForcing  = Space.getForcing(1.0, "km");
    public static Time  temporalForcing = Time.getForcing(0, 0, 0);

    @Execute
    public Object observe(IServiceCall command) throws ThinklabException {

        long context = -1;
        if (command.has("context")) {
            context = ((Number) (command.get("context"))).longValue();
        }

        IModelObject mo = KLAB.MMANAGER.findModelObject(command.get("what").toString());

        if (mo instanceof IDirectObserver) {
            return Client.get()
                    .observe((IDirectObserver) mo, spatialForcing, temporalForcing.isEmpty() ? null
                            : temporalForcing);
        }

        /*
         * TODO if we have multiple argument, we should call a client function with an
         * array argument; we can't just loop over multiple observations in here.
         */
        IContext ctx = Client.get().getContext(context);
        return Client.get()
                .observe(command.get("what").toString(), ctx == null ? null : ctx.getSubject());
    }
}
