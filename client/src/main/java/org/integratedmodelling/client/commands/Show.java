/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.client.commands;

import org.integratedmodelling.api.modelling.IObservation;
import org.integratedmodelling.api.services.IServiceCall;
import org.integratedmodelling.api.services.annotations.Execute;
import org.integratedmodelling.api.services.annotations.Prototype;
import org.integratedmodelling.client.Client;
import org.integratedmodelling.client.modelling.runtime.ContextPath;
import org.integratedmodelling.common.controller.ContextViewer;
import org.integratedmodelling.common.controller.ContextViewer.ViewMode;
import org.integratedmodelling.exceptions.ThinklabException;

@Prototype(
        id = "show",
        description = "start an observation in the current context",
        args = {
                "# path",
                Prototype.TEXT,
                "? f|focus",
                Prototype.NONE,
                "? cm|cycle-maps",
                Prototype.NONE,
                "? md|map-display",
                Prototype.NONE,
                "? gd|graph-display",
                Prototype.NONE,
                "? fd|flow-display",
                Prototype.NONE,
                "? td|timeline-display",
                Prototype.NONE,
                "? dd|data-display",
                Prototype.NONE,
        },
        argDescriptions = {
                "path to what to show (e.g. C1/S2 for the second state in context 1)",
                "make the observation the new focus in view (must specify a direct observation)",
                "cycle base map layer",
                "set display to geographical map",
                "set display to timeseries graph",
                "set display to flow diagram",
                "set display to timeline graph",
                "set display to data spreadsheet"
        })
public class Show {

    @Execute
    public Object show(IServiceCall command) throws ThinklabException {

        ContextViewer cv = Client.get().getContextViewer();

        if (cv == null) {
            return "context viewer could not be started";
        }

        if (command.has("cycle-maps")) {
            cv.cycleMaps();
        }
        if (command.has("graph-display")) {
            cv.setView(ViewMode.TIMEPLOT);
        }
        if (command.has("flow-display")) {
            cv.setView(ViewMode.FLOW);
        }
        if (command.has("timeline-display")) {
            cv.setView(ViewMode.TIMELINE);
        }
        if (command.has("data-display")) {
            cv.setView(ViewMode.DATATABLE);
        }
        if (command.has("map-display")) {
            cv.setView(ViewMode.MAP);
        }

        if (command.has("path")) {

            String path = command.getString("path");
            ContextPath cp = new ContextPath(path);

            if (cp.getContext() == null) {
                throw new ThinklabException("cannot find a context to match path " + path);
            }

            Client.get().showContext(cp.getContext());
            for (IObservation o : cp) {
                Client.get().show(o, cp.getContext(), command.has("focus"));
            }
        }
        return null;
    }
}
