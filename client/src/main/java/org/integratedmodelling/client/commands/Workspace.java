/*******************************************************************************
 *  Copyright (C) 2007, 2014:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.client.commands;

import java.util.ArrayList;

import org.integratedmodelling.api.project.IProject;
import org.integratedmodelling.api.services.IServiceCall;
import org.integratedmodelling.api.services.annotations.Execute;
import org.integratedmodelling.api.services.annotations.Prototype;
import org.integratedmodelling.client.Client;
import org.integratedmodelling.common.configuration.KLAB;

/**
 * Set client properties.
 * 
 * @author ferdinando.villa
 *
 */
@Prototype(id = "workspace",
        description = "manage workspace",
        args = { "# arg0", Prototype.TEXT })
public class Workspace {

    @Execute
    public Object list(IServiceCall command) {

        ArrayList<String> projects = new ArrayList<String>();
        projects.add("Workspace is at " + Client.get().getWorkspace() + " with projects:");
        for (IProject p : KLAB.PMANAGER.getProjects()) {
            projects.add("   " + p.getId());
        }
        return projects;
    }

    @Execute(command = "set", requires = "arg0")
    public Object set(IServiceCall command) {

        Client.get().setProperty("workspace", command.get("arg0").toString());
        return null;
    }
}
