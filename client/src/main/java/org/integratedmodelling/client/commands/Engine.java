/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.client.commands;

import org.integratedmodelling.api.services.IServiceCall;
import org.integratedmodelling.api.services.annotations.Execute;
import org.integratedmodelling.api.services.annotations.Prototype;
import org.integratedmodelling.client.Client;
import org.integratedmodelling.exceptions.ThinklabException;

@Prototype(id = "engine", description = "control the remote engine")
public class Engine {

    @Execute(command = "status")
    public Object getStatus(IServiceCall command) {
        if (Client.get().getEngine() != null) {
            return Client.get().getEngine().getStatus();
        }
        return null;
    }

    @Execute(command = "stop")
    public Object shutdown(IServiceCall command) throws ThinklabException {
        Client.get().getEngine().stop();
        return null;
    }

    @Execute(command = "start")
    public Object start(IServiceCall command) throws ThinklabException {
        Client.get().getEngine().start();
        return null;
    }

    @Execute(command = "restart")
    public Object restart(IServiceCall command) throws ThinklabException {
        Client.get().getEngine().stop();
        Client.get().getEngine().start();
        return null;
    }

    @Execute(command = "upgrade")
    public Object upgrade(IServiceCall command) throws ThinklabException {
        Client.get().upgradeEngine();
        return null;
    }
}
