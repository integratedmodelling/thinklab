/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.client.commands;

import java.io.File;
import java.util.ArrayList;

import org.integratedmodelling.api.project.IProject;
import org.integratedmodelling.api.services.IServiceCall;
import org.integratedmodelling.api.services.annotations.Execute;
import org.integratedmodelling.api.services.annotations.Prototype;
import org.integratedmodelling.common.configuration.KLAB;
import org.integratedmodelling.exceptions.ThinklabException;
import org.integratedmodelling.exceptions.ThinklabResourceNotFoundException;

/**
 * Export states and datasets.
 * 
 * @author ferdinando.villa
 *
 */
@Prototype(id = "project",
        description = "manage projects in workspace",
        args = {
                "# project", Prototype.TEXT,
                "# namespace", Prototype.TEXT,
                "? r|remote", Prototype.NONE
        },
        argDescriptions = { "project", "namespace", "operate on remote server" })
public class Project {

    @Execute
    public Object listProjects(IServiceCall command) {
        ArrayList<String> projects = new ArrayList<String>();
        for (IProject p : KLAB.PMANAGER.getProjects()) {
            projects.add(p.getId());
        }
        return projects;
    }

    @Execute(command = "create-namespace", requires = { "project", "namespace" })
    public Object createNamespace(IServiceCall command) throws ThinklabException {

        String prId = command.get("project").toString();
        String nsId = command.get("namespace").toString();

        IProject project = KLAB.PMANAGER.getProject(prId);
        if (project == null) {
            throw new ThinklabResourceNotFoundException("project " + prId);
        }

        ((org.integratedmodelling.common.project.Project) project).createNamespace(nsId, false);

        /**
         * Open editor on new namespace.
         */
        Edit.editFile(project.findResourceForNamespace(nsId));

        return KLAB.MMANAGER.getNamespace(nsId);
    }

    @Execute(command = "create", requires = { "project" })
    public Object createProject(IServiceCall command) throws ThinklabException {

        String prId = command.get("project").toString();
        return (KLAB.PMANAGER).createProject(
                new File(KLAB.WORKSPACE.getDefaultFileLocation() + File.separator + prId), null);
    }

    /*
     * TODO add subcommands:
     *  create p
     *  delete p
     *  deploy [p]
     *  undeploy [p]
     *  rescan [p]
     *  dependency [add|remove x p]
     */
}
