/*******************************************************************************
 *  Copyright (C) 2007, 2014:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.client.commands;

import org.integratedmodelling.api.knowledge.IConcept;
import org.integratedmodelling.api.services.IServiceCall;
import org.integratedmodelling.api.services.annotations.Execute;
import org.integratedmodelling.api.services.annotations.Prototype;
import org.integratedmodelling.common.vocabulary.NS;

/**
 * Context command.
 * 
 * @author ferdinando.villa
 *
 */
@Prototype(id = "reason",
        description = "reasoner interface",
        args = {
                "# arg1",
                Prototype.CONCEPT,
                "# arg2",
                Prototype.CONCEPT,
                "# arg3",
                Prototype.CONCEPT,
                "# arg4",
                Prototype.CONCEPT })
public class Reason {

    @Execute(command = "is", description = "check for subsumption", requires = { "arg1", "arg2" })
    public Object is(IServiceCall command) {

        IConcept c1 = (IConcept) command.get("arg1");
        IConcept c2 = (IConcept) command.get("arg2");
        return c1.is(c2);
    }

    @Execute(command = "children", description = "get direct children", requires = { "arg1" })
    public Object children(IServiceCall command) {

        IConcept c1 = (IConcept) command.get("arg1");
        return c1.getChildren();
    }

    @Execute(command = "closure", description = "get semantic closure", requires = { "arg1" })
    public Object closure(IServiceCall command) {

        IConcept c1 = (IConcept) command.get("arg1");
        return c1.getSemanticClosure();
    }

    @Execute(command = "traits", description = "extract traits from concept", requires = { "arg1" })
    public Object getTraits(IServiceCall command) {

        IConcept c1 = (IConcept) command.get("arg1");
        return NS.getTraits(c1);
    }

    @Execute(command = "trait?", description = "check if argument is a trait", requires = { "arg1" })
    public Object isTrait(IServiceCall command) {

        IConcept c1 = (IConcept) command.get("arg1");
        return NS.isTrait(c1);
    }

    @Execute(command = "subject?", description = "check if argument is a subject", requires = { "arg1" })
    public Object isSubject(IServiceCall command) {

        IConcept c1 = (IConcept) command.get("arg1");
        return NS.isThing(c1);
    }

    @Execute(command = "quality?", description = "check if argument is a quality", requires = { "arg1" })
    public Object isQuality(IServiceCall command) {

        IConcept c1 = (IConcept) command.get("arg1");
        return NS.isQuality(c1);
    }

    @Execute(
            command = "particular?",
            description = "check if argument is a particular",
            requires = { "arg1" })
    public Object isParticular(IServiceCall command) {

        IConcept c1 = (IConcept) command.get("arg1");
        return NS.isParticular(c1);
    }

    @Execute(
            command = "observable",
            description = "retrieve the primary observable from a concept",
            requires = { "arg1" })
    public Object getObservable(IServiceCall command) {

        IConcept c1 = (IConcept) command.get("arg1");
        return NS.getBaseObservable(c1);
    }
}
