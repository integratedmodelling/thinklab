/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.client.commands;

import java.io.File;
import java.util.Properties;
import java.util.UUID;

import org.integratedmodelling.api.services.IServiceCall;
import org.integratedmodelling.api.services.annotations.Execute;
import org.integratedmodelling.api.services.annotations.Prototype;
import org.integratedmodelling.common.auth.LicenseManager;

/**
 * Create certificate for server. Will only work if keys are in place and referenced through 
 * auth.properties.
 * 
 * @author Ferd
 *
 */
@Prototype(
        id = "certify",
        description = "create a server certificate (needs full authorization configuration)",
        args = { "name", Prototype.TEXT, "url", Prototype.TEXT, "? o|output", Prototype.TEXT })
public class Certify {

    @Execute
    public Object createServerCertificate(IServiceCall command) throws Exception {

        String name = command.get("name").toString();
        String url = command.get("url").toString();
        Properties properties = new Properties();
        properties.setProperty("url", url);
        properties.setProperty("name", name);
        properties.setProperty("key", UUID.randomUUID().toString());

        String outputFile = System.getProperty("user.home") + File.separator + "newcertificate.cert";
        if (command.has("output")) {
            outputFile = command.get("output").toString();
        }
        LicenseManager.get().createCertificate(properties, new File(outputFile));

        return outputFile;
    }
}
