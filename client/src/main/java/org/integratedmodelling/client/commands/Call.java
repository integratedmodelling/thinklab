/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
//package org.integratedmodelling.client.commands;
//
//import java.util.ArrayList;
//
//import org.integratedmodelling.api.services.IServiceCall;
//import org.integratedmodelling.api.services.annotations.Execute;
//import org.integratedmodelling.api.services.annotations.Prototype;
//import org.integratedmodelling.common.utils.RESTUtils;
//import org.integratedmodelling.exceptions.ThinklabException;
//
//@Prototype(id = "call", description = "invoke arbitrary services on remote engines (admin only)", args = {
//        "? e|engine", Prototype.TEXT,
//        // "id", Prototype.TEXT,
//        // max 8 args for free-form call subcommand
//        "# arg0",
//        Prototype.TEXT,
//        "# arg1",
//        Prototype.TEXT,
//        "# arg2",
//        Prototype.TEXT,
//        "# arg3",
//        Prototype.TEXT,
//        "# arg4",
//        Prototype.TEXT,
//        "# arg5",
//        Prototype.TEXT,
//        "# arg6",
//        Prototype.TEXT,
//        "# arg7",
//        Prototype.TEXT,
//        "# arg8",
//        Prototype.TEXT,
//        "# arg9",
//        Prototype.TEXT
//})
//public class Call {
//
//    @Execute
//    public Object call(IServiceCall call) throws ThinklabException {
//
//        ArrayList<Object> args = new ArrayList<>();
//
//        // args.add("id");
//        // args.add(call.get("id"));
//        if (call.has("engine")) {
//            args.add("engine");
//            args.add(call.get("engine"));
//        }
//
//        for (int i = 0; i < 10; i++) {
//            if (!call.has("arg" + i)) {
//                break;
//            }
//            args.add("arg" + i);
//            args.add(call.get("arg" + i));
//        }
//
//        return RESTUtils.post("http://127.0.0.1:8182/rest/", "repeat", args.toArray());
//
//        // return Client.get().getEngineClient().send("repeat", args.toArray());
//    }
//
// }
