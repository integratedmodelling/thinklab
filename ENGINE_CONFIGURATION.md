* Installing and configuring a Thinklab node server

** Installation and startup

The Thinklab engine is built with Maven from the "thinklab" project in the integratedmodelling git repository. If the result of 'mvn clean install' is successful, you have an engine distribution that you can deploy. Alternatively you can use a pre-built distribution or have a continuous development server automatically redeploy your engine when the 'master' branch is committed to. This is the preferred strategy in times of heavy development like these, when version matching across the network is crucial. When Thinklab hits version 1.0, the network API will be frozen and it will be less critical to keep versions synchronized.

** Configuration

*** Linking the node to the Thinklab network

First of all, each Thinklab server needs a server certificate in order to be usable as a node. This certificate is released by integratedmodelling.org and is a small file named 'server.cert'. It must be set in the .tl directory before the server is started.

When a server is certified, it can connect to others and be connected to by others. Network connections between nodes are made explicitly by exchanging certificates, and each link is one-directional. In order to connect a node to another so that network requests can be routed to it, you need to put the node's certificate in the $HOME/.tl/network directory. The certificate is the same as the server.cert file for the linked node and must be named with the name that the other server is know by to the network. So if the other server is named 'xxx' the certificate must be named xxx.cert. 

To establish a link in the other direction, copy your server.cert file to <yournodename>.cert and send it to the other node to be installed in its .tl/network directory. All servers need to be restarted before the certificate is read.

The name of each node is also written in the certificate, and is validated at startup and at connection. All node names are in lowercase and must not be changed unless directed by integratedmodelling.org. Changing the names or renaming certificates is certain to lead to major inconveniences.

*** Choosing the groups that have access to the engine:

Simply list the groups in the $HOME/.tl/thinklab.properties file like so:

	thinklab.allowed.groups=G1,G2...

If this property is missing, any user will have access to the node as long as it is reachable within the network beginning at their primary server. If it is present, users not belonging to the groups listed will not be aware of the node's existence or of the knowledge/services it provides.

*** Serving projects from a Thinklab node

There are two ways to serve a project for public use: 

1. As *projects*: making the node the authoritative server for a project that is synchronized at the user's end (the files are synchronized on the client's machine and loaded at client startup); this method works best for projects containing mostly knowledge (ontologies).

2. As *content*: loading the projects in the engine and making their content available to the client through network search (the files stay on the server and knowledge is transferred on demand). This method works best for projects containing mostly models that observe concepts defined in shared projects. The models resulting from a search will be ranked at the user side, so a model retrieved may not be used if another from a different source ranks best for the context of use.

To make a node serve a particular project (1), do the following:

	1. Ensure that no other node serves it. You don't want conflicts between co-existing nodes serving
	   conflicting versions of the same knowledge.
	2. You are in charge of deploying the project in the assets directory on the server and of making sure
	   it's the version/branch you want served. No synchronization mechanism is provided by
	   thinklab. You should ensure that an appropriate synchronization mechanism is in place.
	3. The synchronization between server and client relies on a MD5 hash being present and up to date at the engine side. To create that,
	   run 'md5sum `find . -type f -print` > filelist.txt' in the project's root directory. It is best to 
	   have a continuous development server regenerate and upload the file list at every commit to the distribution
	   branch. If the hash is not present, the synchronization will fail and the project will not be available
	   for the client.
	3. Ensure that the assets directory (where the project directory is found) is listed in the engine's thinklab.properties:
			thinklab.assets.dir=<your assets directory>
	   You can only have one assets directory.
	4. Ensure that the project is listed as a project asset in thinklab.properties:
			thinklab.project.asset=<your project ID>[,....]

Done this way, the project will be shared with any user who has access to the engine. If it is desired to share only certain projects with certain groups, you can use multiple properties with the names of the groups appended:

	thinklab.project.assets.GROUP1=project1,project2
	thinklab.project.assets.GROUP1.GROUP2=project3		

To make a node serve the _content_ of a particular project (2), i.e. make the models it contains available to users that search the network, use the same technique as above, but list the project in

	thinklab.project.content=...

as before, you can use group names in the property to restrict availability of project content to specified groups.

NOTE: the contents of projects shared as content will be made available to allowed users *only* if the search component is installed. That component provides the search/retrieve services that will serve the contents from the projects. To install the search component, ensure that the search-<VERSION>.jar file is present in .tl/components, and that the version number matches that of your engine distribution. Components are only read at initialization, so you will need to restart the server before this is seen.

*** Serving a component from a Thinklab node:

Components can contain Java binary code, knowledge and data assets. They are packaged as individual JAR files. An engine that serves a component must first of all load it; if the component is tagged as public by its developers, it will also be served as long as its ID is mentioned in the configuration.

To make a node serve a particular component, three conditions must be met:

	1. the component and all its prerequisites (other components it may depend on) must be installed in the node's .tl/components/ directory;
	2. the search component must be installed;
	3. the component's IDs must be listed in the *thinklab.published.components* properties, with or without group restrictions as explained above.

If a component has as a prerequisite a project that is served by either your node or another in the network, the project will be automatically synchronized from there. Otherwise, you must provide it in the assets directory indicated in the correspondent property. All the projects in the assets directory are loaded automatically before components are initialized.

*** Providing remote modeling services

A project may be loaded ONLY on the server and not served to the client, but the models in it may be made available for remote use (no knowledge is synchronized with the client and the models actually RUN on the engine, synchronizing the results as the simulation progresses). This assumes that all the knowledge necessary to interpret them, including the models that call the service, is known at the client side.
