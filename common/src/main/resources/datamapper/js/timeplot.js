/**
 * Created by Ferd on 10/27/2015.
 */
var timeplotInited = false;

function drawPlots() {

    if (!timeplotInited) {
        google.load("visualization", "1.1", {packages:["corechart"]});
        timeplotInited = true;
    }
    
    var data = google.visualization.arrayToDataTable([
        ['Year', 'Sales', 'Expenses'],
        ['2004',  1000,      400],
        ['2005',  1170,      460],
        ['2006',  660,       1120],
        ['2007',  1030,      540]
    ]);

    var options = {
        title: 'Dio Pelota',
        curveType: 'function',
        legend: { position: 'bottom' }
    };

    var chart = new google.visualization.LineChart(document.getElementById('timeplot'));

    chart.draw(data, options);
}