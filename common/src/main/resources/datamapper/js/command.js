/**
 * Implements the polling strategy for duplex communication with a tiny REST server in
 * another application. Check out JSONRESTController in the thinklab project for an
 * easy-to-use and lightweight working peer.
 *
 * Created by Ferd on 10/28/2015.
 */
var delayMs = 333;
var url = 'http://localhost';
var incoming = []
var outgoing = []

function send(message) {
    outgoing.push(message);
}

function switchView(view) {
    switch (view) {
        case "flow":
            $("#geomap").hide();
            $("#flowmap").show("slow").css({"top":"0","bottom":"0","left":"0","right":"0"});
            $("#timeplot").hide();
            $("#timeline").hide();
            $("#semquery").hide();
            break;
        case "map":
            $("#geomap").show("slow").css({"top":"0","bottom":"0","left":"0","right":"0"});
            $("#flowmap").hide();
            $("#timeplot").hide();
            $("#timeline").hide();
            $("#semquery").hide();
            break;
        case "plot":
            $("#geomap").hide();
            $("#flowmap").hide();
            $("#timeplot").show("slow").css({"top":"0","bottom":"0","left":"0","right":"0"});
            $("#timeline").hide();
            $("#semquery").hide();
            break;
        case "timeline":
            $("#geomap").hide();
            $("#flowmap").hide();
            $("#timeplot").hide();
            $("#timeline").show("slow").css({"top":"0","bottom":"0","left":"0","right":"0"});
            $("#semquery").hide();
            break;
        case "query":
            $("#geomap").hide();
            $("#flowmap").hide();
            $("#timeplot").hide();
            $("#timeline").hide();
            $("#semquery").show("slow").css({"top":"0","bottom":"0","left":"0","right":"0"});
            break;
    }
}

function execute(message) {

    switch (message.id) {
        case "set-space":
            showContext(message.args[0], message.args[1], message.args[2], message.args[3], message.args[4]);
            break;
        case "cycle-maps":
            cycleMap();
            break;
        case "show-map":
            showMap(
                message.args[0], // URL array
                message.args[1], // ID array
                message.args[2], // minX
                message.args[3], // minY
                message.args[4], // maxX
                message.args[5], // maxY
                message.args[6], // image X size
                message.args[7]);// image Y size
            break;
        case "draw-mode":
            drawMode(message.args[0] == "on");
            break;
        case "remove-map":
            removeMap(message.args[0]);
            break;
        case "remove-maps":
            removeAllMaps();
            break;
        case "set-opacity":
            setOpacity(message.args[0], message.args[1]);
            break;
        case "reset-map":
            resetMap();
            break;
        case "draw-flows":
            // TODO params
            drawFlows();
            break;
        case "draw-plots":
            // TODO params
            drawPlots();
            break;
        case "draw-timeline":
            // TODO params
            drawTimeline();
            break;
        case "switch-view":
            switchView(message.args[0]);
            break;
        case "add-feature":
            addFeature(
                message.args[0], // shape wkt
                message.args[1], // name
                message.args[2], // id
                (message.args.length > 3 ? message.args[3] : "#ff0000"), // color
                (message.args.length > 4 ? message.args[4] : null), // fillColor
                (message.args.length > 5 ? message.args[5] : 1.0) // opacity
            );
            break;
        case "add-marker":
            addMarker(message.args[0], message.args[1], message.args[2], message.args[3]);
            break;
        case "show-marker":
            showMarker(message.args[0]);
            break;
        case "hide-marker":
            hideMarker(message.args[0]);
            break;
        case "show-feature":
            showFeature(message.args[0]);
            break;
        case "hide-feature":
            hideFeature(message.args[0]);
            break;
        case "show-concepts":
            describeConcepts(message.args[0], "semquery");
            switchView("query");
            break;
        case "show-subjects":
            describeSubjects(message.args[0], "semquery");
            switchView("query");
            break;
    }
}

function getMessageUrl(message) {
    var ret = url + "?cmd=" + message.id;
    $.each(message, function(k,v) {
        if (k != "id") {
            ret += "&" + k + "=" + v;
        }
    })
    return ret;
}

function startPolling() {
    url += ":" + getUrlParameter('port', '8109');
    poll();
}

function poll(){

    $.getJSON(getMessageUrl({id: "_next"}), null, function(data) {
        // enqueue new messages coming from server; ignore no-ops
        if (data != null && "id" in data) {
            if (data.id == "ping") {
                return "ok";
            }
            incoming.push(data);
        }
    });

    // process any incoming messages, one at a time
    if (incoming.length > 0) {
        execute(incoming.shift());
    }
    
    // send any outgoing messages, also one at a time. It's duplex communication, so
    // for now we don't process the response.
    if (outgoing.length > 0) {
    	var msg = outgoing.shift();
        $.get(url,  msg);
    }
    setTimeout(poll, delayMs);
}