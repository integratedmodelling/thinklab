/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.common.authority;

import java.util.ArrayList;

import org.integratedmodelling.api.knowledge.IAuthority;
import org.integratedmodelling.api.knowledge.IAxiom;
import org.integratedmodelling.api.knowledge.IConcept;
import org.integratedmodelling.api.knowledge.IKnowledge;
import org.integratedmodelling.api.knowledge.IOntology;
import org.integratedmodelling.common.configuration.KLAB;
import org.integratedmodelling.common.vocabulary.NS;
import org.integratedmodelling.exceptions.ThinklabValidationException;
import org.integratedmodelling.lang.Axiom;

public abstract class DefaultAbstractAuthority implements IAuthority {

    private IOntology _ontology;

    protected IOntology getOntology() {
        if (_ontology == null) {
            _ontology = KLAB.KM.requireOntology(getAuthorityId().toLowerCase());
        }
        return _ontology;
    }

    protected abstract void checkObservable(IConcept observable)
            throws ThinklabValidationException;

    /**
     * ensure the ID is suitable as a name without becoming ambiguous. This is
     * better done in each authority, knowing how the IDs look like.
     * 
     * @param id
     * @return
     */
    protected String sanitizeID(String id) {
        return id.replaceAll("/", "_");
    }

    @Override
    public IConcept getIdentity(String id) {

        /*
         * check and if necessary, create the trait in our ontology. TODO we
         * should cache all the info related to the ID so that online search is
         * minimized. For now we don't do anything online anyway.
         */
        String cId = getAuthorityId() + "_" + sanitizeID(id);
        IOntology o = getOntology();
        IConcept ret = o.getConcept(cId);
        if (ret == null) {

            ArrayList<IAxiom> ax = new ArrayList<IAxiom>();
            ax.add(Axiom.ClassAssertion(cId));
            ax.add(Axiom.SubClass(NS.CORE_IDENTITY_TRAIT, cId));

            /*
             * TODO get descriptor, add label and other metadata to identify the
             * authority.
             */

            o.define(ax);
            ret = o.getConcept(cId);
        }

        return ret;
    }

    @Override
    public String validateCoreConcept(IKnowledge knowledge, String id) {
        if (!knowledge.is(KLAB.c(NS.CORE_IDENTITY_TRAIT))) {
            return "Concepts identified by the "
                    + getAuthorityId() +
                    " authority must be identities";
        }
        return null;
    }

}
