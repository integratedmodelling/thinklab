/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.common.authority;

import java.util.ArrayList;
import java.util.List;

import org.integratedmodelling.api.knowledge.IConcept;
import org.integratedmodelling.api.metadata.IMetadata;
import org.integratedmodelling.common.configuration.KLAB;
import org.integratedmodelling.common.vocabulary.NS;
import org.integratedmodelling.exceptions.ThinklabValidationException;

public class IUPAC extends DefaultAbstractAuthority {

    public static final String ID = "IUPAC";

    @Override
    public String getAuthorityId() {
        return ID;
    }

    @Override
    public List<IMetadata> search(String query) {
        ArrayList<IMetadata> ret = new ArrayList<IMetadata>();
        return ret;
    }

    @Override
    public String getDescription() {

        return "IUPAC endorses the use of the <b>InChl</b> identifiers for all organic and inorganic compounds.\n\n"
                + "We do not provide a search facility in the application yet. Please search for the InChl "
                + "identifier for the compound you want to annotate (Wikipedia lists them for each compound that has a page)"
                + " and use a form like\n\n"
                + "   <b>im.core:Mass identified as \"1S/H3N/h1H3\" by IUPAC</b>\n\n"
                + "in your annotations.";
    }

    @Override
    public boolean canSearch() {
        // definitely not
        return false;
    }

    @Override
    protected void checkObservable(IConcept observable) throws ThinklabValidationException {
        if (!observable.is(KLAB.c(NS.CORE_PHYSICAL_PROPERTY))) {
            throw new ThinklabValidationException("only physical properties, such as mass or volume, can be identified as a chemical species");
        }
    }
}
