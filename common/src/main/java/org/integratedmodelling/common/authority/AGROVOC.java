/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.common.authority;

import java.util.ArrayList;
import java.util.List;

import org.integratedmodelling.api.knowledge.IConcept;
import org.integratedmodelling.api.knowledge.IKnowledge;
import org.integratedmodelling.api.metadata.IMetadata;
import org.integratedmodelling.common.configuration.KLAB;
import org.integratedmodelling.common.vocabulary.NS;
import org.integratedmodelling.exceptions.ThinklabValidationException;

/**
 * Ah, AgroVOC.
 * 
 * @author Ferd
 *
 */
public class AGROVOC extends DefaultAbstractAuthority {

    public final static String ID = "AGROVOC";

    @Override
    public String getAuthorityId() {
        return ID;
    }

    @Override
    public String getDescription() {
        // TODO
        return "The AGROVOC vocabulary provides standard identifiers for many terms of common use in agriculture.\n\n"
                + "We do not provide a search facility in the application yet. Please connect to the AGROVOC search site at "
                + "<b>http://aims.fao.org/standards/agrovoc/functionalities/search</b> and use a form like\n\n"
                + "   <b>im.ecology:Population identified as \"1931\"  by AGROVOC</b>\n\n"
                + "in your annotations.";
    }

    @Override
    public List<IMetadata> search(String query) {
        ArrayList<IMetadata> ret = new ArrayList<IMetadata>();
        return ret;
    }

    @Override
    public boolean canSearch() {
        // TODO true?
        return false;
    }

    @Override
    protected void checkObservable(IConcept observable) throws ThinklabValidationException {
        // TODO I don't think there is a way to discern what is a proper annotation at this stage.
    }

    @Override
    public String validateCoreConcept(IKnowledge knowledge, String id) {
        /*
         * if an API and enough structure exists, this should check alignment with processes, subjects or
         * traits.
         */
        if (!knowledge.is(KLAB.c(NS.CORE_IDENTITY_TRAIT)) && !NS.isObservable(knowledge)) {
            return "Concepts identified by the "
                    + getAuthorityId() +
                    " authority must be identities or observables";
        }
        return null;
    }

}
