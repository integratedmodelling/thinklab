/*******************************************************************************
 *  Copyright (C) 2007, 2014:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.common.authority;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;

import org.integratedmodelling.api.knowledge.IConcept;
import org.integratedmodelling.api.metadata.IMetadata;
import org.integratedmodelling.common.metadata.Metadata;
import org.integratedmodelling.common.vocabulary.NS;
import org.integratedmodelling.exceptions.ThinklabValidationException;
import org.integratedmodelling.list.Escape;

import us.monoid.json.JSONArray;
import us.monoid.json.JSONObject;
import us.monoid.web.JSONResource;
import us.monoid.web.Resty;

/**
 * Authority to identify species keys re: GBIF
 * http://www.gbif.org/DataProviders/Agreements/DUA
 * @author Ferd
 *
 */
public class GBIF extends DefaultAbstractAuthority {

    public static final String ID = "GBIF";

    @Override
    public String getAuthorityId() {
        return ID;
    }

    @Override
    public List<IMetadata> search(String query) {

        ArrayList<IMetadata> ret = new ArrayList<IMetadata>();

        try {

            JSONResource res = new Resty().json(getSearchURL(query));
            JSONArray ares = res.array();
            // JSONArray ares = (JSONArray) res.get("results");
            for (int i = 0; i < ares.length(); i++) {
                JSONObject ores = (JSONObject) ares.get(i);
                if (ores != null) {
                    Object rank = ores.get("rank");

                    if (!rankOK(rank))
                        continue;

                    Metadata md = new Metadata();
                    md.put("ID", ores.get("speciesKey"));
                    md.put("Label", ores.get("canonicalName"));
                    ret.add(md);
                }
            }

        } catch (Exception e) {
        }

        return ret;
    }

    private boolean rankOK(Object rank) {
        if (rank == null)
            return false;
        String rnk = rank.toString();

        // TODO add search options to select genera, families etc.

        return rnk.equals("SPECIES");
    }

    private URI getSearchURL(String query) throws URISyntaxException {
        // TODO use search instead, once tested - this only returns 20 matches, but search still limits them
        // and returns duplicates
        return new URI("http://api.gbif.org/v0.9/species/suggest?q=" + Escape.forURL(query));
        // return new URI("http://api.gbif.org/v0.9/species/search?q=" + Escape.forURL(query) +
        // "&rank=SPECIES");
    }

    @Override
    public String getDescription() {
        return "<b>Global Biodiversity Information Facility (GBIF)</b>\n\n"
                + "GBIF provides uniform codes for taxonomic entities such as species. This authority provides "
                + "access to the <b>suggest</b> service, which will search for a species matching a string, providing at most 20 matches. "
                + "GBIF matches all taxonomic ranks, but we filter the results to return only species names.\n\nWhen the species "
                + "you are searching for is found, the ID code can be used to construct a Thinklab identity for the species, whose definition can be "
                + "copied to the clipboard by selecting the ID and pressing \"Copy selected\". It can then be pasted in the code after the "
                + "observable you are identifying (e.g. im.ecology:Population). Double-clicking the identifier will "
                + "show more details about the species in this window.\n\n"
                + "For more details, head over to http://gbif.org";
    }

    @Override
    public boolean canSearch() {
        return true;
    }

    @Override
    protected void checkObservable(IConcept observable) throws ThinklabValidationException {
        if (!NS.isThing(observable)) {
            throw new ThinklabValidationException("only subjects, such as individuals or populations, can be identified as a biological species");
        }
    }
}
