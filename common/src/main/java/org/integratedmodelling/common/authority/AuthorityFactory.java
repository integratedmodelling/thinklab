/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.common.authority;

import java.util.Collection;
import java.util.HashMap;

import org.integratedmodelling.api.knowledge.IAuthority;

public class AuthorityFactory {

    HashMap<String, IAuthority>     _authorities = new HashMap<>();
    private static AuthorityFactory _this;

    public static AuthorityFactory get() {
        if (_this == null) {
            _this = new AuthorityFactory();
        }
        return _this;
    }

    private AuthorityFactory() {
        /*
         * TODO this should be extendible by plugins
         */
        _authorities.put(AGROVOC.ID, new AGROVOC());
        _authorities.put(GBIF.ID, new GBIF());
        _authorities.put(IUPAC.ID, new IUPAC());
    }

    public IAuthority getAuthority(String id) {
        return _authorities.get(id);
    }

    public Collection<String> getAuthorityIds() {
        return _authorities.keySet();
    }

}
