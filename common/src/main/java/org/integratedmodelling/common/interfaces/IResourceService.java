package org.integratedmodelling.common.interfaces;

import org.integratedmodelling.api.auth.IUser;
import org.integratedmodelling.exceptions.ThinklabException;

/**
 * A resource handler makes available a generic mechanism for exchanging resources using the engine /get
 * endpoint. In order to work, resource handlers must be tagged with the ResourceService annotation.
 * 
 * @author ferdinando.villa
 *
 */
public abstract interface IResourceService {
    
    /**
     * Check access to URN and if OK, translate it to an object that can be sent to the requester.
     * 
     * Return value: if a bean is returned, it's used directly through JSON mapping. If it's a file, its contents are
     * sent over. If it's a URL, its contents are proxied through the URL.
     * 
     * @param urn
     * @param user
     * @return
     * @throws ThinklabException
     */
    Object resolveUrn(String urn, IUser user) throws ThinklabException;
}
