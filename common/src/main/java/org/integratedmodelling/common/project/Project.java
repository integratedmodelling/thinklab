/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.common.project;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Properties;

import org.apache.commons.io.FileUtils;
import org.integratedmodelling.Version;
import org.integratedmodelling.api.lang.IParsingContext;
import org.integratedmodelling.api.modelling.INamespace;
import org.integratedmodelling.api.project.IProject;
import org.integratedmodelling.common.configuration.KLAB;
import org.integratedmodelling.common.kim.KIM;
import org.integratedmodelling.common.utils.MiscUtilities;
import org.integratedmodelling.common.utils.NameGenerator;
import org.integratedmodelling.common.utils.StringUtils;
import org.integratedmodelling.common.utils.ZipUtils;
import org.integratedmodelling.exceptions.ThinklabException;
import org.integratedmodelling.exceptions.ThinklabIOException;
import org.integratedmodelling.exceptions.ThinklabResourceNotFoundException;
import org.integratedmodelling.exceptions.ThinklabRuntimeException;
import org.integratedmodelling.lang.IRemoteSerializable;

import com.google.common.collect.ImmutableMap;

public class Project implements IProject, IRemoteSerializable {

    /**
     * We can have the project create a namespace then write it using this function instead of 
     * creating an empty one. we take care of opening/closing and refreshing when done.
     * 
     * @author ferdinando.villa
     *
     */
    public static interface NamespaceGenerator {
        void write(String id, boolean isScenario, boolean isAppending, PrintWriter out) throws Exception;
    }

    private final HashMap<String, INamespace> _namespaces = new HashMap<String, INamespace>();

    private final String   _id;
    protected File         _path;
    private File           _pfile;
    private Properties     _properties;
    protected List<String> _dependencies = new ArrayList<>();
    // private final ProjectManager _manager;
    private long           _modificationDate;
    private boolean        _isWrapped;
    private boolean        isWorldview;

    String _srcRelativePath = "src";
    String _binRelativePath = "bin";
    String _libRelativePath = "lib";

    /*
     * package private - set by project manager
     */
    boolean _loaded;

    private final ArrayList<String> _projectErrors = new ArrayList<String>();

    /**
     * This is the "standard" project, expecting knowledge in src/ and all dependencies in 
     * META-INF/thinklab.properties, persisted when settings are changed through the API.
     * 
     * @param path
     * @param manager
     */
    public Project(File path) {

        _path = path;
        _id = MiscUtilities.getFileName(path.toString());

        /*
         * FIXME weak check for wrapped packages vs. user-defined projects.
         */
        File wSrc = new File(_path + File.separator + "src" + File.separator + "main" + File.separator
                + "knowledge");
        File wMvn = new File(_path + File.separator + "target" + File.separator + "classes");
        File wLib = new File(_path + File.separator + "target" + File.separator + "dependency");
        _isWrapped = wSrc.exists() || wMvn.exists();

        if (wSrc.exists()) {
            _srcRelativePath = "src/main/knowledge";
        }
        if (wMvn.exists()) {
            _binRelativePath = "target/classes";
        }
        if (wLib.exists()) {
            _libRelativePath = "target/dependency";
        }

        _properties = getProperties();
        String pp = getProperties().getProperty(IProject.PREREQUISITES_PROPERTY, "");
        String[] deps = pp.isEmpty() ? new String[0]
                : getProperties().getProperty(IProject.PREREQUISITES_PROPERTY, "").split(",");
        for (String s : deps) {
            _dependencies.add(s);
        }
        isWorldview = Boolean
                .parseBoolean(getProperties().getProperty(IProject.IS_WORLDVIEW_PROPERTY, "false"));
    }

    /**
     * This project does not persist properties and is used for wrapped projects coming from
     * components.
     * 
     * @param path
     * @param prerequisites
     * @param relativeSrcPath
     * @param relativeBinPath
     * @param relativeLibPath
     */
    public Project(File path, String[] prerequisites, String relativeSrcPath, String relativeBinPath,
            String relativeLibPath, long modificationDate) {

        _isWrapped = true;
        _modificationDate = modificationDate;
        _path = path;
        _id = MiscUtilities.getFileName(path.toString());
        _properties = getProperties();
        String pp = getProperties().getProperty(IProject.PREREQUISITES_PROPERTY, "");
        String[] deps = pp.isEmpty() ? new String[0]
                : getProperties().getProperty(IProject.PREREQUISITES_PROPERTY, "").split(",");
        for (String s : deps) {
            _dependencies.add(s);
        }
        isWorldview = Boolean
                .parseBoolean(getProperties().getProperty(IProject.IS_WORLDVIEW_PROPERTY, "false"));
    }

    public Project(String id) {
        _id = id;
    }

    public void setSrcRelativePath(String path) {
        _srcRelativePath = path;
    }

    public void setBinRelativePath(String path) {
        _binRelativePath = path;
    }

    public void setLibRelativePath(String path) {
        _libRelativePath = path;
    }

    public boolean isLoaded() {
        return _loaded;
    }

    @Override
    public String getId() {
        return _id;
    }

    public void clear() {
        _namespaces.clear();
        _projectErrors.clear();
        _loaded = false;
    }

    @Override
    public File findResource(String resource) {

        File ff = new File(_path + File.separator + getSourceDirectory() + File.separator + resource);

        if (ff.exists()) {
            return ff;
        }
        return null;

    }

    @Override
    public INamespace findNamespaceForResource(File resource) {

        for (INamespace ns : _namespaces.values()) {
            if (ns.getLocalFile().equals(resource)) {
                return ns;
            }
        }
        return null;
    }

    @Override
    public Properties getProperties() {

        if (_properties == null) {

            _properties = new Properties();

            // if (!_isWrapped) {

            try {
                File pfile = new File(_path + File.separator + "META-INF" + File.separator
                        + "thinklab.properties");

                if (pfile.exists()) {
                    _pfile = pfile;
                    _modificationDate = pfile.lastModified();
                    InputStream inp = null;
                    try {
                        inp = new FileInputStream(pfile);
                        _properties.load(inp);
                        inp.close();
                    } catch (Exception e) {
                        throw new ThinklabIOException(e);
                    }
                }

            } catch (ThinklabException e) {
                throw new ThinklabRuntimeException(e);
            }
        }
        // }
        return _properties;
    }

    @Override
    public File getLoadPath() {
        return _path;
    }

    @Override
    public Collection<INamespace> getNamespaces() {
        return _namespaces.values();
    }

    @Override
    public String getSourceDirectory() {
        return getProperties().getProperty(IProject.SOURCE_FOLDER_PROPERTY, _srcRelativePath);
    }

    @Override
    public String getOntologyNamespacePrefix() {
        return getProperties()
                .getProperty(IProject.ONTOLOGY_NAMESPACE_PREFIX_PROPERTY, IProject.DEFAULT_NAMESPACE_PREFIX);
    }

    @Override
    public File findResourceForNamespace(String namespace) {

        String fp = namespace.replace('.', File.separatorChar);
        for (String extension : new String[] { KIM.FILE_EXTENSION, "owl" }) {
            File ff = new File(_path + File.separator + getSourceDirectory() + File.separator + fp + "."
                    + extension);
            if (ff.exists()) {
                return ff;
            }
        }

        return null;
    }

    @Override
    public List<IProject> getPrerequisites() throws ThinklabException {

        ArrayList<IProject> ret = new ArrayList<IProject>();
        for (String s : _dependencies) {
            IProject p = KLAB.PMANAGER.getProject(s);
            if (p == null) {
                throw new ThinklabResourceNotFoundException("project " + s + " required by project " + _id
                        + " not found in workspace");
            }
            ret.add(p);
        }
        return ret;
    }

    @Override
    public long getLastModificationTime() {

        long lastmod = 0L;
        for (File f : FileUtils.listFiles(_path, null, true)) {
            if (f.lastModified() > lastmod) {
                lastmod = f.lastModified();
            }
        }

        if (_pfile != null && _pfile.lastModified() > lastmod) {
            lastmod = _pfile.lastModified();
        }

        return lastmod;
    }

    @Override
    public boolean equals(Object arg0) {
        return arg0 instanceof Project ? _id.equals(((Project) arg0)._id) : false;
    }

    @Override
    public int hashCode() {
        return _id.hashCode();
    }

    @Override
    public String toString() {
        return "[project " + _id + "]";
    }

    public void addDependency(String plugin) throws ThinklabException {

        String pp = getProperties().getProperty(IProject.PREREQUISITES_PROPERTY, "");
        String[] deps = pp.isEmpty() ? new String[0]
                : getProperties().getProperty(IProject.PREREQUISITES_PROPERTY, "").split(",");

        String dps = "";
        for (String s : deps) {
            if (s.equals(plugin)) {
                return;
            }
            dps += (dps.isEmpty() ? "" : ",") + s;
        }

        dps += (dps.isEmpty() ? "" : ",") + plugin;
        getProperties().setProperty(IProject.PREREQUISITES_PROPERTY, dps);
        deps = dps.split(",");

        persistProperties();

    }

    public void setDependencies(Collection<String> projectIds) throws ThinklabException {

        String dps = StringUtils.join(projectIds, ',');
        getProperties().setProperty(IProject.PREREQUISITES_PROPERTY, dps);
        persistProperties();
        _dependencies.clear();
        _dependencies.addAll(projectIds);
    }

    public void createManifest(String[] dependencies) throws ThinklabException {

        File td = new File(_path + File.separator + "META-INF");
        td.mkdirs();

        new File(_path + File.separator + getSourceDirectory()).mkdirs();

        if (dependencies != null && dependencies.length > 0) {
            for (String d : dependencies) {
                addDependency(d);
            }
        } else {
            persistProperties();
        }
    }

    public void persistProperties() throws ThinklabException {

        if (!_isWrapped) {
            File td = new File(_path + File.separator + "META-INF" + File.separator + "thinklab.properties");

            try {
                getProperties().store(new FileOutputStream(td), null);
            } catch (Exception e) {
                throw new ThinklabIOException(e);
            }
        }
    }

    public File getZipArchive() throws ThinklabException {

        File ret = null;
        try {
            ret = new File(KLAB.CONFIG.getTempArea("pack") + File.separator + NameGenerator.shortUUID()
                    + ".zip");
            ZipUtils.zip(ret, _path, true, false);
            // new ZipArchive(_path, "*.git", "*.settings").archive(ret);
        } catch (Exception e) {
            throw new ThinklabIOException(e);
        }
        return ret;
    }

    public String createNamespace(String ns, boolean isScenario) throws ThinklabException {

        File ret = new File(_path + File.separator + getSourceDirectory() + File.separator
                + ns.replace('.', File.separatorChar) + "." + KIM.FILE_EXTENSION);
        File dir = new File(MiscUtilities.getFilePath(ret.toString()));

        try {
            dir.mkdirs();
            PrintWriter out = new PrintWriter(ret);
            out.println((isScenario ? "scenario " : "namespace ") + ns + ";\n");
            out.close();
        } catch (Exception e) {
            throw new ThinklabIOException(e);
        }

        ((ProjectManager) KLAB.PMANAGER).notifyNewNamespace(this, ns, ret);

        return getSourceDirectory() + File.separator + ns.replace('.', File.separatorChar) + "."
                + KIM.FILE_EXTENSION;
    }

    public String createNamespace(String ns, boolean isScenario, boolean isAppending, NamespaceGenerator generator)
            throws ThinklabException {

        File ret = new File(_path + File.separator + getSourceDirectory() + File.separator
                + ns.replace('.', File.separatorChar) + "." + KIM.FILE_EXTENSION);
        File dir = new File(MiscUtilities.getFilePath(ret.toString()));

        try {
            dir.mkdirs();
            PrintWriter out = new PrintWriter(ret);
            generator.write(ns, isScenario, isAppending, out);
            out.close();
        } catch (Exception e) {
            throw new ThinklabIOException(e);
        }

        ((ProjectManager) KLAB.PMANAGER).notifyNewNamespace(this, ns, ret);
        return getSourceDirectory() + File.separator + ns.replace('.', File.separatorChar) + ".tql";
    }

    public String[] getPrerequisiteIds() {
        return _dependencies.toArray(new String[_dependencies.size()]);
    }

    @Override
    public boolean providesNamespace(String namespaceId) {
        return findResourceForNamespace(namespaceId) != null;
    }

    @Override
    public List<String> getUserResourceFolders() {

        ArrayList<String> ret = new ArrayList<String>();

        for (File f : _path.listFiles()) {
            if (f.isDirectory()
                    && !f.equals(new File(_path + File.separator + getSourceDirectory()))
                    && !((ProjectManager) KLAB.PMANAGER)
                            .isManagedDirectory(MiscUtilities.getFileName(f.toString()), this)) {
                ret.add(MiscUtilities.getFileBaseName(f.toString()));
            }
        }

        return ret;
    }

    @Override
    public boolean hasErrors() {

        if (_projectErrors.size() > 0) {
            return true;
        }

        for (INamespace n : _namespaces.values()) {
            if (n.hasErrors()) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean hasWarnings() {

        for (INamespace n : _namespaces.values()) {
            if (n.hasWarnings()) {
                return true;
            }
        }
        return false;
    }

    @Override
    public Version getVersion() {
        return Version.parse(getProperties().getProperty(IProject.VERSION_PROPERTY, "0.0.0"));
    }

    public void addError(ThinklabException e) {
        _projectErrors.add(e.getMessage());
    }

    @Override
    public INamespace findNamespaceForImport(String namespace, IParsingContext context)
            throws ThinklabException {

        if (_namespaces.containsKey(namespace)) {
            return _namespaces.get(namespace);
        }

        if (KLAB.PMANAGER == null) {
            return null;
        }

        File resource = findResourceForNamespace(namespace);
        if (resource != null && resource.exists()) {

            INamespace ret = context.hasSeen(namespace)
                    ? KLAB.MMANAGER.getNamespace(namespace)
                    : KIM.parse(resource, context.forNamespace(namespace));

            if (ret != null) {
                return ret;
            }
        }

        for (IProject p : getPrerequisites()) {
            INamespace ret = p.findNamespaceForImport(namespace, context.get(p));
            if (ret != null) {
                return ret;
            }
        }

        return null;
    }

    void notifyNamespace(INamespace ns) {
        // if (ns.getProject() != null && ns.getProject().getId().equals(_id))
        _namespaces.put(ns.getId(), ns);
    }

    @Override
    public Collection<File> getScripts() {

        ArrayList<File> ret = new ArrayList<File>();

        File dir = new File(getLoadPath() + File.separator + ".scripts");

        if (dir.exists()) {
            for (File f : dir.listFiles()) {
                if (f.isFile() && f.canRead()
                        && MiscUtilities.getFileExtension(f.toString()).equals("groovy")) {
                    ret.add(f);
                }
            }
        }

        return ret;
    }

    @Override
    public Object adapt() {
        return ImmutableMap.of("id", _id, "last-modification", getLastModificationTime());
    }

    public boolean needsFullRebuild() {
        return false;// _pfile != null && _pfile.lastModified() > _modificationDate;
    }

    public void resetAfterFullBuild() {
        _modificationDate = _pfile == null ? new Date().getTime() : _pfile.lastModified();
    }

    public Collection<String> deleteUnavailableNamespaces() {

        ArrayList<String> ret = new ArrayList<String>();
        for (String s : _namespaces.keySet()) {
            INamespace ns = _namespaces.get(s);
            if (ns.getLocalFile() != null && !ns.getLocalFile().exists()) {
                ret.add(s);
                KLAB.MMANAGER.releaseNamespace(s);
            }
        }

        for (String s : ret) {
            _namespaces.remove(s);
        }

        return ret;
    }

    @Override
    public synchronized void open(boolean open) {
        File check = new File(getLoadPath() + File.separator + "META-INF" + File.separator + ".closed");
        if (open) {
            FileUtils.deleteQuietly(check);
        } else {
            try {
                FileUtils.touch(check);
            } catch (IOException e) {
                throw new ThinklabRuntimeException(e);
            }
        }
    }

    @Override
    public boolean isOpen() {
        return !(new File(getLoadPath() + File.separator + "META-INF" + File.separator + ".closed").exists());
    }

    @Override
    public boolean isRemote() {
        return getProperties().containsKey(REMOTELY_SYNCHRONIZED_GROUP_PROPERTY);
    }

    public File getLibDirectory() {
        return new File(getLoadPath() + File.separator + _libRelativePath);
    }

    public File getBinDirectory() {
        return new File(getLoadPath() + File.separator + _binRelativePath);
    }

    /**
     * Return the relative paths (normalized) to all binary assets in lib dir. Ignores
     * bin (should only be relevant to remote project, which are never in development
     * mode).
     * 
     * @return
     */
    public Collection<String> getBinaryAssetPaths() {

        List<String> ret = new ArrayList<>();
        if (getLibDirectory().exists()) {
            for (File f : getLibDirectory().listFiles()) {
                if (f.toString().endsWith(".jar")) {
                    ret.add(_libRelativePath + "/" + MiscUtilities.getFileName(f.toString()));
                }
            }
        }
        return ret;
    }

    public List<File> getDataAssetPaths() {
        List<File> ret = new ArrayList<>();

        /*
         * TODO use a smarter strategy
         */
        for (String path : new String[] { "data", "assets", "bn" }) {
            File f = new File(_path + File.separator + path);
            if (f.exists() && f.isDirectory()) {
                ret.add(f);
            }
        }

        return ret;
    }

    public String getRelativeSrcPath() {
        return _srcRelativePath;
    }

    public String getRelativeLibPath() {
        return _libRelativePath;
    }

    public void deleteNamespace(String id) {

        INamespace ns = _namespaces.get(id);
        if (ns == null) {
            return;
        }
        _namespaces.remove(id);
        KLAB.MMANAGER.releaseNamespace(id);
        File ff = ns.getLocalFile();
        if (ff.exists()) {
            FileUtils.deleteQuietly(ff);
        }
    }

    @Override
    public boolean isWorldview() {
        return isWorldview;
    }
}
