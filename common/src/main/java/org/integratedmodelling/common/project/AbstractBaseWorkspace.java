/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.common.project;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.integratedmodelling.api.auth.IUser;
import org.integratedmodelling.api.configuration.IConfiguration;
import org.integratedmodelling.api.monitoring.IMonitor;
import org.integratedmodelling.api.network.INetwork;
import org.integratedmodelling.api.project.IProject;
import org.integratedmodelling.api.runtime.IWorkspace;
import org.integratedmodelling.common.configuration.KLAB;
import org.integratedmodelling.common.engine.NetworkedDistribution;
import org.integratedmodelling.common.utils.MiscUtilities;
import org.integratedmodelling.exceptions.ThinklabException;
import org.integratedmodelling.exceptions.ThinklabRuntimeException;

/**
 * Common methods for a workspace - essentially communication with the project manager.
 * 
 * @author ferdinando.villa
 *
 */
public abstract class AbstractBaseWorkspace implements IWorkspace {

    protected ArrayList<File> pDirs    = new ArrayList<>();
    private Set<String>       syncd    = new HashSet<>();
    private boolean           syncDone = false;

    void notifyProjectRegistered(IProject project) {

        if (!getProjectLocations().contains(project.getLoadPath())) {
            getProjectLocations().remove(project.getLoadPath());
        }
    }

    /*
     * compile a list of all projects in synchronized storage and
     * if a project is both in the storage and in the client workspace,
     * keep the latter, otherwise add project to file list and ensure
     * we can inquire whether it's remote.
     */
    protected void synchronizeWorkspace() {

        syncd.clear();

        /*
         * create list of remote project IDs
         */
        File deployDir = new File(KLAB.CONFIG.getDataPath() + File.separator
                + IConfiguration.SUBSPACE_PROJECTS);
        deployDir.mkdirs();

        for (File f : deployDir.listFiles()) {
            // we don't check for closed - we cannot close synchronized projects.
            if (ProjectManager.isThinklabProject(f)) {
                syncd.add(MiscUtilities.getFileName(f.toString()));
            }
        }

        /*
         * remove those overridden in existing client workspace
         */
        for (File f : pDirs) {
            String pid = MiscUtilities.getFileName(f.toString());
            if (syncd.contains(pid)) {
                syncd.remove(pid);
            }
        }

        /*
         * add remaining projects and log actions the first time
         */
        for (File f : deployDir.listFiles()) {
            if (ProjectManager.isThinklabProject(f)) {
                if (syncd.contains(MiscUtilities.getFileName(f.toString()))) {
                    pDirs.add(f);
                    KLAB.info("remote project " + MiscUtilities.getFileName(f.toString())
                            + " scheduled for loading");
                } else {
                    KLAB.info("remote project " + MiscUtilities.getFileName(f.toString())
                            + " overridden in client workspace: client version loaded");
                }
            }
        }
        syncDone = true;
    }

    void notifyProjectUnregistered(IProject project) {

        if (getProjectLocations().contains(project.getLoadPath())) {
            getProjectLocations().remove(project.getLoadPath());
        }
    }

    /**
     * Import all projects that the primary server has sent to us as assets, unless
     * they are already up to date. Return the list of all projects that were successfully
     * synchronized. Ignore any project whose ID is already in the passed list.
     * 
     * Must be called after initial client setup and before the workspace is used for
     * anything.
     * 
     * @param id
     * @param timestamp
     * @throws ThinklabException 
     */
    @Override
    public List<String> synchronizeUserProjects(IUser user, IMonitor monitor) throws ThinklabException {

        ArrayList<String> ret = new ArrayList<>();
        // File deployDir = new File(Env.CONFIG.getDataPath() + File.separator
        // + IConfiguration.SUBSPACE_PROJECTS);
        // deployDir.mkdirs();
        //
        // int np = 0, ne = 0;
        //
        // /*
        // * TODO move to resourceCatalog
        // * FIXME this only gets assets from primary server.
        // */
        // for (String pid : user.getAssets().getCoreProjectNames()) {
        //
        // File pdir = importProject(user, pid, deployDir);
        // if (pdir != null) {
        // ret.add(pid);
        // np++;
        // } else {
        // monitor.error("unable to import remote project " + pid);
        // ne++;
        // }
        // }
        //
        // // tell them all
        // if ((np + ne) > 0) {
        // monitor.info((np > 0 ? (np + " remote projects synchronized") : (ne > 0 ? "; " : "")) +
        // (ne > 0 ? (ne + " errors synchronizing assets") : ""), null);
        // }

        /*
         * redefine the workspace
         */
        synchronizeWorkspace();

        return ret;

    }

    @Override
    public File synchronizeNetworkProject(String projectId, INetwork network) throws ThinklabException {

        KLAB.CONFIG.getDataPath(IConfiguration.SUBSPACE_PROJECTS).mkdirs();
        File pdir = new File(KLAB.CONFIG.getDataPath(IConfiguration.SUBSPACE_PROJECTS) + File.separator
                + projectId);
        File ret = null;
        String url = network.getUrlForProject(projectId);
        if (url != null) {

            /*
             * locally available from assets directory; no sync needed, load from there.
             */
            if (url.startsWith("file:")) {
                try {
                    ret = new File(new URL(url).getFile());
                    KLAB.info("project " + projectId + " is served by this node: loading from " + ret);
                    pdir = ret;
                } catch (MalformedURLException e) {
                    // jesus
                }
            } else {

                NetworkedDistribution nd = new NetworkedDistribution(url, pdir);
                if (nd.sync()) {
                    ret = pdir;
                }
            }
        }

        if (pdir.exists() && ProjectManager.isThinklabProject(pdir)) {
            pDirs.add(pdir);
            syncd.add(projectId);
            synchronizeWorkspace();
        } else {
            return ret;
        }

        return ret;
    }

    @Override
    public final Collection<File> getProjectLocations() {
        return pDirs;
    }

    @Override
    public boolean isRemotelySynchronized(String projectId) {
        if (!syncDone && KLAB.NETWORK.isOnline()) {
            throw new ThinklabRuntimeException("check called before workspace has been synchronized with user");
        }
        return syncd.contains(projectId);
    }
}
