/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.common.project;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Collection;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.integratedmodelling.api.network.INode;
import org.integratedmodelling.collections.Path;
import org.integratedmodelling.common.configuration.KLAB;
import org.integratedmodelling.common.engine.NetworkedDistribution;
import org.integratedmodelling.common.kim.KIM;
import org.integratedmodelling.common.utils.URLUtils;
import org.integratedmodelling.exceptions.ThinklabException;
import org.integratedmodelling.exceptions.ThinklabIOException;
import org.integratedmodelling.exceptions.ThinklabRuntimeException;

import com.google.common.io.Files;

import jodd.util.ClassLoaderUtil;

/**
 * A read-only project that is synchronized from a remote server. Namespace files are downloaded
 * on demand and deleted at the end of the client session.
 * 
 * @author Ferd
 *
 */
public class RemoteProject extends Project {

    String      baseUrl;
    File        localPath;
    static File localProjectPath;
    String      serverId;
    boolean     deleteAtExit;

    Set<String> remoteNamespaces = new HashSet<String>();

    public RemoteProject(String serverId, String projectId, INode node, Map<?, ?> pd, boolean deleteAtExit)
            throws ThinklabException {

        super(projectId);

        this.serverId = serverId;
        this.deleteAtExit = deleteAtExit;

        // remote projects register themselves...
        ((ProjectManager) KLAB.PMANAGER)._projects.put(projectId, this);

        // ... and live in a temporary directory that only exists the lifetime of the engine instance.
        _path = getProjectPath();

        baseUrl = node.getUrl() + (node.getUrl().endsWith("/") ? "" : "/")
                + pd.get("access-key") /* + "/" + projectId */;

        /*
         * first try to sync using a remote synchronizer. If it does not have sync info, 
         * proceed to only retrieve the pieces advertised by the remote service.
         */
        NetworkedDistribution dist = new NetworkedDistribution(baseUrl, _path);
        boolean isSynced = dist.sync();

        setSrcRelativePath(pd.get("src-path").toString());
        setLibRelativePath(pd.get("lib-path").toString());

        if (pd.get("prerequisites") instanceof Collection<?>) {
            for (Object o : ((Collection<?>) pd.get("prerequisites"))) {
                if (KLAB.PMANAGER.getProject(o.toString()) == null) {
                    node.importProject(o.toString());
                }
                _dependencies.add(o.toString());
            }
        }

        if (pd.get("namespaces") instanceof Collection<?>) {
            for (Object o : ((Collection<?>) pd.get("namespaces"))) {
                remoteNamespaces.add(o.toString());
                if (!isSynced) {
                    importNamespace(o.toString());
                }
            }
        }
        boolean binaryLoaded = false;
        if (pd.get("binary-assets") instanceof Collection<?>) {
            for (Object o : ((Collection<?>) pd.get("binary-assets"))) {
                File jar = isSynced ? new File(_path + File.separator + o.toString()) : importAsset(o
                        .toString());
                if (jar.exists()) {
                    binaryLoaded = true;
                    ClassLoaderUtil.addFileToClassPath(jar, ClassLoaderUtil.getDefaultClassLoader());
                }
            }
        }

        if (binaryLoaded) {
            KLAB.ENGINE.rescanClasspath();
        }

        if (pd.get("data-assets") instanceof Collection<?>) {
            if (!isSynced) {
                for (Object o : ((Collection<?>) pd.get("data-assets"))) {
                    importAsset(o.toString());
                }
            }
        }
    }

    private void importNamespace(String string) {
        String apath = Path.getLeading(string, '.');
        apath = apath.replace(".", "/");
        File ipath = new File(getProjectPath() + File.separator + getSourceDirectory() + File.separator
                + apath);
        ipath.mkdirs();
        String fname = Path.getLast(string, '.') + "." + KIM.FILE_EXTENSION;
        File target = new File(ipath + File.separator + fname);
        try {
            URL url = new URL(baseUrl + "/" + getSourceDirectory() + "/" + apath + "/" + fname);
            URLUtils.copy(url, target);
        } catch (ThinklabIOException | MalformedURLException e) {
            throw new ThinklabRuntimeException(e);
        }
    }

    private File importAsset(String string) {

        String apath = Path.getLeading(string, '/');
        File ipath = new File(getProjectPath() + File.separator + apath);
        ipath.mkdirs();
        File target = new File(ipath + File.separator + string);
        try {
            URLUtils.copy(new URL(baseUrl + "/" + string), target);
        } catch (ThinklabIOException | MalformedURLException e) {
            throw new ThinklabRuntimeException(e);
        }
        return target;
    }

    public File getProjectPath() {

        if (localPath == null) {
            if (localProjectPath == null) {
                localProjectPath = Files.createTempDir();
                if (deleteAtExit) {
                    localProjectPath.deleteOnExit();
                }
            }
            localPath = new File(localProjectPath + File.separator + getId());
            localPath.mkdirs();
        }
        return localPath;
    }

    public String getServerId() {
        return serverId;
    }

}
