/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.common.project;

import java.io.File;
import java.io.IOException;
import java.nio.file.FileSystems;
import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.LinkOption;
import java.nio.file.Path;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.StandardWatchEventKinds;
import java.nio.file.WatchEvent;
import java.nio.file.WatchEvent.Kind;
import java.nio.file.WatchKey;
import java.nio.file.WatchService;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.io.FileUtils;
import org.integratedmodelling.api.factories.IProjectFactory;
import org.integratedmodelling.api.factories.IProjectManager;
import org.integratedmodelling.api.lang.IParsingContext;
import org.integratedmodelling.api.modelling.INamespace;
import org.integratedmodelling.api.monitoring.IMonitor;
import org.integratedmodelling.api.monitoring.IProjectLifecycleListener;
import org.integratedmodelling.api.network.INode;
import org.integratedmodelling.api.project.IDependencyGraph;
import org.integratedmodelling.api.project.IProject;
import org.integratedmodelling.collections.Pair;
import org.integratedmodelling.common.configuration.Configuration;
import org.integratedmodelling.common.configuration.KLAB;
import org.integratedmodelling.common.kim.KIM;
import org.integratedmodelling.common.network.Endpoints;
import org.integratedmodelling.common.utils.CamelCase;
import org.integratedmodelling.common.utils.MiscUtilities;
import org.integratedmodelling.common.utils.ZipUtils;
import org.integratedmodelling.exceptions.ThinklabException;
import org.integratedmodelling.exceptions.ThinklabIOException;
import org.integratedmodelling.exceptions.ThinklabResourceNotFoundException;
import org.integratedmodelling.exceptions.ThinklabValidationException;

import jodd.util.ClassLoaderUtil;

public class ProjectManager implements IProjectManager, IProjectFactory {

    HashMap<String, Project> _projects = new HashMap<String, Project>();

    class ResourceInfo {
        INamespace namespace;
        IProject   project;
        boolean    readOnly = false;
        long       modificationDate;
        File       file;
    }

    HashMap<File, ResourceInfo>                           resourceInfo        = new HashMap<File, ResourceInfo>();
    HashMap<String, ResourceInfo>                         nsInfo              = new HashMap<String, ResourceInfo>();

    DependencyGraph                                       dependencyGraph     = null;
    private IProjectLifecycleListener                     listener;

    private Set<File>                                     registeredLocations = new HashSet<>();

    // watcher service to notify of project changes. Complex due to editors creating many
    // events for a single user action.
    private WatchService                                  watcher             = null;
    private HashMap<WatchKey, Path>                       keys                = new HashMap<WatchKey, Path>();
    private HashMap<Path, Pair<Long, WatchEvent.Kind<?>>> fileEvents          = new HashMap<>();

    public ProjectManager() {

        try {
            this.watcher = FileSystems.getDefault().newWatchService();
        } catch (IOException e) {
            // just null
        }

        /*
         * start monitor thread. Will do nothing until there is something
         * to watch.
         */
        new FileMonitor().start();
    }

    /*
     * One of these threads is started the first time a file is modified. It will start a timer and
     * monitor events for the file until the last event on the file is older than a given time. At
     * that point it will report the last file event to the project manager.
     */
    class FileChangeActuator extends Thread {

        Path _file;
        long _idleTime;

        FileChangeActuator(Path file, long idleTime) {
            _file = file;
            _idleTime = idleTime;
        }

        @Override
        public void run() {

            for (;;) {

                long time = new Date().getTime();
                boolean oldEnough = false;

                synchronized (fileEvents) {
                    long delta = time - fileEvents.get(_file).getFirst();
                    oldEnough = delta >= _idleTime;
                }

                if (oldEnough) {
                    Kind<?> lastEvent = null;
                    synchronized (fileEvents) {
                        lastEvent = fileEvents.get(_file).getSecond();
                        fileEvents.remove(_file);
                    }
                    notifyFileEvent(_file, lastEvent);
                    return;
                }

                synchronized (fileEvents) {
                    fileEvents.get(_file).setFirst(time);
                }
                try {
                    Thread.sleep(_idleTime);
                } catch (InterruptedException e) {
                }
            }
        }
    }

    class FileMonitor extends Thread {

        @Override
        public void run() {

            for (;;) {

                /*
                 * re-check every whole second if there is nothing to watch.
                 */
                if (keys.isEmpty()) {
                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException e) {
                    }
                } else {

                    WatchKey key;
                    try {
                        key = watcher.take();
                    } catch (InterruptedException x) {
                        /*
                         * TODO - continue? Signal an error? Does this happen?
                         */
                        return;
                    }

                    Path dir = keys.get(key);
                    if (dir == null) {
                        continue;
                    }

                    for (WatchEvent<?> event : key.pollEvents()) {
                        WatchEvent.Kind<?> kind = event.kind();

                        // TODO - handle OVERFLOW (?)
                        if (kind == StandardWatchEventKinds.OVERFLOW) {
                            continue;
                        }

                        // Context for directory entry event is the file name of entry
                        WatchEvent<Path> ev = cast(event);
                        Path name = ev.context();
                        Path child = dir.resolve(name);

                        if (!isRelevant(child)) {
                            continue;
                        }

                        // TODO use event
                        notifyFileEvent(event, child);

                        // if directory is created, and watching recursively, then
                        // register it and its sub-directories
                        if (kind == StandardWatchEventKinds.ENTRY_CREATE) {
                            try {
                                if (Files.isDirectory(child, LinkOption.NOFOLLOW_LINKS)) {
                                    watchProjectDirectory(child);
                                }
                            } catch (IOException x) {
                                // don't see why this should happen
                            }
                        }
                    }

                    // reset key and remove from set if directory no longer accessible
                    boolean valid = key.reset();
                    if (!valid) {
                        keys.remove(key);

                        // all directories are inaccessible
                        if (keys.isEmpty()) {
                            break;
                        }
                    }
                }
            }
        }

        private void notifyFileEvent(WatchEvent<?> event, Path child) {

            long time = new Date().getTime();

            synchronized (fileEvents) {
                boolean isNew = !fileEvents.containsKey(child);
                fileEvents.put(child, new Pair<Long, WatchEvent.Kind<?>>(time, event.kind()));
                if (isNew) {
                    new FileChangeActuator(child, 500).start();
                }
            }

        }

        private boolean isRelevant(Path child) {
            return child.toString().endsWith("." + KIM.FILE_EXTENSION) ||
                    child.toString().endsWith(".properties") ||
                    child.toString().endsWith(".project") ||
                    child.toString().endsWith(".owl");
        }
    }

    @SuppressWarnings("unchecked")
    static <T> WatchEvent<T> cast(WatchEvent<?> event) {
        return (WatchEvent<T>) event;
    }

    public void notifyFileEvent(Path path, Kind<?> lastEvent) {

        ResourceInfo rinfo = resourceInfo.get(path.toFile());
        if (rinfo != null) {
            if (listener != null && rinfo.namespace != null) {
                if (lastEvent.equals(StandardWatchEventKinds.ENTRY_MODIFY)) {
                    listener.namespaceModified(rinfo.namespace.getId(), rinfo.project);
                } else if (lastEvent.equals(StandardWatchEventKinds.ENTRY_DELETE)) {
                    listener.namespaceDeleted(rinfo.namespace.getId(), rinfo.project);
                }
            }
        } else {
            /*
             * TODO
             * may be a properties file event or a new namespace created.
             */
            IProject project = getProjectForPath(path);
            if (project != null && listener != null) {
                if (path.toString().contains("thinklab.properties") || path.toString().endsWith(".project")) {
                    listener.projectPropertiesModified(project, path.toFile());
                } else {
                    if (lastEvent.equals(StandardWatchEventKinds.ENTRY_MODIFY)) {
                        listener.fileModified(project, path.toFile());
                    } else if (lastEvent.equals(StandardWatchEventKinds.ENTRY_DELETE)) {
                        listener.fileDeleted(project, path.toFile());
                    } else if (lastEvent.equals(StandardWatchEventKinds.ENTRY_CREATE)) {
                        listener.fileCreated(project, path.toFile());
                    }
                }
            }
        }
    }

    private IProject getProjectForPath(Path path) {
        for (Project p : _projects.values()) {
            if (path.toString().startsWith(p.getLoadPath().toString())) {
                return p;
            }
        }
        return null;
    }

    /**
     * Directories in a project that make this one return true are not shown to
     * users.
     * 
     * @param fileBaseName
     *            the BASE name of the directory, without the path.
     * @param project
     *            unused for now
     * @return
     */
    public boolean isManagedDirectory(String fileBaseName, Project project) {
        return fileBaseName.equals("META-INF") || fileBaseName.startsWith(".");
    }

    public static boolean isThinklabProject(File dir) {
        File f = new File(dir + File.separator + "META-INF" + File.separator
                + "thinklab.properties");
        return f.exists();
    }

    /**
     * Check if resource needs to be re-parsed; if so, parse it, reset its info
     * in _resourceInfo and return the namespace; otherwise return null to mean
     * "no action was necessary".
     * 
     * @param resource
     * @param monitor
     * @return
     * @throws ThinklabException
     */
    INamespace checkIn(File resource, IProject project, String namespaceId, IParsingContext context)
            throws ThinklabException {

        ResourceInfo info = resourceInfo.get(resource);

        if (info == null || info.modificationDate < resource.lastModified()) {

            if (info == null) {
                info = new ResourceInfo();
            }

            /*
             * update and store info before we read. If anything bad happens,
             * the NS in the descriptor will be null.
             */
            info.modificationDate = resource.lastModified();
            info.project = project;
            resourceInfo.put(resource, info);

            INamespace ns = (context.hasSeen(namespaceId))
                    ? KLAB.MMANAGER.getNamespace(namespaceId)
                    : KIM.parse(resource, context.forNamespace(namespaceId));

            info.namespace = ns;
            if (ns == null) {
                KLAB.error("result of parsing namespace " + namespaceId + " is null");
            } else {
                ((Project) project).notifyNamespace(ns);
            }
            return ns;
        }

        return null;
    }

    @Override
    public void addListener(IProjectLifecycleListener listener) {
        this.listener = listener;
    }

    @Override
    public IProject getProject(String projectId) {
        return _projects.get(projectId);
    }

    @Override
    public Collection<IProject> getProjects() {
        ArrayList<IProject> ret = new ArrayList<IProject>();
        for (Project p : _projects.values())
            ret.add(p);
        return ret;
    }

    /**
     * Go through every registered project and refresh its contents, reading
     * whatever resource has changed or wasn't seen before. Return the list of
     * all the namespaces that were modified compared to the previous load().
     * 
     * @throws ThinklabException
     */
    @Override
    public synchronized List<INamespace> load(boolean forceReload, IParsingContext context)
            throws ThinklabException {

        if (forceReload) {
            KLAB.MMANAGER.releaseAll();
            resourceInfo.clear();
            for (IProject p : getProjects()) {
                ((Project) p).clear();
            }
        }

        ArrayList<INamespace> ret = new ArrayList<INamespace>();
        HashSet<IProject> pread = new HashSet<IProject>();
        for (IProject p : getProjects()) {
            ret.addAll(loadProject((Project) p, pread, context.get(p)));
        }

        /*
         * refresh everything that can be affected. If we don't have a
         * dependency graph, we are loading for the first time so nothing needs
         * to be refreshed.
         */
        if (dependencyGraph != null) {

            HashSet<INamespace> reloaded = new HashSet<INamespace>(ret);
            for (INamespace ns : ret) {
                for (INamespace dep : getDependencyGraph().getDependents(ns)) {
                    if (!reloaded.contains(dep) && dep.getLocalFile() != null
                            && dep.getProject() != null) {
                        reloaded.add(checkIn(dep.getLocalFile(), dep
                                .getProject(), dep.getId(), context));
                    }
                }
            }

            /*
             * included reloaded NS in returned list.
             */
            ret.clear();
            ret.addAll(reloaded);
        }

        rebuildDependencyGraph();

        if (listener != null) {
            listener.onReload(forceReload);
        }

        return ret;
    }

    private void rebuildDependencyGraph() {

        dependencyGraph = new DependencyGraph();

        for (IProject p : getProjects()) {
            for (INamespace s : p.getNamespaces()) {
                dependencyGraph.addVertex(s.getId());
            }
        }

        for (String s : dependencyGraph.vertexSet()) {
            for (INamespace ns : KLAB.MMANAGER.getNamespace(s)
                    .getImportedNamespaces()) {
                if (ns != null
                        /* happens in error, we don't want NPEs here */
                        && dependencyGraph.containsVertex(s)
                        && dependencyGraph.containsVertex(ns.getId())) {
                    dependencyGraph.addEdge(s, ns.getId());
                }
            }
        }
    }

    /**
     * Count the worldviews. Obviously meant to ensure there is always and only one.
     * 
     * @return
     */
    public int countWorldviews() {
        int ret = 0;
        for (IProject p : _projects.values()) {
            if (p.isWorldview()) {
                ret++;
            }
        }
        return ret;
    }

    /*
     * "smart" project loader.
     */
    private List<INamespace> loadProject(Project project, Set<IProject> read, IParsingContext context) {

        List<INamespace> ret = new ArrayList<INamespace>();

        if (read.contains(project))
            return ret;

        read.add(project);

        try {
            for (IProject p : project.getPrerequisites()) {
                ret.addAll(loadProject((Project) p, read, context.get(p)));
            }
        } catch (ThinklabException e) {
            project.addError(e);
        }

        /*
         * dispose of any namespaces backed by files that were removed.
         */
        for (String ns : project.deleteUnavailableNamespaces()) {
            if (listener != null) {
                listener.namespaceDeleted(ns, project);
            }
        }
        ;

        /**
         * load any binary code in the project if we're running as an engine. 
         * 
         * NOTE: this is not using any real plugin 
         * environment (very much on purpose) so unloading the project will not unload the
         * code, and reloading may have unintended consequences in some cases. In other words,
         * best to use projects with java class content only when they're not reloaded - which
         * is OK with the current use (components).
         */
        if (KLAB.isEngine()) {

            if (project.getLibDirectory().exists()) {

                KLAB.info("loading binary components for project " + project.getId() + ": "
                        + project.getLibDirectory());

                for (File f : project.getLibDirectory().listFiles()) {
                    if (f.toString().endsWith(".jar")) {
                        ClassLoaderUtil.addFileToClassPath(f, ClassLoaderUtil.getDefaultClassLoader());
                    }
                }
            }

            if (project.getBinDirectory().exists()) {

                KLAB.info("loading classes from development tree for project " + project.getId()
                        + ": " + project.getBinDirectory());

                ClassLoaderUtil.addFileToClassPath(project.getBinDirectory(), ClassLoaderUtil
                        .getDefaultClassLoader());
            }
        }
        /*
         * load each file in project's source directory
         */
        File srcDir = new File(project.getLoadPath() + File.separator
                + project.getSourceDirectory());
        if (srcDir.exists()) {
            for (File fl : srcDir.listFiles()) {
                loadFile(fl, "", project, ret, context);
            }
        } else {
            KLAB.warn("project " + project.getId() + " has no source files");
        }

        /*
         * register with watcher
         */
        if (watcher != null && project.getLoadPath() != null) {
            try {
                watchProjectDirectory(project.getLoadPath().toPath());
            } catch (IOException e) {
                // throw new ThinklabIOException(e);
            }
        }

        return ret;
    }

    private void loadFile(File f, String path, Project project, List<INamespace> ret, IParsingContext context) {

        String pth = path == null ? ""
                : (path + (path.isEmpty() ? "" : ".")
                        + CamelCase.toLowerCase(MiscUtilities.getFileBaseName(f.toString()), '-'));

        // if (!forceReload) {
        // forceReload = project.needsFullRebuild();
        // if (forceReload) {
        // System.out.println(project.getId() + " project needs full rebuild");
        // }
        // }

        if (f.isDirectory()) {

            for (File fl : f.listFiles()) {
                loadFile(fl, pth, project, ret, context);
            }

        } else if (KLAB.MMANAGER.isModelFile(f)) {
            INamespace ns = null;
            try {
                ns = checkIn(f, project, pth, context);
            } catch (ThinklabException e) {
                project.addError(e);
            }
            if (ns != null) {
                ret.add(ns);
            }
        }
    }

    @Override
    public void undeployProject(String projectId) throws ThinklabException {

        Project project = _projects.get(projectId);
        if (project != null) {
            unloadProject(projectId);
            unregisterProject(projectId);
        }
    }

    @Override
    public IProject deployProject(String pluginId, String resourceId, IMonitor monitor)
            throws ThinklabException {

        if (getProject(pluginId) != null) {
            undeployProject(pluginId);
        }

        File destdir = new File(KLAB.CONFIG.getDataPath() + File.separator + Configuration.SUBSPACE_PROJECTS);
        File destprj = new File(destdir + File.separator + pluginId);
        destdir.mkdirs();
        if (destprj.exists()) {
            try {
                FileUtils.deleteDirectory(destprj);
            } catch (IOException e) {
                throw new ThinklabIOException(e);
            }
        }
        ZipUtils.unzip(new File(resourceId), destdir);

        for (String p : registerProject(destprj)) {
            KLAB.info("registered deployed project " + p + " into " + destdir);
        }

        return getProject(pluginId);
    }

    @Override
    public List<String> registerProject(File... projectDir)
            throws ThinklabException {

        ArrayList<String> ret = new ArrayList<String>();

        for (int i = 0; i < projectDir.length; i++) {

            if (registeredLocations.contains(projectDir[i])) {
                continue;
            }

            registeredLocations.add(projectDir[i]);

            String projectId = MiscUtilities.getFileName(projectDir[i]
                    .toString());
            Project project = _projects.get(projectId);
            boolean success = false;

            if (project == null) {
                project = new Project(projectDir[i]);
                if (project.isOpen()) {
                    ret.add(project.getId());
                    _projects.put(project.getId(), project);
                    success = true;
                }
            }
            if (success && listener != null) {
                listener.projectRegistered(project);
            }
            if (success && KLAB.WORKSPACE instanceof AbstractBaseWorkspace) {
                ((AbstractBaseWorkspace) KLAB.WORKSPACE).notifyProjectRegistered(project);
            }
        }

        return ret;
    }

    @Override
    public List<INamespace> loadProject(String projectId, IParsingContext context)
            throws ThinklabException {

        Project project = _projects.get(projectId);
        if (project == null) {
            throw new ThinklabResourceNotFoundException("cannot load unknown project " + projectId);
        }

        // IModelResolver resolver = Env.MMANAGER.getRootResolver();
        HashSet<IProject> pread = new HashSet<IProject>();

        List<INamespace> ret = loadProject(project, pread, context.get(project));

        // /*
        // * register with watcher
        // */
        // if (_watcher != null) {
        // try {
        // watchProjectDirectory(project.getLoadPath().toPath());
        // } catch (IOException e) {
        // throw new ThinklabIOException(e);
        // }
        // }

        return ret;
    }

    @Override
    public void unregisterProject(String projectId) throws ThinklabException {

        Project project = _projects.get(projectId);
        if (project != null) {
            if (project.isLoaded()) {
                unloadProject(projectId);
            }
            _projects.remove(projectId);
            if (listener != null) {
                listener.projectUnregistered(project);
            }
            if (KLAB.WORKSPACE instanceof AbstractBaseWorkspace) {
                ((AbstractBaseWorkspace) KLAB.WORKSPACE).notifyProjectUnregistered(project);
            }
        }
    }

    @Override
    public void registerProjectDirectory(File projectDirectory)
            throws ThinklabException {

        /*
         * register all projects in configured directory
         */
        ArrayList<File> pdirs = new ArrayList<File>();
        if (projectDirectory.exists() && projectDirectory.isDirectory()) {
            for (File f : projectDirectory.listFiles()) {
                if (isThinklabProject(f)) {
                    pdirs.add(f);
                }
            }
        }
        if (pdirs.size() > 0) {
            registerProject(pdirs.toArray(new File[pdirs.size()]));
        }
    }

    /**
     * Register the given directory and all its sub-directories with the
     * watch service.
     */
    private void watchProjectDirectory(final Path start) throws IOException {
        // register directory and sub-directories
        Files.walkFileTree(start, new SimpleFileVisitor<Path>() {
            @Override
            public FileVisitResult preVisitDirectory(Path dir, BasicFileAttributes attrs)
                    throws IOException {
                watch(dir);
                return FileVisitResult.CONTINUE;
            }
        });
    }

    /**
     * Register the given directory with the WatchService
     */
    private void watch(Path dir) throws IOException {
        WatchKey key = dir
                .register(watcher, StandardWatchEventKinds.ENTRY_CREATE, StandardWatchEventKinds.ENTRY_DELETE, StandardWatchEventKinds.ENTRY_MODIFY);
        // if (trace) {
        // Path prev = keys.get(key);
        // if (prev == null) {
        // System.out.format("register: %s\n", dir);
        // } else {
        // if (!dir.equals(prev)) {
        // System.out.format("update: %s -> %s\n", prev, dir);
        // }
        // }
        // }
        keys.put(key, dir);
    }

    @Override
    public void unloadProject(String projectId) throws ThinklabException {

        IProject p = _projects.get(projectId);
        if (p != null) {

            if (watcher != null) {
                /*
                 * TODO unregister watcher
                 */
            }

            for (INamespace s : p.getNamespaces()) {
                KLAB.MMANAGER.releaseNamespace(s.getId());
            }
        }
    }

    @Override
    public IProject createProject(File projectPath, String[] prerequisites)
            throws ThinklabException {

        String pid = MiscUtilities.getFileName(projectPath.toString());

        IProject project = _projects.get(pid);

        if (project != null)
            throw new ThinklabValidationException("cannot create already existing project: " + pid);

        project = new Project(projectPath);
        ((Project) project).createManifest(prerequisites);

        return project;
    }

    @Override
    public void deleteProject(String projectId) throws ThinklabException {

        IProject p = _projects.get(projectId);
        if (p == null)
            throw new ThinklabResourceNotFoundException("project " + projectId
                    + " does not exist");
        File path = p.getLoadPath();

        undeployProject(projectId);

        try {
            FileUtils.deleteDirectory(path);
        } catch (IOException e) {
            throw new ThinklabIOException(e);
        }
    }

    @Override
    public File archiveProject(String projectId) throws ThinklabException {

        IProject p = _projects.get(projectId);

        if (p == null)
            throw new ThinklabResourceNotFoundException("project " + projectId
                    + " does not exist");

        return ((Project) p).getZipArchive();
    }

    @Override
    public IDependencyGraph getDependencyGraph() {
        return dependencyGraph;
    }

    @Override
    public boolean hasBeenLoaded() {
        return dependencyGraph != null;
    }

    public void notifyNewNamespace(Project project, String ns, File ret) {
        // TODO register file with file monitor
        if (listener != null) {
            listener.namespaceAdded(ns, project);
        }
        // TODO add to model manager (not needed now because the clients do that, but it should be automatic)
    }

    @Override
    public Collection<INamespace> renameNamespace(INamespace namespace, String newName)
            throws ThinklabException {
        // TODO Auto-generated method stub
        return null;
    }

    /**
     * Call on closed projects to open it and register it. Otherwise opening a non-existing
     * project is a tough operation to do. 
     * 
     * TODO see if this should be floated to the API, or there is a more elegant way.
     * 
     * @param file
     * @throws ThinklabException 
     */
    public List<String> openAndRegisterProject(File... files) throws ThinklabException {

        for (File file : files) {
            File check = new File(file + File.separator + "META-INF" + File.separator + ".closed");
            if (check.exists()) {
                FileUtils.deleteQuietly(check);
            }
        }
        return registerProject(files);
    }

    public static boolean isOpen(File file) {
        File check = new File(file + File.separator + "META-INF" + File.separator + ".closed");
        return !check.exists();
    }

    @Override
    public IProject importProject(INode node, String projectId) throws ThinklabException {

        Object pd = node.get(Endpoints.PROJECT, "cmd", "export", "plugin", projectId);

        if (pd instanceof Map<?, ?>) {
            /*
             * TODO/CHECK: not deleting on exit because large assets should not be lost. This will
             * leave things on disk which may be inappropriate to keep.
             */
            return new RemoteProject(node.getId(), projectId, node, (Map<?, ?>) pd, false);
        }

        return null;
    }
}
