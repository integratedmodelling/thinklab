/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.common.indexing;

import java.util.ArrayList;
import java.util.Iterator;

import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.integratedmodelling.api.metadata.IMetadata;
import org.integratedmodelling.api.modelling.IDirectObserver;
import org.integratedmodelling.exceptions.ThinklabException;

public class ObservationIndexer implements KnowledgeIndex.IndexAdapter {

    IDirectObserver observer;

    @Override
    public Iterator<Document> iterator() {
        ArrayList<Document> rret = new ArrayList<>();
        if (observer != null) {
            rret.add(getDocument(observer));
        }
        return rret.iterator();
    }

    @Override
    public String getUniversalQuery() {
        if (observer == null) {
            return null;
        }
        return "name:" + observer.getName();
    }

    @Override
    public void initialize(Object object) throws ThinklabException {
        this.observer = (IDirectObserver) object;
    }

    private Document getDocument(IDirectObserver c) {

        Document ret = new Document();
        String type = "S";

        ret.add(new Field("id", c.getName(), Field.Store.YES, Field.Index.NO));
        ret.add(new Field("type", type, Field.Store.YES, Field.Index.NO));
        ret.add(new Field("name", c.getId(), Field.Store.YES, Field.Index.ANALYZED));
        ret.add(new Field("namespace", c.getNamespace().getId(), Field.Store.YES, Field.Index.ANALYZED));

        IMetadata md = c.getMetadata();
        String comment = md.getString(IMetadata.DC_COMMENT);
        String label = md.getString(IMetadata.DC_LABEL);

        if (comment != null) {
            ret.add(new Field("description", comment, Field.Store.YES,
                    Field.Index.ANALYZED));
        }
        if (label != null) {
            ret.add(new Field("label", label, Field.Store.YES,
                    Field.Index.ANALYZED));
        }

        return ret;
    }

}
