/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.common.indexing;

import java.io.File;
import java.util.ArrayList;
import java.util.Iterator;

import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.integratedmodelling.api.knowledge.IConcept;
import org.integratedmodelling.api.metadata.IMetadata;
import org.integratedmodelling.api.modelling.IDirectObserver;
import org.integratedmodelling.api.modelling.IKnowledgeObject;
import org.integratedmodelling.api.modelling.IModel;
import org.integratedmodelling.api.modelling.IModelObject;
import org.integratedmodelling.api.modelling.INamespace;
import org.integratedmodelling.common.vocabulary.NS;
import org.integratedmodelling.exceptions.ThinklabException;

public class NamespaceIndexer implements KnowledgeIndex.IndexAdapter {

    INamespace _namespace;

    class DocumentIterator implements Iterator<Document> {

        Iterator<Object>  _it;
        ArrayList<Object> _objects = new ArrayList<Object>();

        DocumentIterator() {
            if (_namespace != null) {
                for (IModelObject o : _namespace.getAllModelObjects()) {
                    if (o instanceof IKnowledgeObject) {

                        /*
                         * TODO do more with annotations
                         */
                        if (!NS.getAnnotations(o, "internal").isEmpty()) {
                            continue;
                        }

                        if (((IKnowledgeObject) o).isConcept()) {
                            _objects.add(((IKnowledgeObject) o).getConcept());
                        }
                    } else if (o instanceof IModel) {
                        _objects.add(o);
                    }
                }
            }
            _it = _objects.iterator();
        }

        @Override
        public boolean hasNext() {
            return _it.hasNext();
        }

        @Override
        public Document next() {
            Object next = _it.next();
            return next instanceof IConcept ? getDocument((IConcept) next)
                    : getDocument((IModelObject) next);
        }

        @Override
        public void remove() {
        }

    }

    @Override
    public Iterator<Document> iterator() {
        return new DocumentIterator();
    }

    @Override
    public void initialize(Object object) throws ThinklabException {
        INamespace namespace = (INamespace) object;
        if (namespace.getResourceUrl() != null) {
            /*
             * FIXME resourceUrl should be a URL not a file; must fix that
             * at the source and handle it consistently.
             */
            File f = namespace.getResourceUrl();
            // if (!f.exists()) {
            // URL url = new URL(namespace.getResourceUrl());
            // f = new File(url.getFile());
            // }
            _namespace = namespace;
        }
    }

    private Document getDocument(IConcept c) {

        Document ret = new Document();

        ret.add(new Field("id", c.toString(), Field.Store.YES, Field.Index.NO));
        ret.add(new Field("type", "C", Field.Store.YES, Field.Index.NO));
        ret.add(new Field("name", c.getLocalName(), Field.Store.YES, Field.Index.ANALYZED));
        ret.add(new Field("namespace", c.getConceptSpace(), Field.Store.YES, Field.Index.ANALYZED));

        IMetadata md = c.getMetadata();

        String comment = md.getString(IMetadata.DC_COMMENT);
        String label = md.getString(IMetadata.DC_LABEL);

        if (comment != null) {
            ret.add(new Field("description", comment, Field.Store.YES, Field.Index.ANALYZED));
        }
        if (label != null) {
            ret.add(new Field("label", label, Field.Store.YES, Field.Index.ANALYZED));
        }

        return ret;
    }

    private Document getDocument(IModelObject c) {

        if (c.getErrorCount() > 0) {
            return null;
        }

        if (c instanceof IKnowledgeObject) {
            return ((IKnowledgeObject) c).getConcept() == null ? null
                    : getDocument(((IKnowledgeObject) c).getConcept());
        }

        Document ret = new Document();

        String type = null;
        if (c instanceof IModel) {
            if (((IModel) c).isResolved()) {
                type = "D";
            } else {
                type = ((IModel) c).getObserver() == null ? "A" : "M";
            }
        } else if (c instanceof IDirectObserver) {
            type = "S";
        } else {
            return null;
        }

        ret.add(new Field("id", c.getName(), Field.Store.YES, Field.Index.NO));
        ret.add(new Field("type", type, Field.Store.YES, Field.Index.NO));
        ret.add(new Field("name", c.getId(), Field.Store.YES, Field.Index.ANALYZED));
        ret.add(new Field("namespace", c.getNamespace().getId(), Field.Store.YES, Field.Index.ANALYZED));

        IMetadata md = c.getMetadata();

        String comment = md.getString(IMetadata.DC_COMMENT);
        String label = md.getString(IMetadata.DC_LABEL);

        if (comment != null) {
            ret.add(new Field("description", comment, Field.Store.YES, Field.Index.ANALYZED));
        }
        if (label != null) {
            ret.add(new Field("label", label, Field.Store.YES, Field.Index.ANALYZED));
        }

        return ret;
    }

    @Override
    public String getUniversalQuery() {
        if (_namespace == null) {
            return null;
        }
        return "namespace:" + _namespace.getId();
    }

}
