/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.common.errormanagement;

import org.apache.commons.lang.exception.ExceptionUtils;
import org.integratedmodelling.api.errormanagement.ICompileError;
import org.integratedmodelling.api.modelling.INamespace;

public class CompileError extends CompileNotification implements ICompileError {

    Throwable cause;

    public CompileError(String namespace, String message, int line) {
        super(namespace, message, line);
    }

    public CompileError(INamespace namespace, String message, int line) {
        super(namespace, message, line);
    }

    public CompileError(INamespace namespace, Throwable throwable, int line) {
        super(namespace, ExceptionUtils.getMessage(throwable), line);
        this.cause = throwable;
    }

    @Override
    public String toString() {
        return namespace + ": " + getLineNumber() + ": error: " + getMessage();
    }

    @Override
    public boolean equals(Object o) {
        return o instanceof Error && getLineNumber() == ((CompileError) o).getLineNumber()
                && getMessage().equals(((CompileError) o).getMessage());
    }

    @Override
    public int hashCode() {
        return ((getNamespace() == null ? "(nothing)" : getNamespace().getId()) + toString()).hashCode();
    }
}
