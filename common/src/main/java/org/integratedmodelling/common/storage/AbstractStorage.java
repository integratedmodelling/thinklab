/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.common.storage;

import java.lang.reflect.Array;
import java.util.List;

import org.integratedmodelling.api.modelling.IScale;
import org.integratedmodelling.api.modelling.IScale.Locator;
import org.integratedmodelling.api.modelling.storage.IStorage;
import org.integratedmodelling.exceptions.ThinklabException;
import org.integratedmodelling.exceptions.ThinklabRuntimeException;

public abstract class AbstractStorage<T> extends SliceStorage implements IStorage<T> {

    IStorage<?> dStore      = null;
    boolean     singleValue = false;
    Object      latest;

    protected Object toStorage(Object o) {
        return o;
    }

    protected Object fromStorage(Object o) {
        return o;
    }

    public AbstractStorage(IScale scale, boolean isDynamic) {
        super(scale, isDynamic);
        singleValue = scale.getMultiplicity() == 1
                || (scale.isTemporallyDistributed() && scale.getTime().getMultiplicity() == scale
                        .getMultiplicity());
    }

    boolean isDirty = false;

    @Override
    public boolean isDynamic() {
        return this.isDynamic;
    }

    /**
    * Use this to define whether the storage is dynamic or not.
    * @param isDynamic
    */
    public void setDynamic(boolean isDynamic) {
        this.isDynamic = isDynamic;
    }

    @Override
    public void flush(PODStorage storage, int currentTimeslice, int nextTimeslice) {
        // do nothing
    }

    @Override
    public void set(Object data, Locator... locators) {

        if (data == null) {
            return;
        }

        if (locators != null && dStore != null) {
            dStore.set(toStorage(data), locators);
        } else {
            if (data.getClass().isArray()) {
                for (int i = 0; i < Array.getLength(data); i++) {
                    set(i, Array.get(data, i));
                }
            } else if (data instanceof List) {
                List<?> coll = (List<?>) data;
                for (int i = 0; i < coll.size(); i++) {
                    set(i, coll.get(i));
                }
            } else {
                throw new ThinklabRuntimeException("cannot set a state from a "
                        + data.getClass().getCanonicalName());
            }
        }
        isDirty = true;
    }

    @Override
    public int size() {
        return (int) scale.getMultiplicity();
    }

    @Override
    public String getEncodedBytes(Locator... locators) throws ThinklabException {
        return dStore == null ? storage.getEncodedBytes() : dStore.getEncodedBytes();
    }

    @Override
    public void setBytes(String encodedBytes, Locator... locators) {
        if (dStore == null)
            storage.setBytes(encodedBytes);
        else
            dStore.setBytes(encodedBytes, locators);
        isDirty = true;
    }

    @Override
    public Object getNonCurrentValue(int fullOffset, int timeslice, int nonTemporalOffset) {
        if (dStore != null) {
            return fromStorage(dStore.get(fullOffset));
        }
        return null;
    }

    @Override
    public void set(int offset, Object value) {

        if (singleValue) {
            /*
             * FIXME this should check that the timeslice is the latest.
             */
            latest = value;
        }

        if (dStore != null) {
            dStore.set(offset, toStorage(value));
        } else {
            super.set(offset, value);
        }
        isDirty = true;
    }

    @Override
    public void setChanged(boolean b) {
        isDirty = b;
    }

    @Override
    public boolean hasChanged() {
        return isDirty;
    }

    @Override
    public Object getLatestAggregatedValue() {
        return latest;
    }

}
