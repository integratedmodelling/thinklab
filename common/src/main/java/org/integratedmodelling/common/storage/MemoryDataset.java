/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.common.storage;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

import org.integratedmodelling.api.modelling.IObservable;
import org.integratedmodelling.api.modelling.IScale;
import org.integratedmodelling.api.modelling.IScale.Locator;
import org.integratedmodelling.api.modelling.scheduling.ITransition;
import org.integratedmodelling.api.modelling.storage.IDataset;
import org.integratedmodelling.api.modelling.storage.IStorage;
import org.integratedmodelling.exceptions.ThinklabException;

/**
 * An in-memory dataset for small storage, which can be written to CSV files.
 * 
 * @author ferdinando.villa
 *
 */
public class MemoryDataset implements IDataset {

    Map<IObservable, InlineStorage> variables  = new HashMap<>();
    IScale                          scale;
    long                            nslices    = 1;
    long                            slicesize;
    int                             latestTime = -1;

    class InlineStorage extends AbstractStorage<Object> {

        Object[][] storage;
        boolean    dynamic;
        Class<?>   dataClass = null;
        double     min       = Double.NaN;
        double     max       = Double.NaN;
        boolean    changed;

        InlineStorage(IScale scale, boolean dynamic) {
            super(scale, dynamic);
            this.dynamic = dynamic;
            int tsize = dynamic ? (int) nslices : 1;
            storage = new Object[tsize][(int) slicesize];
        }

        @Override
        public Object get(int index) {
            return storage[getTimeslice(index)][getNonTemporalOffset(index)];
        }

        @Override
        public void set(int index, Object value) {
            if (dataClass == null && value != null) {
                dataClass = value.getClass();
            }
            if (value instanceof Number) {
                double dv = ((Number) value).doubleValue();
                if (!Double.isNaN(dv)) {
                    if (Double.isNaN(min) || min > dv) {
                        min = dv;
                    }
                    if (Double.isNaN(max) || max < dv) {
                        max = dv;
                    }
                }
            }
            storage[latestTime = getTimeslice(index)][getNonTemporalOffset(index)] = value;
            changed = true;
        }

        @Override
        public void set(Object data, Locator... locators) {
            // TODO
            changed = true;
        }

        @Override
        public int size() {
            return (int) scale.getMultiplicity();
        }

        @Override
        public boolean isDynamic() {
            return dynamic;
        }

        @Override
        public void setBytes(String encodedBytes, Locator... locators) {
            // TODO Auto-generated method stub
            changed = true;
        }

        @Override
        public Class<?> getDataClass() {
            return dataClass;
        }

        @Override
        public void flush(ITransition incomingTransition) {
        }

        @Override
        public double getMin() {
            return min;
        }

        @Override
        public double getMax() {
            return max;
        }

        @Override
        public void setChanged(boolean b) {
            changed = b;
        }

        @Override
        public boolean hasChanged() {
            return changed;
        }

        @Override
        public Object getLatestAggregatedValue() {
            if (latestTime >= 0 && slicesize == 1) {
                return storage[latestTime][0];
            }
            return null;
        }

    }

    public MemoryDataset(IScale scale) {
        this.scale = scale;
        if (scale.isTemporallyDistributed()) {
            nslices = scale.getTime().getMultiplicity();
        }
        slicesize = scale.getMultiplicity() / nslices;
    }

    @Override
    public IScale getScale() {
        return scale;
    }

    @Override
    public File persist(String location) throws ThinklabException {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public IStorage<?> getStorage(IObservable observable, boolean dynamic) {
        if (variables.containsKey(observable)) {
            return variables.get(observable);
        }
        InlineStorage ret = new InlineStorage(scale, dynamic);
        variables.put(observable, ret);
        return ret;
    }

}
