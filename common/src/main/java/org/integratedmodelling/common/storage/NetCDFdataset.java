/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.common.storage;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.mutable.MutableBoolean;
import org.integratedmodelling.api.knowledge.IKnowledge;
import org.integratedmodelling.api.modelling.IClassifyingObserver;
import org.integratedmodelling.api.modelling.IConditionalObserver;
import org.integratedmodelling.api.modelling.IExtent;
import org.integratedmodelling.api.modelling.IMeasuringObserver;
import org.integratedmodelling.api.modelling.INumericObserver;
import org.integratedmodelling.api.modelling.IObservable;
import org.integratedmodelling.api.modelling.IPresenceObserver;
import org.integratedmodelling.api.modelling.IScale;
import org.integratedmodelling.api.modelling.IScale.Locator;
import org.integratedmodelling.api.modelling.scheduling.ITransition;
import org.integratedmodelling.api.modelling.storage.IDataset;
import org.integratedmodelling.api.modelling.storage.IStorage;
import org.integratedmodelling.api.space.IGrid;
import org.integratedmodelling.api.space.ISpatialExtent;
import org.integratedmodelling.api.time.ITemporalExtent;
import org.integratedmodelling.common.kim.KIMConditional;
import org.integratedmodelling.common.model.runtime.Scale;
import org.integratedmodelling.common.storage.PODStorage.Type;
import org.integratedmodelling.common.time.TimeLocator;
import org.integratedmodelling.common.vocabulary.GeoNS;
import org.integratedmodelling.common.vocabulary.NS;
import org.integratedmodelling.common.vocabulary.Observable;
import org.integratedmodelling.exceptions.ThinklabException;
import org.integratedmodelling.exceptions.ThinklabIOException;
import org.integratedmodelling.exceptions.ThinklabRuntimeException;

import ucar.ma2.Array;
import ucar.ma2.ArrayDouble;
import ucar.ma2.DataType;
import ucar.ma2.Index;
import ucar.nc2.Attribute;
import ucar.nc2.Dimension;
import ucar.nc2.Group;
import ucar.nc2.NetcdfFileWriter;
import ucar.nc2.NetcdfFileWriter.Version;
import ucar.nc2.Variable;

/**
 * Wrapper over a NetCDF to use for storage as well as for caching of temporal simulations in a
 * DatasetBackedState.
 */
public class NetCDFdataset implements IDataset {

    private static final String CONVENTIONS_ATTRIBUTE = "Conventions";
    private static final String CREATION_TIME         = "CreationTime";
    private static final String SCALE_ATTRIBUTE       = "ThinklabScale";

    private static final String CONVENTION = "CF-1.6";

    private IScale           _scale;
    private File             _file;
    private NetcdfFileWriter _ncfile;
    private boolean          _open     = false;
    private Group            _group    = null;
    // private long _sliceMultiplicity;
    boolean                  _readOnly = true;
    private Variable         _time     = null;
    Set<String>              _varnames = new HashSet<>();

    /*
     * dimensions result from the initial serialization of scale
     */
    List<Dimension>                  _dimensions = null;
    HashMap<IObservable, CFVariable> _vars       = new HashMap<IObservable, CFVariable>();
    private int                      _lastTime   = -1;

    public class CFVariable extends SliceStorage implements IStorage<Object> {

        private IObservable observable;
        private Variable    ncvar;
        private Array       data;
        private int[]       ones;            // this is just to avoid reallocating every
                                             // time we read.
        private int         lastFlushed = -1;

        // Semaphore writing = new Semaphore(1, true);

        public CFVariable(IObservable o, boolean dynamic) throws ThinklabException {

            super(_scale, dynamic);

            if (o.getType().toString().equals("aesthetic.qc.source:ForestBeauty")) {
                System.out.println("ZIZO");
            }

            if (o.getObserver() instanceof IConditionalObserver) {
                o = ((KIMConditional) o.getObserver()).getRepresentativeObserver().getObservable();
            }
            observable = o;

            PODStorage storage = null;

            if (o.getObserver() instanceof IPresenceObserver) {
                storage = new PODStorage(getSliceMultiplicity(), Type.BOOLEAN);
            } else if (o.getObserver() instanceof IClassifyingObserver) {
                storage = new PODStorage(getSliceMultiplicity(), Type.INT_IDX);
            } else if (o.getObserver() instanceof INumericObserver) {
                if (((INumericObserver) o.getObserver()).getDiscretization() != null) {
                    // TODO for now we store the means.
                    storage = new PODStorage(getSliceMultiplicity(), Type.DOUBLE);
                } else {
                    storage = new PODStorage(getSliceMultiplicity(), Type.DOUBLE);
                }
            }
            this.storage = storage;

            /**
             * create var with time dimension only if dynamic. Flush will only
             * work at currentSlice = 0.
             */
            String varname = getVarname(observable);
            ArrayList<Integer> indexes = new ArrayList<Integer>();
            for (IExtent ext : _scale) {
                if (ext instanceof ITemporalExtent && !isDynamic) {
                    continue;
                }
                // reverse order - only matters for space, and this thing wants lat/lon
                int[] dims = ext.getDimensionSizes();
                for (int i = dims.length - 1; i >= 0; i--) {
                    indexes.add(dims[i]);
                }
            }

            int[] idxx = new int[indexes.size()];
            this.ones = new int[indexes.size()];
            for (int i = 0; i < indexes.size(); i++) {
                idxx[i] = indexes.get(i);
                ones[i] = 1;
            }
            defineMode(true);
            ncvar = _ncfile.addVariable(null, varname, storage.getDataType(), _dimensions);
            defineMode(false);
        }

        @Override
        protected void finalize() throws Throwable {
            close();
            super.finalize();
        }

        @Override
        public void flush(PODStorage storage, int currentTimeslice, int nextTimeslice) {

            // Env.logger.info("FLUSHING " + observable + ": " + currentTimeslice + " -> " + nextTimeslice);

            // try {
            // writing.acquire();
            // } catch (InterruptedException e1) {
            // throw new ThinklabRuntimeException("cannot acquire write lock on " + ncvar);
            // }

            if (lastFlushed == currentTimeslice) {
                return;
            }

            try {

                writeTime(currentTimeslice);

                defineMode(true);
                // keep this to avoid continuous allocations
                if (this.data == null) {
                    this.data = Array.factory(storage.getDataType(), getSliceDimensions());
                }
                Index index = data.getIndex();
                for (int i = 0; i < storage.size(); i++) {
                    MutableBoolean nodata = new MutableBoolean(false);
                    int[] dimensionOffsets = getDimensionOffsets(i, nodata);
                    if (_time != null && isDynamic) {
                        // we set this using the origin, leave it at 0 for unlimited dimension.
                        dimensionOffsets[0] = 0;
                    }
                    data.setObject(index.set(dimensionOffsets), translateBooleans(storage.getPOD(i)));
                }

                defineMode(false);
                int[] origin = new int[_dimensions.size()];
                if (_time != null && isDynamic) {
                    origin[0] = timeslice;
                }
                _ncfile.write(ncvar, origin, data);

            } catch (Exception e) {
                throw new ThinklabRuntimeException(e);
            } finally {
                // writing.release();
            }

            lastFlushed = currentTimeslice;
        }

        private Object translateBooleans(Object pod) {
            return pod instanceof Boolean ? ((Boolean) pod ? 1 : 0) : pod;
        }

        @Override
        public void set(Object data, Locator... locators) {

            if (locators.length > 1) {
                throw new ThinklabRuntimeException("access error: dataset cannot set bytes in anything other than a transition");
            }
            getPODStorage().set(data);
        }

        @Override
        public int size() {
            return (int) _scale.getMultiplicity();
        }

        @Override
        public String getEncodedBytes(Locator... locators) throws ThinklabIOException {
            return getPODStorage().getEncodedBytes();
        }

        @Override
        public void setBytes(String encodedBytes, Locator... locators) {
            if (locators != null && locators.length > 1) {
                throw new ThinklabRuntimeException("access error: dataset cannot set bytes in anything other than a transition");
            }
            getPODStorage().setBytes(encodedBytes);
        }

        @Override
        public Class<?> getDataClass() {
            return storage.getDataClass();
        }

        @Override
        public boolean isDynamic() {
            return isDynamic;
        }

        public PODStorage getPODStorage() {
            return storage;
        }

        @Override
        public Object getNonCurrentValue(int fullOffset, int timeslice, int nonTemporalOffset) {

            // try {
            // writing.acquire();
            // } catch (InterruptedException e1) {
            // throw new ThinklabRuntimeException("cannot read variable due to concurrent writing");
            // }

            try {
                if (!isDynamic) {
                    return storage.get(nonTemporalOffset);
                }
                MutableBoolean nodata = new MutableBoolean(false);
                int[] dimensionOffsets = getOffsets(fullOffset, nodata);
                try {
                    Array aret = nodata.booleanValue() ? null : ncvar.read(dimensionOffsets, ones);
                    if (aret != null) {
                        aret.reduce();
                        return aret.getObject(0);
                    } else {
                        return Double.NaN;
                    }
                } catch (Exception e) {
                    throw new ThinklabRuntimeException(e);
                }
            } finally {
                // writing.release();
            }
        }

        @Override
        public Object get(int index) {
            return getAt(index);
        }

        @Override
        public void flush(ITransition incomingTransition) {

            TimeLocator loc = TimeLocator.get(incomingTransition);
            if (loc != null && _time != null && !loc.isInitialization()) {
                if (lastFlushed < loc.getSlice() - 1) {
                    if ((loc.getSlice() - lastFlushed) > 2) {
                        throw new ThinklabRuntimeException("access error: dataset not flushed or invalid random access");
                    }
                    flush(storage, lastFlushed + 1, lastFlushed + 2);
                    setTimeslice(loc.getSlice());
                }
            }
        }

        @Override
        public double getMin() {
            return Double.NaN;
        }

        @Override
        public double getMax() {
            return Double.NaN;
        }

        @Override
        public void setChanged(boolean b) {
            // TODO Auto-generated method stub

        }

        @Override
        public boolean hasChanged() {
            // TODO Auto-generated method stub
            return false;
        }

        @Override
        public Object getLatestAggregatedValue() {
            // TODO Auto-generated method stub
            return null;
        }
    }

    public NetCDFdataset(File file) throws ThinklabException {
        _file = file;
        open();
    }

    private void writeTime(int currentTimeslice) throws ThinklabIOException {
        if (_time == null || currentTimeslice <= _lastTime) {
            return;
        }
        _lastTime = currentTimeslice;
        long time = _scale.getTime().getExtent(currentTimeslice).getStart().getMillis();
        Array timeData = Array.factory(DataType.DOUBLE, new int[] { 1 });
        timeData.setDouble(timeData.getIndex(), time);
        int[] timeOrigin = new int[] { currentTimeslice };
        try {
            _ncfile.write(_time, timeOrigin, timeData);
        } catch (Exception e) {
            throw new ThinklabIOException(e);
        }
    }

    private void open() throws ThinklabIOException {

        try {
            if (_file.exists()) {
                _ncfile = NetcdfFileWriter.openExisting(_file.toString());
            } else {
                _ncfile = NetcdfFileWriter.createNew(Version.netcdf3, _file.toString());
                _ncfile.create();
            }
        } catch (IOException e) {
            throw new ThinklabIOException(e);
        }

        _open = true;
    }

    public NetCDFdataset(IScale scale) throws ThinklabException {

        try {
            _file = File.createTempFile("nctmp", "nc");
        } catch (IOException e) {
            throw new ThinklabIOException(e);
        }
        _file.deleteOnExit();
        _scale = scale;
        if (_file.exists()) {
            FileUtils.deleteQuietly(_file);
        }

        open();
        writeGlobalAttributes();

        _readOnly = false;
    }

    public void close() throws ThinklabException {

        if (_open) {

            flush();
            _open = false;
            try {
                _ncfile.close();
            } catch (IOException e) {
                throw new ThinklabIOException(e);
            }
        }
    }

    private void flush() {
        // TODO Auto-generated method stub
    }

    private void defineMode(boolean b) throws ThinklabException {
        try {
            _ncfile.setRedefineMode(b);
        } catch (IOException e) {
            throw new ThinklabIOException(e);
        }
    }

    private void writeGlobalAttributes() throws ThinklabException {

        defineMode(true);
        _ncfile.addGroupAttribute(null, new Attribute(CONVENTIONS_ATTRIBUTE, CONVENTION));
        // FLOAT due to LONG raising an exception at runtime.
        _ncfile.addGroupAttribute(null, new Attribute(CREATION_TIME, new Float(new Date().getTime())));
        _ncfile.addGroupAttribute(null, new Attribute(SCALE_ATTRIBUTE, Scale.asString(_scale)));
        defineMode(false);

        // _cursor = _scale.getCursor();
        /*
         * if we have the timeless cursor, we have >1 dimensionality in other extents; otherwise each slice
         * has dimensionality 1.
         */
        if (_scale.getTime() != null && _scale.getExtentCount() > 1
                && _scale.getMultiplicity() > _scale.getTime().getMultiplicity()) {

        }

        serializeScale(_scale);
    }

    private void serializeScale(IScale scale) throws ThinklabException {

        /*
         * write dimensions details
         */
        ArrayList<Dimension> dimensions = new ArrayList<Dimension>();
        for (IExtent e : scale) {
            if (e.getDomainConcept().equals(NS.TIME_DOMAIN)) {
                serializeTime(scale.getTime(), dimensions);
            } else if (e.getDomainConcept().equals(NS.SPACE_DOMAIN)) {
                serializeSpace(scale.getSpace(), dimensions);
            } else {
                serializeExtent(e, dimensions);
            }
        }

        _dimensions = dimensions;
    }

    private void serializeTime(ITemporalExtent time, List<Dimension> dims) throws ThinklabException {

        if (time.getMultiplicity() > 1) {

            defineMode(true);
            Dimension tDim = _ncfile.addUnlimitedDimension("time");
            _time = _ncfile.addVariable(null, "time", DataType.DOUBLE, "time");
            // TODO should probably use natural step in scale when appropriate, assuming we carry over the
            // unit and resolution.
            _time.addAttribute(new Attribute("units", "milliseconds since EPOCH"));
            defineMode(false);
            dims.add(tDim);
        }

    }

    private void serializeExtent(IExtent e, List<Dimension> dims) {
        // TODO Auto-generated method stub
    }

    private void serializeSpace(ISpatialExtent space, List<Dimension> dims) throws ThinklabException {

        /*
         * TODO write bounding box, shape, total multiplicity, other statistics in attributes
         */

        if (space.getGrid() != null) {

            defineMode(true);

            IGrid grid = space.getGrid();

            Dimension latDim = _ncfile.addDimension(_group, "lat", grid.getYCells());
            Dimension lonDim = _ncfile.addDimension(_group, "lon", grid.getXCells());

            /* add latitude and longitude as variables */
            Variable latitude = _ncfile.addVariable(_group, "lat", DataType.DOUBLE, Collections
                    .singletonList(latDim));
            _ncfile.addVariableAttribute(latitude, new Attribute("units", "degrees_north"));
            _ncfile.addVariableAttribute(latitude, new Attribute("long_name", "latitude"));

            /* add latitude and longitude as variables */
            Variable longitude = _ncfile.addVariable(_group, "lon", DataType.DOUBLE, Collections
                    .singletonList(lonDim));
            _ncfile.addVariableAttribute(longitude, new Attribute("units", "degrees_east"));
            _ncfile.addVariableAttribute(longitude, new Attribute("long_name", "longitude"));

            double latDelta = (space.getMaxX() - space.getMinX()) / grid.getXCells();
            double lonDelta = (space.getMaxY() - space.getMinY()) / grid.getYCells();

            ArrayDouble alat = new ArrayDouble.D1(latDim.getLength());
            Index ind1 = alat.getIndex();
            double xcn = space.getMinY();
            for (int i = 0; i < latDim.getLength(); i++) {
                alat.setDouble(ind1.set(i), xcn);
                xcn += latDelta;
            }

            ArrayDouble alon = new ArrayDouble.D1(lonDim.getLength());
            Index ind2 = alon.getIndex();
            xcn = space.getMinX();
            for (int i = 0; i < lonDim.getLength(); i++) {
                alon.setDouble(ind2.set(i), xcn);
                xcn += lonDelta;
            }

            /*
             * TODO write mask
             */
            defineMode(false);

            try {
                _ncfile.write(latitude, alat);
                _ncfile.write(longitude, alon);
            } catch (Exception e) {
                throw new ThinklabIOException(e);
            }

            dims.add(latDim);
            dims.add(lonDim);

        } // TODO else write features if any
    }

    /*
     * Extract the proper name for a state. TODO this needs a lot of work. The metadata from the model and
     * concept should be considered.
     * 
     * just recognize some concepts that have special meaning for the netcdf CF convention
     */
    private String getVarname(IObservable observable) {

        GeoNS.synchronize();

        IKnowledge type = observable.getType();

        String ret = type.getLocalName() + "_" + type.getConceptSpace();
        if (((Observable) observable).getObservingSubjectId() != null) {
            ret = ((Observable) observable).getObservingSubjectId() + "_" + ret;
        }
        ret = ret.replace('.', '_');

        if (GeoNS.ELEVATION != null && type.equals(GeoNS.ELEVATION)
                && observable.getObserver() instanceof IMeasuringObserver
                && ((IMeasuringObserver) observable.getObserver()).getDiscretization() == null) {
            ret = "Altitude";
        }

        /*
         * ensure we have no duplicates
         */
        String rbase = ret;
        int n = 1;
        while (_varnames.contains(ret)) {
            ret = rbase + "_" + n;
            n++;
        }

        _varnames.add(ret);

        return ret;
    }

    @Override
    public File persist(String location) throws ThinklabException {
        File ret = new File(location);
        if (!location.endsWith(".nc")) {
            location = location + ".nc";
        }
        close();
        try {
            FileUtils.copyFile(_file, ret);
        } catch (IOException e) {
            throw new ThinklabIOException(e);
        }
        return ret;
    }

    public void dispose() {
        try {
            close();
            FileUtils.deleteQuietly(_file);
        } catch (Exception e) {
            // throw new ThinklabRuntimeException(e);
        }
    }

    @Override
    public IScale getScale() {
        return _scale;
    }

    @Override
    public IStorage<?> getStorage(IObservable observable, boolean dynamic) {

        CFVariable ret = _vars.get(observable);
        if (ret == null) {
            try {
                ret = new CFVariable(observable, dynamic);
            } catch (ThinklabException e) {
                throw new ThinklabRuntimeException(e);
            }
            _vars.put(observable, ret);
        }

        return ret;
    }

}
