/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.common.storage;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.Collection;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.integratedmodelling.api.modelling.IScale;
import org.integratedmodelling.api.modelling.IScale.Locator;
import org.integratedmodelling.api.modelling.scheduling.ITransition;
import org.integratedmodelling.api.modelling.storage.IStorage;
import org.integratedmodelling.exceptions.ThinklabIOException;
import org.integratedmodelling.exceptions.ThinklabRuntimeException;

/**
 * For when it's needed.
 * 
 * @author ferdinando.villa
 *
 */
public class ConstStorage implements IStorage<Object> {

    Object  object;
    IScale  scale;
    boolean changed;

    public ConstStorage(Object object, IScale scale) {
        this.object = object;
        this.scale = scale;
    }

    @Override
    public Object get(int n) {
        return object;
    }

    @Override
    public int size() {
        return (int) scale.getMultiplicity();
    }

    @Override
    public String getEncodedBytes(Locator... locators) throws ThinklabIOException {
        if (!(object instanceof Serializable)) {
            throw new ThinklabIOException("cannot serialize an object of class "
                    + object.getClass().getCanonicalName());
        }
        try (ByteArrayOutputStream b = new ByteArrayOutputStream();
                ObjectOutputStream o = new ObjectOutputStream(b)) {
            o.writeObject(object);
            return Base64.encodeBase64String(b.toByteArray());
        } catch (IOException e) {
            throw new ThinklabIOException(e);
        }
    }

    @Override
    public void setBytes(String encodedBytes, Locator... locators) {
        // TODO Auto-generated method stub
        byte[] bytes = Base64.decodeBase64(encodedBytes.getBytes());
        try (ByteArrayInputStream b = new ByteArrayInputStream(bytes);
                ObjectInputStream o = new ObjectInputStream(b);) {
            object = o.readObject();
        } catch (Exception e) {
            throw new ThinklabRuntimeException("const state: cannot deserialize object: "
                    + ExceptionUtils.getRootCauseMessage(e));
        }
        changed = true;
    }

    @Override
    public Class<?> getDataClass() {
        return object.getClass();
    }

    @Override
    public void set(int index, Object value) {
        object = value;
        changed = true;
    }

    @Override
    public void set(Object data, Locator... locators) {
        if (data.getClass().isArray() || data instanceof Collection) {
            throw new ThinklabRuntimeException("cannot set a constant state from an array of values");
        }
        if (data instanceof ConstStorage) {
            object = ((ConstStorage) data).object;
        } else {
            object = data;
        }
        changed = true;
    }

    @Override
    public boolean isDynamic() {
        return false;
    }

    @Override
    public void flush(ITransition incomingTransition) {
    }

    @Override
    public double getMin() {
        return Double.NaN;
    }

    @Override
    public double getMax() {
        return Double.NaN;
    }

    @Override
    public void setChanged(boolean b) {
        changed = b;
    }

    @Override
    public boolean hasChanged() {
        return changed;
    }

    @Override
    public Object getLatestAggregatedValue() {
        return object;
    }

}
