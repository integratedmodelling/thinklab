/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.common.storage;

import org.integratedmodelling.api.modelling.IClassification;
import org.integratedmodelling.api.modelling.IObservable;
import org.integratedmodelling.api.modelling.IScale;
import org.integratedmodelling.api.modelling.scheduling.ITransition;
import org.integratedmodelling.api.modelling.storage.IDataset;

public class NumberStorage extends AbstractStorage<Double> {

    public double min = Double.NaN;
    public double max = Double.NaN;

    public NumberStorage(IObservable observable, IScale scale, IClassification discretization,
            IDataset dataset, boolean isDynamic) {

        super(scale, isDynamic);
        if (dataset == null) {
            storage = new PODStorage(getSliceMultiplicity(), PODStorage.Type.DOUBLE);
        } else {
            dStore = dataset.getStorage(observable, isDynamic);
        }
    }

    @Override
    public Double get(int index) {
        return dStore == null ? (Double) getAt(index) : (Double) dStore.get(index);
    }

    @Override
    public Class<?> getDataClass() {
        return Number.class;
    }

    @Override
    public void set(int offset, Object value) {
        if (value instanceof Number) {
            double dv = ((Number) value).doubleValue();
            if (!Double.isNaN(dv)) {
                if (Double.isNaN(min) || min > dv) {
                    min = dv;
                }
                if (Double.isNaN(max) || max < dv) {
                    max = dv;
                }
            }
        }
        super.set(offset, value);
    }

    @Override
    public void flush(ITransition incomingTransition) {
        if (isDynamic && dStore != null) {
            dStore.flush(incomingTransition);
        }
    }

    @Override
    public double getMin() {
        return min;
    }

    @Override
    public double getMax() {
        return max;
    }

}
