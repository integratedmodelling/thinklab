/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.common.network;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.joda.time.DateTime;

public class Announcements {

    String             endpoint;
    List<Announcement> data = new ArrayList<>();

    public class Announcement {
        String   subject;
        String   body;
        String   link;
        DateTime date;
        boolean  isNew;

        public Announcement(Map<?, ?> a) {
            this.subject = a.get("subject").toString();
            this.body = a.get("body").toString();
            this.date = new DateTime(a.get("started"));
            this.link = endpoint + (endpoint.endsWith("/") ? "" : "/") + Endpoints.AUTH_GET_ANNOUNCEMENT + "/"
                    + a.get("id");
        }

        public String getTitle() {
            return subject;
        }

        public String getText() {
            return body;
        }

        public String getLink() {
            return link;
        }
    }

    public Announcements(String endpointURL, List<Map<?, ?>> data) {

        this.endpoint = endpointURL;
        for (Map<?, ?> a : data) {
            this.data.add(new Announcement(a));
        }
    }

    public Announcements() {
        // no news is good news
    }

    /**
     * Get only the new announcements, based on the date of
     * last retrieval stored in properties. If no date of
     * last retrieval is available, only return open 
     * announcements.
     * 
     * @return
     */
    public List<Announcement> getNew() {

        List<Announcement> ret = new ArrayList<>();
        for (Announcement aa : data) {
            // TODO select
            ret.add(aa);
        }
        return ret;

    }

    public List<Announcement> get(Date from, Date to) {

        List<Announcement> ret = new ArrayList<>();

        return ret;
    }

}
