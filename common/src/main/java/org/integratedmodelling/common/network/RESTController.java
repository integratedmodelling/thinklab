/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.common.network;

import java.util.HashMap;
import java.util.Map;

import org.integratedmodelling.api.auth.IUser;
import org.integratedmodelling.api.rest.IRESTController;
import org.integratedmodelling.common.configuration.KLAB;
import org.integratedmodelling.common.engine.JSONdeserializer;
import org.integratedmodelling.common.utils.StringUtils;
import org.integratedmodelling.exceptions.ThinklabException;

/**
 * An object that offers an easy interface to REST Thinklab endpoints while managing the 
 * security context of the call automatically. Endpoints will receive headers identifying
 * the calling end and, if set, the receiving end automatically.
 * 
 * @author ferdinando.villa
 *
 */
public class RESTController implements Endpoints, IRESTController {

    IUser  user;
    String thinklabEndpoint;
    String sessionId;
    String nodeKey;
    String remoteKey;

    @Override
    public IUser getUser() {
        return user;
    }

    public RESTController(String url, String serverKey) {
        this.thinklabEndpoint = url;
        this.nodeKey = serverKey;
    }

    public RESTController(String url, String localKey, String remoteKey) {
        this(url, KLAB.NETWORK.getUser());
        this.nodeKey = localKey;
        this.remoteKey = remoteKey;
    }

    public RESTController(String url, IUser user) {
        this.thinklabEndpoint = url;
        this.user = user;
    }

    @Override
    public IRESTController forNode(String url, String remoteKey) {
        RESTController ret = new RESTController(url, getUser());
        ret.nodeKey = this.nodeKey;
        ret.remoteKey = remoteKey;
        return ret;
    }

    @Override
    public String getUrl() {
        return thinklabEndpoint;
    }

    /**
     * Called to connect a personal engine (user not null) to a public engine for
     * authorization. After this returns a valid key, the context is capable of issuing
     * authenticated calls to the engine.
     * @return
     */
    @Override
    public String connect() {
        try {
            String ret = (String) get(Endpoints.CONNECT, "username", this.user.getUsername(), "key", this.user
                    .getSecurityKey());
            remoteKey = ret;
            return ret;

        } catch (ThinklabException e) {
            return null;
        }
    }

    /**
     * Calls a GET service in the local server with passing all authentication information from the
     * known context.
     *   
     * @param url
     * @param command
     * @param args
     * @return
     * @throws ThinklabException
     */
    @Override
    public Object get(String endpoint, Object... args) throws ThinklabException {
        return JSONdeserializer.get(JSONdeserializer.getURL(thinklabEndpoint, endpoint, args), getHeaders());
    }

    /**
     * Call a POST service in the local server with passing all authentication information from the
     * known context.
     * 
     * @param endpoint
     * @param args
     * @return
     * @throws ThinklabException
     */
    @Override
    public Object post(String endpoint, Object... args) throws ThinklabException {
        Map<String, String> headers = getHeaders();
        return JSONdeserializer
                .postWithHeaders(thinklabEndpoint + (thinklabEndpoint.endsWith("/") ? "" : "/")
                        + (endpoint.startsWith("/") ? endpoint.substring(1) : endpoint), headers, args);

    }

    private Map<String, String> getHeaders() {
        Map<String, String> ret = new HashMap<>();
        if (nodeKey != null && user == null) {
            ret.put(CALLING_ENGINE_KEY_HEADER, nodeKey);
        }
        if (remoteKey != null) {
            ret.put(RECEIVING_ENGINE_KEY_HEADER, remoteKey);
        }
        if (sessionId != null) {
            ret.put(SESSION_AUTHORIZATION_HEADER, sessionId);
        }
        if (user != null) {
            ret.put(USERNAME_INFO_HEADER, user.getUsername());
            ret.put(REQUESTER_GROUPS_HEADER, StringUtils.join(user.getGroups(), ","));
            ret.put(REQUESTER_ROLES_HEADER, StringUtils.join(user.getRoles(), ","));
        }
        return ret;
    }

    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }

    @Override
    public boolean responds() {
        try {
            Object o = get("/");
            return o instanceof Map;
        } catch (Throwable e) {
            return false;
        }
    }

    public void resetSession() {
        this.sessionId = null;
    }
}
