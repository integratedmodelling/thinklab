/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.common.network;

/**
 * Next-generation substitute for Endpoints in this same package. Used in Spring-based kLAB 
 * implementation.
 * 
 * @author ferdinando.villa
 *
 */
public class API {

    /*
     * services for all engines. All engines also admit the "ping" service - just the base URL + slash.
     */
    public static final String IDENTIFY     = "/identify";
    public static final String STATUS       = "/status";
    public static final String CAPABILITIES = "/capabilities";

    /*
     * introspection main endpoint
     */
    public static final String INSPECT = "/inspect";

    /*
     * services for personal engines only (any engine as long as locked for personal use)
     */
    public static final String OBSERVE                  = "/observe";
    public static final String CONTEXT                  = "/context";
    public static final String AUTHENTICATE             = "/authenticate";

    // FIXME can probably be substituted by /status when a session is active
    // unless we do it through websockets, which we probably should.
    public static final String GET_NOTIFICATIONS   = "/get-notifications";
    public static final String PROJECT             = "/project";
    public static final String IMPORT_OBSERVATIONS = "/import-observations";

    /**
     * Services for nodes - those shared with the current collaboration server define the
     * eventual merge.
     */
    public static final String AUTHENTICATION_CERT_FILE = "/authentication/cert-file";

    /**
     * Gives access to all resources mediated by a k.LAB server. The service part identifiers the
     * type of service being accessed. A URN identifies the actual resources. According to the service,
     * GET arguments may be added. If the service proxies to another REST service (such as wfs/wcs),
     * the result will be the redirection to the actual service after URN resolution, with the same
     * arguments as in the original request. 
     * 
     * Engines by default implement only the 'file', 'directory', 'project' and 'component' services.
     * Other services can be added using the ResourceServiceHandler annotation on an object implementing
     * IResourceServiceHandler.
     */
    public static final String GET_RESOURCE = "/get/{service}/{urn}";

    /*
     * --- predefined URNs that are hard-wired to the engines.
     */

    /*
     * Directory of core knowledge. TODO move to where it belongs so that clients can
     * also use it.
     */
    public static final String CORE_KNOWLEDGE_URN = "im:ks:core.knowledge";
}
