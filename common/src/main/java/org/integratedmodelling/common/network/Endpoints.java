/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.common.network;

/**
 * Holder for all k.LAB engine API endpoints.
 * 
 * @author Ferd
 *
 */
public interface Endpoints {

    /*
     * services for all engines. All engines also admit the "ping" service - just the base URL + slash.
     */
    public static final String IDENTIFY     = "identify";
    public static final String CAPABILITIES = "capabilities";

    /*
     * services for public engines that have been connected to through a valid primary server
     */
    public static final String CONNECT               = "connect";
    public static final String GET_NODE_CAPABILITIES = "get-node-capabilities";
    public static final String GET_ASSET             = "get-asset";
    public static final String MANAGE_COMPONENTS     = "component-manager";
    public static final String SUBMIT_OBSERVATION    = "submit-observation";
    public static final String RETRIEVE_OBSERVATION  = "retrieve-observation";
    public static final String QUERY_OBSERVATIONS    = "query-observations";
    public static final String REMOVE_OBSERVATIONS   = "remove-observations";
    public static final String QUERY_MODELS          = "thinklab.search";
    public static final String IMPORT_OBSERVATIONS   = "import-observations";

    /*
     * services for personal engines only (any engine as long as locked for personal use)
     */
    public static final String OBSERVE           = "observe";
    public static final String CONTEXT           = "context";
    public static final String AUTHENTICATE      = "authenticate";
    public static final String GET_NOTIFICATIONS = "get-notifications";
    public static final String PROJECT           = "project";

    /*
     * administration endpoints (TODO)
     */
    public static final String ADMIN_COMPONENTS = "component";
    public static final String SHUTDOWN         = "shutdown";
    public static final String GET_ACTIVE_USERS = "get-active-users";

    /*
     * endpoints recognized at collaboration server (Spring services)
     */
    public static final String AUTH_AUTHENTICATE           = "authentication";
    public static final String AUTH_AUTHENTICATE_CERT_FILE = "authentication/cert-file";
    public static final String AUTH_PROFILE                = "engine/profile";
    public static final String AUTH_DATASOURCES            = "datasources";
    public static final String AUTH_GET_DATASOURCE_ENGINE  = "engine/datasource";
    public static final String AUTH_GET_DATASOURCE_USER    = "datasource-urn";
    public static final String AUTH_GET_ANNOUNCEMENTS      = "engine/announcements";
    public static final String AUTH_GET_ANNOUNCEMENT       = "engine/announcement";

    /**
     * ----- NEW endpoints for 1.0 API. Still unused.
     */

    /**
     * ----- END NEW endpoints 
     */

    /*
     * Headers for requests
     */
    public static final String SESSION_AUTHORIZATION_HEADER = "SessionAuthorization";
    public static final String REQUESTER_GROUPS_HEADER      = "RequesterGroups";
    public static final String REQUESTER_ROLES_HEADER       = "RequesterRoles";

    /*
     * headers for request, also found in authInfo at engine side after successful authorization, with a value
     * of "Ok".
     */
    public static final String CALLING_ENGINE_KEY_HEADER   = "CallingEngineAuthorization";
    public static final String RECEIVING_ENGINE_KEY_HEADER = "ReceivingEngineAuthorization";
    public static final String ADMIN_AUTHORIZATION_HEADER  = "AdminAuthorization";
    public static final String USERNAME_INFO_HEADER        = "UserName";
}
