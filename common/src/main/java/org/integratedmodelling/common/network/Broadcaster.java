package org.integratedmodelling.common.network;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;

import org.integratedmodelling.common.configuration.KLAB;
import org.jgroups.Address;
import org.jgroups.JChannel;
import org.jgroups.Message;
import org.jgroups.ReceiverAdapter;

/**
 * Create a Broadcaster using a unique identifier and port, either in 
 * ADVERTISE or in DISCOVER mode. If advertising, it will broadcast a
 * signal for the identifier every 10 seconds. If discovering, it will react 
 * to any signals broadcast for the identifier by calling onSignal().
 * 
 * After construction, check isRunning() - no exceptions will be thrown but
 * initialization may have failed.
 * 
 * @author ferdinando.villa
 *
 */
public class Broadcaster {

    private int SECONDS_INTERVAL = 10;

    JChannel         channel;
    boolean          running;
    volatile Boolean isBroadcasting;
    Mode             mode;
    private Timer    timer;
    Object           identity;
    int              port;
    String           address;

    enum Mode {
        ADVERTISE,
        DISCOVER
    }

    class AdvertiseTask extends TimerTask {

        @Override
        public void run() {
            try {
                Message message = new Message(null, identity + "#" + new Date().getTime() + "#" + address + "#"
                        + port);
                channel.send(message);
            } catch (Exception e) {
                // TODO count errors? Do these happen? Have a threshold of errorcount to
                // declare failure?
                KLAB.error(e.getMessage());
            }
        }

    }

    /**
     * Start in discovery mode. No advertising is done, onSignal() is called whenever
     * an advertiser on the same identity notifies itself.
     * 
     * @param identity
     */
    public Broadcaster(String identity) {
        this(identity, 0);
    }

    /**
     * Start in advertising mode. Local address and passed port are broadcast for this
     * identity every SECONDS_INTERVAL seconds.
     * 
     * @param identity
     * @param port
     */
    public Broadcaster(String identity, int port) {

        this.mode = port == 0 ? Mode.DISCOVER : Mode.ADVERTISE;
        this.identity = identity;
        this.port = port;

        /*
         * hack for, you guessed it, Windows JVM having a botched DatagramSocketImpl to 
         * make sure that Vista bugs are not exposed.
         * 
         * See https://github.com/belaban/JGroups/wiki/FAQ if this becomes a problem.
         */
        System.setProperty("java.net.preferIPv4Stack", "true");
        
        try {
            channel = new JChannel(/* TODO see if protocol defaults are OK */);
            channel.setReceiver(new ReceiverAdapter() {
                public void receive(Message msg) {
                    signal(msg.getSrc(), msg.getObject());
                }
            });
            channel.connect(identity);
            if (mode == Mode.ADVERTISE) {
                startAdvertising();
            }
        } catch (Exception e) {
            // just don't set running to true
            return;
        }

        running = true;
    }

    private void startAdvertising() throws UnknownHostException {

        this.address = InetAddress.getLocalHost().getHostAddress();
        this.timer = new Timer();
        timer.schedule(new AdvertiseTask(), 0, SECONDS_INTERVAL * 1000);
    }

    private void signal(Address src, Object message) {
        if (mode == Mode.DISCOVER) {
            String[] md = message.toString().split("#");
            if (md[0].equals(this.identity)) {
                onSignal(md[2], Integer.parseInt(md[3]), Long.parseLong(md[1]));
            }
        }
    }

    public void close() {
        if (timer != null) {
            timer.cancel();
        }
        if (channel != null) {
            channel.close();
        }
    }
    
    /**
     * Redefine in discovery mode to react to a ping.
     *  
     * @param ipAddress IP address of advertising server
     * @param port port where service is located
     * @param time time of message reception
     */
    protected void onSignal(String ipAddress, int port, long time) {
    }

    public boolean isRunning() {
        return running;
    }

    public void pause() {
        synchronized (isBroadcasting) {
            isBroadcasting = false;
        }
    }

    public void resume() {
        synchronized (isBroadcasting) {
            isBroadcasting = true;
        }
    }
}
