/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.common.network;

import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.integratedmodelling.api.auth.IResourceCatalog;
import org.integratedmodelling.api.configuration.IConfiguration;
import org.integratedmodelling.api.network.IComponent;
import org.integratedmodelling.api.project.IProject;
import org.integratedmodelling.common.configuration.KLAB;
import org.integratedmodelling.common.utils.MiscUtilities;

/**
 * A simple catalog listing privileges for assets and services built from 
 * configuration. There is one in every engine (empty in personal engines). 
 * Handles the content of properties only.
 * 
 * @author Ferd
 *
 */
public class ResourceCatalog implements IResourceCatalog {

    public static enum ResourceType {
        PROJECT,
        NODE,
        SERVICE,
        COMPONENT
    }

    /*
     * these are relevant at the engine side.
     */
    Map<String, Set<String>> componentRestrictions      = new HashMap<>();
    Map<String, Set<String>> contentProjectRestrictions = new HashMap<>();
    Map<String, Set<String>> assetProjectRestrictions   = new HashMap<>();
    Set<String>              nodeRestrictions           = new HashSet<>();

    /*
     * these are relevant at both the client and engine side.
     */
    List<String> assetProjectIds   = new ArrayList<>();
    List<String> contentProjectIds = new ArrayList<>();
    List<String> componentIds      = new ArrayList<>();

    // IDs of projects where components have been registered.
    Map<String, String> componentProjectIds = new HashMap<>();

    public void clear() {
        assetProjectIds.clear();
        componentIds.clear();
    }

    public String getDescription() {
        return "resource catalog configured to serve " + assetProjectIds.size()
                + " projects, content from " + contentProjectIds.size() + " projects, and "
                + componentIds.size() + " components";
    }

    /*
     * called after get-services: no need to worry about permissions because get-services is 
     * user-filtered.
     */
    public void addSynchronizedProject(String projectId) {
        assetProjectIds.add(projectId);
    }

    /*
     * called after get-services: no need to worry about permissions because get-services is 
     * user-filtered.
     */
    public void addComponent(String componentId) {
        componentIds.add(componentId);
    }

    public void build() {

        for (Object s : KLAB.CONFIG.getProperties().keySet()) {

            String prop = s.toString();

            if (prop.startsWith(IConfiguration.THINKLAB_ASSETS_PROJECTS_PROPERTY)) {
                String[] ids = KLAB.CONFIG.getProperties().getProperty(prop).split(",");
                Set<String> groups = extractGroups(prop, IConfiguration.THINKLAB_ASSETS_PROJECTS_PROPERTY);
                if (groups != null) {
                    for (String id : ids) {
                        assetProjectRestrictions.put(id, groups);
                    }
                }
                for (String id : ids) {
                    assetProjectIds.add(id);
                }
            } else if (prop.startsWith(IConfiguration.THINKLAB_CONTENT_PROJECTS_PROPERTY)) {
                String[] ids = KLAB.CONFIG.getProperties().getProperty(prop).split(",");
                Set<String> groups = extractGroups(prop, IConfiguration.THINKLAB_CONTENT_PROJECTS_PROPERTY);
                if (groups != null) {
                    for (String id : ids) {
                        contentProjectRestrictions.put(id, groups);
                    }
                }
                for (String id : ids) {
                    contentProjectIds.add(id);
                }
            } else if (prop.startsWith(IConfiguration.THINKLAB_PUBLISHED_COMPONENTS_PROPERTY)) {
                String[] ids = KLAB.CONFIG.getProperties().getProperty(prop).split(",");
                Set<String> groups = extractGroups(prop, IConfiguration.THINKLAB_PUBLISHED_COMPONENTS_PROPERTY);
                if (groups != null) {
                    for (String id : ids) {
                        componentRestrictions.put(id, groups);
                    }
                }
                for (String id : ids) {
                    componentIds.add(id);
                }
            } else if (prop.equals(IConfiguration.THINKLAB_ALLOWED_GROUPS_PROPERTY)) {
                for (String id : KLAB.CONFIG.getProperties().getProperty(prop).split(",")) {
                    nodeRestrictions.add(id);
                }
            }
        }

        if (!KLAB.NETWORK.isPersonal()) {
            KLAB.info(getDescription());
        }
    }

    private Set<String> extractGroups(String prop, String propertyRoot) {

        if (prop.equals(propertyRoot)) {
            return null;
        }

        Set<String> ret = new HashSet<>();
        String rem = prop.substring(propertyRoot.length() + 1);
        for (String ss : rem.split("\\.")) {
            ret.add(ss);
        }
        return ret;
    }

    /**
     * Return all shared projects irrespective of authorization
     * 
     * @return
     */
    @Override
    public Collection<String> getSynchronizedProjectIds() {
        return assetProjectIds;
    }

    /**
     * Return all content projects irrespective of authorization
     * 
     * @return
     */
    @Override
    public Collection<String> getContentProjectIds() {
        return contentProjectIds;
    }

    /**
     * Return all components irrespective of authorization
     * 
     * @return
     */
    @Override
    public Collection<String> getComponentIds() {
        return componentIds;
    }

    /**
     * Get the groups that can access every resource and service in this
     * node.
     * 
     * @return
     */
    @Override
    public Set<String> getAuthorizedGroups() {
        return nodeRestrictions;
    }

    @Override
    public boolean isAuthorized(String projectId, Set<String> groups) {

        if (assetProjectIds.contains(projectId)) {
            return isAssetProjectAuthorized(projectId, groups);
        }
        if (contentProjectIds.contains(projectId)) {
            return isContentProjectAuthorized(projectId, groups);
        }
        if (componentProjectIds.containsKey(projectId)) {
            return isComponentAuthorized(componentProjectIds.get(projectId), groups);
        }
        return false;
    }

    private boolean isAssetProjectAuthorized(String id, Set<String> groups) {

        /*
         * we need no groups for our own internal server.
         */
        if (groups.isEmpty()) {
            return KLAB.NETWORK.isPersonal();
        }

        if (assetProjectRestrictions.containsKey(id)) {
            HashSet<String> set = new HashSet<>(assetProjectRestrictions.get(id));
            set.retainAll(groups);
            return set.size() > 0;
        }
        return true;
    }

    private boolean isContentProjectAuthorized(String id, Set<String> groups) {

        /*
         * we need no groups for our own internal server.
         */
        if (groups.isEmpty()) {
            return KLAB.NETWORK.isPersonal();
        }

        if (contentProjectRestrictions.containsKey(id)) {
            HashSet<String> set = new HashSet<>(contentProjectRestrictions.get(id));
            set.retainAll(groups);
            return set.size() > 0;
        }
        return true;
    }

    private boolean isComponentAuthorized(String id, Set<String> groups) {

        /*
         * we need no groups for our own internal server.
         */
        if (groups.isEmpty()) {
            return KLAB.NETWORK.isPersonal();
        }

        if (!componentIds.contains(id)) {
            return false;
        }
        if (componentRestrictions.containsKey(id)) {
            HashSet<String> set = new HashSet<>(componentRestrictions.get(id));
            set.retainAll(groups);
            return set.size() > 0;
        }
        return true;
    }

    @Override
    public void registerComponentPath(IComponent c, File path) {
        String id = c.getId();
        componentProjectIds.put(MiscUtilities.getFileBaseName(path.toString()), id);
    }

    @Override
    public boolean hasAuthorizedContent(Set<String> groups) {
        /*
         * TODO/FIXME - use groups
         */
        return componentIds.size() > 0 || contentProjectIds.size() > 0;
    }

    @Override
    public Collection<IProject> getAuthorizedProjects(Set<String> requesterGroups) {
        ArrayList<IProject> ret = new ArrayList<>();
        for (String p : assetProjectIds) {

            if (requesterGroups == null || isAuthorized(p, requesterGroups)) {
                IProject proj = KLAB.PMANAGER.getProject(p);
                if (proj != null) {
                    ret.add(proj);
                }
            }
        }
        return ret;
    }

    @Override
    public Collection<IComponent> getAuthorizedComponents(Set<String> requesterGroups) {
        ArrayList<IComponent> ret = new ArrayList<>();
        for (String p : componentIds) {

            // Env.logger.info("have registration for component " + p);

            if (requesterGroups == null || isComponentAuthorized(p, requesterGroups)) {
                IComponent c = KLAB.CMANAGER.getComponent(p);
                if (c != null && c.isActive()) {
                    // Env.logger.info("authorizing component " + c);
                    ret.add(c);
                } /* else {
                    Env.logger.info("component " + p + " not authorized because "
                            + (c == null ? "it's null" : "is inactive"));
                  } */
            }
        }
        return ret;
    }

}
