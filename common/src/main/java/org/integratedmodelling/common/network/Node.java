/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.common.network;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.integratedmodelling.api.auth.IResourceCatalog;
import org.integratedmodelling.api.network.IComponent;
import org.integratedmodelling.api.network.INode;
import org.integratedmodelling.api.project.IProject;
import org.integratedmodelling.api.rest.IRESTController;
import org.integratedmodelling.api.services.IPrototype;
import org.integratedmodelling.api.services.IServiceCall;
import org.integratedmodelling.collections.Pair;
import org.integratedmodelling.common.command.ServiceCall;
import org.integratedmodelling.common.configuration.KLAB;
import org.integratedmodelling.common.engine.NetworkedEngine;
import org.integratedmodelling.exceptions.ThinklabException;

/**
 * Descriptor for a network node. Its update() method should be called at regular
 * intervals. Offline nodes will only be re-checked every OFFLINE_UPDATE_INTERVAL
 * minutes; online nodes will be polled every ONLINE_UPDATE_INTERVAL for their
 * situation. Each node can report the list of the nodes it's connected to based on
 * the connecting user: full information is available to engine nodes, filtered 
 * information gets to personal engines.
 *
 * @author Ferd
 *
 */
public class Node extends NetworkedEngine implements INode {

    /**
     * These are in milliseconds. The first number is minutes.
     */
    static public long ONLINE_UPDATE_INTERVAL  = 10 * 60 * 1000;
    static public long OFFLINE_UPDATE_INTERVAL = 30 * 60 * 1000;

    boolean         active;
    long            lastUpdate;
    String          key;
    ResourceCatalog catalog     = new ResourceCatalog();
    String          description = "";

    /**
     * contains the mapping name -> [url, key] for all the nodes that
     * are linked to this one and are visible in the security context
     * this is called from. Will be empty for unprivileged callers.
     */
    Map<String, Pair<String, String>> knownPeers = new HashMap<>();

    Node(String id, String url, String key) {
        super(new RESTController(url, KLAB.NETWORK.getKey(), key), id);
        this.key = key;
        update();
    }

    Node(IRESTController controller, String id, String url, String key) {
        super(controller.forNode(url, key), id);
        this.key = key;
        update();
    }

    public Object call(IServiceCall command) throws ThinklabException {

        if (!isActive()) {
            return null;
        }

        ArrayList<Object> args = new ArrayList<>();
        for (String a : ((ServiceCall) command).getAllArgumentNames()) {
            args.add(a);
            args.add(command.get(a));
        }

        return ((ServiceCall) command).isPost() ? post(command.getPrototype().getId(), args.toArray())
                : get(command.getPrototype().getId(), args.toArray());
    }

    @Override
    public String getId() {
        return getName();
    }

    @Override
    public boolean isPrimaryNode() {
        return KLAB.NETWORK.getUser() != null && getUrl().equals(KLAB.NETWORK.getUser().getServerURL());
    }

    @Override
    public IResourceCatalog getResourceCatalog() {
        return catalog;
    }

    @Override
    public boolean isActive() {
        return active;
    }

    @Override
    public String getKey() {
        return key;
    }

    @Override
    public String getDescription() {
        return description;
    }

    @Override
    public boolean providesComponent(String id) {
        return catalog.getComponentIds().contains(id);
    }

    @Override
    public IProject importProject(String projectId) throws ThinklabException {
        KLAB.info("attempting to import project " + projectId + " from node " + getId());
        IProject ret = KLAB.PMANAGER.importProject(this, projectId);
        KLAB.info("import resulted in " + ret);
        return ret;
    }

    /**
     * Call at creation and from an automated daemon to keep the node's situation updated. If we have a 
     * user in the security context, this will see services etc. filtered according to privileges.
     */
    public void update() {

        long curtime = new Date().getTime();
        boolean doUpdate = (active && (curtime - lastUpdate) > ONLINE_UPDATE_INTERVAL) ||
                (!active && (curtime - lastUpdate) > OFFLINE_UPDATE_INTERVAL);

        if (doUpdate) {

            lastUpdate = curtime;
            boolean wasActive = active;

            try {
                Object r = get(Endpoints.GET_NODE_CAPABILITIES);
                if (r instanceof Map) {

                    catalog.clear();
                    submitters.clear();
                    services.clear();

                    Map<?, ?> map = (Map<?, ?>) r;

                    for (Object o : (List<?>) map.get("components")) {
                        catalog.addComponent(((IComponent) o).getId());
                    }

                    for (Object o : (List<?>) map.get("services")) {
                        services.put(((IPrototype) o).getId(), (IPrototype) o);
                    }

                    for (Object o : (List<?>) map.get("projects")) {
                        catalog.addSynchronizedProject(o.toString());
                    }

                    if (map.containsKey("submitters")) {
                        for (Object o : (List<?>) map.get("submitters")) {
                            submitters.add(o.toString());
                        }
                    }

                    active = true;
                }

            } catch (ThinklabException e) {
                active = false;
            }

            if (active != wasActive) {
                KLAB.info("node " + getId() + (wasActive ? " went offline" : " is online"));
            }
        }
    }

    @Override
    public String toString() {
        return "<NODE " + getId() + " " + getUrl() + ">";
    }
}
