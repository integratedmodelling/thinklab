/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.common.network;

import java.io.File;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

import org.apache.commons.lang.exception.ExceptionUtils;
import org.integratedmodelling.api.auth.IResourceCatalog;
import org.integratedmodelling.api.auth.IUser;
import org.integratedmodelling.api.configuration.IConfiguration;
import org.integratedmodelling.api.engine.IEngine;
import org.integratedmodelling.api.monitoring.IMonitor;
import org.integratedmodelling.api.network.INetwork;
import org.integratedmodelling.api.network.INode;
import org.integratedmodelling.api.rest.IRESTController;
import org.integratedmodelling.api.services.IPrototype;
import org.integratedmodelling.api.services.IServiceCall;
import org.integratedmodelling.common.auth.LicenseManager;
import org.integratedmodelling.common.auth.RESTUser;
import org.integratedmodelling.common.command.ServiceCall;
import org.integratedmodelling.common.configuration.KLAB;
import org.integratedmodelling.common.data.Edge;
import org.integratedmodelling.common.project.ProjectManager;
import org.integratedmodelling.common.utils.MiscUtilities;
import org.integratedmodelling.exceptions.ThinklabException;
import org.integratedmodelling.exceptions.ThinklabIOException;
import org.integratedmodelling.exceptions.ThinklabValidationException;
import org.jgrapht.graph.DefaultDirectedGraph;

/**
 * This one is responsible for building up the list of nodes based on calling context. A list must be read from the
 * suitable source:
 * 
 *      the personal engine for clients, 
 *      the primary server for personal engines, 
 *      the public server configuration and the nodes themselves for public servers.
 *      
 *  After the nodes are in, the resource catalog should be rebuilt. The node list should be refreshed
 *  periodically, and any differences due to nodes coming online or going offline should be reflected
 *  in the resource catalog.     
 */
public class Network implements INetwork {

    private static long MAX_TIMEOUT_REMOTE_SERVICE_SECONDS = 2;

    Map<String, INode>      nodes                = new HashMap<>();
    ResourceCatalog         catalog              = new ResourceCatalog();
    IRESTController         context;
    boolean                 isPublic;
    boolean                 isPersonal;
    String                  authEndpoint;
    Set<String>             authorizedSubmitters = new HashSet<>();
    File                    assetsDirectory;
    String                  url;
    String                  key;
    Set<String>             projects             = new HashSet<>();
    Set<String>             pcontent             = new HashSet<>();
    IUser                   user;
    boolean                 isOnline;
    boolean                 hasChanged;
    List<NetworkConnection> connections          = new ArrayList<>();

    /**
     * This is empty unless we're a server; if so, it contains only the names of the
     * nodes we have certificates for.
     */
    List<String> connectedNodeNames = new ArrayList<>();

    public Network() {
    }

    public boolean initialize() {

        File serverCertFile = new File(KLAB.CONFIG.getDataPath()
                + File.separator + "server.cert");
        File clientCertFile = new File(KLAB.CONFIG.getDataPath()
                + File.separator + "im.cert");
        File publicKey = new File(KLAB.CONFIG.getDataPath()
                + File.separator + "ssh" +
                File.separator + "pubring.gpg");

        catalog.build();

        if (KLAB.CLIENT != null) {
            return isOnline = initializeClient(KLAB.CLIENT.getEngine());
        } else if (serverCertFile.exists() && !clientCertFile.exists() && publicKey.exists()) {
            return isOnline = initializePublicNode(serverCertFile, publicKey);
        } else if (clientCertFile.exists() && !serverCertFile.exists() && publicKey.exists()) {
            return isOnline = initializeFromPrimaryServer(clientCertFile, publicKey);
        }

        KLAB.NAME = "anonymous";
        return isOnline = false;
    }

    public boolean initializeClient(IEngine engine) {

        this.context = engine;
        this.user = this.context.getUser();
        this.authEndpoint = ((RESTUser) this.user).getAuthClient().getAuthenticationEndpoint();
        this.url = engine.getUrl();

        /*
         * retrieve nodes through call to personal engine
         */
        startPolling();

        return true;

    }

    public void readStructureFromList(IRESTController controller, List<?> network) {

        nodes.clear();
        connections.clear();

        for (int i = 0; i < network.size(); i++) {

            String ndef = network.get(i).toString();
            if (i == network.size() - 1) {
                String ss[] = ndef.split("\\|");
                for (String sss : ss) {
                    String[] nn = sss.split(",");
                    connections.add(new NetworkConnection(nodes.get(nn[0]), nodes.get(nn[1])));
                }
            } else {
                String ss[] = ndef.split("\\|");
                nodes.put(ss[0], new Node(controller, ss[0], ss[1], ss[2]));
            }
        }
    }

    /**
     * Get the network's structure as a list of node descriptor in the form
     * "id|url|key". The last element of the list, if not empty, describes
     * the relationships in the form "n1,n1|n3,n2|..." using the node IDs as 
     * keys.
     * 
     * @param groups
     * @return
     */
    public List<String> getStructureAsList() {

        List<String> network = new ArrayList<>();
        Set<String> nids = new HashSet<>();

        if (!isPersonal) {
            nids.add(KLAB.NAME);
            network.add(KLAB.NAME + "|" + KLAB.NETWORK.getUrl() + "|" + KLAB.NETWORK.getKey());
        }
        String relationships = "";

        for (INode n : getNodes()) {

            if (!nids.contains(n.getId())) {
                nids.add(n.getId());
                network.add(n.getId() + "|" + n.getUrl() + "|" + n.getKey());
            }
            try {
                Object o = n.get(Endpoints.IDENTIFY);
                if (o instanceof Map && ((Map<?, ?>) o).containsKey("network")) {
                    List<?> nodes = (List<?>) ((Map<?, ?>) o).get("network");
                    for (int i = 0; i < nodes.size(); i++) {
                        if (i == nodes.size() - 1) {
                            relationships += ((relationships.isEmpty()
                                    || nodes.get(i).toString().isEmpty()) ? "" : "|")
                                    + nodes.get(i).toString();
                        } else {
                            String rnl = nodes.get(i).toString();
                            String[] ss = rnl.split("\\|");
                            if (!nids.contains(ss[0])) {
                                nids.add(ss[0]);
                                network.add(rnl);
                            }
                            if (!isPersonal) {
                                relationships += (relationships.isEmpty() ? "" : "|") + KLAB.NAME + ","
                                        + ss[0];
                            }
                        }
                    }
                }
            } catch (ThinklabException e) {
                KLAB.warn("error calling identify on node " + n.getId(), e);
                // just don't.
            }
        }

        if (network.size() > 0) {
            network.add(relationships);
        }

        return network;
    }

    private boolean initializeFromPrimaryServer(File clientCertFile, File publicKey) {

        isPersonal = true;

        /*
         * personal server starts already authenticated. The auth token will be different from the client's; we
         * will compare the usernames at login for extra safety, which is ok as they both have authenticated
         * from the certificate.
         */
        try {
            this.user = new RESTUser(clientCertFile);
            this.url = this.user.getServerURL();
            KLAB.NAME = this.user.getUsername();
            this.authEndpoint = ((RESTUser) user).getAuthClient().getAuthenticationEndpoint();
        } catch (Throwable e) {
            this.user = new RESTUser();
            KLAB.NAME = "anonymous";
        }

        /*
         * security contect now has the key for the local user.
         */
        this.context = new RESTController(url, user);

        /*
         * call connect on primary server to log us on the Thinklab network. Server will
         * check credentials again (double-check to prevent intrusion) and if OK will 
         * respond with an engine key which is also stored in the context to access
         * future network operations.
         */
        if (!this.user.isAnonymous()) {

            if ((this.key = context.connect()) == null) {

                // network timeout most likely. Log error and proceed anonymously.
                KLAB.error("cannot communicate with primary server");
                return false;
            }

            try {
                /*
                 * having a user and a remote key, this will return the view of the network corresponding
                 * to the user.
                 */
                Map<?, ?> map = (Map<?, ?>) context.get(Endpoints.IDENTIFY);
                if (map.containsKey("network")) {
                    readStructureFromList(this.context, (List<?>) map.get("network"));
                }

            } catch (Throwable e) {
                KLAB.error("network identification with primary server failed", e);
                return false;

            }
        }

        startPolling();

        return true;
    }

    public boolean initializePublicNode(File serverCertFile, File publicKey) {

        isPublic = true;
        Properties identity = null;

        if (KLAB.CONFIG.getProperties().containsKey(IConfiguration.THINKLAB_AUTHENTICATION_ENDPOINT)) {
            this.authEndpoint = KLAB.CONFIG.getProperties()
                    .getProperty(IConfiguration.THINKLAB_AUTHENTICATION_ENDPOINT);
        }

        if (KLAB.CONFIG.getProperties().containsKey(IConfiguration.SUBMITTING_GROUPS_PROPERTY)) {
            for (String s : KLAB.CONFIG.getProperties()
                    .getProperty(IConfiguration.SUBMITTING_GROUPS_PROPERTY).split(",")) {
                authorizedSubmitters.add(s);
            }
        }
        /*
         * the resource catalog contains names of projects and components we authorize but does not
         * check for their existence.
         */
        if (KLAB.CONFIG.getProperties().containsKey(IConfiguration.THINKLAB_ASSETS_DIR_PROPERTY)) {

            this.assetsDirectory = new File(KLAB.CONFIG.getProperties()
                    .getProperty(IConfiguration.THINKLAB_ASSETS_DIR_PROPERTY));

            if (assetsDirectory.exists() && assetsDirectory.isDirectory() && assetsDirectory.canRead()) {
                for (File f : assetsDirectory.listFiles()) {
                    if (ProjectManager.isThinklabProject(f)) {

                        String pname = MiscUtilities.getFileName(f.toString());

                        if (catalog.getSynchronizedProjectIds().contains(pname)) {
                            KLAB.info("serving project " + pname);
                            projects.add(pname);
                        }

                        if (catalog.getContentProjectIds().contains(pname)) {
                            KLAB.info("serving knowledge from project " + pname);
                            pcontent.add(pname);
                        }
                    }
                }
            }
        }

        File serverDir = new File(KLAB.CONFIG.getDataPath()
                + File.separator + "network");
        try {

            identity = LicenseManager.get()
                    .readCertificate(serverCertFile, publicKey, new String[] {
                            "url",
                            "name",
                            "key" });

            serverDir.mkdirs();

            KLAB.NAME = identity.getProperty("name");
            this.url = identity.getProperty("url");
            this.key = identity.getProperty("key");
            this.context = new RESTController(url, key);

            /*
             * TODO check expiration
             */

        } catch (Exception e) {
            KLAB.error("Certificate is invalid: public network initialization failed: ", e);
            return false;
        }

        KLAB
                .info("initializing node " + KLAB.NAME + " for public use: scanning certified network peers");

        /*
         * scan all remaining nodes
         */
        for (File sc : serverDir.listFiles()) {
            if (sc.canRead() && sc.isFile() && sc.toString().endsWith(".cert")) {
                try {
                    Properties pp = LicenseManager.get().readCertificate(sc, publicKey, new String[] {
                            "url",
                            "name",
                            "key" });

                    connectedNodeNames.add(pp.getProperty("name"));

                    nodes.put(pp
                            .getProperty("name"), new Node(pp.getProperty("name"), pp.getProperty("url"), pp
                                    .getProperty("key")));

                    KLAB.info("read certificate for node " + pp.getProperty("name"));

                } catch (Exception e) {
                    // bad certificate or connection error; warn and move on
                    KLAB.warn("error reading certificate for node "
                            + MiscUtilities.getFileBaseName(sc.toString()) + ": ignored: "
                            + ExceptionUtils.getFullStackTrace(e));
                    continue;
                }
            }
        }

        startPolling();

        return true;
    }

    @Override
    public Collection<INode> getNodes() {
        return nodes.values();
    }

    @Override
    public INode getNode(String s) {
        return nodes.get(s);
    }

    /**
     * Return the names of all nodes we know by certificate. Can only return
     * something in public servers.
     * 
     * @return
     */
    public List<String> getConnectedNodeNames() {
        return connectedNodeNames;
    }

    @Override
    public String getUrlForProject(String projectId) {

        /*
         * if I'm serving this project, return a file URL
         */
        if (catalog.getSynchronizedProjectIds().contains(projectId)) {
            try {
                return new File(assetsDirectory + File.separator + projectId).toURI().toURL().toString();
            } catch (MalformedURLException e) {
                // this WILL work.
            }
        }

        synchronized (nodes) {
            for (INode node : getNodes()) {
                if (node.getResourceCatalog().getSynchronizedProjectIds().contains(projectId)) {
                    try {
                        Object pd = node.get(Endpoints.PROJECT, "cmd", "export", "plugin", projectId);
                        if (pd instanceof Map && ((Map<?, ?>) pd).containsKey("access-key")) {
                            return node.getUrl() + "/" + ((Map<?, ?>) pd).get("access-key");
                        }
                    } catch (ThinklabException e) {
                        KLAB.error(e);
                    }
                }
            }
        }
        return null;
    }

    @Override
    public Object broadcast(final IServiceCall command, IMonitor monitor) throws ThinklabException {

        class Service implements Callable<Object> {

            Node         node;
            IServiceCall call;

            public Service(INode node, IServiceCall command) {
                this.node = (Node) node;
                this.call = command;
            }

            @Override
            public Object call() throws Exception {
                return node.call(((ServiceCall) call).forNode(node));
            }

        }

        ArrayList<Node> nn = new ArrayList<>();
        ArrayList<Callable<Object>> rr = new ArrayList<>();
        for (INode n : nodes.values()) {
            if (n.isActive() && ((Node) n).providesService(command.getPrototype().getId())) {
                nn.add((Node) n);
                rr.add(new Service(n, command));
            }
        }

        /*
         * create threads, call them all and wait for completion; then
         * merge results, which should be collections and whose members
         * should provide proper equal() and hashCode() implementations.
         */
        ExecutorService executor = Executors.newScheduledThreadPool(nn.size());
        List<Collection<?>> results = new ArrayList<>();
        try {
            List<Future<Object>> res = executor.invokeAll(rr);
            executor.awaitTermination(MAX_TIMEOUT_REMOTE_SERVICE_SECONDS, TimeUnit.SECONDS);

            for (Future<Object> r : res) {
                Object o = null;
                try {
                    o = r.get();
                } catch (ExecutionException e) {
                }

                if (o instanceof Collection<?>) {
                    results.add((Collection<?>) o);
                }
            }

        } catch (InterruptedException e) {
            throw new ThinklabIOException(e);
        }

        return results.size() > 0 ? mergeResults(results) : null;
    }

    private Collection<Object> mergeResults(List<Collection<?>> results) throws ThinklabValidationException {
        ArrayList<Object> ret = new ArrayList<>();
        Class<?> cl = null;
        for (Collection<?> c : results) {
            for (Object o : c) {
                if (o == null) {
                    continue;
                }
                if (cl == null) {
                    cl = o.getClass();
                } else if (!(o.getClass().isAssignableFrom(cl))) {
                    throw new ThinklabValidationException("results of call were of disparate types");
                }
                ret.add(o);
            }
        }
        return ret;
    }

    @Override
    public String getKey() {
        return key;
    }

    @Override
    public boolean hasChanged() {
        boolean ret = hasChanged;
        hasChanged = false;
        return ret;
    }

    @Override
    public String getUrl() {
        return url;
    }

    @Override
    public boolean isPersonal() {
        return isPersonal;
    }

    @Override
    public boolean providesComponent(String string) {
        for (INode n : getNodes()) {
            if (n.providesComponent(string)) {
                return true;
            }
        }
        return false;
    }

    @Override
    public IPrototype findPrototype(String id) {
        for (INode node : getNodes()) {
            for (IPrototype p : node.getServices()) {
                if (p.getId().equals(id)) {
                    return p;
                }
            }
        }
        return null;
    }

    @Override
    public String getAuthenticationEndpoint() {
        return authEndpoint;
    }

    @Override
    public IResourceCatalog getResourceCatalog() {
        return catalog;
    }

    @Override
    public Collection<String> getAuthorizedSubmitters() {
        return authorizedSubmitters;
    }

    @Override
    public IUser getUser() {
        return user;
    }

    @Override
    public INode getNodeProviding(String id) {
        for (INode n : getNodes()) {
            if (n.providesService(id)) {
                return n;
            }
        }
        return null;
    }

    @Override
    public boolean isOnline() {
        return isOnline;
    }

    /**
     * Get the graph representation of the known network. If a personal engine, add the PE
     * connection to its primary node. The vertex type will be either a IEngine or a IUser. 
     * Our own user is represented by a IEngine; if we're admins we'll have IUsers for all
     * active users connected to the primary server.
     * 
     * @return
     */
    public DefaultDirectedGraph<Object, NetworkConnection> getGraph() {

        DefaultDirectedGraph<Object, NetworkConnection> ret = new DefaultDirectedGraph<>(NetworkConnection.class);
        long now = new Date().getTime();

        IEngine primary = null;
        for (INode n : getNodes()) {
            if (n.isPrimaryNode()
                    /*FIXME remove this hack when all certificates are hostname-based */ || n
                            .getUrl().equals("http://150.241.131.4/tl0")) {
                primary = n;
            }
            ret.addVertex(n);
        }
        for (NetworkConnection c : connections) {
            ret.addEdge(c.getSourceObject(), c.getDestinationObject(), c);
        }

        if (primary != null && this.context != null) {

            ret.addVertex(this.context);
            ret.addEdge(this.context, primary, new NetworkConnection(this.context, primary));

            if (user != null && user.getGroups().contains("ADMIN")) {
                /*
                 * we can also see who else is connected to our primary server
                 */
                try {
                    Object o = primary.get(Endpoints.GET_ACTIVE_USERS);
                    if (o instanceof List<?>) {
                        /*
                         * add for the active users except self, connect them to
                         * primary.
                         */
                        for (Object m : (List<?>) o) {
                            String username = ((Map<?, ?>) m).get("username").toString();
                            long lastactive = ((Map<?, ?>) m).containsKey("last-activity") ? Long
                                    .parseLong(((Map<?, ?>) m).get("last-activity").toString()) : now;

                            /*
                             * just don't add self.
                             */
                            if (username.equals(KLAB.NAME)) {
                                continue;
                            }

                            RESTUser user = new RESTUser(username);
                            user.setLastLogin(lastactive);

                            ret.addVertex(user);
                            ret.addEdge(user, primary, new NetworkConnection(user, primary));

                        }
                    }
                } catch (ThinklabException e) {
                    // ok, forget it.
                }
            }
        }

        return ret;
    }

    /*
     * --------------------------------------------------------------------
     * monitoring
     * --------------------------------------------------------------------
     */

    ScheduledExecutorService executor                  = null;
    ScheduledFuture<?>       future                    = null;
    volatile Boolean         _monitoring               = false;
    int                      _errorcount               = 0;
    static int               MAX_ERRORS_BEFORE_WARNING = 3;

    /*
     * default: poll network every minute; nodes will decide whether to actually update 
     * their status according to the state they're in.
     */
    static long POLLING_PERIOD_MILLISECONDS = 1 * 60 * 1000;

    class PollingService implements Runnable {

        @Override
        public void run() {
            synchronized (_monitoring) {
                monitor();
            }
        }
    }

    public void monitor() {

        _monitoring = true;
        for (INode n : getNodes()) {
            ((Node) n).update();
        }
        _monitoring = false;
    }

    /**
     * Add a monitor for notification of server status and spawn an appropriate
     * monitoring cycle. Monitor will get all notifications sent (as instances
     * of EngineNotification).
     * 
     * @param serverMonitor
     */
    public void startPolling() {

        if (future != null) {
            future.cancel(false);
            executor.shutdown();
        }

        executor = Executors.newScheduledThreadPool(24);
        future = executor
                .scheduleWithFixedDelay(new PollingService(), 0, POLLING_PERIOD_MILLISECONDS, TimeUnit.MILLISECONDS);
    }

    /**
     * Stop the polling.
     */
    public void pausePolling() {

        synchronized (_monitoring) {
            if (future != null) {
                future.cancel(false);
                executor.shutdown();
                future = null;
            }
        }
    }

    /**
     * Network graph
     */

    public class NetworkConnection extends Edge {

        private static final long serialVersionUID = -3258474726633419385L;

        Object from;
        Object to;

        public NetworkConnection(Object from, Object to) {
            this.from = from;
            this.to = to;
        }

        public Object getSourceObject() {
            return from;
        }

        public Object getDestinationObject() {
            return to;
        }

        public String getName(Object o) {
            return o instanceof IEngine ? ((IEngine) o).getName() : ((IUser) o).getUsername();
        }

        public String getId() {
            return getName(from) + "|" + getName(to);
        }

        @Override
        public boolean equals(Object o) {
            return o instanceof NetworkConnection && ((NetworkConnection) o).getId().equals(getId());
        }

        @Override
        public int hashCode() {
            return getId().hashCode();
        }
    }

}
