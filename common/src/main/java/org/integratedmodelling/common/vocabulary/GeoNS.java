/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/

package org.integratedmodelling.common.vocabulary;

import org.integratedmodelling.api.knowledge.IConcept;
import org.integratedmodelling.common.configuration.KLAB;
import org.integratedmodelling.common.space.IGeometricShape;
import org.integratedmodelling.exceptions.ThinklabException;

import com.vividsolutions.jts.geom.Coordinate;

/**
 * Namespace constants for concepts essential in GIS modeling. Also provides general functions to
 * handle UTM and Lat/Lon coordinate conversion.
 * 
 * TODO use defaults only after
 * checking the properties for overridden concepts - for current uses this is OK but depends
 * on the im ontologies being available.
 * 
 * @author Ferd
 *
 */
public class GeoNS extends NS {

    // things
    public static IConcept PLAIN_REGION                     = null;
    public static IConcept WATERSHED                        = null;
    public static IConcept STREAM                           = null;
    public static IConcept ELEVATION                        = null;
    public static IConcept PIT_FILLED_ELEVATION             = null;
    public static IConcept SLOPE                            = null;
    public static IConcept SLOPE_LENGTH                     = null;
    public static IConcept ASPECT                           = null;
    public static IConcept FLOW_DIRECTION                   = null;
    public static IConcept TOTAL_CONTRIBUTING_AREA          = null;
    public static IConcept STREAM_PRESENCE                  = null;
    public static IConcept STREAM_OUTLET                    = null;
    public static IConcept PRECIPITATION_VOLUME             = null;
    public static IConcept SNOWFALL_VOLUME                  = null;
    public static IConcept INFILTRATED_VOLUME               = null;
    public static IConcept EVAPORATED_VOLUME                = null;
    public static IConcept LOCALLY_EXCHANGED_VOLUME         = null;
    public static IConcept RUNOFF_VOLUME                    = null;
    public static IConcept HYDROLOGIC_SOIL_GROUP            = null;
    public static IConcept GLOBCOVER_CLASS                  = null;
    public static IConcept ATMOSPHERIC_TEMPERATURE          = null;
    public static IConcept WEATHER_STATION                  = null;

    /*
     * these must be exported somewhere in the active worldview
     */
    public static String   WATERSHED_CONCEPT                = "IM.WATERSHED";
    public static String   PLAIN_REGION_CONCEPT             = "EARTH.REGION";
    public static String   STREAM_CONCEPT                   = "IM.STREAM";
    public static String   STREAM_OUTLET_CONCEPT            = "IM.STREAM_OUTLET";
    public static String   ELEVATION_CONCEPT                = "IM.GEOGRAPHICAL_ELEVATION";
    public static String   PIT_FILLED_ELEVATION_CONCEPT     = "IM.PIT_FILLED_GEOGRAPHICAL_ELEVATION";
    public static String   SLOPE_CONCEPT                    = "IM.GEOGRAPHICAL_SLOPE";
    public static String   ASPECT_CONCEPT                   = "IM.GEOGRAPHICAL_ASPECT";
    public static String   PRECIPITATION_VOLUME_CONCEPT     = "IM.PRECIPITATION_VOLUME";
    public static String   SNOWFALL_VOLUME_CONCEPT          = "IM.SNOWFALL_VOLUME";
    public static String   INFILTRATED_VOLUME_CONCEPT       = "IM.INFILTRATED_WATER_VOLUME";
    public static String   EVAPORATED_VOLUME_CONCEPT        = "IM.EVAPORATED_WATER_VOLUME";
    public static String   LOCALLY_EXCHANGED_VOLUME_CONCEPT = "IM.LOCALLY_EXCHANGED_WATER_VOLUME";
    public static String   RUNOFF_VOLUME_CONCEPT            = "IM.RUNOFF_WATER_VOLUME";
    public static String   SLOPE_LENGTH_CONCEPT             = "IM.GEOGRAPHICAL_SLOPE_LENGTH";
    public static String   FLOW_DIRECTION_CONCEPT           = "IM.WATER_FLOW_DIRECTION";
    public static String   TOTAL_CONTRIBUTING_AREA_CONCEPT  = "IM.TOTAL_FLOW_CONTRIBUTING_AREA";
    public static String   HYDROLOGIC_SOIL_GROUP_CONCEPT    = "IM.HYDROLOGIC_SOIL_GROUP";
    public static String   GLOBCOVER_CLASS_CONCEPT          = "IM.GLOBCOVER_CLASS";
    public static String   ATMOSPHERIC_TEMPERATURE_CONCEPT  = "IM.ATMOSPHERIC_TEMPERATURE";
    public static String   WEATHER_STATION_CONCEPT          = "IM.WEATHER_STATION";

    public static boolean synchronize() {

        if (!NS.synchronize()) {
            return false;
        }

        if (ELEVATION == null) {

            try {
                PLAIN_REGION = checkClass(KLAB.MMANAGER
                        .getExportedKnowledge(PLAIN_REGION_CONCEPT), IConcept.class);
                ELEVATION = checkClass(KLAB.MMANAGER.getExportedKnowledge(ELEVATION_CONCEPT), IConcept.class);
                PIT_FILLED_ELEVATION = checkClass(KLAB.MMANAGER
                        .getExportedKnowledge(PIT_FILLED_ELEVATION_CONCEPT), IConcept.class);
                PRECIPITATION_VOLUME = checkClass(KLAB.MMANAGER
                        .getExportedKnowledge(PRECIPITATION_VOLUME_CONCEPT), IConcept.class);
                SNOWFALL_VOLUME = checkClass(KLAB.MMANAGER
                        .getExportedKnowledge(SNOWFALL_VOLUME_CONCEPT), IConcept.class);
                INFILTRATED_VOLUME = checkClass(KLAB.MMANAGER
                        .getExportedKnowledge(INFILTRATED_VOLUME_CONCEPT), IConcept.class);
                EVAPORATED_VOLUME = checkClass(KLAB.MMANAGER
                        .getExportedKnowledge(EVAPORATED_VOLUME_CONCEPT), IConcept.class);
                LOCALLY_EXCHANGED_VOLUME = checkClass(KLAB.MMANAGER
                        .getExportedKnowledge(LOCALLY_EXCHANGED_VOLUME_CONCEPT), IConcept.class);
                RUNOFF_VOLUME = checkClass(KLAB.MMANAGER
                        .getExportedKnowledge(RUNOFF_VOLUME_CONCEPT), IConcept.class);
                STREAM_OUTLET = checkClass(KLAB.MMANAGER
                        .getExportedKnowledge(STREAM_OUTLET_CONCEPT), IConcept.class);
                WATERSHED = checkClass(KLAB.MMANAGER.getExportedKnowledge(WATERSHED_CONCEPT), IConcept.class);
                SLOPE = checkClass(KLAB.MMANAGER.getExportedKnowledge(SLOPE_CONCEPT), IConcept.class);
                ASPECT = checkClass(KLAB.MMANAGER.getExportedKnowledge(ASPECT_CONCEPT), IConcept.class);
                SLOPE_LENGTH = checkClass(KLAB.MMANAGER
                        .getExportedKnowledge(SLOPE_LENGTH_CONCEPT), IConcept.class);
                FLOW_DIRECTION = checkClass(KLAB.MMANAGER
                        .getExportedKnowledge(FLOW_DIRECTION_CONCEPT), IConcept.class);
                TOTAL_CONTRIBUTING_AREA = checkClass(KLAB.MMANAGER
                        .getExportedKnowledge(TOTAL_CONTRIBUTING_AREA_CONCEPT), IConcept.class);
                HYDROLOGIC_SOIL_GROUP = checkClass(KLAB.MMANAGER
                        .getExportedKnowledge(HYDROLOGIC_SOIL_GROUP_CONCEPT), IConcept.class);
                GLOBCOVER_CLASS = checkClass(KLAB.MMANAGER
                        .getExportedKnowledge(GLOBCOVER_CLASS_CONCEPT), IConcept.class);
                STREAM = checkClass(KLAB.MMANAGER.getExportedKnowledge(STREAM_CONCEPT), IConcept.class);
                STREAM_PRESENCE = makePresence(checkClass(KLAB.MMANAGER
                        .getExportedKnowledge(STREAM_CONCEPT), IConcept.class));
                ATMOSPHERIC_TEMPERATURE = checkClass(KLAB.MMANAGER
                        .getExportedKnowledge(ATMOSPHERIC_TEMPERATURE_CONCEPT), IConcept.class);
                WEATHER_STATION = checkClass(KLAB.MMANAGER
                        .getExportedKnowledge(WEATHER_STATION_CONCEPT), IConcept.class);
                
            } catch (ThinklabException e) {
                return false;
            }
        }

        return true;
    }

    final private static double R_MAJOR = 6378137.0;
    final private static double R_MINOR = 6356752.3142;

    /**
     * Elliptical mercator conversion of lon/lat
     * 
     * @param x
     * @param y
     * @return
     */
    public static double[] merc(double x, double y) {
        return new double[] { mercX(x), mercY(y) };
    }

    public static double mercX(double lon) {
        return R_MAJOR * Math.toRadians(lon);
    }

    public static double mercY(double lat) {
        if (lat > 89.5) {
            lat = 89.5;
        }
        if (lat < -89.5) {
            lat = -89.5;
        }
        double temp = R_MINOR / R_MAJOR;
        double es = 1.0 - (temp * temp);
        double eccent = Math.sqrt(es);
        double phi = Math.toRadians(lat);
        double sinphi = Math.sin(phi);
        double con = eccent * sinphi;
        double com = 0.5 * eccent;
        con = Math.pow(((1.0 - con) / (1.0 + con)), com);
        double ts = Math.tan(0.5 * ((Math.PI * 0.5) - phi)) / con;
        double y = 0 - R_MAJOR * Math.log(ts);
        return y;
    }

    static public void main(String[] args) {
        System.out.println(mercX(10.2) + " " + mercY(-55.2));
    }

    static double pi             = 3.14159265358979;

    /* Ellipsoid model constants (actual values here are for WGS84) */
    static double sm_a           = 6378137.0;
    static double sm_b           = 6356752.314;
    static double sm_EccSquared  = 6.69437999013e-03;

    static double UTMScaleFactor = 0.9996;

    /**
     * Distance in KM between two lat/lon coordinates
     * @param reference
     * @param point
     * @return
     */
    public static double getDistance(Coordinate reference, Coordinate point) {
        // TODO use org.geotools.referencing.GeodeticCalculator?

        // d = acos(sin(lat1) * sin(lat2) + cos(lat1) * cos(lat2) * cos(lon2 - lon1)) * R
        double distanceInKm = Math.acos(Math.sin(Math.toRadians(reference.y))
                * Math.sin(Math.toRadians(point.y)) + Math.cos(Math.toRadians(reference.y))
                        * Math.cos(Math.toRadians(point.y))
                        * Math.cos(Math.toRadians(point.x) - Math.toRadians(reference.x)))
                * earthRadiusInKm;
        return distanceInKm;
    }

    /**
    * Linear distance in KM between the centers of any two shapes.
    *
    * @param reference
    * @param point
    * @return
    */
    public static double getDistance(IGeometricShape a, IGeometricShape b) {
        return getDistance(a.getStandardizedGeometry().getCentroid().getCoordinate(), b
                .getStandardizedGeometry().getCentroid().getCoordinate());
    }

    /*
    * DegToRad
    *
    * Converts degrees to radians.
    *
    */
    static public double DegToRad(double deg) {
        return (deg / 180.0 * pi);
    }

    /*
    * RadToDeg
    *
    * Converts radians to degrees.
    *
    */
    static public double RadToDeg(double rad) {
        return (rad / pi * 180.0);
    }

    /*
    * ArcLengthOfMeridian
    *
    * Computes the ellipsoidal distance from the equator to a point at a
    * given latitude.
    *
    * Reference: Hoffmann-Wellenhof, B., Lichtenegger, H., and Collins, J.,
    * GPS: Theory and Practice, 3rd ed.  New York: Springer-Verlag Wien, 1994.
    *
    * Inputs:
    *     phi - Latitude of the point, in radians.
    *
    * Globals:
    *     sm_a - Ellipsoid model major axis.
    *     sm_b - Ellipsoid model minor axis.
    *
    * Returns:
    *     The ellipsoidal distance of the point from the equator, in meters.
    *
    */
    static public double ArcLengthOfMeridian(double phi) {
        double alpha, beta, gamma, delta, epsilon, n;
        double result;

        /* Precalculate n */
        n = (sm_a - sm_b) / (sm_a + sm_b);

        /* Precalculate alpha */
        alpha = ((sm_a + sm_b) / 2.0) * (1.0 + (Math.pow(n, 2.0) / 4.0) + (Math.pow(n, 4.0) / 64.0));

        /* Precalculate beta */
        beta = (-3.0 * n / 2.0) + (9.0 * Math.pow(n, 3.0) / 16.0) + (-3.0 * Math.pow(n, 5.0) / 32.0);

        /* Precalculate gamma */
        gamma = (15.0 * Math.pow(n, 2.0) / 16.0) + (-15.0 * Math.pow(n, 4.0) / 32.0);

        /* Precalculate delta */
        delta = (-35.0 * Math.pow(n, 3.0) / 48.0) + (105.0 * Math.pow(n, 5.0) / 256.0);

        /* Precalculate epsilon */
        epsilon = (315.0 * Math.pow(n, 4.0) / 512.0);

        /* Now calculate the sum of the series and return */
        result = alpha
                * (phi + (beta * Math.sin(2.0 * phi)) + (gamma * Math.sin(4.0 * phi))
                        + (delta * Math.sin(6.0 * phi)) + (epsilon * Math.sin(8.0 * phi)));

        return result;
    }

    /*
    * UTMCentralMeridian
    *
    * Determines the central meridian for the given UTM zone.
    *
    * Inputs:
    *     zone - An integer value designating the UTM zone, range [1,60].
    *
    * Returns:
    *   The central meridian for the given UTM zone, in radians, or zero
    *   if the UTM zone parameter is outside the range [1,60].
    *   Range of the central meridian is the radian equivalent of [-177,+177].
    *
    */
    static public double UTMCentralMeridian(int zone) {
        return DegToRad(-183.0 + (zone * 6.0));
    }

    /*
    * FootpointLatitude
    *
    * Computes the footpoint latitude for use in converting transverse
    * Mercator coordinates to ellipsoidal coordinates.
    *
    * Reference: Hoffmann-Wellenhof, B., Lichtenegger, H., and Collins, J.,
    *   GPS: Theory and Practice, 3rd ed.  New York: Springer-Verlag Wien, 1994.
    *
    * Inputs:
    *   y - The UTM northing coordinate, in meters.
    *
    * Returns:
    *   The footpoint latitude, in radians.
    *
    */
    static double FootpointLatitude(double y) {
        double y_, alpha_, beta_, gamma_, delta_, epsilon_, n;
        double result;

        /* Precalculate n (Eq. 10.18) */
        n = (sm_a - sm_b) / (sm_a + sm_b);

        /* Precalculate alpha_ (Eq. 10.22) */
        /* (Same as alpha in Eq. 10.17) */
        alpha_ = ((sm_a + sm_b) / 2.0) * (1 + (Math.pow(n, 2.0) / 4) + (Math.pow(n, 4.0) / 64));

        /* Precalculate y_ (Eq. 10.23) */
        y_ = y / alpha_;

        /* Precalculate beta_ (Eq. 10.22) */
        beta_ = (3.0 * n / 2.0) + (-27.0 * Math.pow(n, 3.0) / 32.0) + (269.0 * Math.pow(n, 5.0) / 512.0);

        /* Precalculate gamma_ (Eq. 10.22) */
        gamma_ = (21.0 * Math.pow(n, 2.0) / 16.0) + (-55.0 * Math.pow(n, 4.0) / 32.0);

        /* Precalculate delta_ (Eq. 10.22) */
        delta_ = (151.0 * Math.pow(n, 3.0) / 96.0) + (-417.0 * Math.pow(n, 5.0) / 128.0);

        /* Precalculate epsilon_ (Eq. 10.22) */
        epsilon_ = (1097.0 * Math.pow(n, 4.0) / 512.0);

        /* Now calculate the sum of the series (Eq. 10.21) */
        result = y_ + (beta_ * Math.sin(2.0 * y_)) + (gamma_ * Math.sin(4.0 * y_))
                + (delta_ * Math.sin(6.0 * y_)) + (epsilon_ * Math.sin(8.0 * y_));

        return result;
    }

    /*
    * MapLatLonToXY
    *
    * Converts a latitude/longitude pair to x and y coordinates in the
    * Transverse Mercator projection.  Note that Transverse Mercator is not
    * the same as UTM; a scale factor is required to convert between them.
    *
    * Reference: Hoffmann-Wellenhof, B., Lichtenegger, H., and Collins, J.,
    * GPS: Theory and Practice, 3rd ed.  New York: Springer-Verlag Wien, 1994.
    *
    * Inputs:
    *    phi - Latitude of the point, in radians.
    *    lambda - Longitude of the point, in radians.
    *    lambda0 - Longitude of the central meridian to be used, in radians.
    *
    * Outputs:
    *    xy - A 2-element array containing the x and y coordinates
    *         of the computed point.
    *
    * Returns:
    *    The function does not return a value.
    *
    */
    static public double[] MapLatLonToXY(double phi, double lambda, double lambda0) {
        double N, nu2, ep2, t, t2, l;
        double l3coef, l4coef, l5coef, l6coef, l7coef, l8coef;

        /* Precalculate ep2 */
        ep2 = (Math.pow(sm_a, 2.0) - Math.pow(sm_b, 2.0)) / Math.pow(sm_b, 2.0);

        /* Precalculate nu2 */
        nu2 = ep2 * Math.pow(Math.cos(phi), 2.0);

        /* Precalculate N */
        N = Math.pow(sm_a, 2.0) / (sm_b * Math.sqrt(1 + nu2));

        /* Precalculate t */
        t = Math.tan(phi);
        t2 = t * t;

        /* Precalculate l */
        l = lambda - lambda0;

        /* Precalculate coefficients for l**n in the equations below
           so a normal human being can read the expressions for easting
           and northing
           -- l**1 and l**2 have coefficients of 1.0 */
        l3coef = 1.0 - t2 + nu2;

        l4coef = 5.0 - t2 + 9 * nu2 + 4.0 * (nu2 * nu2);

        l5coef = 5.0 - 18.0 * t2 + (t2 * t2) + 14.0 * nu2 - 58.0 * t2 * nu2;

        l6coef = 61.0 - 58.0 * t2 + (t2 * t2) + 270.0 * nu2 - 330.0 * t2 * nu2;

        l7coef = 61.0 - 479.0 * t2 + 179.0 * (t2 * t2) - (t2 * t2 * t2);

        l8coef = 1385.0 - 3111.0 * t2 + 543.0 * (t2 * t2) - (t2 * t2 * t2);

        /* Calculate easting (x) */
        double x = N * Math.cos(phi) * l
                + (N / 6.0 * Math.pow(Math.cos(phi), 3.0) * l3coef * Math.pow(l, 3.0))
                + (N / 120.0 * Math.pow(Math.cos(phi), 5.0) * l5coef * Math.pow(l, 5.0))
                + (N / 5040.0 * Math.pow(Math.cos(phi), 7.0) * l7coef * Math.pow(l, 7.0));

        /* Calculate northing (y) */
        double y = ArcLengthOfMeridian(phi) + (t / 2.0 * N * Math.pow(Math.cos(phi), 2.0) * Math.pow(l, 2.0))
                + (t / 24.0 * N * Math.pow(Math.cos(phi), 4.0) * l4coef * Math.pow(l, 4.0))
                + (t / 720.0 * N * Math.pow(Math.cos(phi), 6.0) * l6coef * Math.pow(l, 6.0))
                + (t / 40320.0 * N * Math.pow(Math.cos(phi), 8.0) * l8coef * Math.pow(l, 8.0));

        return new double[] { x, y };
    }

    /*
    * MapXYToLatLon
    *
    * Converts x and y coordinates in the Transverse Mercator projection to
    * a latitude/longitude pair.  Note that Transverse Mercator is not
    * the same as UTM; a scale factor is required to convert between them.
    *
    * Reference: Hoffmann-Wellenhof, B., Lichtenegger, H., and Collins, J.,
    *   GPS: Theory and Practice, 3rd ed.  New York: Springer-Verlag Wien, 1994.
    *
    * Inputs:
    *   x - The easting of the point, in meters.
    *   y - The northing of the point, in meters.
    *   lambda0 - Longitude of the central meridian to be used, in radians.
    *
    * Outputs:
    *   philambda - A 2-element containing the latitude and longitude
    *               in radians.
    *
    * Returns:
    *   The function does not return a value.
    *
    * Remarks:
    *   The local variables Nf, nuf2, tf, and tf2 serve the same purpose as
    *   N, nu2, t, and t2 in MapLatLonToXY, but they are computed with respect
    *   to the footpoint latitude phif.
    *
    *   x1frac, x2frac, x2poly, x3poly, etc. are to enhance readability and
    *   to optimize computations.
    *
    */
    static public double[] MapXYToLatLon(double x, double y, double lambda0) {
        double phif, Nf, Nfpow, nuf2, ep2, tf, tf2, tf4, cf;
        double x1frac, x2frac, x3frac, x4frac, x5frac, x6frac, x7frac, x8frac;
        double x2poly, x3poly, x4poly, x5poly, x6poly, x7poly, x8poly;

        /* Get the value of phif, the footpoint latitude. */
        phif = FootpointLatitude(y);

        /* Precalculate ep2 */
        ep2 = (Math.pow(sm_a, 2.0) - Math.pow(sm_b, 2.0)) / Math.pow(sm_b, 2.0);

        /* Precalculate cos (phif) */
        cf = Math.cos(phif);

        /* Precalculate nuf2 */
        nuf2 = ep2 * Math.pow(cf, 2.0);

        /* Precalculate Nf and initialize Nfpow */
        Nf = Math.pow(sm_a, 2.0) / (sm_b * Math.sqrt(1 + nuf2));
        Nfpow = Nf;

        /* Precalculate tf */
        tf = Math.tan(phif);
        tf2 = tf * tf;
        tf4 = tf2 * tf2;

        /* Precalculate fractional coefficients for x**n in the equations
           below to simplify the expressions for latitude and longitude. */
        x1frac = 1.0 / (Nfpow * cf);

        Nfpow *= Nf; /* now equals Nf**2) */
        x2frac = tf / (2.0 * Nfpow);

        Nfpow *= Nf; /* now equals Nf**3) */
        x3frac = 1.0 / (6.0 * Nfpow * cf);

        Nfpow *= Nf; /* now equals Nf**4) */
        x4frac = tf / (24.0 * Nfpow);

        Nfpow *= Nf; /* now equals Nf**5) */
        x5frac = 1.0 / (120.0 * Nfpow * cf);

        Nfpow *= Nf; /* now equals Nf**6) */
        x6frac = tf / (720.0 * Nfpow);

        Nfpow *= Nf; /* now equals Nf**7) */
        x7frac = 1.0 / (5040.0 * Nfpow * cf);

        Nfpow *= Nf; /* now equals Nf**8) */
        x8frac = tf / (40320.0 * Nfpow);

        /* Precalculate polynomial coefficients for x**n.
           -- x**1 does not have a polynomial coefficient. */
        x2poly = -1.0 - nuf2;

        x3poly = -1.0 - 2 * tf2 - nuf2;

        x4poly = 5.0 + 3.0 * tf2 + 6.0 * nuf2 - 6.0 * tf2 * nuf2 - 3.0 * (nuf2 * nuf2) - 9.0 * tf2
                * (nuf2 * nuf2);

        x5poly = 5.0 + 28.0 * tf2 + 24.0 * tf4 + 6.0 * nuf2 + 8.0 * tf2 * nuf2;

        x6poly = -61.0 - 90.0 * tf2 - 45.0 * tf4 - 107.0 * nuf2 + 162.0 * tf2 * nuf2;

        x7poly = -61.0 - 662.0 * tf2 - 1320.0 * tf4 - 720.0 * (tf4 * tf2);

        x8poly = 1385.0 + 3633.0 * tf2 + 4095.0 * tf4 + 1575 * (tf4 * tf2);

        /* Calculate latitude */
        double lat = phif + x2frac * x2poly * (x * x) + x4frac * x4poly * Math.pow(x, 4.0) + x6frac * x6poly
                * Math.pow(x, 6.0) + x8frac * x8poly * Math.pow(x, 8.0);

        /* Calculate longitude */
        double lon = lambda0 + x1frac * x + x3frac * x3poly * Math.pow(x, 3.0) + x5frac * x5poly
                * Math.pow(x, 5.0) + x7frac * x7poly * Math.pow(x, 7.0);

        return new double[] { lat, lon };
    }

    /*
    * LatLonToUTMXY
    *
    * Converts a latitude/longitude pair to x and y coordinates in the
    * Universal Transverse Mercator projection.
    *
    * Inputs:
    *   lat - Latitude of the point, in radians.
    *   lon - Longitude of the point, in radians.
    *   zone - UTM zone to be used for calculating values for x and y.
    *          If zone is less than 1 or greater than 60, the routine
    *          will determine the appropriate zone from the value of lon.
    *
    * Outputs:
    *   xy - A 2-element array where the UTM x and y values will be stored.
    *
    * Returns:
    *   The UTM zone used for calculating the values of x and y.
    *
    */
    static public double[] LatLonToUTMXY(double lat, double lon, int zone) {
        double[] xy = MapLatLonToXY(lat, lon, UTMCentralMeridian(zone));

        /* Adjust easting and northing for UTM system. */
        xy[0] = xy[0] * UTMScaleFactor + 500000.0;
        xy[1] = xy[1] * UTMScaleFactor;
        if (xy[1] < 0.0)
            xy[1] = xy[1] + 10000000.0;

        return xy;
    }

    /*
    * LatLonToUTMXY
    *
    * Converts a latitude/longitude pair to x and y coordinates in the
    * Universal Transverse Mercator projection.
    *
    * Inputs:
    *   lat - Latitude of the point, in radians.
    *   lon - Longitude of the point, in radians.
    *
    * Outputs:
    *   xy - A 2-element array where the UTM x and y values will be stored.
    *
    * Returns:
    *   The UTM zone used for calculating the values of x and y.
    *
    */
    static public double[] LatLonToUTMXY(double lat, double lon) {
        double[] xy = MapLatLonToXY(lat, lon, UTMCentralMeridian(0));

        /* Adjust easting and northing for UTM system. */
        xy[0] = xy[0] * UTMScaleFactor + 500000.0;
        xy[1] = xy[1] * UTMScaleFactor;
        if (xy[1] < 0.0)
            xy[1] = xy[1] + 10000000.0;

        return xy;
    }

    /*
    * UTMXYToLatLon
    *
    * Converts x and y coordinates in the Universal Transverse Mercator
    * projection to a latitude/longitude pair.
    *
    * Inputs:
    *   x - The easting of the point, in meters.
    *   y - The northing of the point, in meters.
    *   zone - The UTM zone in which the point lies.
    *   southhemi - True if the point is in the southern hemisphere;
    *               false otherwise.
    *
    * Outputs:
    *   latlon - A 2-element array containing the latitude and
    *            longitude of the point, in radians.
    *
    * Returns:
    *   The function does not return a value.
    *
    */
    static public double[] UTMXYToLatLon(double x, double y, int zone, boolean southhemi) {
        x -= 500000.0;
        x /= UTMScaleFactor;

        /* If in southern hemisphere, adjust y accordingly. */
        if (southhemi)
            y -= 10000000.0;

        y /= UTMScaleFactor;

        double cmeridian = UTMCentralMeridian(zone);
        return MapXYToLatLon(x, y, cmeridian);
    }

    public static final double earthRadiusInKm = 6371;

    double[] GoogleBingtoWGS84Mercator(double x, double y) {
        double lon = (x / 20037508.34) * 180;
        double lat = (y / 20037508.34) * 180;

        lat = 180 / Math.PI * (2 * Math.atan(Math.exp(lat * Math.PI / 180)) - Math.PI / 2);
        return new double[] { lon, lat };
    }

    double[] WGS84toGoogleBing(double lon, double lat) {
        double x = lon * 20037508.34 / 180;
        double y = Math.log(Math.tan((90 + lat) * Math.PI / 360)) / (Math.PI / 180);
        y = y * 20037508.34 / 180;
        return new double[] { x, y };
    }
}
