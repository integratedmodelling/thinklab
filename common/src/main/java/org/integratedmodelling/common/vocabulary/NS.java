/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/

package org.integratedmodelling.common.vocabulary;

import java.io.File;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import javax.annotation.Nullable;

import org.apache.commons.collections.MultiMap;
import org.apache.commons.collections.map.MultiValueMap;
import org.integratedmodelling.api.knowledge.IAxiom;
import org.integratedmodelling.api.knowledge.IConcept;
import org.integratedmodelling.api.knowledge.IExpression;
import org.integratedmodelling.api.knowledge.IKnowledge;
import org.integratedmodelling.api.knowledge.IOntology;
import org.integratedmodelling.api.knowledge.IProperty;
import org.integratedmodelling.api.knowledge.ISemantic;
import org.integratedmodelling.api.metadata.IMetadata;
import org.integratedmodelling.api.modelling.IAnnotation;
import org.integratedmodelling.api.modelling.IClassification;
import org.integratedmodelling.api.modelling.IClassifier;
import org.integratedmodelling.api.modelling.IConditionalObserver;
import org.integratedmodelling.api.modelling.IKnowledgeObject;
import org.integratedmodelling.api.modelling.IMeasuringObserver;
import org.integratedmodelling.api.modelling.IModel;
import org.integratedmodelling.api.modelling.IModelObject;
import org.integratedmodelling.api.modelling.INamespace;
import org.integratedmodelling.api.modelling.IObservable;
import org.integratedmodelling.api.modelling.IObserver;
import org.integratedmodelling.api.modelling.IProcess;
import org.integratedmodelling.api.modelling.IRankingObserver;
import org.integratedmodelling.api.modelling.IScale;
import org.integratedmodelling.api.modelling.IState;
import org.integratedmodelling.api.modelling.ISubject;
import org.integratedmodelling.api.modelling.IValuingObserver;
import org.integratedmodelling.api.modelling.resolution.IResolutionContext;
import org.integratedmodelling.collections.Pair;
import org.integratedmodelling.collections.Triple;
import org.integratedmodelling.common.classification.Classification;
import org.integratedmodelling.common.classification.Classifier;
import org.integratedmodelling.common.configuration.KLAB;
import org.integratedmodelling.common.kim.KIM;
import org.integratedmodelling.common.owl.Concept;
import org.integratedmodelling.common.utils.MapUtils;
import org.integratedmodelling.common.utils.Permutations;
import org.integratedmodelling.common.utils.StringUtils;
import org.integratedmodelling.exceptions.ThinklabException;
import org.integratedmodelling.exceptions.ThinklabInternalErrorException;
import org.integratedmodelling.exceptions.ThinklabRuntimeException;
import org.integratedmodelling.exceptions.ThinklabValidationException;
import org.integratedmodelling.lang.Axiom;
import org.integratedmodelling.lang.SemanticType;

/**
 * One and only holder of semantic concept IDs. These are all expected to exist
 * in the default core knowledge base, known to both clients and servers.
 * 
 * @author Ferd
 * 
 */
public class NS {

    /**
     * Sometimes we want these without referring to actual concepts.
     * 
     * @author Ferd
     *
     */
    public static enum CoreType {
        SUBJECT,
        PROCESS,
        EVENT,
        QUALITY,
        TRAIT,
        RELATIONSHIP
    };

    /**
     * Describes the full structure of traits exposed and adopted for a class type and all
     * its subclasses.
     * 
     * @author fvilla
     *
     */
    public static class ClassTraitStructure {
        IConcept                             abstractClass;
        List<IObservable>                    abstractTraitsExposed = new ArrayList<>();
        HashMap<IConcept, List<IObservable>> concreteTraitsAdopted = new HashMap<>();

        ClassTraitStructure(IConcept c) {
            abstractClass = c;
        }
    }

    /**
     * Descriptor for a role. Starting after KLAB-76 roles are scoped and not
     * based on restrictions, to allow their use within scenarios.
     * 
     * @author Ferd
     *
     */
    public static class RoleDescriptor {

        IConcept within;
        IConcept target;
        IConcept targetSubject;
        IConcept role;
        // IProperty property;
        String   scenario;
        String   namespaceId;
        IScale   coverage;

        private String sig() {
            return within + "|" + target + "|" + role + "|" + namespaceId;
        }

        @Override
        public boolean equals(Object o) {
            return o instanceof RoleDescriptor && ((RoleDescriptor) o).sig().equals(sig());
        }

        @Override
        public int hashCode() {
            return sig().hashCode();
        };

        @Override
        public String toString() {
            return "concept " + target + " has role " + role + " within " + within +
                    (scenario == null ? "" : (" in scenario " + scenario));
        }

        public Map<?, ?> dump() {
            return MapUtils
                    .of("role", role.toString(), "target", target.toString(), "for", targetSubject == null
                            ? null : targetSubject.toString(), "namespace", namespaceId, "within", within
                                    .toString());
        }
    }

    private static Map<String, String>                    derivedConceptDefs   = new ConcurrentHashMap<>();
    private static HashMap<IConcept, ClassTraitStructure> traitContextualizers = new HashMap<>();
    private static Set<String>                            nothings             = new HashSet<>();

    // role descriptors indexed by role
    private static MultiMap roles = new MultiValueMap();

    /*
     * these are only available after NS.synchronize() has returned true.
     */
    public static IConcept  DATA_REDUCTION_TRAIT;
    public static IConcept  AVERAGE_TRAIT;
    public static IConcept  MINIMUM_TRAIT;
    public static IConcept  MAXIMUM_TRAIT;
    public static IConcept  LENGTH;
    public static IConcept  VOLUME;
    public static IConcept  DISTANCE;
    public static IConcept  AREA;
    public static IProperty FLOW;

    /*
     * traits to categorize inputs and outputs
     */
    public static IConcept HOURLY_TRAIT  = null;
    public static IConcept DAILY_TRAIT   = null;
    public static IConcept WEEKLY_TRAIT  = null;
    public static IConcept MONTHLY_TRAIT = null;
    public static IConcept YEARLY_TRAIT  = null;

    /*
     * traits for viewshed computation and other GIS ops
     */
    public static IConcept VISIBLE_TRAIT = null;

    /**
     * Ontologies created by loading knowledge from thinklab modeling language
     * files get URLs with this prefix unless a different one is specified in
     * project properties.
     */
    public static final String DEFAULT_THINKLAB_ONTOLOGY_PREFIX = "http://www.integratedmodelling.org/ks";

    // // the universe and its complement
    // public static final String THING = "owl:Thing";
    // public static final String NOTHING = "owl:Nothing";

    // domain concepts for known extents
    public static final String SPACE_DOMAIN = "imcore:Space";
    public static final String TIME_DOMAIN  = "imcore:Time";

    // core objects that everyone will need
    public static final String INTEGER = "thinklab:ShortInteger";
    public static final String FLOAT   = "thinklab:FloatingPoint";
    public static final String TEXT    = "thinklab:Text";
    public static final String LONG    = "thinklab:LongInteger";
    public static final String DOUBLE  = "thinklab:LongFloatingPoint";
    public static final String BOOLEAN = "thinklab:Boolean";
    public static final String NUMBER  = "thinklab:Number";

    public static final String SHAPE          = "geospace:SpatialRecord";
    public static final String POLYGON        = "geospace:PolygonValue";
    public static final String POINT          = "geospace:PointValue";
    public static final String LINE           = "geospace:LineValue";
    public static final String POLYGON_DTYPE  = "geospace:polygon";
    public static final String POINT_DTYPE    = "geospace:point";
    public static final String LINE_DTYPE     = "geospace:line";
    public static final String GEOMETRY_DTYPE = "geospace:geometry";

    // standard operations
    public static final String OPERATION_EQUALS           = "thinklab:Equal";
    public static final String OPERATION_GREATER_OR_EQUAL = "thinklab:GreaterEqual";
    public static final String OPERATION_GREATER_THAN     = "thinklab:GreaterThan";
    public static final String OPERATION_LESS_OR_EQUAL    = "thinklab:LowerEqual";
    public static final String OPERATION_LESS_THAN        = "thinklab:LowerThan";
    public static final String OPERATION_NOT_EQUALS       = "thinklab:NotEqual";
    public static final String OPERATION_LIKE             = "thinklab:MatchesPattern";

    // topological operators and other specifically for space and time
    static final public String OPERATION_CROSSES             = "thinklab:Crosses";
    static final public String OPERATION_INTERSECTS          = "thinklab:Intersects";
    static final public String OPERATION_INTERSECTS_ENVELOPE = "thinklab:IntersectsEnvelope";
    static final public String OPERATION_COVERS              = "thinklab:Covers";
    static final public String OPERATION_COVERED_BY          = "thinklab:IsCoveredBy";
    static final public String OPERATION_OVERLAPS            = "thinklab:Overlaps";
    static final public String OPERATION_TOUCHES             = "thinklab:Touches";
    static final public String OPERATION_CONTAINS            = "thinklab:Contains";
    static final public String OPERATION_CONTAINED_BY        = "thinklab:IsContainedBy";
    static final public String OPERATION_NEAREST_NEIGHBOUR   = "thinklab:NearestNeighbour";
    static final public String OPERATION_WITHIN_DISTANCE     = "thinklab:WithinDistance";

    // core and convenience properties
    public static final String CLASSIFICATION_PROPERTY = "imcore:isClassification";
    public static String       IS_ABSTRACT             = "imcore:isAbstract";
    public static String       BASE_DECLARATION        = "imcore:baseDeclaration";
    public static String       ORDER_PROPERTY          = "imcore:orderingRank";

    // core observation ontology
    public static final String OBSERVATION             = "imcore:Observation";
    public static final String DIRECT_OBSERVATION      = "imcore:DirectObservation";
    public static final String INDIRECT_OBSERVATION    = "imcore:IndirectObservation";
    public static final String CLASSIFICATION          = "imcore:Classification";
    public static final String MEASUREMENT             = "imcore:Measurement";
    public static final String QUANTIFICATION          = "imcore:Quantification";
    public static final String RANKING                 = "imcore:Ranking";
    public static final String COUNT_OBSERVATION       = "imcore:CountObservation";
    public static final String PERCENTAGE_OBSERVATION  = "imcore:PercentageObservation";
    public static final String PROPORTION_OBSERVATION  = "imcore:ProportionObservation";
    public static final String RATIO_OBSERVATION       = "imcore:RatioObservation";
    public static final String DISTANCE_OBSERVATION    = "imcore:DistanceObservation";
    public static final String VALUE_OBSERVATION       = "imcore:Valuation";
    public static final String IDENTIFICATION          = "imcore:Identification";
    public static final String PROBABILITY_OBSERVATION = "imcore:ProbabilityObservation";
    public static final String UNCERTAINTY_OBSERVATION = "imcore:UncertaintyObservation";

    // the (invariable) property observed by an Acknowledgement observer
    public static final String PRESENCE_OBSERVATION = "imcore:PresenceObservation";

    // observable types and traits
    public static final String BOOLEAN_RANKING            = "imcore:PresenceAbsenceQuality";
    public static final String ORDINAL_RANKING            = "imcore:OrderedClassificationQuality";
    public static final String CONTINUOUS_NUMERIC_QUALITY = "imcore:ContinuousNumericallyQuantifiableQuality";

    // core properties
    public static final String REPRESENTS_OBSERVABLE = "imcore:representsObservable";

    // conceptualized extents
    public static final String GRID_EXTENT  = "geospace:GridExtent";
    public static final String SHAPE_EXTENT = "geospace:ShapeExtent";

    // im ontology annotations affecting the ranking system. Used as key in
    // maps, so
    // they don't depend on the ontology being in the system.
    public static final String LEXICAL_SCOPE          = "im:lexical-scope";
    public static final String TRAIT_CONCORDANCE      = "im:trait-concordance";
    public static final String SCALE_COVERAGE         = "im:scale-coverage";
    public static final String SCALE_SPECIFICITY      = "im:scale-specificity";
    public static final String INHERENCY              = "im:inherency";
    public static final String EVIDENCE               = "im:evidence";
    public static final String NETWORK_REMOTENESS     = "im:network-remoteness";
    public static final String SCALE_COHERENCY        = "im:scale-coherency";
    public static final String SUBJECTIVE_CONCORDANCE = "im:subjective-concordance";

    // only annotation used for subjective ranking in the default behavior
    public static final String RELIABILITY = "im:reliability";

    // spatial convenience relationships
    public static final String GEOSPACE_HAS_SHAPE = "geospace:hasShape";

    // plug-in elements (return types for API-defined functions)
    public static final String STATE_CONTEXTUALIZER        = "modelling.thinklab:StateContextualizer";
    public static final String SUBJECT_CONTEXTUALIZER      = "modelling.thinklab:SubjectContextualizer";
    public static final String PROCESS_CONTEXTUALIZER      = "modelling.thinklab:ProcessContextualizer";
    public static final String EVENT_INSTANTIATOR          = "modelling.thinklab:EventInstantiator";
    public static final String SUBJECT_INSTANTIATOR        = "modelling.thinklab:SubjectInstantiator";
    public static final String EVENT_CONTEXTUALIZER        = "modelling.thinklab:EventContextualizer";
    public static final String RELATIONSHIP_CONTEXTUALIZER = "modelling.thinklab:RelationshipContextualizer";
    public static final String DATASOURCE                  = "modelling.thinklab:DataSource";
    public static final String OBJECTSOURCE                = "modelling.thinklab:ObjectSource";
    public static final String LOOKUP_TABLE                = "modelling.thinklab:LookupTable";

    /*
     * the core properties we use internally to establish observation semantics
     */
    public static final String INHERENT_IN          = "imcore:inherentIn";
    public static final String OBSERVED_INTO        = "imcore:observedInto";
    public static final String PART_OF              = "imcore:partOf";
    public static final String OBSERVED_AS          = "imcore:observedAs";
    public static final String OBSERVATION_TYPE     = "imcore:hasObservationType";
    public static final String STRUCTURING_PROPERTY = "imcore:structuringObjectProperty";
    public static final String STRUCTURAL_PROPERTY  = "imcore:structuralProperty";
    public static final String FUNCTIONAL_PROPERTY  = "imcore:functionalProperty";

    // extent properties
    public static final String SPATIAL_EXTENT_PROPERTY  = "geospace:hasSpatialExtent";
    public static final String TEMPORAL_EXTENT_PROPERTY = "time:hasTemporalExtent";

    /**
     * The core ontology for all the core concepts (which depends only on DOLCE).
     */
    public static final String CORE_ONTOLOGY = "imcore";

    /**
     * Only class that subsumes both observables and traits (but not observations). It also
     * subsumes domains, so anything defined in a domain ontology.
     */
    public static final String CORE_PARTICULAR = "core.dolce:particular";

    /**
     * the root domain for the ontologies. For many reasons it becomes very difficult to keep it in the
     * imcore namespace, so we use the most general abstract in DOLCE.
     */
    public static final String CORE_DOMAIN = "core.dolce:proposition";

    // core concepts to map with the language keywords
    public static final String CORE_OBSERVABLE                  = "imcore:Observable";
    public static final String CORE_OBJECT                      = "imcore:Thing";
    public static final String CORE_PROCESS                     = "imcore:Process";
    public static final String CORE_QUALITY                     = "imcore:Quality";
    public static final String CORE_EVENT                       = "imcore:Event";
    public static final String CORE_TRAIT                       = "imcore:Trait";
    public static final String CORE_IDENTITY_TRAIT              = "imcore:Identity";
    public static final String SUBJECTIVE_TRAIT                 = "imcore:SubjectiveTrait";
    public static final String CORE_QUANTITY                    = "imcore:QuantifiableQuality";
    public static final String CORE_NUMERIC_QUANTITY            = "imcore:ContinuousNumericallyQuantifiableQuality";
    public static final String CORE_ASSERTED_QUALITY            = "imcore:AssertedQuality";
    public static final String CORE_PHYSICAL_OBJECT             = "imcore:PhysicalObject";
    public static final String CORE_PHYSICAL_PROPERTY           = "imcore:PhysicalProperty";
    public static final String CORE_EXTENSIVE_PHYSICAL_PROPERTY = "imcore:ExtensivePhysicalProperty";
    public static final String CORE_INTENSIVE_PHYSICAL_PROPERTY = "imcore:IntensivePhysicalProperty";
    public static final String CORE_ENERGY                      = "imcore:Energy";
    public static final String CORE_ENTROPY                     = "imcore:Entropy";
    public static final String CORE_LENGTH                      = "imcore:Length";
    public static final String CORE_MASS                        = "imcore:Mass";
    public static final String CORE_PROBABILITY                 = "imcore:Probability";
    public static final String CORE_VOLUME                      = "imcore:Volume";
    public static final String CORE_WEIGHT                      = "imcore:Weight";
    public static final String CORE_DURATION                    = "imcore:Duration";
    public static final String CORE_MONETARY_VALUE              = "imcore:MonetaryValue";
    public static final String CORE_PREFERENCE_VALUE            = "imcore:PreferenceValue";
    public static final String CORE_ACCELERATION                = "imcore:Acceleration";
    public static final String CORE_AREA                        = "imcore:Area";
    public static final String CORE_DENSITY                     = "imcore:Density";
    public static final String CORE_ELECTRIC_POTENTIAL          = "imcore:ElectricPotential";
    public static final String CORE_CHARGE                      = "imcore:Charge";
    public static final String CORE_RESISTANCE                  = "imcore:Resistance";
    public static final String CORE_RESISTIVITY                 = "imcore:Resistivity";
    public static final String CORE_PRESSURE                    = "imcore:Pressure";
    public static final String CORE_ANGLE                       = "imcore:Angle";
    public static final String CORE_SPEED                       = "imcore:Speed";
    public static final String CORE_TEMPERATURE                 = "imcore:Temperature";
    public static final String CORE_VISCOSITY                   = "imcore:Viscosity";
    public static final String CORE_AGENT                       = "imcore:Agent";
    public static final String CORE_PATTERN                     = "imcore:Configuration";
    public static final String TYPE                             = "imcore:Type";
    public static final String ORDERING                         = "imcore:Ordering";
    public static final String CORE_REALM_TRAIT                 = "imcore:Realm";
    public static final String ATTRIBUTE_TRAIT                  = "imcore:Attribute";
    public static final String ROLE_TRAIT                       = "imcore:Role";
    public static final String CORE_COUNT                       = "imcore:Numerosity";
    public static final String CORE_PROPORTION                  = "imcore:Proportion";
    public static final String CORE_RATIO                       = "imcore:Ratio";
    public static final String CORE_PRESENCE                    = "imcore:Presence";
    public static final String CORE_VALUE                       = "imcore:Value";
    public static final String CORE_DISTANCE                    = "imcore:Distance";
    public static final String BASE_AGENT                       = "imcore:Agent";
    public static final String REACTIVE_AGENT                   = "imcore:ReactiveAgent";
    public static final String DELIBERATIVE_AGENT               = "imcore:DeliberativeAgent";
    public static final String SOCIAL_AGENT                     = "imcore:SocialAgent";
    public static final String ORGANIZED_AGENT                  = "imcore:OrganizedAgent";
    public static final String CORE_UNCERTAINTY                 = "imcore:Uncertainty";
    public static final String OBSERVABILITY_TRAIT              = "imcore:Observability";
    public static final String EXTENT                           = "imcore:Extent";

    /*
     * IM properties for visualization and ranking
     */
    public static final String DISPLAY_LABEL_PROPERTY    = "im:display-label";
    public static final String NUMERIC_ENCODING_PROPERTY = "im:numeric-encoding";
    public static final String COLORMAP_PROPERTY         = "im:colormap";
    public static final String LOWER_BOUND_PROPERTY      = "im:lower-bound";
    public static final String UPPER_BOUND_PROPERTY      = "im:upper-bound";
    public static final String COLOR_PROPERTY            = "im:color";
    public static final String OPENNESS_PROPERTY         = "im:openness";
    public static final String RELIABILITY_PROPERTY      = "im:reliability";
    public static final String PEER_REVIEW_PROPERTY      = "im:peer-review";
    public static final String EFFICIENCY_PROPERTY       = "im:efficiency";
    public static final String TESTING_PROPERTY          = "im:testing";
    public static final String STATUS_PROPERTY           = "im:status";
    public static final String INTEGRATION_PROPERTY      = "im:integration";
    public static final String DESIGN_PROPERTY           = "im:design";

    /*
     * internal use
     */
    public static final String INTERNAL_DATA_LABEL_PROPERTY = "internal:data-label";
    public static final String AVERAGE_TRAIT_CONCEPT        = "IM.AVERAGE_TRAIT";
    public static final String MINIMUM_TRAIT_CONCEPT        = "IM.MINIMUM_TRAIT";
    public static final String HOURLY_TRAIT_CONCEPT         = "IM.HOURLY_TRAIT";
    public static final String DAILY_TRAIT_CONCEPT          = "IM.DAILY_TRAIT";
    public static final String MONTHLY_TRAIT_CONCEPT        = "IM.MONTHLY_TRAIT";
    public static final String WEEKLY_TRAIT_CONCEPT         = "IM.WEEKLY_TRAIT";
    public static final String YEARLY_TRAIT_CONCEPT         = "IM.YEARLY_TRAIT";
    public static final String VISIBLE_TRAIT_CONCEPT        = "IM.VISIBLE_TRAIT";
    public static final String MAXIMUM_TRAIT_CONCEPT        = "IM.MAXIMUM_TRAIT";

    public static boolean isDerived(IKnowledge k) {
        return derivedConceptDefs.containsKey(k.toString());
    }

    public static void registerNothingConcept(String s) {
        nothings.add(s);
    }

    /**
     * Create a classification based on the encodings stored as metadata in the
     * concept hierarchy.
     * 
     * @param rootClass
     * @param metadataEncodingProperty
     * @return
     */
    public static IClassification createClassificationFromMetadata(IObservable rootClass, String metadataEncodingProperty) {

        ArrayList<Pair<IClassifier, IConcept>> classifiers = new ArrayList<Pair<IClassifier, IConcept>>();

        for (IKnowledge c : rootClass.getTypeAsConcept().getSemanticClosure()) {

            IMetadata m = c.getMetadata();
            Object o = m.get(metadataEncodingProperty);

            if (o != null) {
                classifiers.add(new Pair<IClassifier, IConcept>(new Classifier(o), (IConcept) c));
            }
        }

        return new Classification(rootClass.getTypeAsConcept(), classifiers);
    }

    public static boolean isParticular(ISemantic concept) {
        return concept.getType() instanceof IConcept && concept.getType().is(KLAB.c(CORE_PARTICULAR));
    }

    public static boolean isClass(ISemantic o) {
        IKnowledge concept = o.getType();
        return concept instanceof IConcept && concept.is(KLAB.c(TYPE));
    }

    public static boolean isOrdering(ISemantic o) {
        IKnowledge concept = o.getType();
        return concept instanceof IConcept && concept.is(KLAB.c(ORDERING));
    }

    public static boolean isObservable(ISemantic o) {
        IKnowledge concept = o.getType();
        return concept.is(KLAB.c(CORE_OBSERVABLE)) || concept.is(KLAB.p(STRUCTURING_PROPERTY));
    }

    /**
     * true for all objects - i.e., subjects, processes and events.
     * 
     * @param concept
     * @return
     */
    public static boolean isObject(ISemantic o) {
        IKnowledge concept = o.getType();
        return concept.is(KLAB.c(NS.CORE_PHYSICAL_OBJECT))
                || concept.is(KLAB.c(NS.CORE_EVENT)) || concept.is(KLAB.c(NS.CORE_PROCESS));
    }

    public static boolean isRelationship(ISemantic o) {

        IKnowledge knowledge = o.getType();

        /*
         * FIXME this is primitive - we need core types for observable relationships.
         */
        return knowledge instanceof IProperty && ((IProperty) knowledge).isObjectProperty();
    }

    public static boolean isThing(ISemantic o) {
        IKnowledge concept = o.getType();
        return concept.is(KLAB.c(NS.CORE_PHYSICAL_OBJECT));
    }

    public static boolean isQuality(ISemantic o) {
        IKnowledge concept = o.getType();
        return concept.is(KLAB.c(CORE_QUALITY));
    }

    public static boolean isCountable(ISemantic o) {
        IKnowledge concept = o.getType();
        return concept.is(KLAB.c(CORE_PHYSICAL_OBJECT)) || concept.is(KLAB.c(CORE_EVENT));
    }

    public static boolean isProcess(ISemantic o) {
        IKnowledge concept = o.getType();
        return concept.is(KLAB.c(CORE_PROCESS));
    }

    public static boolean isTrait(ISemantic o) {
        IKnowledge concept = o.getType();
        return /* concept.is(Env.c(TYPE)) || */ (concept.is(KLAB.c(CORE_TRAIT)) && !isObservable(concept));
    }

    public static boolean isRole(ISemantic o) {
        IKnowledge concept = o.getType();
        return concept.is(KLAB.c(ROLE_TRAIT)) && !isObservable(concept);
    }

    public static boolean isEvent(ISemantic o) {
        IKnowledge concept = o.getType();
        return concept.is(KLAB.c(CORE_EVENT));
    }

    public static boolean isDomain(ISemantic o) {
        IKnowledge concept = o.getType();
        return !isObservable(o) && !isTrait(o) && concept.is(KLAB.c(CORE_DOMAIN));
    }

    public static boolean isConfiguration(ISemantic o) {
        IKnowledge concept = o.getType();
        return concept.is(KLAB.c(CORE_PATTERN));
    }

    public static boolean isNothing(ISemantic o) {
        return nothings.contains(o.getType().toString());
    }

    /**
     * True if the passed knowledge has been explicitly declared in k.IM.
     * 
     * @param o
     * @return
     */
    public static boolean isDeclared(ISemantic o) {
        IKnowledge observable = o.getType();
        INamespace ns = KLAB.MMANAGER.getNamespace(observable.getConceptSpace());
        return ns != null && ns.getModelObject(observable.getLocalName()) != null;
    }

    public static String getDefinition(IKnowledge k) {
        if (derivedConceptDefs.containsKey(k.toString())) {
            return derivedConceptDefs.get(k.toString());
        }
        return k.toString();
    }

    /**
     * True if the passed knowledge is a direct observable, i.e., an object 
     * (see @{link isObject}) or a relationship.
     * 
     * @param o
     * @return
     */
    public static boolean isDirect(ISemantic o) {
        IKnowledge observable = o.getType();
        return isObject(observable) || isRelationship(observable);
    }

    /**
     * Like the other addTraits(), puts the new concept in the same ontology as 
     * main.
     * 
     * @param main
     * @param traits
     * @return
     * @throws ThinklabValidationException
     */
    public static IConcept addTraits(IKnowledge main, Collection<IConcept> traits)
            throws ThinklabValidationException {
        return addTraits(main, traits, main.getOntology());
    }

    /**
     * Return the concept corresponding to the version of main that inherits all
     * the passed traits.
     * 
     * NOTE: using this with traits that are subjective (e.g. im:High) can
     * result in ambiguities so it's not allowed. We only check that if the
     * subjective trait concept exists.
     * 
     * TODO: properly handle abstract status. Should be: if main is abstract and
     * no concrete identities are given, the result remains abstract. All traits
     * given should be concrete, but I'm not really sure of this.
     * 
     * @param main
     * @param traits
     * @return
     * @throws ThinklabValidationException
     */
    public static IConcept addTraits(IKnowledge main, Collection<IConcept> traits, IOntology ontology)
            throws ThinklabValidationException {

        boolean gotIdentity = false;
        IConcept ret = null;
        if (main instanceof IProperty) {
            main = getBackingConcept(main);
        }
        ArrayList<String> tids = new ArrayList<String>();
        ArrayList<IConcept> keep = new ArrayList<IConcept>();
        for (IConcept t : traits) {

            if (!gotIdentity && (t.is(KLAB.c(CORE_IDENTITY_TRAIT)) || t.is(KLAB.c(CORE_REALM_TRAIT)))) {
                gotIdentity = true;
            }

            if (KLAB.KM.getConcept(SUBJECTIVE_TRAIT) != null) {
                if (t.is(KLAB.c(SUBJECTIVE_TRAIT))) {
                    throw new ThinklabValidationException("an observable concept cannot inherit the subjective trait "
                            + t + " without ambiguity");
                }
            }
            if (!main.is(t)) {
                tids.add(t.getLocalName());
                keep.add(t);
            }
        }

        Collections.sort(tids);

        String cId = "";
        for (String s : tids) {
            cId += s;
        }
        cId += main.getLocalName();

        ret = ontology.getConcept(cId);

        if (ret == null && keep.size() > 0) {

            /*
             * sort the trait array alphabetically so that the definition is stable to permutations
             */
            Collections.sort(keep, new Comparator<IConcept>() {
                @Override
                public int compare(IConcept arg0, IConcept arg1) {
                    return arg0.toString().compareTo(arg1.toString());
                }
            });

            String def = getDefinition(main);
            List<IAxiom> axioms = new ArrayList<IAxiom>();
            axioms.add(Axiom.ClassAssertion(cId));
            axioms.add(Axiom.SubClass(main.getLocalName(), cId));

            /*
             * abstract concept remains abstract unless we anchor it to an identity or
             * a realm.
             */
            if (main.isAbstract() && !gotIdentity) {
                axioms.add(Axiom.AnnotationAssertion(cId, IS_ABSTRACT, "true"));
            }
            for (IConcept t : keep) {
                /**
                * UNALIAS the trait when it's used in case it's from an authority.
                * TODO put in when it's clear when exactly to do this.
                */
                // t = unalias(t);
                axioms.add(Axiom.SubClass(t.toString(), cId));
                def += "+" + t;
            }
            ontology.define(axioms);
            ret = ontology.getConcept(cId);
            derivedConceptDefs.put(ret.toString(), def);

            // System.out.println("ADDED TRAITS " + traits + " to " + main + " in " +
            // ontology.getConceptSpace()
            // + ": core concept of resulting "
            // + ret + " is " + getCoreType(ret));
        }

        return ret;
    }

    private static IConcept unalias(IConcept t) {
        IKnowledge k = t.getOntology().getAlias(t);
        return (IConcept) (k == null ? t : k);
    }

    /**
     * Given a trait class (which must be a base trait) return all the concrete
     * types this trait can assume, using superclasses appropriately, and
     * ordering them if the trait is an ordering.
     * 
     * @param trait
     * @return
     */
    public static Collection<IConcept> getTraitSpace(IConcept trait) {

        List<IConcept> ret = new ArrayList<IConcept>();

        /*
         * the trait space is the set of non-base children of the closest
         * superclass that has any.
         */
        IConcept tr = trait;
        while (tr != null && ret.isEmpty()) {

            Set<IConcept> ch = getNonBaseChildren(tr);
            if (ch.size() > 0) {
                ret.addAll(ch);
            }
            tr = getBaseParentTrait(tr);
        }

        if (trait.is(KLAB.c(NS.ORDERING))) {
            Collections.sort(ret, new Comparator<IConcept>() {

                @Override
                public int compare(IConcept arg0, IConcept arg1) {
                    Integer o0 = arg0.getMetadata().getInt(NS.ORDER_PROPERTY, 0);
                    Integer o1 = arg1.getMetadata().getInt(NS.ORDER_PROPERTY, 0);
                    return o0.compareTo(o1);
                }
            });
        }

        return ret;

    }

    public static IConcept getBaseParentTrait(IConcept tr) {

        for (IConcept c : tr.getAllParents()) {
            /*
             * there should only be one of these or none.
             */
            if (c.getMetadata().get(NS.BASE_DECLARATION) != null) {
                return c;
            }
        }
        return null;
    }

    private static Set<IConcept> getNonBaseChildren(IConcept tr) {

        Set<IConcept> ret = new HashSet<IConcept>();
        for (IConcept c : tr.getChildren()) {
            if (c.getMetadata().get(NS.BASE_DECLARATION) == null) {
                ret.add(c);
            }
        }
        return ret;
    }

    /**
     * For each trait expressed by the passed concept, return the base trait and
     * its incarnation in the concept.
     * 
     * @param concept
     * @return
     */
    public static Collection<Pair<IConcept, IConcept>> getTraits(IKnowledge knowledge) {
        ArrayList<Pair<IConcept, IConcept>> ret = new ArrayList<Pair<IConcept, IConcept>>();
        if (NS.isObservable(knowledge)) {
            getTraits(getBackingConcept(knowledge), ret);
        }
        return ret;
    }

    /**
     * Return the concrete trait of the base type passed incarnated by the passed knowledge,
     * or null if the trait is absent.
     * 
     * @param concept
     * @return
     */
    public static IConcept getTrait(IKnowledge knowledge, IConcept baseTrait) {

        IConcept ret = null;
        for (Pair<IConcept, IConcept> trait : getTraits(knowledge)) {
            if (trait.getFirst().equals(baseTrait)) {
                return trait.getSecond();
            }
        }

        return ret;
    }

    /**
     * Return the passed knowledge after removing any traits of the passed base type, or 
     * the original knowledge if the trait was absent.
     * 
     * @param concept
     * @return
     * @throws ThinklabValidationException 
     */
    public static IKnowledge removeTrait(IKnowledge knowledge, IConcept baseTrait)
            throws ThinklabValidationException {

        Pair<IConcept, Collection<IConcept>> trs = separateAttributes(knowledge);
        ArrayList<IConcept> traits = new ArrayList<>();

        boolean found = false;
        for (IConcept tr : trs.getSecond()) {
            if (tr.is(baseTrait)) {
                found = true;
            } else {
                traits.add(tr);
            }
        }

        return found ? addTraits(trs.getFirst(), traits) : knowledge;
    }

    public static boolean hasTraitOfType(IKnowledge knowledge, IConcept baseTrait) {

        for (Pair<IConcept, IConcept> trait : getTraits(knowledge)) {
            if (trait.getFirst().equals(baseTrait)) {
                return true;
            }
        }
        return false;
    }

    private static IConcept getBackingConcept(IKnowledge knowledge) {
        if (knowledge instanceof IConcept) {
            return (IConcept) knowledge;
        }
        // TODO return the actual backing concept
        return KLAB.c(CORE_OBJECT);
    }

    /**
     * Return the observable that represent the passed one without any of the
     * traits it may inherit.
     * 
     * @param observable
     * @return
     */
    public static IKnowledge getBaseObservable(IKnowledge observable) {

        if (observable == null)
            return null;

        if (getTraits(observable).size() == 0)
            return observable;
        return getBaseObservable(getNonTraitParent(observable));
    }

    /**
     * Return the most specific core observable that subsumes this object (e.g. event, subject, length, trait etc) 
     * or null if it's not an observable or trait. If the concept is a trait and not an observable, the
     * core trait type will be returned. Otherwise the core observable is returned (even if it has traits). If
     * this is not a core particular this will return null.
     * 
     * @param observable
     * @return
     */
    public static IKnowledge getCoreType(ISemantic observable) {

        String toCheck = isObservable(observable) ? CORE_OBSERVABLE : CORE_PARTICULAR;

        IKnowledge k = observable.getType();
        if (k.is(KLAB.p(STRUCTURING_PROPERTY))) {
            return k;
        }

        if (k.is(KLAB.c(toCheck))) {
            for (IConcept c : getLeastGeneral(((IConcept) k).getAllParents())) {
                if (c.is(KLAB.c(toCheck))) {
                    return getLeastGeneralFrom(c, CORE_ONTOLOGY, KLAB.c(toCheck));
                }
            }
        }

        return null;
    }

    /**
     * Return the domain, if any, that a given object belongs to. For now, that is simply the (unique)
     * domain assigned to the closest namespace. Later we may have different domains in a namespace.
     * 
     * @param observable
     * @return
     */
    public static IConcept getDomain(ISemantic observable) {

        IKnowledge k = observable.getType();
        INamespace ns = KLAB.MMANAGER.getNamespace(k.getConceptSpace());
        if (ns != null && ns.getDomain() != null) {
            return ns.getDomain();
        }
        if (k instanceof IConcept) {
            for (IConcept c : ((IConcept) k).getParents()) {
                IConcept d = getDomain(c);
                if (d != null) {
                    return d;
                }
            }
        } else if (k instanceof IProperty) {
            for (IProperty c : ((IProperty) k).getParents()) {
                IConcept d = getDomain(c);
                if (d != null) {
                    return d;
                }
            }
        }
        return null;
    }

    /**
     * Get the least general concept in the passed concept's hierarchy that
     * belongs to the named concept space. Will return the first parent found in
     * it.
     * 
     * @param c
     * @param ontology
     * @return
     */
    public static IConcept getLeastGeneralFrom(IConcept c, String ontology) {

        if (c.getConceptSpace().equals(ontology)) {
            return c;
        }

        for (IConcept p : c.getParents()) {
            IConcept ret = getLeastGeneralFrom(p, ontology);
            if (ret != null) {
                return ret;
            }
        }

        return null;
    }

    /**
     * Get the least general concept in the passed concept's hierarchy that
     * belongs to the named concept space and is the passed base concept. 
     * Will return the first non-derived parent found in it.
     * 
     * @param c
     * @param ontology
     * @return
     */
    public static IConcept getLeastGeneralFrom(IConcept c, String ontology, IConcept root) {

        if (c.equals(root)) {
            return c;
        }

        if (c.getConceptSpace().equals(ontology) && c.is(root) && !isDerived(c)) {
            return c;
        }

        for (IConcept p : c.getParents()) {
            IConcept ret = getLeastGeneralFrom(p, ontology, root);
            if (ret != null) {
                return ret;
            }
        }

        return null;
    }

    /**
     * Return the observable that represent the passed one without any of the
     * ATTRIBUTES it may inherit but with all the realms and identities.
     * 
     * @param observable
     * @return
     */
    public static IKnowledge getBaseIdentity(IKnowledge observable) {

        if (observable == null)
            return null;
        if (getTraits(observable).size() == 0)
            return observable;
        return getNonAttributeParent(observable);
    }

    /**
     * Analyze an observable concept and return the main observable with all the original identities and realms 
     * but no attributes; separately, return the list of the attributes that were removed.
     * 
     * @param observable
     * @return
     * @throws ThinklabValidationException
     */
    public static Pair<IConcept, Collection<IConcept>> separateAttributes(IKnowledge observable)
            throws ThinklabValidationException {

        IKnowledge obs = getBaseIdentity(observable);
        ArrayList<IConcept> tret = new ArrayList<>();
        ArrayList<IConcept> keep = new ArrayList<>();

        for (Pair<IConcept, IConcept> zt : getTraits(observable)) {
            if (zt.getFirst().is(KLAB.c(CORE_IDENTITY_TRAIT)) || zt.getFirst().is(KLAB.c(CORE_REALM_TRAIT))) {
                keep.add(zt.getSecond());
            } else {
                tret.add(zt.getSecond());
            }
        }

        return new Pair<IConcept, Collection<IConcept>>(addTraits(obs == null ? observable
                : obs, keep), tret);

    }

    public static IKnowledge getNonTraitParent(IKnowledge observable) {

        if (observable instanceof IProperty) {
            for (IProperty p : ((IProperty) observable).getParents()) {
                if (isObservable(p) && !isTrait(p, false)) {
                    return p;
                }
            }
        } else {
            for (IConcept p : ((IConcept) observable).getParents()) {
                if (isObservable(p) && !isTrait(p, false)) {
                    return p;
                }
            }
        }
        return null;
    }

    public static IKnowledge getNonAttributeParent(IKnowledge observable) {

        if (!observable.is(KLAB.c(ATTRIBUTE_TRAIT)))
            return observable;
        if (observable instanceof IProperty) {
            for (IProperty p : ((IProperty) observable).getParents()) {
                if (isObservable(p) && !p.is(KLAB.c(ATTRIBUTE_TRAIT))) {
                    return p;
                }
            }
        } else {
            for (IConcept p : ((IConcept) observable).getParents()) {
                if (isObservable(p) && !p.is(KLAB.c(ATTRIBUTE_TRAIT))) {
                    return p;
                }
            }
        }
        return null;
    }

    private static void getTraits(IConcept concept, ArrayList<Pair<IConcept, IConcept>> ret) {
        getTraits(concept, ret, new HashSet<IConcept>());
    }

    private static void getTraits(IConcept concept, ArrayList<Pair<IConcept, IConcept>> ret, Set<IConcept> seen) {
        if (concept == null)
            return;

        if (seen.contains(concept)) {
            return;
        }

        seen.add(concept);

        for (IConcept t : concept.getParents()) {

            if (isTrait(t, false)
                    && t.getMetadata().get(NS.BASE_DECLARATION) == null) {

                boolean skip = false;
                IConcept base = getBaseParentTrait(t);

                if (base == null) {
                    throw new ThinklabRuntimeException("logical error: no base trait for non-observable "
                            + t);
                }

                for (Pair<IConcept, IConcept> p : ret) {
                    if (p.getFirst().equals(base)) {
                        skip = true;
                        break;
                    }
                }
                if (!skip) {
                    ret.add(new Pair<IConcept, IConcept>(base, t));
                }

            } else if (!isTrait(t, false)) {
                getTraits(t, ret, seen);
            }
        }

    }

    /**
     * True if concept is a trait and not an observable. If isBaseTrait is true,
     * also check that the concept is a base trait class - an abstract one that
     * can produce concrete concepts to classify things with.
     * 
     * @param cc
     * @param isBaseTrait
     * @return
     */
    public static boolean isTrait(IKnowledge cc, boolean isBaseTrait) {
        boolean ret = getBackingConcept(cc).is(KLAB.c(NS.CORE_TRAIT)) && !isObservable(cc);
        if (ret && isBaseTrait) {
            ret = cc.getMetadata().get(NS.BASE_DECLARATION) != null;
        }
        return ret;
    }

    /**
     * Return all concepts that the passed concept is and are observables.
     * 
     * @param subject
     * @return
     */
    public static Set<IKnowledge> getObservableClosure(IKnowledge subject) {

        Set<IKnowledge> ret = new HashSet<>();
        ret.add(subject);

        if (subject instanceof IConcept) {
            for (IConcept c : ((IConcept) subject).getAllParents()) {
                if (isObservable(c)) {
                    ret.add(c);
                }
            }
        } else if (subject instanceof IProperty) {
            for (IProperty c : ((IProperty) subject).getAllParents()) {
                if (isObservable(c)) {
                    ret.add(c);
                }
            }
        }
        return ret;
    }

    /**
     * If a concept is a classification (quality space), just return it
     * unmodified; otherwise, create the quality of being that concept and
     * return that.
     * 
     * @param concept
     * @return
     */
    public static IConcept makeClassification(IConcept concept) {

        if (concept.is(KLAB.c(NS.TYPE)) || isTrait(concept))
            return concept;

        IConcept ret = null;

        if (isObservable(concept)) {

            /*
             * it's ANOTHER kind of observable, so make a class for it.
             * 
             * TODO revise - use observability when appropriate, according to presence
             * of classifiers.
             * 
             */
            String cName = concept.getLocalName() + "Class";
            ret = concept.getOntology().getConcept(cName);
            if (ret == null) {
                ArrayList<IAxiom> ax = new ArrayList<IAxiom>();
                ax.add(Axiom.ClassAssertion(cName));
                ax.add(Axiom.SubClass(NS.TYPE, cName));
                ax.add(Axiom.SubClass(NS.CORE_ASSERTED_QUALITY, cName));
                concept.getOntology().define(ax);
                ret = concept.getOntology().getConcept(cName);
                derivedConceptDefs
                        .put(ret.toString(), concept + "+" + NS.TYPE + "+" + NS.CORE_ASSERTED_QUALITY);
            }
        } else {
            /*
             * it's nothing yet, so just keep it as is but make it a class.
             */
            String cName = concept.getLocalName();
            ret = concept;
            ArrayList<IAxiom> ax = new ArrayList<IAxiom>();
            ax.add(Axiom.SubClass(NS.TYPE, cName));
            concept.getOntology().define(ax);
            ret = concept.getOntology().getConcept(cName);
        }

        return ret;
    }

    public static IConcept makeCount(IConcept concept) {

        /*
         * first, ensure we're counting things and not amounts.
         */
        if (!NS.isCountable(concept)) {
            return null;
        }

        String cName = concept.getLocalName() + "Count";

        /*
         * make a ConceptCount if not there, and ensure it's a continuously
         * quantifiable quality. Must be in same ontology as the original
         * concept.
         */
        IConcept ret = concept.getOntology().getConcept(cName);

        if (ret == null) {

            ArrayList<IAxiom> ax = new ArrayList<IAxiom>();
            ax.add(Axiom.ClassAssertion(cName));
            ax.add(Axiom.SubClass(NS.CORE_NUMERIC_QUANTITY, cName));
            concept.getOntology().define(ax);
            ret = concept.getOntology().getConcept(cName);
            derivedConceptDefs.put(ret.toString(), NS.CORE_NUMERIC_QUANTITY);
        }

        return ret;

    }

    public static IConcept makePresence(IKnowledge concept) {

        if (concept.is(KLAB.c(NS.CORE_PRESENCE))) {
            return (IConcept) concept;
        }

        if (!NS.isObject(concept)) {
            throw new ThinklabRuntimeException("cannot observe the presence of a quality");
        }

        String cName = concept.getLocalName() + "Presence";

        /*
         * make a ConceptCount if not there, and ensure it's a continuously
         * quantifiable quality. Must be in same ontology as the original
         * concept.
         */
        IConcept ret = concept.getOntology().getConcept(cName);

        if (ret == null) {
            ArrayList<IAxiom> ax = new ArrayList<IAxiom>();
            ax.add(Axiom.ClassAssertion(cName));
            ax.add(Axiom.SubClass(NS.CORE_PRESENCE, cName));
            concept.getOntology().define(ax);
            ret = concept.getOntology().getConcept(cName);
            String def = NS.CORE_PRESENCE;
            if (derivedConceptDefs.containsKey(concept.toString())) {
                def = derivedConceptDefs.get(concept.toString()) + "+" + def;
            }
            derivedConceptDefs.put(ret.toString(), def);
        }

        return ret;
    }

    /*
     * TODO finish; may be an object this time.
     */
    public static IConcept makeDistance(IConcept concept) {

        if (concept.is(KLAB.c(NS.CORE_DISTANCE))) {
            return concept;
        }

        if (!NS.isObject(concept)) {
            throw new ThinklabRuntimeException("cannot compute the distance to a quality");
        }

        String cName = "DistanceTo" + concept.getLocalName();

        /*
         * make a ConceptCount if not there, and ensure it's a continuously
         * quantifiable quality. Must be in same ontology as the original
         * concept.
         */
        IConcept ret = concept.getOntology().getConcept(cName);

        if (ret == null) {
            ArrayList<IAxiom> ax = new ArrayList<IAxiom>();
            ax.add(Axiom.ClassAssertion(cName));
            ax.add(Axiom.SubClass(NS.CORE_DISTANCE, cName));
            concept.getOntology().define(ax);
            ret = concept.getOntology().getConcept(cName);
            String def = NS.CORE_DISTANCE;
            if (derivedConceptDefs.containsKey(concept.toString())) {
                def = derivedConceptDefs.get(concept.toString()) + "+" + def;
            }
            derivedConceptDefs.put(ret.toString(), def);
        }

        return ret;
    }

    public static IConcept makeDistanceTo(INamespace namespace, String whatFrom) {

        String cName = "DistanceTo" + whatFrom;

        /*
         * make a ConceptCount if not there, and ensure it's a continuously
         * quantifiable quality. Must be in same ontology as the original
         * concept.
         */
        IConcept ret = namespace.getOntology().getConcept(cName);

        if (ret == null) {
            ArrayList<IAxiom> ax = new ArrayList<IAxiom>();
            ax.add(Axiom.ClassAssertion(cName));
            ax.add(Axiom.SubClass(NS.CORE_DISTANCE, cName));
            namespace.getOntology().define(ax);
            ret = namespace.getOntology().getConcept(cName);
            String def = NS.CORE_DISTANCE;
            if (derivedConceptDefs.containsKey(ret.toString())) {
                def = derivedConceptDefs.get(ret.toString()) + "+" + def;
            }
            derivedConceptDefs.put(ret.toString(), def);
        }

        return ret;
    }

    public static IConcept makeValue(IKnowledge concept, IKnowledge comparison) {

        if (concept.is(KLAB.c(NS.CORE_VALUE))) {
            return (IConcept) concept;
        }

        String cName = "ValueOf" + concept.getLocalName()
                + (comparison == null ? "" : ("Over" + comparison.getLocalName()));

        /*
         * make a ConceptCount if not there, and ensure it's a continuously
         * quantifiable quality. Must be in same ontology as the original
         * concept.
         */
        IConcept ret = concept.getOntology().getConcept(cName);

        if (ret == null) {
            ArrayList<IAxiom> ax = new ArrayList<IAxiom>();
            ax.add(Axiom.ClassAssertion(cName));
            ax.add(Axiom.SubClass(NS.CORE_VALUE, cName));
            concept.getOntology().define(ax);
            ret = concept.getOntology().getConcept(cName);
            String def = NS.CORE_VALUE;
            if (derivedConceptDefs.containsKey(concept.toString())) {
                def = derivedConceptDefs.get(concept.toString()) + "+" + def;
            }
            derivedConceptDefs.put(ret.toString(), def);
        }

        return ret;
    }
    //
    // public static void validateObservable(IModelResolver resolver, IKnowledgeObject observable,
    // ObservableRole role, IObserver observer, int line, IKnowledgeObject... context) {
    //
    // // String ctx = "";
    // // if (context != null) {
    // // for (IConceptDefinition c : context)
    // // ctx += (ctx.isEmpty() ? "" : ", ") + c;
    // // }
    // //
    // // System.out.println("VALIDATING " + observable + " as " + type +
    // // " in role " + role
    // // + (ctx.isEmpty() ? "" : (" for " + ctx)));
    //
    // }

    public static IConcept makeRatio(IConcept concept, IConcept comparison) {

        /*
         * accept only two qualities of the same physical nature (TODO)
         */
        if (!(isQuality(concept) || isTrait(concept)) || !isQuality(comparison)) {
            return null;
        }

        String cName = concept.getLocalName() + "To"
                + comparison.getLocalName() + "Ratio";
        IConcept ret = concept.getOntology().getConcept(cName);

        if (ret == null) {

            ArrayList<IAxiom> ax = new ArrayList<IAxiom>();
            ax.add(Axiom.ClassAssertion(cName));
            ax.add(Axiom.SubClass(NS.CORE_RATIO, cName));
            concept.getOntology().define(ax);
            ret = concept.getOntology().getConcept(cName);
            derivedConceptDefs.put(ret.toString(), NS.CORE_RATIO);
        }

        return ret;
    }

    public static IConcept makeProportion(IConcept concept, IConcept comparison) {

        if (!(isQuality(concept) || isTrait(concept)) && !isQuality(comparison)) {
            return null;
        }

        String cName = concept.getLocalName() + "ProportionTo"
                + comparison.getLocalName();
        IConcept ret = concept.getOntology().getConcept(cName);

        if (ret == null) {

            ArrayList<IAxiom> ax = new ArrayList<IAxiom>();
            ax.add(Axiom.ClassAssertion(cName));
            ax.add(Axiom.SubClass(NS.CORE_PROPORTION, cName));
            concept.getOntology().define(ax);
            ret = concept.getOntology().getConcept(cName);
            derivedConceptDefs.put(ret.toString(), NS.CORE_PROPORTION);
        }

        return ret;
    }

    public static IConcept makeProbability(IConcept concept) {

        if (!isProcess(concept) && !isEvent(concept)) {
            return null;
        }

        String cName = concept.getLocalName() + "Probability";
        IConcept ret = concept.getOntology().getConcept(cName);

        if (ret == null) {

            ArrayList<IAxiom> ax = new ArrayList<IAxiom>();
            ax.add(Axiom.ClassAssertion(cName));
            ax.add(Axiom.SubClass(NS.CORE_PROBABILITY, cName));
            concept.getOntology().define(ax);
            ret = concept.getOntology().getConcept(cName);
            derivedConceptDefs.put(ret.toString(), NS.CORE_PROBABILITY);
        }

        return ret;
    }

    public static IConcept makeUncertainty(IConcept concept) {

        if (concept.is(KLAB.c(NS.CORE_UNCERTAINTY))) {
            return null;
        }

        String cName = concept.getLocalName() + "Uncertainty";
        IConcept ret = concept.getOntology().getConcept(cName);

        if (ret == null) {

            ArrayList<IAxiom> ax = new ArrayList<IAxiom>();
            ax.add(Axiom.ClassAssertion(cName));
            ax.add(Axiom.SubClass(NS.CORE_UNCERTAINTY, cName));
            concept.getOntology().define(ax);
            ret = concept.getOntology().getConcept(cName);
            derivedConceptDefs.put(ret.toString(), NS.CORE_UNCERTAINTY);
        }

        return ret;
    }

    /**
     * Collect all concrete disjoint children of a class, stopping the recursive search
     * at the first concrete child. Used when observing dependencies with the "every"
     * qualifier.
     * 
     * @param concept
     * @return
     */
    public static Collection<IKnowledge> getConcreteDisjointChildren(IKnowledge concept) {

        HashSet<IKnowledge> ret = new HashSet<IKnowledge>();
        collectConcreteChildren(concept, ret);
        return ensureDisjoint(ret);

    }

    /**
     * Remove all concepts in the passed collection that are not disjoint with each 
     * other. TODO unimplemented - needs functionality in IConcept, or better, needs
     * full reasoner integration.
     * 
     * @param ret
     * @return
     */
    public static Collection<IKnowledge> ensureDisjoint(Collection<IKnowledge> ret) {

        ArrayList<IKnowledge> rr = new ArrayList<>();
        rr.addAll(ret);
        HashSet<IConcept> toRemove = new HashSet<IConcept>();
        for (int i = 0; i < rr.size(); i++) {
            for (int j = 0; j < rr.size(); j++) {
                // if (i != j && !rr.get(i).isDisjointWith(rr.get(j))) {
                // toRemove.add(rr.get(i));
                // toRemove.add(rr.get(j));
                // }
            }
        }

        ret.removeAll(toRemove);

        return ret;
    }

    private static void collectConcreteChildren(IKnowledge concept, HashSet<IKnowledge> ret) {

        if (!concept.isAbstract()) {
            ret.add(concept);
        } else {
            if (concept instanceof IConcept) {
                for (IConcept c : ((IConcept) concept).getChildren()) {
                    collectConcreteChildren(c, ret);
                }
            } else {
                for (IProperty c : ((IProperty) concept).getChildren()) {
                    collectConcreteChildren(c, ret);
                }
            }
        }

    }

    public static boolean isIdentity(IConcept c) {
        return c.is(KLAB.c(NS.CORE_IDENTITY_TRAIT));
    }

    public static boolean isSubjective(IObservable obs) {
        // TODO Auto-generated method stub
        return obs.getType().is(KLAB.c(NS.SUBJECTIVE_TRAIT))
                || (obs.getTraitType() != null && obs.getTraitType().is(KLAB.c(NS.SUBJECTIVE_TRAIT)));
    }

    public static boolean isSubjective(IConcept concept) {
        return concept.is(KLAB.c(NS.SUBJECTIVE_TRAIT));
    }

    /**
     * TODO use reasoner for better resolution of physical properties etc.
     * 
     * @param observable
     * @return
     */
    public static IConcept getObservationTypeFor(IConcept observable) {

        if (observable.is(KLAB.c(CORE_PHYSICAL_PROPERTY))) {
            return KLAB.c(MEASUREMENT);
        } else if (observable.is(KLAB.c(CORE_PREFERENCE_VALUE))) {
            return KLAB.c(RANKING);
        } else if (isClass(observable) || isTrait(observable)) {
            return KLAB.c(CLASSIFICATION);
        } else if (isObject(observable)) {
            return KLAB.c(DIRECT_OBSERVATION);
        }

        return KLAB.c(INDIRECT_OBSERVATION);
    }

    /**
     * Return all the observables that have the same identity of the passed one and 
     * zero or more of its attributes.
     * @param type
     * @return
     * @throws ThinklabValidationException
     */
    public static Set<IKnowledge> getCompatibleObservables(IKnowledge type, Collection<IConcept> attributes)
            throws ThinklabValidationException {

        Set<IKnowledge> ret = new HashSet<IKnowledge>();
        ret.add(type);

        if (attributes.size() == 0)
            return ret;

        for (Set<IConcept> set : Permutations.powerSet(attributes)) {
            if (set.size() > 0) {
                ret.add(addTraits(type, set));
            }
        }

        return ret;
    }

    /**
     * True if the observable has the specific trait passed (not a base trait). Should be
     * used instead of is() - as the trait inheritance will use restrictions when reasoning
     * is more proper.
     * 
     * @param observable
     * @param trait
     * @return
     */
    public static boolean hasTrait(ISemantic observable, IConcept trait) {
        return getBackingConcept(observable.getType()).is(trait);
    }

    /**
     * Find or create the property that would link an observable to a concept.
     * 
     * FIXME of course must use the semantics and a reasoner more than it does, and return null or throw
     * exceptions if anything inconsistent is attempted. When that is done, remove checks in the actual
     * observing functions that call this one.
     * 
     * FIXME generalize and move to the common package in the NS class.
     * 
     * @param observable
     * @param receiver
     * @param namespace
     * @return
     * @throws ThinklabException
     */
    public static IProperty getPropertyFor(Object observable, IObservable receiver, INamespace namespace)
            throws ThinklabException {

        IProperty property = null;

        IConcept c = null;
        if (observable instanceof IConcept)
            c = (IConcept) observable;
        else if (observable instanceof IObservable)
            c = ((IObservable) observable).getTypeAsConcept();
        else if (observable instanceof IModel)
            c = ((IModel) observable).getObservable().getTypeAsConcept();
        else if (observable instanceof ISubject)
            c = ((ISubject) observable).getObservable().getTypeAsConcept();
        else if (observable instanceof IProcess)
            c = ((IProcess) observable).getObservable().getTypeAsConcept();
        // else if (observable instanceof ISemanticObject<?>)
        // c = (IConcept) ((ISemanticObject<?>) observable).getDirectType();
        else if (observable instanceof IState)
            c = ((IState) observable).getObservable().getTypeAsConcept();

        if (c != null && receiver.getType() != null) {

            /*
             * FIXME: if a model is taking subjects (say a railway) but discretizes it to an observation
             * it will MAKE the observation a quality. The attribute is for the OBSERVATION, not for the
             * observable. For now adjust things looking at the model first, but we need proper treatment
             * of the semantics.
             */
            boolean isQuality = isQuality(c);
            boolean isQualitySpace = isClass(c);

            if (observable instanceof IModel) {
                isQuality = ((IModel) observable).getObserver() != null;
                if (isQuality) {
                    isQualitySpace = !isNumericTransformation(((IModel) observable).getObserver());
                }
            } else if (observable instanceof IState) {
                isQuality = true;
                /*
                 * FIXME should also check isQualitySpace, but if we get here we have already created or
                 * found the property before, so it's already there and the check after this will succeed
                 * without further intelligence.
                 */
            }

            /*
             * no range to speak of if we have a dataproperty, so just look for hasxxx and return it
             * if there.
             */
            if (isQuality && namespace.getOntology().getProperty("has" + c.getLocalName()) != null) {
                return namespace.getOntology().getProperty("has" + c.getLocalName());
            }

            /*
             * TODO should put them all away and raise an exception if
             * there is ambiguity.
             */
            for (IProperty p : receiver.getTypeAsConcept().getAllProperties()) {

                if (receiver.getTypeAsConcept().getPropertyRange(p).contains(c)) {
                    property = p;
                    break;
                }
            }

            if (property /* still */ == null) {

                /*
                 * TODO/FIXME: using only object properties.
                 */
                boolean isObject = true; // !(isQuality || isQualitySpace);

                ArrayList<IAxiom> axioms = new ArrayList<IAxiom>();

                axioms.add(isObject ? Axiom.ObjectPropertyAssertion("has" + c.getLocalName()) : Axiom
                        .DataPropertyAssertion("has" + c.getLocalName()));
                axioms.add(isObject ? Axiom.ObjectPropertyRange("has" + c.getLocalName(), c.toString())
                        : Axiom.DataPropertyRange("has" + c.getLocalName(), c.toString()));
                axioms.add(isObject
                        ? Axiom.ObjectPropertyDomain("has" + c.getLocalName(), receiver.toString())
                        : Axiom.DataPropertyDomain("has" + c.getLocalName(), receiver.toString()));

                namespace.getOntology().define(axioms);

                property = namespace.getOntology().getProperty("has" + c.getLocalName());

                // TODO turn to debug or remove
                // Env.logger
                // .info((property.isObjectProperty() ? "object" : "data") + " property " + property
                // + " created for " + c);

            } else {

                // // TODO turn to debug or remove
                // Env.logger
                // .info((property.isObjectProperty() ? "object" : "data") + " property " + property
                // + " found for " + c);

            }
        } else {

            /*
             * huh?
             */
            throw new ThinklabInternalErrorException("internal error: adding dependency on something that is not semantic: "
                    + observable);
        }

        return property;
    }

    /*
     * return true if this observed doesn't change the semantics of the ultimate observer.
     */
    public static boolean isNumericTransformation(IObserver observer) {

        if (observer instanceof IRankingObserver || observer instanceof IMeasuringObserver
                || observer instanceof IValuingObserver) {
            return true;
        }

        if (observer instanceof IConditionalObserver) {
            boolean ret = true;
            for (Pair<IModel, IExpression> o : ((IConditionalObserver) observer).getModels()) {
                if (!isNumericTransformation(o.getFirst().getObserver())) {
                    ret = false;
                    break;
                }
            }
            return ret;
        }

        return false;
    }

    public static void removeRolesFor(INamespace namespace) {

        List<Pair<IConcept, RoleDescriptor>> toRemove = new ArrayList<>();
        for (Object o : roles.values()) {
            RoleDescriptor rd = (RoleDescriptor) o;
            if (rd.namespaceId.equals(namespace.getId())) {
                toRemove.add(new Pair<>(rd.role, rd));
            }
        }

        for (Pair<IConcept, RoleDescriptor> r : toRemove) {
            roles.remove(r.getFirst(), r.getSecond());
        }
    }

    public static boolean synchronize() {

        if (LENGTH == null) {
            try {
                LENGTH = checkClass(KLAB.MMANAGER.getExportedKnowledge("IM.LENGTH"), IConcept.class);
                AREA = checkClass(KLAB.MMANAGER.getExportedKnowledge("IM.AREA"), IConcept.class);
                DISTANCE = checkClass(KLAB.MMANAGER.getExportedKnowledge("IM.DISTANCE"), IConcept.class);
                VOLUME = checkClass(KLAB.MMANAGER.getExportedKnowledge("IM.VOLUME"), IConcept.class);
                MINIMUM_TRAIT = checkClass(KLAB.MMANAGER
                        .getExportedKnowledge("IM.MINIMUM_TRAIT"), IConcept.class);
                MAXIMUM_TRAIT = checkClass(KLAB.MMANAGER
                        .getExportedKnowledge("IM.MAXIMUM_TRAIT"), IConcept.class);
                AVERAGE_TRAIT = checkClass(KLAB.MMANAGER
                        .getExportedKnowledge("IM.AVERAGE_TRAIT"), IConcept.class);
                DATA_REDUCTION_TRAIT = checkClass(KLAB.MMANAGER
                        .getExportedKnowledge("IM.DATA_REDUCTION_TRAIT"), IConcept.class);
                FLOW = checkClass(KLAB.MMANAGER.getExportedKnowledge("IM.FLOW"), IProperty.class);
                HOURLY_TRAIT = checkClass(KLAB.MMANAGER
                        .getExportedKnowledge(HOURLY_TRAIT_CONCEPT), IConcept.class);
                DAILY_TRAIT = checkClass(KLAB.MMANAGER
                        .getExportedKnowledge(DAILY_TRAIT_CONCEPT), IConcept.class);
                WEEKLY_TRAIT = checkClass(KLAB.MMANAGER
                        .getExportedKnowledge(WEEKLY_TRAIT_CONCEPT), IConcept.class);
                MONTHLY_TRAIT = checkClass(KLAB.MMANAGER
                        .getExportedKnowledge(MONTHLY_TRAIT_CONCEPT), IConcept.class);
                YEARLY_TRAIT = checkClass(KLAB.MMANAGER
                        .getExportedKnowledge(YEARLY_TRAIT_CONCEPT), IConcept.class);
                VISIBLE_TRAIT = checkClass(KLAB.MMANAGER
                        .getExportedKnowledge(VISIBLE_TRAIT_CONCEPT), IConcept.class);
            } catch (ThinklabException e) {
                return false;
            }
        }
        return true;
    }

    /**
     * Check that passed object is of passed class (and not null); throw the necessary exceptions if
     * not so, and return the casted object if so.
     * 
     * @param object
     * @param cls
     * @return
     * @throws ThinklabException
     */
    @SuppressWarnings("unchecked")
    public static <T> T checkClass(Object object, Class<? extends T> cls) throws ThinklabException {
        if (object == null) {
            throw new ThinklabValidationException("required content of type " + cls.getCanonicalName()
                    + " is null");
        }
        if (!cls.isInstance(object)) {
            throw new ThinklabValidationException("wrong type for " + object + ": " + cls.getCanonicalName()
                    + " required, " + object.getClass().getCanonicalName() + " passed");
        }
        return (T) object;
    }

    public static IKnowledge reconstructDerivedConcept(String baseid, String supers) {

        IConcept base = KLAB.KM.getConcept(baseid);
        if (base != null) {
            return base;
        }

        String[] ts = supers.split("\\+");
        List<IConcept> sup = new ArrayList<>();
        for (int i = 0; i < ts.length; i++) {
            IConcept trait = KLAB.KM.getConcept(ts[i]);
            if (trait == null) {
                return null;
            }
            sup.add(trait);
        }

        SemanticType st = new SemanticType(baseid);

        String cName = st.getLocalName();
        INamespace namespace = KLAB.MMANAGER.getNamespace(st.getConceptSpace());

        if (namespace == null) {
            throw new ThinklabRuntimeException("cannot create a derived concept in a non-existent namespace: "
                    + st.getConceptSpace());
        }

        ArrayList<IAxiom> ax = new ArrayList<IAxiom>();
        ax.add(Axiom.ClassAssertion(cName));
        for (IConcept c : sup) {
            ax.add(Axiom.SubClass(c.toString(), cName));
        }
        namespace.getOntology().define(ax);
        IConcept ret = namespace.getOntology().getConcept(cName);
        derivedConceptDefs.put(ret.toString(), supers);
        return ret;
    }

    public static void dumpDerived(PrintStream out) {
        for (String k : derivedConceptDefs.keySet()) {
            out.println(k + " = " + derivedConceptDefs.get(k));
        }
    }

    public static void dumpOntologies(File outDir) throws ThinklabException {

        for (INamespace ns : KLAB.MMANAGER.getNamespaces()) {
            IOntology o = ns.getOntology();
            if (o != null) {
                o.write(new File(outDir + File.separator + o.getConceptSpace() + ".owl"));
            }
        }

    }

    /**
     * Arrange a set of concepts into the collection of the most specific members of each
     * concept hierarchy therein.
     * 
     * @param cc
     * @return
     */
    public static Collection<IConcept> getLeastGeneral(Collection<IConcept> cc) {

        Set<IConcept> ret = new HashSet<>();
        for (IConcept c : cc) {
            List<IConcept> ccs = new ArrayList<>(ret);
            boolean set = false;
            for (IConcept kn : ccs) {
                if (c.is(kn)) {
                    ret.remove(kn);
                    ret.add(c);
                    set = true;
                } else if (kn.is(c)) {
                    set = true;
                }
            }
            if (!set) {
                ret.add(c);
            }
        }
        return ret;
    }

    /**
     * Arrange a set of concepts into the collection of the most specific members of each
     * concept hierarchy therein. Return one concept or null.
     * 
     * @param cc
     * @return
     */
    public static IConcept getLeastGeneralConcept(Collection<IConcept> cc) {
        Collection<IConcept> z = getLeastGeneral(cc);
        return z.size() > 0 ? z.iterator().next() : null;
    }

    public static IConcept getLeastGeneralCommonConcept(IConcept concept1, IConcept c) {
        return concept1.getLeastGeneralCommonConcept(c);
    }

    public static IConcept getLeastGeneralCommonConcept(Collection<IConcept> cc) {

        IConcept ret = null;
        Iterator<IConcept> ii = cc.iterator();

        if (ii.hasNext()) {

            ret = ii.next();

            if (ret != null)
                while (ii.hasNext()) {
                    ret = ret.getLeastGeneralCommonConcept(ii.next());
                    if (ret == null)
                        break;
                }
        }

        return ret;
    }

    public static Collection<RoleDescriptor> getRoles() {
        return roles.values();
    }

    /**
     * Check if observable has any subclass of the passed role in the context of
     * observable main. Used within model implementations to classify any pre-observed
     * objects and states.
     * 
     * TODO implement differentiation between process within subject or just subject or just process.
     * 
     * @param role
     * @param observable
     */
    public static /* IProperty */boolean hasRole(ISemantic observable, ISemantic role, IObservable subjectObservable, IObservable processObservable, IResolutionContext context) {

        Collection<?> rls = (Collection<?>) roles.get(role);

        if (rls == null) {
            return false;
        }

        for (Object o : rls) {
            RoleDescriptor rd = (RoleDescriptor) o;

            /*
             * TODO: filter by target; if role is scenario-driven and scenario not in context, skip;
             * everything else is good for checking.
             * 
             * Maybe we should only allow roles in scenarios for user project, don't know.
             */
            Collection<String> scenarios = context.getScenarios();
            if (rd.scenario != null) {
                if (!(scenarios != null && !scenarios.isEmpty() && scenarios.contains(rd.scenario))) {
                    continue;
                }
            }

            boolean subjOK = true;
            if (rd.targetSubject != null && subjectObservable != null) {
                subjOK = subjectObservable.is(rd.targetSubject);
            }

            if (subjOK && rd.target.is(observable.getType()) && rd.within.is(processObservable.getType())) {
                return true; // rd.property;
            }
        }

        return false;
    }

    /**
     * Return all independent observables that have a stated role within the context of the passed
     * observable, which must be a direct observable. The result may differ if a reasoner
     * is connected.
     * 
     * If an actuator is passed, check if it wants to rearrange the dependencies based on the context
     * before anything happens.
     * 
     * @param observable
     * @return triples containing the target concept, the role it is restricting, and the property that carries the restriction.
     */
    public static Collection<Triple<IConcept, IConcept, IProperty>> getSemanticDependencies(ISemantic observable) {

        Map<IConcept, IProperty> cc = new HashMap<>();
        List<Triple<IConcept, IConcept, IProperty>> ret = new ArrayList<>();

        IKnowledge k = observable.getType();
        if (k instanceof Concept) {
            for (Pair<IConcept, IProperty> cp : ((Concept) k).getObjectRestrictions()) {
                if (isObservable(cp.getFirst()) && !cp.getFirst().isAbstract()) {
                    cc.put(cp.getFirst(), cp.getSecond());
                }
            }
        }

        for (IConcept fc : getLeastGeneral(cc.keySet())) {
            Collection<IConcept> restricted;
            try {
                restricted = getLeastGeneral(fc.getPropertyRange(cc.get(fc)));
                if (restricted.size() > 0) {
                    IConcept rest = restricted.iterator().next();
                    if (NS.isRole(rest)) {
                        ret.add(new Triple<IConcept, IConcept, IProperty>(fc, rest, cc.get(fc)));
                    }
                }
            } catch (ThinklabException e) {
            }
        }

        return ret;
    }

    public static IConcept getCoreConceptFor(String key) {

        if (key.equals(KIM.OBJECT_CONCEPT)) {
            return KLAB.c(NS.CORE_OBJECT);
        } else if (key.equals(KIM.ATTRIBUTE_CONCEPT)) {
            return KLAB.c(NS.ATTRIBUTE_TRAIT);
        } else if (key.equals(KIM.ROLE_CONCEPT)) {
            return KLAB.c(NS.ROLE_TRAIT);
        } else if (key.equals(KIM.PROCESS_CONCEPT)) {
            return KLAB.c(NS.CORE_PROCESS);
        } else if (key.equals(KIM.QUALITY_CONCEPT)) {
            return KLAB.c(NS.CORE_QUALITY);
        } else if (key.equals(KIM.QUANTITY_CONCEPT)) {
            return KLAB.c(NS.CORE_QUANTITY);
        } else if (key.equals(KIM.THING_CONCEPT)) {
            return KLAB.c(NS.CORE_PHYSICAL_OBJECT);
        } else if (key.equals(KIM.ENERGY_CONCEPT)) {
            return KLAB.c(NS.CORE_ENERGY);
        } else if (key.equals(KIM.ENTROPY_CONCEPT)) {
            return KLAB.c(NS.CORE_ENTROPY);
        } else if (key.equals(KIM.LENGTH_CONCEPT)) {
            return KLAB.c(NS.CORE_LENGTH);
        } else if (key.equals(KIM.MASS_CONCEPT)) {
            return KLAB.c(NS.CORE_MASS);
        } else if (key.equals(KIM.VOLUME_CONCEPT)) {
            return KLAB.c(NS.CORE_VOLUME);
        } else if (key.equals(KIM.WEIGHT_CONCEPT)) {
            return KLAB.c(NS.CORE_WEIGHT);
        } else if (key.equals(KIM.MONETARY_VALUE_CONCEPT)) {
            return KLAB.c(NS.CORE_MONETARY_VALUE);
        } else if (key.equals(KIM.DURATION_CONCEPT)) {
            return KLAB.c(NS.CORE_DURATION);
        } else if (key.equals(KIM.PREFERENCE_VALUE_CONCEPT)) {
            return KLAB.c(NS.CORE_PREFERENCE_VALUE);
        } else if (key.equals(KIM.ACCELERATION_CONCEPT)) {
            return KLAB.c(NS.CORE_ACCELERATION);
        } else if (key.equals(KIM.AREA_CONCEPT)) {
            return KLAB.c(NS.CORE_AREA);
        } else if (key.equals(KIM.DENSITY_CONCEPT)) {
            return KLAB.c(NS.CORE_DENSITY);
        } else if (key.equals(KIM.ELECTRIC_POTENTIAL_CONCEPT)) {
            return KLAB.c(NS.CORE_ELECTRIC_POTENTIAL);
        } else if (key.equals(KIM.CHARGE_CONCEPT)) {
            return KLAB.c(NS.CORE_CHARGE);
        } else if (key.equals(KIM.RESISTANCE_CONCEPT)) {
            return KLAB.c(NS.CORE_RESISTANCE);
        } else if (key.equals(KIM.RESISTIVITY_CONCEPT)) {
            return KLAB.c(NS.CORE_RESISTIVITY);
        } else if (key.equals(KIM.PRESSURE_CONCEPT)) {
            return KLAB.c(NS.CORE_PRESSURE);
        } else if (key.equals(KIM.SLOPE_CONCEPT)) {
            return KLAB.c(NS.CORE_ANGLE);
        } else if (key.equals(KIM.PATTERN_CONCEPT)) {
            return KLAB.c(NS.CORE_PATTERN);
        } else if (key.equals(KIM.SPEED_CONCEPT)) {
            return KLAB.c(NS.CORE_SPEED);
        } else if (key.equals(KIM.TEMPERATURE_CONCEPT)) {
            return KLAB.c(NS.CORE_TEMPERATURE);
        } else if (key.equals(KIM.VISCOSITY_CONCEPT)) {
            return KLAB.c(NS.CORE_VISCOSITY);
        } else if (key.equals(KIM.SOCIAL_AGENT_CONCEPT)) {
            return KLAB.c(NS.SOCIAL_AGENT);
        } else if (key.equals(KIM.DELIBERATIVE_AGENT_CONCEPT)) {
            return KLAB.c(NS.DELIBERATIVE_AGENT);
        } else if (key.equals(KIM.REACTIVE_AGENT_CONCEPT)) {
            return KLAB.c(NS.REACTIVE_AGENT);
        } else if (key.equals(KIM.ORGANIZED_AGENT_CONCEPT)) {
            return KLAB.c(NS.ORGANIZED_AGENT);
        } else if (key.equals(KIM.AGENT_CONCEPT)) {
            return KLAB.c(NS.CORE_AGENT);
        } else if (key.equals(KIM.EVENT_CONCEPT)) {
            return KLAB.c(NS.CORE_EVENT);
        } else if (key.equals(KIM.QUALITY_SPACE_CONCEPT)) {
            return KLAB.c(NS.TYPE);
        } else if (key.equals(KIM.ORDERING_CONCEPT)) {
            return KLAB.c(NS.ORDERING);
        } else if (key.equals(KIM.DOMAIN_CONCEPT)) {
            return KLAB.c(NS.CORE_DOMAIN);
        } else if (key.equals(KIM.SUBJECTIVE_SPECIFIER)) {
            return KLAB.c(NS.SUBJECTIVE_TRAIT);
        } else if (key.equals(KIM.IDENTITY_CONCEPT)) {
            return KLAB.c(NS.CORE_IDENTITY_TRAIT);
        } else if (key.equals(KIM.REALM_CONCEPT)) {
            return KLAB.c(NS.CORE_REALM_TRAIT);
        }

        return KLAB.c(key);
    }

    /**
     * Add a trait exposed by the passed type. Must be valid and must be called in order of
     * declaration. 
     * 
     * @param type
     * @param trait
     */
    public static void addExposedTrait(IConcept type, IObservable trait) {
        if (!traitContextualizers.containsKey(type)) {
            traitContextualizers.put(type, new ClassTraitStructure(type));
        }
        traitContextualizers.get(type).abstractTraitsExposed.add(trait);
    }

    /**
     * Return all the types that are capable of contextualizing the passed trait, i.e.
     * allow its observation in a context through the observation of the class.
     * 
     * @param trait
     * @return
     */
    public static Collection<IConcept> getContextualizingTypes(IConcept trait) {
        Set<IConcept> ret = new HashSet<>();
        for (IConcept c : traitContextualizers.keySet()) {
            if (traitContextualizers.get(c).abstractTraitsExposed.contains(trait)) {
                ret.add(c);
            }
        }
        return ret;
    }

    /**
     * If passed concept is a trait, return it unmodified; else if the concept is 
     * observable, return the abstract trait corresponding to the observability of the concept and
     * ensure that its superclass and negation exist in the knowledge base. If the
     * concept is not observable, return null.
     * 
     * @param concept
     * @return
     */
    public static IConcept getTraitFor(IKnowledge concept) {
        if (NS.isTrait(concept)) {
            return (IConcept) concept;
        }
        IConcept ret = null;
        if (NS.isObservable(concept)) {
            String traitID = concept.getLocalName() + "Observability";
            ret = concept.getOntology().getConcept(traitID);
            if (ret == null) {
                String yesTraitID = concept.getLocalName() + "Observable";
                String noTraitID = concept.getLocalName() + "NotObservable";
                List<IAxiom> axioms = new ArrayList<>();
                axioms.add(Axiom.ClassAssertion(traitID));
                axioms.add(Axiom.SubClass(OBSERVABILITY_TRAIT, traitID));
                axioms.add(Axiom.AnnotationAssertion(traitID, NS.IS_ABSTRACT, "true"));
                axioms.add(Axiom.ClassAssertion(yesTraitID));
                axioms.add(Axiom.ClassAssertion(noTraitID));
                axioms.add(Axiom.SubClass(traitID, yesTraitID));
                axioms.add(Axiom.SubClass(traitID, noTraitID));
                concept.getOntology().define(axioms);
                ret = concept.getOntology().getConcept(traitID);
            }
        }
        return ret;
    }

    /**
     * Get the concrete observability trait corresponding to the abstract observability trait passed. Must
     * have been created by system. Everything else will return null.
     * 
     * @param abstractObservabilityTrait
     * @param negated
     * @return
     */
    public static IConcept getConcreteObservabilityTrait(IConcept abstractObservabilityTrait, boolean negated) {

        if (!abstractObservabilityTrait.is(KLAB.c(OBSERVABILITY_TRAIT))
                || !abstractObservabilityTrait.getLocalName().endsWith("Observability")) {
            return null;
        }

        String traitID = abstractObservabilityTrait.getLocalName();
        traitID = traitID.substring(0, traitID.length() - "Observability".length());

        return negated ? abstractObservabilityTrait.getOntology().getConcept(traitID + "NotObservable")
                : abstractObservabilityTrait.getOntology().getConcept(traitID + "Observable");
    }

    /**
     * Return the traits that are contextualized by the passed type, or a null
     * collection if not found.
     * 
     * @param type
     * @return
     */
    public static List<IObservable> getExposedTraitsForType(IConcept type) {
        type = getRootType(type);
        ClassTraitStructure ret = type == null ? null : traitContextualizers.get(type);
        return ret == null ? null : ret.abstractTraitsExposed;
    }

    public static void resetContextualizedTraits(IConcept type) {
        traitContextualizers.remove(type);
    }

    /**
     * Return the parent type that was given a list of exposed traits, or null if this
     * is a root type.
     * 
     * @param type
     * @return
     */
    public static IConcept getRootType(IConcept type) {

        if (traitContextualizers.containsKey(type)) {
            return type;
        }
        for (IConcept c : type.getParents()) {
            IConcept cc = getRootType(c);
            if (cc != null) {
                return cc;
            }
        }
        return null;
    }

    /**
     * Get the inherent type either from the concept (through restrictions) or from the
     * observable if one is passed.
     * 
     * @param type
     * @return
     */
    public static IKnowledge getInherentType(ISemantic type) {
        if (type instanceof IObservable) {
            return ((IObservable) type).getInherentType();
        }
        if (type.getType() instanceof IConcept) {
            IConcept c = (IConcept) type.getType();
            Collection<IConcept> rng = null;
            try {
                rng = c.getPropertyRange(KLAB.p(NS.INHERENT_IN));
            } catch (ThinklabException e) {
                return null;
            }
            if (rng != null && rng.size() > 0) {
                return rng.iterator().next();
            }
        }
        return null;
    }

    /**
     * Get the context type either from the concept (through restrictions) or from the
     * observable if one is passed.
     * 
     * @param type
     * @return
     */
    public static IKnowledge getContextType(ISemantic type) {
        if (type instanceof IObservable) {
            return ((IObservable) type).getContextType();
        }
        if (type.getType() instanceof IConcept) {
            IConcept c = (IConcept) type.getType();
            Collection<IConcept> rng = null;
            try {
                rng = c.getPropertyRange(KLAB.p(NS.OBSERVED_INTO));
            } catch (ThinklabException e) {
                return null;
            }
            if (rng != null && rng.size() > 0) {
                return rng.iterator().next();
            }
        }
        return null;
    }

    /**
     * Return the order of the concepts subsumed by the passed one, which must be an ordering
     * trait. 
     * 
     * @param ordering
     * @param level
     * @return
     */
    public static List<IConcept> getOrdering(IConcept ordering, @Nullable String level) {

        List<IConcept> ret = new ArrayList<>();

        return ret;
    }

    // /**
    // * Get the admitted children concepts at the passed level of detail (including all parents)
    // * according to the k.IM declaration of the base concept.
    // *
    // * @param concept
    // * @param level
    // * @return
    // */
    // public static Set<IConcept> getConcreteChildrenAtDetailLevel(IConcept concept, int level) {
    // Set<IConcept> ret = new HashSet<>();
    // return ret;
    // }

    /**
     * Get the negation trait of the passed concept ('no' or 'not' <C> in the language) which may be:
     *  1. an empty list (concept does not have a negation)
     *  2. a list with one concept (the 'no' case, e.g. non-observability of C, or the denied trait if deniable)
     *     if concept is deniable and abstract
     *  3. a list with the "other" concepts (in OR) if concept is deniable and concrete.
     *   
     * @param concept
     * @return
     */
    public static Collection<IConcept> getNegation(IConcept concept) {
        Set<IConcept> ret = new HashSet<>();
        // TODO
        return ret;
    }

    /**
     * Return the observation type according to restrictions. May not be the same stated by the
     * observer, but the latter must be compatible with it. If the passed knowledge is not observable
     * this returns null, and it's an error to observe the type.
     * 
     * @param observable
     * @return
     */
    public static IConcept getObservationType(IKnowledge observable) {
        if (NS.isQuality(observable) || NS.isTrait(observable)) {
            try {
                Collection<IConcept> ccs = ((IConcept) observable).getPropertyRange(KLAB.p(OBSERVATION_TYPE));
                return ccs.size() > 0 ? getLeastGeneralConcept(ccs) : KLAB.c(INDIRECT_OBSERVATION);
            } catch (ThinklabException e) {
                return KLAB.c(INDIRECT_OBSERVATION);
            }
        }
        return KLAB.c(DIRECT_OBSERVATION);
    }

    public static List<String> dumpConcepts(String namespace) {

        List<String> ret = new ArrayList<>();

        for (INamespace ns : KLAB.MMANAGER.getNamespaces()) {
            if (namespace != null && !namespace.equals(ns.getId())) {
                continue;
            }

            for (IModelObject o : ns.getModelObjects()) {
                dumpKnowledge(o, ret, 0);
            }

        }
        return ret;
    }

    private static void dumpKnowledge(IModelObject o, List<String> ret, int indent) {

        if (o instanceof IKnowledgeObject) {

            if (ret.isEmpty()) {
                ret.add("NAMESPACE,ID,ID (hierarchy),DOMAIN,CORE TYPE,DESCRIPTION");
            }

            IConcept domain = ((IKnowledgeObject) o).getKnowledge().getDomain();
            IKnowledge coretp = NS.getCoreType((IKnowledgeObject) o);
            IKnowledge type = ((IKnowledgeObject) o).getKnowledge();

            String id = o.getId();
            String ns = o.getNamespace().getId();
            String in = StringUtils.repeat("*", indent) + o.getId();
            String dm = domain == null ? "N/A" : domain.toString();
            String ct = coretp == null ? "N/A" : coretp.getLocalName();
            String cm = type.getMetadata().getString(IMetadata.DC_COMMENT);

            ret.add(ns + "," + id + "," + in + "," + dm + "," + ct + "," + cm);

            for (IModelObject oo : o.getChildren()) {
                dumpKnowledge(oo, ret, indent + 1);
            }
        }
    }

    /**
     * Define the adopted traits for a subclass of a class that has previously exposed their 
     * abstract definitions.
     * 
     * @param anc
     * @param knowledge
     * @param adoptedTraits
     */
    public static void setAdoptedTraits(IConcept anc, IConcept knowledge, List<IObservable> adoptedTraits) {
        ClassTraitStructure cs = traitContextualizers.get(anc);
        if (cs != null) {
            cs.concreteTraitsAdopted.put(knowledge, adoptedTraits);
        }
    }

    /**
     * Return the adopted trait for the passed subclass of the passed class. '
     * 
     * @param anc
     * @param knowledge
     * @return
     */
    public static List<IObservable> getAdoptedTraits(IConcept anc, IConcept knowledge) {
        ClassTraitStructure cs = traitContextualizers.get(anc);
        if (cs != null) {
            return cs.concreteTraitsAdopted.get(knowledge);
        }
        return null;
    }

    /**
     * Return a (flat) list of all children up to the passed level of detail, using
     * the model object (stated) hierarchy and keeping the order of declaration (depth-
     * first if more levels are involved). Allows abstract concepts in the result - if
     * only concrete ones are desires, use {@link getConcreteChildrenAtLevel} instead.
     * 
     * @param base
     * @param level
     * @return
     */
    public static List<IConcept> getChildrenAtLevel(IConcept baseType, int level) {

        List<IConcept> ret = new ArrayList<>();

        if (!isClass(baseType) && !isTrait(baseType)) {
            return ret;
        }

        INamespace ns = KLAB.MMANAGER.getNamespace(baseType.getConceptSpace());
        if (ns == null) {
            return ret;
        }

        IModelObject mo = ns.getModelObject(baseType.getLocalName());
        if (mo == null) {
            return ret;
        }

        findAtLevel(mo, ret, level, 0, false);

        return ret;
    }

    /**
     * Return a (flat) list of all CONCRETE children up to the passed level of detail, using
     * the model object (stated) hierarchy and keeping the order of declaration (depth-
     * first if more levels are involved).
     * 
     * @param base
     * @param level
     * @return
     */
    public static List<IConcept> getConcreteChildrenAtLevel(IConcept baseType, int level) {

        List<IConcept> ret = new ArrayList<>();

        if (!isClass(baseType) && !isTrait(baseType)) {
            return ret;
        }

        INamespace ns = KLAB.MMANAGER.getNamespace(baseType.getConceptSpace());
        if (ns == null) {
            return ret;
        }

        IModelObject mo = ns.getModelObject(baseType.getLocalName());
        if (mo == null) {
            return ret;
        }

        findAtLevel(mo, ret, level, 0, true);

        return ret;
    }

    private static void findAtLevel(IModelObject mo, List<IConcept> ret, int level, int current, boolean filterAbstract) {

        IConcept k = KLAB.c(mo.getName());
        if (!filterAbstract || !k.isAbstract()) {
            ret.add(k);
        }
        if (level < 0 || level < current) {
            for (IModelObject o : mo.getChildren()) {
                if (o instanceof IKnowledgeObject) {
                    findAtLevel(o, ret, level, current + 1, filterAbstract);
                }
            }
        }
    }

    /**
     * If current is a child of base at passed level, return it; otherwise return the
     * parent at the passed level, or null if the concept is unrelated or higher than
     * level. Uses the model object (stated) hierarchy.
     * 
     * @param base
     * @param current
     * @param level
     * @return
     */
    public static IConcept getParentAtLevel(IConcept base, IConcept current, int level) {
        IConcept ret = null;
        if (current.is(base)) {
            int l = getDetailLevel(base, current);
            if (l == level) {
                return current;
            }
            if (l > level) {
                for (IConcept c : getChildrenAtLevel(base, level)) {
                    if (current.is(c)) {
                        return c;
                    }
                }
            }
        }
        return ret;
    }

    /**
     * Get the level of detail of current in the DECLARED hierarchy of base - i.e. using the model objects 
     * declared in k.IM. Only works with trait and class types, as this is only relevant to
     * classifications.
     * 
     * @param baseType
     * @param key
     * @return
     */
    public static int getDetailLevel(IConcept base, IConcept current) {
        return getDetailLevel(base, current.toString());
    }

    /**
     * Get the level of detail corresponding to the passed key in the DECLARED hierarchy 
     * of baseType - i.e. using the model objects declared in k.IM. Key can be the concept
     * fully qualified name or its ID alone, matched case-insensitive. Semantics alone does 
     * not suffice: the concepts must be arranged in a declaration hierarchy for the levels 
     * to be attributed.
     * 
     * Also only works with trait and class types, as this is only relevant to
     * classifications.
     * 
     * @param baseType
     * @param key
     * @return
     */
    public static int getDetailLevel(IKnowledge baseType, String key) {

        if (!isClass(baseType) && !isTrait(baseType)) {
            return -1;
        }

        /*
         * go through children using the model object hierarchy. Will only find the ones declared in a hierarchy.
         */
        INamespace ns = KLAB.MMANAGER.getNamespace(baseType.getConceptSpace());
        if (ns == null) {
            return -1;
        }

        IModelObject mo = ns.getModelObject(baseType.getLocalName());
        if (mo == null) {
            return -1;
        }

        return findLevel(mo, key, 0);
    }

    private static int findLevel(IModelObject mo, String key, int level) {

        if (mo == null || mo.getName() == null) {
            return -1;
        }

        if (mo.getName().equals(key) || mo.getId().equalsIgnoreCase(key)) {
            return level;
        }

        for (IModelObject o : mo.getChildren()) {
            if (o instanceof IKnowledgeObject) {
                int l = findLevel(o, key, level + 1);
                if (l > 0) {
                    return l;
                }
            }
        }

        return -1;
    }

    public static List<IAnnotation> getAnnotations(IModelObject o, String string) {
        List<IAnnotation> ret = new ArrayList<>();
        for (IAnnotation a : o.getAnnotations()) {
            if (a.getId().equals(string)) {
                ret.add(a);
            }
        }
        return ret;
    }

    /**
     * State that subject has (role) in (target subject in)? restricted.
     * 
     * @param role
     * @param subject
     * @param restricted
     */
    public static void addRole(IConcept role, IConcept subject, @Nullable IConcept targetSubject, IConcept restricted /*, IProperty restrictingProperty*/, INamespace namespace) {

        RoleDescriptor rd = new RoleDescriptor();
        rd.role = role;
        rd.target = subject;
        rd.targetSubject = targetSubject;
        rd.within = restricted;
        // rd.property = restrictingProperty;
        rd.namespaceId = namespace.getId();
        rd.scenario = namespace.isScenario() ? namespace.getId() : null;
        roles.put(role, rd);

    }

    // public getApplicableRoles(IObservable observable, IResolutionContext context) {
    //
    // }

    /**
     * Get all dependencies that are implied by the roles attributed to observable in context. Use
     * stated semantics, scenarios and scale. 
     * 
     * @param observable
     * @param context
     * @return
     */
    public static Collection<IConcept> getRoles(IObservable observable, IResolutionContext context) {

        Map<IConcept, List<RoleDescriptor>> cnd = new HashMap<>();
        List<IConcept> ret = new ArrayList<>();

        /*
         * collect all applicable
         */
        for (Object o : roles.values()) {
            RoleDescriptor rd = (RoleDescriptor) o;
            if (rd.within.is(observable)) {
                if (contextApplies(rd, context)) {
                    if (!cnd.containsKey(rd.target)) {
                        cnd.put(rd.target, new ArrayList<RoleDescriptor>());
                    }
                    cnd.get(rd.target).add(rd);
                }
            }
        }

        ret.addAll(cnd.keySet());

        // for (IConcept target : cnd.keySet()) {
        // IConcept role = prioritizeRole(context, cnd.get(target));
        // if (role != null) {
        // ret.add(target);
        // }
        // }

        return ret;
    }

    // private static IConcept prioritizeRole(IResolutionContext context, List<RoleDescriptor> list) {
    // // TODO Auto-generated method stub
    // return null;
    // }

    private static boolean contextApplies(RoleDescriptor rd, IResolutionContext context) {

        if (rd.scenario != null) {
            return context.getScenarios().contains(rd.scenario);
        }
        if (rd.targetSubject != null) {
            return context.getSubject().getObservable().is(rd.targetSubject);
        }
        if (rd.coverage != null) {
            try {
                return context.getScale().intersects(rd.coverage);
            } catch (ThinklabException e) {
                return false;
            }
        }

        return true;
    }

    /**
     * Return a list of n levels (tagged as an ordering) guaranteed to exist, in the local namespace.
     * 
     * @param maxBins
     * @return
     */
    public static List<IConcept> getLevels(int maxBins) {

        List<IConcept> ret = new ArrayList<>();
        INamespace ns = KLAB.MMANAGER.getLocalNamespace();
        IConcept parent = NS.getUserOrdering();
        for (int i = 0; i < maxBins; i++) {
            String lname = "UL" + i;
            IConcept level = KLAB.KM.getConcept(ns.getId() + ":" + lname);
            if (level == null) {
                ArrayList<IAxiom> ax = new ArrayList<IAxiom>();
                ax.add(Axiom.ClassAssertion(lname));
                ax.add(Axiom.SubClass(parent.getLocalName(), lname));
                ax.add(Axiom.AnnotationAssertion(lname, NS.ORDER_PROPERTY, "" + i));
                ns.getOntology().define(ax);
                level = ns.getOntology().getConcept(lname);
            }
            ret.add(level);
        }

        return ret;
    }

    /**
     * Return a stable abstract ordering from the local namespace.
     * 
     * @return
     */
    public static IConcept getUserOrdering() {
        String cname = "UL";
        INamespace ns = KLAB.MMANAGER.getLocalNamespace();
        IConcept ret = KLAB.KM.getConcept(ns.getId() + ":" + cname);
        if (ret == null) {
            ArrayList<IAxiom> ax = new ArrayList<IAxiom>();
            ax.add(Axiom.ClassAssertion(cname));
            ax.add(Axiom.SubClass(NS.ORDERING, cname));
            ax.add(Axiom.AnnotationAssertion(cname, NS.IS_ABSTRACT, "true"));
            ns.getOntology().define(ax);
            ret = ns.getOntology().getConcept(cname);
        }
        return ret;
    }

}
