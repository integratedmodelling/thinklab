/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/

package org.integratedmodelling.common.vocabulary;

import org.integratedmodelling.api.knowledge.IConcept;
import org.integratedmodelling.common.configuration.KLAB;
import org.integratedmodelling.exceptions.ThinklabException;

/**
 * Namespace constants for concepts essential in GIS modeling. Also provides general functions to
 * handle UTM and Lat/Lon coordinate conversion.
 * 
 * TODO use defaults only after
 * checking the properties for overridden concepts - for current uses this is OK but depends
 * on the im ontologies being available.
 * 
 * @author Ferd
 *
 */
public class EcologyNS extends GeoNS {

    // main observables
    public static IConcept BIOMASS                = null;
    public static IConcept PLANT_BIOMASS          = null;
    public static IConcept VASCULAR_PLANT_BIOMASS = null;
    public static IConcept ALGAL_BIOMASS          = null;
    public static IConcept TERRESTRIAL_BIOMASS    = null;
    public static IConcept BELOW_GROUND_BIOMASS   = null;
    public static IConcept ROOT_BIOMASS           = null;
    public static IConcept ABOVE_GROUND_BIOMASS   = null;
    public static IConcept WOOD_BIOMASS           = null;
    public static IConcept HARDWOOD_BIOMASS       = null;
    public static IConcept SOFTWOOD_BIOMASS       = null;

    /**
     * correspondent metadata tags in namespace
     */
    public static String BIOMASS_CONCEPT                = "IM.BIOMASS";
    public static String PLANT_BIOMASS_CONCEPT          = "IM.PLANT_BIOMASS";
    public static String VASCULAR_PLANT_BIOMASS_CONCEPT = "IM.VASCULAR_PLANT_BIOMASS";
    public static String ALGAL_BIOMASS_CONCEPT          = "IM.ALGAL_BIOMASS";
    public static String TERRESTRIAL_BIOMASS_CONCEPT    = "IM.TERRESTRIAL_BIOMASS";
    public static String BELOW_GROUND_BIOMASS_CONCEPT   = "IM.BELOW_GROUND_BIOMASS";
    public static String ROOT_BIOMASS_CONCEPT           = "IM.ROOT_BIOMASS";
    public static String ABOVE_GROUND_BIOMASS_CONCEPT   = "IM.ABOVE_GROUND_BIOMASS";
    public static String WOOD_BIOMASS_CONCEPT           = "IM.WOOD_BIOMASS";
    public static String HARDWOOD_BIOMASS_CONCEPT       = "IM.HARDWOOD_BIOMASS";
    public static String SOFTWOOD_BIOMASS_CONCEPT       = "IM.SOFTWOOD_BIOMASS";

    public static boolean synchronize() {

        if (!GeoNS.synchronize()) {
            return false;
        }

        if (BIOMASS == null) {

            try {
                BIOMASS = checkClass(KLAB.MMANAGER
                        .getExportedKnowledge(BIOMASS_CONCEPT), IConcept.class);
                PLANT_BIOMASS = checkClass(KLAB.MMANAGER
                        .getExportedKnowledge(PLANT_BIOMASS_CONCEPT), IConcept.class);
                VASCULAR_PLANT_BIOMASS = checkClass(KLAB.MMANAGER
                        .getExportedKnowledge(VASCULAR_PLANT_BIOMASS_CONCEPT), IConcept.class);
                ALGAL_BIOMASS = checkClass(KLAB.MMANAGER
                        .getExportedKnowledge(ALGAL_BIOMASS_CONCEPT), IConcept.class);
                TERRESTRIAL_BIOMASS = checkClass(KLAB.MMANAGER
                        .getExportedKnowledge(TERRESTRIAL_BIOMASS_CONCEPT), IConcept.class);
                BELOW_GROUND_BIOMASS = checkClass(KLAB.MMANAGER
                        .getExportedKnowledge(BELOW_GROUND_BIOMASS_CONCEPT), IConcept.class);
                ROOT_BIOMASS = checkClass(KLAB.MMANAGER
                        .getExportedKnowledge(ROOT_BIOMASS_CONCEPT), IConcept.class);
                ABOVE_GROUND_BIOMASS = checkClass(KLAB.MMANAGER
                        .getExportedKnowledge(ABOVE_GROUND_BIOMASS_CONCEPT), IConcept.class);
                WOOD_BIOMASS = checkClass(KLAB.MMANAGER
                        .getExportedKnowledge(WOOD_BIOMASS_CONCEPT), IConcept.class);
                HARDWOOD_BIOMASS = checkClass(KLAB.MMANAGER
                        .getExportedKnowledge(HARDWOOD_BIOMASS_CONCEPT), IConcept.class);
                SOFTWOOD_BIOMASS = checkClass(KLAB.MMANAGER
                        .getExportedKnowledge(SOFTWOOD_BIOMASS_CONCEPT), IConcept.class);
            } catch (ThinklabException e) {
                return false;
            }
        }

        return true;
    }
}
