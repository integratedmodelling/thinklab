/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/

package org.integratedmodelling.common.vocabulary;

import java.util.Map;

import org.integratedmodelling.api.knowledge.IConcept;
import org.integratedmodelling.api.metadata.IObservationMetadata;
import org.integratedmodelling.api.modelling.IDirectObserver;
import org.integratedmodelling.api.modelling.INamespace;
import org.integratedmodelling.api.time.ITemporalExtent;
import org.integratedmodelling.common.configuration.KLAB;
import org.integratedmodelling.common.kim.KIMDirectObserver;
import org.integratedmodelling.common.model.runtime.Time;
import org.integratedmodelling.common.owl.Knowledge;
import org.integratedmodelling.common.utils.MapUtils;
import org.integratedmodelling.lang.IRemoteSerializable;

/**
 * Simple holder of the crucial metadata to instantiate a remote observation, returned by 
 * services so we don't need to instantiate the results after query. Not only a matter of
 * efficiency, as the storing engine may have lost concepts that are known to the client so
 * we don't want to instantiate observables at the engine side.
 * 
 * @author Ferd
 *
 */
public class ObservationMetadata implements IRemoteSerializable, IObservationMetadata {

    public String name;
    public String id;
    public String namespaceId;
    public String serverId;
    public String observableName;
    public String description;
    public String geometryWKB;
    public Long   start;
    public Long   end;
    public Long   step;

    @Override
    public Object adapt() {
        return MapUtils
                .of("name", name, "id", id, "namespace-id", namespaceId, "server-id", serverId, "observable-name", observableName, "description", description, "geometry-wkb", geometryWKB, "start", start, "end", end, "step", step);
    }

    public ObservationMetadata() {
    }

    public ObservationMetadata(Map<?, ?> map) {
        this.name = map.get("name").toString();
        this.id = map.get("id").toString();
        this.namespaceId = map.get("namespace-id").toString();
        this.serverId = map.get("server-id").toString();
        this.observableName = map.get("observable-name").toString();
        if (map.containsKey("description")) {
            this.description = map.get("description").toString();
        }
        if (map.containsKey("geometry-wkb")) {
            this.geometryWKB = map.get("geometry-wkb").toString();
        }
        if (map.containsKey("start")) {
            this.start = ((Number) (map.get("start"))).longValue();
        }
        if (map.containsKey("end")) {
            this.end = ((Number) (map.get("end"))).longValue();
        }
        if (map.containsKey("step")) {
            this.step = ((Number) (map.get("step"))).longValue();
        }
    }

    @Override
    public boolean equals(Object obj) {
        return obj instanceof ObservationMetadata && name.equals(((ObservationMetadata) obj).name);
    }

    @Override
    public int hashCode() {
        return name.hashCode();
    }

    @Override
    public String toString() {
        return "[OM " + observableName + " " + name + " from " + serverId + "]";
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getId() {
        return id;
    }

    @Override
    public String getNodeId() {
        return serverId;
    }

    @Override
    public IConcept getConcept() {
        return (IConcept) Knowledge.parse(observableName);
    }

    /**
     * Create a subject observer from data, assuming the concept exists and everything is legitimate.
     * 
     * @return
     */
    @Override
    public IDirectObserver getSubjectObserver() {

        IConcept observable = (IConcept) Knowledge.parse(observableName);
        if (observable == null) {
            return null;
        }

        //
        INamespace namespace = KLAB.MMANAGER.getNamespace(namespaceId);
        if (namespace == null) {
            namespace = KLAB.MMANAGER.getLocalNamespace();
        }
        ITemporalExtent time = null;
        if (getN(start) != 0 || getN(end) != 0) {
            time = new Time(getN(start), getN(end), getN(step));
        }

        IDirectObserver ret = new KIMDirectObserver(namespace, observable, id, KLAB.MFACTORY
                .createScale(geometryWKB, time));

        return ret;

    }

    private long getN(Long n) {
        return n == null ? 0 : n;
    }
}
