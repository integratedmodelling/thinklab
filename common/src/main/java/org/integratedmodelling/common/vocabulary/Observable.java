/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/

package org.integratedmodelling.common.vocabulary;

import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import org.integratedmodelling.api.knowledge.IConcept;
import org.integratedmodelling.api.knowledge.IKnowledge;
import org.integratedmodelling.api.knowledge.IProperty;
import org.integratedmodelling.api.lang.IParseable;
import org.integratedmodelling.api.modelling.IDirectObservation;
import org.integratedmodelling.api.modelling.IKnowledgeObject;
import org.integratedmodelling.api.modelling.IModel;
import org.integratedmodelling.api.modelling.IObservable;
import org.integratedmodelling.api.modelling.IObserver;
import org.integratedmodelling.collections.Triple;
import org.integratedmodelling.common.configuration.KLAB;
import org.integratedmodelling.common.kim.KIMContext;
import org.integratedmodelling.common.kim.ModelFactory;
import org.integratedmodelling.common.owl.Knowledge;
import org.integratedmodelling.common.utils.CamelCase;
import org.integratedmodelling.exceptions.ThinklabRuntimeException;
import org.integratedmodelling.exceptions.ThinklabValidationException;
import org.integratedmodelling.lang.Axiom;
import org.integratedmodelling.lang.IRemoteSerializable;

/**
 * Works as a delegate for a non-observed concept (e.g. ElevationSeaLevel) but provides the info that describe
 * the semantics of its observation that was (in a IState/ISubject) or can (in a IModel/IObserver) produce it.
 * 
 * @author Ferd
 * 
 */
public class Observable implements IObservable, IRemoteSerializable {

    IKnowledge mainType     = null;
    IConcept   obsType      = null;
    IKnowledge inherentType = null;
    IConcept   traiType     = null;
    IConcept   contextType  = null;
    String     formalName   = null;
    Action     action       = Action.EXPLAIN;

    // model and observer should be stored with the model, and restored at initializeFromStorage().
    IModel    model     = null;
    IObserver observer  = null;
    IConcept  semantics = null;

    // when using a classification type, this should be 0 or more, and refers to the level of
    // resolution in the hierarchy we're classifiying to. If 0 we're using the full hierarchy.
    int downTo      = -1;
    // when classifying 'by' a trait type, we may specify a detail level for that, too - independent of the
    // main level.
    int traitDownTo = -1;

    // observing subject makes two observables of the same thing reflect different POVs.
    IDirectObservation observingSubject = null;

    /*
     * TODO
     * whether we want to instantiate or explain direct objects. For now this is just a gimmick to
     * solve some difficult situations in resolution, and the "actual" info is in the IResolutionContext,
     * but it should probably be moved here and removed from there, as it's very hard to predict all
     * cases and new observers may need different strategies.
     */
    boolean isInstantiator = false;

    /*
     * this is set when deserialized, and must be linked to a real subject after the observation hierarchy
     * is completed.
     */
    String observingSubjectId = null;

    /*
     * roles are cached to avoid having to retrieve them too many times.
     */
    Collection<Triple<IConcept, IConcept, IProperty>> roles = null;

    // we keep a catalog of delegate concept -> its observable. Faster and easier than
    // doing everything with actual OWL restrictions.
    static Map<IKnowledge, IObservable> _catalog = new HashMap<IKnowledge, IObservable>();

    // for the DB
    public Observable() {
    }

    public Observable(Map<?, ?> map) {

        mainType = Knowledge.parse(map.get("type").toString());
        if (map.containsKey("observation")) {
            obsType = KLAB.c(map.get("observation").toString());
        }
        if (map.containsKey("inherent")) {
            inherentType = Knowledge.parse(map.get("inherent").toString());
        }
        if (map.containsKey("context")) {
            contextType = (IConcept) Knowledge.parse(map.get("context").toString());
        }
        if (map.containsKey("name")) {
            formalName = map.get("name").toString();
        }
        if (map.containsKey("observing-subject")) {
            observingSubjectId = map.get("observing-subject").toString();
            /*
             * TODO must link the subject in.
             */
        }
        if (map.containsKey("down-to")) {
            downTo = Integer.parseInt(map.get("down-to").toString());
        }
        if (map.containsKey("trait-down-to")) {
            traitDownTo = Integer.parseInt(map.get("trait-down-to").toString());
        }
    }

    public Observable(IKnowledge observedConcept, IConcept observationType, String name) {

        mainType = observedConcept;
        obsType = observationType;
        formalName = name;
    }

    /**
     * Direct observation of passed knowledge. Name is generated in the most obvious way.
     * @param observedConcept
     */
    public Observable(IKnowledge observedConcept) {
        if (observedConcept != null) {
            mainType = observedConcept;
            obsType = KLAB.c(NS.DIRECT_OBSERVATION);
            formalName = CamelCase.toLowerCase(observedConcept.getLocalName(), '-');
        }
    }

    public Observable(IKnowledge observedConcept, IConcept observationType, IConcept subjectType,
            String name, boolean reification) {
        this.mainType = observedConcept;
        this.obsType = observationType;
        this.inherentType = subjectType;
        this.formalName = name;
        this.action = reification ? Action.INSTANTIATE : Action.EXPLAIN;
    }

    public Observable(IObserver observer, IDirectObservation observingSubject, String name) {
        this.mainType = observer.getObservable().getType();
        this.obsType = observer.getObservable().getObservationType();
        this.inherentType = observer.getObservable().getInherentType();
        this.traiType = observer.getObservable().getTraitType();
        this.contextType = observer.getObservable().getContextType();

        if (observingSubject != null) {
            this.observingSubject = observingSubject;
            this.observingSubjectId = observingSubject.getId();
            // FIXME - confusing, unsafe and ugly.
            ((Observable) observer.getObservable()).observingSubject = observingSubject;
            ((Observable) observer.getObservable()).observingSubjectId = observingSubject.getId();
        }

        this.observer = observer;
        this.formalName = name;
        this.action = Action.EXPLAIN;
    }

    // public Observable(IKnowledge observedConcept, IConcept observationType, IConcept subjectType,
    // /* IConcept traitType,*/ String name, boolean reification) {
    // this.mainType = observedConcept;
    // this.obsType = observationType;
    // this.inherentType = subjectType;
    // // this.traiType = traitType;
    // this.formalName = name;
    // this.action = reification ? Action.INSTANTIATE : Action.EXPLAIN;
    // }

    /**
     * Passed a delegate observation type stated in a model's specification, which is used to reference the
     * observed types of the passed model. This stores the observable in the catalog.
     * 
     * @param concept
     * @param representedModel
     * @param name
     */
    public Observable(IKnowledge concept, IModel model, String name) {

        this(model, name);

        /*
         * make the concept a subclass of mainType if it's not already...
         */
        if (mainType != null && !concept.is(mainType) && concept.getOntology() != null) {
            concept.getOntology()
                    .define(Collections.singleton(Axiom.SubClass(mainType.toString(), concept.toString())));
        }

        /*
         * ...then make it the main type...
         */
        mainType = concept;

        /*
         * ...and remember the observable for this delegate so we don't make mistakes attributing it to an
         * incompatible observation type.
         */
        _catalog.put(concept, this);
    }

    /**
     * Create from an initialized observer (promoted to model). Used to define the delegate observation type
     * for models where the observable is anonymous, e.g. "model 100 as measure Elevation in m".
     * 
     * @param observableDefinition
     */
    public Observable(IModel model, String name) {

        if (model.getObserver() == null) {
            mainType = model.getObservable().getType();
            obsType = model.getObservable().getObservationType();
            inherentType = model.getObservable().getInherentType();
            traiType = model.getObservable().getTraitType();
            contextType = model.getObservable().getContextType();
            downTo = model.getObservable().getDetailLevel();
            traitDownTo = model.getObservable().getTraitDetailLevel();
        } else if (model.getObserver().getObservable() != null /* happens only in error */) {
            mainType = model.getObserver().getObservable().getType();
            obsType = model.getObserver().getObservable().getObservationType();
            inherentType = model.getObserver().getObservable().getInherentType();
            traiType = model.getObserver().getObservable().getTraitType();
            contextType = model.getObserver().getObservable().getContextType();
            downTo = model.getObserver().getObservable().getDetailLevel();
            traitDownTo = model.getObserver().getObservable().getTraitDetailLevel();
        }
        this.observer = model.getObserver();
        this.model = model;
        this.formalName = name;
        this.action = model.isInstantiator() ? Action.INSTANTIATE : Action.EXPLAIN;
    }

    /**
     * Constructor that will set the observation type to the most general for the passed observed concept.
     * @param contextModel
     */
    public Observable(IConcept concept) {

        mainType = concept;
        obsType = NS.isObject(concept) ? KLAB.c(NS.DIRECT_OBSERVATION) : KLAB.c(NS.INDIRECT_OBSERVATION);
        formalName = CamelCase.toLowerCase(concept.getLocalName(), '-');
    }

    public Observable(Observable o) {

        if (o == null) {
            formalName = "error";
            return;
        }

        mainType = o.mainType;
        obsType = o.obsType;
        formalName = o.formalName;
        observer = o.observer;
        model = o.model;
        semantics = o.semantics;
        inherentType = o.inherentType;
        traiType = o.traiType;
        contextType = o.contextType;
        observingSubject = o.observingSubject;
        observingSubjectId = o.observingSubjectId;
        downTo = o.downTo;
        traitDownTo = o.traitDownTo;
        isInstantiator = o.isInstantiator;
    }

    public void setObservingSubject(IDirectObservation subject) {
        observingSubject = subject;
        observingSubjectId = subject.getId();
    }

    public static IObservable getObservable(IConcept delegate) {
        return _catalog.get(delegate);
    }

    public boolean isInstantiator() {
        return isInstantiator;
    }

    // util to keep any semantic dependencies and their roles in this observable between calls.
    Collection<Triple<IConcept, IConcept, IProperty>> getRoles() {
        if (roles == null) {
            roles = NS.getSemanticDependencies(mainType);
        }
        return roles;
    }

    @Override
    public IKnowledge getType() {
        return mainType;
    }

    public void setFormalName(String name) {
        formalName = name;
    }

    public void setLevel(String key) {
        downTo = NS.getDetailLevel(mainType, key);
    }

    public void setDetailLevel(int n) {
        downTo = n;
    }

    @Override
    public String toString() {
        /*
         * must serialize back to the represented concept, otherwise storage of observables won't work
         * properly.
         */
        return "[" + (obsType == null ? "NULL" : obsType.getLocalName()) + // DirectObservation
                " of " + // of
                (mainType == null ? "NULL" : mainType.toString()) + // Elevation
                (inherentType == null ? "" : (" of " + inherentType)) + // of Mountain
                (contextType == null ? "" : (" in " + contextType)) + // in Region
                (traiType == null ? "" : (" by " + traiType)) + // by Trait
                "]";
    }

    @Override
    public String getLocalName() {
        return mainType.getLocalName();
    }

    @Override
    public boolean is(IObservable concept) {

        boolean conceptOK = mainType.is(concept.getType());
        boolean obsOK = obsType.is(concept.getObservationType());
        boolean inhOK = (inherentType == null && concept.getInherentType() == null)
                || (inherentType != null && concept.getInherentType() != null && inherentType.is(concept
                        .getInherentType()));
        boolean trtOK = (traiType == null && concept.getTraitType() == null)
                || (traiType != null && concept.getTraitType() != null && traiType.is(concept
                        .getTraitType()));
        return conceptOK && obsOK && inhOK && trtOK;
    }

    @Override
    public boolean is(IKnowledge concept) {
        return mainType == null ? false : mainType.is(concept);
    }

    /**
     * 
     * @param observationType
     * @param observedType
     * @return
     */
    @Override
    public boolean is(IConcept observationType, IConcept observedType) {

        boolean conceptOK = mainType.is(observedType);
        boolean obsOK = obsType.is(observationType);
        return conceptOK && obsOK;
    }

    /**
     * 
     * @param observationType
     * @param observedType
     * @param inherentToType
     * @return
     */
    @Override
    public boolean is(IConcept observationType, IConcept observedType, IConcept inherentToType) {

        boolean conceptOK = mainType.is(observedType);
        boolean obsOK = obsType.is(observationType);
        boolean inhOK = (inherentType == null && inherentToType == null)
                || (inherentType != null && inherentToType != null && inherentType.is(inherentToType));
        return conceptOK && obsOK && inhOK;
    }

    /**
     * 
     * @param observationType
     * @param observedType
     * @param inherentToType
     * @return
     */
    @Override
    public boolean is(IConcept observationType, IConcept observedType, IConcept inherentToType, IConcept traitType) {

        boolean conceptOK = mainType.is(observedType);
        boolean obsOK = obsType.is(observationType);
        boolean inhOK = (inherentType == null && inherentToType == null)
                || (inherentType != null && inherentToType != null && inherentType.is(inherentToType));
        return conceptOK && obsOK && inhOK;
    }

    // @Override
    public String getConceptSpace() {
        return mainType.getConceptSpace();
    }

    // @Override
    // public IMetadata getMetadata() {
    // return mainType.getMetadata();
    // }

    @Override
    public IModel getModel() {
        return model;
    }

    @Override
    public IObserver getObserver() {
        return observer;
    }

    @Override
    public IConcept getObservationType() {
        return obsType;
    }

    @Override
    public IKnowledge getInherentType() {
        return inherentType;
    }

    @Override
    public IConcept getTraitType() {
        return traiType;
    }

    @Override
    public String getFormalName() {
        return formalName;
    }

    @Override
    public int hashCode() {
        return getSignature().hashCode();
    }

    @Override
    public boolean equals(Object o) {
        return o instanceof Observable && getSignature().equals(((Observable) o).getSignature());
    }

    String _signature = null;

    private String getSignature() {
        if (_signature == null) {
            _signature = mainType + "|" + obsType;
            if (inherentType != null) {
                _signature += "|" + inherentType;
            }
            if (contextType != null) {
                _signature += "|" + contextType;
            }
            if (observingSubjectId != null) {
                _signature += "|" + observingSubjectId;
            }
        }
        return _signature;
    }

    public void setModel(IModel iModel) {
        model = iModel;
        if (iModel != null) {
            observer = iModel.getObserver();
        }
    }

    public void setInherentType(IKnowledge type) {
        inherentType = type;
    }

    public void setTraitType(IConcept type, int level) {
        traiType = type;
    }

    public Observable withInherentType(IKnowledge type) {
        Observable ret = new Observable(this);
        ret.setInherentType(type);
        ret.setObserver(observer);
        return ret;
    }

    public void setObservationType(IConcept c) {
        obsType = c;
    }

    public void setObservedType(IKnowledge c) {
        mainType = c;
    }

    @Override
    public Object adapt() {

        Map<Object, Object> ret = new HashMap<Object, Object>();

        if (mainType != null)
            ret.put("type", ((IParseable) mainType).asText());
        if (obsType != null)
            ret.put("observation", obsType.toString());
        if (inherentType != null)
            ret.put("inherent", ((IParseable) inherentType).asText());
        if (formalName != null)
            ret.put("name", formalName);
        if (observingSubjectId != null) {
            ret.put("observing-subject", observingSubjectId);
        }
        if (contextType != null) {
            ret.put("context", contextType);
        }
        ret.put("down-to", downTo + "");
        ret.put("trait-down-to", traitDownTo + "");

        return ret;
    }

    @Override
    public boolean canResolve(IObservable o) {

        /*
         * for now: like is(), except if our inherent type is null we let any inherent type pass.
         */
        boolean conceptOK = mainType.is(o.getType());
        boolean obsOK = obsType.is(o.getObservationType());
        boolean inhOK = inherentType == null
                || (inherentType != null && o.getInherentType() != null
                        && inherentType.is(o.getInherentType()));
        return conceptOK && obsOK && inhOK;
    }

    public IObservable setInstantiator(boolean b) {
        isInstantiator = b;
        return this;
    }

    public void setType(IKnowledge type) {
        mainType = type;
    }

    public void setType(IKnowledgeObject type, KIMContext context) {
        mainType = type.getKnowledge();
        if (type.getInherentType() != null) {
            if (this.inherentType != null && !this.inherentType.is(type.getInherentType())) {
                context.error("incompatible inherent types: " + this.inherentType + " vs. "
                        + type.getInherentType(), type.getFirstLineNumber());
            }
            inherentType = type.getInherentType();
        }
        if (type.getContextType() != null) {
            if (this.contextType != null && !this.contextType.is(type.getContextType())) {
                context.error("incompatible context types: " + this.contextType + " vs. "
                        + type.getContextType(), type.getFirstLineNumber());
            }
            contextType = (IConcept) type.getContextType();
        }
        if (type.getDetailLevel() > 0) {
            this.downTo = type.getDetailLevel();
        }
        if (this.obsType == null && mainType != null) {
            this.obsType = NS.getObservationType(mainType);
        }
        if (this.formalName == null && mainType != null) {
            formalName = CamelCase.toLowerCase(this.mainType.getLocalName(), '-');
        }
    }

    public void setContextType(IConcept type) {
        contextType = type;
    }

    public Observable withType(IKnowledge type) {
        Observable ret = new Observable(this);
        ret.setType(type);
        return ret;
    }

    // for the serializer
    public static String asString(IObservable observable) {

        String ret = "OBS!" + ((Knowledge) observable.getType()).asText();
        ret += "!" + observable.getFormalName();
        ret += "!"
                + (observable.getObservationType() == null ? "X"
                        : observable.getObservationType().toString());
        ret += "!"
                + (observable.getInherentType() == null ? "X" : ((Knowledge) observable.getInherentType())
                        .asText());
        ret += "!"
                + (observable.getTraitType() == null ? "X" : observable.getTraitType().toString());
        ret += "!"
                + (observable.getObserver() == null ? "X" : ModelFactory.serialize(observable
                        .getObserver()));
        ret += "!" +
                (((Observable) observable).observingSubjectId == null ? "X"
                        : ((Observable) observable).observingSubjectId);
        ret += "!"
                + (observable.getContextType() == null ? "X" : ((Knowledge) observable.getContextType())
                        .asText());
        ret += "!" + observable.getDetailLevel();
        ret += "!" + observable.getTraitDetailLevel();

        return ret;
    }

    // for the serializer
    public Observable(String string) {

        String[] defs = string.split("!");

        mainType = Knowledge.parse(defs[1]);
        formalName = defs[2];
        obsType = defs[3].equals("X") ? null : KLAB.c(defs[3]);
        inherentType = defs[4].equals("X") ? null : (IConcept) Knowledge.parse(defs[4]);
        traiType = defs[5].equals("X") ? null : KLAB.c(defs[5]);
        observer = defs[6].equals("X") ? null : ModelFactory.deserializeObserver(defs[6], this);
        observingSubjectId = defs[7].equals("X") ? null : defs[7];
        // TODO remove length checks when network is up to date
        contextType = (defs.length < 9 || defs[8].equals("X")) ? null : KLAB.c(defs[5]);
        downTo = (defs.length < 10 || defs[9].equals("X")) ? -1 : Integer.parseInt(defs[9]);
        traitDownTo = (defs.length < 11 || defs[10].equals("X")) ? -1 : Integer.parseInt(defs[10]);
    }

    public void setObserver(IObserver observer) {
        this.observer = observer;
    }

    @Override
    public IConcept getTypeAsConcept() {
        if (!(mainType instanceof IConcept)) {
            throw new ThinklabRuntimeException("internal error: requesting a concept in a property observable");
        }
        return (IConcept) mainType;
    }

    @Override
    public IProperty getTypeAsProperty() {
        if (!(mainType instanceof IProperty)) {
            throw new ThinklabRuntimeException("internal error: requesting a property in a concept observable");
        }
        return (IProperty) mainType;
    }

    @Override
    public Action getAction() {
        return action;
    }

    @Override
    public IDirectObservation getObservingSubject() {
        return observingSubject;
    }

    public String getObservingSubjectId() {
        return observingSubjectId;
    }

    public Observable withInterpretingTrait(IConcept interpretAs) throws ThinklabValidationException {

        if (interpretAs != null) {
            return withType(NS.addTraits(getType(), Collections.singleton(interpretAs)));
        }

        return this;

    }

    public boolean equalsWithoutInherency(IObservable observable) {
        return getType().equals(observable.getType())
                &&
                getObservationType().equals(observable.getObservationType())
                &&
                // jesus
                ((observingSubjectId == null && ((Observable) observable).observingSubjectId == null)
                        || (observingSubjectId != null
                                && ((Observable) observable).observingSubjectId != null && observingSubjectId
                                        .equals(((Observable) observable).observingSubjectId)));
    }

    public String getExportFileName() {

        String ret = CamelCase.toLowerCase(getType().getLocalName(), '_');
        if (observingSubjectId != null) {
            ret = observingSubjectId + "_" + ret;
        }
        return ret;
    }

    @Override
    public IConcept getContextType() {
        return contextType;
    }

    @Override
    public int getDetailLevel() {
        return downTo;
    }

    @Override
    public int getTraitDetailLevel() {
        return traitDownTo;
    }

}
