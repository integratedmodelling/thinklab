/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/

package org.integratedmodelling.common.vocabulary;

import java.util.ArrayList;
import java.util.Collection;

import org.integratedmodelling.api.knowledge.IConcept;
import org.integratedmodelling.api.knowledge.IKnowledge;
import org.integratedmodelling.api.knowledge.IOntology;
import org.integratedmodelling.api.runtime.IContext;
import org.integratedmodelling.common.Filter;
import org.integratedmodelling.common.configuration.KLAB;
import org.integratedmodelling.common.owl.Ontology;

/**
 * General utility filters and listers of concepts that will return top-level
 * concepts for many useful browsing operations. By default it only includes
 * concepts from the core ontologies (defaulting at im.*) and skips generated
 * concepts.
 * 
 * @author Ferd
 *
 */
public class Observables {

    String  _mainOntology           = "im";
    String  _coreOntology           = "im.core";
    String  _mainNamespacePrefix    = "im.";
    boolean _skipGenerated          = true;
    boolean _includeNonCoreConcepts = false;

    public void includeNonCoreConcepts(boolean b) {
        _includeNonCoreConcepts = b;
    }

    public void skipGenerated(boolean b) {
        _skipGenerated = b;
    }

    /**
     * TODO this should better take a regexp, but I have exactly three seconds left and I'm not
     * going to need it anyway.
     * 
     * @param main
     * @param core
     * @param prefix
     */
    public void setCoreOntologies(String main, String core, String prefix) {
        _mainOntology = main;
        _coreOntology = core;
        _mainNamespacePrefix = prefix;
    }

    abstract class ObservableFilter implements Filter<IKnowledge> {

        @Override
        public final boolean match(IKnowledge o) {

            boolean ok = true;
            if (!_includeNonCoreConcepts && (_mainOntology != null && _mainNamespacePrefix != null)) {
                ok = o.getConceptSpace().equals(_mainOntology)
                        || o.getConceptSpace().startsWith(_mainNamespacePrefix);
            }
            if (ok && _skipGenerated) {
                ok = !NS.isDerived(o);
            }
            return ok ? select(o) : false;
        }

        public abstract boolean select(IKnowledge o);

    }

    private Collection<IKnowledge> filter(Filter<IKnowledge> filter) {
        Collection<IKnowledge> ret = new ArrayList<>();
        for (IOntology o : (KLAB.KM).getOntologies(false)) {
            for (IConcept c : ((Ontology) o).getTopConcepts(true)) {
                if (filter.match(c))
                    ret.add(c);
            }
        }
        return ret;
    }

    public Collection<IKnowledge> getAllFundamentalQualities() {
        ArrayList<IKnowledge> ret = new ArrayList<>();
        IOntology o = (KLAB.KM).getOntology(_coreOntology);
        if (o != null) {
            for (IConcept c : ((Ontology) o).getTopConcepts(true)) {
                if (NS.isQuality(c)) {
                    ret.add(c);
                }
            }
        }
        return ret;
    }

    public Collection<IKnowledge> getAllPhysicalProperties() {
        return filter(new ObservableFilter() {
            @Override
            public boolean select(IKnowledge o) {
                return o.is(KLAB.c(NS.CORE_PHYSICAL_PROPERTY));
            }
        });
    }

    public Collection<IKnowledge> getAllFundamentalIdentities() {
        return filter(getAllFundamentalIdentitiesFilter());
    }

    public Collection<IKnowledge> getAllFundamentalSubjects() {
        return filter(getAllFundamentalSubjectsFilter());
    }

    public Collection<IKnowledge> getAllFundamentalAttributes() {
        return filter(getAllFundamentalAttributesFilter());
    }

    public Collection<IKnowledge> getAllFundamentalRealms() {
        return filter(getAllFundamentalRealmsFilter());
    }

    public Collection<IKnowledge> getAllRootProcesses() {
        return filter(getAllRootProcessesFilter());
    }

    public Collection<IKnowledge> getAllRootEvents() {
        return filter(getAllRootEventsFilter());
    }

    public Filter<IKnowledge> getAllFundamentalQualitiesFilter() {
        return new ObservableFilter() {
            @Override
            public boolean select(IKnowledge o) {
                return NS.isQuality(o);
            }
        };
    }

    public Filter<IKnowledge> getAllFundamentalSubjectsFilter() {
        return new ObservableFilter() {
            @Override
            public boolean select(IKnowledge o) {
                return NS.isThing(o);
            }
        };
    }

    public Filter<IKnowledge> getAllFundamentalAttributesFilter() {
        return new ObservableFilter() {
            @Override
            public boolean select(IKnowledge o) {
                return o.is(KLAB.c(NS.ATTRIBUTE_TRAIT)) && !NS.isObservable(o);
            }
        };
    }

    public Filter<IKnowledge> getAllFundamentalIdentitiesFilter() {
        return new ObservableFilter() {
            @Override
            public boolean select(IKnowledge o) {
                return o.is(KLAB.c(NS.CORE_IDENTITY_TRAIT)) && !NS.isObservable(o);
            }
        };
    }

    public Filter<IKnowledge> getAllFundamentalRealmsFilter() {
        return new ObservableFilter() {
            @Override
            public boolean select(IKnowledge o) {
                return o.is(KLAB.c(NS.CORE_REALM_TRAIT)) && !NS.isObservable(o);
            }
        };
    }

    public Filter<IKnowledge> getAllRootProcessesFilter() {
        return new ObservableFilter() {
            @Override
            public boolean select(IKnowledge o) {
                return NS.isProcess(o);
            }
        };
    }

    public Filter<IKnowledge> getAllRootEventsFilter() {
        return new ObservableFilter() {
            @Override
            public boolean select(IKnowledge o) {
                return NS.isEvent(o);
            }
        };
    }

    public Filter<IKnowledge> getAllUserObservableEvents(IContext context) {
        // TODO Auto-generated method stub
        return null;
    }

    public Filter<IKnowledge> getAllUserObservableProcesses(IContext context) {
        // TODO Auto-generated method stub
        return null;
    }

    public Filter<IKnowledge> getAllUserObservableQualities(IContext context) {
        // TODO Auto-generated method stub
        return null;
    }

    public Filter<IKnowledge> getAllUserObservableSubjects(IContext context) {
        // TODO Auto-generated method stub
        return null;
    }

    public Filter<IKnowledge> getAllUserObservableTraits(IContext context) {
        // TODO Auto-generated method stub
        return null;
    }

}
