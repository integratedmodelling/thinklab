/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.common.data;

import java.io.File;

import org.apache.commons.io.FilenameUtils;
import org.integratedmodelling.api.data.IAggregator;
import org.integratedmodelling.api.data.ITableSet;

public class TableFactory {

    public static class MeanAggregator implements IAggregator {

        @Override
        public Object aggregate(Iterable<Object> objects) {

            double sum = 0.0;
            double n = 0;

            for (Object o : objects) {
                sum += toDouble(o);
                n += 1.0;
            }

            return sum / n;
        }

    }

    public static class SumAggregator implements IAggregator {

        @Override
        public Object aggregate(Iterable<Object> objects) {

            double sum = 0.0;
            for (Object o : objects) {
                sum += toDouble(o);
            }
            return sum;
        }

    }

    public static class MinAggregator implements IAggregator {

        @Override
        public Object aggregate(Iterable<Object> objects) {

            double min = Double.NaN;

            for (Object o : objects) {
                double d = toDouble(o);
                if (!Double.isNaN(d) && (Double.isNaN(min) || d < min)) {
                    min = d;
                }
            }

            return min;
        }
    }

    public static class MaxAggregator implements IAggregator {

        @Override
        public Object aggregate(Iterable<Object> objects) {

            double max = Double.NaN;

            for (Object o : objects) {
                double d = toDouble(o);
                if (!Double.isNaN(d) && (Double.isNaN(max) || d > max)) {
                    max = d;
                }
            }

            return max;
        }
    }

    public static ITableSet open(File file) {

        String ext = FilenameUtils.getExtension(file.toString());

        if (ext.equals("xls") || ext.equals("xlsx") || ext.equals("csv")) {
            return new ExcelTableSet(file);
        } else if (ext.equals("mdb")) {
            return new AccessTableSet(file);
        }

        return null;

    }

    public static double toDouble(Object o) {

        double ret = Double.NaN;
        if (o instanceof String) {
            ret = Double.parseDouble(o.toString());
        } else if (o instanceof Number) {
            ret = ((Number) o).doubleValue();
        }
        return ret;
    }

    public static IAggregator getAggregator(String operation) {
        if (operation.equals("mean")) {
            return new MeanAggregator();
        } else if (operation.equals("min")) {
            return new MinAggregator();
        } else if (operation.equals("max")) {
            return new MaxAggregator();
        } else if (operation.equals("sum")) {
            return new SumAggregator();
        }

        return null;
    }

}
