/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.common.data;

import java.io.File;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import org.integratedmodelling.common.configuration.KLAB;
import org.integratedmodelling.common.utils.NumberUtils;
import org.mapdb.DB;
import org.mapdb.DBMaker;

/**
 * Not an abstract class at all, but a common ancestor for the various ITableSet implementations,
 * holding the methods to persist a MapDB column to column mapping and the timestamp of source
 * files to check for the need to cache again.
 * 
 * @author ferdinando.villa
 *
 */
public class AbstractTableSet {

    static DB                                       db;
    private static Map<String, Long>                timestamps;
    private static Map<String, String>              mappings;

    private static Map<String, Map<String, Object>> cache      = new HashMap<>();

    /**
     * true if the cache did not exist before. Nothing is done - actions
     * are left to child classes.
     */
    protected boolean                               isNew      = false;

    /**
     * true if cache exists but file has a newer timestamp. Nothing is done - actions
     * are left to child classes.
     */
    protected boolean                               isModified = false;

    public AbstractTableSet(File file) {

        if (db == null) {
            db = DBMaker
                    .newFileDB(new File(KLAB.CONFIG.getDataPath("tablesets") + File.separator
                            + "tabledata"))
                    .closeOnJvmShutdown()
                    .make();

            timestamps = db.getTreeMap("timestamps");
            mappings = db.getTreeMap("tableids");

            Runtime.getRuntime().addShutdownHook(new Thread() {
                @Override
                public void run() {
                    // NPE - hm.
                    // db.close();
                }
            });
        }

        long timestamp = file.lastModified();
        if (timestamps.get(file.toString()) == null) {
            timestamps.put(file.toString(), timestamp);
            isNew = true;
        } else {
            if (timestamp > timestamps.get(file.toString())) {
                isModified = true;
                timestamps.put(file.toString(), timestamp);
            }
        }
    }

    public boolean hasMappingFor(String tableId, String keyId, String valueId) {
        String key = tableId + "|" + keyId + "|" + valueId;
        if (cache.get(key) != null) {
            return true;
        }
        return mappings.get(key) != null;
    }

    public static String sanitizeKey(Object o) {
        /*
         * TODO/CHECK: won't work with bigints - big deal.
         */
        if (o instanceof Number && NumberUtils.isInteger((Number) o)) {
            return "" + ((Number) o).longValue();
        }
        return o.toString();
    }

    public Map<String, Object> getMappingFor(String tableId, String keyId, String valueId) {

        String mapid = null;
        String key = tableId + "|" + keyId + "|" + valueId;

        if (cache.containsKey(key)) {
            return cache.get(key);
        }

        if (mappings.get(key) == null) {
            mapid = UUID.randomUUID().toString();
            mappings.put(key, mapid);
        } else {
            mapid = mappings.get(key);
        }

        Map<String, Object> ret = db.getTreeMap(mapid);
        db.commit();

        cache.put(key, ret);
        return ret;

    }
}
