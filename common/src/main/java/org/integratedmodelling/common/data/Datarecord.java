/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.common.data;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import org.integratedmodelling.api.data.IDataAsset;
import org.integratedmodelling.lang.IRemoteSerializable;

/**
 * Used to store data source access details at the server side, and
 * cached at the client side. Allows using a simple URI to refer to
 * a data source without having to consider the type and protocol of
 * access, and allowing access control at the server side (all sources
 * can only be cached at the client side if they are accessible to the
 * client user).
 * 
 * Simple and inelegant POD object whose purpose is being serialized and
 * used as is.
 * 
 * Bears much relationship with the LSID concept, but no need to
 * adopt a more complex and deprecated standard right now.
 * 
 * @author Ferd
 *
 */
public class Datarecord implements IDataAsset, Serializable, IRemoteSerializable {

    long                           id;

    private static final long      serialVersionUID = -2757179569524317851L;

    /**
     * Main identifier. Doesn't have to be a real URN - for now anything
     * will do as long as it's unique.
     * 
     * Suggested (current) structure: server ID : data path
     */
    public String                  urn;

    /**
     * See NS for vocabulary. Just used to validate the
     * source type when connected to a data source; using 
     * wcs/wfs/raster/vector for now, same name as functions, or
     * 'inline' for direct values.
     */
    public String                  type;

    /**
     * if not empty, the asset is available only to the indicated groups
     */
    public ArrayList<String>       allowedGroups    = new ArrayList<>();

    /**
     * if not empty, the asset is available only to users with the indicated
     * roles.
     */
    public ArrayList<String>       allowedRoles     = new ArrayList<>();

    public HashMap<String, String> attributes       = new HashMap<String, String>();

    // do NOT remove
    public Datarecord() {
    }

    public Datarecord(Map<?, ?> map) {

        for (Object o : map.keySet()) {
            if (o.toString().equals("urn")) {
                this.urn = map.get(o).toString();
            } else if (o.toString().equals("type")) {
                this.type = map.get(o).toString();
            } else {

                /*
                 * hacks - remove when working
                 */
                String key = o.toString();
                String val = map.get(o).toString();
                if (key.equals("url") && val.contains("UNKNOWN_ENDPOINT")) {
                    val = val.replace("UNKNOWN_ENDPOINT", "wcs");
                } else if (key.equals("namespace") && val.equals("geography")) {
                    val = "global-geography";
                }

                this.attributes.put(key, val);

            }
        }
    }

    @Override
    public String getUrn() {
        return urn;
    }

    @Override
    public String getType() {
        return type;
    }

    @Override
    public String getAttribute(String key) {
        return attributes.get(key);
    }

    @Override
    public String toString() {
        String ret = urn + " [" + type + "]";
        for (String s : attributes.keySet()) {
            ret += "\n  " + s + " = " + attributes.get(s);
        }
        return ret;
    }

    @Override
    public Object adapt() {
        HashMap<String, Object> ret = new HashMap<>();
        ret.put("urn", urn);
        ret.put("type", type);
        ret.putAll(attributes);
        return ret;
    }

}
