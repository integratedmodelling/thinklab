/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.common.data;

import java.util.Map;

import org.integratedmodelling.api.data.IRankingScale;
import org.integratedmodelling.api.modelling.IValueMediator;
import org.integratedmodelling.collections.Pair;
import org.integratedmodelling.common.utils.MapUtils;
import org.integratedmodelling.exceptions.ThinklabRuntimeException;
import org.integratedmodelling.lang.IRemoteSerializable;

/**
 * A simple object that allows numeric ranking scales to be defined and in some cases
 * mediated.
 * 
 * @author Ferd
 *
 */
public class RankingScale implements IRankingScale, IRemoteSerializable {

    Number _lowerBound = null;
    Number _upperBound = null;

    boolean _integerScale = false;
    boolean _bounded      = false;

    /**
     * Unbounded ranking, basically a no-op to filter values through.
     */
    public RankingScale() {
    }

    @Override
    public String toString() {
        if (_lowerBound == null && _upperBound == null) {
            return "";
        }
        return (_lowerBound == null ? " " : _lowerBound.toString()) + " - "
                + (_upperBound == null ? " " : _upperBound.toString());
    }

    /**
     * Full specification - can be unbounded, partially bounded of fully bounded.
     * 
     * @param from
     * @param to
     */
    public RankingScale(Number from, Number to) {
        // TODO Auto-generated constructor stub
        _lowerBound = from;
        _upperBound = to;
        _integerScale = ((from instanceof Integer || from instanceof Long)
                && (to instanceof Integer || to instanceof Long));
        _bounded = _lowerBound != null && _upperBound != null && !checkInfinity(_lowerBound)
                && !checkInfinity(_upperBound);
    }

    private boolean checkInfinity(Number n) {

        if (n instanceof Double && (Double.isInfinite(n.doubleValue()) || Double.isNaN(n.doubleValue())))
            return true;

        if (n instanceof Float && (Float.isInfinite(n.floatValue()) || Float.isNaN(n.floatValue())))
            return true;

        return false;
    }

    @Override
    public Pair<Number, Number> getRange() {
        return new Pair<Number, Number>(_lowerBound, _upperBound);
    }

    /**
     * Convert passed value in passed scale to our own scale and number 
     * representation. If anyone is unbounded or the passed scale is
     * null, shut up and just return the value as passed.
     * 
     * @param d
     * @param scale
     * @return
     */
    @Override
    public Number convert(Number d, IValueMediator scale) {

        if (!(scale instanceof IRankingScale)) {
            throw new ThinklabRuntimeException("illegal conversion in ranking: " + scale);
        }

        if (scale != null && _bounded && ((RankingScale) scale)._bounded) {

            double conversion = (_upperBound.doubleValue() - _lowerBound.doubleValue())
                    / (((RankingScale) scale)._upperBound.doubleValue() - ((RankingScale) scale)._lowerBound
                            .doubleValue());
            d = _lowerBound.doubleValue() + (d.doubleValue() * conversion);
            if (_integerScale) {
                d = new Integer((int) Math.rint(d.doubleValue()));
            }
        }

        return d;
    }

    @Override
    public boolean isBounded() {
        return _bounded;
    }

    @Override
    public boolean isInteger() {
        return _integerScale;
    }

    public RankingScale(Map<?, ?> map) {
        _bounded = map.get("bounded?").equals("true");
        _integerScale = map.get("bounded?").equals("true");
        _lowerBound = (Number) map.get("lower");
        _upperBound = (Number) map.get("upper");
    }

    @Override
    public Object adapt() {
        return MapUtils
                .of("bounded?", _bounded, "integer?", _integerScale, "lower", _lowerBound, "upper", _upperBound);
    }

    @Override
    public boolean isCompatible(IValueMediator other) {
        return other instanceof RankingScale &&
                ((RankingScale) other).canMediate(this);
    }

    private boolean canMediate(RankingScale rankingScale) {
        return _bounded && rankingScale._bounded;
    }
}
