/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.common.controller;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.integratedmodelling.api.modelling.IDirectObservation;
import org.integratedmodelling.api.modelling.IEvent;
import org.integratedmodelling.api.modelling.IObservation;
import org.integratedmodelling.api.modelling.IRelationship;
import org.integratedmodelling.api.modelling.IScale;
import org.integratedmodelling.api.modelling.IState;
import org.integratedmodelling.api.modelling.ISubject;
import org.integratedmodelling.api.modelling.scheduling.ITransition;
import org.integratedmodelling.api.runtime.IContext;
import org.integratedmodelling.api.runtime.IMVCController;
import org.integratedmodelling.api.space.ISpatialExtent;
import org.integratedmodelling.api.time.ITemporalExtent;
import org.integratedmodelling.collections.Pair;
import org.integratedmodelling.common.Resources;
import org.integratedmodelling.common.geospace.SpatialServices;
import org.integratedmodelling.common.space.SpaceLocator;
import org.integratedmodelling.common.time.TimeLocator;
import org.integratedmodelling.common.utils.BrowserUtils;

import com.vividsolutions.jts.geom.Coordinate;

/**
 * A controller specialized for the JS browser side that comes within resources/datamapper.
 * @author ferdinando.villa
 *
 */
public class ContextViewer extends JSONRestController {

    /*
     * initial location of map window if no geolocation is available
     */
    public static final double DEFAULT_LAT = 34.297256;
    public static final double DEFAULT_LON = 27.579316;

    class ContextView {

        /*
         * observations in view
         */
        IContext           context;
        // observation in focus (subject or relationship) starts at root but may change. The
        // states, events etc are relative to the focal observation.
        IDirectObservation focus;
        // state at top of visualization stack if any - the one we explore with the info tools.
        IState             state;

        // space and time currently in focus.
        ISpatialExtent  space;
        ITemporalExtent time;

        // WKT of current focus. Can be a point, polygon or line, or nothing.
        String shape;

        // paths of states shown, relative to current focus.
        List<String> statesShown = new ArrayList<>();

        // if not null, we're looking at a particular time. Should not be null unless
        // in non-temporal contexts.
        TimeLocator  timeLocator;
        // if not null, we're looking at a subset - likely a point - of the context's space.
        SpaceLocator spaceLocator;

        /**
         * whether the outline or point (if spatial) of the focal observation is shown.
         */
        boolean focusShown = false;

        /**
         * The stack of maps being shown at the moment. Changed by showMap() and reset
         * when the focus is set.
         */
        List<String> mapURLs    = new ArrayList<>();
        List<String> mapIDs     = new ArrayList<>();
        List<String> featureIDs = new ArrayList<>();

        /**
         * Timeplots shown in this view.
         */
        List<List<Double>> timeplots = new ArrayList<>();

        /*
         * these are all the spatial locations we explored so far - kept so that we can
         * show them alongside the current one in a plot or table.
         */
        List<SpaceLocator> spaceExplored = new ArrayList<>();

        ContextView(IContext ctx) {

            this.context = ctx;
            this.space = context.getSubject().getScale().getSpace();
            this.time = context.getSubject().getScale().getTime();

            if (this.space != null) {
                this.shape = getShapeWKT(space.getShape().toString());
            }

            setFocus(context.getSubject());
        }

        public void setFocus(IDirectObservation observation) {

            focusShown = true;

            if (observation == null) {
                this.focus = this.context.getSubject();
            } else {
                this.focus = observation;
            }

            mapURLs.clear();
            mapIDs.clear();
            featureIDs.clear();
            timeplots.clear();

            display();
        }

        public Pair<List<String>, List<String>> showMap(IState observation, boolean activated, boolean refocus) {
            // TODO add/remove map from stack; put on top if focus is requested
            return null;
        }

        public void display() {

            if (focusShown && focus != null) {
                if (space != null) {
                    send("set-space", space.getMinX(), space.getMinY(), space.getMaxX(), space
                            .getMaxY(), shape);
                } else if (time != null) {
                    // TODO set view to the most appropriate display: event/etc? timeplot?
                } else {
                    // TODO
                }
            } else if (focus != null) {
                send("hide-feature", "/");
            }

            /*
             * show states appropriately
             */
            if (statesShown.size() > 0) {

                /*
                 * map
                        for (State state : states) {
                if (state.isSpatiallyDistributed()) {
                String url = state.getMediaURL("image/png", viewport, locators);
                if (xy == null) {
                    xy = viewport
                            .getSizeFor(state.getSpace().getMaxX() - state.getSpace().getMinX(), state
                                    .getSpace().getMaxY() - state.getSpace().getMinY());
                }
                // System.out.println("will ask for " + url + " sized " + xy[0] + "," + xy[1]);
                stateUrls += (stateUrls.isEmpty() ? "" : "@") + url;
                stateNames += (stateNames.isEmpty() ? "" : "@") + state.getId();
                }
                }
                
                send("show-map", stateUrls, stateNames, space.getMinX(), space.getMinY(), space.getMaxX(), space
                .getMaxY(), xy[0], xy[1]);
                 *
                 */

            }

            /*
             * subject and relationship outlines
             */

            /*
             * TODO
             * setup event display if we have any events in this
             * timeline.
             */

        }
    }

    public enum ViewMode {

        MAP("map"),
        FLOW("flow"),
        TIMEPLOT("plot"),
        TIMELINE("timeline"),
        DATATABLE("table");

        private String cmd;

        ViewMode(String s) {
            cmd = s;
        }
    };

    Map<IContext, ContextView> views          = new HashMap<>();
    IContext                   currentContext = null;

    private ContextView view() {

        if (currentContext == null) {
            return null;
        }
        ContextView view = views.get(currentContext);
        if (view == null) {
            view = new ContextView(currentContext);
            views.put(currentContext, view);
        }
        return view;
    }

    public ContextViewer() {
        this(IMVCController.DEFAULT_PORT);
    }

    // @Override
    // protected Object execute(String command, Map<String, String> arguments) {
    //
    // switch (command) {
    // case "on-click":
    // /*
    // * if transect mode, establish transects in current view
    // * else just call listener to report coordinates
    // */
    // break;
    // case "shape-added":
    // /*
    // * if in context edit mode, ingest new shapw
    // */
    // break;
    // }
    // return super.execute(command, arguments);
    // }

    public ContextViewer(int port) {
        super(port == 0 ? IMVCController.DEFAULT_PORT : port, "null");
        setContentType("application/json");
    }

    public void setView(ViewMode mode) {
        send("switch-view", mode.cmd);
    }

    public void setTime(ITransition transition) {
        view().timeLocator = TimeLocator.get(transition);
        view().display();
    }

    /**
     * Sets up the view to show the passed context. If the context has been seen before, set to its
     * previous view, otherwise set the focus to the root context and prepare a new view. Return the
     * ID of the view corresponding to the context.
     * 
     * @param context
     */
    public void showContext(IContext context) {
        currentContext = context;
        view().display();
    }

    /**
     * Set the view so that the passed observation, which must be in the currently viewed context,
     * is either shown or hidden according to the second parameter. The setFocus parameter applies
     * to subjects and relationships only, and makes the observation become the primary focus of the
     * visualization. View is adjusted according to context.
     * 
     * @param observation
     * @param activated
     * @param setFocus
     */
    public void showObservation(IObservation observation, boolean activated, boolean setFocus) {

        if (observation instanceof ISubject || observation instanceof IRelationship) {
            if (setFocus) {
                /*
                 * activate/deactivate new focus context
                 */
                view().setFocus(activated ? (IDirectObservation) observation : null);
                /*
                 * TODO (not here): set focus for drag/drop in context view.
                 */
            } else {
                /*
                 * show feature or marker if spatial
                 */
                if (observation.getScale().getSpace() != null) {

                } else {
                    // TODO
                    // do nothing?
                }
            }
        } else if (observation instanceof IState) {
            /*
             * move to map or timeplot. refocus = bring to top for events to get there.
             */
            if (((IState) observation).isSpatiallyDistributed()) {

                Pair<List<String>, List<String>> shown = view()
                        .showMap((IState) observation, activated, setFocus);

            } else if (((IState) observation).isTemporallyDistributed()
                    && view().timeLocator.getSlice() > 0) {
                /*
                 * plot
                 */
            } else {
                /*
                 * just data
                 */
            }

        } else if (observation instanceof IEvent) {
            /*
             * move to timeline
             */
        }
    }

    public void showStates(List<IState> states, List<IScale.Locator> locators) {

    }

    public void cycleMaps() {
        send("cycle-maps");
    }

    public void drawMode(boolean selection) {
        send("draw-mode", selection ? "on" : "off");
    }

    private String getShapeWKT(String string) {
        if (string.startsWith("EPSG:")) {
            int idx = string.indexOf(' ');
            if (idx > -1) {
                return string.substring(idx);
            }
        }
        return string;
    }

    /**
     * Start the system browser with the visualizer interface in it, listening on the
     * passed port. If port is 0, the default port is used.
     * 
     * @return
     */
    public boolean startVisualizer(int port) {

        if (port == 0) {
            port = IMVCController.DEFAULT_PORT;
        }

        String args = "port=" + port;
        Coordinate location = SpatialServices.geolocate();
        if (location == null) {
            args += "&lat=" + DEFAULT_LAT;
            args += "&lon=" + DEFAULT_LON;
        } else {
            args += "&lat=" + location.y;
            args += "&lon=" + location.x;
        }

        try {
            File f = Resources.getVisualizerPath();
            if (!f.exists()) {
                return false;
            }

            BrowserUtils.startBrowser(f.toURI().toURL() + "/datamapper.html?" + args);
            Thread.sleep(2500);

        } catch (Exception e) {
            return false;
        }

        return true;
    }

}
