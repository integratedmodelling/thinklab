/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.common.controller;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;

import org.integratedmodelling.api.runtime.IMVCController;
import org.integratedmodelling.common.utils.MapUtils;
import org.integratedmodelling.common.utils.NumberUtils;
import org.integratedmodelling.common.utils.StringUtils;

/**
 * Enqueues commands in a queue, "sends" them when the client side requests them from a 
 * polling cycle using the 'next' command, returns client response. Works with polling 
 * client such as the k.LAB context viewer. Has a convenient command line interface that
 * will send test commands.
 * 
 * @author ferdinando.villa
 *
 */
public class JSONRestController extends LightweightRESTServer {

    Queue<Map<?, ?>> outgoing = new ConcurrentLinkedQueue<>();

    public JSONRestController(int port, String origin) {
        super(port, null, origin);
        setController(new IMVCController() {

            @Override
            public Object commandReceived(String command, Map<String, String> arguments) {
                return execute(command, arguments);
            }
        });
    }

    /**
     * Use to send a command to the remote end. Do not use the reserved command _next.
     * 
     * @param cmd
     * @param args
     */
    public void send(String cmd, List<Object> args) {
        outgoing.add(MapUtils.of("id", cmd, "args", args.toArray()));
    }

    public void send(String cmd, Object... args) {
        if (args == null) {
            outgoing.add(MapUtils.of("id", cmd));
        } else {
            outgoing.add(MapUtils.of("id", cmd, "args", args));
        }
    }

    /**
     * Override to handle commands coming from the remote end.
     * 
     * @param command
     * @param arguments
     * @return
     */
    protected Object execute(String command, Map<String, String> arguments) {
        System.out.println("received " + command + " with arguments "
                + StringUtils.joinObjects(arguments, ','));
        return null;
    }

    @Override
    protected String dispatch(String cmd, Map<String, String> args) {
        if (cmd.equals("_next")) {
            return outgoing.size() == 0 ? null : processObject(outgoing.poll());
        }
        return super.dispatch(cmd, args);
    }

    public static void main(String[] args) {

        try {
            final JSONRestController rst = new JSONRestController(IMVCController.DEFAULT_PORT, "null");
            rst.setContentType("application/json");
            rst.start();

            for (;;) {
                BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
                System.out.print("> ");
                String s = br.readLine().trim();
                if (s.equals("exit")) {
                    System.exit(0);
                }

                String[] dio = s.split("\\s+");
                if (dio.length > 0) {
                    String id = dio[0];
                    List<Object> aaa = new ArrayList<>();
                    for (int i = 1; i < dio.length; i++) {
                        if (NumberUtils.isNumber(dio[i])) {
                            aaa.add(Double.parseDouble(dio[i]));
                        } else {
                            aaa.add(dio[i]);
                        }
                    }

                    System.err.println("Sending " + id + " with args " + StringUtils.join(aaa, ", "));
                    rst.send(id, aaa);
                }

            }

        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

}
