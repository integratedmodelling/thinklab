/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.common.controller;

import java.util.HashMap;
import java.util.Map;

import org.integratedmodelling.api.runtime.IMVCController;
import org.integratedmodelling.exceptions.ThinklabException;

import jodd.json.JsonSerializer;

/**
 * A tiny REST server that listens for user events from an independent GUI - like a GWT
 * client running on the same machine. Implements the calls defined in the IController
 * interface and uses a passed IController as the executor.
 * 
 * @author Ferd
 *
 */
public class LightweightRESTServer {

    int                            _port;
    IMVCController                 _controller;
    private String                 _hostUrl;
    private String                 _contentType;
    protected IceBreakerRESTServer _rest;
    private JsonSerializer         _jsonSerializer;

    public LightweightRESTServer(int port, IMVCController controller) {
        _port = port;
        _controller = controller;
    }

    public LightweightRESTServer(int port, IMVCController controller, String origin) {
        this(port, controller);
        _hostUrl = origin;
    }

    public LightweightRESTServer setContentType(String ctype) {
        _contentType = ctype;
        return this;
    }

    protected void setController(IMVCController controller) {
        _controller = controller;
    }

    public void start() throws ThinklabException {

        new Thread() {

            @Override
            public void run() {

                try {
                    _rest = new IceBreakerRESTServer();
                    _rest.setPort(_port);
                    if (_hostUrl != null) {
                        _rest.setAllowedHost(_hostUrl);
                    }
                    if (_contentType != null) {
                        _rest.setContentType(_contentType);
                    }

                    while (_rest != null) {

                        _rest.getHttpRequest();

                        String cmd = _rest.getQuery("cmd");
                        Map<String, String> args = new HashMap<>();

                        int n = 0;
                        for (String key : _rest.getResponseData().keySet()) {
                            if (key.equals("cmd")) {
                                continue;
                            }
                            args.put(key, _rest.getQuery(key));
                        }

                        String response = "";
                        if (cmd != null) {
                            response = dispatch(cmd, args);
                        }

                        _rest.write(response);
                        _rest.flush();
                    }

                } catch (Throwable ex) {
                    // error(ex);
                }
            }

        }.start();
    }

    protected String dispatch(String cmd, Map<String, String> args) {
        /*
         * call method corresponding to cmd on controller. Could use reflection or a
         * switch. For now let's say that we leave complete control to IController.
         */
        return processObject(_controller.commandReceived(cmd, args));
    }

    protected String processObject(Object commandReceived) {
        if (_contentType != null) {
            if (_contentType.startsWith(("application/json"))) {
                return getJsonSerializer().serialize(commandReceived);
            }
        }
        return commandReceived == null ? null : commandReceived.toString();
    }

    private JsonSerializer getJsonSerializer() {
        // TODO Auto-generated method stub
        if (_jsonSerializer == null) {
            _jsonSerializer = new JsonSerializer();
            _jsonSerializer.deep(true);
        }
        return _jsonSerializer;
    }

    protected void error(Throwable ex) {
        System.out.println(ex.getMessage() + ": port = " + _port);
    }

    public void stop() throws ThinklabException {
        if (_rest != null) {
            _rest.stop();
            _rest = null;
        }
    }

    // public static void main(String[] args) {
    //
    // try {
    // new LightweightRESTServer(IMVCController.DEFAULT_PORT, new IMVCController() {
    //
    // int n = 0;
    //
    // @Override
    // public Object commandReceived(String command, Map<String, String> arguments) {
    //
    // System.out.println("called " + command);
    // for (String s : arguments.keySet()) {
    // System.out.println(" " + s + " = " + arguments.get(s));
    // }
    // return n++;
    // }
    //
    // }, "http://localhost:63342").setContentType("application/json").start();
    //
    // for (;;) {
    // BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
    // System.out.print("> ");
    // String s = br.readLine().trim();
    // if (s.equals("exit")) {
    // System.exit(0);
    // }
    //
    // String[] dio = s.split("\\s+");
    //
    // }
    //
    // } catch (Exception e) {
    // // TODO Auto-generated catch block
    // e.printStackTrace();
    // }
    // }

    @Override
    protected void finalize() throws Throwable {
        stop();
        super.finalize();
    }

    public void setAllowedDomain(String hostUrl) {
        _hostUrl = hostUrl;
    }

}
