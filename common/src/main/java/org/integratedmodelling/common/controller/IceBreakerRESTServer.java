package org.integratedmodelling.common.controller;

/* ---------------------------------------------------------------------------------------- */
/*                                                                                          */
/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
/*    Licensed under the Apache License, Version 2.0 (the "License");                       */
/*    you may not use this file except in compliance with the License.                      */
/*    You may obtain a copy of the License at                                               */
/*                                                                                          */
/*        http://www.apache.org/licenses/LICENSE-2.0                                        */
/*                                                                                          */
/*    Unless required by applicable law or agreed to in writing, software                   */
/*    distributed under the License is distributed on an "AS IS" BASIS,                     */
/*    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.              */
/*    See the License for the specific language governing permissions and                   */
/*    limitations under the License.                                                        */
/*                                                                                          */
/*    Design - Niels Liisberg                                                               */
/*                                                                                          */
/* ---------------------------------------------------------------------------------------- */

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.URLDecoder;
import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import org.integratedmodelling.common.utils.UrlEscape;

/**
 * Super tiny HTTP serverside protocol for monolitic RESTservice applications
 * Simply drop the IceBreakRestServer jar file in your project ( classpath) and you are golden.
 *
 * A simple server looks like this:
 *
 *
 * <pre>
 * {@code
 * // Drop this jar-file into you project
 * import IceBreakRestServer.*;
 * import java.io.IOException;
 * public class Simple {
 *
 *   public static void main(String[] args) {
 *
 *     // Declare the IceBreak HTTP REST server class
 *     IceBreakRestServer rest;
 *
 *
 *     try {
 *
 *       // Instantiate it once
 *       rest  = new IceBreakRestServer();
 *
 *       while (true) {
 *
 *         // Now wait for any HTTP request
 *         // the "config.properties" file contains the port we are listening on
 *         rest.getHttpRequest();
 *
 *         // If we reach this point, we have received a request
 *         // now we can pull out the parameters from the query-string
 *         // if not found we return the default "N/A"
 *         String name = rest.getQuery("name", "N/A");
 *
 *         // we can now produce the response back to the client.
 *         // That might be XML, HTML, JSON or just plain text like here:
 *         rest.write("Hello world - the 'name' parameter is: " + name );
 *       }
 *     } catch (IOException ex) {
 *       System.out.println(ex.getMessage());
 *     }
 *   }
 * }
 * }
 * </pre>
 * 
 * Ferd: added setAllowedHost
 */

public class IceBreakerRESTServer {

    private ServerSocket  providerSocket = null;
    private Socket        connection     = null;
    private PrintWriter   pw;
    private String        ContentType    = "text/plain; charset=utf-8";
    private String        Status;
    private StringBuilder resp           = new StringBuilder(1024);
    private Boolean       doFlush        = false;
    private int           Port;
    private int           Queue;
    private InputStream   in;

    // TODO make it configurable
    private String _sourceHost = "http://127.0.0.1:8888";

    /** This is the complete querysting including the resource. Just as you write it in your browser - you have to URL decode it or rather use getQuery to get paramter */
    public String  request;
    /** This is the contents sent by a POST  */
    public String  payload;
    /** This is the request type GET, POST, HEAD - your application have to responde coretly to this ( ore simply ignore it */
    public String  method;
    /** This is the complete querysting after the resource as you write it in your browser - you have to URL decode it or rather use getQuery to get paramter */
    public String  queryStr;
    /** This is the name of the resource to run or get i.e. http://x/myApp.aspx/p1=abc it will return /myApp.aspx  */
    public String  resource;
    /** This is the version of the HTTP protocol requested   */
    public String  httpVer;
    /** Set this to true to get some system.out.print */
    public Boolean debug = false;

    /** This is the HTTP headers in the request. Use normal "Map" methods  */
    public Map<String, String> header = new HashMap<String, String>();
    /** This is the HTTP quesystring parameters as map. Use normal "Map" methods or getQuery() method  */
    public Map<String, String> parms  = new HashMap<String, String>();

    private void loadProps() {
        Properties prop = new Properties();

        try {
            // load a properties file
            prop.load(new FileInputStream("config.properties"));
            Port = Integer.parseInt(prop.getProperty("restserver.port", "65000"));
            Queue = Integer.parseInt(prop.getProperty("restserver.queuesize", "10"));
        } catch (IOException ex) {
            // ex.printStackTrace();
        }
    }

    /**
           * Contructor, returns an instance of the rest server
           */
    public IceBreakerRESTServer() {
        loadProps();
    }

    /**
           * Set the contents type of the HTTP contens. It has to conform
     * the mime type. By default it has the value of "text/plain; charset=utf-8"
           * @param contents type string to set
           */
    public void setContentType(String s) {
        ContentType = s;
    }

    /**
           * Set the status of HTTP contens.
     * @see <a href="http://www.w3.org/Protocols/rfc2616/rfc2616-sec10.html">HTTP status codes</a>
           * @param status string . by default the is "200 OK"
           */
    public void setStatus(String s) {
        Status = s;
    }

    /**
           * Set the TCP/IP port that you server is listening on. This is by default port 65000 and you can
     * set this value in the config.prperties file. Or you can set it programatically here but before issuing
     * a "getRequest()".
     * @param port TCP/IP port to listen on
           */
    public void setPort(int port) {
        Port = port;
    }

    /**
           * Set the TCP/IP queue depth for your HTTP server . This is by default port 10 and you can
     * set this value in the config.prperties file. Or you can set it programatically here but before issuing
     * a "getRequest()".
     * @param port TCP/IP port to listen on
           */
    public void setQueue(int queue) {
        Queue = queue;
    }

    /**
           * Returns the parameter from the querystring with the name of "key". if the querystring
     * parameter was not fount it will return the default paramter
     * Note: key is case sensitive!!
     * @param Key - to return value for in the querystring
     * @param Default - when key is not found this wil be the default value
     * @return value of the querystring parameter
           */
    public String getQuery(String Key, String Default) {
        checkPayload();
        String temp = parms.get(Key);
        if (temp == null)
            return Default;
        return temp;
    }

    private void checkPayload() {
        if (parms.isEmpty() && payload != null && payload.length() > 2) {
            /*
             * process any POST request so that we can use the same methods for get/post
             */
            String[] ss = payload.split("&");
            for (String s : ss) {
                String[] ps = s.split("=");
                parms.put(ps[0], UrlEscape.unescapeurl(ps[1]));
            }
        }

    }

    /**
           * Returns the parameter from the querystring with the name of "key". if the querystring
     * parameter was not fount it will <code>null</code>
     * Note: key is case sensitive!!
     * @param Key - to return value for in the querystring
     * @return value of the querystring parameter
           */
    public String getQuery(String Key) {
        checkPayload();
        return parms.get(Key);
    }

    /**
           * Just return a simple string with current timestamp in hh:mm:ss format
     * @return current time in hh:mm:ss format
           */
    public String now() {
        String s;
        Format formatter;
        Date date = new Date();
        formatter = new SimpleDateFormat("hh:mm:ss");
        s = formatter.format(date);
        return s;
    }

    private static Map<String, String> getQueryMap(String query) {
        String[] params = query.split("&");
        Map<String, String> map = new HashMap<String, String>();
        for (String param : params) {
            int p = param.indexOf('=');
            if (p >= 0) {
                String name = param.substring(0, p);
                String value = param.substring(p + 1);
                String s = URLDecoder.decode(value);
                map.put(name, s);
            }
        }
        return map;
    }

    // This handles both windows <CR><LF> and mac/aix/linux <CR>
    // and returns both end of header and end of line sequence
    private int isEol(byte[] buf, int i) {
        if (buf[i] == 0x0d && buf[i + 1] == 0x0a) {
            if (buf[i + 2] == 0x0d && buf[i + 3] == 0x0a) {
                return -4; // End Of header
            }
            return 2;
        }
        if (buf[i] == 0x0d) {
            if (buf[i + 1] == 0x0d) {
                return -2; // End Of header
            }
            return 1;
        }
        if (buf[i] == 0x0a) {
            if (buf[i + 1] == 0x0a) {
                return -2; // End Of header
            }
            return 1;
        }
        return 0;
    }

    private void unpackRequest() throws IOException {

        byte buf[] = new byte[32768];
        in = connection.getInputStream();
        int read = in.read(buf);
        int len = 0, pos = 0, eol = 0;
        header.clear();
        parms.clear();
        request = payload = method = queryStr = httpVer = resource = null;
        for (int i = 0; i < read && eol >= 0; i++) {
            eol = isEol(buf, i);
            if (eol > 0) {

                String dbg = new String(buf, pos, len);
                if (dbg.startsWith("POST")) {
                    eol = isEol(buf, i++);
                    dbg = new String(buf, pos, len);
                }

                // First line is the request. Now parse that partial
                if (request == null) {
                    request = new String(buf, pos, len);
                    String[] temp = request.split(" ");
                    method = temp[0];
                    queryStr = temp[1];
                    httpVer = temp[2];
                    int p = queryStr.indexOf('?');
                    if (p >= 0) {
                        resource = queryStr.substring(0, p);
                        parms = getQueryMap(queryStr.substring(p + 1));
                    } else {
                        resource = queryStr;
                    }
                    // Following lines are the header - put them into a map
                } else {
                    String param = new String(buf, pos, len);
                    int p = param.indexOf(':');
                    String name = param.substring(0, p);
                    String value = param.substring(p + 1);
                    header.put(name, value.trim());
                    // if (name.equals("Access-Control-Request-Headers")) {
                    // System.out.println("ZOYA got " + value.trim());
                    // }
                }
                len = 0;
                pos = i + eol;
                i += eol - 1;
            } else if (eol < 0) {
                pos = i + (-eol);
                payload = new String(buf, pos, read - pos);
            } else {
                len++;
            }
        }

        // this is only for debugging
        if (debug) {
            System.out.println("resource: " + request);
            System.out.println("method: " + method);
            System.out.println("resource: " + resource);
            System.out.println("queryStr: " + queryStr);
            System.out.println("httpVer: " + httpVer);
            System.out.println("header  : " + header);
            System.out.println("parms : " + parms);
            System.out.println("payload  : " + payload);
        }

    }

    private void sendResponse() {

        pw.print("HTTP/1.1 " + Status + "\r\n" + "Connection: Keep-Alive\r\n"
                + (_sourceHost == null ? "" : ("Access-Control-Allow-Origin: *" + /* _sourceHost +*/ "\r\n"))
                + "Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept, Key\r\n"
                + "Accept: multipart/form-data\r\n" + "Accept-Encoding: multipart/form-data\r\n"
                + "Server: IceBreak Java Services\r\n" + "cache-control: no-store\r\n" + "Content-Length: "
                + Integer.toString(resp.length()) + "\r\n" + "Content-Type: " + ContentType + "\r\n" + "\r\n"
                + resp.toString());
        pw.flush();
    }

    /**
     * This waits for the next HTTP request from the client
     *
     */
    public void getHttpRequest() throws IOException {

        if (providerSocket == null) {
            providerSocket = new ServerSocket(Port, Queue);
        }
        if (doFlush)
            flush();

        connection = providerSocket.accept();
        pw = new PrintWriter(connection.getOutputStream());
        resp.setLength(0);
        unpackRequest();
        // ContentType =
        Status = "200 OK";
        doFlush = true;
    }

    /**
           * write a string back to the client. The complete result will be
     * send back to the client when you issue a "flush" or do the next "getHttpRequest()"
     * @param String - to send back to the client
           */
    public void write(String s) {
        resp.append(s);
    }

    public Map<String, String> getResponseData() {
        checkPayload();
        return parms;
    }

    /**
     * send back the complete response to the client. Now we are ready to wait for the next request by issue a "getHttpRequest()"
     */
    public void flush() throws IOException {
        sendResponse();
        connection.close();
        doFlush = false;
    }

    /**
     * If this is set before start() is called, add a Access-Control-Allow-Origin header to each response, so that
     * the host response is accepted if the client implements a same-origin policy. Necessary
     * for most AJAX/GWT applications with servers operating on different ports of same host. 
     * Use appropriately (i.e. only between localhost and itself) - some hurt potential here.
     *  
     * @param hostUrl
     */
    public void setAllowedHost(String hostUrl) {
        _sourceHost = hostUrl;
    }

    public void stop() {
        try {
            connection.close();
            providerSocket.close();
        } catch (Exception e) {

        }
    }
}
