/*******************************************************************************
 *  Copyright (C) 2007, 2014:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.common.visualization;
///**
// * Copyright 2011 The ARIES Consortium (http://www.ariesonline.org) and
// * www.integratedmodelling.org. 
//
//   This file is part of Thinklab.
//
//   Thinklab is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License as published
//   by the Free Software Foundation, either version 3 of the License,
//   or (at your option) any later version.
//
//   Thinklab is distributed in the hope that it will be useful, but
//   WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//   General Public License for more details.
//
//   You should have received a copy of the GNU General Public License
//   along with Thinklab.  If not, see <http://www.gnu.org/licenses/>.
// */
//package org.integratedmodelling.thinklab.common.visualization;
//
//import org.integratedmodelling.exceptions.ThinklabException;
//import org.integratedmodelling.exceptions.ThinklabValidationException;
//
///**
// * Maps concepts to colormaps. The modelling plugin has one of these and an API + commands to access it.
// * 
// * @author Ferdinando
// *
// */
//public class ColormapChooser extends ConfigurableIntelligentMap<ColorMap> {
//
//	public ColormapChooser(String propertyPrefix) {
//		super(propertyPrefix);
//	}
//
//	@Override
//	protected ColorMap getObjectFromPropertyValue(String pvalue, Object[] parameters) throws ThinklabException {
//	
//		if (parameters == null || parameters.length == 0 || !(parameters[0] instanceof Integer)) {
//			throw new ThinklabValidationException("ColormapChooser: must pass the number of levels");
//		}
//		
//		int levels = (Integer)parameters[0];
//		Boolean isz = parameters.length > 1 ? (Boolean)parameters[1] : null;
//		
//		ColorMap ret = null;
//		String[] pdefs = pvalue.split(","); 
//		       
//		for (String pdef : pdefs) {
//			
//			pdef = pdef.trim();
//			String[] ppd = pdef.split("\\s+");
//			
//			String cname = ppd[0] + "(";
//			
//			for (int i = 1; i < ppd.length; i++) {
//				cname += (ppd[i] + (i == (ppd.length-1) ? "" : ","));
//			}
//			cname += ")";
//			
//			ret = ColorMap.getColormap(cname, levels, isz);
//			if (ret != null)
//				break;
//		}
//		       
//		return ret;
//	}
//
//
//}
