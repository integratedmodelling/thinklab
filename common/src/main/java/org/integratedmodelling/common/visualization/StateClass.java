/*******************************************************************************
 *  Copyright (C) 2007, 2014:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.common.visualization;

public class StateClass {

    public static enum Type {
        EMPTY("no data"),
        BOOLEAN("boolean"),
        ENUMERATED("enumerated"),
        CLASSIFICATION_Z("zero-including classification"),
        CLASSIFICATION_NZ("classification"),
        ORDERED_CLASSIFICATION_Z("zero-based ordered classification"),
        ORDERED_CLASSIFICATION_NZ("ordered classification"),
        DISCRETIZED_RANGES("discretized range"),
        QUANTITATIVE_BOUNDED("bounded numeric range"),
        QUANTITATIVE_UNBOUNDED("unbounded numeric range");

        Type(String d) {
            description = d;
        }

        String description;
    }

    public Type    type                = Type.EMPTY;
    public boolean needsZeroInColormap = false;

}
