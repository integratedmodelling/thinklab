/*******************************************************************************
 *  Copyright (C) 2007, 2014:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.common.visualization;

import java.io.Serializable;
import java.util.HashMap;

import org.integratedmodelling.api.lang.IMetadataHolder;
import org.integratedmodelling.api.metadata.IMetadata;
import org.integratedmodelling.common.metadata.Metadata;
import org.jgrapht.DirectedGraph;
import org.jgrapht.graph.DefaultDirectedGraph;
import org.jgrapht.graph.DefaultEdge;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.xml.StaxDriver;

/**
 * Very simple serializable "shuttle" for provenance, workflows, and any other graphs that
 * needs to be visualized across a client/server boundary.
 * 
 * @author Ferd
 *
 */
public class GraphVisualization extends
        DefaultDirectedGraph<GraphVisualization.Node, GraphVisualization.Edge> {

    public Object parent;
    public String name;

    public GraphVisualization() {
        super(Edge.class);
    }

    /**
     * 
     */
    private static final long serialVersionUID = -8914777117876448583L;

    public static interface GraphAdapter<N, E> {

        String getNodeType(N o);

        String getNodeId(N o);

        String getNodeLabel(N o);

        String getNodeDescription(N o);

        String getEdgeType(E o);

        String getEdgeId(E o);

        String getEdgeLabel(E o);

        String getEdgeDescription(E o);
    }

    public static class Node implements IMetadataHolder {

        public String    type;
        public String    id;
        public String    label;
        public String    description;
        public IMetadata metadata;

        @Override
        public boolean equals(Object o) {
            return (type + id).equals(((Node) o).type + ((Node) o).id);
        }

        @Override
        public int hashCode() {
            return (type + id).hashCode();
        }

        @Override
        public IMetadata getMetadata() {
            return metadata == null ? new Metadata() : metadata;
        }
    }

    public class Edge extends DefaultEdge {

        private static final long serialVersionUID = 2885737621388268518L;

        public String             type;
        public String             id;
        public String             label;
        public String             description;

        @Override
        public boolean equals(Object o) {
            return (type + id).equals(((Node) o).type + ((Node) o).id);
        }

        @Override
        public int hashCode() {
            return (type + id).hashCode();
        }

        public Object getTargetNode() {
            return getTarget();
        }

        public Object getSourceNode() {
            return getSource();
        }

    }

    @Override
    public String toString() {
        XStream xstream = new XStream(new StaxDriver());
        return xstream.toXML(this);
    }

    public static GraphVisualization fromString(String xml) {
        XStream xstream = new XStream(new StaxDriver());
        return (GraphVisualization) xstream.fromXML(xml);
    }

    @SuppressWarnings({ "unchecked", "rawtypes" })
    public void adapt(DirectedGraph<?, ? extends org.integratedmodelling.common.data.Edge> graph,
            GraphAdapter adapter) {

        HashMap<Object, Node> nodes = new HashMap<Object, Node>();

        for (Object o : graph.vertexSet()) {

            Node n = new Node();

            n.id = adapter.getNodeId(o);
            n.label = adapter.getNodeLabel(o);
            n.description = adapter.getNodeDescription(o);
            n.type = adapter.getNodeType(o);

            /*
             * transfer all serializable metadata
             */
            if (o instanceof IMetadataHolder) {
                for (String key : ((IMetadataHolder) o).getMetadata().getKeys()) {
                    if (((IMetadataHolder) o).getMetadata().get(key) instanceof Serializable) {
                        n.getMetadata().put(key, ((IMetadataHolder) o).getMetadata().get(key));
                    }
                }
            }

            addVertex(n);
            nodes.put(o, n);
        }

        for (org.integratedmodelling.common.data.Edge edge : graph.edgeSet()) {

            Object s = edge.source();
            Object t = edge.target();

            Edge e = new Edge();
            e.id = adapter.getEdgeId(edge);
            e.label = adapter.getEdgeLabel(edge);
            e.description = adapter.getEdgeDescription(edge);
            e.type = adapter.getEdgeType(edge);

            addEdge(nodes.get(s), nodes.get(t), e);
        }
    }
}
