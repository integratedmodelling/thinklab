/*******************************************************************************
 *  Copyright (C) 2007, 2014:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.common.visualization.knowledge;

import org.integratedmodelling.api.knowledge.IConcept;
import org.integratedmodelling.api.knowledge.IProperty;
import org.jgrapht.graph.DefaultDirectedGraph;
import org.jgrapht.graph.DefaultEdge;

/**
 * A graph centered on an object. Contains the knowledge graph to the requested depth.
 * Instances are accepted as inputs but not shown as part of the graph unless they are
 * directly related to the focus object.
 * 
 * @author Ferd
 *
 */
public class KnowledgeGraph extends DefaultDirectedGraph<Object, KnowledgeGraph.Property> {

    private static final long serialVersionUID = 7774773435217255691L;

    /*
     * pass these to define what edges we want to show.
     */
    public static final int   ISA_PROPERTY     = 0x0001;
    public static final int   EQUIVALENCES     = 0x0002;
    public static final int   RESTRICTIONS     = 0x0004;

    public class Property extends DefaultEdge {

        private static final long serialVersionUID = -389089793793609846L;

        IProperty                 property;
        int                       type;

        public Property(int type) {
            this.type = type;
        }

        public Property(IProperty p) {
            type = RESTRICTIONS;
            property = p;
        }

        @Override
        public String toString() {
            return property == null ? (type == ISA_PROPERTY ? "is" : "=") : property.getLocalName();
        }

        public Object getSourceObject() {
            return this.getSource();
        }

        public Object getTargetObject() {
            return this.getTarget();
        }
    }

    public KnowledgeGraph() {
        super(Property.class);
    }

    public void define(Object root, int depth, int flags) {
        add(root, depth, flags);
    }

    private void add(Object root, int depth, int flags) {

        if (vertexSet().contains(root))
            return;

        addVertex(root);
        addIncoming(root, depth - 1, flags);
        addOutgoing(root, depth - 1, flags);
    }

    private void addIncoming(Object root, int depth, int flags) {

        if (depth < 0)
            return;

        if (root instanceof IConcept) {
            if ((flags & ISA_PROPERTY) != 0) {

                for (IConcept c : ((IConcept) root).getParents()) {
                    add(c, depth - 1, flags);
                    addEdge(c, root, new Property(ISA_PROPERTY));
                }
            }
            if ((flags & EQUIVALENCES) != 0) {

            }
            if ((flags & RESTRICTIONS) != 0) {

            }
        }

    }

    private void addOutgoing(Object root, int depth, int flags) {

        if (depth < 0)
            return;

        if (root instanceof IConcept) {
            if ((flags & ISA_PROPERTY) != 0) {
                for (IConcept c : ((IConcept) root).getChildren()) {
                    add(c, depth - 1, flags);
                    addEdge(root, c, new Property(ISA_PROPERTY));
                }
            }
            if ((flags & EQUIVALENCES) != 0) {

            }
            if ((flags & RESTRICTIONS) != 0) {

            }
        }
    }

}
