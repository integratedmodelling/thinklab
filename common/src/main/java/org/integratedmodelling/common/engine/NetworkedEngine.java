/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.common.engine;

import java.util.HashSet;
import java.util.Set;

import org.integratedmodelling.api.engine.IRemoteEngine;
import org.integratedmodelling.api.project.IProject;
import org.integratedmodelling.api.rest.IRESTController;
import org.integratedmodelling.exceptions.ThinklabException;

/**
 * Represents an engine on the public network. Base class for INode and IPrimaryEngine.
 * 
 * @author ferdinando.villa
 *
 */
public class NetworkedEngine extends Engine implements IRemoteEngine {

    String                name;
    boolean               running    = true;
    protected Set<String> submitters = new HashSet<>();

    /*
     * only use if initialize() is called later.
     */
    protected NetworkedEngine(IRESTController controller) {
        super(controller);
    }

    protected NetworkedEngine(IRESTController controller, String name) {
        super(controller);
        initialize(name);
    }

    protected void initialize(String name) {
        this.name = name;
        try {
            getCapabilities();
        } catch (Throwable t) {
            running = false;
        }
    }

    @Override
    public boolean isRunning() {
        return running;
    }

    @Override
    public IProject importProject(String projectId) throws ThinklabException {
        return null;
    }

    @Override
    public Set<String> getSubmittingGroupSet() {
        return submitters;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    protected void notifyUnresponsiveEngine() throws ThinklabException {
        // do nothing. Won't be called unless we
    }

}
