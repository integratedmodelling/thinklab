/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.common.engine;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.exec.CommandLine;
import org.apache.commons.exec.DefaultExecutor;
import org.apache.commons.exec.ExecuteException;
import org.apache.commons.exec.ExecuteResultHandler;
import org.integratedmodelling.api.configuration.IConfiguration;
import org.integratedmodelling.api.monitoring.IMonitor;
import org.integratedmodelling.api.monitoring.Messages;
import org.integratedmodelling.api.rest.IRESTController;
import org.integratedmodelling.collections.OS;
import org.integratedmodelling.common.configuration.KLAB;
import org.integratedmodelling.common.monitoring.Notification;
import org.integratedmodelling.common.utils.JavaUtils;
import org.integratedmodelling.common.utils.NetUtilities;
import org.integratedmodelling.exceptions.ThinklabException;
import org.integratedmodelling.exceptions.ThinklabIOException;

/**
 * A Thinklab engine running locally whose implementation is synchronized automatically
 * from a remote server distribution.
 * 
 * @author Ferd
 *
 */
public class RemotelySynchronizedLocalEngine extends ModelingEngine {

    boolean                   ok         = true;
    Boolean                   running    = false;
    NetworkedDistribution     distribution;
    HashMap<String, String>   environment;
    private String            contextPath;
    private int               port;
    String                    mainClass  = "org.integratedmodelling.engine.main.RESTServer";

    // this is used to simulate upload/download with no waste of either
    // bandwidth or privacy.
    Map<String, File>         files      = new HashMap<>();

    /*
     * default timeout 1min until we decide that the server didn't start up properly. 
     * FV made it 2mins to accommodate BC3 worst-in-their-class laptops for modelers.
     */
    private static final long TIMEOUT    = 120000;
    private static int        MAXMEM     = 4096;
    private static int        PERMSIZE   = 256;
    private static int        DEBUG_PORT = 8897;

    public RemotelySynchronizedLocalEngine(IRESTController controller,
            NetworkedDistribution serverDistribution, String contextPath,
            int port, IMonitor monitor) {

        super(controller);

        distribution = serverDistribution;
        this.monitor = monitor;
        this.contextPath = contextPath;
        this.port = port;
    }

    @Override
    public boolean isRunning() {
        synchronized (running) {
            return running;
        }
    }

    @Override
    public boolean start() throws ThinklabException {

        if (!distribution.isComplete())
            return false;

        String url = "http://127.0.0.1:" + port + "/" + (contextPath == null ? "" : contextPath);

        if (NetUtilities.urlResponds(url + "/")) {
            monitor.info("connecting to running local engine on port " + port, null);
            synchronized (running) {
                running = true;
            }
            getCapabilities();
            monitor.send(new Notification(Messages.ENGINE_AVAILABLE, null, this));
            return true;
        }

        String ext = "ext/win64";
        if (KLAB.CONFIG.getOS().equals(OS.UNIX)) {
            ext = "ext/linux";
        } else if (KLAB.CONFIG.getOS().equals(OS.MACOS)) {
            ext = "ext/osx";
        }

        CommandLine cmdLine = new CommandLine(JavaUtils.getJavaExecutable());

        for (String s : JavaUtils.getOptions(512, MAXMEM, true, PERMSIZE)) {
            cmdLine.addArgument(s);
        }

        String debug = KLAB.CONFIG.getProperties().getProperty("thinklab.client.debug");
        if (debug != null && debug.equals("on")) {

            /*
             * add debug server parameters
             */
            cmdLine.addArgument("-Xdebug");
            cmdLine.addArgument("-Xbootclasspath/p:lib/jsr166.jar");
            cmdLine.addArgument("-Xrunjdwp:transport=dt_socket,server=y,suspend=n,address=" + DEBUG_PORT);
        }

        cmdLine.addArgument("-Djava.library.path=" + ext);
        cmdLine.addArgument("-cp");
        cmdLine.addArgument("lib/*");
        cmdLine.addArgument("-D" + IConfiguration.THINKLAB_WORK_DIRECTORY_PROPERTY + "="
                + KLAB.CONFIG.getWorkDirectoryName());
        cmdLine.addArgument(mainClass);

        DefaultExecutor executor = new DefaultExecutor();
        executor.setWorkingDirectory(distribution.getServerWorkspace());

        Map<String, String> env = new HashMap<String, String>();
        env.putAll(System.getenv());
        if (System.getProperties().getProperty(KLAB.THINKLAB_JRE_PROPERTY) != null) {
            env.put("JAVA_HOME", System.getProperties().getProperty(KLAB.THINKLAB_JRE_PROPERTY));
        }
        if (environment != null) {
            env.putAll(environment);
        }

        // communicate port and context
        env.put(KLAB.THINKLAB_REST_CONTEXT_PROPERTY, contextPath);
        env.put(KLAB.THINKLAB_REST_PORT_PROPERTY, port + "");

        try {

            KLAB.info("starting remotely synchronized local server from "
                    + distribution.getServerWorkspace());
            KLAB.info("command line is: " + cmdLine.toString());

            monitor.send(new Notification(Messages.ENGINE_BOOTING, null, this));

            /**
             * TODO catch the log
             */
            executor.execute(cmdLine, env, new ExecuteResultHandler() {

                @Override
                public void onProcessFailed(ExecuteException arg0) {
                    monitor.send(new Notification(Messages.ENGINE_EXCEPTION, null, RemotelySynchronizedLocalEngine.this, arg0));
                    synchronized (running) {
                        running = false;
                    }
                    // ok = false;
                }

                @Override
                public void onProcessComplete(int arg0) {
                    synchronized (running) {
                        running = false;
                    }
                    monitor.send(new Notification(Messages.ENGINE_STOPPED, null, this));
                }
            });
        } catch (Exception e) {
            monitor.send(new Notification(Messages.ENGINE_EXCEPTION, null, this, e));
            // synchronized (running) {
            // running = false;
            // }
            ok = false;
        }

        /*
         * wait until server is active before giving control to client
         */
        if (ok) {
            long timeout = 0;
            do {
                try {
                    Thread.sleep(500);
                    timeout += 500;
                } catch (Exception e) {
                }
            } while (!NetUtilities.urlResponds(url + "/") && timeout < TIMEOUT);

            if (NetUtilities.urlResponds(url + "/")) {
                synchronized (running) {
                    running = true;
                }
                getCapabilities();
                monitor.send(new Notification(Messages.ENGINE_AVAILABLE, null, this));
            } else {
                synchronized (running) {
                    running = false;
                }
                ok = false;
                monitor.send(new Notification(Messages.ENGINE_STOPPED, null, this));
            }
        }
        return ok && running;

    }

    // @Override
    // public String upload(File file, ISession session) {
    // String key = UUID.randomUUID().toString();
    // files.put(key + "," + session.getID(), file);
    // return key;
    // }
    //
    // @Override
    // public File download(String key, ISession session) {
    // return files.get(key + "," + session.getID());
    // }

    @Override
    public String getName() {
        // TODO add version
        return "Local engine";
    }

    @Override
    public void setMonitor(IMonitor monitor) {
        this.monitor = monitor;
    }

    @Override
    public void reset() {
        running = false;
        super.reset();
    }

    @Override
    public IMonitor getMonitor() {
        return monitor;
    }

    @Override
    protected void notifyUnresponsiveEngine() throws ThinklabException {
        monitor.send(new Notification(Messages.ENGINE_EXCEPTION, null, this, new ThinklabIOException("engine "
                + getName() + " is not responding: aborting")));
        synchronized (running) {
            running = false;
        }
    }

}
