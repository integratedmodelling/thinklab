/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.common.engine;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.exec.CommandLine;
import org.apache.commons.exec.DefaultExecutor;
import org.apache.commons.exec.ExecuteException;
import org.apache.commons.exec.ExecuteResultHandler;
import org.integratedmodelling.Version;
import org.integratedmodelling.api.configuration.IConfiguration;
import org.integratedmodelling.api.monitoring.IMonitor;
import org.integratedmodelling.api.monitoring.Messages;
import org.integratedmodelling.api.rest.IRESTController;
import org.integratedmodelling.collections.OS;
import org.integratedmodelling.common.configuration.KLAB;
import org.integratedmodelling.common.monitoring.Notification;
import org.integratedmodelling.common.utils.JavaUtils;
import org.integratedmodelling.common.utils.NetUtilities;
import org.integratedmodelling.exceptions.ThinklabException;
import org.integratedmodelling.exceptions.ThinklabIOException;

/**
 * An engine that uses a development Thinklab source configuration. It can be created in all situations, but
 * isAvailable() must be called before dreaming of calling start(), and if that returns false, a 
 * RemotelySynchronizedLocalEngine should be used instead.
 * 
 *  A source configuration consists of:
 * 
 * 	1. current path points to client project in Maven distribution;
 * 	2. all crucial projects (api, client, common, auth, org.integratedmodelling.thinkql, and engine) are available;
 *  3. compiled classes are in target/classed in each project;
 *  4. engine dependency closure (jars) are available in engine-launcher/target/dependency.
 *  
 *  Some (incomplete, minimal) checks are made to be reasonably sure that the distribution is current.
 *  
 * If the configuration is available, the start() and stop() method will start/stop a server instance using the above 
 * classpath.
 * 
 * @author Ferd
 *
 */
public class LocalDevelopmentEngine extends ModelingEngine {

    // paths of class files relative to parent of current dir. Change if project organization changes.
    final static String        ENGINE_DIR                            = "engine/target/classes";
    final static String        API_DIR                               = "api/target/classes";
    final static String        COMMON_DIR                            = "common/target/classes";
    final static String        CLIENT_DIR                            = "client/target/classes";
    final static String        ENGINE_LAUNCHER_DIR                   = "engine-launcher/target/classes";
    final static String        ENGINE_LAUNCHER_ROOT_DIR              = "engine-launcher";

    // location of jar files relative to engine-launcher project. Depends on pom.xml file in it. Change as
    // necessary.
    final static String        LIB_FILES_PATH                        = "engine-launcher/target/dependency";

    private static final long  TIMEOUT                               = 60000;
    public static final String THINKLAB_SOURCE_DISTRIBUTION_PROPERTY = "thinklab.source.distribution";
    public static final String DEFAULT_LOCAL_URL                     = "http://127.0.0.1:8181/rest";
    private static int         MAXMEM                                = 4096;
    private static int         PERMSIZE                              = 256;
    private static int         DEBUG_PORT                            = 8897;

    boolean                    ok                                    = true;
    Boolean                    running                               = false;
    HashMap<String, String>    environment;
    String                     classpath;
    String                     mainClass                             = "org.integratedmodelling.engine.main.RESTServer";
    String                     cpath;
    private String             contextPath;
    private int                port;

    // this is used to simulate upload/download with no waste of either
    // bandwidth or privacy.
    Map<String, File>          files                                 = new HashMap<>();

    public LocalDevelopmentEngine(IRESTController controller, IMonitor monitor, String contextPath, int port) {

        super(controller);

        this.monitor = monitor;
        this.contextPath = contextPath;
        this.port = port;

        String CLASSPATH_SEPARATOR = getClasspathSeparator();
        String cpath;
        try {
            if (System.getProperties().getProperty(THINKLAB_SOURCE_DISTRIBUTION_PROPERTY) != null) {
                cpath = new File(System.getProperties().getProperty(THINKLAB_SOURCE_DISTRIBUTION_PROPERTY))
                        .toString();
            } else {
                cpath = new File(".").getCanonicalFile().getParent().toString();
            }
        } catch (IOException e) {
            ok = false;
            return;
        }

        this.cpath = cpath;

        /**
         * TODO add any wanted component package from preferences
         */
        for (String s : new String[] {
                ENGINE_DIR,
                API_DIR,
                COMMON_DIR,
                ENGINE_LAUNCHER_DIR,
                LIB_FILES_PATH }) {
            File f = new File(cpath + File.separator + s);
            if (!(f.exists() && f.isDirectory())) {
                ok = false;
            }
        }

        if (ok) {
            /*
             * TODO more tests
             */

            /*
             * build classpath with development classes first
             */
            classpath = "";
            for (String s : new String[] { ENGINE_DIR,
                    API_DIR,
                    COMMON_DIR,
                    ENGINE_LAUNCHER_DIR }) {
                classpath += (classpath.isEmpty() ? "" : CLASSPATH_SEPARATOR) + cpath + File.separator + s;
            }

            classpath += CLASSPATH_SEPARATOR + cpath + File.separator + LIB_FILES_PATH + "/*";
        }
    }

    public static boolean isAvailable() {

        boolean ok = true;
        String CLASSPATH_SEPARATOR = getClasspathSeparator();
        String cpath;
        try {
            if (System.getProperties().getProperty(THINKLAB_SOURCE_DISTRIBUTION_PROPERTY) != null) {
                cpath = new File(System.getProperties().getProperty(THINKLAB_SOURCE_DISTRIBUTION_PROPERTY))
                        .toString();
            } else {
                cpath = new File(".").getCanonicalFile().getParent().toString();
            }
        } catch (IOException e) {
            return false;
        }

        /**
         * TODO add any wanted component package from preferences
         */
        for (String s : new String[] {
                ENGINE_DIR,
                API_DIR,
                COMMON_DIR,
                ENGINE_LAUNCHER_DIR,
                LIB_FILES_PATH }) {
            File f = new File(cpath + File.separator + s);
            if (!(f.exists() && f.isDirectory())) {
                ok = false;
            }
        }

        return ok;
    }

    @Override
    public boolean isRunning() {
        synchronized (running) {
            return running;
        }
    }

    @Override
    public boolean start() throws ThinklabException {

        String url = "http://127.0.0.1:" + port + "/" + (contextPath == null ? "" : contextPath);

        if (NetUtilities.urlResponds(url + "/")) {
            monitor.info("connecting to running local engine on port " + port, null);
            synchronized (running) {
                running = true;
            }
            getCapabilities();
            monitor.send(new Notification(Messages.ENGINE_AVAILABLE, null, this));
            return true;
        }

        String ext = "engine-launcher/ext/win64";
        if (KLAB.CONFIG.getOS().equals(OS.UNIX)) {
            ext = "engine-launcher/ext/linux";
        } else if (KLAB.CONFIG.getOS().equals(OS.MACOS)) {
            ext = "engine-launcher/ext/osx";
        }

        CommandLine cmdLine = new CommandLine(JavaUtils.getJavaExecutable());

        for (String s : JavaUtils.getOptions(512, MAXMEM, true, PERMSIZE)) {
            cmdLine.addArgument(s);
        }

        String debug = KLAB.CONFIG.getProperties().getProperty("thinklab.client.debug");
        if (debug != null && debug.equals("on")) {

            /*
             * add debug server parameters
             */
            cmdLine.addArgument("-Xdebug");
            cmdLine.addArgument("-Xbootclasspath/p:lib/jsr166.jar");
            cmdLine.addArgument("-Xrunjdwp:transport=dt_socket,server=y,suspend=n,address=" + DEBUG_PORT);
        }

        System.out.println("-Djava.library.path=" + cpath + "/" + ext);

        cmdLine.addArgument("-Djava.library.path=" + cpath + "/" + ext);
        cmdLine.addArgument("-cp");
        cmdLine.addArgument(classpath);
        cmdLine.addArgument("-D" + IConfiguration.THINKLAB_WORK_DIRECTORY_PROPERTY + "="
                + KLAB.CONFIG.getWorkDirectoryName());

        // publish path so the engine can get components from here
        cmdLine.addArgument("-Dthinklab.source.distribution=" + cpath);

        cmdLine.addArgument(mainClass);

        DefaultExecutor executor = new DefaultExecutor();
        executor.setWorkingDirectory(new File(cpath + File.separator + ENGINE_LAUNCHER_ROOT_DIR));

        Map<String, String> env = new HashMap<String, String>();
        env.putAll(System.getenv());
        if (environment != null) {
            env.putAll(environment);
        }

        try {

            KLAB.info("starting development server from " + cpath);
            monitor.send(new Notification(Messages.ENGINE_BOOTING, null, this));

            executor.execute(cmdLine, env, new ExecuteResultHandler() {

                @Override
                public void onProcessFailed(ExecuteException arg0) {
                    monitor.send(new Notification(Messages.ENGINE_EXCEPTION, null, LocalDevelopmentEngine.this, arg0));
                    synchronized (running) {
                        running = false;
                    }
                    // ok = false;
                }

                @Override
                public void onProcessComplete(int arg0) {
                    synchronized (running) {
                        running = false;
                    }
                    monitor.send(new Notification(Messages.ENGINE_STOPPED, null, LocalDevelopmentEngine.this));
                }
            });
        } catch (Exception e) {
            monitor.send(new Notification(Messages.ENGINE_EXCEPTION, null, this, e));
            ok = false;
        }

        /*
         * wait until server is active before giving control to client
         */
        if (ok) {
            long timeout = 0;
            do {
                try {
                    Thread.sleep(500);
                    timeout += 500;
                } catch (Exception e) {
                }
            } while (!NetUtilities.urlResponds(url + "/") && timeout < TIMEOUT);

            if (NetUtilities.urlResponds(url + "/")) {
                synchronized (running) {
                    running = true;
                }
                getCapabilities();
                monitor.send(new Notification(Messages.ENGINE_AVAILABLE, null, this));
            } else {
                synchronized (running) {
                    running = false;
                }
                ok = false;
                monitor.send(new Notification(Messages.ENGINE_STOPPED, null, this));
            }
        }
        return ok && running;

    }

    @Override
    public String getName() {
        return "Development engine v" + new Version().toString();
    }

    @Override
    public void reset() {
        running = false;
        super.reset();
    }

    @Override
    protected void notifyUnresponsiveEngine() throws ThinklabException {
        monitor.send(new Notification(Messages.ENGINE_EXCEPTION, null, this, new ThinklabIOException("engine "
                + getName() + " is not responding: aborting")));
        synchronized (running) {
            running = false;
        }
    }

}
