/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.common.engine;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

import org.integratedmodelling.api.auth.IUser;
import org.integratedmodelling.api.engine.IEngine;
import org.integratedmodelling.api.lang.IParseable;
import org.integratedmodelling.api.metadata.IModelMetadata;
import org.integratedmodelling.api.metadata.IObservationMetadata;
import org.integratedmodelling.api.modelling.IDirectObserver;
import org.integratedmodelling.api.modelling.IObservable;
import org.integratedmodelling.api.modelling.resolution.IResolutionContext;
import org.integratedmodelling.api.monitoring.IMonitor;
import org.integratedmodelling.api.monitoring.INotification;
import org.integratedmodelling.api.monitoring.Messages;
import org.integratedmodelling.api.project.IProject;
import org.integratedmodelling.api.rest.IRESTController;
import org.integratedmodelling.api.runtime.ISession;
import org.integratedmodelling.api.runtime.ITask;
import org.integratedmodelling.api.services.IPrototype;
import org.integratedmodelling.common.configuration.KLAB;
import org.integratedmodelling.common.model.runtime.Scale;
import org.integratedmodelling.common.monitoring.Notification;
import org.integratedmodelling.common.network.Endpoints;
import org.integratedmodelling.common.network.RESTController;
import org.integratedmodelling.common.owl.Knowledge;
import org.integratedmodelling.common.utils.NetUtilities;
import org.integratedmodelling.common.utils.StringUtils;
import org.integratedmodelling.common.vocabulary.ObservationMetadata;
import org.integratedmodelling.exceptions.ThinklabException;

/**
 * Base implementation for all REST-driven engines. Has logics to allow periodic polling, which
 * is only used in the ModelingEngines to avoid excess network traffic, but can be used with
 * others if needed.
 * 
 * @author ferdinando.villa
 *
 */
public abstract class Engine implements IEngine {

    protected IRESTController controller;

    Map<String, Object>               capabilities       = new HashMap<>();
    Map<String, Object>               status             = new HashMap<>();
    protected Map<String, IPrototype> functions          = new HashMap<>();
    protected Map<String, IPrototype> services           = new HashMap<>();
    Set<IProject>                     registeredProjects = new HashSet<>();
    IMonitor                          monitor;

    protected Engine(IRESTController controller) {
        this.controller = controller;
    }

    @Override
    public Object get(String endpoint, Object... args) throws ThinklabException {
        return controller.get(endpoint, args);
    }

    @Override
    public Object post(String endpoint, Object... args) throws ThinklabException {
        return controller.post(endpoint, args);
    }

    @Override
    public IRESTController forNode(String url, String remoteKey) {
        return controller.forNode(url, remoteKey);
    }

    @Override
    public boolean responds() {
        return controller.responds();
    }

    @Override
    public IUser getUser() {
        return controller.getUser();
    }

    @Override
    public String connect() {
        return controller.connect();
    }

    @Override
    public List<IModelMetadata> queryModels(IObservable observable, IResolutionContext context)
            throws ThinklabException {

        List<IModelMetadata> ret = new ArrayList<>();

        /**
         * TODO 
         * pass context type properly, inherent type and detail level.
         */

        Object mdd = post(Endpoints.QUERY_MODELS, "is-instantiator", (context.isForInstantiation() ? "true"
                : "false"), "subject-type", ((Knowledge) context.getSubject().getObservable()
                        .getType()).asText(), "observable-type", ((Knowledge) observable.getType())
                                .asText(), "observation-type", observable
                                        .getObservationType(), "inherent-subject-type", ((Knowledge) context
                                                .getSubject()
                                                .getObservable()
                                                .getType()).asText(), "context-traits", StringUtils
                                                        .joinObjects(context
                                                                .getTraits(), ','), "scenarios", StringUtils
                                                                        .join(context
                                                                                .getScenarios(), ','), "scale", Scale
                                                                                        .asString(context
                                                                                                .getScale()), "criteria", context
                                                                                                        .getPrioritizer()
                                                                                                        .asText());

        if (mdd instanceof Collection<?>) {
            for (Object md : ((Collection<?>) mdd)) {
                if (md instanceof IModelMetadata) {
                    ret.add((IModelMetadata) md);
                }
            }
        }
        return ret;
    }

    @Override
    public String getUrl() {
        return controller.getUrl();
    }

    @Override
    public boolean providesComponent(String id) {
        // only INodes "provide" components.
        return false;
    }

    @Override
    public ITask setupComponent(String componentId) throws ThinklabException {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public String submitObservation(IDirectObserver observer, boolean store) throws ThinklabException {
        String scale = Scale.asString(observer.getCoverage());
        return post(Endpoints.SUBMIT_OBSERVATION, "type", ((IParseable) observer.getObservable().getType())
                .asText(), "scale", scale, "id", observer.getId(), "store", (store ? "true" : "false"))
                        .toString();
    }

    @Override
    public List<IObservationMetadata> queryObservations(String text, boolean localOnly)
            throws ThinklabException {
        List<IObservationMetadata> ret = new ArrayList<>();
        Object r = get(Endpoints.QUERY_OBSERVATIONS, "query-string", text, "local-only", (localOnly ? "true"
                : "false"));
        if (r instanceof List<?>) {
            for (Object o : ((List<?>) r)) {
                if (o instanceof ObservationMetadata) {
                    ret.add((ObservationMetadata) o);
                }
            }
        }
        return ret;
    }

    @Override
    public void removeObservations(Collection<String> observationNames)
            throws ThinklabException {
        post(Endpoints.REMOVE_OBSERVATIONS, "observation-names", StringUtils.join(observationNames, ","));
    }

    @Override
    public IDirectObserver retrieveObservation(String observationId, String nodeId)
            throws ThinklabException {

        IRESTController source = controller;
        if (!nodeId.equals(KLAB.NAME)) {
            source = KLAB.NETWORK.getNode(nodeId);
        }
        if (source != null) {
            Object r = source.get(Endpoints.RETRIEVE_OBSERVATION, "observation-name", observationId);
            if (r instanceof IObservationMetadata) {
                return ((IObservationMetadata) r).getSubjectObserver();
            }
        }
        return null;
    }

    /*
     * call after establishing connection. Fills in the prototype list and the
     * rest.
     */
    public void getCapabilities() throws ThinklabException {

        Map<?, ?> cap = (Map<?, ?>) get(Endpoints.CAPABILITIES);
        for (Object o : cap.keySet()) {
            switch (o.toString()) {
            case "functions":
                for (Object p : ((List<?>) (cap.get(o)))) {
                    functions.put(((IPrototype) p).getId(), (IPrototype) p);
                }
                break;
            case "services":
                for (Object p : ((List<?>) (cap.get(o)))) {
                    services.put(((IPrototype) p).getId(), (IPrototype) p);
                }
                break;
            default:
                capabilities.put(o.toString(), cap.get(o));
            }
        }
    }

    @Override
    public Collection<IPrototype> getServices() {
        return services.values();
    }

    @Override
    public Collection<IPrototype> getFunctions() {
        return functions.values();
    }

    @Override
    public IPrototype getServicePrototype(String id) {
        return services.get(id);
    }

    @Override
    public boolean providesService(String id) {
        return services.containsKey(id);
    }

    @Override
    public IPrototype getFunctionPrototype(String id) {
        return functions.get(id);
    }

    @Override
    public Map<String, Object> getStatus() {
        synchronized (status) {
            return new HashMap<String, Object>(status);
        }
    }

    /**
     * Inelegant, so we don't float it to the API and it may disappear if I find
     * a better way. Called to initialize all components after all projects have
     * been registered (but not loaded). Re-reads capabilities after that is
     * done.
     * 
     * @throws ThinklabException
     */
    public void initializeComponents() throws ThinklabException {
        // FIXME check endpoint; use subcommand properly
        get(Endpoints.PROJECT, "cmd", "init-components");
        getCapabilities();
    }

    /*
     * call after stopping the engine
     */
    public void reset() {
        registeredProjects.clear();
        ((RESTController) controller).resetSession();
    }

    /*
     * --------------------------------------------------------------------
     * monitoring
     * --------------------------------------------------------------------
     */

    @Override
    public void setMonitor(IMonitor monitor) {
        this.monitor = monitor;
    }

    @Override
    public IMonitor getMonitor() {
        return monitor;
    }

    /*
     * callback used if polling is active and server does not respond for
     * MAX_ERRORS_BEFORE_WARNING polling cycles.
     */
    protected abstract void notifyUnresponsiveEngine() throws ThinklabException;

    ScheduledExecutorService   executor                    = null;
    ScheduledFuture<?>         future                      = null;
    volatile Boolean           _monitoring                 = false;
    int                        _errorcount                 = 0;
    static int                 MAX_ERRORS_BEFORE_WARNING   = 30;
    static long                POLLING_PERIOD_MILLISECONDS = 1500;
    public static final String CACHE_ID                    = "project-cache";

    class PollingService implements Runnable {

        ISession session;

        public PollingService(ISession session) {
            this.session = session;
        }

        @Override
        public void run() {
            synchronized (_monitoring) {
                try {
                    monitor(session);
                } catch (Throwable e) {
                    getMonitor().error(e);
                }
            }
        }
    }

    @SuppressWarnings("unchecked")
    public void monitor(ISession session) throws ThinklabException {

        _monitoring = true;
        if (!NetUtilities.urlResponds(getUrl() + "/")) {
            if (_errorcount++ > MAX_ERRORS_BEFORE_WARNING) {
                getMonitor().warn("engine " + getName() + " is not responding");
                notifyUnresponsiveEngine();
                pausePolling();
                _monitoring = false;
                return;
            }
        }

        Map<?, ?> result = (Map<?, ?>) get(Endpoints.GET_NOTIFICATIONS, "clear", "true");

        if (result == null || result instanceof Throwable) {
            if (_errorcount++ > MAX_ERRORS_BEFORE_WARNING) {
                getMonitor().warn("engine " + getName() + " is not responding");
                notifyUnresponsiveEngine();
                pausePolling();
                _monitoring = false;
                return;
            }
        }

        synchronized (status) {
            status.clear();
            status.putAll((Map<? extends String, ? extends Object>) result);
        }

        for (Object o : (List<?>) result.get("notifications")) {
            getMonitor().send(((INotification) o));
        }

        getMonitor()
                .send(new Notification(Messages.ENGINE_STATUS, null, this, new HashMap<String, Object>(status)));

        _monitoring = false;
    }

    /**
     * Add a monitor for notification of server status and spawn an appropriate
     * monitoring cycle. Monitor will get all notifications sent (as instances
     * of EngineNotification).
     * 
     * @param serverMonitor
     */
    public void startPolling(ISession session) {

        if (future != null) {
            future.cancel(false);
            executor.shutdown();
        }

        executor = Executors.newScheduledThreadPool(24);
        future = executor
                .scheduleWithFixedDelay(new PollingService(session), 0, POLLING_PERIOD_MILLISECONDS, TimeUnit.MILLISECONDS);
    }

    /**
     * Stop the polling.
     */
    public void pausePolling() {

        synchronized (status) {
            status.clear();
        }

        if (future != null) {
            future.cancel(false);
            executor.shutdown();
            future = null;
        }
    }

    @Override
    public void rescanClasspath() {
        // nothing to do for the client.
    }

}
