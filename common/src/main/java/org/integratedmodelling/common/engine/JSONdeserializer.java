/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.common.engine;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Constructor;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import javax.annotation.Nullable;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.integratedmodelling.common.utils.Escape;
import org.integratedmodelling.common.utils.MiscUtilities;
import org.integratedmodelling.exceptions.ThinklabException;
import org.integratedmodelling.exceptions.ThinklabIOException;
import org.integratedmodelling.exceptions.ThinklabInternalErrorException;
import org.integratedmodelling.exceptions.ThinklabRemoteException;
import org.integratedmodelling.lang.IRemoteSerializable;
import org.springframework.web.client.RestTemplate;

import us.monoid.json.JSONArray;
import us.monoid.json.JSONException;
import us.monoid.json.JSONObject;
import us.monoid.web.JSONResource;
import us.monoid.web.Resty;

/**
 * Singleton that handles deserialization of API objects - Jackson-style but with
 * much less configuration and Thinklab-aware. Will automatically create object that
 * have a certain interface as long as the interfaces and the implementing class are
 * registered (register()) at both sides and the receiver has a constructor that takes
 * a Map<?,?>. Easiest way to get the map generated at sender side is to implement 
 * {@link IRemoteSerializable}.
 * 
 * Uses the client JSON implementation and Resty; NOT intended for concurrent use.
 * 
 * @author ferdinando.villa
 */
public class JSONdeserializer {

    static HashMap<String, Class<?>> _implementations = new HashMap<>();
    static Resty                     _resty           = new Resty();

    public JSONdeserializer() {

    }

    /**
     * Helper to build a GET url from arguments. Endpoint may start with a slash or not. Argument
     * list is null-tolerant.
     * 
     * @param serverURL
     * @param endpoint
     * @param arguments
     * @return
     */
    public static String getURL(String service, @Nullable String endpoint, Object... arguments) {

        String url = service + (endpoint == null ? "" : ((endpoint.startsWith("/") ? "" : "/") + endpoint));
        if (arguments != null && arguments.length > 0) {
            for (int i = 0; i < arguments.length; i++) {
                if (arguments[i + 1] == null) {
                    i++;
                    continue;
                }
                url += (i == 0 ? "?" : "&") + Escape.forURL(arguments[i].toString()) + "="
                        + Escape.forURL(arguments[++i].toString());
            }
        }
        return url;
    }

    public static void register(Class<?> interfaceClass, Class<?> implementationClass) {
        _implementations.put(interfaceClass.getCanonicalName(), implementationClass);
    }

    /**
     * Return the deserialized content of the passed JSON resource. Will return
     * an exception object (not throw it) if the server has sent one, or
     * errors happened during transfer or configuration.
     * 
     * @param resource
     * @return
     * @throws ThinklabException if a thinklab exception came through from the remote engine.
     */
    public static Object deserialize(JSONResource resource) throws ThinklabException {

        if (resource == null)
            return null;

        try {
            JSONObject obj = resource.object();
            return deserializeInternal(obj, true);

        } catch (IOException | JSONException e) {
            throw new ThinklabIOException(e);
        }
    }

    /**
     * Return the deserialized content of the passed JSON resource. Will return
     * an exception object (not throw it) if the server has sent one, or
     * errors happened during transfer or configuration.
     * 
     * @param resource
     * @return
     * @throws ThinklabException if a thinklab exception came through from the remote engine.
     */
    public static Object deserializeArray(JSONResource resource) throws ThinklabException {

        if (resource == null)
            return null;

        try {
            JSONArray obj = resource.array();
            return deserializeInternal(obj, true);

        } catch (IOException | JSONException e) {
            throw new ThinklabIOException(e);
        }
    }

    private static Object deserializeInternal(Object obj, boolean honorTypes) throws JSONException,
            ThinklabException {

        if (obj == null)
            return null;

        if (obj instanceof JSONObject) {

            JSONObject jobj = (JSONObject) obj;

            if (!jobj.isNull("_itype")) {

                String itype = jobj.getString("_itype");
                switch (itype) {
                case "_array":
                    return deserializeInternal(jobj.getJSONArray("_ivalue"), true);
                case "_int":
                    return jobj.getInt("_ivalue");
                case "_double":
                    return jobj.getDouble("_ivalue");
                case "_string":
                    return jobj.get("_ivalue").toString();
                case "_null":
                    return null;
                }
            } else if (!jobj.isNull("_otype") && honorTypes) {

                String cclass = jobj.getString("_otype");
                Class<?> impl = _implementations.get(cclass);
                if (impl == null) {
                    throw new ThinklabInternalErrorException("no installed implementation to deserialize "
                            + cclass);
                }

                try {
                    Object ret = null;
                    Constructor<?> constructor = impl.getDeclaredConstructor(Map.class);
                    if (constructor != null) {
                        ret = constructor.newInstance(deserializeInternal(jobj, false));
                    } else {
                        ret = impl.newInstance();
                    }
                    return ret;
                } catch (Exception e) {
                    throw new ThinklabInternalErrorException(ExceptionUtils.getRootCause(e));
                }

            } else if (!jobj.isNull("_exceptionClass")) {

                /*
                 * TODO reconstruct the server exception class instead of using generic
                 * exception - not a priority
                 */
                throw new ThinklabRemoteException(jobj.getString("_exceptionMessage"));

            } else {

                HashMap<String, Object> ret = new HashMap<>();
                for (Iterator<String> it = jobj.keys(); it.hasNext();) {
                    String key = it.next();
                    ret.put(key, deserializeInternal(jobj.get(key), true));
                }
                return ret;
            }

        } else if (obj instanceof JSONArray) {
            ArrayList<Object> ret = new ArrayList<>();
            for (int i = 0; i < ((JSONArray) obj).length(); i++) {
                ret.add(deserializeInternal(((JSONArray) obj).get(i), true));
            }
            return ret;
        }
        return obj;
    }

    public static Object get(String url) throws ThinklabException {
        try {
            return deserialize(_resty.json(url));
        } catch (IOException e) {
            throw new ThinklabIOException(e.getMessage());
        }
    }

    public static Object get(String url, @Nullable Map<?, ?> headers, Object... arguments) throws ThinklabException {

        Resty resty = new Resty();
        if (headers != null) {
            for (Object key : headers.keySet()) {
                resty.withHeader(key.toString(), headers.get(key).toString());
            }
        }

        if (arguments != null) {
            url = getURL(url, null, arguments);
        }

        try {
            return deserialize(resty.json(url));
        } catch (IOException e) {

            /**
             * TODO if this is a 401, we need to communicate authorization failure, not just error.
             */

            throw new ThinklabIOException(e.getMessage());
        }
    }

    public static Object getArray(String url, @Nullable Map<?, ?> headers, Object... arguments) throws ThinklabException {

        Resty resty = new Resty();
        if (headers != null) {
            for (Object key : headers.keySet()) {
                resty.withHeader(key.toString(), headers.get(key).toString());
            }
        }

        if (arguments != null) {
            url = getURL(url, null, arguments);
        }

        try {
            return deserializeArray(resty.json(url));
        } catch (IOException e) {

            /**
             * TODO if this is a 401, we need to communicate authorization failure, not just error.
             */

            throw new ThinklabIOException(e.getMessage());
        }
    }

    public static Object getArray(String url, Map<?, ?> headers) throws ThinklabException {

        Resty resty = new Resty();
        for (Object key : headers.keySet()) {
            resty.withHeader(key.toString(), headers.get(key).toString());
        }

        try {
            return deserializeArray(resty.json(url));
        } catch (IOException e) {
            throw new ThinklabIOException(e);
        }
    }

    public static Object post(String url, Object... arguments) throws ThinklabException {

        /*
         * temporary hack for POST with just one argument = POST payload, using RestTemplate
         */
        if (arguments != null && arguments.length == 1) {
            return new RestTemplate().postForObject(url, arguments[0], Map.class);
        }
        
        String formData = "";
        for (int i = 0; i < arguments.length; i++) {
            if (arguments[i + 1] == null) {
                i++;
                continue;
            }
            formData += (formData.isEmpty() ? "" : "&") + arguments[i] + "="
                    + Resty.enc(arguments[++i].toString());
        }
        try {
            return deserialize(_resty.json(url, Resty.form(formData)));
        } catch (IOException e) {
            throw new ThinklabIOException(e);
        }
    }

    public static Object postWithHeaders(String url, Map<String, String> headers, Object... arguments)
            throws ThinklabException {

        Resty resty = new Resty();
        for (String key : headers.keySet()) {
            resty.withHeader(key, headers.get(key));
        }

        String formData = "";
        for (int i = 0; i < arguments.length; i++) {
            if (arguments[i + 1] == null) {
                i++;
                continue;
            }
            formData += (formData.isEmpty() ? "" : "&") + arguments[i] + "="
                    + Resty.enc(arguments[++i].toString());
        }
        try {
            return deserialize(resty.json(url, Resty.form(formData)));
        } catch (IOException e) {
            throw new ThinklabIOException(e);
        }
    }

    public static Object postJSON(String url, Object... arguments) throws ThinklabException {

        Map<String, Object> formData = new HashMap<>();
        for (int i = 0; i < arguments.length; i++) {
            if (arguments[i + 1] == null) {
                i++;
                continue;
            }
            formData.put(arguments[i].toString(), arguments[i + 1]);
            i++;
        }
        try {
            return deserialize(_resty.json(url, Resty.content(new JSONObject(formData))));
        } catch (IOException e) {
            throw new ThinklabIOException(e);
        }
    }

    public static Object postJSON(String url, Map<String, String> headers, Object... arguments)
            throws ThinklabException {

        Resty resty = new Resty();
        for (String key : headers.keySet()) {
            resty.withHeader(key, headers.get(key));
        }

        Map<String, Object> formData = new HashMap<>();
        for (int i = 0; i < arguments.length; i++) {
            if (arguments[i + 1] == null) {
                i++;
                continue;
            }
            formData.put(arguments[i].toString(), arguments[i + 1]);
            i++;
        }
        try {
            return deserialize(resty.json(url, Resty.content(new JSONObject(formData))));
        } catch (IOException e) {
            throw new ThinklabIOException(e);
        }
    }

    /**
     * Put JSON from arguments, read JSON back.
     * 
     * @param url
     * @param arguments
     * @return
     * @throws ThinklabException
     */
    public static Object putJSON(String url, Object... arguments) throws ThinklabException {

        Map<String, Object> formData = new HashMap<>();
        for (int i = 0; i < arguments.length; i++) {
            if (arguments[i + 1] == null) {
                i++;
                continue;
            }
            formData.put(arguments[i].toString(), arguments[i + 1]);
            i++;
        }
        try {
            return deserialize(_resty.json(url, Resty.put(Resty.content(new JSONObject(formData)))));
        } catch (IOException e) {
            throw new ThinklabIOException(e);
        }
    }

    /**
     * Put plain text, read JSON back.
     * 
     * @param url
     * @param stuff
     * @return
     * @throws ThinklabException
     */
    public static Object put(String url, String stuff) throws ThinklabException {
        try {
            return deserialize(_resty.json(url, Resty.put(Resty.content(stuff))));
        } catch (IOException e) {
            throw new ThinklabIOException(e);
        }
    }

    /**
     * Put plain text, read JSON back.
     * 
     * @param url
     * @param stuff
     * @return
     * @throws ThinklabException
     */
    public static Object put(String url, Map<?, ?> headers, String stuff) throws ThinklabException {

        Resty resty = new Resty();
        for (Object key : headers.keySet()) {
            resty.withHeader(key.toString(), headers.get(key).toString());
        }
        try {
            return deserialize(resty.json(url, Resty.put(Resty.content(stuff))));
        } catch (IOException e) {
            throw new ThinklabIOException(e);
        }
    }

    public static Object postFile(String url, File toUpload) throws ThinklabException {

        try {
            byte[] bytes = FileUtils.readFileToByteArray(toUpload);
            return deserialize(_resty.json(url, Resty.form(Resty.data(MiscUtilities.getFileName(toUpload
                    .toString()), Resty.content(bytes)))));
        } catch (IOException e) {
            throw new ThinklabIOException(e);
        }
    }

    public static String getString(String url) throws ThinklabException {
        Object ret = get(url);
        if (ret != null && !(ret instanceof String)) {
            throw (ret instanceof Throwable) ? new ThinklabIOException((Throwable) ret)
                    : new ThinklabIOException("return value is not a string");
        }
        return (String) ret;

    }

    public static Number getNumber(String url) throws ThinklabException {
        Object ret = get(url);
        if (ret != null && !(ret instanceof Number)) {
            throw (ret instanceof Throwable) ? new ThinklabIOException((Throwable) ret)
                    : new ThinklabIOException("return value is not a number");
        }
        return (Number) ret;

    }

}
