/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.common.engine;

import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import org.integratedmodelling.api.engine.IModelingEngine;
import org.integratedmodelling.api.knowledge.IKnowledge;
import org.integratedmodelling.api.lang.IParseable;
import org.integratedmodelling.api.modelling.IDirectObserver;
import org.integratedmodelling.api.modelling.IExtent;
import org.integratedmodelling.api.modelling.IModelObject;
import org.integratedmodelling.api.modelling.IScale;
import org.integratedmodelling.api.modelling.IScale.Locator;
import org.integratedmodelling.api.modelling.ISubject;
import org.integratedmodelling.api.modelling.visualization.IMedia;
import org.integratedmodelling.api.monitoring.Messages;
import org.integratedmodelling.api.project.IProject;
import org.integratedmodelling.api.rest.IRESTController;
import org.integratedmodelling.api.runtime.IContext;
import org.integratedmodelling.api.runtime.ITask;
import org.integratedmodelling.api.time.ITemporalExtent;
import org.integratedmodelling.common.configuration.KLAB;
import org.integratedmodelling.common.model.runtime.Scale;
import org.integratedmodelling.common.model.runtime.Space;
import org.integratedmodelling.common.model.runtime.Time;
import org.integratedmodelling.common.monitoring.Notification;
import org.integratedmodelling.common.network.Endpoints;
import org.integratedmodelling.common.network.Network;
import org.integratedmodelling.common.network.RESTController;
import org.integratedmodelling.common.utils.NetUtilities;
import org.integratedmodelling.common.utils.StringUtils;
import org.integratedmodelling.common.visualization.Viewport;
import org.integratedmodelling.exceptions.ThinklabException;
import org.integratedmodelling.exceptions.ThinklabIOException;
import org.integratedmodelling.exceptions.ThinklabRuntimeException;

public abstract class ModelingEngine extends Engine implements IModelingEngine {

    protected ModelingEngine(IRESTController controller) {
        super(controller);
    }

    @Override
    public String openSession() throws ThinklabException {

        String ret = null;
        try {
            Object o = get(Endpoints.AUTHENTICATE, "username", getUser().getUsername());

            /*
             * results contain both the session ID and the network structure.
             */
            if (o instanceof Map && ((Map<?, ?>) o).containsKey("session")) {
                Map<?, ?> map = (Map<?, ?>) o;
                ret = map.get("session").toString();
                if (map.containsKey("network"))
                    ((Network) KLAB.NETWORK).readStructureFromList(this, (List<?>) map.get("network"));
            }

        } catch (ThinklabException e) {
            throw new ThinklabRuntimeException(e.getMessage());
        }

        ((RESTController) controller).setSessionId(ret);

        return ret;
    }

    @Override
    public ITask observe(Object observable, IContext context, ISubject subcontext, Collection<String> scenarios)
            throws ThinklabException {

        if (!isRunning())
            return null;

        String scenario = null;
        if (scenarios != null && scenarios.size() > 0) {
            scenario = StringUtils.join(scenarios, ",");
        }

        refreshWorkspace(false);

        Object r = get(Endpoints.OBSERVE, "observable", subjectName(observable), "asynchronous", "true", "target", (subcontext == null
                ? null
                : context.getPathFor(subcontext)), "context", (context == null ? null
                        : context.getId()), "scenario", scenario);

        if (r instanceof Throwable) {
            throw new ThinklabIOException((Throwable) r);
        }

        if (!(r instanceof ITask)) {
            throw new ThinklabIOException("an error occurred while observing " + observable);
        }

        ITask ret = (ITask) r;
        monitor.send(new Notification(Messages.TASK_STARTED, KLAB.CLIENT.getCurrentSession(), ret));
        return ret;
    }

    private static String subjectName(Object observable) {
        if (observable instanceof IModelObject) {
            return ((IModelObject) observable).getName();
        } else if (observable instanceof IKnowledge) {
            return ((IParseable) observable).asText();
        }
        return observable.toString();
    }

    @Override
    public ITask observe(IDirectObserver observer, IExtent... forceScale) throws ThinklabException {
        if (!isRunning())
            return null;

        refreshWorkspace(false);

        ArrayList<Object> args = new ArrayList<>();
        args.add("observable");
        args.add(observer.getName());
        args.add("asynchronous");
        args.add("true");
        args.add("reset");
        args.add("false");

        if (forceScale != null) {
            for (IExtent e : forceScale) {
                if (e instanceof ITemporalExtent) {
                    args.add("force-time");
                    args.add(Time.asString((ITemporalExtent) e));
                } else if (e instanceof Space) {
                    args.add("force-space");
                    args.add(Space.asString((Space) e));
                }
            }
        }

        Object r = get(Endpoints.OBSERVE, args.toArray());

        if (r instanceof Throwable) {
            throw new ThinklabIOException((Throwable) r);
        }

        if (!(r instanceof ITask)) {
            throw new ThinklabIOException("an error occurred while observing " + observer);
        }

        ITask ret = (ITask) r;
        monitor.send(new Notification(Messages.TASK_STARTED, KLAB.CLIENT.getCurrentSession(), ret));
        return ret;

    }

    public void refreshWorkspace(boolean forceReload) throws ThinklabException {
        /*
         * load projects 
         * TODO much better approach but it's still creating too many issues with the client to
         * use. Currently the server is doing this within a task.
         */
        // Object rt = engine.send("project", getSession(), "cmd", "load", "reset", (forceReload ? "true"
        // : "false"));
        // if (rt instanceof ITask) {
        // ((ITask) rt).finish();
        // }
    }

    @Override
    public void interruptTask(long taskId) {
        // TODO Auto-generated method stub

    }

    @SuppressWarnings("unchecked")
    @Override
    public List<Map<String, Object>> importObservations(File file) throws ThinklabException {
        return (List<Map<String, Object>>) get(Endpoints.IMPORT_OBSERVATIONS, "source", file);
    }

    @Override
    public Object getStateValue(long contextId, int offset, String statePath) throws ThinklabException {
        Object o = get(Endpoints.CONTEXT, "cmd", "get-value", "ctx", ""
                + contextId, "stateindex", offset + "", "path", statePath);
        if (o instanceof Map) {
            return ((Map<?, ?>) o).get("value");
        }
        return null;
    }

    @Override
    public ITask contextualize(long contextId) throws ThinklabException {
        return (ITask) get(Endpoints.CONTEXT, "cmd", "run", "ctx", contextId + "");
    }

    @Override
    public Map<?, ?> getStatistics(long contextId, long timeAfter, String statePath, Iterable<IScale.Locator> locators)
            throws ThinklabException {
        return (Map<?, ?>) get(Endpoints.CONTEXT, "cmd", "get-data-summary", "ctx", ""
                + contextId, "after", timeAfter + "", "index", Scale
                        .locatorsAsText(locators), "path", statePath);
    }

    @Override
    public void persistContext(File file, long contextId, String path, IMedia.Type mediaType, Object... options)
            throws ThinklabException {

        List<String> args = new ArrayList<>();

        args.add("cmd");
        args.add("persist");
        args.add("ctx");
        args.add("" + contextId);
        args.add("file");
        args.add(file.toString());
        if (path != null && !path.equals("/")) {
            args.add("path");
            args.add(path);
        }
        args.add("type");
        args.add(mediaType.name());

        List<IScale.Locator> locators = new ArrayList<>();
        for (Object o : options) {
            if (o instanceof Viewport) {
                args.add("viewport");
                args.add(o.toString());
            } else if (o instanceof IScale.Locator) {
                locators.add((Locator) o);
            }
        }

        if (locators.size() > 0) {
            args.add("index");
            args.add(Scale.locatorsAsText(locators));
        }
        get(Endpoints.CONTEXT, args.toArray());

    }

    @Override
    public boolean undeployProject(String project) throws ThinklabException {
        if (registeredProjects.contains(project)) {
            // FIXME use sub-URL for cmd
            get(Endpoints.PROJECT, "cmd", "undeploy", "plugin", project);
        }
        return registeredProjects.remove(project);
    }

    @Override
    public boolean deployProject(IProject p, boolean deployPrerequisites, boolean loadAfterDeploy)
            throws ThinklabException {

        String dirs = "";

        /*
         * embedded server just registers the dirs, making sure the projects are
         * known, then loads the passed one.
         */
        if (deployPrerequisites) {
            for (IProject proj : p.getPrerequisites()) {
                if (!registeredProjects.contains(proj)) {
                    dirs += (dirs.isEmpty() ? "" : ",") + proj.getLoadPath();
                }
                registeredProjects.add(proj);
            }
        }

        if (!registeredProjects.contains(p)) {
            dirs += (dirs.isEmpty() ? "" : ",") + p.getLoadPath();
            registeredProjects.add(p);
        }

        if (!dirs.isEmpty()) {
            // FIXME use sub-endpoint properly
            if (loadAfterDeploy) {
                get(Endpoints.PROJECT, "cmd", "register", "directory", dirs, "plugin", p
                        .getId(), "load", "true");
            } else {
                get(Endpoints.PROJECT, "cmd", "register", "directory", dirs, "plugin", p.getId());
            }
        }

        return true;
    }

    /**
     * Ensure the local server is not running or if it is, do your best to stop it before returning.
     * 
     * @return
     */
    @Override
    public boolean stop() {

        final int MAX_ATTEMPTS = 3;
        final String LOCAL_URL = "http://127.0.0.1:8182/rest/";
        int attempt = 0;
        if (NetUtilities.urlResponds(LOCAL_URL)) {
            do {
                try {
                    get(Endpoints.SHUTDOWN);
                } catch (Exception e) {
                    break;
                }
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                }
                attempt++;
            } while (NetUtilities.urlResponds(LOCAL_URL) || attempt <= MAX_ATTEMPTS);
        }
        reset();
        return attempt <= MAX_ATTEMPTS;
    }

    /*
     * can't believe this is necessary.
     */
    protected static String getClasspathSeparator() {
        switch (KLAB.CONFIG.getOS()) {
        case MACOS:
        case UNIX:
            return ":";
        case WIN:
            return ";";
        }
        return ":";
    }
}
