/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.common.thinkql;

import org.integratedmodelling.api.knowledge.ICodeExpression;
import org.integratedmodelling.api.modelling.INamespace;
import org.integratedmodelling.api.project.IProject;

/**
 * Simple class to facilitate communication of namespaces and projects to expressions
 * coming from k.IM. If the expression created by a function call is a CodeExpression,
 * the model factory will add the namespace after evaluation.
 * 
 * @author ferdinando.villa
 *
 */
public abstract class CodeExpression implements ICodeExpression {

    protected INamespace namespace;

    public void setNamespace(INamespace namespace) {
        this.namespace = namespace;
    }

    public INamespace getNamespace() {
        return this.namespace;
    }

    public IProject getProject() {
        return namespace == null ? null : namespace.getProject();
    }

}
