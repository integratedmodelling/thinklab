/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.common.thinkql;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.codehaus.groovy.control.CompilerConfiguration;
import org.integratedmodelling.api.knowledge.IConcept;
import org.integratedmodelling.api.knowledge.ICondition;
import org.integratedmodelling.api.modelling.IDependency;
import org.integratedmodelling.api.modelling.IFunctionCall;
import org.integratedmodelling.api.modelling.IModel;
import org.integratedmodelling.api.modelling.INamespace;
import org.integratedmodelling.api.modelling.IObserver;
import org.integratedmodelling.api.modelling.IObservingObject;
import org.integratedmodelling.api.monitoring.IMonitor;
import org.integratedmodelling.common.configuration.KLAB;
import org.integratedmodelling.common.errormanagement.CompileError;
import org.integratedmodelling.common.vocabulary.NS;
import org.integratedmodelling.exceptions.ThinklabException;

import groovy.lang.Binding;
import groovy.lang.GroovyShell;
import groovy.lang.Script;

public class GroovyExpression extends CodeExpression implements ICondition {

    protected String        code;
    protected boolean       negated = false;
    protected Object        object;
    protected IFunctionCall functionCall;
    protected IModel        model;
    protected boolean       isNull  = false;
    protected boolean       isTrue  = false;

    Script        script;
    Set<IConcept> domain;
    INamespace    namespace;

    private List<CompileError> errors = new ArrayList<>();

    /*
     * used by Thinklab to instantiate
     */
    public GroovyExpression() {
    }

    public boolean hasErrors() {
        return errors.size() > 0;
    }

    public List<CompileError> getErrors() {
        return errors;
    }

    /*
     * used by Thinklab - when using the API use the String constructor. MUST be called in all
     * cases.
     */
    public void initialize(Map<String, IObserver> inputs, Map<String, IObserver> outputs) {
        compile(preprocess(code, inputs, outputs));
    }

    /**
     * Simple expression without context or receivers. NOT PREPROCESSED in the context it's in.
     *
     * @param code
     */
    public GroovyExpression(String code, Map<String, IObserver> inputs, Map<String, IObserver> outputs,
            IConcept... domain) {
        this.code = code;
        this.domain = new HashSet<>();
        if (domain != null) {
            for (IConcept c : domain) {
                this.domain.add(c);
            }
        }
        initialize(inputs, outputs);
    }

    /**
     * Preprocess with the dependencies of the passed model preset in symbol table.
     * 
     * @param code
     * @param model
     */
    public GroovyExpression(String code, IObservingObject model) {
        this.code = code;
        Map<String, IObserver> inputs = new HashMap<>();
        for (IDependency d : model.getDependencies()) {
            if (d.getObservable().getObserver() != null) {
                inputs.put(d.getFormalName(), d.getObservable().getObserver());
            }
        }
        initialize(inputs, null);
    }

    public GroovyExpression(String code, INamespace namespace, Set<IConcept> domain) {
        this.namespace = namespace;
        this.code = code;
        this.domain.addAll(domain);
    }

    public GroovyExpression(String code) {
        this.code = code;
        this.domain = new HashSet<>();
    }

    private void compile(String code) {
        CompilerConfiguration compiler = new CompilerConfiguration();
        compiler.setScriptBaseClass(getBaseClass());
        GroovyShell shell = new GroovyShell(this.getClass().getClassLoader(), new Binding(), compiler);
        script = shell.parse(code);
    }

    protected String getBaseClass() {

        /*
         * choose proper class according to domains so that the appropriate functions are
         * supported.
         */
        if (domain != null) {

            if (domain.contains(KLAB.c(NS.SPACE_DOMAIN)) && domain.contains(KLAB.c(NS.TIME_DOMAIN))) {
                return "org.integratedmodelling.thinklab.actions.SpatioTemporalActionScript";
            } else if (domain.contains(KLAB.c(NS.SPACE_DOMAIN))) {
                return "org.integratedmodelling.thinklab.actions.SpatialActionScript";
            } else if (domain.contains(KLAB.c(NS.TIME_DOMAIN))) {
                return "org.integratedmodelling.thinklab.actions.TemporalActionScript";
            }
        }
        return "org.integratedmodelling.thinklab.actions.ActionScript";
    }

    @Override
    public Object eval(Map<String, Object> parameters, IMonitor monitor, IConcept... context)
            throws ThinklabException {

        if (isTrue) {
            return true;
        }

        if (isNull) {
            return null;
        }

        if (code != null) {
            try {
                script.setBinding(getBinding(parameters));
                return script.run();
            } catch (Throwable t) {
                throw new ThinklabException(t);
            }
        } else if (object != null) {
            return object;
        } else if (functionCall != null) {
            return KLAB.MFACTORY.callFunction(functionCall, monitor, model, context);
        }
        return null;
    }

    private Binding getBinding(Map<String, Object> parameters) {

        HashMap<String, Object> p = new HashMap<String, Object>();
        p.put("_p", parameters);
        p.putAll(parameters);
        p.put("_ns", namespace);
        p.put("_mmanager", KLAB.MMANAGER);
        p.put("_engine", KLAB.ENGINE);
        p.put("_pmanager", KLAB.PMANAGER);
        p.put("_kmanager", KLAB.KM);
        p.put("_config", KLAB.CONFIG);
        p.put("_network", KLAB.NETWORK);
        Binding ret = new Binding(p);

        /*
         * TODO bind receivers, project, schedule, events etc.
         */

        return ret;
    }

    private String preprocess(String code, Map<String, IObserver> inputs, Map<String, IObserver> outputs) {

        GroovyExpressionPreprocessor processor = new GroovyExpressionPreprocessor(namespace, inputs
                .keySet(), domain);
        String ret = processor.process(code);
        this.errors.addAll(processor.getErrors());
        return ret;
    }

    @Override
    public String toString() {
        return code;
    }

    @Override
    public void setNegated(boolean negate) {
        negated = negate;
    }

    @Override
    public boolean isNegated() {
        return negated;
    }

}
