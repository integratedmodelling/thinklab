/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.common.command;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import joptsimple.OptionParser;
import joptsimple.OptionSet;

import org.integratedmodelling.api.knowledge.IExpression;
import org.integratedmodelling.api.modelling.IDirectObservation;
import org.integratedmodelling.api.modelling.IProcess;
import org.integratedmodelling.api.modelling.ISubject;
import org.integratedmodelling.api.modelling.runtime.IContextualizer;
import org.integratedmodelling.api.modelling.scheduling.ITransition;
import org.integratedmodelling.api.monitoring.IMonitor;
import org.integratedmodelling.api.runtime.ISession;
import org.integratedmodelling.api.services.IPrototype;
import org.integratedmodelling.api.services.IServiceCall;
import org.integratedmodelling.api.services.IServiceManager;
import org.integratedmodelling.common.configuration.KLAB;
import org.integratedmodelling.common.model.remote.ModelService;
import org.integratedmodelling.common.model.runtime.Scale;
import org.integratedmodelling.common.model.runtime.Transition;
import org.integratedmodelling.common.monitoring.Monitor;
import org.integratedmodelling.common.utils.StringUtils;
import org.integratedmodelling.common.vocabulary.Observable;
import org.integratedmodelling.exceptions.ThinklabValidationException;

public class ServiceManager implements IServiceManager {

    HashMap<String, IPrototype>   _prototypes               = new HashMap<String, IPrototype>();
    HashMap<String, IPrototype>   _functionPrototypes       = new HashMap<String, IPrototype>();
    HashMap<String, IPrototype>   _remoteFunctionPrototypes = new HashMap<String, IPrototype>();

    private static ServiceManager _this;

    private ServiceManager() {
    }

    public List<IPrototype> getPrototypes() {
        ArrayList<IPrototype> ret = new ArrayList<>();
        for (IPrototype p : _prototypes.values()) {
            ret.add(p);
        }
        Collections.sort(ret, new Comparator<IPrototype>() {

            @Override
            public int compare(IPrototype o1, IPrototype o2) {
                return o1.getId().compareTo(o2.getId());
            }
        });
        return ret;
    }

    public List<IPrototype> getFunctionPrototypes() {
        ArrayList<IPrototype> ret = new ArrayList<>();
        for (IPrototype p : _functionPrototypes.values()) {
            ret.add(p);
        }
        Collections.sort(ret, new Comparator<IPrototype>() {

            @Override
            public int compare(IPrototype o1, IPrototype o2) {
                return o1.getId().compareTo(o2.getId());
            }
        });
        return ret;
    }

    public static ServiceManager get() {
        if (_this == null) {
            _this = new ServiceManager();
        }
        return _this;
    }

    @Override
    public IPrototype processPrototypeDeclaration(org.integratedmodelling.api.services.annotations.Prototype command, Class<?> executor) {

        IPrototype ret = new Prototype(command, executor);
        if (IExpression.class.isAssignableFrom(executor)) {
            _functionPrototypes.put(command.id(), ret);
        } else {
            _prototypes.put(command.id(), ret);

            /*
             * if this service is a modeling service, we also make it available
             * as a function local to this engine.
             */
            if (IContextualizer.class.isAssignableFrom(executor)) {
                _functionPrototypes.put(command.id(), ret);
            }

        }
        for (IPrototype fp : ((Prototype) ret).getFunctionPrototypes()) {
            _functionPrototypes.put(fp.getId(), fp);
        }
        return ret;
    }

    /**
     * Create a service call using the first argument as the service name and the remaining as 
     * arguments. The remaining arguments must be in pairs <id, value> and an odd number will cause
     * a nasty outcome. The call will contain a simple logging monitor and no session. Returns a
     * ServiceCall so we can call modifiers on it.
     * 
     * @param arguments
     * @return
     * @throws ThinklabValidationException 
     */
    static public ServiceCall getServiceCall(String serviceId, Object... arguments)
            throws ThinklabValidationException {
        Map<String, Object> map = new HashMap<>();
        for (int i = 0; i < arguments.length; i++) {
            String aname = arguments[i].toString();
            Object avalu = arguments[++i];
            if (avalu != null) {
                map.put(aname, avalu);
            }
        }
        return (ServiceCall) parseCall(serviceId, map, new Monitor(), null);
    }

    static public ServiceCall getUnvalidatedServiceCall(String serviceId, Object... arguments)
            throws ThinklabValidationException {
        Map<String, Object> map = new HashMap<>();
        for (int i = 0; i < arguments.length; i++) {
            String aname = arguments[i].toString();
            Object avalu = arguments[++i];
            if (avalu != null) {
                map.put(aname, avalu);
            }
        }
        return (ServiceCall) parseUnvalidatedCall(serviceId, map, new Monitor(), null);
    }

    public static IServiceCall parseCall(String id, Map<String, ?> arguments, IMonitor monitor, ISession session)
            throws ThinklabValidationException {

        ServiceCall ret = null;
        IPrototype prototype = get().getPrototype(id);
        if (prototype == null)
            return null;

        ret = new ServiceCall(prototype, monitor, session);

        if (arguments.containsKey("cmd")) {
            ret.setSubcommand(arguments.get("cmd").toString());
        }

        for (String s : prototype.getOptionNames()) {
            if (arguments.containsKey(s)) {
                ret.setOption(s, arguments.get(s));
            }
        }
        for (String s : prototype.getArgumentNames()) {
            if (arguments.containsKey(s)) {
                ret.setArgument(s, arguments.get(s));
            }
        }

        prototype.validateArguments(ret);

        /*
         * let any internal parameters through
         */
        for (String s : arguments.keySet()) {
            if (s.startsWith("__")) {
                ret.setArgument(s, arguments.get(s));
            }
        }

        return ret;
    }

    /**
     * Only for general REST services without a prototype. Used in core network calls.
     * 
     * @param id
     * @param arguments
     * @param monitor
     * @param session
     * @return
     * @throws ThinklabValidationException
     */
    public static IServiceCall parseUnvalidatedCall(String id, Map<String, ?> arguments, IMonitor monitor, ISession session)
            throws ThinklabValidationException {

        ServiceCall ret = null;
        IPrototype prototype = get().getPrototype(id);
        if (prototype == null)
            prototype = new Prototype(id);

        ret = new ServiceCall(prototype, monitor, session);
        if (arguments.containsKey("cmd")) {
            ret.setSubcommand(arguments.get("cmd").toString());
        }
        for (String s : arguments.keySet()) {
            ret._arguments.put(s, arguments.get(s));
        }
        return ret;
    }

    @Override
    public IServiceCall parseCommandLine(String line, IMonitor monitor, ISession session)
            throws ThinklabValidationException {

        String[] a = line.split("\\s");
        org.integratedmodelling.common.command.ServiceCall ret = null;

        if (a.length < 1) {
            return null;
        }

        IPrototype prototype = getPrototype(a[0]);
        if (prototype == null)
            return null;

        ret = new org.integratedmodelling.common.command.ServiceCall(prototype, monitor, session);

        String[] args = new String[a.length - 1];
        System.arraycopy(a, 1, args, 0, a.length - 1);

        OptionParser parser = ((Prototype) prototype).getParser();

        // TODO catch UnrecognizedOptionException (from joptsimple) and propagate
        OptionSet options = parser.parse(args);

        for (String s : prototype.getOptionNames()) {
            if (options.has(s)) {
                ret.setOption(s, options.valueOf(s));
            }
        }

        int n = 0;
        int argn = 0;
        boolean acceptsSubcommand = prototype.getSubcommandNames().size() > 0;
        boolean requiresSubcommand = ((Prototype) prototype).getSubcommandMethod("") == null;
        for (Object o : options.nonOptionArguments()) {
            // pair with arguments
            if (n == 0 && acceptsSubcommand) {
                if (prototype.getSubcommandNames().contains(o.toString())) {
                    ret.setSubcommand(o.toString());
                    n++;
                    continue;
                } else if (requiresSubcommand) {
                    throw new ThinklabValidationException("command " + a[0] + " requires one of "
                            + StringUtils.join(prototype.getSubcommandNames(), ',') + " as subcommand");
                }
            }

            if (prototype.getArgumentNames().size() <= argn) {
                throw new ThinklabValidationException("command " + a[0] + " cannot be called with " + (n + 1)
                        + " arguments");
            }

            ret.setArgument(prototype.getArgumentNames().get(argn++), o);

            n++;
        }

        prototype.validateArguments(ret);

        return ret;
    }

    @Override
    public IPrototype getPrototype(String id) {
        IPrototype ret = _prototypes.get(id);
        if (ret == null) {
            ret = KLAB.NETWORK.findPrototype(id);
        }
        return ret;
    }

    @Override
    public boolean isServiceAvailableRemotely(String id) {
        return _remoteFunctionPrototypes.containsKey(id);
    }

    @Override
    public IPrototype getFunctionPrototype(String id) {
        return _functionPrototypes.get(id);
    }

    public void registerRemoteServiceAccessor(IPrototype p, Class<?> cls) {
        if (_remoteFunctionPrototypes.get(p.getId()) == null) {
            ((Prototype) p).executor = cls;
            _remoteFunctionPrototypes.put(p.getId(), p);
        }
    }

    public IPrototype getRemoteService(String id) {
        return _remoteFunctionPrototypes.get(id);
    }

    public IServiceCall getSubjectAccessorCall(IPrototype prototype, ISubject subject, Map<String, ?> parameters, ITransition transition, IMonitor monitor)
            throws ThinklabValidationException {

        ServiceCall call = new ServiceCall(prototype);

        call.setMonitor(monitor);
        call.setArgument(ModelService.MODEL_CONTEXT_ARGUMENT, "O");
        call.setArgument(ModelService.SCALE_ARGUMENT, Scale.asString(subject.getScale()));
        if (transition != null) {
            call.setArgument(ModelService.TRANSITION_ARGUMENT, Transition.asString(transition));
        }

        for (String s : parameters.keySet()) {
            if (s.startsWith("__")) {
                call.setArgument(s, parameters.get(s));
            }
        }

        for (String s : prototype.getOptionNames()) {
            if (parameters.containsKey(s)) {
                call.setOption(s, parameters.get(s));
            }
        }
        for (String s : prototype.getArgumentNames()) {
            if (parameters.containsKey(s)) {
                call.setArgument(s, parameters.get(s));
            }
        }

        return call;
    }

    public IServiceCall getStateAccessorCall(IPrototype prototype, ISubject subject, Map<String, ?> parameters, ITransition transition, int index, IMonitor monitor)
            throws ThinklabValidationException {
        ServiceCall call = new ServiceCall(prototype);

        call.setMonitor(monitor);
        call.setArgument(ModelService.MODEL_CONTEXT_ARGUMENT, "S");
        call.setArgument(ModelService.SCALE_ARGUMENT, Scale.asString(subject.getScale()));
        call.setArgument(ModelService.STATE_INDEX_ARGUMENT, index);
        if (transition != null) {
            call.setArgument(ModelService.TRANSITION_ARGUMENT, Transition.asString(transition));
        }

        for (String s : parameters.keySet()) {
            if (s.startsWith("__")) {
                call.setArgument(s, parameters.get(s));
            }
        }

        for (String s : prototype.getOptionNames()) {
            if (parameters.containsKey(s)) {
                call.setOption(s, parameters.get(s));
            }
        }
        for (String s : prototype.getArgumentNames()) {
            if (parameters.containsKey(s)) {
                call.setArgument(s, parameters.get(s));
            }
        }

        return call;
    }

    public IServiceCall getProcessAccessorCall(IPrototype prototype, IProcess process, IDirectObservation context, Map<String, ?> parameters, ITransition transition, IMonitor monitor)
            throws ThinklabValidationException {

        ServiceCall call = new ServiceCall(prototype);

        call.setMonitor(monitor);
        call.setArgument(ModelService.MODEL_CONTEXT_ARGUMENT, "P");
        call.setArgument(ModelService.SCALE_ARGUMENT, Scale.asString(context.getScale()));
        call.setArgument(ModelService.MAIN_OBSERVABLE_ARGUMENT, Observable.asString(process.getObservable()));
        call.setArgument(ModelService.CONTEXT_OBSERVABLE_ARGUMENT, Observable.asString(context
                .getObservable()));
        if (transition != null) {
            call.setArgument(ModelService.TRANSITION_ARGUMENT, Transition.asString(transition));
        }

        for (String s : parameters.keySet()) {
            if (s.startsWith("__")) {
                call.setArgument(s, parameters.get(s));
            }
        }

        for (String s : prototype.getOptionNames()) {
            if (parameters.containsKey(s)) {
                call.setOption(s, parameters.get(s));
            }
        }
        for (String s : prototype.getArgumentNames()) {
            if (parameters.containsKey(s)) {
                call.setArgument(s, parameters.get(s));
            }
        }

        return call;
    }

    public boolean hasPrototype(String id) {
        return _functionPrototypes.containsKey(id) || _prototypes.containsKey(id);
    }
}
