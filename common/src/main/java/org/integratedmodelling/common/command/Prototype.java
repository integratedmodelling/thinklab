/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.common.command;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.integratedmodelling.api.knowledge.IConcept;
import org.integratedmodelling.api.knowledge.IExpression;
import org.integratedmodelling.api.knowledge.IKnowledge;
import org.integratedmodelling.api.lang.IList;
import org.integratedmodelling.api.modelling.IFunctionCall;
import org.integratedmodelling.api.modelling.runtime.IContextualizer;
import org.integratedmodelling.api.modelling.runtime.IEventInstantiator;
import org.integratedmodelling.api.modelling.runtime.IProcessContextualizer;
import org.integratedmodelling.api.modelling.runtime.IStateContextualizer;
import org.integratedmodelling.api.modelling.runtime.ISubjectContextualizer;
import org.integratedmodelling.api.modelling.runtime.ISubjectInstantiator;
import org.integratedmodelling.api.network.IComponent;
import org.integratedmodelling.api.services.IPrototype;
import org.integratedmodelling.api.services.IServiceCall;
import org.integratedmodelling.api.services.annotations.Execute;
import org.integratedmodelling.common.configuration.KLAB;
import org.integratedmodelling.common.owl.Knowledge;
import org.integratedmodelling.common.utils.MapUtils;
import org.integratedmodelling.common.utils.StringUtils;
import org.integratedmodelling.common.vocabulary.NS;
import org.integratedmodelling.exceptions.ThinklabRuntimeException;
import org.integratedmodelling.exceptions.ThinklabValidationException;
import org.integratedmodelling.lang.IRemoteSerializable;
import org.integratedmodelling.list.PolyList;

import joptsimple.OptionParser;
import joptsimple.OptionSpecBuilder;

/**
 * Note: duplication of names as both arguments and options is not permitted.
 * 
 * TODO: support multiple return types
 * 
 * @author Ferd
 *
 */
public class Prototype implements IPrototype, IRemoteSerializable {

    String     id;
    IComponent component;

    /*
     * argument type - either a concept to validate the arg value against, or
     * an array of strings to match fixed alternatives. One and only one of
     * the members will be not-null.
     */
    static public class ArgType {
        List<String> type = new ArrayList<>();
        String[]     tokens;

        @Override
        public String toString() {
            return type.size() == 0 ? StringUtils.join(tokens, '|') : StringUtils.join(type, ",");
        }

        public boolean isVoid() {
            return type.size() == 1
                    && type.get(0).equals(org.integratedmodelling.api.services.annotations.Prototype.NONE);
        }
    }

    static class Argument {
        String  id;
        String  shortId;
        boolean isOption;
        boolean isOptionalArgument;
        String  description = "";
        ArgType type;
    }

    static class Subcommand {
        String   id;
        String[] requiredArgs = new String[] {};
        String   description  = "";
    }

    String description = "";

    /*
     * record the executor method name for each subcommand. The "" subcommand is the
     * method to call to handle a call with no subcommand.
     */
    HashMap<String, String>     subcommandExecutors = new HashMap<String, String>();
    HashMap<String, Subcommand> subcommands         = new HashMap<String, Subcommand>();
    // all subcommands in order of declaration without the "" one.
    List<String>                subcommandNames     = new ArrayList<>();
    List<String>                argNames            = new ArrayList<>();
    List<String>                optNames            = new ArrayList<>();
    boolean                     isDistributed       = false;
    boolean                     isPublished         = false;

    // if a @Function annotation is used in any methods, we create a separate prototype for
    // each function, which will be indexed by the command manager.
    List<IPrototype> functionPrototypes = new ArrayList<>();
    List<String>     returnTypes        = new ArrayList<>();
    Class<?>         executor;

    // this can contain the single characters P=process, S=state, O=subject and E=event. Used internally to
    // assess at the client side which kinds of model service executor are associated if any.
    String modelcontext = "";

    HashMap<String, Argument> arguments = new HashMap<>();

    private OptionParser parser;

    public Prototype(Map<?, ?> map) {

        // AARGH
        this.id = map.get("id").toString();
        this.modelcontext = map.get("model-context").toString();
        for (Object s : ((List<?>) (map.get("subcommandNames")))) {
            subcommandNames.add(s.toString());
        }
        for (Object s : ((List<?>) (map.get("argNames")))) {
            argNames.add(s.toString());
        }
        for (Object s : ((List<?>) (map.get("optNames")))) {
            optNames.add(s.toString());
        }
        isDistributed = map.get("distributed?").toString().equals("true");
        isPublished = map.get("published?").toString().equals("true");
        for (Object am : (List<?>) map.get("args")) {
            Map<?, ?> amm = (Map<?, ?>) am;
            Map<?, ?> amt = (Map<?, ?>) amm.get("type");
            Argument argument = new Argument();
            ArgType argtype = new ArgType();
            argument.description = amm.get("description").toString();
            argument.isOption = amm.get("option?").equals("true");
            argument.isOptionalArgument = amm.get("optarg?").equals("true");
            argument.id = amm.get("id").toString();
            argument.shortId = amm.containsKey("short-id") ? amm.get("short-id").toString() : null;
            if (amt.containsKey("tokens")) {
                List<?> tokens = (List<?>) amt.get("tokens");
                argtype.tokens = new String[tokens.size()];
                for (int i = 0; i < tokens.size(); i++) {
                    argtype.tokens[i] = tokens.get(i).toString();
                }
            }

            if (amt.containsKey("type")) {
                List<?> tokens = (List<?>) amt.get("type");
                for (Object o : tokens) {
                    argtype.type.add(o.toString());
                }
            }
            argument.type = argtype;
            arguments.put(argument.id, argument);
        }
        for (Object sc : (List<?>) map.get("subcommands")) {
            Map<?, ?> scc = (Map<?, ?>) sc;
            Subcommand subcommand = new Subcommand();
            subcommand.id = scc.get("id").toString();
            subcommand.description = scc.get("description").toString();
            if (scc.containsKey("required")) {
                List<?> tokens = (List<?>) scc.get("required");
                subcommand.requiredArgs = new String[tokens.size()];
                for (int i = 0; i < tokens.size(); i++) {
                    subcommand.requiredArgs[i] = tokens.get(i).toString();
                }
            }
            subcommands.put(subcommand.id, subcommand);
        }
        if (map.containsKey("return-types")) {
            for (Object sc : (List<?>) map.get("return-types")) {
                returnTypes.add(sc.toString());
            }
        }
    }

    public Prototype(String id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "<P " + id + " " + (isPublished ? "PUB" : "PRV") + ">";
    }

    @Override
    public Object adapt() {
        HashMap<String, Object> ret = new HashMap<>();

        ret.put("id", id);
        ret.put("description", description);
        ret.put("arguments", arguments);
        ret.put("subcommandNames", subcommandNames);
        ret.put("argNames", argNames);
        ret.put("optNames", optNames);
        ret.put("model-context", modelcontext);
        ret.put("return-types", returnTypes);
        ret.put("distributed?", isDistributed ? "true" : "false");
        ret.put("published?", isPublished ? "true" : "false");
        ArrayList<Map<?, ?>> args = new ArrayList<>();
        for (String a : arguments.keySet()) {
            Argument aa = arguments.get(a);
            args.add(MapUtils
                    .of("id", aa.id, "description", aa.description, "option?", aa.isOption, "optarg?", aa.isOptionalArgument, "short-id", aa.shortId, "type", MapUtils
                            .of("tokens", aa.type.tokens, "type", aa.type.type)));
        }
        ret.put("args", args);
        ArrayList<Map<?, ?>> subc = new ArrayList<>();
        for (Subcommand sc : subcommands.values()) {
            subc.add(MapUtils.of("id", sc.id, "description", sc.description, "required", sc.requiredArgs));
        }
        ret.put("subcommands", subc);
        return ret;
    }

    @Override
    public String getSubcommandDescription(String subcommand) {
        return this.subcommands.get(subcommand).description;
    }

    public Prototype(org.integratedmodelling.api.services.annotations.Prototype command,
            Class<?> executor) {

        this(command.id(), command.description(), command
                .args(), command.argDescriptions(), command.returnTypes(), executor);
        isDistributed = command.distributed();
        isPublished = command.published();
    }

    public Prototype(String name, String description,
            String[] arguments, String[] argDescriptions, String[] returnTypes, Class<?> executor) {

        this.description = description;
        this.id = name;
        this.executor = executor;

        if (argDescriptions.length > 0 && (arguments.length / 2) != argDescriptions.length) {
            throw new ThinklabRuntimeException("argument specification incoherent for command " + id
                    + ": descriptions");
        }

        for (int argIdx = 0; argIdx < arguments.length; argIdx++) {

            boolean optional = false;
            boolean optionalArgument = false;
            String id = null;
            String shortId = null;

            String[] aa = arguments[argIdx].split("\\s+");

            int n = 0;
            if (aa[0].equals("?")) {
                n++;
                optional = true;
            } else if (aa[0].equals("#")) {
                n++;
                optionalArgument = true;
            }

            id = aa[n++];
            if (id.contains("|")) {

                if (!optional) {
                    throw new ThinklabRuntimeException("only optional arguments can have short identifiers: "
                            + ": " + name);
                }

                String[] sid = id.split("\\|");
                shortId = sid[0];
                id = sid[1];
            }

            String[] atypes = arguments[++argIdx].split(",");
            ArgType atp = new ArgType();

            for (String atype : atypes) {
                if (atype.contains("|")) {
                    atp.tokens = atype.split("\\|");
                    atp.type.add(org.integratedmodelling.api.services.annotations.Prototype.ENUM);
                } else {
                    if (!optional
                            && atype.toString()
                                    .equals(org.integratedmodelling.api.services.annotations.Prototype.NONE)) {
                        throw new ThinklabRuntimeException("only optional arguments can have no value: "
                                + ": " + name);
                    }
                    atp.type.add(atype);
                }
            }

            Argument argument = new Argument();
            argument.id = id;
            argument.isOption = optional;
            argument.isOptionalArgument = optionalArgument;
            argument.shortId = shortId;
            argument.type = atp;
            argument.description = (argDescriptions.length == 0 ? "" : argDescriptions[(argIdx - 1) / 2]);

            if (optional)
                optNames.add(id);
            else
                argNames.add(id);

            this.arguments.put(id, argument);
        }

        if (IContextualizer.class.isAssignableFrom(executor)) {

            if (IProcessContextualizer.class.isAssignableFrom(executor)) {
                modelcontext += "P";
            }
            if (IStateContextualizer.class.isAssignableFrom(executor)) {
                modelcontext += "S";
            }
            if (IEventInstantiator.class.isAssignableFrom(executor)) {
                modelcontext += "E";
            }
            if (ISubjectContextualizer.class.isAssignableFrom(executor)) {
                modelcontext += "O";
            }
            if (ISubjectInstantiator.class.isAssignableFrom(executor)) {
                modelcontext += "I";
            }
        } else {

            /*
             * process executor and define subcommands and functions if any
             */
            for (Method method : executor.getMethods()) {
                if (method.isAnnotationPresent(Execute.class)) {
                    Execute ex = method.getAnnotation(Execute.class);
                    subcommandExecutors.put(ex.command(), method.getName());
                    if (!ex.command().isEmpty()) {
                        subcommandNames.add(ex.command());
                        Subcommand subc = new Subcommand();
                        subc.description = ex.description();
                        subc.id = ex.command();
                        subc.requiredArgs = ex.requires();
                        subcommands.put(ex.command(), subc);
                    }
                }
            }
        }

        for (String rt : returnTypes) {
            this.returnTypes.add(rt);
        }
    }

    @Override
    public String getId() {
        return id;
    }

    @Override
    public Set<IConcept> getReturnTypes() {
        Set<IConcept> ret = new HashSet<IConcept>();
        for (int i = 0; i < returnTypes.size(); i++) {
            ret.add(stringToConcept(returnTypes.get(i)));
        }
        return ret;
    }

    private IConcept stringToConcept(String c) {

        IConcept ret = null;
        switch (c) {
        case org.integratedmodelling.api.services.annotations.Prototype.INT:
            ret = KLAB.c(NS.INTEGER);
            break;
        case org.integratedmodelling.api.services.annotations.Prototype.FLOAT:
            ret = KLAB.c(NS.FLOAT);
            break;
        case org.integratedmodelling.api.services.annotations.Prototype.TEXT:
            ret = KLAB.c(NS.TEXT);
            break;
        case org.integratedmodelling.api.services.annotations.Prototype.NONE:
            ret = KLAB.KM.getNothing();
            break;
        default:
            ret = KLAB.c(c);
        }

        return ret;
    }

    @Override
    public List<String> getArgumentNames() {
        return argNames;
    }

    @Override
    public List<String> getOptionNames() {
        return optNames;
    }

    @Override
    public String getDescription() {
        return description;
    }

    // @Override
    // public boolean isStateless() {
    // return IStatelessService.class.isAssignableFrom(executor);
    // }
    //
    // @Override
    // public boolean isRestricted() {
    // return IRestrictedService.class.isAssignableFrom(executor);
    // }
    //
    // @Override
    // public boolean isExclusive() {
    // return IExclusiveService.class.isAssignableFrom(executor);
    // }

    public OptionParser getParser() {
        if (parser == null) {
            parser = new OptionParser();

            for (String o : optNames) {

                Argument odesc = arguments.get(o);
                OptionSpecBuilder b = parser.acceptsAll(Arrays
                        .asList(new String[] { odesc.shortId, odesc.id }), getArgumentDescription(o));

                if (!odesc.type.type.get(0)
                        .equals(org.integratedmodelling.api.services.annotations.Prototype.NONE)) {
                    b.withRequiredArg();
                }
            }
        }
        return parser;
    }

    @Override
    public String getArgumentDescription(String argumentName) {
        return arguments.get(argumentName).description;
    }

    @Override
    public Collection<String> getSubcommandNames() {
        return subcommandNames;
    }

    @Override
    public String[] getSubcommandRequiredArguments(String subcommand) {
        return subcommands.get(subcommand).requiredArgs;
    }

    public String getSubcommandMethod(String subcommand) {
        return subcommandExecutors.get(subcommand);
    }

    @Override
    public void validateArguments(IServiceCall call) throws ThinklabValidationException {

        HashSet<String> required = new HashSet<>();
        boolean isdefault = call.getSubcommand() == null;
        if (!isdefault) {
            isdefault = subcommands.get(call.getSubcommand()).requiredArgs.length == 0;
        }

        if (isdefault) {
            for (Argument arg : arguments.values()) {
                if (!arg.isOption && !arg.isOptionalArgument)
                    required.add(arg.id);
            }
        } else {
            for (String arg : subcommands.get(call.getSubcommand()).requiredArgs) {
                required.add(arg);
            }
        }

        /*
         * check that all required are there
         */
        for (String ra : required) {
            if (!call.has(ra)) {
                String d = arguments.get(ra).isOption ? "option" : "argument";
                throw new ThinklabValidationException("command " + id + " requires " +
                        d + " " + ra);
            }
        }

        /*
         * arg types are converted and validation is performed at conversion, so don't check them
         * here.
         */
    }

    @Override
    public void validateArguments(IFunctionCall call) throws ThinklabValidationException {

        HashSet<String> required = new HashSet<>();
        for (Argument arg : arguments.values()) {
            if (!arg.isOption && !arg.isOptionalArgument)
                required.add(arg.id);
        }

        /*
         * check that all required are there
         */
        for (String ra : required) {
            if (!call.getParameters().containsKey(ra)) {
                String d = arguments.get(ra).isOption ? "option" : "argument";
                throw new ThinklabValidationException("function " + id + " requires " +
                        d + " " + ra);
            }
        }

        /*
         * arg types (these come from the language so they must be valid at the call).
         */
        if (call.getParameters() != null) {
            for (String key : call.getParameters().keySet()) {
                validateArgumentType(key, call.getParameters().get(key));
            }
        }
    }

    @Override
    public Object validateArgumentType(String key, Object value)
            throws ThinklabValidationException {

        // pass through any internal args
        if (key.startsWith("__")) {
            return value;
        }

        if (arguments.get(key) == null) {
            throw new ThinklabValidationException(value + ": argument " + key + " to " + getId()
                    + " is not recognized");
        }

        ArgType type = arguments.get(key).type;

        /*
         * null (unknown) is a valid everything, of course it must be handled by the
         * callee. TODO We may want to add flags to the arguments for nullable. 
         */
        if (value == null) {
            return IFunctionCall.NULL;
        }

        if (value instanceof IFunctionCall) {
            /*
             * validate against a prototype value and return the function.
             */
            validateArgumentType(key, getFunctionDummyValue((IFunctionCall) value, type));
            return value;
        }

        ThinklabValidationException error = null;
        boolean isOk = false;

        for (String tp : type.type) {
            switch (tp) {
            case org.integratedmodelling.api.services.annotations.Prototype.INT:
                if (value instanceof String) {
                    try {
                        value = Long.parseLong(value.toString());
                    } catch (Throwable t) {
                        value = null;
                    }
                }
                if (value instanceof Number) {
                    value = ((Number) value).longValue();
                    isOk = true;
                } else {
                    error = new ThinklabValidationException(value + ": argument " + key + " to " + getId()
                            + " must be an integer");
                }
                break;
            case org.integratedmodelling.api.services.annotations.Prototype.BOOLEAN:
                if (value instanceof String) {
                    try {
                        if (value.toString().equals("true") || value.toString().equals("false")) {
                            value = new Boolean(value.equals("true"));
                        }
                    } catch (Throwable t) {
                        value = null;
                    }
                }
                if (value instanceof Boolean) {
                    isOk = true;
                } else {
                    error = new ThinklabValidationException(value + ": argument " + key + " to " + getId()
                            + " must be a boolean (true or false)");
                }
                break;
            case org.integratedmodelling.api.services.annotations.Prototype.FLOAT:
                if (value instanceof String) {
                    try {
                        value = Double.parseDouble(value.toString());
                    } catch (Throwable t) {
                        value = null;
                    }
                }
                if (value instanceof Number) {
                    value = ((Number) value).doubleValue();
                    isOk = true;
                } else {
                    error = new ThinklabValidationException(value + ": argument " + key + " to " + getId()
                            + " must be a double");
                }
                break;
            case org.integratedmodelling.api.services.annotations.Prototype.LIST:
                if (!(value instanceof IList)) {
                    error = new ThinklabValidationException(value + ": argument " + key + " to " + getId()
                            + " must be a list");
                }
                break;
            case org.integratedmodelling.api.services.annotations.Prototype.CONCEPT:
                if (!(value instanceof IConcept)) {
                    try {
                        value = Knowledge.parse(value.toString());
                    } catch (Throwable e) {
                        value = null;
                    }
                    if (!(value instanceof IConcept)) {
                        error = new ThinklabValidationException(value + ": argument " + key + " to "
                                + getId()
                                + " does not specify a concept");
                    } else {
                        isOk = true;
                    }
                }
                break;
            case org.integratedmodelling.api.services.annotations.Prototype.KNOWLEDGE:
                if (!(value instanceof IKnowledge)) {
                    try {
                        value = Knowledge.parse(value.toString());
                    } catch (Throwable e) {
                        value = null;
                    }
                    if (!(value instanceof IKnowledge)) {
                        error = new ThinklabValidationException(value + ": argument " + key + " to "
                                + getId()
                                + " does not specify knowledge");
                    } else {
                        isOk = true;
                    }
                }
                break;
            case org.integratedmodelling.api.services.annotations.Prototype.NONE:
                if (value != null) {
                    error = new ThinklabValidationException("option " + key + " to " + getId()
                            + " cannot have a value");
                } else {
                    isOk = true;
                }
                break;
            case org.integratedmodelling.api.services.annotations.Prototype.TEXT:
                if (!(value instanceof String)) {
                    value = value.toString();
                    isOk = true;
                }
                break;
            case org.integratedmodelling.api.services.annotations.Prototype.EXPRESSION:
                /*
                 * TODO no real validation here, as what is passed by the parser is simply
                 * a string with the brackets removed.
                 */
                if (!(value instanceof String) && !(value instanceof IExpression)) {
                    error = new ThinklabValidationException(value + ": argument " + key + " to " + getId()
                            + " must be an expression in square brackets");
                } else {
                    isOk = true;
                }
                break;
            case org.integratedmodelling.api.services.annotations.Prototype.ENUM:
                boolean ok = false;
                for (String s : type.tokens) {
                    if (value.toString().equals(s)) {
                        value = s;
                        ok = true;
                        break;
                    }
                }
                if (!ok) {
                    error = new ThinklabValidationException(value + ": argument " + key + " to " + getId()
                            + " must be one of " + type);
                } else {
                    isOk = true;
                }
                break;
            }

            if (isOk) {
                break;
            }
        }

        if (!isOk && error != null) {
            throw error;
        }

        return value;
    }

    private Object getFunctionDummyValue(IFunctionCall fc, ArgType type) throws ThinklabValidationException {
        Prototype p = (Prototype) fc.getPrototype();
        if (p.returnTypes.size() == 0) {
            throw new ThinklabValidationException("cannot use the value of a void function: " + p.getId());
        }
        String s = p.returnTypes.get(0);

        Object ret = null;

        switch (s) {
        case org.integratedmodelling.api.services.annotations.Prototype.INT:
            ret = 0;
            break;
        case org.integratedmodelling.api.services.annotations.Prototype.BOOLEAN:
            ret = true;
            break;
        case org.integratedmodelling.api.services.annotations.Prototype.FLOAT:
            ret = 0.0;
            break;
        case org.integratedmodelling.api.services.annotations.Prototype.LIST:
            ret = new PolyList();
            break;
        case org.integratedmodelling.api.services.annotations.Prototype.CONCEPT:
            ret = KLAB.KM.getRootConcept();
            break;
        case org.integratedmodelling.api.services.annotations.Prototype.NONE:
            throw new ThinklabValidationException("cannot use the value of a void function: " + p.getId());
        case org.integratedmodelling.api.services.annotations.Prototype.TEXT:
            ret = "";
            break;
        case org.integratedmodelling.api.services.annotations.Prototype.ENUM:
            ret = type.tokens[0];
            break;
        }

        return ret;
    }

    @Override
    public boolean isArgumentOptional(String arg) {
        return arguments.get(arg).isOptionalArgument;
    }

    @Override
    public String getShortSynopsis() {

        String s = id;

        if (getSubcommandNames().size() > 0) {
            s += (requiresSubcommand() ? " {" : " [") +
                    StringUtils.join(getSubcommandNames(), '|') +
                    (requiresSubcommand() ? "}" : "]");
        }

        for (String opt : optNames) {
            Argument ad = arguments.get(opt);
            s += " [-" + ad.shortId + "|--" + ad.id;
            if (!ad.type.isVoid())
                s += "=<" + ad.type + ">";
            s += "]";
        }

        for (String arg : argNames) {
            Argument ad = arguments.get(arg);
            if (ad.isOptionalArgument) {
                s += " [[" + ad.id + "=]<" + ad.type + ">]";
            } else {
                s += " [" + ad.id + "=]<" + ad.type + ">";
            }
        }

        return s;
    }

    @Override
    public String getSynopsis() {

        String s = "Usage: " + getShortSynopsis();

        s += "\n\n" + description + "\n\n";

        if (getSubcommandNames().size() > 0) {
            s += "Subcommands:\n\n";
            for (String sc : getSubcommandNames()) {
                s += "  " + sc + ": " + getSubcommandDescription(sc) + "\n";
            }
            s += "\n";
        }

        if (getOptionNames().size() > 0) {
            s += "Options:\n\n";
            for (String o : getOptionNames()) {
                Argument ad = arguments.get(o);
                s += "  -" + ad.shortId + "|--" + ad.id + ": " + ad.description + "\n";
            }
            s += "\n";
        }

        if (getArgumentNames().size() > 0) {
            s += "Arguments:\n\n";
            for (String a : getArgumentNames()) {
                Argument ad = arguments.get(a);
                s += "  " + ad.id + ": " + ad.description
                        + (ad.isOptionalArgument ? " (optional)" : " (required)") + "\n";
            }
            s += "\n";
        }

        return s;
    }

    @Override
    public boolean requiresSubcommand() {
        return getSubcommandMethod("") == null;
    }

    public Collection<IPrototype> getFunctionPrototypes() {
        return functionPrototypes;
    }

    @Override
    public Class<?> getExecutorClass() {
        return executor;
    }

    @Override
    public boolean isDistributed() {
        return isDistributed;
    }

    @Override
    public boolean isPublished() {
        return isPublished;
    }

    @Override
    public boolean canModel(IConcept accessorType) {
        if (accessorType.is(KLAB.c(NS.STATE_CONTEXTUALIZER))) {
            return modelcontext.contains("S");
        } else if (accessorType.is(KLAB.c(NS.SUBJECT_CONTEXTUALIZER))) {
            return modelcontext.contains("O");
        } else if (accessorType.is(KLAB.c(NS.PROCESS_CONTEXTUALIZER))) {
            return modelcontext.contains("P");
        } else if (accessorType.is(KLAB.c(NS.EVENT_INSTANTIATOR))) {
            return modelcontext.contains("E");
        }
        return false;
    }

    public void setComponent(IComponent component) {
        this.component = component;
    }

    public IComponent getComponent() {
        return component;
    }
}
