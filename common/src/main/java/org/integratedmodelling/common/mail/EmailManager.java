/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.common.mail;

import java.util.Iterator;
import java.util.List;
import java.util.Properties;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

import org.integratedmodelling.common.auth.AuthConfig;

/**
 * Sends emails. Singleton.
 * 
 * @author Ferd
 * 
 */
public class EmailManager {

    static private EmailManager _this = null;

    public static EmailManager get() {
        if (_this == null) {
            _this = new EmailManager();
        }
        return _this;
    }

    private EmailManager() {
    }

    private static final String SMTP_HOST_NAME = "smtp.gmail.com";
    private static final String SMTP_PORT      = "465";
    private static final String SSL_FACTORY    = "javax.net.ssl.SSLSocketFactory";

    /**
     * Add attachments. The String[] array must have pairs of filename, name where 
     * the second names the file in the attachment.
     * 
     * @param attachments
     * @param multipart
     * @throws MessagingException
     * @throws AddressException
     */
    protected void addAttachments(String[] attachments, Multipart multipart)
            throws MessagingException, AddressException {
        for (int i = 0; i <= attachments.length - 1; i++) {
            String filename = attachments[i];
            String name = attachments[++i];
            MimeBodyPart attachmentBodyPart = new MimeBodyPart();

            // use a JAF FileDataSource as it does MIME type detection
            DataSource source = new FileDataSource(filename);
            attachmentBodyPart.setDataHandler(new DataHandler(source));
            attachmentBodyPart.setFileName(name);

            // add the attachment
            multipart.addBodyPart(attachmentBodyPart);
        }
    }

    /**
     * @param recipients
     * @param subject
     * @param messageContent
     * @param from
     * @throws MessagingException
     */
    public void sendSSLMessage(List<String> recipients, String subject,
            String messageContent, String from, String[] attachments)
            throws Exception {

        boolean debug = false;

        Properties props = new Properties();
        props.put("mail.smtp.host", SMTP_HOST_NAME);
        props.put("mail.smtp.auth", "true");
        props.put("mail.debug", "true");
        props.put("mail.smtp.port", SMTP_PORT);
        props.put("mail.smtp.socketFactory.port", SMTP_PORT);
        props.put("mail.smtp.socketFactory.class", SSL_FACTORY);
        props.put("mail.smtp.socketFactory.fallback", "false");

        final String user = AuthConfig.getProperty(AuthConfig.EMAIL_USER);
        final String pass = AuthConfig.getProperty(AuthConfig.EMAIL_PASSWORD);

        Session session = Session.getDefaultInstance(props,
                new javax.mail.Authenticator() {
                    protected PasswordAuthentication getPasswordAuthentication() {
                        return new PasswordAuthentication(user, pass);
                    }
                });

        session.setDebug(debug);

        Message message = new MimeMessage(session);
        InternetAddress addressFrom = new InternetAddress(from);
        message.setFrom(addressFrom);

        for (Iterator<String> it = recipients.iterator(); it.hasNext();) {
            message.addRecipient(Message.RecipientType.TO, new InternetAddress(it.next()));
        }

        message.setSubject(subject);
        BodyPart messageBodyPart = new MimeBodyPart();
        messageBodyPart.setContent(messageContent, "text/html");

        // use a MimeMultipart as we need to handle the file attachments
        Multipart multipart = new MimeMultipart();

        // add the message body to the mime message
        multipart.addBodyPart(messageBodyPart);

        // add any file attachments to the message
        if (attachments != null) {
            addAttachments(attachments, multipart);
        }
        // Put all message parts in the message
        message.setContent(multipart);

        Transport.send(message);
    }
}
