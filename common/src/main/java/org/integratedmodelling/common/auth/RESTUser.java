/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.common.auth;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.integratedmodelling.api.auth.IUser;
import org.integratedmodelling.common.network.Endpoints;
import org.integratedmodelling.exceptions.ThinklabRuntimeException;
import org.joda.time.DateTime;

/**
 * RESTUser is created from a certificate and its details are filled in using
 * the online services from its primary server. A password is necessary; it can
 * be supplied in the constructor or the getPassword() function can be redefined
 * to supply it.
 * 
 * @author ferdinando.villa
 * @deprecated will be substituted by User in same package when move to Spring is
 *             complete.
 */
public class RESTUser implements IUser, Endpoints {

    String     username;
    String     serverUrl     = null;
    AuthClient client        = null;
    boolean    authenticated = false;

    Set<String>      roles  = new HashSet<>();
    Set<String>      groups = new HashSet<>();
    private String   email;
    private DateTime expiration;

    /*
     * time of creation for now. Used in several points so should not be null.
     */
    private Date lastLogin = new Date();

    // set manually through API upon successful network logon.
    private Status onlineStatus = Status.UNKNOWN;

    public RESTUser() {
        this.username = "anonymous";
    }

    /**
     * Creates an offline user who does nothing but has this username. For the
     * children.
     * 
     * @param username
     */
    public RESTUser(String username) {
        this.username = username;
    }

    /**
     * Create an authenticated user using the passed authentication service and credentials. This
     * is the endorsed way for fully privileged user connections.
     * 
     * @param authServiceUrl
     * @param username
     * @param password
     */
    public RESTUser(String authServiceUrl, String username, String password) {

        /*
         * hack to fix existing certfiles with the iP only , which seems to be
         * filtered by some router configs.
         */
        authServiceUrl = authServiceUrl
                .replaceAll("150\\.241\\.131\\.4", "www.integratedmodelling.org");

        this.serverUrl = authServiceUrl;
        this.username = username;
        this.client = new AuthClient(serverUrl, username, password);
        if (this.authenticated = client.isAuthenticated()) {
            readUserData();
        }
    }

    /**
     * Create an authenticated user from a certificate. Read the authorization
     * endpoint URL from the primary server referenced in the certificate (this
     * is the endorsed way for system connections).
     * 
     * @param certificate
     */
    public RESTUser(File certificate) {
        client = new AuthClient(certificate);
        this.username = client.getUsername();
        if (this.authenticated = client.isAuthenticated()) {
            readUserData();
        }
    }

    public RESTUser(File certificate, String authEndpoint) {
        client = new AuthClient(certificate, authEndpoint);
        this.username = client.getUsername();
        if (this.authenticated = client.isAuthenticated()) {
            readUserData();
        }
    }

    public RESTUser(AuthClient client) {
        this.client = client;
        this.username = client.getUsername();
        if (this.authenticated = client.isAuthenticated()) {
            readUserData();
        }
    }

    private void readUserData() {

        Map<?, ?> map = client.getUserData();
        if (map.get("roles") instanceof List) {
            for (Object role : (List<?>) map.get("roles")) {
                roles.add(role.toString());
            }
        }
        if (map.containsKey("expiration")) {
            this.expiration = DateTime.parse(map.get("expiration").toString());
            /*
             * TODO check expiration? Unsure if I have to do it myself.
             */
        }
        if (map.get("profile") instanceof Map) {

            Map<?, ?> profile = (Map<?, ?>) map.get("profile");
            if (profile.get("groups") instanceof List) {
                for (Object group : (List<?>) profile.get("groups")) {
                    groups.add(group.toString());
                }
            }
            if (profile.containsKey("email")) {
                this.email = profile.get("email").toString();
            }
            if (profile.containsKey("serverUrl")) {
                this.serverUrl = profile.get("serverUrl").toString();

                /*
                 * hack to fix existing certfiles with the iP only , which seems to be
                 * filtered by some router configs.
                 */
                this.serverUrl = this.serverUrl
                        .replaceAll("150\\.241\\.131\\.4", "www.integratedmodelling.org");
            }
        }
    }

    @Override
    public String getUsername() {
        return username;
    }

    @Override
    public Set<String> getRoles() {
        return roles;
    }

    @Override
    public Set<String> getGroups() {
        return groups;
    }

    @Override
    public boolean isAnonymous() {
        return (!authenticated) || client == null || client.authToken == null || serverUrl == null;
    }

    @Override
    public String toString() {
        return username +
                (email == null ? "" : " (" + email + ")");
    }

    @Override
    public String getServerURL() {
        return serverUrl;
    }

    @Override
    public String getSecurityKey() {
        return client == null ? null : client.authToken;
    }

    @Override
    public String getEmailAddress() {
        return email;
    }

    @Override
    public String getFirstName() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public String getLastName() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public String getInitials() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public String getAffiliation() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public String getComment() {
        // TODO Auto-generated method stub
        return null;
    }

    static public void main(String[] args) throws Exception {

        RESTUser io = new RESTUser(new File(System.getProperty("user.home") + "/.tl/im.cert"));
        io.getAuthClient().getNotifications();
        // RESTUser io = new RESTUser("http://integratedmodelling.org/collaboration", "engine",
        // // AuthClient.getStoredPassword("ferdinando.villa"));
        // for (Datarecord dr : dio.client.getDatasources()) {
        // System.out
        // .println("DR: "
        // + io.getAuthClient()
        // .getDatasource("im:stefano.balbi:af.mw.infrastructure:malawi_villages",
        // Endpoints.AUTH_GET_DATASOURCE_ENGINE));
        // // }
    }

    public void setLastLogin(long time) {
        lastLogin = new Date(time);
    }

    @Override
    public Date getLastLogin() {
        return lastLogin;
    }

    /**
     * Online status is set from the outside.
     * @param online
     */
    public void setOnline(Status online) {
        this.onlineStatus = online;
    }

    @Override
    public Status getOnlineStatus() {
        return onlineStatus;
    }

    /**
     * Create user profile from an authentication endpoint URL and a previously authorized
     * user.
     * 
     * @param authenticationEndpoint
     * @param uAuth
     * @return
     */
    public static IUser authenticateFromToken(String authenticationEndpoint, String uAuth) {
        try {
            AuthClient client = new AuthClient(new URL(authenticationEndpoint), uAuth);
            return new RESTUser(client);
        } catch (MalformedURLException e) {
            throw new ThinklabRuntimeException(e);
        }
    }

    /*
     * Read a certificate to retrieve the primary server; ask that server for the
     * authentication endpoint; send the certificate to that endpoint for authentication.
     * 
     * FIXME - UNOFFICIAL starting point to be used only exceptionally (like now that
     * the server still doesn't communicate auth URLs).
     * 
     * @param certificate
     * @return
     */
    public static IUser fromCertificate(File certificate, String url) {
        return new RESTUser(certificate, url);
    }

    /**
     * Read a certificate to retrieve the primary server; ask that server for the
     * authentication endpoint; send the certificate to that endpoint for authentication.
     * 
     * @param certificate
     * @return
     */
    public static IUser fromCertificate(File certificate) {
        return new RESTUser(certificate);
    }

    public AuthClient getAuthClient() {
        return client;
    }

    @Override
    public boolean canLockEngine() {
        // TODO Auto-generated method stub
        return false;
    }

}
