package org.integratedmodelling.common.auth;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import org.integratedmodelling.api.auth.IUser;
import org.integratedmodelling.common.beans.auth.AuthenticationResultResource;
import org.integratedmodelling.common.beans.auth.Role;
import org.joda.time.DateTime;

public class User implements IUser {

    AuthenticationResultResource data;
    Status                       onlineStatus = Status.UNKNOWN;

    public User(AuthenticationResultResource data) {
        this.data = data;
    }

    @Override
    public String getUsername() {
        return data.getUsername();
    }

    @Override
    public Set<String> getRoles() {
        Set<String> ret = new HashSet<>();
        for (Role r : data.getRoles()) {
            ret.add(r.name());
        }
        return ret;
    }

    @Override
    public Set<String> getGroups() {
        Set<String> ret = new HashSet<>();
        ret.addAll(data.getProfile().getGroups());
        return ret;
    }

    @Override
    public boolean isAnonymous() {
        return getUsername().equals(IUser.ANONYMOUS_USER_ID);
    }

    @Override
    public String getServerURL() {
        return data.getProfile().getServerUrl();
    }

    @Override
    public String getSecurityKey() {
        return data.getAuthToken();
    }

    @Override
    public String getEmailAddress() {
        return data.getProfile().getEmail();
    }

    @Override
    public String getFirstName() {
        return data.getProfile().getFirstName();
    }

    @Override
    public String getLastName() {
        return data.getProfile().getLastName();
    }

    @Override
    public String getInitials() {
        return data.getProfile().getInitials();
    }

    @Override
    public String getAffiliation() {
        return data.getProfile().getAffiliation();
    }

    @Override
    public String getComment() {
        return data.getProfile().getComments();
    }

    @Override
    public Date getLastLogin() {
        return new Date(DateTime.parse(data.getProfile().getLastLogin()).getMillis());
    }

    /**
     * Online status is set from the outside.
     * @param online
     */
    public void setOnline(Status online) {
        this.onlineStatus = online;
    }

    @Override
    public Status getOnlineStatus() {
        return onlineStatus;
    }

    @Override
    public boolean canLockEngine() {
        return data.isExclusive();
    }

}
