/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.common.auth;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Date;
import java.util.Properties;
import java.util.Set;

import com.verhas.licensor.License;

/**
 * Handles generation of licenses from user data.
 * 
 * @author Ferd
 *
 */
public class LicenseManager {

    static LicenseManager _this  = null;

    private static byte[] digest = new byte[] {
                                 (byte) 0x27,
                                 (byte) 0x6E,
                                 (byte) 0x68,
                                 (byte) 0x40,
                                 (byte) 0xB7,
                                 (byte) 0x35,
                                 (byte) 0xDC,
                                 (byte) 0x0D,
                                 (byte) 0x51,
                                 (byte) 0x1C,
                                 (byte) 0x81,
                                 (byte) 0xBF,
                                 (byte) 0xF1,
                                 (byte) 0x54,
                                 (byte) 0x15,
                                 (byte) 0x9C,
                                 (byte) 0xC9,
                                 (byte) 0xE4,
                                 (byte) 0x2F,
                                 (byte) 0x77,
                                 (byte) 0xF1,
                                 (byte) 0x40,
                                 (byte) 0x42,
                                 (byte) 0x7F,
                                 (byte) 0x9B,
                                 (byte) 0x1D,
                                 (byte) 0x14,
                                 (byte) 0x17,
                                 (byte) 0x5B,
                                 (byte) 0xAD,
                                 (byte) 0xDB,
                                 (byte) 0x1D,
                                 (byte) 0x6C,
                                 (byte) 0x81,
                                 (byte) 0x73,
                                 (byte) 0xAD,
                                 (byte) 0xF8,
                                 (byte) 0x5E,
                                 (byte) 0x25,
                                 (byte) 0xBA,
                                 (byte) 0x20,
                                 (byte) 0xB2,
                                 (byte) 0x70,
                                 (byte) 0xCE,
                                 (byte) 0xE4,
                                 (byte) 0xEE,
                                 (byte) 0xBB,
                                 (byte) 0xFF,
                                 (byte) 0x1D,
                                 (byte) 0xCC,
                                 (byte) 0xA5,
                                 (byte) 0x2F,
                                 (byte) 0x63,
                                 (byte) 0x93,
                                 (byte) 0x78,
                                 (byte) 0x98,
                                 (byte) 0x30,
                                 (byte) 0xDB,
                                 (byte) 0xEB,
                                 (byte) 0xB5,
                                 (byte) 0xBB,
                                 (byte) 0xA1,
                                 (byte) 0x1C,
                                 (byte) 0x2E,
                                 };

    public static LicenseManager get() throws Exception {
        if (_this == null) {
            _this = new LicenseManager();
        }
        return _this;
    }

    private LicenseManager() {
    }

    // public File getLicense(String username, boolean email) throws Exception {
    //
    // IUser user = User.retrieve(username);
    // if (user == null)
    // return null;
    // return getLicense(user, email);
    // }

    /**
     * Create an encrypted certificate containing arbitrary properties.
     * 
     * @param properties properties to encrypt
     * @param outputFile file to write certificate on
     * @throws Exception any error
     */
    public void createCertificate(Properties properties, File outputFile) throws Exception {

        String keyringFile = AuthConfig.getKeyringFile();
        String password = AuthConfig.getPassword();
        String key = AuthConfig.getKey();

        File oprop = File.createTempFile("uprop", "prop");
        OutputStream fos = new FileOutputStream(oprop);
        properties.store(fos, "Generated by integratedmodelling.org on " + new Date());
        fos.close();

        /*
         * encode the license
         */
        try {
            OutputStream os = new FileOutputStream(outputFile);
            os.write((
                    new License().
                            setLicense(oprop).
                            loadKey(keyringFile, key).
                            encodeLicense(password)).
                            getBytes("utf-8"));
            os.close();
        } catch (Exception e) {
            throw e;
        }
    }

    /**
     * Read keys from a certificate with the passed public key and return properties that
     * include those keys.
     * 
     * @param certFile
     * @param publicKey
     * @param keys
     * @return
     */
    public Properties readCertificate(File certFile, File publicKey, String[] keys) {

        URL url = null;
        Properties ret = new Properties();
        try {
            url = publicKey.toURI().toURL();
        } catch (MalformedURLException e1) {
            return null;
        }

        try (InputStream in = url.openConnection().getInputStream()) {
            License license = new License();
            license.loadKeyRing(in, digest);
            license.setLicenseEncodedFromFile(certFile.toString());
            for (String s : keys) {
                ret.setProperty(s, license.getFeature(s));
            }
        } catch (Exception e) {
            return null;
        }

        return ret;
    }
    
    /**
     * Read keys from a certificate with the passed public key and return properties that
     * include those keys.
     * 
     * @param certFile
     * @param publicKey
     * @param keys
     * @return
     */
    public Properties readCertificate(String certificate, URL publicKey, String[] keys) {

        Properties ret = new Properties();

        try (InputStream in = publicKey.openConnection().getInputStream()) {
            License license = new License();
            license.loadKeyRing(in, digest);
            license.setLicenseEncoded(certificate);
            for (String s : keys) {
                ret.setProperty(s, license.getFeature(s));
            }
        } catch (Exception e) {
            return null;
        }

        return ret;
    }

    // /**
    // * Create encrypted license for given user data.
    // *
    // * @param user
    // * @return
    // * @throws Exception
    // */
    // public File getLicense(IUser user, boolean email) throws Exception {
    //
    // File output = File.createTempFile("certificate", "crt");
    // String keyringFile = Config.getKeyringFile();
    // String password = Config.getPassword();
    // String key = Config.getKey();
    // File licenseFile = File.createTempFile("certificate", "properties");
    //
    // /**
    // * Ensure we have all the info
    // */
    // ((User) user).synchronize();
    //
    // /*
    // * create property file from user data
    // */
    // Properties up = new Properties();
    // store(user, up);
    // OutputStream fos = new FileOutputStream(licenseFile);
    // up.store(fos, "Generated by integratedmodelling.org on " + new Date());
    // fos.close();
    //
    // /*
    // * encode the license
    // */
    // try {
    // OutputStream os = new FileOutputStream(output);
    // os.write((
    // new License().
    // setLicense(licenseFile).
    // loadKey(keyringFile, key).
    // encodeLicense(password)).
    // getBytes("utf-8"));
    // os.close();
    // } catch (Exception e) {
    // throw e;
    // }
    //
    // if (email) {
    // EmailManager
    // .get()
    // .sendSSLMessage(
    // Collections.singletonList(user.getEmailAddress()),
    // "Your integratedmodelling.org certificate",
    // "Thank you for requesting a certificate. The attached reflects your settings as of "
    // +
    // new Date()
    // +
    // " and is valid for one year. Please activate it by placing the attached file in your "
    // +
    // "<home>/.thinklab directory, replacing the previous file if you had one.\n\r\n\r"
    // +
    // "Sincerely,\n\r\nwww.integratedmodelling.org",
    // "integrated.modelling@gmail.com",
    // new String[] { output.toString(), "im.cert" });
    //
    // }
    //
    // return output;
    // }

    private String serializeSet(Set<String> groups) {
        String ret = "";
        int i = 0;
        if (groups != null) {
            for (String s : groups) {
                ret += s + (i == (groups.size() - 1) ? "" : ",");
            }
        }
        return ret;
    }

    // private void store(IUser ud, Properties up) throws Exception {
    //
    // setProperty(up, IUser.USER, ud.getUsername());
    // setProperty(up, IUser.EMAIL, ud.getEmailAddress());
    // setProperty(up, IUser.FIRSTNAME, ud.getFirstName());
    // setProperty(up, IUser.LASTNAME, ud.getLastName());
    // setProperty(up, IUser.INITIALS, ud.getInitials());
    // setProperty(up, IUser.AFFILIATION, ud.getAffiliation());
    // setProperty(up, IUser.SERVER, ud.getServerURL());
    // setProperty(up, IUser.ROLES, serializeSet(ud.getRoles()));
    // setProperty(up, IUser.SKEY, ud.getSecurityKey());
    // //
    // // User.store(ud);
    // }

    private void setProperty(Properties up, String key, String value) {
        if (value != null) {
            up.setProperty(key, value);
        }
    }
}
