/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.common.auth;

import java.io.File;
import java.lang.reflect.Field;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import org.integratedmodelling.api.auth.IUser;
import org.integratedmodelling.api.engine.IEngine;
import org.integratedmodelling.common.Resources;
import org.integratedmodelling.common.configuration.KLAB;
import org.integratedmodelling.common.data.Datarecord;
import org.integratedmodelling.common.engine.JSONdeserializer;
import org.integratedmodelling.common.network.API;
import org.integratedmodelling.common.network.Announcements;
import org.integratedmodelling.common.network.Endpoints;
import org.integratedmodelling.common.utils.FileUtils;
import org.integratedmodelling.common.utils.MapUtils;
import org.integratedmodelling.common.utils.NetUtilities;
import org.integratedmodelling.exceptions.ThinklabException;
import org.integratedmodelling.exceptions.ThinklabRuntimeException;
import org.jasypt.util.password.BasicPasswordEncryptor;
import org.jasypt.util.text.StrongTextEncryptor;

/**
 * REST-based proxy to the collaboration services on the primary server. Allows
 * programmatic access to all relevant REST services with the user's privileges.
 * Used inside User implementation so there should be no need to create one 
 * directly from there, but other service implementations may want to use the class
 * directly.
 * 
 * @author ferdinando.villa
 *
 */
public class AuthClient {

    private static final String ENCRYPTED_PASSWORD_PROPERTY = "thinklab.local.idtoken";
    private static final String AUTHENTICATION_HEADER_ID    = "Authentication";

    private static boolean _hacked = false;

    String    authToken = null;
    String    authService;
    Map<?, ?> userData;
    String    username;
    String    primaryServer;

    public static void JCEHack() {
        if (!_hacked) {
            _hacked = true;
            try {
                Field field = Class.forName("javax.crypto.JceSecurity").getDeclaredField("isRestricted");
                field.setAccessible(true);
                field.set(null, java.lang.Boolean.FALSE);
            } catch (Exception ex) {
                throw new RuntimeException("Cannot circumvent JCE policy in this JVM. Please install JCE manually.");
            }
        }
    }

    public static String getLocalizedPrivateKey(String username) {
        BasicPasswordEncryptor passwordEncryptor = new BasicPasswordEncryptor();
        String encryptedUname = passwordEncryptor.encryptPassword(username);
        return NetUtilities.getMACAddress(encryptedUname);
    }

    public static boolean hasStoredPassword() {
        return KLAB.CONFIG.getProperties().containsKey(ENCRYPTED_PASSWORD_PROPERTY);
    }

    public static boolean removeStoredPassword() {
        boolean ret = KLAB.CONFIG.getProperties().containsKey(ENCRYPTED_PASSWORD_PROPERTY);
        if (ret) {
            KLAB.CONFIG.getProperties().remove(ENCRYPTED_PASSWORD_PROPERTY);
            KLAB.CONFIG.persistProperties();
        }
        return ret;
    }

    public static void storePassword(String username, String password) {
        JCEHack();
        StrongTextEncryptor textEncryptor = new StrongTextEncryptor();
        textEncryptor.setPassword(getLocalizedPrivateKey(username));
        KLAB.CONFIG.getProperties().setProperty(ENCRYPTED_PASSWORD_PROPERTY, textEncryptor.encrypt(password));
        KLAB.CONFIG.persistProperties();
    }

    public static String getStoredPassword(String username) {
        JCEHack();
        if (KLAB.CONFIG.getProperties().containsKey(ENCRYPTED_PASSWORD_PROPERTY)) {
            StrongTextEncryptor textEncryptor = new StrongTextEncryptor();
            textEncryptor.setPassword(getLocalizedPrivateKey(username));
            return textEncryptor.decrypt(KLAB.CONFIG.getProperties().getProperty(ENCRYPTED_PASSWORD_PROPERTY));
        }
        return null;
    }

    /**
     * Authenticates the user and sets groups and roles and
     * online status. Won't generate errors if network is 
     * unreachable or authentication fails, so check the user
     * after login to know how things went.
     * 
     * @param user
     */
    public AuthClient(String authEndpoint, String user, String password) {

        this.authService = authEndpoint;

        /*
         * connect to the authenticate service and get an auth token
         * for all subsequent operations. If unsuccessful, leave the
         * token null and return with no error.
         */
        try {
            Object o = JSONdeserializer
                    .postJSON(authEndpoint + "/authentication", "username", user, "password", password);

            if (o instanceof Map) {
                Map<?, ?> map = (Map<?, ?>) o;
                authToken = map.get("authToken").toString();
                this.userData = map;
            }
        } catch (ThinklabException e) {

        }

    }

    public AuthClient(File certificate, String authEndpoint) {

        File pkey = new File(KLAB.CONFIG.getDataPath() + File.separator + "ssh" + File.separator
                + "pubring.gpg");

        if (!pkey.exists()) {
            throw new ThinklabRuntimeException("cannot read certificate: public key not found. Check your installation.");
        }

        /*
         * connect to the authenticate service and get an auth token
         * for all subsequent operations. If unsuccessful, leave the
         * token null and return with no error.
         */
        try {
            Properties properties = LicenseManager.get().readCertificate(certificate, pkey, new String[] {
                    "user",
                    "primary.server" });

            this.primaryServer = properties.getProperty("primary.server");
            this.username = properties.getProperty("user");

            this.authService = authEndpoint;
            String cert = FileUtils.readFileToString(certificate);

            // connect to authentication endpoint and retrieve auth token.
            Object o = JSONdeserializer.post(authService + "/" + Endpoints.AUTH_AUTHENTICATE_CERT_FILE, cert);

            if (o instanceof Map) {
                Map<?, ?> map = (Map<?, ?>) o;
                authToken = map.get("authToken").toString();
                this.userData = map;
            }

        } catch (Exception e) {
            throw new ThinklabRuntimeException("authentication failed: " + e.getMessage());
        }

    }

    public AuthClient(File certificate) {

        File pkey;
        try {
            pkey = new File(Resources.getEtcPath() + File.separator
                    + "pubring.gpg");
        } catch (Exception e1) {
            throw new ThinklabRuntimeException(e1.getMessage());
        }

        if (!pkey.exists()) {
            throw new ThinklabRuntimeException("cannot read certificate: public key not found. Check your installation.");
        }

        /*
         * connect to the authenticate service and get an auth token
         * for all subsequent operations. If unsuccessful, leave the
         * token null and return with no error.
         */
        try {
            Properties properties = LicenseManager.get().readCertificate(certificate, pkey, new String[] {
                    "user",
                    "primary.server" });

            this.primaryServer = properties.getProperty("primary.server");
            this.username = properties.getProperty("user");

            /*
             * hack to fix existing certfiles with the iP only , which seems to be
             * filtered by some router configs.
             */
            this.primaryServer = this.primaryServer
                    .replaceAll("150\\.241\\.131\\.4", "www.integratedmodelling.org");

            Object o = null;
            try {
                o = JSONdeserializer.get(this.primaryServer + (this.primaryServer.endsWith("/") ? "" : "/")
                        + Endpoints.IDENTIFY);
                if (o instanceof Map && ((Map<?, ?>) o).containsKey(IEngine.AUTH_ENDPOINT)) {
                    this.authService = ((Map<?, ?>) o).get(IEngine.AUTH_ENDPOINT).toString();
                    // this.authServer.replaceAll("150\\.241\\.131\\.4", "www.integratedmodelling.org");
                }
            } catch (ThinklabException e) {
                KLAB.warn("primary engine is offline or unreachable");
                this.authService = "http://integratedmodelling.org/collaboration";
            }

            if (this.authService != null) {

                String cert = FileUtils.readFileToString(certificate);
                try {
                    o = JSONdeserializer.post(authService + (authService.toString().endsWith("/") ? "" : "/")
                            + Endpoints.AUTH_AUTHENTICATE_CERT_FILE, cert);
                } catch (ThinklabException e) {
                    // authentication and expired services errors end up here.
                    throw new ThinklabRuntimeException("authentication failed: " + e.getMessage());
                }

                if (o instanceof Map) {
                    Map<?, ?> map = (Map<?, ?>) o;
                    authToken = map.get("authToken").toString();
                    this.userData = map;
                }

            }

        } catch (Exception e) {
            throw new ThinklabRuntimeException("authentication failed");
        }

    }

    public AuthClient(URL authService, String authToken) {

        this.authService = authService.toString();
        this.authToken = authToken;
        Object o;
        try {
            o = JSONdeserializer.get(authService + (authService.toString().endsWith("/") ? "" : "/")
                    + Endpoints.AUTH_PROFILE, MapUtils
                            .of(AUTHENTICATION_HEADER_ID, authToken));

            if (o instanceof Map) {
                Map<?, ?> map = (Map<?, ?>) o;
                this.userData = map;
                this.username = map.get("username").toString();
            }

        } catch (ThinklabException e) {
            this.authToken = null;
        }
    }

    public AuthClient(String authenticationEndpoint, IUser user) {
        this.authService = authenticationEndpoint;
        this.authToken = user.getSecurityKey();
        this.username = user.getUsername();
    }

    public boolean isAuthenticated() {
        return authToken != null;
    }

    public String getUsername() {
        return username;
    }

    public Map<?, ?> getUserData() {
        return userData;
    }

    /**
     * Get all data records at authentication server. Only works when authenticated with
     * direct username/password.
     * 
     * @return
     * @throws ThinklabException
     */
    public List<Datarecord> getDatasources() throws ThinklabException {

        List<Datarecord> ret = new ArrayList<>();

        if (authToken != null) {
            /*
             * TODO this only works when authenticated with password.
             */
            Object o = JSONdeserializer.getArray(authService
                    + (authService.toString().endsWith("/") ? "" : "/")
                    + Endpoints.AUTH_DATASOURCES, MapUtils.of(AUTHENTICATION_HEADER_ID, authToken));

            if (o instanceof List) {
                for (Object rec : (List<?>) o) {
                    if (rec instanceof Map) {
                        ret.add(new Datarecord((Map<?, ?>) rec));
                    }
                }
            }
        }

        return ret;
    }

    public Announcements getNotifications() throws ThinklabException {

        Announcements ret = new Announcements();

        if (authToken != null) {
            /*
             * TODO this only works when authenticated with password.
             */
            Object o = JSONdeserializer.getArray(authService
                    + (authService.toString().endsWith("/") ? "" : "/")
                    + Endpoints.AUTH_GET_ANNOUNCEMENTS, MapUtils.of(AUTHENTICATION_HEADER_ID, authToken));

            if (o instanceof List<?>) {
                @SuppressWarnings("unchecked")
                List<Map<?, ?>> data = (List<Map<?, ?>>) o;
                ret = new Announcements(authService, data);
                //
                // for (Map<?, ?> dd : data) {
                // Object zorda = JSONdeserializer.getArray(authService
                // + (authService.toString().endsWith("/") ? "" : "/")
                // + Endpoints.AUTH_GET_ANNOUNCEMENT + "/"
                // + ((Map<?, ?>) dd).get("id"), MapUtils.of(AUTHENTICATION_HEADER_ID, authToken));
                //
                // System.out.println(zorda + "");
                // }

            }
        }

        return ret;
    }

    public String getAuthenticationEndpoint() {
        return authService;
    }

    /**
     * Get the datasource with the specified urn. Return null on any error without
     * exceptions. Only works when authenticated with certificate.
     * 
     * @param urn
     * @return
     */
    public Datarecord getDatasource(String urn, String endpoint) {

        Datarecord ret = null;
        try {
            Object o = JSONdeserializer.get(authService + (authService.toString().endsWith("/") ? "" : "/")
                    + endpoint + "/"
                    + urn + ".junk", MapUtils.of(AUTHENTICATION_HEADER_ID, authToken));

            if (o instanceof Map) {
                ret = new Datarecord((Map<?, ?>) o);
            }
        } catch (Exception e) {
            if (e.getMessage().contains("[401]")) {

            }
            // just return null
        }
        return ret;
    }
}
