/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.common.editor;

import java.io.Serializable;

import org.integratedmodelling.api.modelling.IAnnotation;
import org.integratedmodelling.api.modelling.IDirectObserver;
import org.integratedmodelling.api.modelling.IKnowledgeObject;
import org.integratedmodelling.api.modelling.IModel;
import org.integratedmodelling.api.modelling.IModelObject;
import org.integratedmodelling.api.ui.IBookmark;
import org.integratedmodelling.common.configuration.KLAB;
import org.integratedmodelling.common.utils.StringUtils;

/*
 * Any bookmark. According to its class, it can be associated to different actions. All bookmarks
 * point to somewhere in the code.
 */
public class Bookmark implements IBookmark, Serializable {

    private static final long serialVersionUID = 8310496081216098912L;

    // object

    public enum Type {
        FOLDER,
        CONCEPT,
        MODEL,
        SUBJECT
    }

    public String   file;
    public int      lineNumber = -1;
    public String   message;
    public String   name;
    public String   namespace;
    public String   id;
    public String   context;
    public String[] inheritance;
    public boolean  agent      = false;

    /*
     * object type using the IThinklabElement constants. The default object is
     * obviously not a workspace, but since nothing will ever want to be it, we
     * use it as "undefined".
     */
    public Type   objectType = Type.FOLDER;
    public String objectName = null;

    static int issues = 1;

    public Bookmark() {
    }

    public Bookmark(IModelObject object) {

        objectName = object.getName();
        lineNumber = object.getFirstLineNumber();
        if (object.getNamespace().getResourceUrl() != null) {
        file = object.getNamespace().getResourceUrl().toString();
        } 
        namespace = object.getNamespace().getId();
        if (object instanceof IModel) {
            objectType = Type.MODEL;
            if (((IModel) object).getObserver() == null) {
                agent = true;
            }
        } else if (object instanceof IDirectObserver) {
            objectType = Type.SUBJECT;
        } else if (object instanceof IKnowledgeObject) {
            objectType = Type.CONCEPT;
        }
    }

    /*
     * for annotations that require further processing
     */
    public Bookmark(IModelObject object, IAnnotation a) {

        this(object);

        message = check(a.getParameters().get("what"));
        context = check(a.getParameters().get("context"));

        if (message == null)
            message = check(a.getParameters().get("description"));

        if (BookmarkManager.CLASS_CHECK.contains(a.getId())) {
            name = check(a.getParameters().get("id"));
        }

        if (a.getParameters().get("id") != null) {
            id = a.getParameters().get("id").toString();
            inheritance = id.split("/");
            name = inheritance[inheritance.length - 1];
        } else {
            name = object.getId();
            inheritance = new String[] {
                    object.getNamespace().getId(),
                    object.getId() };
            id = StringUtils.join(inheritance, "/");
        }

        if (lineNumber == -1) {
            lineNumber = a.getFirstLineNumber();
            file = a.getNamespace().getResourceUrl().toString();
        }
    }

    static String check(Object object) {
        return object == null ? null : object.toString();
    }

    public Bookmark(String path, String ns) {
        id = path;
        inheritance = id.split("/");
        name = inheritance[inheritance.length - 1];
        namespace = ns;
    }

    public Bookmark(IModelObject o, String path, String description) {

        this(o);
        message = description;
        id = path;
        inheritance = id.split("/");
        name = inheritance[inheritance.length - 1];
        namespace = o.getNamespace().getId();

    }

    public Bookmark(Bookmark bookmark) {
        // copy constructor used to quickly copy a derived non-serializable
        // bookmark
        // into a serializable one.
        this.id = bookmark.id;
        this.file = bookmark.file;
        this.lineNumber = bookmark.lineNumber;
        this.message = bookmark.message;
        this.name = bookmark.name;
        this.namespace = bookmark.namespace;
        this.inheritance = bookmark.inheritance;
        this.agent = bookmark.agent;
        this.objectType = bookmark.objectType;
        this.objectName = bookmark.objectName;
        this.message = bookmark.message;
    }

    public boolean isResolved() {

        return true;
        //
        // if (objectType == Type.FOLDER)
        // return true;
        // if (Env.MMANAGER == null) {
        // return false;
        // }
        // IModelObject mo = Env.MMANAGER.findModelObject(objectName);
        // return mo != null;
    }

    public boolean isPermanent() {
        return BookmarkManager.get().isPermanent(this);
    }

    @Override
    public boolean equals(Object obj) {

        if (id != null) {
            return obj instanceof Bookmark && ((Bookmark) obj).id != null
                    && ((Bookmark) obj).id.equals(id);
        }

        return obj instanceof Bookmark && ((Bookmark) obj).objectName != null
                && ((Bookmark) obj).objectName.equals(objectName);
    }

    @Override
    public int hashCode() {
        return id == null ? objectName.hashCode() : id.hashCode();
    }

}
