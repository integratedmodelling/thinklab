/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.common.time;

import org.integratedmodelling.api.modelling.IScale.Locator;
import org.integratedmodelling.api.modelling.scheduling.ITransition;
import org.integratedmodelling.api.runtime.IContext;
import org.integratedmodelling.api.time.ITemporalExtent;
import org.integratedmodelling.common.model.runtime.AbstractLocator;
import org.integratedmodelling.exceptions.ThinklabRuntimeException;

/**
 * Simple serializable object to use in requests for partial states. Can specify a given
 * timeslice, all of them, or the latest. Only real utility for this class is that it can 
 * be passed to IScale.locate() and recognized.
 * 
 * @author ferdinando.villa
 *
 */
public class TimeLocator extends AbstractLocator implements Locator {

    int n = -2;

    private TimeLocator(int n) {
        this.n = n;
    }

    public TimeLocator(String s) {
        if (!s.startsWith("T")) {
            throw new ThinklabRuntimeException("error parsing time locator: " + s);
        }
        n = Integer.parseInt(s.substring(1));
    }

    @Override
    public String toString() {
        return "T" + n;
    }

    public static TimeLocator get(ITransition transition) {
        return new TimeLocator(transition == null ? 0 : transition.getTimeIndex());
    }

    public static TimeLocator get(int slice) {
        return new TimeLocator(slice);
    }

    public static TimeLocator initialization() {
        return new TimeLocator(0);
    }

    /**
     * Get the locator corresponding to the passed time in a context.
     * TODO only works properly for time grids so far.
     * 
     * @param context
     * @param time
     * @return
     */
    public static TimeLocator get(IContext context, long time) {
        ITemporalExtent text = context.getSubject().getScale().getTime();
        if (text == null || time < text.getStart().getMillis() || time > text.getEnd().getMillis()) {
            return null;
        }
        int tr = (int) (text.getMultiplicity() * (double) (time - text.getStart().getMillis()) / (text
                .getEnd().getMillis() - text.getStart().getMillis()));
        return new TimeLocator(tr);
    }

    public static TimeLocator latest() {
        return new TimeLocator(-2);
    }

    public static TimeLocator all() {
        return new TimeLocator(-1);
    }

    public boolean isLatest() {
        return n == -2;
    }

    @Override
    public boolean isAll() {
        return n < 0;
    }

    public boolean isInitialization() {
        return n == 0;
    }

    public int getSlice() {
        return n;
    }

    @Override
    public int getDimensionCount() {
        return 1;
    }

    @Override
    public String asText() {
        return toString();
    }

}
