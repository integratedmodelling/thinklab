/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.common.time;

import java.util.Iterator;

import org.integratedmodelling.api.knowledge.IConcept;
import org.integratedmodelling.api.knowledge.IKnowledge;
import org.integratedmodelling.api.knowledge.IProperty;
import org.integratedmodelling.api.lang.IParseable;
import org.integratedmodelling.api.metadata.IMetadata;
import org.integratedmodelling.api.modelling.IExtent;
import org.integratedmodelling.api.modelling.IObservable;
import org.integratedmodelling.api.modelling.IObserver;
import org.integratedmodelling.api.modelling.IScale;
import org.integratedmodelling.api.modelling.IScale.Index;
import org.integratedmodelling.api.modelling.IScale.Locator;
import org.integratedmodelling.api.modelling.ITopologicallyComparable;
import org.integratedmodelling.api.modelling.storage.IStorage;
import org.integratedmodelling.api.space.ISpatialExtent;
import org.integratedmodelling.api.time.ITemporalExtent;
import org.integratedmodelling.api.time.ITimeDuration;
import org.integratedmodelling.api.time.ITimeInstant;
import org.integratedmodelling.api.time.ITimePeriod;
import org.integratedmodelling.collections.Pair;
import org.integratedmodelling.common.configuration.KLAB;
import org.integratedmodelling.common.model.runtime.AbstractLocator;
import org.integratedmodelling.common.vocabulary.NS;
import org.integratedmodelling.exceptions.ThinklabException;
import org.joda.time.Interval;

public class PeriodValue extends AbstractLocator implements ITimePeriod, IParseable, IScale.Locator {

    private Interval   value;
    protected IConcept domainConcept;

    @Override
    public boolean equals(Object o) {
        return (o instanceof PeriodValue && ((PeriodValue) o).value.equals(value)) ||
                (o instanceof Interval && o.equals(value));
    }

    @Override
    public int hashCode() {
        return value.hashCode();
    };

    @Override
    public int[] getDimensionSizes() {
        return new int[] { (int) getMultiplicity() };
    }

    @Override
    public IKnowledge getType() {
        return getObservable().getType();
    }

    @Override
    public int[] getDimensionOffsets(int linearOffset, boolean rowFirst) {
        return new int[] { linearOffset };
    }

    public PeriodValue(Interval interval, IConcept domainConcept) {
        this.value = interval;
        this.domainConcept = domainConcept;
    }

    public PeriodValue(long x, long x2, IConcept domainConcept) {
        this(new Interval(x, x2), domainConcept);
    }

    public PeriodValue(long x, long x2) {
        this(new Interval(x, x2), KLAB.c(NS.TIME_DOMAIN));
    }

    protected Interval getValue() {
        // simple wrapper for casting for consistency
        return value;
    }

    @Override
    public String toString() {
        return value.toString();
    }

    @Override
    public Object clone() {
        return new PeriodValue(getInterval(), domainConcept);
    }

    public Interval getInterval() {
        return value;
    }

    // @Override
    // public boolean is(Object other) {
    // if (!other.getClass().equals(getClass())) {
    // return false;
    // }
    // return ((PeriodValue) other).getStart().equals(getStart())
    // && ((PeriodValue) other).getEnd().equals(getEnd());
    // }

    @Override
    public String asText() {
        // ??
        return toString();
    }

    /**
     * Does this time period contain the time instant?
     *
     * (t,t+1] Semantics:
     * This class modifies the behavior of the underlying Joda Interval (and most interval semantics)
     * in that the start value is EXCLUSIVE and the end value is INCLUSIVE.
     *
     * This is to avoid circular-dependency problems during observation.
     *
     * @param time
     * @return
     */
    @Override
    public boolean contains(ITimeInstant time) {
        long millisInstant = time.getMillis();
        return contains(millisInstant);
    }

    /**
     * @see #contains(TimeValue)
     */
    @Override
    public boolean contains(long millisInstant) {
        long thisStart = getInterval().getStartMillis();
        long thisEnd = getInterval().getEndMillis();
        return (millisInstant > thisStart && millisInstant <= thisEnd);
    }

    @Override
    public double getCoveredExtent() {
        // TODO Auto-generated method stub
        return 0;
    }

    @Override
    public boolean endsBefore(ITimeInstant instant) {
        return (getEnd().compareTo(instant) < 0);
    }

    @Override
    public boolean endsBefore(ITemporalExtent other) {
        return (getEnd().compareTo(other.getStart()) < 0);
    }

    @Override
    public boolean overlaps(ITemporalExtent other) {
        return (getStart().compareTo(other.getEnd()) < 0) && (getEnd().compareTo(other.getStart()) > 0);
    }

    @Override
    public ITimeInstant getStart() {
        return new TimeValue(getValue().getStartMillis());
    }

    @Override
    public ITimeInstant getEnd() {
        return new TimeValue(getValue().getEndMillis());
    }

    @Override
    public ITemporalExtent getExtent(int stateIndex) {
        if (stateIndex != 0) {
            return null;
        }
        return this;
    }

    @Override
    public IProperty getDomainProperty() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public IProperty getCoverageProperty() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public ITimePeriod collapse() {
        return this;
    }

    @Override
    public boolean isCovered(int stateIndex) {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public IExtent merge(IExtent extent, boolean force) throws ThinklabException {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public Pair<ITopologicallyComparable<?>, Double> checkCoverage(ITopologicallyComparable<?> obj)
            throws ThinklabException {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public boolean isConsistent() {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public boolean isEmpty() {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public long getValueCount() {
        return 1;
    }

    @Override
    public IObserver getObserver() {
        return null;
    }

    @Override
    public boolean isSpatiallyDistributed() {
        return false;
    }

    @Override
    public boolean isTemporallyDistributed() {
        return true;
    }

    @Override
    public boolean isTemporal() {
        return true;
    }

    @Override
    public boolean isSpatial() {
        return false;
    }

    @Override
    public ISpatialExtent getSpace() {
        return null;
    }

    @Override
    public ITemporalExtent getTime() {
        return this;
    }

    @Override
    public IMetadata getMetadata() {
        return null;
    }

    @Override
    public IObservable getObservable() {
        return null;
    }

    @Override
    public long getMultiplicity() {
        return 1;
    }

    @Override
    public ITemporalExtent intersection(IExtent other) throws ThinklabException {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public ITemporalExtent union(IExtent other) throws ThinklabException {
        if (!(other instanceof ITemporalExtent) || !overlaps(other)) {
            return null;
        }
        ITimeInstant otherStart = ((ITemporalExtent) other).getStart();
        ITimeInstant otherEnd = ((ITemporalExtent) other).getStart();
        long start = otherStart.compareTo(getStart()) < 0 ? otherStart.getMillis() : getValue()
                .getStartMillis();
        long end = otherEnd.compareTo(getEnd()) > 0 ? otherEnd.getMillis() : getValue().getEndMillis();
        return new PeriodValue(start, end, domainConcept);
    }

    @Override
    public boolean contains(IExtent other) throws ThinklabException {
        if (!(other instanceof ITemporalExtent)) {
            return false;
        }
        return contains(((ITemporalExtent) other).getStart()) && contains(((ITemporalExtent) other).getEnd());
    }

    @Override
    public boolean overlaps(IExtent other) throws ThinklabException {
        if (!(other instanceof ITemporalExtent)) {
            return false;
        }
        return overlaps((ITemporalExtent) other);
    }

    @Override
    public boolean intersects(IExtent other) throws ThinklabException {
        if (!(other instanceof ITemporalExtent)) {
            return false;
        }
        // TODO
        return false;
    }

    @Override
    public ITopologicallyComparable<IExtent> union(ITopologicallyComparable<?> other)
            throws ThinklabException {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public ITopologicallyComparable<IExtent> intersection(ITopologicallyComparable<?> other)
            throws ThinklabException {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public IScale getScale() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public Object getValue(int index) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public Iterator<Object> iterator(Index index) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public ITemporalExtent getExtent() {
        /*
         * interval implements the temporal semantics agreed for the agent system.
         */
        return this; /* new NumericInterval((double) getStart().getMillis(), (double) getEnd().getMillis(), false,
                     true); */
    }

    @Override
    public ITimeDuration getStep() {
        return new DurationValue(getEnd().getMillis() - getStart().getMillis());
    }

    @Override
    public long getMillis() {
        return getInterval().getEndMillis() - getInterval().getStartMillis();
    }

    @Override
    public IStorage<?> getStorage() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public int locate(Locator locator) {
        return locator.equals(this) ? 0 : -1;
    }

    @Override
    public IConcept getDomainConcept() {
        // TODO Auto-generated method stub
        return null;
    }

    // @Override
    // public IConcept getDirectType() {
    // // TODO Auto-generated method stub
    // return null;
    // }
    //
    // @Override
    // public INamespace getNamespace() {
    // // TODO Auto-generated method stub
    // return null;
    // }

    @Override
    public int getDimensionCount() {
        return 1;
    }

    @Override
    public boolean isAll() {
        return false;
    }

    @Override
    public Mediator getMediator(IExtent extent, IObservable observable, IConcept trait) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public boolean isConstant() {
        return getMultiplicity() == 1;
    }

}
