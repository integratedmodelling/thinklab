/*******************************************************************************
 *  Copyright (C) 2007, 2014:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.common.time;

import org.integratedmodelling.api.time.ITimeDuration;
import org.integratedmodelling.api.time.ITimeInstant;
import org.integratedmodelling.collections.Pair;
import org.joda.time.DateTime;
import org.joda.time.DateTimeConstants;

/**
 * A parsed literal representing a time duration. Linked to the DurationValue OWL class to be used as an
 * extended literal for it. Maximum resolution is milliseconds. Can be initialized
 * by a string expressing number with units; unit must express time and the syntax is the one
 * supported by the implementation of IUnit.
 *
 * @author Ferdinando Villa
 * FIXME make it wrap something real
 * FIXME resolve ambiguity with the "semantic literal" version in engine package
 */
public class DurationValue implements ITimeDuration {

    String literal      = null;
    int    precision    = TemporalPrecision.MILLISECOND;
    int    origQuantity = 1;
    String origUnit     = "";
    long   value;

    public DurationValue(long l) {
        value = l;
    }

    @Override
    public String toString() {
        return literal == null ? (value + " ms") : literal;
    }

    @Override
    public long getMilliseconds() {
        return value;
    }

    @Override
    public Pair<ITimeInstant, ITimeInstant> localize() {

        DateTime date = new DateTime();
        TimeValue start = null, end = null;
        long val = value;

        switch (precision) {

        case TemporalPrecision.MILLISECOND:
            start = new TimeValue(date);
            end = new TimeValue(date.plus(val));
            break;
        case TemporalPrecision.SECOND:
            val = value / DateTimeConstants.MILLIS_PER_SECOND;
            start = new TimeValue(new DateTime(date.getYear(), date.getMonthOfYear(), date.getDayOfMonth(),
                    date.getHourOfDay(), date.getMinuteOfHour(), date.getSecondOfMinute(), 0));
            end = new TimeValue(start.getTimeData().plusSeconds((int) val));
            break;
        case TemporalPrecision.MINUTE:
            val = value / DateTimeConstants.MILLIS_PER_MINUTE;
            start = new TimeValue(new DateTime(date.getYear(), date.getMonthOfYear(), date.getDayOfMonth(),
                    date.getHourOfDay(), date.getMinuteOfHour(), 0, 0));
            end = new TimeValue(start.getTimeData().plusMinutes((int) val));
            break;
        case TemporalPrecision.HOUR:
            val = value / DateTimeConstants.MILLIS_PER_HOUR;
            start = new TimeValue(new DateTime(date.getYear(), date.getMonthOfYear(), date.getDayOfMonth(),
                    date.getHourOfDay(), 0, 0, 0));
            end = new TimeValue(start.getTimeData().plusHours((int) val));
            break;
        case TemporalPrecision.DAY:
            val = value / DateTimeConstants.MILLIS_PER_DAY;
            start = new TimeValue(new DateTime(date.getYear(), date.getMonthOfYear(), date.getDayOfMonth(),
                    0, 0, 0, 0));
            end = new TimeValue(start.getTimeData().plusDays((int) val));
            break;
        case TemporalPrecision.MONTH:
            start = new TimeValue(new DateTime(date.getYear(), date.getMonthOfYear(), 1, 0, 0, 0, 0));
            end = new TimeValue(start.getTimeData().plusMonths(origQuantity));
            break;
        case TemporalPrecision.YEAR:
            start = new TimeValue(new DateTime(date.getYear(), 1, 1, 0, 0, 0, 0));
            end = new TimeValue(start.getTimeData().plusYears(origQuantity));
            break;
        }

        return new Pair<ITimeInstant, ITimeInstant>(start, end);
    }

    public int getOriginalQuantity() {
        return origQuantity;
    }

    public String getOriginalUnit() {
        return origUnit;
    }

    @Override
    public int compareTo(ITimeDuration other) {
        long otherValue = other.getMilliseconds();
        if (value == otherValue) {
            return 0;
        }
        if (value < otherValue) {
            return -1;
        }
        return 1;
    }
}
