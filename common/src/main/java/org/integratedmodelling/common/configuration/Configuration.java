/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.common.configuration;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.Properties;

import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;
import org.integratedmodelling.api.configuration.IConfiguration;
import org.integratedmodelling.api.monitoring.INotification;
import org.integratedmodelling.collections.OS;
import org.integratedmodelling.exceptions.ThinklabRuntimeException;

/**
 * Global Thinklab configuration. Thinklab proxies to one instance
 * of this.
 * 
 * Global Thinklab properties are by default in ${user.home}/.thinklab/thinklab.properties.
 * 
 * @author Ferd
 *
 */
public class Configuration implements IConfiguration {

    static OS os = null;

    // default work dir unless redefined through an environmental variable or system property.
    private static String thinklabWorkDirectoryName = ".tl";

    /*
     * all configurable paths; others are derived from them.
     */
    File           _scratchPath;
    File           _dataPath;
    Properties     _properties;
    private int    _notificationLevel;
    private String _adminKey;

    public Configuration(boolean setupLogging) {

        if (KLAB.getLogger() != null && setupLogging) {
            KLAB.setLogger(Logger.getLogger("k.lab"));
        }
        
        if (System.getProperty(THINKLAB_DATA_DIRECTORY_PROPERTY) != null) {

            this._dataPath = new File(System.getProperty(THINKLAB_DATA_DIRECTORY_PROPERTY));
            this._scratchPath = new File(this._dataPath + File.separator + ".scratch");
        
        } else {
            String home = System.getProperty("user.home");
            if (System.getProperty(THINKLAB_WORK_DIRECTORY_PROPERTY) != null) {
                thinklabWorkDirectoryName = System.getProperty(THINKLAB_WORK_DIRECTORY_PROPERTY);
            }
            this._dataPath = new File(home + File.separator + thinklabWorkDirectoryName);
            this._scratchPath = new File(this._dataPath + File.separator + ".scratch");
        }
        

        if (getProperties().containsKey(THINKLAB_ADMINISTRATION_KEY)) {
            _adminKey = getProperties().getProperty(THINKLAB_ADMINISTRATION_KEY);
        }

        _notificationLevel = INotification.INFO;
        if (getProperties().containsKey("thinklab.client.debug")) {
            if (getProperties().getProperty("thinklab.client.debug").equals("on")) {
                _notificationLevel = INotification.DEBUG;
            }
        }
    }

    @Override
    public Properties getProperties() {

        if (_properties == null) {
            /*
             * load or create thinklab system properties
             */
            _properties = new Properties();
            File properties = new File(_dataPath + File.separator + "thinklab.properties");
            try {
                if (properties.exists()) {
                    FileInputStream input;

                    input = new FileInputStream(properties);
                    _properties.load(input);
                    input.close();
                } else {
                    FileUtils.touch(properties);
                }
            } catch (Exception e) {
                throw new ThinklabRuntimeException(e);
            }
        }
        return _properties;
    }

    @Override
    public void persistProperties() {
        File td = new File(_dataPath + File.separator + "thinklab.properties");

        try {
            getProperties().store(new FileOutputStream(td), null);
        } catch (Exception e) {
            throw new ThinklabRuntimeException(e);
        }

    }

    @Override
    public File getDataPath(String subspace) {
        File ret = new File(_dataPath + File.separator + subspace);
        ret.mkdirs();
        return ret;
    }

    @Override
    public File getScratchArea() {
        return _scratchPath;
    }

    @Override
    public File getScratchArea(String subArea) {
        File ret = new File(_scratchPath + File.separator + subArea);
        ret.mkdirs();
        return ret;
    }

    @Override
    public File getTempArea(String subArea) {

        File ret = new File(_scratchPath + File.separator + "tmp");
        if (subArea != null) {
            ret = new File(ret + File.separator + subArea);
        }
        boolean exists = ret.exists();
        ret.mkdirs();

        if (!exists) {
            ret.deleteOnExit();
        }

        return ret;
    }

    @Override
    public String getWorkDirectoryName() {
        return thinklabWorkDirectoryName;
    }

    @Override
    public File getDataPath() {
        return _dataPath;
    }

    /**
     * Quickly and unreliably retrieve the class of OS we're running on.
     * @return
     */
    @Override
    public OS getOS() {

        if (os == null) {

            String osd = System.getProperty("os.name").toLowerCase();

            // TODO ALL these checks need careful checking
            if (osd.contains("windows")) {
                os = OS.WIN;
            } else if (osd.contains("mac")) {
                os = OS.MACOS;
            } else if (osd.contains("linux") || osd.contains("unix")) {
                os = OS.UNIX;
            }

        }

        return os;
    }

    @Override
    public int getNotificationLevel() {
        return _notificationLevel;
    }

    @Override
    public Object getAdminKey() {
        return _adminKey;
    }

    @Override
    public boolean isDebug() {
        return getProperties().getProperty("thinklab.client.debug", "off").equals("on");
    }

    @Override
    public int getDebugLevel() {
        return Integer.parseInt(getProperties().getProperty("thinklab.debug.level", "0"));
    }
}
