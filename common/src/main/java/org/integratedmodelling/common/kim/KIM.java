/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.common.kim;

import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.UUID;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.xtext.ISetup;
import org.eclipse.xtext.nodemodel.INode;
import org.eclipse.xtext.resource.IResourceFactory;
import org.eclipse.xtext.resource.XtextResource;
import org.eclipse.xtext.resource.XtextResourceSet;
import org.integratedmodelling.ThinkQLStandaloneSetup;
import org.integratedmodelling.api.knowledge.IConcept;
import org.integratedmodelling.api.knowledge.IProperty;
import org.integratedmodelling.api.lang.IList;
import org.integratedmodelling.api.lang.IParsingContext;
import org.integratedmodelling.api.modelling.IKnowledgeObject;
import org.integratedmodelling.api.modelling.IModelObject;
import org.integratedmodelling.api.modelling.INamespace;
import org.integratedmodelling.api.monitoring.IKnowledgeLifecycleListener;
import org.integratedmodelling.collections.NumericInterval;
import org.integratedmodelling.collections.Pair;
import org.integratedmodelling.common.configuration.KLAB;
import org.integratedmodelling.common.vocabulary.NS;
import org.integratedmodelling.exceptions.ThinklabException;
import org.integratedmodelling.exceptions.ThinklabIOException;
import org.integratedmodelling.lang.SemanticType;
import org.integratedmodelling.list.PolyList;
import org.integratedmodelling.thinkQL.Annotation;
import org.integratedmodelling.thinkQL.ContextualizeStatement;
import org.integratedmodelling.thinkQL.DefineStatement;
import org.integratedmodelling.thinkQL.Literal;
import org.integratedmodelling.thinkQL.Model;
import org.integratedmodelling.thinkQL.NUMBER;
import org.integratedmodelling.thinkQL.ResolutionStatement;
import org.integratedmodelling.thinkQL.RoleStatement;
import org.integratedmodelling.thinkQL.Statement;
import org.integratedmodelling.thinkQL.Value;

import com.google.inject.Inject;
import com.google.inject.Injector;

/**
 * Translates KIM input into k.LAB API objects. The main operation is parse(), which returns a namespace. 
 * 
 * @author Ferd
 * 
 */
public class KIM {

    /*
     * TODO change to .kim later (after making sure something automatic is in place to change it).
     */
    public static final String FILE_EXTENSION = "tql";

    /*
    * the following are keys for concepts that the upper ontology is expected to provide. Concept
    definitions
    * will be automatically derived from these according to the idiom used to declare them.
    */
    public static final String OBJECT_CONCEPT             = "object";
    public static final String ATTRIBUTE_CONCEPT          = "attribute";
    public static final String REALM_CONCEPT              = "realm";
    public static final String PROCESS_CONCEPT            = "process";
    public static final String QUALITY_CONCEPT            = "quality";
    public static final String QUANTITY_CONCEPT           = "quantity";
    public static final String QUALITY_SPACE_CONCEPT      = "class";
    public static final String IDENTITY_CONCEPT           = "identity";
    public static final String ORDERING_CONCEPT           = "ordering";
    public static final String DOMAIN_CONCEPT             = "domain";
    public static final String ROLE_CONCEPT               = "role";
    public static final String THING_CONCEPT              = "thing";
    public static final String ENERGY_CONCEPT             = "energy";
    public static final String ENTROPY_CONCEPT            = "entropy";
    public static final String LENGTH_CONCEPT             = "length";
    public static final String MASS_CONCEPT               = "mass";
    public static final String VOLUME_CONCEPT             = "volume";
    public static final String WEIGHT_CONCEPT             = "weight";
    public static final String DURATION_CONCEPT           = "duration";
    public static final String MONETARY_VALUE_CONCEPT     = "money";
    public static final String PREFERENCE_VALUE_CONCEPT   = "priority";
    public static final String ACCELERATION_CONCEPT       = "acceleration";
    public static final String AREA_CONCEPT               = "area";
    public static final String DENSITY_CONCEPT            = "density";
    public static final String ELECTRIC_POTENTIAL_CONCEPT = "electric-potential";
    public static final String CHARGE_CONCEPT             = "charge";
    public static final String RESISTANCE_CONCEPT         = "resistance";
    public static final String RESISTIVITY_CONCEPT        = "resistivity";
    public static final String PRESSURE_CONCEPT           = "pressure";
    public static final String SLOPE_CONCEPT              = "angle";
    public static final String SPEED_CONCEPT              = "velocity";
    public static final String PATTERN_CONCEPT            = "configuration";
    public static final String TEMPERATURE_CONCEPT        = "temperature";
    public static final String VISCOSITY_CONCEPT          = "viscosity";
    public static final String AGENT_CONCEPT              = "agent";
    public static final String EVENT_CONCEPT              = "event";
    public static final String DELIBERATIVE_AGENT_CONCEPT = "deliberative-agent";
    public static final String REACTIVE_AGENT_CONCEPT     = "reactive-agent";
    public static final String SOCIAL_AGENT_CONCEPT       = "social-agent";
    public static final String ORGANIZED_AGENT_CONCEPT    = "organized-agent";
    public static final String SUBJECTIVE_SPECIFIER       = "subjective";

    @Inject
    public KIM() {
    }

    /**
     * Parse the passed resource into a namespace. Throw an exception only if the resource can't be
     * accessed; otherwise the resulting namespace will have errors and no exception will be thrown.
     * 
     * The passed context determines whether the resource is part of a project. If an input stream
     * is passed, the context must contain a namespace ID already.
     * 
     * @param resource
     * @param context
     * @return
     * @throws ThinklabException
     */
    public static INamespace parse(Object resource, IParsingContext context)
            throws ThinklabException {

        /*
         * don't parse twice in the same context.
         */
        if (((KIMContext) context).getId() != null && context.hasSeen(((KIMContext) context).getId())) {
            return KLAB.MMANAGER.getNamespace(((KIMContext) context).getId());
        }

        /*
         * at this point if we have it, it comes from a previous parsing so we release it.
         */
        if (KLAB.MMANAGER.getNamespace(((KIMContext) context).getId()) != null) {
            KLAB.MMANAGER.releaseNamespace(((KIMContext) context).getId());
        }

        KIMContext ctx = (KIMContext) context.forResource(resource);
        if (ctx.getInputStream() == null) {
            throw new ThinklabIOException("resource for namespace " + ctx.getId() + " cannot be accessed");
        }

        // System.out.println("----------------- parsing " + ((KIMContext) context).getId()
        // + " ----------------------------------");

        long t = System.nanoTime();

        INamespace ret = parseStream(ctx);
        ctx.close();

        long elapsed = (System.nanoTime() - t) / 1000000L;

        /*
         * warn for long-running parses
         */
        if (elapsed > 3 && ret != null) {
            KLAB.info("parsing of " + ((KIMContext) context).getId() + " took " + elapsed +
                    " milliseconds; namespace read took " + ((KIMNamespace) ret).elapsedMs);
        }

        return ret;
    }

    static INamespace parseStream(KIMContext context) {

        ISetup setup = new ThinkQLStandaloneSetup();
        Injector injector = setup.createInjectorAndDoEMFRegistration();
        XtextResourceSet rs = injector.getInstance(XtextResourceSet.class);
        rs.setClasspathURIContext(KIM.class);

        IResourceFactory resourceFactory = injector.getInstance(IResourceFactory.class);

        URI uri = URI.createURI("thinklab://" + UUID.randomUUID());
        XtextResource resource = (XtextResource) resourceFactory.createResource(uri);
        rs.getResources().add(resource);

        // if (!resource.getErrors().isEmpty())
        // return null;

        try {
            resource.load(context.getInputStream(), null);
        } catch (Exception e) {
            context.error(e.getMessage(), 0);
        }

        EcoreUtil.resolveAll(resource);

        if (resource.getParseResult() == null) {
            /*
             * either empty file or read error - both exceptions.
             */
            context.error("read error or empty file", 0);
            return null;
        }

        List<Pair<String, Integer>> syntaxErrors = new ArrayList<>();
        if (resource.getParseResult().hasSyntaxErrors()) {
            // syntax errors occured - handle them properly. We don't have the namespace now, so save them
            // and report after process().
            for (INode n : resource.getParseResult().getSyntaxErrors()) {
                syntaxErrors.add(new Pair<>("syntax error: " + n.getText(), n.getStartLine()));
            }
        }

        /*
         * this sets the namespace in the context
         */
        long start = System.nanoTime();
        Model root = (Model) resource.getParseResult().getRootASTElement();
        if (root.getNamespace() == null) {
            return null;
        }
        KIMNamespace namespace = new KIMNamespace(root.getNamespace(), root
                .getNamespace().getAnnotations(), context);
        if (KLAB.CONFIG.getDebugLevel() > 99) {
            KLAB.info("   read of " + namespace.getId() + " header took "
                    + (System.nanoTime() - start) / 1000000);
        }

        try {
            long elapsed = process(namespace, root, context);
            for (Pair<String, Integer> zio : syntaxErrors) {
                context.error(zio.getFirst(), zio.getSecond());
            }
            namespace.elapsedMs = elapsed;
        } catch (Throwable e) {
            context.error("unrecoverable error reading namespace: " + e.getMessage(), 0);
        }
        return namespace;
    }

    /**
     * Process the parsed model and return the corresponding namespace. If the model doesn't define a
     * namespace, use the default (user) namespace, creating it if not existing, and add the statements to it.
     * 
     * @param root
     * @param expectedNamespace
     * @return the namespace. If a user namespace was defined earlier and not specified in the model, the
     * statements in the model will be appended to it.
     * @throws ThinklabException
     */
    private static long process(KIMNamespace namespace, Model root, KIMContext context) {

        long startT = System.nanoTime();

        if (KLAB.MMANAGER != null) {
            ((KIMModelManager) KLAB.MMANAGER).addNamespace(namespace);
        }

        for (IKnowledgeLifecycleListener l : ((ModelFactory) KLAB.MFACTORY).knowledgeListeners) {
            l.namespaceDeclared(namespace);
        }

        /*
         * process all statements
         */
        for (Statement statement : root.getStatements()) {

            KIMModelObject ret = null;

            long start = System.nanoTime();

            if (statement.getModel() != null) {

                ret = new KIMModel(context.get(KIMContext.MODEL).firstClass(), statement
                        .getModel(), null, statement
                                .getAnnotations());

                if (ret != null) {
                    namespace.addModelObject(ret);
                }

            } else if (statement.getConcept() != null) {

                ret = new KIMKnowledge(context.get(KIMContext.CONCEPT).firstClass(), statement
                        .getConcept(), null, statement
                                .getAnnotations());

                if (!statement.getConcept().isRoot()) {
                    if (ret != null) {
                        namespace.addModelObject(ret);
                    }
                }

            } else if (statement.getProperty() != null) {

                ret = new KIMKnowledge(context.get(KIMContext.PROPERTY).firstClass(), statement
                        .getProperty(), null, statement
                                .getAnnotations());

                if (ret != null) {
                    namespace.addModelObject(ret);
                }

            } else if (statement.getDefine() != null) {

                /*
                 * TODO
                 */
                DefineStatement define = statement.getDefine();
                Object o = define.getValue() == null ? null : processLiteral(context, define.getValue());

                // if (define.getObject() != null) {
                // /*
                // * we get an object, we submit a list for further processing. Resolver can intercept
                // specific
                // * concepts and dispatch them to catalogs accordingly.
                // */
                // String id = define.getValue().getId();
                // IList object = processObject(define.getValue());
                // defineSymbol(id, object, lineNumber(define));
                //
                /* } else */
                if (define.getTable() != null) {
                    KIMLookupTable lut = new KIMLookupTable(context.get(KIMContext.LOOKUP_TABLE), define
                            .getTable());
                    namespace.symbolTable.put(define.getName(), lut);
                } else if (o != null) {
                    namespace.symbolTable.put(define.getName(), o);
                }

            } else if (statement.getObserve() != null) {

                ret = new KIMDirectObserver(context.get(KIMContext.OBSERVATION).firstClass(), statement
                        .getObserve(), null, statement
                                .getAnnotations());

                if (ret != null) {
                    namespace.addModelObject(ret);
                }

            } else if (statement.getContextualization() != null) {

                processContextualization(context, statement.getContextualization(), statement
                        .getAnnotations());
            }

            if (ret != null) {

                long elapsed1 = (System.nanoTime() - start) / 1000000L;
                for (IKnowledgeLifecycleListener l : ((ModelFactory) KLAB.MFACTORY).knowledgeListeners) {
                    l.objectDefined(ret);
                }
                long elapsed2 = (System.nanoTime() - start) / 1000000L;

                if (KLAB.CONFIG.isDebug() && KLAB.CONFIG.getDebugLevel() > 99) {
                    KLAB
                            .info("   took " + elapsed2 + " ms to process " + ret + "; reading took "
                                    + elapsed1);
                }
            }
        }

        long elapsedT = (System.nanoTime() - startT) / 1000000L;
        if (KLAB.CONFIG.isDebug() && KLAB.CONFIG.getDebugLevel() > 99) {
            KLAB.info("   TOTAL took " + elapsedT + " ms to process all objects");
        }

        namespace.processAnnotations(context, root.getNamespace());

        /*
         * record exports in catalog
         */
        ((KIMModelManager) KLAB.MMANAGER).addExports(namespace.getExportedKnowledgeMap());

        for (IKnowledgeLifecycleListener l : ((ModelFactory) KLAB.MFACTORY).knowledgeListeners) {
            l.namespaceDefined(namespace);
        }

        elapsedT = (System.nanoTime() - startT) / 1000000L;
        if (KLAB.CONFIG.isDebug() && KLAB.CONFIG.getDebugLevel() > 99) {
            KLAB.info("   FINAL took " + elapsedT + " to process all objects");
        }

        return elapsedT;
    }

    private static void processContextualization(KIMContext context, ContextualizeStatement statement, EList<Annotation> annotations) {

        KIMKnowledge k = new KIMKnowledge(context.get(KIMContext.CONTEXTUALIZED_CONCEPT), statement
                .getConcept());

        if (k.isNothing()) {
            return;
        }

        if (statement.getResolutionStatement() != null) {
            for (ResolutionStatement resol : statement.getResolutionStatement()) {
                /*
                 * TODO create resolution model
                 */
            }
        }

        if (statement.getRoleStatement() != null) {
            for (RoleStatement rolest : statement.getRoleStatement()) {

                for (IKnowledgeObject role : new KIMConceptList(context
                        .get(KIMContext.CONTEXTUALIZED_ROLE), rolest
                                .getRole(), null)) {

                    if (role.isNothing() || role.getConcept() == null || !NS.isTrait(role)) {
                        context.error(role
                                + " is not a role", KIMLanguageObject.lineNumber(rolest.getRole()));
                        continue;
                    }

                    IConcept trait = role.getConcept();

                    List<IKnowledgeObject> targets = new ArrayList<>();
                    if (rolest.getTargetObservable() != null) {
                        for (IKnowledgeObject rob : new KIMConceptList(context
                                .get(KIMContext.ROLE_TARGET_OBSERVABLE_LIST), rolest
                                        .getTargetObservable(), null)) {
                            targets.add(rob);
                        }
                    }
                    if (targets.isEmpty()) {
                        targets.add(null);
                    }
                    for (IKnowledgeObject target : targets) {

                        IConcept trg = target == null ? null : target.getConcept();

                        if (trg != null && !NS.isCountable(trg)) {
                            context.error("the target context ('for') of a role must be a subject or an event", KIMLanguageObject
                                    .lineNumber(rolest
                                            .getTargetObservable()));
                        }

                        for (IKnowledgeObject rob : new KIMConceptList(context
                                .get(KIMContext.ROLE_RESTRICTED_OBSERVABLE_LIST), rolest
                                        .getRestrictedObservable(), null)) {

                            IConcept restricted = KLAB.c(rob.getName());

                            if (restricted == null
                                    || !(NS.isCountable(restricted) || NS.isProcess(restricted))) {
                                context.error("the target scenario ('in') of a role must be a process, subject or event", KIMLanguageObject
                                        .lineNumber(rolest
                                                .getRestrictedObservable()));
                            }
                            NS.addRole(trait, k.getConcept(), trg, restricted, context
                                    .getNamespace());
                        }

                    }

                }
            }
        }

    }

    static Object processLiteral(KIMContext context, Value value) {

        Object ret = null;
        if (value.getFunction() != null) {
            ret = new KIMFunctionCall(context.get(KIMContext.FUNCTIONCALL), value.getFunction());
            // ret = processFunctionCall(value.getFunction());
            // resolver.validateFunctionCall((IFunctionCall) ret);
        } else if (value.getLiteral() != null) {
            ret = processLiteral(value.getLiteral());
        } else if (value.getId() != null) {
            //
            // ret = resolver.getSymbolTable().get(value.getId());
            // if (ret == null) {
            // resolver.onException(new ThinklabValidationException("unknown symbol in function call: "
            // + value.getId()), lineNumber(value));
            // }
        } else if (value.getExpr() != null) {

            return new KIMExpression(context, value);

        } else if (value.getList() != null) {

            return nodeToList(value.getList());

        } else if (value.isNull()) {

            // not really necessary, but OK
            return null;
        }

        return ret;
    }

    public static Object processLiteral(Literal l) {

        if (l.getBoolean() != null) {
            return l.getBoolean().equals("true") ? Boolean.TRUE : Boolean.FALSE;
        }
        if (l.getFrom() != null) {
            return new NumericInterval(processNumber(l.getFrom()).doubleValue(), processNumber(l.getTo())
                    .doubleValue(), false, true);
        }
        if (l.getId() != null && KLAB.KM != null) {
            if (SemanticType.validate(l.getId())) {
                IConcept c = KLAB.KM.getConcept(l.getId());
                if (c != null)
                    return c;
                IProperty p = KLAB.KM.getProperty(l.getId());
                if (p != null)
                    return p;

            } else {

                /*
                 * TODO symbol from symbol table - requires context
                 */

                /*
                 * FIXME not sure of this heuristics for concepts
                 */
                // if (l.getId().length() > 1 && Character.isUpperCase(l.getId().charAt(0))) {
                //
                // /*
                // * symbol should be a concept
                // * TODO check if a property is OK here too - for now disabling.
                // */
                // IKnowledgeDefinition conc = declareConcept(Collections
                // .singleton(l.getId()), resolver, null, lineNumber(l), false, false, false, true);
                // resolver.getNamespace().synchronizeKnowledge(resolver);
                // return Env.c(conc.getName());
                // }

            }
            return l.getId();
        }
        if (l.getNumber() != null)
            return processNumber(l.getNumber());
        if (l.getString() != null)
            return l.getString();
        return null;
    }

    public static Number processNumber(NUMBER lid) {

        Number ret = null;
        int n = lid.getInt();
        if (lid.getFloat() != null) {
            ret = Double.parseDouble(lid.getFloat());
        } else if (lid.getSint() != null) {
            ret = Integer.parseInt(lid.getSint());
        } else {
            ret = new Integer(n);
        }

        return ret;
    }

    public static void main(String[] args) throws Exception {

        KLAB.bootMinimal();
        KLAB.PMANAGER.registerProjectDirectory(new File(System.getProperty("user.home") + "/git"));
        KLAB.PMANAGER.loadProject("thinklab.testsuite", KLAB.MFACTORY.getRootParsingContext());
        System.exit(0);
    }

    static protected IList nodeToList(org.integratedmodelling.thinkQL.List list) {

        IList ret = null;

        if (list != null) {

            ArrayList<Object> ct = new ArrayList<Object>();

            for (EObject zio : list.getContents()) {
                if (zio instanceof org.integratedmodelling.thinkQL.List)
                    ct.add(nodeToList((org.integratedmodelling.thinkQL.List) zio));
                else if (zio instanceof Literal) {
                    // treat commas inside lists as whitespace. TODO we may use them for
                    // grouping at some point.
                    if (!((Literal) zio).isComma()) {
                        ct.add(KIM.processLiteral((Literal) zio));
                    }
                }
            }

            ret = PolyList.fromCollection(ct);
        }

        return ret;
    }

    /**
     * Flatten the model object hierarchy and return all declared children of passed object, including the
     * object itself.
     * 
     * @param o
     * @return
     */
    public static Collection<IModelObject> getAllChildren(IModelObject o) {
        List<IModelObject> ret = new ArrayList<>();
        addChildren(o, ret);
        return ret;
    }

    private static void addChildren(IModelObject o, List<IModelObject> ret) {
        ret.add(o);
        for (IModelObject oo : o.getChildren()) {
            addChildren(oo, ret);
        }
    }

}
