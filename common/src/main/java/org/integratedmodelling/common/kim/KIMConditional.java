/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.common.kim;

import java.util.ArrayList;
import java.util.List;

import org.integratedmodelling.api.knowledge.IConcept;
import org.integratedmodelling.api.knowledge.ICondition;
import org.integratedmodelling.api.knowledge.IExpression;
import org.integratedmodelling.api.modelling.IAction;
import org.integratedmodelling.api.modelling.IConditionalObserver;
import org.integratedmodelling.api.modelling.IModel;
import org.integratedmodelling.api.modelling.IObservable;
import org.integratedmodelling.api.modelling.IObserver;
import org.integratedmodelling.api.modelling.runtime.IActuator;
import org.integratedmodelling.api.modelling.runtime.IStateActuator;
import org.integratedmodelling.api.modelling.runtime.ISwitchingActuator;
import org.integratedmodelling.api.modelling.scheduling.ITransition;
import org.integratedmodelling.api.monitoring.IMonitor;
import org.integratedmodelling.collections.Pair;
import org.integratedmodelling.common.model.actuators.StateActuator;
import org.integratedmodelling.common.thinkql.GroovyExpression;
import org.integratedmodelling.exceptions.ThinklabException;
import org.integratedmodelling.exceptions.ThinklabRuntimeException;
import org.integratedmodelling.exceptions.ThinklabValidationException;
import org.integratedmodelling.lang.IRemoteSerializable;
import org.integratedmodelling.thinkQL.ObservationGeneratorSwitch;

public class KIMConditional extends KIMObserver implements IConditionalObserver {

    List<Pair<IModel, IExpression>> models   = new ArrayList<>();
    boolean                         reported = false;

    public class ConditionalActuator extends StateActuator implements ISwitchingActuator {

        public ConditionalActuator(List<IAction> actions, IMonitor monitor) {
            super(actions, monitor);
        }

        boolean preprocessed = false;

        @Override
        public void processState(int stateIndex, ITransition transition) throws ThinklabException {
        }

        @Override
        public boolean getConditionValue(int condition) throws ThinklabException {

            if (!preprocessed) {
                preprocess();
            }

            IExpression cond = models.get(condition).getSecond();
            try {
                Object iif = cond.eval(_parameters, getMonitor());
                if (!(iif instanceof Boolean)) {
                    monitor.error(new ThinklabValidationException("condition in observer does not evaluate to true or false"));
                }
                if (cond instanceof ICondition && ((ICondition) cond).isNegated()) {
                    iif = !((Boolean) iif);
                }
                return (Boolean) iif;
            } catch (Exception e) {
                if (!reported) {
                    reported = true;
                    throw new ThinklabValidationException(e);
                }
            }
            return false;
        }

        private synchronized void preprocess() {
            preprocessed = true;
            for (Pair<IModel, IExpression> zio : models) {
                if (zio.getSecond() instanceof GroovyExpression) {
                    ((GroovyExpression) zio.getSecond()).initialize(_inputs, _outputs);
                }
            }
        }

        @Override
        public int getConditionsCount() {
            return models.size();
        }

        @Override
        public boolean isNullCondition(int condition) {
            return models.get(condition).getSecond() == null;
        }

        @Override
        public String toString() {
            return "switch " + getName();
        }

        @Override
        public void notifyConditionalAccessor(IActuator actuator, int conditionIndex) {
        }

    }

    public KIMConditional(KIMContext context, KIMModel model,
            List<Pair<IObserver, IExpression>> observers, ObservationGeneratorSwitch statement) {
        super(context, model, statement);
        IObserver std = null;
        for (Pair<IObserver, IExpression> oe : observers) {
            if (std == null) {
                std = oe.getFirst();
            } else {
                if (!((KIMObserver) std).isCompatible(oe.getFirst())) {
                    context.error("observer is incompatible with previously defined one", oe.getFirst()
                            .getFirstLineNumber());
                    continue;
                }
            }
            models.add(new Pair<IModel, IExpression>(new KIMModel((KIMObserver) oe.getFirst()), oe
                    .getSecond()));
        }
    }

    @Override
    public List<Pair<IModel, IExpression>> getModels() {
        return models;
    }

    @Override
    public IObserver getRepresentativeObserver() {
        return models.size() == 0 ? null : models.get(0).getFirst().getObserver();
    }

    @Override
    public IConcept getObservationType() {
        return models.size() == 0 ? null : getRepresentativeObserver().getObservationType();
    }

    @Override
    public IObservable getObservable() {
        return models.size() == 0 ? null : getRepresentativeObserver().getObservable();
    }

    @Override
    public void name(String id) {
        super.name(id);
        for (Pair<IModel, IExpression> zio : models) {
            ((KIMObservingObject) zio.getFirst()).name(id);
        }
    }

    @Override
    public IObserver copy() {
        // TODO for now - don't think this will be called, but in case, let it throw an exception so we know.
        throw new ThinklabRuntimeException("internal: copy() unimplemented in conditional observer: please report this to developers");
    }

    @Override
    public IStateActuator getMediator(IObserver observer, IMonitor monitor) throws ThinklabException {
        return new ConditionalActuator(getActions(), monitor);
    }

    @Override
    public IStateActuator getInterpreter(IStateActuator accessor, IMonitor monitor) {
        return new ConditionalActuator(getActions(), monitor);
    }

    @Override
    public boolean canInterpretDirectly(IActuator actuator) {
        return getActions().size() == 0;
    }

    @Override
    public Object adapt() {
        return ((IRemoteSerializable) getRepresentativeObserver()).adapt();
    }

    @Override
    public String toString() {
        return "COND/" + getRepresentativeObserver();
    }

}
