/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.common.kim;

import org.integratedmodelling.api.knowledge.IKnowledge;
import org.integratedmodelling.api.modelling.IKnowledgeObject;
import org.integratedmodelling.api.modelling.ILanguageObject;
import org.integratedmodelling.api.modelling.IObserver;
import org.integratedmodelling.common.vocabulary.NS;
import org.integratedmodelling.common.vocabulary.Observable;

public class KIMObservable extends Observable implements ILanguageObject {

    int     firstLine, lastLine;
    boolean isNothing;

    public KIMObservable(KIMContext context, KIMKnowledge concept) {

        firstLine = concept.getFirstLineNumber();
        lastLine = concept.getLastLineNumber();
        this.setType(concept, context);
        isNothing = concept.isNothing();

        if (isNothing && concept.getObjectDeclarations().size() > 0) {
            /*
             * ASK OBSERVER TO CREATE TYPE
             */
            IObserver oo = (IObserver) context.get(IObserver.class);
            IKnowledge obser = null;
            if (oo == null || (obser = ((KIMObserver) oo).getObservableConcept(context, concept)) == null) {
                context.error("using direct objects as observables is not supported here", firstLine);
            } else {
                this.setType(obser);
            }
        }

        // I know
        if (!concept.isNothing() && NS.isNothing(concept)) {
            context.error(concept + " has been declared as 'nothing' and cannot be observed", firstLine);
        }
    }

    public KIMObservable(IObserver observer) {
        super((Observable) observer.getObservable());
        this.setObserver(observer);
    }

    public KIMObservable(Object literal, IObserver observer) {
        super((Observable) observer.getObservable());
        this.setObserver(observer);
        this.setModel(new KIMModel(literal, (KIMObserver) observer));
    }

    public KIMObservable(IKnowledgeObject knowledge, IObserver observer, KIMContext context) {
        this(observer);
        this.setType(knowledge, context);
    }

    // public void setInherentTypes(KIMContext context, ConceptDeclaration declaration) {
    //
    // if (declaration.getInherent() != null) {
    // IKnowledge k = KIMKnowledge
    // .declare(context, declaration.getInherent(), KIMLanguageObject.lineNumber(declaration));
    // setInherentType(k);
    // }
    // if (declaration.getOuterContext() != null) {
    // IKnowledge k = KIMKnowledge
    // .declare(context, declaration.getOuterContext().getIds(), KIMLanguageObject
    // .lineNumber(declaration));
    // if (k instanceof IConcept) {
    // setContextType((IConcept) k);
    // }
    // }
    // }

    @Override
    public int getFirstLineNumber() {
        return firstLine;
    }

    @Override
    public int getLastLineNumber() {
        return lastLine;
    }

    public boolean isNothing() {
        return isNothing;
    }

    // public void validateTypes(boolean isData, KIMContext context) {
    //
    // IKnowledge inherent = getInherentType() != null ? getInherentType() : getContextType();
    //
    // if (NS.isQuality(getType())) {
    // if (inherent == null && isData) {
    // context.error("inherent types ('of' ... or 'within') are required to specify which subject qualities
    // belong to", firstLine);
    // }
    // if (inherent != null && !NS.isThing(inherent)) {
    // context.error("inherent types ('of' ...) must specify subjects", firstLine);
    // }
    // } else if (getInherentType() != null) {
    // context.error("inherent types ('of' ...) are only required for qualities", firstLine);
    // }
    // }
}
