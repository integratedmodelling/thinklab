/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.common.kim;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.integratedmodelling.api.knowledge.IConcept;
import org.integratedmodelling.api.knowledge.IExpression;
import org.integratedmodelling.api.modelling.IAction;
import org.integratedmodelling.api.modelling.IDependency;
import org.integratedmodelling.api.modelling.IDirectObservation;
import org.integratedmodelling.api.modelling.IExtent;
import org.integratedmodelling.api.modelling.IModel;
import org.integratedmodelling.api.modelling.IObserver;
import org.integratedmodelling.api.modelling.scheduling.ITransition;
import org.integratedmodelling.api.monitoring.IMonitor;
import org.integratedmodelling.common.configuration.KLAB;
import org.integratedmodelling.common.thinkql.GroovyExpression;
import org.integratedmodelling.exceptions.ThinklabException;
import org.integratedmodelling.thinkQL.Action;

public class KIMAction extends KIMModelObject implements IAction {

    Set<IConcept> domain = new HashSet<>();
    Type          type   = Type.SET;
    IExpression   action;
    IExpression   condition;
    Object        whereTo;
    String        target;

    public KIMAction(KIMContext context, KIMModelObject observer, Set<IConcept> domain,
            Action statement) {

        super(context, statement, observer);
        this.domain.addAll(domain);

        if (statement.isSet() || statement.isChange()) {
            type = Type.CHANGE;
            if (statement.isSet() && domain.isEmpty()) {
                observer.isComputed = true;
            }
        } else if (statement.isDo()) {
            type = Type.DO;
        } else if (statement.isIntegrate()) {
            type = Type.INTEGRATE;
        } else if (statement.isMove()) {

            if (statement.isAway()) {
                type = Type.DESTROY;
                /*
                 * TODO add object disposal for language adapter.
                 */
            } else {
                type = Type.MOVE;
                whereTo = defineValue(context.get(KIMContext.VALUE), statement.getWhere());
            }
        }

        if (statement.getValue() != null) {
            action = new KIMExpression(context, statement.getValue());
        }

        if (statement.getCondition() != null) {
            condition = new KIMExpression(context, statement.getCondition());
        }

        this.target = statement.getChanged();
    }

    public KIMAction(KIMAction kimAction, Map<String, IObserver> inputs, Map<String, IObserver> outputs,
            IConcept... domain) {

        super(null);

        this.target = kimAction.target;
        this.type = kimAction.type;
        this.whereTo = kimAction.whereTo;

        if (domain != null) {
            for (IConcept c : domain) {
                this.domain.add(c);
            }
        }

        this.condition = kimAction.condition == null ? null
                : new GroovyExpression(kimAction.condition.toString(), inputs, outputs, domain);
        this.action = kimAction.action == null ? null
                : new GroovyExpression(kimAction.action.toString(), inputs, outputs, domain);
    }

    @Override
    public Set<IConcept> getDomain() {
        return domain;
    }

    @Override
    public Type getType() {
        return type;
    }

    @Override
    public String getTargetStateId() {
        return target;
    }

    public void setTargetStateId(String name) {
        target = name;
    }

    @Override
    public IExpression getAction() {
        return action;
    }

    @Override
    public IExpression getCondition() {
        return condition;
    }

    /**
     * FIXME this must be const - return a new Action with the preprocessed expressions for these
     * inputs.
     * 
     * @param inputs
     * @param outputs
     */
    public void preprocess(Map<String, IObserver> inputs, HashMap<String, IObserver> outputs) {

        if (action instanceof GroovyExpression) {
            ((GroovyExpression) action).initialize(inputs, outputs);
        }
        if (condition instanceof GroovyExpression) {
            ((GroovyExpression) condition).initialize(inputs, outputs);
        }
    }

    @Override
    public String toString() {
        return "A/[" + action + "]" + (condition == null ? "" : ("/[" + condition + "]"));
    }

    /**
     * Called directly by KIMModel when actions are executed.
     * 
     * @param context
     * @param subject
     * @param transition
     * @param monitor
     * @return
     * @throws ThinklabException
     */
    public Object execute(IDirectObservation context, IDirectObservation subject, ITransition transition, IMonitor monitor)
            throws ThinklabException {

        /*
         * prepare environment. TODO the rest
         */
        Map<String, Object> args = new HashMap<>();

        /*
         * system parameters
         */
        args.put("_monitor", monitor);

        /*
         * user variables
         */
        args.put("context", KLAB.MFACTORY.wrapForAction(context));
        args.put("self", KLAB.MFACTORY.wrapForAction(subject));
        args.put("scale", KLAB.MFACTORY.wrapForAction(subject.getScale()));
        if (subject.getScale().getSpace() != null) {
            args.put("space", KLAB.MFACTORY.wrapForAction(subject.getScale().getSpace()));
        }
        if (transition != null) {
            args.put("time", KLAB.MFACTORY.wrapForAction(transition.getTime()));
        }

        /*
         * TODO states (using their formal names in dependencies and outputs).
         * TODO set the dependency formal name in the states when created so this
         * can be done quickly.
         */

        if (condition != null) {
            Object cret = condition.eval(args, monitor);
            if (!(cret instanceof Boolean && (Boolean) cret)) {
                return null;
            }
        }

        if (action != null) {
            return action.eval(args, monitor);
        }

        return null;
    }

    /*
     * Clone action and preprocess/compile for the passed observation
     */
    public IAction compileFor(IDirectObservation directObservation, IModel model) {
        List<IConcept> doms = new ArrayList<>();
        for (IExtent e : directObservation.getScale()) {
            doms.add(e.getDomainConcept());
        }

        Map<String, IObserver> inputs = new HashMap<>();
        Map<String, IObserver> outputs = new HashMap<>();

        if (model != null) {
            for (int n = 1; n < model.getObservables().size(); n++) {
                if (model.getObservables().get(n).getObserver() != null) {
                    outputs.put(model.getObservables().get(n).getFormalName(), model.getObservables().get(n)
                            .getObserver());
                }
            }
            for (IDependency d : model.getDependencies()) {
                if (d.getObservable().getObserver() != null) {
                    inputs.put(d.getFormalName(), d.getObservable().getObserver());
                }
            }
        }

        KIMAction ret = new KIMAction(this, inputs, outputs, doms.toArray(new IConcept[doms.size()]));
        return ret;
    }
}
