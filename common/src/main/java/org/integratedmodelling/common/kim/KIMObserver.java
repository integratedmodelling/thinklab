/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.common.kim;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Set;

import org.eclipse.emf.ecore.EObject;
import org.integratedmodelling.api.data.ITable;
import org.integratedmodelling.api.knowledge.IConcept;
import org.integratedmodelling.api.knowledge.IKnowledge;
import org.integratedmodelling.api.modelling.IAction;
import org.integratedmodelling.api.modelling.IConcreteObserver;
import org.integratedmodelling.api.modelling.IConditionalObserver;
import org.integratedmodelling.api.modelling.IDirectObserver;
import org.integratedmodelling.api.modelling.IFunctionCall;
import org.integratedmodelling.api.modelling.IMediatingObserver;
import org.integratedmodelling.api.modelling.IModel;
import org.integratedmodelling.api.modelling.IObservable;
import org.integratedmodelling.api.modelling.IObserver;
import org.integratedmodelling.api.modelling.ISubject;
import org.integratedmodelling.api.modelling.runtime.IActiveObserver;
import org.integratedmodelling.api.modelling.runtime.IStateActuator;
import org.integratedmodelling.api.monitoring.IMonitor;
import org.integratedmodelling.collections.Pair;
import org.integratedmodelling.common.configuration.KLAB;
import org.integratedmodelling.common.model.actuators.LookupTableActuator;
import org.integratedmodelling.common.model.actuators.StateActuator;
import org.integratedmodelling.common.vocabulary.NS;
import org.integratedmodelling.common.vocabulary.Observable;
import org.integratedmodelling.exceptions.ThinklabException;
import org.integratedmodelling.exceptions.ThinklabResourceNotFoundException;
import org.integratedmodelling.exceptions.ThinklabRuntimeException;
import org.integratedmodelling.lang.IRemoteSerializable;
import org.integratedmodelling.thinkQL.ConceptDeclaration;
import org.integratedmodelling.thinkQL.LookupFunction;
import org.integratedmodelling.thinkQL.Observer;

public abstract class KIMObserver extends KIMObservingObject
        implements IObserver, IActiveObserver, IRemoteSerializable {

    protected IModel              model;
    protected IObservable         observable;
    protected IObserver           mediated;
    protected ITable              lookupTable;

    /*
     * these may be defined only in observers that allow direct observations instead
     * of concepts (implementing IConcreteObserver)
     */
    protected List<String>        fromObject    = new ArrayList<>();
    private List<IDirectObserver> directObjects = null;

    public KIMObserver() {
        super((KIMContext) null, null, null);
    }

    public KIMObserver(IObserver observer) {
        super((KIMObserver) observer);
        this.model = ((KIMObserver) observer).model;
        this.observable = ((KIMObserver) observer).observable;
        this.mediated = ((KIMObserver) observer).mediated;
    }

    public KIMObserver(KIMContext context, IModel model, EObject statement) {

        super(context, statement, model);
        this.model = model;

        /*
         * statement may be an observer or a conditional
         */
        if (statement instanceof Observer) {

            Observer ostatement = (Observer) statement;
            if (ostatement.getConcept() != null || (ostatement.getObservable() != null
                    && ostatement.getObservable().getConcept() != null)) {

                ConceptDeclaration declaration = ostatement.getConcept() == null ? ostatement.getObservable()
                        .getConcept() : ostatement.getConcept();
                KIMKnowledge k = new KIMKnowledge(context
                        .get(KIMContext.OBSERVER_OBSERVABLE), declaration, this, this instanceof IConcreteObserver);

                if (k.isNothing()) {

                    /*
                     * TODO may have an object if observer allows it (in this case it would be
                     * distance only).
                     */
                    if (this instanceof IConcreteObserver && k.getObjectDeclarations().size() > 0) {
                        fromObject.addAll(k.getObjectDeclarations());
                    } else {
                        return;
                    }
                }

                this.observable = new KIMObservable(context.get(KIMContext.OBSERVER_OBSERVABLE), k);
                ((Observable) observable).setObservationType(this.getObservationType());
                ((Observable) observable).setObserver(this);

            } else if (ostatement.getObservable() != null
                    && ostatement.getObservable().getConceptStatement() != null) {

                this.observable = new KIMObservable(context
                        .get(KIMContext.OBSERVER_OBSERVABLE), new KIMKnowledge(context
                                .get(KIMContext.OBSERVER_OBSERVABLE), ostatement.getObservable()
                                        .getConceptStatement(), this));

                try {
                    ((Observable) observable).setObservationType(this.getObservationType());
                } catch (ThinklabRuntimeException e) {
                    context.error(e
                            .getMessage(), lineNumber(ostatement.getObservable().getConceptStatement()));
                }
                ((Observable) observable).setObserver(this);

            } else if (ostatement.getObservable() != null
                    && ostatement.getObservable().getMediated() != null) {

                if (!(this instanceof IMediatingObserver)) {
                    context.error("this observer cannot mediate another", lineNumber(ostatement
                            .getObservable().getMediated()));
                }

                this.mediated = defineObserver(context, (KIMModel) model, ostatement.getObservable()
                        .getMediated());

                if (this.mediated.getObservable() != null) {
                    try {
                        this.observable = new Observable(getObservedType(context, this.mediated
                                .getObservable()
                                .getTypeAsConcept()), this
                                        .getObservationType(), this.mediated.getObservable().getFormalName());
                        ((Observable) observable).setObserver(this);
                    } catch (ThinklabRuntimeException e) {
                        context.error(e.getMessage(), getFirstLineNumber());
                    }
                }
            }

            defineDependencies(context, ostatement.getDependencies());

            if (ostatement.getAccessor() != null) {
                this.actuatorCall = new KIMFunctionCall(context
                        .get(KIMContext.ACTUATOR_FUNCTION_CALL), ostatement
                                .getAccessor());
                validateFunctionCall(context, this.actuatorCall, KLAB.c(NS.STATE_CONTEXTUALIZER));
            } else if (ostatement.getLookup() != null) {

                /*
                 * parse a lookup table and ask the resolver for the accessor that will lookup data from it.
                 */
                LookupFunction lfunc = ostatement.getLookup();
                ITable lookupTable = null;
                if (lfunc.getRef() != null) {
                    Object table = context.getNamespace().getSymbolTable().get(lfunc.getRef());
                    if (!(table instanceof ITable)) {
                        context.error("object identified by "
                                + lfunc.getRef()
                                + " is unknown or is not a lookup table", lineNumber(lfunc));
                    } else {
                        lookupTable = (ITable) table;
                    }
                }
                this.actuatorCall = new LookupTableActuator(lookupTable, context.getNamespace(), lfunc
                        .getArgs());
            }

            if (ostatement.getContextualizers() != null) {

                Pair<List<IFunctionCall>, List<IAction>> zio = defineContextActions(context, this, ostatement
                        .getContextualizers());

                /*
                 * float the scale specs if any
                 */
                if (scaleGenerators.size() > 0) {
                    ((KIMModel) model).scaleGenerators.addAll(scaleGenerators);
                }

                for (IFunctionCall function : zio.getFirst()) {
                    scaleGenerators.add(function);
                }

                for (IAction action : zio.getSecond()) {
                    actions.add(action);
                }
            }
        }

    }

    public abstract IObserver copy();

    public IModel getTopLevelModel() {
        return getTopLevelModel(this);
    }

    protected IModel getTopLevelModel(KIMModelObject object) {
        if (object instanceof IModel && object.parent == null) {
            return (IModel) object;
        }
        return object.parent == null ? null : getTopLevelModel((KIMModelObject) object.parent);
    }

    /**
     * Check compatibility; used within a set of conditional observers.
     * @param observer
     * @return
     */
    boolean isCompatible(IObserver observer) {
        // TODO check the units etc
        return this.getClass().equals(observer.getClass());
    }

    @Override
    public IObserver train(ISubject context, IMonitor monitor) throws ThinklabException {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public IModel getModel() {
        return model;
    }

    @Override
    protected void name(String id) {
        super.name(id);
        if (observable != null) {
            ((Observable) observable).setFormalName(id);
        }
        if (mediated != null) {
            ((KIMObserver) mediated).name(id);
        }
    }

    /**
     * Filter the observable type, if necessary, to make it match the result of this observation made on the stated type.
     * 
     * @param knowledge
     * @return
     */
    protected IConcept getObservedType(KIMContext context, IConcept knowledge) {
        return knowledge;
    }

    @Override
    public boolean isAggregating() {
        return aggregatesContext;
    }

    @Override
    public Set<IConcept> getAggregationDomains() {
        return aggregationDomains;
    }

    @Override
    public IObservable getObservable() {
        return observable;
    }

    @Override
    public IKnowledge getType() {
        return observable.getType();
    }

    public IObserver getRepresentativeObserver() {
        return (this instanceof KIMConditional) ? getRepresentativeObserver() : this;
    }

    @Override
    public IStateActuator getComputingAccessor(IMonitor monitor) throws ThinklabException {
        IStateActuator ret = (IStateActuator) getActuator(monitor);
        if (ret == null) {
            ret = new StateActuator(this.getActions(), monitor);
        }
        return ret;
    }

    /**
     * utility to check observers in mediators - gives us the observer that has the semantics of the data,
     * being smart about conditional observers.
     * 
     * @param observer
     * @return
     */
    static public IObserver getRepresentativeObserver(IObserver observer) {
        return (observer instanceof IConditionalObserver) ? ((KIMConditional) observer)
                .getRepresentativeObserver() : observer;
    }

    public boolean isCompatibleWith(IKnowledge type) {
        return type.is(getObservable().getType());
    }

    protected Collection<IDirectObserver> resolveConcreteIds() throws ThinklabException {
        if (directObjects == null) {
            directObjects = new ArrayList<>();
            for (String s : fromObject) {
                IDirectObserver obs = KLAB.ENGINE.retrieveObservation(s, null);
                if (obs == null) {
                    throw new ThinklabResourceNotFoundException("observation " + s
                            + " was not found either locally or remotely");
                }
                directObjects.add(obs);
            }
        }
        return directObjects;
    }

    protected IKnowledge getObservableConcept(KIMContext context, KIMKnowledge observed) {
        return null;
    }

}
