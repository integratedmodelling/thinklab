/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.common.kim;

import java.util.ArrayList;
import java.util.List;

import org.integratedmodelling.api.knowledge.IConcept;
import org.integratedmodelling.api.knowledge.IKnowledge;
import org.integratedmodelling.api.modelling.IAction;
import org.integratedmodelling.api.modelling.IDirectObserver;
import org.integratedmodelling.api.modelling.IFunctionCall;
import org.integratedmodelling.api.modelling.IModelObject;
import org.integratedmodelling.api.modelling.INamespace;
import org.integratedmodelling.api.modelling.IObservable;
import org.integratedmodelling.api.modelling.IObserver;
import org.integratedmodelling.api.modelling.IScale;
import org.integratedmodelling.collections.Pair;
import org.integratedmodelling.common.configuration.KLAB;
import org.integratedmodelling.common.vocabulary.Observable;
import org.integratedmodelling.thinkQL.Annotation;
import org.integratedmodelling.thinkQL.ObserveStatement;
import org.integratedmodelling.thinkQL.State;

/**
 * Same bean for all direct observations. The model factory will discriminate and decide what to build.
 * 
 * @author ferdinando.villa
 *
 */
public class KIMDirectObserver extends KIMObservingObject implements IDirectObserver {

    IObservable                   observable;
    List<Pair<IObserver, Object>> literalStates = new ArrayList<>();
    List<IDirectObserver>         children      = new ArrayList<>();

    public KIMDirectObserver(KIMContext context, ObserveStatement statement, IModelObject parent,
            List<Annotation> annotations) {

        super(context, statement, parent, annotations);

        this.id = statement.getName();

        KIMKnowledge k = new KIMKnowledge(context
                .get(KIMContext.OBSERVER_OBSERVABLE), statement.getConcept(), null);

        this.observable = new Observable(k.getKnowledge());

        if (this.observable.getType() != null && this.observable.getType().isAbstract()) {
            context.error("the type for an observation cannot be abstract: abstract types are only "
                    + "admitted in dependencies or 'resolve' statements", lineNumber(statement
                            .getConcept()));
        }

        if (statement.getContextualizers() != null) {
            Pair<List<IFunctionCall>, List<IAction>> zio = KIMObservingObject
                    .defineContextActions(context, this, statement
                            .getContextualizers());

            for (IFunctionCall function : zio.getFirst()) {
                scaleGenerators.add(function);
            }

            for (IAction action : zio.getSecond()) {
                actions.add(action);
            }
        }

        for (State state : statement.getStates()) {
            if (state.getObserver() != null) {
                IObserver observer = KIMObservingObject.defineObserver(context
                        .get(KIMContext.OBSERVATION_STATE_OBSERVER), null, state.getObserver());
                Object value = null;
                if (state.getLiteral() != null) {
                    value = KIM.processLiteral(state.getLiteral());
                } else if (state.getFunction() != null) {
                    value = new KIMFunctionCall(context.get(KIMContext.FUNCTIONCALL), state.getFunction());
                }

                literalStates.add(new Pair<>(observer, value));

            } else if (state.getObservation() != null) {
                children.add(new KIMDirectObserver(context.get(KIMContext.OBSERVATION), state
                        .getObservation(), this, new ArrayList<Annotation>()));
            }
        }
    }

    /**
     * Creates a new object with the same exact data using the installed model factory. Will sanitize
     * scales etc. to ensure they're created by the currently active factory, so it's good to turn a
     * client implementation into an engine one and the other way around.
     * 
     * @param observer
     */
    public KIMDirectObserver(KIMDirectObserver observer) {
        super(KLAB.MMANAGER
                .getNamespace(observer.getNamespace().getId()), observer
                        .getId());
        this.observable = observer.getObservable();
    }

    // public void define(IConcept concept, String name, String shape, ITemporalExtent time) {
    // this.observable = new Observable(concept);
    // this.setId(name);
    // this.scale = Scale.create(new Space(new ShapeValue(shape)), time);
    // ((INamespaceDefinition) this.getNamespace()).addModelObject(this);
    // }

    public KIMDirectObserver(INamespace namespace, IConcept observable, String id, IScale scale) {
        super(namespace, id);
        this.observable = new Observable(observable);
        this.scale = scale;
    }

    public KIMDirectObserver(IConcept observable, String id, IScale scale) {
        super(KLAB.MMANAGER.getLocalNamespace(), id);
        this.observable = new Observable(observable);
        this.scale = scale;
    }

    @Override
    public IKnowledge getType() {
        return observable.getType();
    }

    @Override
    public IObservable getObservable() {
        return observable;
    }

    @Override
    public List<Pair<IObserver, Object>> getStatesDefinition() {
        return literalStates;
    }

    @Override
    public String toString() {
        return "O/" + getName() + "/" + getObservable();
    }

    @Override
    public List<IDirectObserver> getChildrenDefinition() {
        return children;
    }

}
