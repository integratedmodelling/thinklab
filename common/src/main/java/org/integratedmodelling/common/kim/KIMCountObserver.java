/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.common.kim;

import java.util.List;
import java.util.Map;

import org.integratedmodelling.api.knowledge.IConcept;
import org.integratedmodelling.api.knowledge.ISemantic;
import org.integratedmodelling.api.modelling.IAction;
import org.integratedmodelling.api.modelling.IClassification;
import org.integratedmodelling.api.modelling.ICountingObserver;
import org.integratedmodelling.api.modelling.IObserver;
import org.integratedmodelling.api.modelling.IUnit;
import org.integratedmodelling.api.modelling.runtime.IActuator;
import org.integratedmodelling.api.modelling.runtime.IStateActuator;
import org.integratedmodelling.api.monitoring.IMonitor;
import org.integratedmodelling.common.classification.Classification;
import org.integratedmodelling.common.configuration.KLAB;
import org.integratedmodelling.common.model.actuators.MediatingActuator;
import org.integratedmodelling.common.utils.MapUtils;
import org.integratedmodelling.common.vocabulary.NS;
import org.integratedmodelling.common.vocabulary.Observable;
import org.integratedmodelling.common.vocabulary.Unit;
import org.integratedmodelling.exceptions.ThinklabException;
import org.integratedmodelling.exceptions.ThinklabRuntimeException;
import org.integratedmodelling.exceptions.ThinklabValidationException;
import org.integratedmodelling.thinkQL.Observer;

public class KIMCountObserver extends KIMNumericObserver implements ICountingObserver {

    IUnit unit;

    public class CountActuator extends MediatingActuator {

        IObserver other;

        public CountActuator(IObserver other, List<IAction> actions, IMonitor monitor,
                boolean interpreting) {
            super(actions, monitor, interpreting);
            this.other = other;
        }

        @Override
        public String toString() {
            return "[count: " + unit + "]";
        }

        public IUnit getUnit() {
            return unit;
        }

        @Override
        public Object mediate(Object object) {

            if (object == null || (object instanceof Number && Double.isNaN(((Number) object).doubleValue())))
                return Double.NaN;

            if (!(other instanceof KIMCountObserver)) {
                return object;
            }

            return unit == null ? object
                    : unit.convert(((Number) object).doubleValue(), ((KIMCountObserver) other).getUnit());
        }
    }

    public KIMCountObserver(KIMContext context, KIMModel model, Observer statement) {
        super(context, model, statement);
        if (statement.getUnit() != null) {
            unit = new KIMUnit(context.get(KIMContext.UNIT), statement.getUnit());
            /* 
             * TODO check
             * must be distributional only
             */
        }
        if (observable != null) {
            ((Observable) observable)
                    .setType(this.getObservedType(context, this.observable.getTypeAsConcept()));
        }
    }

    public KIMCountObserver(IObserver observer) {
        super(observer);
        if (!(observer instanceof ICountingObserver)) {
            throw new ThinklabRuntimeException("cannot initialize a counting observer from a "
                    + observer.getClass().getCanonicalName());
        }
        this.unit = ((ICountingObserver) observer).getUnit();
    }

    public KIMCountObserver(Map<?, ?> map) {

        if (map.containsKey("discretization")) {
            discretization = map.get("discretization") instanceof IClassification
                    ? (IClassification) map.get("discretization")
                    : new Classification((Map<?, ?>) map.get("discretization"));
        }

        if (map.containsKey("unit")) {
            unit = new Unit(map.get("unit").toString());
        }
    }

    @Override
    public IObserver copy() {
        return new KIMCountObserver(this);
    }

    KIMCountObserver(ISemantic observable, String unit2) {
        // TODO Auto-generated constructor stub
    }

    @Override
    public IUnit getUnit() {
        return unit;
    }

    @Override
    public IConcept getObservationType() {
        return KLAB.c(NS.COUNT_OBSERVATION);
    }

    @Override
    public IConcept getObservedType(KIMContext context, IConcept concept) {
        IConcept ret = NS.makeCount(concept);
        if (ret == null) {
            context.error("only things or agents can be counted", getFirstLineNumber());
            return concept;
        }
        return ret;
    }

    @Override
    public IStateActuator getMediator(IObserver observer, IMonitor monitor) throws ThinklabException {

        observer = getRepresentativeObserver(observer);

        IActuator ret = getActuator(monitor);
        if (ret != null) {
            return (IStateActuator) ret;
        }
        /*
         * FIXME this gets called when getInterpreter should be called
         */
        if (observer instanceof ICountingObserver) {

            if (!observer.getObservable().is(getObservable()))
                throw new ThinklabValidationException("counts can only mediate other counts of the same thing");

            if ((((ICountingObserver) observer).getUnit() == null && unit != null)
                    || (((ICountingObserver) observer).getUnit() != null && unit == null))
                throw new ThinklabValidationException("counts with units can only mediate other counts with units");

            if (getActions().size() == 0
                    && ((((ICountingObserver) observer).getUnit() == null && unit == null)
                            || ((ICountingObserver) observer)
                                    .getUnit().equals(getUnit()))) {
                return null;
            }
        }

        return new CountActuator(observer, getActions(), monitor, false);
    }

    @Override
    public IStateActuator getInterpreter(IStateActuator accessor, IMonitor monitor) {
        return new CountActuator(this, getActions(), monitor, true);
    }

    @Override
    public boolean canInterpretDirectly(IActuator actuator) {
        return getActions().size() == 0 && discretization == null;
    }

    @Override
    public Object adapt() {
        return MapUtils.of("unit", unit, "discretization", discretization);
    }

    @Override
    public String toString() {
        return "CNT/" + getObservable() + (unit == null ? "" : (" (" + unit + ")"));
    }
}
