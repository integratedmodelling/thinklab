/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.common.kim;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtext.nodemodel.util.NodeModelUtils;
import org.integratedmodelling.api.modelling.ILanguageObject;
import org.integratedmodelling.common.HashableObject;
import org.integratedmodelling.lang.SemanticType;

public class KIMLanguageObject extends HashableObject implements ILanguageObject {

    private int startLine;
    private int endLine;

    public KIMLanguageObject(EObject statement) {
        if (statement != null) {
            this.startLine = NodeModelUtils.getNode(statement).getStartLine();
            this.endLine = NodeModelUtils.getNode(statement).getEndLine();
        }
    }

    protected KIMLanguageObject(KIMLanguageObject object) {
        if (object != null) {
            this.startLine = object.getFirstLineNumber();
            this.endLine = object.getLastLineNumber();
        }
    }

    @Override
    public int getFirstLineNumber() {
        return startLine;
    }

    @Override
    public int getLastLineNumber() {
        return endLine;
    }

    public static int lineNumber(EObject statement) {
        return NodeModelUtils.getNode(statement).getStartLine();
    }

    static protected boolean isSuitablePropertyId(String pp) {
        if (pp == null)
            return false;
        // Let them do it.
        // if (SemanticType.validate(pp)) {
        // return Character.isLowerCase(new SemanticType(pp).getLocalName().charAt(0));
        // }
        // return Character.isLowerCase(pp.charAt(0));
        return SemanticType.validate(pp);
    }

    static protected boolean isSuitableConceptId(String pp) {
        if (pp == null)
            return false;
        if (SemanticType.validate(pp)) {
            return Character.isUpperCase(new SemanticType(pp).getLocalName().charAt(0));
        }
        return Character.isUpperCase(pp.charAt(0));
    }
}
