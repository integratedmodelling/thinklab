/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.common.kim;

import java.util.List;

import org.integratedmodelling.api.knowledge.IConcept;
import org.integratedmodelling.api.knowledge.IExpression;
import org.integratedmodelling.api.knowledge.IKnowledge;
import org.integratedmodelling.api.knowledge.IProperty;
import org.integratedmodelling.api.modelling.IDependency;
import org.integratedmodelling.api.modelling.IKnowledgeObject;
import org.integratedmodelling.api.modelling.IModel;
import org.integratedmodelling.api.modelling.IObservable;
import org.integratedmodelling.api.modelling.IObserver;
import org.integratedmodelling.common.configuration.KLAB;
import org.integratedmodelling.common.utils.CamelCase;
import org.integratedmodelling.common.vocabulary.NS;
import org.integratedmodelling.common.vocabulary.Observable;
import org.integratedmodelling.thinkQL.Dependency;

public class KIMDependency extends KIMModelObject implements IDependency {

    boolean     isNothing;
    IObservable observable;
    String      formalName;
    IProperty   property;
    boolean     isOptional;
    boolean     isGeneric;
    boolean     isDistributing;
    Object      contextModel;
    IConcept    interpretAs;
    IExpression whereCondition;
    IConcept    distributionContext;

    public KIMDependency(KIMDependency other, IObservable observable, String formalName) {
        super(other);
        this.isNothing = other.isNothing;
        this.property = other.property;
        this.isOptional = other.isOptional;
        this.isDistributing = other.isDistributing;
        this.interpretAs = other.interpretAs;
        this.whereCondition = other.whereCondition;
        this.distributionContext = other.distributionContext;
        this.observable = observable;
        this.formalName = formalName;
    }

    public KIMDependency(Observable observable, String fname, IProperty property, boolean isOptional,
            Object contextModel) {
        super(null);
        this.formalName = fname;
        this.observable = observable;
        this.property = property;
        this.isOptional = isOptional;
        this.contextModel = contextModel;
    }

    public KIMDependency(KIMContext context, Dependency statement, KIMObservingObject observer) {
        super(context, statement, observer);

        /**
         * TODO see logics in old alignDependencies()
         */

        IKnowledgeObject obs = null;
        IKnowledgeObject trait = null;
        IObserver observ = null;
        Object literalValue = null;
        boolean bareObservable = false;
        IModel depModel = null;
        int downTo = -1;
        IConcept byTrait = null;

        this.isOptional = statement.getOptional() != null && statement.getOptional().equals("optional");
        if (statement.getConcept() != null) {

            bareObservable = true;

            obs = new KIMKnowledge(context.get(KIMContext.DEPENDENCY_OBSERVABLE), statement
                    .getConcept(), null);

            if (statement.getTrait() != null) {

                trait = new KIMKnowledge(context.get(KIMContext.CONCEPT), statement
                        .getTrait().getId(), false, lineNumber(statement.getTrait()));

                if (NS.isObservable(trait)) {
                    byTrait = NS.getTraitFor(trait.getKnowledge());
                }

                if (trait.isNothing() || !NS.isTrait(trait)) {
                    context.error("concept in 'by' clause must be a trait", lineNumber(statement
                            .getConcept()));
                    return;
                } else {

                    byTrait = trait.getConcept();

                    /*
                     * main observable must be a class and expose this trait.
                     */
                    if (!NS.isClass(obs)) {
                        context.error("in a bare dependency, only a class can use 'by' to select one of its exposed traits", lineNumber(statement
                                .getConcept()));
                    } else {

                        List<IObservable> exposedTraits = NS.getExposedTraitsForType(obs.getConcept());
                        if (!exposedTraits.contains(byTrait)) {
                            context.error("the trait used in a 'by' statement must be exposed by the observed class", lineNumber(statement
                                    .getConcept()));
                        }
                    }
                }

            }
            /*
             * now 'by' is defined: may have 'down to' in the following ways: 
             * C down to X by Y
             * C by X down to Y or both 
             * C down to X by Y down to Z  
             */

            if (obs.isNothing()) {
                return;
            }

            /*
             * must be direct, class (then check for trait, otherwise trait is an error) or
             * be a quality and have a model in the ns.
             */
            if (NS.isQuality(obs) || NS.isTrait(obs)) {

                /*
                 * needs a unique model in namespace; this will automatically complain if not found
                 */
                depModel = KIMObservingObject
                        .findObservableModel(context, obs
                                .getKnowledge(), lineNumber(statement.getConcept()), NS.isQuality(obs));

            } else if (!(NS.isClass(obs) || NS.isTrait(obs)) && !NS.isDirect(obs)) {

                /*
                 * bitch about it
                 */
                context.error("bare concept dependencies must be classifications, direct observables (subjects, "
                        + "processes, events) or qualities previously observed by a model in this namespace", lineNumber(statement
                                .getConcept()));

                return;
            }

            /* 
             * if we still have no model and no error, must be a legal class/trait or direct: make an observable from obs
             */
            if (depModel == null) {

                KIMObservable oob = new KIMObservable(context
                        .get(KIMContext.DEPENDENCY_OBSERVABLE), (KIMKnowledge) obs);
                if (byTrait != null) {
                    oob.setTraitType(byTrait, trait.getDetailLevel());
                }

                depModel = KIMModel.makeModel(context, oob);
            }

        } else if (statement.getInlineModel() != null) {

            if (statement.getInlineModel().getValue() != null) {
                literalValue = KIM.processLiteral(statement.getInlineModel().getValue());
            } else if (statement.getInlineModel().getConcept() != null) {
                obs = new KIMKnowledge(context.get(KIMContext.DEPENDENCY_OBSERVABLE), statement
                        .getInlineModel().getConcept(), null);
            }
            observ = KIMObservingObject.defineObserver(context, null, statement.getInlineModel()
                    .getObserver());
            if (obs /* still */ == null && observ != null && observ.getObservable() != null) {
                obs = new KIMKnowledge(context.get(KIMContext.DEPENDENCY_OBSERVABLE), observ.getObservable()
                        .getType());
            }
        }

        /**
         * user specified an observer
         */
        if (!bareObservable && observ != null) {

            if (obs != null && !obs.isNothing()) {

                /**
                 * TODO check the observable is nothing or is compatible
                 */
                if (!NS.isParticular(obs)) {
                    if (NS.isTrait(obs)) {
                        // ERROR
                    } else {
                        // MAKE IT A SUBCLASS OF THE OBSERVER'S TYPE
                    }
                } else {

                    // MUST BE SAME CORE TYPE

                }

                depModel = new KIMModel(obs.getKnowledge(), observ, statement.getFormalname());

                if (literalValue != null) {

                    /*
                     * set depModel to literal model with observ
                     */
                    ((KIMModel) depModel).inlineValue = literalValue;

                }

            }
        }

        if (depModel != null) {
            this.observable = new Observable(depModel, statement.getFormalname());
            if (this.observable.getType() == null) {
                // BS concepts, pre-existing error, stop here
                return;
            }
            // hm - if we leave it, it will screw up resolution. Must review all this.
            if (NS.isObject(this.observable)) {
                ((Observable)this.observable).setModel(null);
            }
        } else {

            if (obs == null) {
                return;
            }
            // anything else should have been dealt with
            this.observable = new Observable(obs.getKnowledge(), KLAB.c(NS.DIRECT_OBSERVATION), statement
                    .getFormalname());
        }

        if (statement.getDcontext() != null) {
            IKnowledgeObject dctx = new KIMKnowledge(context.get(KIMContext.DEPENDENCY_OBSERVABLE), statement
                    .getDcontext(), null);
            if (!((statement.isEach() && NS.isCountable(dctx))
                    || (statement.isEach() && NS.isProcess(dctx)))) {
                context.error("distributing dependencies must be on countable observables (subjects or events) if 'each' is given, or on processes if not", lineNumber(statement
                        .getDcontext()));
            } else {
                isDistributing = true;
                distributionContext = dctx.getConcept();
                if (statement.getWhereCondition() != null) {
                    whereCondition = new KIMExpression(context
                            .get(KIMContext.DISTRIBUTION_DEPENDENCY_CONDITION), statement
                                    .getWhereCondition());
                }
            }
        }

        isOptional = statement.getOptional() != null && statement.getOptional().equals("optional");
        isGeneric = statement.isGeneric();

        if (statement.getFormalname() != null) {
            formalName = statement.getFormalname();
        }

    }

    @Override
    public IObservable getObservable() {
        return observable;
    }

    @Override
    public boolean isOptional() {
        return isOptional;
    }

    @Override
    public IProperty getProperty() {
        return property;
    }

    @Override
    public IConcept reinterpretAs() {
        return interpretAs;
    }

    @Override
    public boolean isGeneric() {
        return isGeneric;
    }

    @Override
    public Object getContextModel() {
        return contextModel;
    }

    @Override
    public String getFormalName() {
        return formalName;
    }

    public boolean isNothing() {
        return isNothing;
    }

    public String name() {
        if (observable != null && observable.getType() != null) {
            if (observable.getFormalName() != null) {
                formalName = observable.getFormalName();
            } else {
                ((Observable) observable)
                        .setFormalName(CamelCase.toLowerCase(observable.getType().getLocalName(), '-'));
                formalName = observable.getFormalName();
            }
            if (observable.getModel() != null) {
                ((KIMModel) observable.getModel()).name(formalName);
            }
        } else {
            formalName = "error";
        }

        return formalName;
    }

    @Override
    public boolean isDistributing() {
        return isDistributing;
    }

    @Override
    public IExpression getWhereCondition() {
        return whereCondition;
    }

    @Override
    public IConcept getDistributionContext() {
        return distributionContext;
    }

    @Override
    public IKnowledge getType() {
        return observable.getType();
    }
    
    @Override
    public String toString() {
        return "D/"+observable;
    }
}
