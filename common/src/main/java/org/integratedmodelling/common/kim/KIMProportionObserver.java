/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.common.kim;

import java.util.List;
import java.util.Map;

import org.integratedmodelling.api.knowledge.IConcept;
import org.integratedmodelling.api.knowledge.ISemantic;
import org.integratedmodelling.api.modelling.IAction;
import org.integratedmodelling.api.modelling.IClassification;
import org.integratedmodelling.api.modelling.IObserver;
import org.integratedmodelling.api.modelling.IProportionObserver;
import org.integratedmodelling.api.modelling.runtime.IActuator;
import org.integratedmodelling.api.modelling.runtime.IStateActuator;
import org.integratedmodelling.api.monitoring.IMonitor;
import org.integratedmodelling.common.classification.Classification;
import org.integratedmodelling.common.configuration.KLAB;
import org.integratedmodelling.common.data.IndexedCategoricalDistribution;
import org.integratedmodelling.common.model.actuators.MediatingActuator;
import org.integratedmodelling.common.utils.MapUtils;
import org.integratedmodelling.common.vocabulary.NS;
import org.integratedmodelling.common.vocabulary.Observable;
import org.integratedmodelling.exceptions.ThinklabException;
import org.integratedmodelling.exceptions.ThinklabRuntimeException;
import org.integratedmodelling.thinkQL.Observer;

public class KIMProportionObserver extends KIMNumericObserver implements IProportionObserver {

    IConcept comparisonConcept;
    boolean  isProportion;
    boolean  isIndirect;

    public class ProportionActuator extends MediatingActuator {

        boolean errorsPresent = false;

        public ProportionActuator(List<IAction> actions, IMonitor monitor, boolean interpreting) {
            super(actions, monitor, interpreting);
            if (discretization != null)
                ((org.integratedmodelling.common.classification.Classification) discretization)
                        .reset();

        }

        @Override
        public String toString() {
            return "[" + "percentage/proportion" + "]";
        }

        @Override
        public Object mediate(Object object) {

            double val = Double.NaN;

            if (object == null || (object instanceof Number && Double.isNaN(((Number) object).doubleValue())))
                return val;

            if (object instanceof IndexedCategoricalDistribution) {

                /*
                 * TODO try to mediate distributions
                 */
                val = ((IndexedCategoricalDistribution) object).getMean();
            } else if (object instanceof Number) {
                val = ((Number) object).doubleValue();
            } else {
                try {
                    val = object.toString().equals("NaN") ? Double.NaN : Double
                            .parseDouble(object.toString());
                } catch (Exception e) {
                    if (!errorsPresent) {
                        monitor.error("cannot interpret value: " + object + " as a number");
                        errorsPresent = true;
                    }
                    return Double.NaN;
                }
            }

            return val;
        }
    }

    public KIMProportionObserver(KIMContext context, KIMModel model, Observer statement) {
        super(context, model, statement);
        if (statement.getOther() != null) {
            comparisonConcept = new KIMKnowledge(context.get(KIMContext.COMPARISON_OBSERVABLE), statement
                    .getOther(), this).getConcept();
        }
        if (observable != null) {
            ((Observable) observable)
                    .setType(this.getObservedType(context, this.observable.getTypeAsConcept()));
        }
        isProportion = statement.getType().equals("proportion");
    }

    public KIMProportionObserver(IObserver observer) {
        super(observer);
        if (!(observer instanceof IProportionObserver)) {
            throw new ThinklabRuntimeException("cannot initialize a proportion observer from a "
                    + observer.getClass().getCanonicalName());
        }
        this.comparisonConcept = ((KIMProportionObserver) observer).comparisonConcept;
    }

    public KIMProportionObserver(Map<?, ?> map) {
        if (map.containsKey("discretization")) {
            discretization = map.get("discretization") instanceof IClassification
                    ? (IClassification) map.get("discretization")
                    : new Classification((Map<?, ?>) map.get("discretization"));
        }
        comparisonConcept = map.containsKey("comparison-concept") ? KLAB.c(map.get("comparison-concept")
                .toString())
                : null;
        isIndirect = map.get("indirect?").equals("true");
        isProportion = map.get("proportion?").equals("true");
    }

    @Override
    public IObserver copy() {
        return new KIMProportionObserver(this);
    }

    KIMProportionObserver(ISemantic observable, ISemantic comparison, boolean makePercentage) {
        // TODO Auto-generated constructor stub
    }

    @Override
    public IConcept getComparisonConcept() {
        return comparisonConcept;
    }

    @Override
    public boolean isIndirect() {
        return comparisonConcept != null;
    }

    @Override
    protected int getDiscretizationContext() {
        return KIMContext.PROPORTION_DISCRETIZATION;
    }

    @Override
    public IConcept getObservationType() {
        return KLAB.c(NS.PROPORTION_OBSERVATION);
    }

    @Override
    public IConcept getObservedType(KIMContext context, IConcept concept) {

        IConcept ret = concept;

        if (comparisonConcept != null) {
            ret = NS.makeProportion(concept, comparisonConcept);
            if (ret == null) {
                context.error(concept
                        + ": proportions must compare two related observables, differing by a trait or by inheritance.", getFirstLineNumber());
                return concept;
            }
        } else if (!concept.is(KLAB.c(NS.CORE_PROPORTION))) {
            context.error(concept
                    + ": the observable in this statement must be a proportion or percentage. Use the indirect form (with 'of') to annotate explicit comparisons.", getFirstLineNumber());
        }
        return ret;
    }

    public static String asString(IProportionObserver observer) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public IStateActuator getMediator(IObserver observer, IMonitor monitor) throws ThinklabException {
        IActuator ret = getActuator(monitor);
        if (ret != null)
            return (IStateActuator) ret;

        return new ProportionActuator(getActions(), monitor, false);
    }

    @Override
    public IStateActuator getInterpreter(IStateActuator accessor, IMonitor monitor) {
        return new ProportionActuator(getActions(), monitor, true);
    }

    @Override
    public boolean canInterpretDirectly(IActuator actuator) {
        return getActions().size() == 0 && discretization == null;
    }

    @Override
    public Object adapt() {
        return MapUtils.of("proportion?", isProportion, "comparison-concept", (comparisonConcept == null
                ? null
                : comparisonConcept.toString()), "indirect?", isIndirect, "discretization", discretization);
    }

}
