/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.common.kim;

import java.util.List;
import java.util.Map;

import org.integratedmodelling.api.knowledge.IConcept;
import org.integratedmodelling.api.knowledge.ISemantic;
import org.integratedmodelling.api.modelling.IAction;
import org.integratedmodelling.api.modelling.IClassification;
import org.integratedmodelling.api.modelling.IMeasuringObserver;
import org.integratedmodelling.api.modelling.IObservable;
import org.integratedmodelling.api.modelling.IObserver;
import org.integratedmodelling.api.modelling.IUnit;
import org.integratedmodelling.api.modelling.runtime.IActuator;
import org.integratedmodelling.api.modelling.runtime.IStateActuator;
import org.integratedmodelling.api.monitoring.IMonitor;
import org.integratedmodelling.common.classification.Classification;
import org.integratedmodelling.common.configuration.KLAB;
import org.integratedmodelling.common.data.IndexedCategoricalDistribution;
import org.integratedmodelling.common.model.actuators.MediatingActuator;
import org.integratedmodelling.common.utils.CamelCase;
import org.integratedmodelling.common.utils.MapUtils;
import org.integratedmodelling.common.vocabulary.NS;
import org.integratedmodelling.common.vocabulary.Observable;
import org.integratedmodelling.common.vocabulary.Unit;
import org.integratedmodelling.exceptions.ThinklabException;
import org.integratedmodelling.exceptions.ThinklabRuntimeException;
import org.integratedmodelling.exceptions.ThinklabValidationException;
import org.integratedmodelling.thinkQL.Observer;

public class KIMMeasurementObserver extends KIMNumericObserver implements IMeasuringObserver {

    IUnit unit;

    /*
     * -----------------------------------------------------------------------------------
     * accessor - it's always a mediator, either to another measurement or to a datasource
     * whose content was defined explicitly to conform to our semantics
     * -----------------------------------------------------------------------------------
     */
    public class MeasurementActuator extends MediatingActuator {

        IMeasuringObserver other;
        boolean            errorsPresent = false;
        IMonitor           monitor;

        public MeasurementActuator(IMeasuringObserver other, List<IAction> actions, IMonitor monitor,
                boolean interpreting) {
            super(actions, monitor, interpreting);
            this.other = other;
            this.monitor = monitor;

            if (discretization != null)
                ((org.integratedmodelling.common.classification.Classification) discretization)
                        .reset();

        }

        @Override
        public String toString() {
            return "[measurement: " + unit + "]";
        }

        public IUnit getUnit() {
            return unit;
        }

        @Override
        public Object mediate(Object object) {

            double val = Double.NaN;

            if (object == null || (object instanceof Number && Double.isNaN(((Number) object).doubleValue())))
                return val;

            if (object instanceof IndexedCategoricalDistribution) {
                val = ((IndexedCategoricalDistribution) object).getMean();
            } else if (object instanceof Number) {
                val = ((Number) object).doubleValue();
            } else {
                try {
                    val = object.toString().equals("NaN") ? Double.NaN : Double
                            .parseDouble(object.toString());
                } catch (Exception e) {
                    if (!errorsPresent) {
                        monitor.error("cannot interpret value: " + object);
                        errorsPresent = true;
                    }
                    return Double.NaN;
                }
            }

            return unit.convert(val, other.getUnit());
        }
    }

    public KIMMeasurementObserver(KIMContext context, KIMModel model, Observer statement) {
        super(context, model, statement);
        unit = new KIMUnit(context.get(KIMContext.UNIT), statement.getUnit());
        // TODO check consistency of unit with observable
    }

    KIMMeasurementObserver(ISemantic observable, String unit2) {
        if (observable instanceof IObservable) {
            this.observable = (IObservable)observable;
        } else {
            this.observable = new Observable(observable.getType(), KLAB.c(NS.MEASUREMENT), CamelCase.toLowerCase(observable.getType().getLocalName(), '-'));
            ((Observable)this.observable).setObserver(this);
        }
        this.unit = new Unit(unit2);
    }

    public KIMMeasurementObserver(IObserver observer) {
        super(observer);
        if (!(observer instanceof IMeasuringObserver)) {
            throw new ThinklabRuntimeException("cannot initialize a measuring observer from a "
                    + observer.getClass().getCanonicalName());
        }
        this.unit = ((IMeasuringObserver) observer).getUnit();
    }

    public KIMMeasurementObserver(Map<?, ?> map) {

        if (map.containsKey("discretization")) {
            discretization = map.get("discretization") instanceof IClassification
                    ? (IClassification) map.get("discretization")
                    : new Classification((Map<?, ?>) map.get("discretization"));
        }
        // discretization = (IClassification) map.get("discretization");
        unit = new Unit(map.get("unit").toString());
    }

    public KIMMeasurementObserver(String string, IObservable obs) {
        String[] defs = string.split("\\|");
        unit = new Unit(defs[1]);
        if (defs.length > 2) {
            discretization = new org.integratedmodelling.common.classification.Classification(defs[2]);
        }
        observable = obs;
    }

    @Override
    public IObserver copy() {
        return new KIMMeasurementObserver(this);
    }

    @Override
    public IUnit getUnit() {
        return unit;
    }

    @Override
    public IObserver getMediatedObserver() {
        // TODO Auto-generated method stub
        return mediated;
    }

    @Override
    public IConcept getObservationType() {
        return KLAB.c(NS.MEASUREMENT);
    }

    public static String asString(IMeasuringObserver m) {
        String ret = "oMS|" + m.getUnit();
        if (m.getDiscretization() != null) {
            ret += "|"
                    + org.integratedmodelling.common.classification.Classification.asString(m
                            .getDiscretization());
        }
        return ret;
    }

    @Override
    public IStateActuator getMediator(IObserver observer, IMonitor monitor) throws ThinklabException {

        observer = getRepresentativeObserver(observer);

        IActuator ret = getActuator(monitor);
        if (ret != null)
            return (IStateActuator) ret;

        if (!(observer instanceof IMeasuringObserver))
            throw new ThinklabValidationException("measurements can only mediate other measurements");

        if (getActions().size() == 0 && ((IMeasuringObserver) observer).getUnit().equals(getUnit())) {
            return null;
        }

        return new MeasurementActuator((IMeasuringObserver) observer, getActions(), monitor, false);
    }

    @Override
    public IStateActuator getInterpreter(IStateActuator accessor, IMonitor monitor) {
        return new MeasurementActuator(this, getActions(), monitor, true);
    }

    @Override
    public boolean canInterpretDirectly(IActuator actuator) {
        return getActions().size() == 0 && discretization == null;
    }

    @Override
    public Object adapt() {
        return MapUtils.of("unit", getUnit().toString(), "discretization", discretization);
    }

    @Override
    public String toString() {
        return "MEA/" + getObservable() + " (" + unit + ")";
    }

}
