/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.common.kim;

import java.util.List;
import java.util.Map;

import org.integratedmodelling.api.knowledge.IConcept;
import org.integratedmodelling.api.knowledge.ISemantic;
import org.integratedmodelling.api.modelling.IAction;
import org.integratedmodelling.api.modelling.IClassification;
import org.integratedmodelling.api.modelling.IObserver;
import org.integratedmodelling.api.modelling.IProbabilityObserver;
import org.integratedmodelling.api.modelling.runtime.IActuator;
import org.integratedmodelling.api.modelling.runtime.IStateActuator;
import org.integratedmodelling.api.monitoring.IMonitor;
import org.integratedmodelling.common.configuration.KLAB;
import org.integratedmodelling.common.data.IndexedCategoricalDistribution;
import org.integratedmodelling.common.model.actuators.MediatingActuator;
import org.integratedmodelling.common.utils.MapUtils;
import org.integratedmodelling.common.vocabulary.NS;
import org.integratedmodelling.common.vocabulary.Observable;
import org.integratedmodelling.exceptions.ThinklabException;
import org.integratedmodelling.exceptions.ThinklabRuntimeException;
import org.integratedmodelling.thinkQL.Observer;

public class KIMProbabilityObserver extends KIMNumericObserver implements IProbabilityObserver {

    boolean isIndirect;

    public class ProbabilityActuator extends MediatingActuator {

        boolean _errorsPresent = false;

        public ProbabilityActuator(List<IAction> actions, IMonitor monitor, boolean interpreting) {
            super(actions, monitor, interpreting);
            if (discretization != null)
                ((org.integratedmodelling.common.classification.Classification) discretization)
                        .reset();

        }

        @Override
        public String toString() {
            return "[probability " + getType() + "]";
        }

        @Override
        public Object mediate(Object object) {

            double val = Double.NaN;

            if (object == null || (object instanceof Number && Double.isNaN(((Number) object).doubleValue())))
                return val;

            if (object instanceof IndexedCategoricalDistribution) {

                /*
                 * TODO try to mediate distributions
                 */
                val = ((IndexedCategoricalDistribution) object).getMean();
            } else if (object instanceof Number) {
                val = ((Number) object).doubleValue();
            } else {
                try {
                    val = object.toString().equals("NaN") ? Double.NaN : Double
                            .parseDouble(object.toString());
                } catch (Exception e) {
                    if (!_errorsPresent) {
                        monitor.error("cannot interpret value: " + object + " as a number");
                        _errorsPresent = true;
                    }
                    return Double.NaN;
                }
            }

            if (!Double.isNaN(val) && (val < 0 || val > 1)) {
                throw new ThinklabRuntimeException("probabilities cannot have a value outside of the 0-1 range");
            }

            return val;
        }
    }

    public KIMProbabilityObserver(KIMContext context, KIMModel model, Observer statement) {
        super(context, model, statement);
        isIndirect = statement.isDerived();
        if (observable != null) {
            ((Observable) observable)
                    .setType(this.getObservedType(context, this.observable.getTypeAsConcept()));
        }
    }

    public KIMProbabilityObserver(IObserver observer) {
        super(observer);
        if (!(observer instanceof IProbabilityObserver)) {
            throw new ThinklabRuntimeException("cannot initialize a probability observer from a "
                    + observer.getClass().getCanonicalName());
        }
        this.isIndirect = ((KIMProbabilityObserver) observer).isIndirect;
    }

    public KIMProbabilityObserver(Map<?, ?> map) {
        discretization = (IClassification) map.get("discretization");
        isIndirect = map.get("indirect?").equals("true");
    }

    @Override
    public IObserver copy() {
        return new KIMProbabilityObserver(this);
    }

    KIMProbabilityObserver(ISemantic observable) {
        // TODO Auto-generated constructor stub
    }

    @Override
    protected int getDiscretizationContext() {
        return KIMContext.PROPORTION_DISCRETIZATION;
    }

    @Override
    public IConcept getObservationType() {
        return KLAB.c(NS.PROBABILITY_OBSERVATION);
    }

    @Override
    public IConcept getObservedType(KIMContext context, IConcept concept) {

        IConcept ret = concept;
        if (isIndirect) {
            ret = NS.makeProbability(concept);
            if (ret == null) {
                context.error(concept
                        + ": probabilities are assigned only to events or processes. Use the direct form (without 'of') for observables that are already probabilities.", getFirstLineNumber());
                return concept;
            }
        } else if (!concept.is(KLAB.c(NS.CORE_PROBABILITY))) {
            context.error(concept
                    + ": the observable in this statement must be a probability. Use the indirect form (with 'of') for events or processes.", getFirstLineNumber());
        }
        return ret;
    }

    public static String asString(IProbabilityObserver observer) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public IStateActuator getMediator(IObserver observer, IMonitor monitor) throws ThinklabException {
        IActuator ret = getActuator(monitor);
        if (ret != null)
            return (IStateActuator) ret;

        return new ProbabilityActuator(getActions(), monitor, false);
    }

    @Override
    public IStateActuator getInterpreter(IStateActuator accessor, IMonitor monitor) {
        return new ProbabilityActuator(getActions(), monitor, true);
    }

    @Override
    public boolean canInterpretDirectly(IActuator actuator) {
        return getActions().size() == 0 && discretization == null;
    }

    @Override
    public Object adapt() {
        return MapUtils.of("indirect?", isIndirect, "discretization", discretization);
    }
}
