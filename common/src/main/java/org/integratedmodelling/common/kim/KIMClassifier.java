/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.common.kim;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.xtext.nodemodel.util.NodeModelUtils;
import org.integratedmodelling.api.knowledge.IConcept;
import org.integratedmodelling.api.lang.IList;
import org.integratedmodelling.api.modelling.IKnowledgeObject;
import org.integratedmodelling.api.modelling.ILanguageObject;
import org.integratedmodelling.collections.NumericInterval;
import org.integratedmodelling.common.classification.Classifier;
import org.integratedmodelling.thinkQL.ClassifierRHS;
import org.integratedmodelling.thinkQL.ConceptDeclaration;
import org.integratedmodelling.thinkQL.ConceptDeclarationUnion;

public class KIMClassifier extends Classifier implements ILanguageObject {

    int firstLine, lastLine;

    public KIMClassifier(KIMContext context, ClassifierRHS statement, IKnowledgeObject knowledgeToMatch,
            boolean negate) {

        if (negate) {
            negate();
        }

        if (statement == null && knowledgeToMatch != null) {
            conceptMatch = knowledgeToMatch.getConcept();
            return;
        }

        firstLine = NodeModelUtils.getNode(statement).getStartLine();
        lastLine = NodeModelUtils.getNode(statement).getEndLine();

        if (statement.getNum() != null) {
            Number n = KIM.processNumber(statement.getNum());
            numberMatch = n.doubleValue();

        } else if (statement.getBoolean() != null) {

            booleanMatch = statement.getBoolean().equals("true") ? 1 : 0;

        } else if (statement.getInt0() != null) {

            Number from = KIM.processNumber(statement.getInt0());
            Number to = KIM.processNumber(statement.getInt1());
            String lt = statement.getLeftLimit();
            String rt = statement.getRightLimit();
            if (lt == null)
                lt = "inclusive";
            if (rt == null)
                rt = "exclusive";
            intervalMatch = new NumericInterval(from.doubleValue(), to.doubleValue(), lt
                    .equals("exclusive"), rt.equals("exclusive"));

        } else if (statement.getOp() != null) {

            NumericInterval ni = null;
            Number op = KIM.processNumber(statement.getExpression());

            if (statement.getOp().isGe()) {
                ni = new NumericInterval(op.doubleValue(), null, false, true);
            } else if (statement.getOp().isGt()) {
                ni = new NumericInterval(op.doubleValue(), null, true, true);
            } else if (statement.getOp().isLe()) {
                ni = new NumericInterval(null, op.doubleValue(), true, false);
            } else if (statement.getOp().isLt()) {
                ni = new NumericInterval(null, op.doubleValue(), true, true);
            } else if (statement.getOp().isEq()) {
                numberMatch = op.doubleValue();
            } else if (statement.getOp().isNe()) {
                numberMatch = op.doubleValue();
                negate();
            }

            if (ni != null) {
                intervalMatch = ni;
            }

        } else if (statement.getNodata() != null) {

            nullMatch = true;

        } else if (statement.getSet() != null) {

            for (Object o : KIM.nodeToList(statement.getSet())) {
                if (o instanceof Number) {
                    addClassifier(NumberMatcher((Number) o));
                } else if (o instanceof String) {
                    addClassifier(StringMatcher((String) o));
                } else if (o instanceof IConcept) {
                    addClassifier(ConceptMatcher((IConcept) o));
                } else if (o == null) {
                    addClassifier(NullMatcher());
                } else if (o instanceof IList) {
                    addClassifier((Classifier) Multiple((IList) o));
                }
            }

        } else if (statement.getToResolve() != null && statement.getToResolve().size() > 0) {

            for (ConceptDeclarationUnion cdu : statement.getToResolve()) {
                List<IConcept> or = new ArrayList<>();
                for (ConceptDeclaration cd : cdu.getConcept()) {
                    IKnowledgeObject ko = new KIMKnowledge(context.get(KIMContext.CONCEPT), cd, null);
                    if (!ko.isNothing() && ko.isConcept()) {
                        or.add(ko.getConcept());
                    }
                }
                if (or.isEmpty()) {
                    continue;
                }
                if (conceptMatches == null) {
                    conceptMatches = new ArrayList<>();
                }
                conceptMatches.add(or);
            }

        } else if (statement.getConcept() != null) {

            IKnowledgeObject ko = new KIMKnowledge(context.get(KIMContext.CONCEPT), statement
                    .getConcept());
            if (!ko.isNothing() && ko.isConcept()) {
                conceptMatch = ko.getConcept();
            }
        } else if (statement.getString() != null) {
            stringMatch = statement.getString();
        } else if (statement.isStar()) {
            catchAll = true;
        }

    }

    @Override
    public int getFirstLineNumber() {
        return firstLine;
    }

    @Override
    public int getLastLineNumber() {
        return lastLine;
    }
}
