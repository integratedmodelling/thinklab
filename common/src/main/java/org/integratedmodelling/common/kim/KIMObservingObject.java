/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.common.kim;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;
import org.integratedmodelling.api.knowledge.IConcept;
import org.integratedmodelling.api.knowledge.IExpression;
import org.integratedmodelling.api.knowledge.IKnowledge;
import org.integratedmodelling.api.knowledge.IProperty;
import org.integratedmodelling.api.modelling.IAction;
import org.integratedmodelling.api.modelling.IDependency;
import org.integratedmodelling.api.modelling.IFunctionCall;
import org.integratedmodelling.api.modelling.IModel;
import org.integratedmodelling.api.modelling.IModelObject;
import org.integratedmodelling.api.modelling.INamespace;
import org.integratedmodelling.api.modelling.IObserver;
import org.integratedmodelling.api.modelling.IObservingObject;
import org.integratedmodelling.api.modelling.IScale;
import org.integratedmodelling.api.modelling.resolution.IResolutionContext;
import org.integratedmodelling.api.modelling.runtime.IActuator;
import org.integratedmodelling.api.monitoring.IMonitor;
import org.integratedmodelling.collections.Pair;
import org.integratedmodelling.collections.Triple;
import org.integratedmodelling.common.configuration.KLAB;
import org.integratedmodelling.common.utils.CamelCase;
import org.integratedmodelling.common.vocabulary.NS;
import org.integratedmodelling.common.vocabulary.Observable;
import org.integratedmodelling.exceptions.ThinklabException;
import org.integratedmodelling.exceptions.ThinklabRuntimeException;
import org.integratedmodelling.thinkQL.Action;
import org.integratedmodelling.thinkQL.Annotation;
import org.integratedmodelling.thinkQL.Contextualization;
import org.integratedmodelling.thinkQL.Dependency;
import org.integratedmodelling.thinkQL.FunctionOrID;
import org.integratedmodelling.thinkQL.ObservationGenerator;
import org.integratedmodelling.thinkQL.ObservationGeneratorConditional;
import org.integratedmodelling.thinkQL.ObservationGeneratorSwitch;

public abstract class KIMObservingObject extends KIMModelObject implements IObservingObject {

    IFunctionCall actuatorCall;

    List<IFunctionCall> scaleGenerators = new ArrayList<>();
    List<IAction>       actions         = new ArrayList<>();
    List<IDependency>   dependencies    = new ArrayList<>();

    boolean       aggregatesContext;
    Set<IConcept> aggregationDomains = new HashSet<>();

    /*
     * created from scale generators on demand.
     */
    IScale scale = null;

    public KIMObservingObject(KIMContext context, EObject statement, IModelObject parent,
            List<Annotation> annotations) {
        super(context, statement, parent, annotations);
    }

    public KIMObservingObject(KIMContext context, EObject statement, IModelObject parent) {
        super(context, statement, parent);
    }

    protected KIMObservingObject(KIMObserver observer) {
        super(observer);
    }

    public KIMObservingObject(INamespace namespace, String id) {
        super(namespace, id);
        this.id = id;
    }

    protected void defineDependencies(KIMContext context, EList<Dependency> dependencies) {

        for (Dependency dependency : dependencies) {
            KIMDependency d = new KIMDependency(context.get(KIMContext.DEPENDENCY), dependency, this);
            if (!d.isNothing()) {
                this.dependencies.add(d);
            }
        }

        /*
         * ensure that no naming conflicts exist and name any
         * unnamed dependencies.
         */
        Set<String> depNames = new HashSet<>();
        for (IDependency dependency : this.dependencies) {
            if (dependency.getFormalName() == null) {
                String name = ((KIMDependency) dependency).name();
                if (name != null) {
                    if (depNames.contains(name)) {
                        context.error("dependency default name '" + name
                                + " cannot be used as it's ambiguous; please name this dependency explicitly", dependency
                                        .getFirstLineNumber());
                    } else if (!context.hasErrors()) {
                        context.info("dependency was named " + name
                                + " from its observable; use 'named' to explicitly set the name", dependency
                                        .getFirstLineNumber());
                    }
                    depNames.add(name);
                }
            } else {
                if (depNames.contains(dependency.getFormalName())) {
                    context.error("dependency cannot be named '" + dependency.getFormalName()
                            + "': name is ambiguous", dependency.getFirstLineNumber());
                }
                depNames.add(dependency.getFormalName());
            }
        }
    }

    static IObserver defineObserver(KIMContext context, KIMModel model, ObservationGeneratorSwitch statement) {

        if (statement.getMediated() != null) {
            if (statement.getMediated().size() == 1 && statement.getMediated().get(0).getWhen() == null) {
                return defineObserver(context.get(KIMContext.OBSERVER), model, statement.getMediated().get(0)
                        .getObservable());
            } else {
                List<Pair<IObserver, IExpression>> observers = new ArrayList<>();
                for (ObservationGeneratorConditional oc : statement.getMediated()) {
                    IObserver cobs = defineObserver(context.get(KIMContext.OBSERVER), model, oc
                            .getObservable());

                    if (cobs.getObservable() != null
                            && !(NS.isQuality(cobs.getObservable()) || NS.isTrait(cobs.getObservable()))) {
                        context.error("the observable in any observer must be a quality", lineNumber(statement));
                    }

                    IExpression cexp = new KIMExpression(context
                            .get(KIMContext.OBSERVER_CONDITIONAL_EXPRESSION), model, cobs, oc.getWhen());
                    observers.add(new Pair<>(cobs, cexp));
                }
                return new KIMConditional(context
                        .get(KIMContext.OBSERVER_CONDITIONAL), model, observers, statement);
            }
        }
        return null;
    }

    static IObserver defineObserver(KIMContext context, KIMModel model, ObservationGenerator statement) {

        if (statement.getClassification() != null) {
            return new KIMClassificationObserver(context, model, statement.getClassification());
        } else if (statement.getCount() != null) {
            return new KIMCountObserver(context, model, statement.getCount());
        } else if (statement.getDistance() != null) {
            return new KIMDistanceObserver(context, model, statement.getDistance());
        } else if (statement.getMeasurement() != null) {
            return new KIMMeasurementObserver(context, model, statement.getMeasurement());
        } else if (statement.getPresence() != null) {
            return new KIMPresenceObserver(context, model, statement.getPresence());
        } else if (statement.getProbability() != null) {
            return new KIMProbabilityObserver(context, model, statement.getProbability());
        } else if (statement.getProportion() != null) {
            return new KIMProportionObserver(context, model, statement.getProportion());
        } else if (statement.getRanking() != null) {
            return new KIMRankingObserver(context, model, statement.getRanking());
        } else if (statement.getRatio() != null) {
            return new KIMRatioObserver(context, model, statement.getRatio());
        } else if (statement.getUncertainty() != null) {
            return new KIMUncertaintyObserver(context, model, statement.getUncertainty());
        } else if (statement.getValuation() != null) {
            return new KIMValuationObserver(context, model, statement.getValuation());
        }
        // will never get here
        return null;
    }

    static Pair<List<IFunctionCall>, List<IAction>> defineContextActions(KIMContext context, KIMModelObject observer, List<Contextualization> statements) {

        List<IFunctionCall> functions = new ArrayList<>();
        List<IAction> actions = new ArrayList<>();

        for (Contextualization c : statements) {

            boolean init = c.isInitialization();
            boolean aggr = c.isIntegrated();

            Set<IConcept> domains = new HashSet<>();
            boolean ok = true;

            if (c.getDomain() != null) {

                for (FunctionOrID fid : c.getDomain()) {

                    IFunctionCall function = null;
                    if (fid.getFunctionId() != null) {
                        function = new KIMFunctionCall(context.get(KIMContext.COVERAGE_FUNCTION_CALL), fid
                                .getFunctionId());
                    } else if (fid.getFunction() != null) {
                        function = new KIMFunctionCall(context.get(KIMContext.COVERAGE_FUNCTION_CALL), fid
                                .getFunction());
                    }

                    IConcept domain = validateFunctionCall(context, function, KLAB.c(NS.EXTENT));
                    if (domain != null) {
                        domains.add(domain);
                        functions.add(function);
                    } else {
                        ok = false;
                    }

                    /**
                     * Function defines the scale even if no actions are specified.
                     */
                    // if (function != null && observer instanceof KIMObservingObject) {
                    // ((KIMObservingObject) observer).scaleGenerators.add(function);
                    // }
                }
            }

            if (ok && aggr && domains.size() > 0 && observer instanceof KIMObservingObject) {
                ((KIMObservingObject) observer).aggregationDomains.addAll(domains);
                ((KIMObservingObject) observer).aggregatesContext = true;
            }

            for (Action action : c.getActions()) {
                if (ok) {
                    actions.add(new KIMAction(context
                            .get(KIMContext.CONTEXTUALIZATION_ACTION), observer, domains, action));
                } else {
                    context.warning("cannot determine domain: ignoring action", lineNumber(action));
                }
            }
        }

        return new Pair<>(functions, actions);

    }

    @Override
    public List<IDependency> getDependencies() {
        return dependencies;
    }

    @Override
    public boolean hasActionsFor(IConcept observable, IConcept domainConcept) {

        for (IAction a : actions) {
            for (IConcept d : a.getDomain()) {
                if (d.is(domainConcept)) {

                    /*
                     * TODO check observable
                     */
                    return true;
                }
            }
        }

        return false;
    }

    @Override
    public IScale getCoverage() {
        try {
            return makeScale();
        } catch (Exception e) {
            throw new ThinklabRuntimeException(e);
        }
    }

    protected IScale makeScale() throws ThinklabException {
        if (scale == null) {
            scale = KLAB.MFACTORY.createScale(scaleGenerators);
        }
        return scale;
    }

    @Override
    public List<IAction> getActions() {
        return actions;
    }

    @Override
    public boolean isComputed() {
        return isComputed;
    }

    @Override
    public IActuator getActuator(IMonitor monitor) throws ThinklabException {

        IActuator ret = null;
        // this will also take care of creating a default actuator if the call is null and there are actions.
        if (hasActuator()) {
            ret = KLAB.MFACTORY.getActuator(actuatorCall, this, monitor);
        }
        return ret;
    }

    protected void name(String id) {
        this.id = id;
    }

    /**
     * Lookup a model in the namespace 
     * @param context
     * @param c
     * @param lineNumber
     * @return
     */
    static IModel findObservableModel(KIMContext context, IKnowledge c, int lineNumber, boolean complain) {

        IModel ret = null;

        /*
        * we should find one and only one model in the namespace defined before this to model this
        * observable.
        */
        ArrayList<IModel> candidates = new ArrayList<IModel>();
        for (IModelObject o : context.getNamespace().getModelObjects()) {
            if (!o.isInactive() && o instanceof IModel) {

                if (o.getErrorCount() == 0 && ((IModel) o).getObservable().is(c))
                    candidates.add((IModel) o);
            }
        }

        if (candidates.size() == 0 && complain) {
            context.error("bare quality dependencies need a model to interpret them in the namespace; please define one for "
                    + c + "  before this line", lineNumber);
        } else if (candidates.size() > 1) {
            context.error("found more than one model in the namespace to interpret "
                    + c
                    + ": please indicate one explicitly or define an observer here", lineNumber);

        } else if (candidates.size() > 0) {
            ret = candidates.get(0);
        }

        return ret;
    }

    public boolean hasActuator() {
        return actuatorCall != null && actions.isEmpty();
    }

    /**
     * Return an inferred dependencies. Called only at resolution. Only 
     * direct observables are allowed as return values here. Some observers
     * will redefine this.
     * 
     * @param knowledge
     * @throws ThinklabException 
     */
    public List<IKnowledge> getInferredDependencies(IResolutionContext context) throws ThinklabException {
        return new ArrayList<>();
    }

    /**
     * This (non-API for the time being) will pair the stated dependencies with any other
     * coming from the semantics. Some observers may have additional requirements that will
     * also end up here through getInferredDependencies().
     * 
     * @return
     * @throws ThinklabException 
     */
    public List<IDependency> getAllDependencies(IResolutionContext context) throws ThinklabException {

        List<IDependency> ret = new ArrayList<>(dependencies);
        for (IKnowledge c : getInferredDependencies(context)) {
            if (!haveDep(ret, c)) {
                Observable obs = new Observable(c);
                if (NS.isCountable(c)) {
                    obs.setInstantiator(true);
                }
                ret.add(new KIMDependency(obs, obs.getFormalName(), null, false, null));
            }
        }

        /**
         * Add whatever is specified by the semantics and is not already in the stated dependencies.
         * Only for direct observations - indirect are "data" and they naturally can't have anything
         * playing a role in their semantics, only models can define that.
         */
        if (NS.isDirect(getObservable())) {

            for (IConcept c : NS.getRoles(this.getObservable(), context)) {
                if (!haveDep(ret, c)) {
                    String fname = CamelCase.toLowerCase(c.getLocalName(), '-');
                    // TODO depends on the restriction and the property!
                    boolean isOptional = true;
                    IDependency ndep = new KIMDependency(new Observable(c, NS
                            .getObservationTypeFor(c), fname), fname, null, isOptional, null);
                    ret.add(ndep);
                }
            }

            for (Triple<IConcept, IConcept, IProperty> sd : NS.getSemanticDependencies(getObservable())) {
                IConcept dep = sd.getFirst();
                String fname = CamelCase.toLowerCase(dep.getLocalName(), '-');
                if (!haveDep(ret, dep)) {
                    // TODO depends on the restriction and the property!
                    boolean isOptional = true;
                    IDependency ndep = new KIMDependency(new Observable(dep, NS
                            .getObservationTypeFor(dep), fname), fname, sd
                                    .getThird(), isOptional, null);
                    ret.add(ndep);
                }
            }
        }

        /*
         * TODO must check if any dep was abstract, and if so, resolve them contextually to
         * concrete ones, looking up models as necessary. No abstract dependencies should
         * remain.
         */

        return ret;
    }

    private boolean haveDep(List<IDependency> ret, IKnowledge c) {
        for (IDependency d : ret) {
            if (d.getType().is(c)) {
                return true;
            }
        }
        return false;
    }

    // /**
    // * If this dependency is generic, return the list of all dependencies the
    // * observation semantics imply. Else return a list containing just this
    // * dependency. Used during resolution.
    // *
    // * @return
    // */
    // public List<IDependency> concretize() {
    //
    // ArrayList<IDependency> ret = new ArrayList<IDependency>();
    // if (!isGeneric) {
    // ret.add(this);
    // } else {
    // int i = 1;
    // for (IKnowledge c : NS.getConcreteDisjointChildren(observable.getType())) {
    // Observable obs = new Observable((Observable) observable);
    // obs.setType(c);
    // ret.add(new KIMDependency(this, obs, formalName + (i++)));
    // }
    // }
    //
    // return ret;
    // }

}
