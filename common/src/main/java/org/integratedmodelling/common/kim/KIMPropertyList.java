/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.common.kim;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

import org.integratedmodelling.api.knowledge.IKnowledge;
import org.integratedmodelling.api.modelling.IKnowledgeObject;
import org.integratedmodelling.thinkQL.PropertyList;
import org.integratedmodelling.thinkQL.PropertyStatement;

/**
 * Parses a list of declaration, using the context for validation of allowed knowledge.
 * 
 * @author ferdinando.villa
 *
 */
public class KIMPropertyList extends KIMModelObject implements List<IKnowledgeObject> {

    List<IKnowledgeObject> properties = new ArrayList<>();

    public KIMPropertyList(KIMContext context, PropertyStatement main, PropertyList statement,
            KIMModelObject parent) {

        super(context, statement, parent);

        for (String declaration : statement.getProperty()) {
            IKnowledge k = KIMKnowledge.declare(context, main, declaration);
            if (k != null) {
                properties.add(new KIMKnowledge(context, k, this, lineNumber(statement)));
            }
        }
        for (PropertyStatement declaration : statement.getDefinitions()) {
            IKnowledgeObject k = new KIMKnowledge(context, declaration, this, declaration.getAnnotations());
            if (!k.isNothing()) {
                properties.add(k);
            }
        }
    }

    @Override
    public boolean add(IKnowledgeObject arg0) {
        return properties.add(arg0);
    }

    @Override
    public void add(int arg0, IKnowledgeObject arg1) {
        properties.add(arg0, arg1);
    }

    @Override
    public boolean addAll(Collection<? extends IKnowledgeObject> arg0) {
        return properties.addAll(arg0);
    }

    @Override
    public boolean addAll(int arg0, Collection<? extends IKnowledgeObject> arg1) {
        return properties.addAll(arg0, arg1);
    }

    @Override
    public void clear() {
        properties.clear();
    }

    @Override
    public boolean contains(Object arg0) {
        return properties.contains(arg0);
    }

    @Override
    public boolean containsAll(Collection<?> arg0) {
        return properties.containsAll(arg0);
    }

    @Override
    public IKnowledgeObject get(int arg0) {
        return properties.get(arg0);
    }

    @Override
    public int indexOf(Object arg0) {
        return properties.indexOf(arg0);
    }

    @Override
    public boolean isEmpty() {
        return properties.isEmpty();
    }

    @Override
    public Iterator<IKnowledgeObject> iterator() {
        return properties.iterator();
    }

    @Override
    public int lastIndexOf(Object arg0) {
        return properties.lastIndexOf(arg0);
    }

    @Override
    public ListIterator<IKnowledgeObject> listIterator() {
        return properties.listIterator();
    }

    @Override
    public ListIterator<IKnowledgeObject> listIterator(int arg0) {
        return properties.listIterator(arg0);
    }

    @Override
    public boolean remove(Object arg0) {
        return properties.remove(arg0);
    }

    @Override
    public IKnowledgeObject remove(int arg0) {
        return properties.remove(arg0);
    }

    @Override
    public boolean removeAll(Collection<?> arg0) {
        return properties.removeAll(arg0);
    }

    @Override
    public boolean retainAll(Collection<?> arg0) {
        return properties.retainAll(arg0);
    }

    @Override
    public IKnowledgeObject set(int arg0, IKnowledgeObject arg1) {
        return properties.set(arg0, arg1);
    }

    @Override
    public int size() {
        return properties.size();
    }

    @Override
    public List<IKnowledgeObject> subList(int arg0, int arg1) {
        return properties.subList(arg0, arg1);
    }

    @Override
    public Object[] toArray() {
        return properties.toArray();
    }

    @Override
    public <T> T[] toArray(T[] arg0) {
        return properties.toArray(arg0);
    }

}
