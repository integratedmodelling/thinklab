/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.common.kim;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.integratedmodelling.api.knowledge.IAuthority;
import org.integratedmodelling.api.knowledge.IConcept;
import org.integratedmodelling.api.knowledge.IKnowledge;
import org.integratedmodelling.api.knowledge.IProperty;
import org.integratedmodelling.api.metadata.IMetadata;
import org.integratedmodelling.api.modelling.IDirectObserver;
import org.integratedmodelling.api.modelling.IKnowledgeObject;
import org.integratedmodelling.api.modelling.IModel;
import org.integratedmodelling.api.modelling.IModelObject;
import org.integratedmodelling.api.modelling.IObservable;
import org.integratedmodelling.common.configuration.KLAB;
import org.integratedmodelling.common.owl.Ontology;
import org.integratedmodelling.common.thinkql.Validator;
import org.integratedmodelling.common.vocabulary.NS;
import org.integratedmodelling.exceptions.ThinklabException;
import org.integratedmodelling.exceptions.ThinklabRuntimeException;
import org.integratedmodelling.exceptions.ThinklabValidationException;
import org.integratedmodelling.lang.Axiom;
import org.integratedmodelling.thinkQL.Annotation;
import org.integratedmodelling.thinkQL.ConceptDeclaration;
import org.integratedmodelling.thinkQL.ConceptIdentifier;
import org.integratedmodelling.thinkQL.ConceptStatement;
import org.integratedmodelling.thinkQL.PropertyModifier;
import org.integratedmodelling.thinkQL.PropertyStatement;
import org.integratedmodelling.thinkQL.RestrictionDefinition;
import org.integratedmodelling.thinkQL.RestrictionStatement;

public class KIMKnowledge extends KIMModelObject implements IKnowledgeObject {

    // knowledge has errors or inconsistencies
    boolean         isNothing;
    // knowledge has been explicitly declared to be owl:Nothing
    boolean         isOwlNothing;
    IKnowledge      knowledge;
    boolean         isNegated;
    boolean         isDeniable;
    IModel          model;
    int             detailLevel = -1;
    IDirectObserver directObserver;
    IKnowledge      inherentType;
    IKnowledge      contextType;

    /*
     * if allowed to (by using a specific constructor), this will contain a list of
     * unresolved object names.
     */
    List<String> objectDeclarations = new ArrayList<>();

    static Set<String> traitKeywords = new HashSet<String>();

    static {
        traitKeywords.add(KIM.ATTRIBUTE_CONCEPT);
        traitKeywords.add(KIM.REALM_CONCEPT);
        traitKeywords.add(KIM.IDENTITY_CONCEPT);
    }

    /**
     * This creates references to knowledge, potentially external. The namespace for the model object
     * is the one being parsed even if knowledge comes from outside.
     * 
     * @param context
     * @param knowledge
     */
    public KIMKnowledge(KIMContext context, IKnowledge knowledge) {
        super(context, null, null);
        this.knowledge = knowledge;
    }

    /**
     * This creates references to knowledge, potentially external. The namespace for the model object
     * is the one being parsed even if knowledge comes from outside.
     * 
     * @param context
     * @param knowledge
     */
    public KIMKnowledge(KIMContext context, IKnowledge knowledge, IModelObject parent, int lineNumber) {
        super(context, null, parent);
        this.knowledge = knowledge;
    }

    /**
     * Matches ID or optionally creates in namespace. Context to define what is wanted - ensure it's either CONCEPT or PROPERTY.
     * 
     * @param context
     * @param knowledge
     */
    public KIMKnowledge(KIMContext context, String id, boolean createIfAbsent, int lineNumber) {

        super(context, null, null);

        boolean external = id.contains(":");
        IKnowledge existing = null;
        if (external) {
            existing = KLAB.KM.getKnowledge(id);
        } else {
            if (context.is(KIMContext.PROPERTY)) {
                existing = context.getNamespace().getOntology().getProperty(id);
            } else if (context.is(KIMContext.CONCEPT)) {
                existing = context.getNamespace().getOntology().getConcept(id);
            }
        }

        if (external && existing == null) {
            isNothing = true;
            return;
        }

        if (external) {
            context.getNamespace().checkImported(context, existing, lineNumber);
        }

        if (context.is(KIMContext.PROPERTY)) {

            if (existing instanceof IProperty) {
                knowledge = existing;
            } else if (existing == null && createIfAbsent) {
                knowledge = ((Ontology) context.getNamespace().getOntology()).createProperty(id, false);
            } else {
                isNothing = true;
            }
        } else if (context.is(KIMContext.CONCEPT)) {

            if (existing instanceof IConcept) {
                knowledge = existing;
            } else if (existing == null && createIfAbsent) {
                ((Ontology) context.getNamespace().getOntology()).createConcept(id);
            } else {
                isNothing = true;
            }
        } else {
            isNothing = true;
        }

    }

    public KIMKnowledge(KIMContext context, PropertyStatement propertyStatement,
            IModelObject parent, List<Annotation> annotations) {

        super(context, propertyStatement, parent, annotations);

        String id = propertyStatement.getId();

        if (id == null) {
            return;
        }

        boolean isObject = !(propertyStatement.isData() || propertyStatement.isAnnotation());
        knowledge = declare(context, propertyStatement, propertyStatement.getId());

        if (!id.contains(":")) {

            if (propertyStatement.getModifiers() != null) {

                for (PropertyModifier pm : propertyStatement.getModifiers().getModifier()) {

                    /*
                     * TODO these are subproperties!
                     */
                    if (pm.getValue() == PropertyModifier.FUNCTIONAL_VALUE) {
                        context.getNamespace()
                                .addAxiom(isObject ? Axiom.FunctionalObjectProperty(id) : Axiom
                                        .FunctionalDataProperty(id));
                    }
                    /*
                     * TODO the rest
                     */
                }
            }

        } else {
            if (propertyStatement.getModifiers() != null) {
                context.error("cannot apply modifiers to an external property", lineNumber(propertyStatement));
            }
        }

        if (propertyStatement.isAbstract()) {
            context.getNamespace().addAxiom(Axiom.AnnotationAssertion(id, NS.IS_ABSTRACT, "true"));
        }

        if (propertyStatement.getParents() != null) {
            for (IKnowledgeObject co : new KIMPropertyList(context
                    .get(KIMContext.PARENT_LIST), propertyStatement, propertyStatement
                            .getParents(), null)) {
                context.getNamespace().addAxiom(isObject ? Axiom.SubObjectProperty(co.getName(), id)
                        : Axiom.SubDataProperty(co.getName(), id));
            }
        }

        /*
         * children
         */
        if (propertyStatement.getChildren() != null) {
            ArrayList<String> children = new ArrayList<String>();
            for (IKnowledgeObject co : new KIMPropertyList(context
                    .get(KIMContext.CHILD_LIST), propertyStatement, propertyStatement
                            .getChildren(), null)) {
                if (co != null) {

                    context.getNamespace().addAxiom(isObject ? Axiom.SubObjectProperty(id, co.getId())
                            : Axiom.SubDataProperty(id, co.getId()));
                    children.add(co.getId());

                    /*
                     * record declaration hierarchy
                     * FIXME redundant with new constructors - check behavior.
                     */
                    ((KIMKnowledge) co).parent = this;
                    this.children.add(co);
                }
            }
        }

        if (propertyStatement.getDomain() != null) {
            for (IKnowledgeObject co : new KIMConceptList(context
                    .get(KIMContext.PROPERTY_DOMAIN), propertyStatement
                            .getDomain(), this)) {
                context.getNamespace().addAxiom(Axiom.ObjectPropertyDomain(id, co.getName()));
            }
        }

        if (propertyStatement.getRange() != null) {
            for (IKnowledgeObject co : new KIMConceptList(context
                    .get(KIMContext.PROPERTY_RANGE), propertyStatement
                            .getRange(), this)) {
                context.getNamespace().addAxiom(Axiom.ObjectPropertyRange(id, co.getName()));
            }
        }

        /*
         * TODO the rest
         */
    }

    public KIMKnowledge(KIMContext context, ConceptDeclaration statement) {
        this(context, statement, null);
    }

    public KIMKnowledge(KIMContext context, ConceptDeclaration statement, IModelObject parent) {
        this(context, statement, parent, false);
    }

    /**
     * If allowObjects is true, won't complain for anything undefined and just set a list of 
     * names declared. 
     * 
     * @param context
     * @param statement
     * @param parent
     * @param allowObjects
     */
    public KIMKnowledge(KIMContext context, ConceptDeclaration statement, IModelObject parent,
            boolean allowObjects) {

        super(context, statement, parent, new ArrayList<Annotation>());
        this.knowledge = declare(context, statement, allowObjects);

        if (objectDeclarations.size() > 0) {
            return;
        }

        /*
         * these must be done outside of declare() after the concept is clarified.
         */
        if (this.knowledge != null && statement.getDownTo() != null) {
            this.detailLevel = NS.getDetailLevel(this.knowledge, statement.getDownTo());
            if (this.detailLevel < 0) {
                context.error("the detail level indicated is not recognized for this concept, or the concept is not a class or trait", lineNumber(statement));
            }
            if (this.detailLevel == 0) {
                context.error("a concept cannot be observed at the level of detail of its most general abstraction", lineNumber(statement));
            }
        }

        if (this.knowledge != null && statement.getInherent() != null && statement.getInherent().size() > 0) {
            if (!NS.isQuality(this.knowledge) && !NS.isTrait(this.knowledge)
                    && !NS.isConfiguration(this.knowledge)) {
                context.error("inherent types ('of') can only be specified for qualities and traits", lineNumber(statement));
            }
            inherentType = declare(context, statement.getInherent(), lineNumber(statement));
        }

        if (this.knowledge != null && statement.getOuterContext() != null
                && statement.getOuterContext().getIds() != null &&
                statement.getOuterContext().getIds().size() > 0) {
            contextType = declare(context, statement.getOuterContext()
                    .getIds(), lineNumber(statement.getOuterContext()));

            if (contextType == null || !NS.isThing(contextType)) {
                context.error("the context type ('within') must be a subject", lineNumber(statement
                        .getOuterContext()));
            }
        }
    }

    public KIMKnowledge(KIMContext context, ConceptStatement statement, IModelObject parent) {
        this(context, statement, parent, new ArrayList<Annotation>());
    }

    public KIMKnowledge(KIMContext context, PropertyStatement statement, IModelObject parent) {
        this(context, statement, parent, new ArrayList<Annotation>());
    }

    public KIMKnowledge(KIMContext context, ConceptStatement statement, IModelObject parent,
            List<Annotation> annotations) {

        super(context, statement, parent, annotations);

        if (statement.getConcept() != null) {
            context.setCoreConcept(NS.getCoreConceptFor(statement.getConcept()));
        }

        IConcept coreConcept = context.isWithin(KIMContext.TRAIT_LIST) ? KLAB.c(NS.CORE_TRAIT)
                : context.getCoreConcept();

        // happens in error
        if (statement == null || (!statement.isRoot()
                && statement.getDeclaration() == null))
            return;

        if (statement.isRoot()) {
            knowledge = KLAB.c(NS.CORE_DOMAIN);
        } else if (statement.isRedeclaration()) {
            knowledge = new KIMKnowledge(context, statement.getRedeclared()).getKnowledge();
        } else {
            knowledge = declare(context, Collections
                    .singleton(statement.getDeclaration()), lineNumber(statement));
        }

        if (knowledge == null) {
            return;
        }

        if (statement.isNothing()) {
            // knowledge = Env.KM.getNothing();
            this.isOwlNothing = true;
        }

        boolean isOrdering = false;
        boolean isSubjective = false;
        boolean needsSuperclass = false;
        boolean isPrimary = statement.getConcept() != null;

        IConcept cconcept = null;

        if (statement.isRoot()) {
            if (!statement.getConcept().equals("domain")) {
                context.error("root is only allowed in core domain specifications: "
                        + knowledge, lineNumber(statement));
                return;
            }
            if (context.getNamespace().getDomain() == null
                    || !context.getNamespace().getDomain().is(KLAB.c(NS.CORE_DOMAIN))) {
                context.error("domain specifications are only allowed in the core domain: "
                        + knowledge, lineNumber(statement));
                return;
            }

            cconcept = coreConcept = KLAB.c(NS.CORE_DOMAIN);

        } else {
            cconcept = this.getConcept();
        }

        String id = knowledge.toString();
        if (id.startsWith(context.getNamespace().getId() + ":")) {
            id = knowledge.getLocalName();
        }

        boolean isExternal = !statement.isRoot() && id.contains(":") && !isOwlNothing;

        if (isExternal && cconcept == null) {
            context.error("cannot redefine unknown external concept "
                    + knowledge, lineNumber(statement));
            return;
        }

        if (coreConcept == null) {

            if (isExternal) {
                coreConcept = (IConcept) NS.getCoreType(cconcept);
            } else {

                String key = statement.getConcept();
                if (key == null) {
                    context.error("internal: semantic key expected", lineNumber(statement));
                }
                coreConcept = NS.getCoreConceptFor(key);
                if (coreConcept == null) {
                    context.error("internal: cannot resolve core concept at "
                            + key, lineNumber(statement));
                }
            }
        }

        /*
         * add to ontology
         */
        if (!isExternal) {

            /*
             * set as child of whatever concept is implied by the keyword used to declare ACHTUNG: this
             * depends on the keys being the same as the language keywords in IResolver.
             */
            String key = statement.getConcept();
            String mod = statement.getSpecifier();

            /*
             * Modify according to modifiers, for now only supported for agent types.
             */
            if (key != null) {

                isOrdering = key.equals(KIM.ORDERING_CONCEPT);
                isSubjective = mod != null && mod.equals(KIM.SUBJECTIVE_SPECIFIER);

                // simplify handling below
                if (isSubjective) {
                    if (!(key.equals(KIM.ATTRIBUTE_CONCEPT) || key
                            .equals(KIM.ORDERING_CONCEPT))) {
                        context.error("only attributes and ordering traits can use the 'subjective' modifier", lineNumber(statement));
                    }
                    mod = null;
                    key = KIM.SUBJECTIVE_SPECIFIER;
                }

                if (mod != null && !key.equals(KIM.AGENT_CONCEPT)) {
                    context.error("only agent concepts can use the modifier "
                            + mod, lineNumber(statement));
                } else if (mod != null) {
                    key = mod + "-" + key;
                }

                if (coreConcept != null && cconcept != null && !cconcept.is(coreConcept)) {

                    /*
                     * ensure it isn't redeclaring a different kind of observable.
                     */
                    ThinklabException exc = Validator.ensureCompatible(cconcept, coreConcept);
                    if (exc != null) {
                        context.error(exc.getMessage(), lineNumber(statement));
                    }

                    needsSuperclass = true;

                }

                if (!statement.isRoot()) {
                    context.getNamespace()
                            .addAxiom(Axiom.AnnotationAssertion(id, NS.BASE_DECLARATION, "true"));
                }

            }
        } else {

            ThinklabException exc = Validator.ensureCompatible(cconcept, coreConcept);
            if (exc != null) {
                context.error(exc.getMessage(), lineNumber(statement));
            }
            context.getNamespace().checkImported(context, knowledge, lineNumber(statement));
        }

        /*
         * modifiers
         */
        if (statement.isAbstract()) {

            if (isExternal) {
                context.error("modifiers are illegal on external concepts", lineNumber(statement));
                return;
            }
            context.getNamespace().addAxiom(Axiom.AnnotationAssertion(id, NS.IS_ABSTRACT, "true"));
        }

        context.synchronize();

        /*
         * parents
         */
        if (statement.getParents() != null) {

            if (isExternal) {
                context.error("parents are illegal on external concepts", lineNumber(statement));
                return;
            }

            for (IKnowledgeObject co : new KIMConceptList(context.get(KIMContext.PARENT_LIST), statement
                    .getParents(), this)) {

                if (NS.isNothing(co)) {

                    if (knowledge != null) {
                        NS.registerNothingConcept(knowledge.toString());
                    }
                    if (statement.getDocstring() == null) {
                        context.getNamespace().addAxiom(Axiom
                                .AnnotationAssertion(id, IMetadata.DC_COMMENT, "this concept derives from an inconsistent ancestor and cannot be used."));
                    }
                }

                /*
                 * check for proper inheritance
                 */
                IConcept c = co.getConcept();
                IConcept core = (IConcept) NS.getCoreType(c);
                if (!Validator.checkParent(core, coreConcept)) {
                    context.error("cannot make "
                            + (core == null ? id : ("a " + coreConcept))
                            + " a child of "
                            + (core == null ? ("the partially defined concept " + c)
                                    : core.toString()), lineNumber(statement.getParents()));
                } else {

                    context.getNamespace().addAxiom(Axiom.SubClass(co.getName(), id));

                    /*
                     * don't add the core superclass if any of the parents already has it.
                     */
                    if (c.is(coreConcept)) {
                        needsSuperclass = false;
                    }
                }
            }
        }

        if (needsSuperclass) {
            context.getNamespace().addAxiom(Axiom.SubClass(coreConcept.toString(), id));
        }

        context.synchronize();

        int conTraits = 0;

        /*
         * contextualized traits - only for classes and allowed only on the primary concept spec.
         */
        if (statement.getContextualizedTraits() != null) {

            if (isExternal) {
                context.error("exposing traits is illegal on external concepts", lineNumber(statement
                        .getContextualizedTraits()));
                return;
            }

            if (!coreConcept.is(KLAB.c(NS.TYPE))) {
                context.error("only classes can use the 'expose' notation", lineNumber(statement
                        .getContextualizedTraits()));
                return;
            }

            /*
             * check that no ancestors expose any traits.
             */
            IConcept anc = NS.getRootType(context.getNamespace().getOntology().getConcept(id));
            if (!statement.isSpecific() && anc != null
                    && !anc.equals(context.getNamespace().getOntology().getConcept(id))) {
                context.error("traits are already exposed by parent concept "
                        + anc
                        + ": only one class in a hierarchy can expose traits", lineNumber(statement
                                .getContextualizedTraits()));
                return;
            }

            /**
             * reset static contextualized trait index in reasoner
             */
            NS.resetContextualizedTraits(context.getNamespace().getOntology().getConcept(id));

            /*
             * create reference list in order of declaration.
             */
            List<IObservable> exposedTraits = null;
            List<IObservable> adoptedTraits = null;
            if (statement.isSpecific()) {
                exposedTraits = NS.getExposedTraitsForType((IConcept) knowledge);
                if (exposedTraits == null) {
                    context.error("the traits to adopt have not been declared in a parent class using an 'exposes' statement", lineNumber(statement
                            .getContextualizedTraits()));
                    return;
                }
                adoptedTraits = new ArrayList<>();
                for (int i = 0; i < exposedTraits.size(); i++) {
                    adoptedTraits.add(null);
                }
            } else {
                exposedTraits = new ArrayList<>();
            }

            for (IKnowledgeObject co : new KIMConceptList(context
                    .get(statement.isSpecific() ? KIMContext.ADOPTED_EXPOSED_TRAIT_LIST
                            : KIMContext.EXPOSED_TRAIT_LIST), statement.getContextualizedTraits(), this)) {

                IKnowledge t = co.getKnowledge();

                if (t == null) {
                    continue;
                }

                Collection<IConcept> negation = null;
                if (NS.isObservable(t)) {

                    /*
                     * turn to trait
                     */
                    t = NS.getTraitFor(t);
                    if (statement.isSpecific()) {
                        t = NS.getConcreteObservabilityTrait(NS.getTraitFor(t), co.isNegated());
                    } else if (co.isNegated()) {
                        // don't think the syntax allows this anyway
                        context.error(co.getId()
                                + ": cannot negate an abstract observability trait: negations can only be used in concrete subclasses", co
                                        .getFirstLineNumber());
                    }

                    // set back into knowledge object because we'll build an observable with the rest of the
                    // info.
                    ((KIMKnowledge) co).knowledge = t;

                } else if (!NS.isTrait(t)) {
                    context.error(co.getId() + ": only traits or observables are allowed here", co
                            .getFirstLineNumber());
                    continue;
                } else {
                    if (co.isNegated()) {
                        negation = NS.getNegation((IConcept) t);
                        if (negation.isEmpty()) {
                            context.error(co.getId()
                                    + ": this trait cannot be negated; ensure it's a deniable trait", co
                                            .getFirstLineNumber());
                            continue;
                        }
                    }
                }

                if (!statement.isSpecific()) {

                    if (!t.isAbstract()) {
                        context.error(co.getId()
                                + ": the traits exposed by a classification must be abstract", co
                                        .getFirstLineNumber());
                        continue;
                    }

                    /*
                     * inform the knowledge base that we are contextualizing this trait.
                     */
                    NS.addExposedTrait((IConcept) knowledge, new KIMObservable(context, (KIMKnowledge) co));

                } else {

                    if (t.isAbstract()) {
                        context.error(co.getId()
                                + ": the traits adopted by a concrete class must be concrete", co
                                        .getFirstLineNumber());
                        continue;
                    }

                    int nMatch = 0;
                    int i = 0;
                    int thm = -1;
                    String mtchdes = "";
                    IObservable thr = null;
                    for (IObservable o : exposedTraits) {
                        if (t.is(o)) {
                            nMatch++;
                            thr = o;
                            thm = i;
                            mtchdes += (mtchdes.isEmpty() ? "" : ", ") + o;
                        }
                        i++;
                    }

                    if (nMatch == 0) {
                        context.error("no matching exposed trait for " + co, co.getFirstLineNumber());
                        continue;
                    } else if (nMatch > 1) {
                        context.error("ambiguous exposed trait for " + co + ": can match " + mtchdes, co
                                .getFirstLineNumber());
                        continue;
                    }

                    /*
                     * negation may be not null. Must use an observable
                     */
                    adoptedTraits.add(thm, new KIMObservable(context, (KIMKnowledge) co));
                }
            }

            if (adoptedTraits != null) {
                NS.setAdoptedTraits(anc, (IConcept) knowledge, adoptedTraits);
            }

        } else if (!isExternal && coreConcept.is(KLAB.c(NS.TYPE)) && !knowledge.isAbstract()) {
            context.error("concrete class concepts must adopt ('with') one or more traits exposed ('exposes') by a superclass", lineNumber(statement));
        }

        context.synchronize();

        /**
         * Must be a trait: defines which (existing) observables it applies to.
         */
        if (statement.getTraitTargets() != null) {

            if (isExternal) {
                context.error("this specification is illegal on external concepts", lineNumber(statement
                        .getTraitTargets()));
                return;
            }

            for (IKnowledgeObject co : new KIMConceptList(context
                    .get(KIMContext.TARGET_TRAIT_LIST), statement
                            .getTraitTargets(), this)) {

                /*
                 * TODO do something
                 */
            }

        } else if (coreConcept != null && NS.isConfiguration(coreConcept)) {
            context.error("patterns must specify a subject type in the 'applies to' clause", lineNumber(statement));
        }

        /**
         * Must be a process: (existing) qualities that it can affect
         */
        if (statement.getQualitiesAffected() != null) {

            if (isExternal) {
                context.error("this specification is illegal on external concepts", lineNumber(statement
                        .getQualitiesAffected()));
                return;
            }

            if (coreConcept != null && !NS.isProcess(coreConcept)) {
                context.error("only processes can use the 'affects' clause, to define the qualities they modify in their context", lineNumber(statement));
            }

            for (IKnowledgeObject co : new KIMConceptList(context
                    .get(KIMContext.AFFECTED_QUALITIES_LIST), statement
                            .getQualitiesAffected(), this)) {

                /*
                 * TODO do something
                 */
            }
        }

        /**
         * Must be a process: (existing) traits it will confer to its participants.
         */
        if (statement.getConferredTraits() != null) {

            if (isExternal) {
                context.error("this specification is illegal on external concepts", lineNumber(statement
                        .getConferredTraits()));
                return;
            }

            /*
             * TODO two different restrictions for permanent confer or temporary
             */

            if (coreConcept != null && !(NS.isProcess(coreConcept) || NS.isEvent(coreConcept))) {
                context.error("only processes can use the 'confers' clause, to define traits they confer to their context subjects", lineNumber(statement));
            }

            for (IKnowledgeObject co : new KIMConceptList(context
                    .get(KIMContext.CONFERRED_TRAIT_LIST), statement
                            .getConferredTraits(), this)) {

                /*
                 * TODO do something
                 */
            }

        }

        /**
         * Must be a trait: (existing) traits it will confer to its participants.
         */
        if (statement.getDescribedQuality() != null) {

            if (isExternal) {
                context.error("this specification is illegal on external concepts", lineNumber(statement
                        .getDescribedQuality()));
                return;
            }

            if (coreConcept != null && !NS.isTrait(coreConcept)) {
                context.error("only processes can use the 'confers' clause, to define traits they confer to their context subjects", lineNumber(statement));
            }

            IKnowledgeObject ko = new KIMKnowledge(context.get(KIMContext.TRAIT_DESCRIBED_QUALITY), statement
                    .getDescribedQuality(), this);

        }

        /**
         * Set constraint on required traits
         */
        if (statement.getRequirement() != null) {

            if (isExternal) {
                context.error("this specification is illegal on external concepts", lineNumber(statement
                        .getRequirement()));
                return;
            }

        }

        if (statement.getActuallyInheritedTraits() != null) {

            if (isExternal) {
                context.error("this specification is illegal on external concepts", lineNumber(statement
                        .getActuallyInheritedTraits()));
                return;
            }

            if (statement.getConcept() != null && traitKeywords.contains(statement.getConcept())) {
                context.error("traits cannot use 'inherits' - use 'is' to derive from other traits", lineNumber(statement
                        .getActuallyInheritedTraits()));
            } else {

                for (IKnowledgeObject co : new KIMConceptList(context
                        .get(KIMContext.INHERITED_TRAIT_LIST), statement
                                .getActuallyInheritedTraits(), this)) {

                    IConcept trait = co.getConcept();
                    if (!NS.isTrait(trait, false) || NS.isObservable(trait)) {
                        context.error("only traits are allowed in an 'observing' list", lineNumber(statement
                                .getActuallyInheritedTraits()));
                    } else {
                        context.getNamespace().addAxiom(Axiom.SubClass(co.getName(), id));
                    }

                }
            }
        }

        context.synchronize();

        if (statement.getRoles() != null) {

            if (statement.getConcept() != null && traitKeywords.contains(statement.getConcept())) {
                context.error("traits cannot have roles", lineNumber(statement
                        .getRoles()));
            } else {

                for (IKnowledgeObject co : new KIMConceptList(context.get(KIMContext.ROLE_LIST), statement
                        .getRoles(), this)) {

                    IConcept trait = KLAB.c(co.getName());
                    if (!NS.isRole(trait) || NS.isObservable(trait)) {
                        context.error(trait
                                + " is not a role", lineNumber(statement.getRoles()));
                    } else {

                        IConcept main = getConcept();

                        List<IKnowledgeObject> targets = new ArrayList<>();
                        if (statement.getTargetObservable() != null) {
                            for (IKnowledgeObject rob : new KIMConceptList(context
                                    .get(KIMContext.ROLE_TARGET_OBSERVABLE_LIST), statement
                                            .getTargetObservable(), this)) {
                                targets.add(rob);
                            }
                        }
                        if (targets.isEmpty()) {
                            targets.add(null);
                        }

                        for (IKnowledgeObject target : targets) {

                            IConcept trg = target == null ? null : target.getConcept();

                            if (trg != null && !NS.isCountable(trg)) {
                                context.error("the target context ('for') of a role must be a subject or an event", lineNumber(statement
                                        .getTargetObservable()));
                            }

                            for (IKnowledgeObject rob : new KIMConceptList(context
                                    .get(KIMContext.ROLE_RESTRICTED_OBSERVABLE_LIST), statement
                                            .getRestrictedObservable(), this)) {

                                IConcept restricted = KLAB.c(rob.getName());

                                if (restricted == null
                                        || !(NS.isCountable(restricted) || NS.isProcess(restricted))) {
                                    context.error("the target scenario ('in') of a role must be a process, subject or event", lineNumber(statement
                                            .getRestrictedObservable()));
                                }

                                // Collection<IProperty> pr = restricted.findRestrictingProperty(trait);
                                // if (pr.size() > 0) {
                                // for (IProperty prop : pr) {
                                // restricted.getOntology()
                                // .define(Collections.singleton(Axiom.SomeValuesFrom(restricted
                                // .getLocalName(), prop.toString(), main.toString())));

                                /*
                                 * leave the restriction so that it shows up in OWL, but use the scoped catalog 
                                 * in NS for any operation involving roles.
                                 */
                                NS.addRole(trait, main, trg, restricted, /* prop,*/ context
                                        .getNamespace());
                                // }
                                //
                                // } else {
                                // context.error("cannot attribute role "
                                // + main
                                // + " unless a restriction on " + trait
                                // + " authorizes it within "
                                // + restricted, lineNumber(statement));
                                // }
                            }
                        }

                    }

                }
            }
        }

        context.synchronize();

        /*
         * Equivalence: OK with externals
         */
        if (statement.getEquivalences() != null) {
            for (IKnowledgeObject co : new KIMConceptList(context
                    .get(KIMContext.EQUIVALENT_CONCEPT_LIST), statement
                            .getEquivalences(), this)) {

                String coc = co.getNamespace().getId().equals(context.getNamespace().getId()) ? co.getId()
                        : co.getName();
                context.getNamespace().addAxiom(Axiom.EquivalentClasses(coc, id));
            }
            context.synchronize();
        }

        /*
         * children
         */
        if (statement.getChildren() != null) {
            ArrayList<String> children = new ArrayList<String>();
            int nC = 1;
            for (IKnowledgeObject co : new KIMConceptList(context
                    .get(KIMContext.CHILD_LIST), statement.getChildren(), this)) {

                if (co != null) {
                    context.getNamespace().addAxiom(Axiom.SubClass(id, co.getId()));
                    children.add(co.getId());
                    if (isOrdering) {
                        context.getNamespace()
                                .addAxiom(Axiom.AnnotationAssertion(co.getId(), NS.ORDER_PROPERTY, "" + nC));
                    }

                    /*
                     * record declaration hierarchy
                     * FIXME redundant with new constructors - check behavior. Or don't.
                     */
                    ((KIMKnowledge) co).parent = this;
                    this.children.add(co);

                    nC++;
                }
            }
            if (statement.isDisjoint()) {
                context.getNamespace()
                        .addAxiom(Axiom.DisjointClasses(children.toArray(new String[children.size()])));
            }
        }

        context.synchronize();

        if (statement.getRestrictions() != null && statement.getRestrictions().size() > 0) {

            if (isExternal) {
                context.error("cannot restrict external concepts", lineNumber(statement));
                return;
            }

            for (RestrictionStatement rs : statement.getRestrictions()) {

                for (RestrictionDefinition rd : rs.getRelDefs().getDefinitions()) {
                    new KIMRestriction(context.get(getContextForRelationship(rs.getRelType())), rd, this);
                }
            }
        }

        if (statement.getMetadata() != null) {

            if (isExternal) {
                context.error("cannot add metadata to external concepts", lineNumber(statement
                        .getMetadata()));
                return;
            }

            metadata = new KIMMetadata(context, statement.getMetadata(), this);

            /*
             * assert annotations from metadata
             */
            for (String key : metadata.getKeys()) {
                /*
                 * TODO check that key is a valid property; use a catalog key -> property if one exists
                 */
                Object o = metadata.get(key);
                context.getNamespace().addAxiom(Axiom.AnnotationAssertion(id, key, o));
            }

            context.synchronize();
        }

        if (statement.getDocstring() != null) {

            if (isExternal) {
                context.error("cannot add descriptions to external concepts", lineNumber(statement));
                return;
            }

            if (!statement.isRoot()) {
                context.getNamespace().addAxiom(Axiom.AnnotationAssertion(id, IMetadata.DC_COMMENT, statement
                        .getDocstring()));
                try {
                    context.synchronize();
                } catch (Throwable e) {
                    context.error("internal error: " + e.getMessage(), lineNumber(statement));
                }
            }
        }

        /*
         * TODO/NOW gather and validate constraints on required traits.
         */

        /*
         * annotations in inner objects are only allowed on dependent
         * concept and property definitions. Just check that there aren't - they 
         * have been processed already.
         */
        if (statement.getAnnotations() != null && statement.getAnnotations().size() > 0) {

            if (isExternal) {
                context.error("cannot add annotations to external concepts", lineNumber(statement
                        .getAnnotations().get(0)));
                return;
            }
        }

        /*
         * if it's an authority-identify concept, we ensure it's an identity (or whatever the
         * authority requires) and alias it in the ontology so that any reasoning involving
         * the concept is actually done on the stable identity.
         */
        if (statement.getAuthority() != null && knowledge != null && coreConcept != null) {

            IAuthority auth = KLAB.KM.getAuthority(statement.getAuthority());
            if (auth == null) {
                context.error("no authority named "
                        + statement.getAuthority()
                        + " has been configured", lineNumber(statement));
            }
            try {
                String error = auth.validateCoreConcept(coreConcept, statement.getIdentifier());
                if (error != null) {
                    context.error(error, lineNumber(statement));
                } else {
                    IConcept identity = auth.getIdentity(statement.getIdentifier());
                    if (identity != null) {
                        ((Ontology) context.getNamespace().getOntology())
                                .alias(knowledge, identity);
                        context.getNamespace().registerAuthority(identity.getConceptSpace());
                    } else {
                        context.error("authority " + auth.getAuthorityId() + " does not recognize identifier "
                                + statement.getIdentifier(), lineNumber(statement));
                    }
                }
            } catch (Exception e) {
                context.error(e.getMessage(), lineNumber(statement));
            }
        } else if (isPrimary && NS.getDomain(knowledge) == null && !NS.isDomain(knowledge)) {
            if (NS.getAnnotations(this, "internal").isEmpty()) {
                context.warning("The concept does not belong to a domain. This will prevent this namespace from being published. Please ensure its inheritance is complete or mark it internal.", lineNumber(statement));
            }
        }

        context.synchronize();

        if (statement.isDeniable()) {

            if (isExternal) {
                context.error("modifiers are illegal on external concepts", lineNumber(statement));
                return;
            }

            if (knowledge != null && !NS.isTrait(knowledge)) {
                context.error("only traits can be declared deniable", lineNumber(statement));
            }

            isDeniable = true;
        }

        if (isOwlNothing) {
            isNothing = true;
            if (knowledge != null) {
                NS.registerNothingConcept(knowledge.toString());
            }
        }

    }

    private int getContextForRelationship(String relType) {
        switch (relType) {
        case "uses":
            return KIMContext.USES_RELATIONSHIP;
        case "contains":
            return KIMContext.CONTAINS_RELATIONSHIP;
        case "has":
            return KIMContext.HAS_RELATIONSHIP;
        }
        throw new ThinklabRuntimeException("unsupported relationship type " + relType);
    }

    static IKnowledge declare(KIMContext context, PropertyStatement statement, String id) {

        boolean isRoot = statement.getId().equals(id);
        boolean mustExist = !context.isWithin(KIMContext.CHILD_LIST) && !isRoot;
        IKnowledge ret = null;

        boolean isObject = !(statement.isData() || statement.isAnnotation());

        if (id.contains(":")) {
            ret = KLAB.KM.getProperty(id);
            if (ret == null) {
                context.error("cannot resolve external property " + id, lineNumber(statement));
            } else {
                context.getNamespace().checkImported(context, ret, lineNumber(statement));
            }

        } else {

            ret = context.getNamespace().getOntology().getProperty(id);

            if (ret == null) {
                if (mustExist) {
                    context.error("knowledge " + statement.getId()
                            + " is unknown: new declarations are not allowed in this context", lineNumber(statement));
                } else {

                    context.getNamespace()
                            .addAxiom(statement.isAnnotation() ? Axiom.AnnotationPropertyAssertion(id)
                                    : (isObject ? Axiom
                                            .ObjectPropertyAssertion(id) : Axiom.DataPropertyAssertion(id)));
                    context.getNamespace().synchronizeKnowledge();
                    ret = context.getNamespace().getOntology().getProperty(id);

                }
            }
        }

        return ret;
    }

    private IKnowledge declare(KIMContext context, ConceptDeclaration statement) {
        return declare(context, statement, false);
    }

    private IKnowledge declare(KIMContext context, ConceptDeclaration statement, boolean allowObjects) {

        boolean propertyOK = context.isWithin(KIMContext.PROPERTY)
                || context.isWithin(KIMContext.MODEL_OBSERVABLE); // TODO link to context
        boolean mustExist = !context.isWithin(KIMContext.CHILD_LIST); // TODO link to context
        boolean mustBeObservable = context.isWithin(KIMContext.OBSERVER)
                || context.isWithin(KIMContext.MODEL); // TODO link to context
        IConcept rootConcept = null; // TODO link to context

        if (statement == null || statement.getIds() == null || statement.getIds().isEmpty()) {
            this.isNothing = true;
            return null;
        }

        if (allowObjects) {
            List<String> objIds = new ArrayList<>();
            for (ConceptIdentifier cid : statement.getIds()) {

                // TODO support the rest
                String string = cid.getId();
                IKnowledge existing = handleConceptIdentifier(context, cid);

                if (existing == null) {

                    if (KLAB.KM.getConcept(string) != null) {
                        break;
                    }
                    String thisC = context.getNamespace().getId() + ":" + string;
                    if (KLAB.KM.getConcept(thisC) != null) {
                        break;
                    }

                    if (!string.contains(".")) {
                        string = context.getNamespace().getId() + "." + string;
                    }
                    objIds.add(string);

                } else if (existing.equals(KLAB.KM.getNothing())) {
                    this.isNothing = true;
                    return null;
                } else {
                    string = existing.toString();
                }

            }

            /*
             * only proceed with objects if ALL are potential object IDs.
             */
            if (objIds.size() == statement.getIds().size()) {
                this.objectDeclarations.addAll(objIds);
                return null;
            }

        }

        this.isNegated = statement.isNegated();

        IKnowledge main = null;
        ArrayList<IConcept> traits = new ArrayList<IConcept>();
        IKnowledge existing = null;
        for (ConceptIdentifier cid : statement.getIds()) {

            String string = cid.getId();
            existing = handleConceptIdentifier(context, cid);

            if (existing == null) {

                if (string.contains(":")) {

                    if (KLAB.KM.getConcept(string) == null
                            && (!propertyOK || KLAB.KM.getProperty(string) == null)
                            && mustExist) {
                        context.error("knowledge " + string
                                + " is unknown: new declarations are not allowed in this context", lineNumber(statement));
                    }

                    // bit of a hack here, but should work.
                    if (propertyOK && KLAB.KM.getProperty(string) != null) {
                        existing = KLAB.KM.getProperty(string);
                    }

                    if (existing == null) {
                        existing = KLAB.KM.getConcept(string);
                    }

                    if (existing != null) {
                        context.getNamespace().checkImported(context, existing, lineNumber(statement));
                    }

                } else {

                    if (propertyOK && KLAB.KM.getProperty(string) != null) {
                        existing = namespace.getOntology().getProperty(string);
                    }

                    if (existing == null) {

                        existing = ((Ontology) namespace.getOntology()).createConcept(string);

                        /*
                         * New local knowledge is child of any model where it's been generated. Good for display
                         * and debugging. Does not happen in other knowledgeobjects.
                         */
                        IModelObject model = context.get(IModel.class);
                        if (model != null) {
                            ((KIMModelObject) model).children.add(this);
                            this.parent = model;
                        }
                    }

                    if (namespace.getOntology().getConcept(string) == null
                            && (!propertyOK || namespace.getOntology().getProperty(string) == null)
                            && mustExist) {
                        context.error("knowledge " + string
                                + " is unknown: new declarations are not allowed in this context", lineNumber(statement));
                    }

                    if (!(existing instanceof IProperty) && Character.isLowerCase(string.charAt(0))
                            && !propertyOK) {
                        context.error("concept names start with an uppercase letter: "
                                + string, lineNumber(statement));
                    }
                }
            } else if (existing.equals(KLAB.KM.getNothing())) {
                isNothing = true;
                return null;
            }

            /*
             * if it's a trait (alone), add it;
             */
            if (existing != null) {

                if (NS.isTrait(existing, false) && !NS.isObservable(existing)) {
                    traits.add((IConcept) existing);
                } else {
                    if (main != null && !main.equals(existing)) {
                        context.error("both " + main + " and " + existing
                                + " are observables: only one concept can be observable, all others must be traits", lineNumber(statement));
                    }
                    main = existing;
                }
            }
        }

        boolean traitOnly = false;
        if (!mustBeObservable && main == null) {
            if (statement.getIds().size() > 1) {

                /*
                 * compose multiple traits into one; use the last specified as root.
                 */
                Set<IConcept> trs = new HashSet<>();
                for (int i = 0; i < traits.size() - 1; i++) {
                    trs.add(traits.get(i));
                }
                IConcept cnc;
                if (traits.size() > 0) {
                    try {
                        cnc = NS.addTraits(traits.get(traits.size() - 1), trs);
                        main = KLAB.KM.getConcept(cnc.toString());
                    } catch (ThinklabValidationException e) {
                        context.error(e.getMessage(), lineNumber(statement));
                    }
                }

            } else {
                main = existing;
            }
            traitOnly = true;
        }

        if (main == null && rootConcept == null) {

            if (traits.size() == 1) {
                main = KLAB.c(traits.get(0).toString());
            }

            if (main == null && propertyOK && traits.size() > 0) {
                main = KLAB.p(traits.get(0).toString());
            }
            if (main == null) {
                context.error("no valid observable: at least one observable concept is mandatory", lineNumber(statement));
            }
        } else if (traits.size() > 0 && !traitOnly) {
            try {
                main = NS.addTraits(main == null ? (IKnowledge) rootConcept : main, traits);
            } catch (ThinklabValidationException e) {
                context.error(e.getMessage(), lineNumber(statement));
            }
        }

        return main;
    }

    /*
     * Find the knowledge corresponding to this id, which must be known.
     * reminds of something
     */
    public static IConcept getKnownConcept(KIMContext context, String id, int line) {

        IConcept ret = null;
//        boolean isLocal = id.contains(":");

        if (!id.contains(":")) {
            id = context.getNamespace().getId() + ":" + id;
        }

        if (KLAB.KM.getProperty(id) != null) {
            context.error("knowledge " + id
                    + " is a property: properties are not allowed in this context", line);
        }

        // if (KLAB.KM.getConcept(id) == null) {
        // context.error("knowledge " + id
        // + " is unknown: new declarations are not allowed in this context", line);
        // }

        ret = KLAB.KM.getConcept(id);
        // if (!isLocal && ret != null) {
        // context.getNamespace().checkImported(context, ret, line);
        // }

        return ret;
    }

    public List<String> getObjectDeclarations() {
        return objectDeclarations;
    }

    /**
     * This will handle all the syntax for concept identification, returning null only when no special
     * identifiers have been given (in which case the default ID treatment should take over). Any concept
     * identification must refer to compatible and existing concepts.
     * 
     * Returns Owl:Nothing in error.
     * 
     * @param context
     * @param cid
     * @return
     */
    public static IKnowledge handleConceptIdentifier(KIMContext context, ConceptIdentifier cid) {

        IConcept ret = null;
        IConcept other = null;

        if (cid.isCount()) {

            if ((ret = getKnownConcept(context, cid.getId(), lineNumber(cid))) == null) {
                return KLAB.KM.getNothing();
            }

            if (ret.is(KLAB.c(NS.CORE_COUNT))) {
                context.error(cid.getId()
                        + " is already a count: you cannot declare a count of a count", lineNumber(cid));
                return KLAB.KM.getNothing();
            }

            if (!NS.isCountable(ret)) {
                context.error(cid.getId()
                        + " is not a countable concept: you cannot count this", lineNumber(cid));
                return KLAB.KM.getNothing();
            }

            ret = NS.makeCount(ret);

        } else if (cid.isDistance()) {

            if ((ret = getKnownConcept(context, cid.getId(), lineNumber(cid))) == null) {
                return KLAB.KM.getNothing();
            }

            if (ret.is(KLAB.c(NS.CORE_DISTANCE))) {
                context.error(cid.getId()
                        + " is already a distance: you cannot declare the distance to a distance", lineNumber(cid));
                return KLAB.KM.getNothing();
            }

            if (!NS.isObject(ret)) {
                context.error(cid.getId()
                        + " is not an object: you can only observe the distance to an object", lineNumber(cid));
                return KLAB.KM.getNothing();
            }

            ret = NS.makeDistance(ret);

        } else if (cid.isPresence()) {

            if ((ret = getKnownConcept(context, cid.getId(), lineNumber(cid))) == null) {
                return KLAB.KM.getNothing();
            }

            if (ret.is(KLAB.c(NS.CORE_PRESENCE))) {
                context.error(cid.getId()
                        + " is already a presence: you cannot declare the presence of a presence", lineNumber(cid));
                return KLAB.KM.getNothing();
            }

            if (!NS.isObject(ret)) {
                context.error(cid.getId()
                        + " is not an object: you can only observe the presence of an object or process", lineNumber(cid));
                return KLAB.KM.getNothing();
            }

            ret = NS.makePresence(ret);

        } else if (cid.isProbability()) {

            if ((ret = getKnownConcept(context, cid.getId(), lineNumber(cid))) == null) {
                return KLAB.KM.getNothing();
            }

            if (ret.is(KLAB.c(NS.CORE_PROBABILITY))) {
                context.error(cid.getId()
                        + " is already a probability: you cannot declare the probability of a probability", lineNumber(cid));
                return KLAB.KM.getNothing();
            }

            if (!NS.isEvent(ret)) {
                context.error(cid.getId()
                        + " is not an event: you can only observe the probability of an event", lineNumber(cid));
                return KLAB.KM.getNothing();
            }

            ret = NS.makeProbability(ret);

        } else if (cid.isProportion()) {

            if (cid.getId2() != null) {
                if ((other = getKnownConcept(context, cid.getId2(), lineNumber(cid))) == null) {
                    return KLAB.KM.getNothing();
                }
            }

            if ((ret = getKnownConcept(context, cid.getId(), lineNumber(cid))) == null) {
                return KLAB.KM.getNothing();
            }

            if (ret.is(KLAB.c(NS.CORE_PROPORTION))) {
                context.error(cid.getId()
                        + " is already a proportion: you cannot declare the proportion of a proportion", lineNumber(cid));
                return KLAB.KM.getNothing();
            }

            if (!(NS.isQuality(ret) || NS.isTrait(ret)) && !NS.isQuality(other)) {
                context.error(cid.getId()
                        + " proportions are of qualities in qualities or traits in qualities", lineNumber(cid));
                return KLAB.KM.getNothing();
            }

            ret = NS.makeProportion(ret, other);

        } else if (cid.isRatio()) {

            if (cid.getId2() != null) {
                if ((other = getKnownConcept(context, cid.getId2(), lineNumber(cid))) == null) {
                    return KLAB.KM.getNothing();
                }
            }

            if ((ret = getKnownConcept(context, cid.getId(), lineNumber(cid))) == null) {
                return KLAB.KM.getNothing();
            }

            if (ret.is(KLAB.c(NS.CORE_RATIO))) {
                context.error(cid.getId()
                        + " is already a ratio: you cannot declare the ratio of a ratio", lineNumber(cid));
                return KLAB.KM.getNothing();
            }

            if (!(NS.isQuality(ret) || NS.isTrait(ret)) && !NS.isQuality(other)) {
                context.error(cid.getId()
                        + " ratios are of qualities over qualities or traits over qualities", lineNumber(cid));
                return KLAB.KM.getNothing();
            }

            ret = NS.makeRatio(ret, other);

        } else if (cid.isUncertainty()) {

            if ((ret = getKnownConcept(context, cid.getId(), lineNumber(cid))) == null) {
                return KLAB.KM.getNothing();
            }

            if (ret.is(KLAB.c(NS.CORE_UNCERTAINTY))) {
                context.error(cid.getId()
                        + " is already an uncertainty: you cannot declare the uncertainty of an uncertainty", lineNumber(cid));
                return KLAB.KM.getNothing();
            }

            // TODO check - should probably be a quality or an event, but maybe also an identity so that's
            // complicated.
            // if (!NS.isObject(ret)) {
            // context.error(cid.getId()
            // + " is not an object: you can only observe the distance to an object", lineNumber(cid));
            // return KLAB.KM.getNothing();
            // }

            ret = NS.makeUncertainty(ret);

        } else if (cid.isValue()) {

            if (cid.getId2() != null) {
                if ((other = getKnownConcept(context, cid.getId2(), lineNumber(cid))) == null) {
                    return KLAB.KM.getNothing();
                }
            }

            if ((ret = getKnownConcept(context, cid.getId(), lineNumber(cid))) == null) {
                return KLAB.KM.getNothing();
            }

            if (ret.is(KLAB.c(NS.CORE_VALUE))) {
                context.error(cid.getId()
                        + " is already a value: you cannot declare the value of a value", lineNumber(cid));
                return KLAB.KM.getNothing();
            }

            // if (!(NS.isQuality(ret) || NS.isTrait(ret)) && !NS.isQuality(other)) {
            // context.error(cid.getId()
            // + " ratios are of qualities over qualities or traits over qualities", lineNumber(cid));
            // return KLAB.KM.getNothing();
            // }

            ret = NS.makeValue(ret, other);

        }

        return ret;
    }

    public static IKnowledge declare(KIMContext context, Collection<String> ids, int lineNumber) {

        boolean propertyOK = context.isWithin(KIMContext.PROPERTY)
                || context.isWithin(KIMContext.MODEL_OBSERVABLE); // TODO link to context
        boolean mustExist = !context.isWithin(KIMContext.CHILD_LIST); // TODO link to context
        boolean mustBeObservable = context.isWithin(KIMContext.OBSERVER)
                || context.isWithin(KIMContext.MODEL); // TODO link to context
        IConcept rootConcept = null; // TODO link to context

        if (ids == null || ids.isEmpty()) {
            return null;
        }

        IKnowledge main = null;
        ArrayList<IConcept> traits = new ArrayList<IConcept>();
        IKnowledge existing = null;
        for (String string : ids) {

            existing = null;

            if (string.contains(":")) {

                if (KLAB.KM.getConcept(string) == null && (!propertyOK || KLAB.KM.getProperty(string) == null)
                        && mustExist) {
                    context.error("knowledge " + string
                            + " is unknown: new declarations are not allowed in this context", lineNumber);
                }

                // bit of a hack here, but should work.
                if (propertyOK && KLAB.KM.getProperty(string) != null) {
                    existing = KLAB.KM.getProperty(string);
                }

                if (existing == null) {
                    existing = KLAB.KM.getConcept(string);
                }

                if (existing != null) {
                    context.getNamespace().checkImported(context, existing, lineNumber);
                }

            } else {

                if (propertyOK && KLAB.KM.getProperty(string) != null) {
                    existing = context.getNamespace().getOntology().getProperty(string);
                }

                if (existing == null) {
                    existing = ((Ontology) context.getNamespace().getOntology()).createConcept(string);
                }

                if (context.getNamespace().getOntology().getConcept(string) == null
                        && (!propertyOK || context.getNamespace().getOntology().getProperty(string) == null)
                        && mustExist) {
                    context.error("knowledge " + string
                            + " is unknown: new declarations are not allowed in this context", lineNumber);
                }

                if (!(existing instanceof IProperty) && Character.isLowerCase(string.charAt(0))
                        && !propertyOK) {
                    context.error("concept names start with an uppercase letter: "
                            + string, lineNumber);
                }
            }

            /*
             * if it's a trait (alone), add it;
             */
            if (existing != null) {

                if (NS.isTrait(existing, false) && !NS.isObservable(existing)) {
                    traits.add((IConcept) existing);
                } else {
                    if (main != null && !main.equals(existing)) {
                        context.error("both " + main + " and " + existing
                                + " are observables: only one concept can be observable, all others must be traits", lineNumber);
                    }
                    main = existing;
                }
            }
        }

        boolean traitOnly = false;
        if (!mustBeObservable && main == null) {
            if (ids.size() > 1) {

                /*
                 * compose multiple traits into one; use the last specified as root.
                 */
                Set<IConcept> trs = new HashSet<>();
                for (int i = 0; i < traits.size() - 1; i++) {
                    trs.add(traits.get(i));
                }
                IConcept cnc;
                if (traits.size() > 0) {
                    try {
                        cnc = NS.addTraits(traits.get(traits.size() - 1), trs);
                        main = KLAB.KM.getConcept(cnc.toString());
                    } catch (ThinklabValidationException e) {
                        context.error(e.getMessage(), lineNumber);
                    }
                }

            } else {
                main = existing;
            }
            traitOnly = true;
        }

        if (main == null && rootConcept == null) {

            if (traits.size() == 1) {
                main = KLAB.c(traits.get(0).toString());
            }

            if (propertyOK && traits.size() > 0) {
                main = KLAB.p(traits.get(0).toString());
            }
            if (main == null) {
                context.error("no valid observable: at least one observable concept is mandatory", lineNumber);
            }
        } else if (traits.size() > 0 && !traitOnly) {
            try {
                main = NS.addTraits(main == null ? (IKnowledge) rootConcept : main, traits);
            } catch (ThinklabValidationException e) {
                context.error(e.getMessage(), lineNumber);
            }
        }

        return main;
    }

    @Override
    public boolean isConcept() {
        return this.knowledge instanceof IConcept;
    }

    @Override
    public boolean isProperty() {
        return this.knowledge instanceof IProperty;
    }

    @Override
    public boolean isNegated() {
        return isNegated;
    }

    @Override
    public String getId() {
        return knowledge == null ? null : knowledge.getLocalName();
    }

    @Override
    public String getName() {
        return knowledge == null ? null : knowledge.toString();
    }

    @Override
    public IKnowledge getKnowledge() {
        return knowledge;
    }

    @Override
    public IConcept getConcept() {
        return knowledge instanceof IConcept ? (IConcept) knowledge : null;
    }

    @Override
    public IProperty getProperty() {
        return knowledge instanceof IProperty ? (IProperty) knowledge : null;
    }

    @Override
    public IKnowledge getInherentType() {
        return inherentType;
    }

    @Override
    public IKnowledge getContextType() {
        return contextType;
    }

    @Override
    public boolean isNothing() {
        return knowledge == null;
    }

    @Override
    public String toString() {
        return "K/" + (knowledge == null ? "(nothing)" : knowledge.toString());
    }

    @Override
    public IKnowledge getType() {
        return getKnowledge();
    }

    @Override
    public IModel getModel() {
        return model;
    }

    @Override
    public IDirectObserver getDirectObserver() {
        return directObserver;
    }

    @Override
    public int getDetailLevel() {
        return detailLevel;
    }
}
