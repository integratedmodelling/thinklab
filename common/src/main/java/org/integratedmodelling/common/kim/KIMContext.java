/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.common.kim;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.integratedmodelling.api.errormanagement.ICompileError;
import org.integratedmodelling.api.errormanagement.ICompileInfo;
import org.integratedmodelling.api.errormanagement.ICompileWarning;
import org.integratedmodelling.api.knowledge.IConcept;
import org.integratedmodelling.api.lang.IParsingContext;
import org.integratedmodelling.api.modelling.IModelObject;
import org.integratedmodelling.api.project.IProject;
import org.integratedmodelling.common.errormanagement.CompileError;
import org.integratedmodelling.common.errormanagement.CompileInfo;
import org.integratedmodelling.common.errormanagement.CompileWarning;
import org.integratedmodelling.common.utils.MiscUtilities;
import org.integratedmodelling.exceptions.ThinklabException;
import org.integratedmodelling.exceptions.ThinklabIOException;
import org.integratedmodelling.exceptions.ThinklabRuntimeException;
import org.integratedmodelling.exceptions.ThinklabValidationException;

public class KIMContext implements IParsingContext {

    public static final int ROOT                              = 0;
    public static final int NAMESPACE                         = 1;
    public static final int MODEL                             = 2;
    public static final int CONCEPT                           = 3;
    public static final int PROPERTY                          = 4;
    public static final int OBSERVATION                       = 5;
    public static final int PROJECT                           = 5;
    public static final int FUNCTIONCALL                      = 6;
    public static final int METADATA                          = 7;
    public static final int TRAIT_LIST                        = 8;
    public static final int CHILD_LIST                        = 9;
    public static final int PARENT_LIST                       = 10;
    public static final int EXPOSED_TRAIT_LIST                = 11;
    public static final int ADOPTED_EXPOSED_TRAIT_LIST        = 12;
    public static final int INHERITED_TRAIT_LIST              = 13;
    public static final int ROLE_LIST                         = 14;
    public static final int ROLE_RESTRICTED_OBSERVABLE_LIST   = 15;
    public static final int EQUIVALENT_CONCEPT_LIST           = 16;
    public static final int USES_RELATIONSHIP                 = 17;
    public static final int HAS_RELATIONSHIP                  = 18;
    public static final int CONTAINS_RELATIONSHIP             = 19;
    public static final int MODEL_OBSERVABLE                  = 20;
    public static final int ANNOTATION                        = 21;
    public static final int OBSERVER_OBSERVABLE               = 22;
    public static final int OBJECT_OBSERVABLE                 = 23;
    public static final int OBSERVER_CONDITIONAL              = 24;
    public static final int OBSERVER                          = 25;
    // the declaration inside an observable
    public static final int OBSERVABLE_DECLARATION            = 26;
    public static final int OBSERVER_CONDITIONAL_EXPRESSION   = 27;
    public static final int NUMERIC_DISCRETIZATION            = 28;
    public static final int PROPORTION_DISCRETIZATION         = 29;
    public static final int CLASSIFICATION                    = 30;
    public static final int CLASSIFIER                        = 31;
    public static final int CLASSIFIER_CONCEPT                = 32;
    // function call as main observed object in a model.
    public static final int OBSERVED_FUNCTIONCALL             = 33;
    public static final int DEPENDENCY                        = 34;
    public static final int UNIT                              = 35;
    public static final int DEPENDENCY_OBSERVABLE             = 36;
    public static final int COMPARISON_OBSERVABLE             = 37;
    public static final int TABLE_CLASSIFIER                  = 38;
    // the observer that describes a literal state for a direct observation
    public static final int OBSERVATION_STATE_OBSERVER        = 39;
    // function call in a context transition action or contextualization
    public static final int COVERAGE_FUNCTION_CALL            = 40;
    // 'using' accessor call
    public static final int ACTUATOR_FUNCTION_CALL            = 41;
    // generic literal value or generation function
    public static final int VALUE                             = 42;
    public static final int LIST_LITERAL                      = 43;
    public static final int CONTEXTUALIZATION_ACTION          = 44;
    public static final int REIFICATION_FUNCTION_CALL         = 45;
    public static final int DISTRIBUTION_DEPENDENCY_CONDITION = 46;
    public static final int LOOKUP_TABLE                      = 47;
    public static final int OBSERVER_ATTRIBUTE                = 48;
    public static final int PROPERTY_DOMAIN                   = 49;
    public static final int PROPERTY_RANGE                    = 50;
    public static final int TRAIT_DESCRIBED_QUALITY           = 51;
    public static final int CONFERRED_TRAIT_LIST              = 52;
    public static final int AFFECTED_QUALITIES_LIST           = 53;
    public static final int TARGET_TRAIT_LIST                 = 54;
    public static final int CURRENCY                          = 55;
    public static final int CURRENCY_CONCEPT                  = 56;
    public static final int ROLE_TARGET_OBSERVABLE_LIST       = 57;
    public static final int CONTEXTUALIZED_CONCEPT            = 58;
    public static final int CONTEXTUALIZED_ROLE               = 59;
    public static final int CONTEXTUALIZED_RESOLUTION         = 60;

    private KIMContext            parent      = null;
    private int                   type        = ROOT;
    private List<ICompileError>   errors;
    private List<ICompileInfo>    info;
    private List<ICompileWarning> warnings;
    private IProject              project;
    private KIMNamespace          namespace;
    private KIMModelObject        object;
    private File                  resource;
    private String                namespaceId;
    private boolean               mustClose;
    private InputStream           input;
    private long                  timestamp;
    private IConcept              coreConcept;
    private Set<String>           beingParsed = new HashSet<>();

    boolean firstClass;

    public KIMContext() {
        this.errors = new ArrayList<>();
        this.info = new ArrayList<>();
        this.warnings = new ArrayList<>();
    }

    protected KIMContext newInstance() {
        return new KIMContext();
    }

    /*
     * create a copy of the same type with us as parents. Redefine if necessary to transmit
     * more objects.
     */
    protected KIMContext getChild() {

        try {
            KIMContext ret = newInstance();

            ret.parent = this;
            ret.errors = this.errors;
            ret.warnings = this.warnings;
            ret.info = this.info;
            ret.namespace = this.namespace;
            ret.project = this.project;
            ret.namespaceId = this.namespaceId;
            ret.resource = this.resource;
            // object = kimContext.object;
            ret.timestamp = this.timestamp;
            ret.beingParsed = this.beingParsed;
            /*
             * don't transmit the inputstream
             */
            return ret;

        } catch (Throwable t) {
            throw new ThinklabRuntimeException(t);
        }
    }

    @Override
    public KIMContext get(IProject project) {
        KIMContext ret = getChild();
        ret.namespace = null;
        ret.namespaceId = null;
        ret.resource = null;
        ret.project = project;
        ret.type = PROJECT;
        ret.errors = new ArrayList<>();
        ret.warnings = new ArrayList<>();
        ret.info = new ArrayList<>();
        return ret;
    }

    @Override
    public KIMContext get(int type) {
        KIMContext ret = getChild();
        ret.type = type;
        return ret;
    }

    @Override
    public long getTimestamp() {
        return timestamp;
    }

    @Override
    public KIMNamespace getNamespace() {
        return namespace != null ? namespace : (parent == null ? null : parent.getNamespace());
    }

    @Override
    public IProject getProject() {
        return project != null ? project : (parent == null ? null : parent.getProject());
    }

    public IModelObject get(Class<? extends IModelObject> cls) {
        return (object != null && cls.isAssignableFrom(object.getClass())) ? object
                : (parent == null ? null : parent.get(cls));
    }

    /**
     * Find the innermost concept statement and return the core concept that has been
     * declared for it.
     * 
     * @return
     */
    public IConcept getCoreConcept() {
        return coreConcept != null ? coreConcept : (parent == null ? null : parent.getCoreConcept());
    }

    @Override
    public boolean isWithin(int type) {
        return this.type == type || (parent != null && parent.isWithin(type));
    }

    @Override
    public void error(String message, int line) {

        KIMModelObject obj = getFirstClassObject();
        if (obj != null) {
            obj.errorCount++;
        }
        ICompileError error = new CompileError(namespaceId, message, line);
        if (getNamespace() != null) {
            namespace.errors.add(error);
        }
        errors.add(error);
        onError(error);
    }

    protected void onError(ICompileError error) {
        System.err.println("" + error);
    }

    protected void onWarning(ICompileWarning error) {
        System.err.println("" + error);
    }

    protected void onInfo(ICompileInfo error) {
        // System.err.println("" + error);
    }

    private KIMModelObject getFirstClassObject() {
        return firstClass ? object : (parent == null ? null : parent.getFirstClassObject());
    }

    @Override
    public void info(String message, int line) {
        ICompileInfo info = new CompileInfo(namespaceId, message, line);
        if (getNamespace() != null) {
            namespace.info.add(info);
        }
        this.info.add(info);
        onInfo(info);
    }

    @Override
    public void warning(String message, int line) {

        KIMModelObject obj = getFirstClassObject();
        if (obj != null) {
            obj.warningCount++;
        }
        ICompileWarning error = new CompileWarning(namespaceId, message, line);
        if (getNamespace() != null) {
            namespace.warnings.add(error);
        }
        warnings.add(error);
        onWarning(error);
    }

    @Override
    public boolean equals(Object o) {
        return o instanceof KIMContext && ((KIMContext) o).type == type;
    }

    @Override
    public int hashCode() {
        return type;
    }

    /**
     * Return the ID attributed to the closest named context.
     * 
     * @return
     */
    public String getId() {
        return namespaceId;
    }

    public void accept(Object kimObject) {
        if (kimObject instanceof KIMNamespace) {
            this.namespace = (KIMNamespace) kimObject;
            beingParsed.add(namespaceId);
        } /*else if (kimObject instanceof KIMModel) {
            this.model = (KIMModel) kimObject;
          } */else if (kimObject instanceof KIMModelObject) {
            this.object = (KIMModelObject) kimObject;
        }
    }

    @Override
    public boolean hasSeen(String namespaceId) {
        return beingParsed.contains(namespaceId);
    }

    @Override
    public KIMContext forNamespace(String namespaceId) {
        KIMContext ret = getChild();
        ret.type = NAMESPACE;
        ret.namespaceId = namespaceId;
        return ret;
    }

    /**
     * Given an object passed to parse(), set up the environment for parsing the namespace identified
     * by the object, setting the namespaceId, the resource, and the inputStream field to valid values. 
     * If resource cannot be opened, just leave everything null in the returned context.
     * 
     * @param resource
     * @return
     * @throws ThinklabException 
     */
    @Override
    public KIMContext forResource(Object resource) throws ThinklabException {

        KIMContext ret = getChild();

        if (resource instanceof InputStream) {
            if (getId() == null) {
                throw new ThinklabValidationException("cannot parse a KIM input stream in a context that does not define a namespace ID");
            }
            ret.input = (InputStream) resource;
        } else {
            File file = null;
            if (resource instanceof File) {
                if (ret.namespaceId == null) {
                    ret.namespaceId = MiscUtilities.getFileBaseName(resource.toString());
                }
                file = (File) resource;

            } else if (resource instanceof URL) {
                if (ret.namespaceId == null) {
                    ret.namespaceId = MiscUtilities.getFileBaseName(((URL) resource).getPath());
                }
                try {
                    file = File.createTempFile(namespaceId, KIM.FILE_EXTENSION);
                } catch (IOException e) {
                    throw new ThinklabIOException(e);
                }
            } else if (resource instanceof String) {

                /*
                 * if we have a project, look it up in there first
                 */
                IProject p = getProject();
                if (p != null) {
                    file = p.findResourceForNamespace(resource.toString());
                }
            }

            if (file != null) {
                ret.resource = file;
                ret.timestamp = file.lastModified();
                try {
                    ret.input = new FileInputStream(file);
                    ret.mustClose = true;
                } catch (FileNotFoundException e) {
                    throw new ThinklabIOException(e);
                }
            }
        }

        return ret;
    }

    public InputStream getInputStream() {
        return input;
    }

    public void close() throws ThinklabException {
        if (mustClose) {
            try {
                input.close();
            } catch (Exception e) {
                throw new ThinklabIOException(e);
            }
        }
    }

    public static KIMContext forProject(IProject project) {
        KIMContext ret = new KIMContext();
        ret.project = project;
        ret.type = PROJECT;
        return ret;
    }

    public File getResource() {
        return resource;
    }

    public void synchronize() {
        KIMNamespace ns = getNamespace();
        if (ns != null) {
            try {
                ns.synchronizeKnowledge();
            } catch (Throwable e) {
                error("internal error: " + e.getMessage(), this.object == null ? 0
                        : this.object.getFirstLineNumber());
            }
        }
    }

    public void setCoreConcept(IConcept c) {
        coreConcept = c;
    }

    public KIMContext firstClass() {
        firstClass = true;
        return this;
    }

    public boolean is(int type) {
        return this.type == type;
    }

    public boolean hasErrors() {
        return errors.size() > 0;
    }

}
