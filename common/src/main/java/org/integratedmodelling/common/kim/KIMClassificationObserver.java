/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.common.kim;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.integratedmodelling.api.knowledge.IConcept;
import org.integratedmodelling.api.knowledge.IKnowledge;
import org.integratedmodelling.api.modelling.IAction;
import org.integratedmodelling.api.modelling.IClassification;
import org.integratedmodelling.api.modelling.IClassifyingObserver;
import org.integratedmodelling.api.modelling.IKnowledgeObject;
import org.integratedmodelling.api.modelling.IObservable;
import org.integratedmodelling.api.modelling.IObserver;
import org.integratedmodelling.api.modelling.runtime.IActuator;
import org.integratedmodelling.api.modelling.runtime.IStateActuator;
import org.integratedmodelling.api.monitoring.IMonitor;
import org.integratedmodelling.common.classification.Classification;
import org.integratedmodelling.common.configuration.KLAB;
import org.integratedmodelling.common.model.actuators.MediatingActuator;
import org.integratedmodelling.common.utils.MapUtils;
import org.integratedmodelling.common.vocabulary.NS;
import org.integratedmodelling.common.vocabulary.Observable;
import org.integratedmodelling.exceptions.ThinklabException;
import org.integratedmodelling.exceptions.ThinklabRuntimeException;
import org.integratedmodelling.exceptions.ThinklabValidationException;
import org.integratedmodelling.thinkQL.Observer;
import org.integratedmodelling.thinkQL.TraitDef;

public class KIMClassificationObserver extends KIMObserver implements IClassifyingObserver {

    IClassification classification;
    boolean         isDiscretization;
    String          metadataProperty;

    /*
     * -----------------------------------------------------------------------------------
     * accessor - it's always a mediator, either to another measurement or to a datasource
     * whose content was defined explicitly to conform to our semantics
     * -----------------------------------------------------------------------------------
     */
    public class ClassificationActuator extends MediatingActuator {

        int                     convertToTrait = -1;
        int                     maxLevel       = -1;
        IClassification         classif        = null;
        IKnowledge              baseClass      = null;
        IConcept                baseTrait      = null;
        Map<IConcept, IConcept> traitCache     = new HashMap<>();
        Map<IConcept, IConcept> levelCache     = new HashMap<>();

        public ClassificationActuator(List<IAction> actions, IMonitor monitor) {
            super(actions, monitor, false);
            classif = classification;
            if (classif != null) {
                ((org.integratedmodelling.common.classification.Classification) classification)
                        .reset();
            }
        }

        /**
         * Called for 
         * @param actions
         * @param observer
         * @param monitor
         * @throws ThinklabValidationException 
         */
        public ClassificationActuator(List<IAction> actions, IObserver observer, IMonitor monitor)
                throws ThinklabValidationException {

            super(actions, monitor, false);
            classif = classification;

            if (classif != null) {
                ((org.integratedmodelling.common.classification.Classification) classification)
                        .reset();
            } else {
                classif = classification = new org.integratedmodelling.common.classification.Classification(getObservable());
            }

            if (!observer.getType().equals(getType())) {

                if (NS.isClass(observer)) {

                    List<IObservable> etypes = NS.getExposedTraitsForType((IConcept) observer.getType());
                    for (int i = 0; i < etypes.size(); i++) {
                        if (etypes.get(i).getType().equals(getType())) {
                            convertToTrait = i;
                            baseClass = observer.getType();
                            baseTrait = etypes.get(i).getTypeAsConcept();
                            break;
                        }
                    }
                } else {
                    throw new ThinklabValidationException("wrong operation in class mediation: conversion between incompatible types "
                            + observer.getType() + " and " + getType());
                }
            }

            if (observer.getObservable().getDetailLevel() != getObservable().getDetailLevel()
                    && getObservable().getDetailLevel() > 0) {
                maxLevel = getObservable().getDetailLevel();
            }
        }

        @Override
        public String toString() {
            return "[classif: "
                    + (classif == null ? "null" : classif.getConceptSpace()) + "]";
        }

        @Override
        public Object mediate(Object object) {

            if (object == null) {
                return null;
            }

            Object ret = null;

            if (object instanceof IConcept) {
                if (convertToTrait >= 0) {
                    ret = traitCache.get(object);
                    if (ret == null) {
                        List<IObservable> tr = NS.getAdoptedTraits((IConcept) baseClass, (IConcept) object);
                        if (tr != null) {
                            ret = tr.get(convertToTrait).getType();
                            traitCache.put((IConcept) object, (IConcept) ret);
                        }
                    }
                }

                if (ret != null && maxLevel > 0) {
                    IConcept r = levelCache.get(ret);
                    if (r == null) {
                        IConcept zc = (IConcept) ret;
                        ret = NS.getParentAtLevel(baseTrait, (IConcept) ret, maxLevel);
                        if (ret != null) {
                            levelCache.put(zc, (IConcept) ret);
                        }
                    }
                }
            }

            if (ret != null) {
                return ret;
            }

            return classif.classify(object);
        }
    }

    public KIMClassificationObserver(KIMContext context, KIMModel model, Observer statement) {

        super(context, model, statement);

        if (statement.getTraits() != null && statement.getTraits().size() > 0) {
            TraitDef trait = statement.getTraits().get(0);

            /*
             * TODO support more traits and full authority defs
             */
            IKnowledgeObject oo = new KIMKnowledge(context.get(KIMContext.CONCEPT), trait
                    .getId(), false, lineNumber(trait));
            IConcept traitType = oo.getConcept();
            if (traitType != null) {
                int level = -1;
                if (trait.getDownTo() != null) {
                    level = NS.getDetailLevel(traitType, trait.getDownTo());
                }
                if (this.observable != null) {
                    ((Observable) this.observable).setTraitType(traitType, level);
                }
            }
        }

        if (statement.getClassification() != null) {
            classification = new KIMClassification(context.get(KIMContext.CLASSIFICATION), statement
                    .getClassification(), this.observable);
        }

        isDiscretization = statement.isDiscretizer();
        metadataProperty = statement.getMetadataProperty();
        if (observable != null) {
            ((Observable) observable)
                    .setType(this.getObservedType(context, this.observable.getTypeAsConcept()));
            if (metadataProperty != null) {
                classification = NS.createClassificationFromMetadata(getObservable(), metadataProperty);
            }
        }
    }

    public KIMClassificationObserver(IObserver observer) {
        super(observer);
        if (!(observer instanceof IClassifyingObserver)) {
            throw new ThinklabRuntimeException("cannot initialize a classification observer from a "
                    + observer.getClass().getCanonicalName());
        }
        this.classification = ((IClassifyingObserver) observer).getClassification();
    }

    @Override
    public IObserver copy() {
        return new KIMClassificationObserver(this);
    }

    @Override
    public IClassification getClassification() {
        return classification;
    }

    @Override
    public IObserver getMediatedObserver() {
        return mediated;
    }

    @Override
    public IConcept getObservationType() {

        /*
         * TODO take the Classification type and restrict it to 
         * represent the particular concept space or mediator we're handling.
         */
        return KLAB.c(NS.CLASSIFICATION);
    }

    @Override
    public IConcept getObservedType(KIMContext context, IConcept concept) {
        return concept;// NS.makeClassification(concept);
    }

    /*
    * for remote serialization. Silly and insufficient for any other use than transmitting
    * info.
    */
    public KIMClassificationObserver(String string) {
        String[] defs = string.split("\\|");
        classification = new Classification(defs[1]);
    }

    public KIMClassificationObserver(Map<?, ?> map) {
        // mah
        classification = map.get("classification") instanceof IClassification
                ? (IClassification) map.get("classification")
                : new Classification((Map<?, ?>) map.get("classification"));
        isDiscretization = map.get("discretization?").equals("true");
    }

    KIMClassificationObserver(IObservable observable) {

        if (!NS.isClass(observable) && !NS.isTrait(observable)) {
            throw new ThinklabRuntimeException("cannot create a classification observer from non-class knowledge");
        }
        this.observable = observable;
        classification = new Classification(observable.getTypeAsConcept(), NS
                .getConcreteChildrenAtLevel(observable.getTypeAsConcept(), observable.getDetailLevel()));
    }

    public static String asString(IClassifyingObserver cl) {
        return "oCL" + "|" + Classification.asString(cl.getClassification());
    }

    @Override
    public IStateActuator getMediator(IObserver observer, IMonitor monitor) throws ThinklabException {

        IActuator ret = getActuator(monitor);
        if (ret != null)
            return (IStateActuator) ret;

        if (observer instanceof KIMClassificationObserver
                && observer.getActions().size() == 0
                && (classification == null || classification
                        .isIdentical(((KIMClassificationObserver) observer).classification))) {
            return null;
        }

        if (observer.getType().equals(this.getType())
                || observer.getObservable().getDetailLevel() != this.getObservable().getDetailLevel() ||
                observer.getObservable().getTraitDetailLevel() != getObservable().getTraitDetailLevel()) {
            return new ClassificationActuator(getActions(), observer, monitor);
        }

        return new ClassificationActuator(getActions(), monitor);
    }

    @Override
    public IStateActuator getInterpreter(IStateActuator accessor, IMonitor monitor) {
        if (classification == null)
            return accessor;
        return new ClassificationActuator(getActions(), monitor);
    }

    @Override
    public boolean canInterpretDirectly(IActuator actuator) {
        /*
         * classifications must always be seen through, data
         * don't come classified, unless we are an empty 'classify' statement
         * which just bridges to whatever classification it's asked to mediate.
         */
        if (actuator instanceof ClassificationActuator && classification == null)
            return true;

        return false;
    }

    @Override
    public boolean isCompatibleWith(IKnowledge type) {
        // TODO/CHECK: classifications can classify anything
        return true; // type.is(getObservable().getType());
    }

    @Override
    public Object adapt() {
        return MapUtils
                .of("discretization?", isDiscretization, "metadata-property", metadataProperty, "classification", classification);
    }

    @Override
    public String toString() {
        return "CLS/" + getObservable();
    }

}
