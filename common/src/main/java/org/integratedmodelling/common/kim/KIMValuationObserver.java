/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.common.kim;

import java.util.List;
import java.util.Map;

import org.integratedmodelling.api.knowledge.IConcept;
import org.integratedmodelling.api.knowledge.IKnowledge;
import org.integratedmodelling.api.knowledge.ISemantic;
import org.integratedmodelling.api.modelling.IAction;
import org.integratedmodelling.api.modelling.IClassification;
import org.integratedmodelling.api.modelling.ICurrency;
import org.integratedmodelling.api.modelling.IObservable;
import org.integratedmodelling.api.modelling.IObserver;
import org.integratedmodelling.api.modelling.IValuingObserver;
import org.integratedmodelling.api.modelling.runtime.IActuator;
import org.integratedmodelling.api.modelling.runtime.IStateActuator;
import org.integratedmodelling.api.monitoring.IMonitor;
import org.integratedmodelling.common.classification.Classification;
import org.integratedmodelling.common.configuration.KLAB;
import org.integratedmodelling.common.data.IndexedCategoricalDistribution;
import org.integratedmodelling.common.model.actuators.MediatingActuator;
import org.integratedmodelling.common.utils.MapUtils;
import org.integratedmodelling.common.vocabulary.Currency;
import org.integratedmodelling.common.vocabulary.NS;
import org.integratedmodelling.common.vocabulary.Observable;
import org.integratedmodelling.exceptions.ThinklabException;
import org.integratedmodelling.exceptions.ThinklabRuntimeException;
import org.integratedmodelling.exceptions.ThinklabValidationException;
import org.integratedmodelling.thinkQL.Observer;

public class KIMValuationObserver extends KIMNumericObserver implements IValuingObserver {

    ICurrency  currency;
    IKnowledge valuedObservable;
    IKnowledge comparisonObservable;

    public class ValueActuator extends MediatingActuator {

        IValuingObserver other;
        boolean          errorsPresent = false;
        IMonitor         monitor;

        public ValueActuator(IValuingObserver other, List<IAction> actions, IMonitor monitor,
                boolean interpreting) {
            super(actions, monitor, interpreting);
            this.other = other;
            this.monitor = monitor;
            if (discretization != null)
                ((org.integratedmodelling.common.classification.Classification) discretization)
                        .reset();

        }

        @Override
        public String toString() {
            return "[valuation: " + currency + "]";
        }

        public ICurrency getCurrency() {
            return currency;
        }

        @Override
        public Object mediate(Object object) {

            double val = Double.NaN;

            if (object == null || (object instanceof Number && Double.isNaN(((Number) object).doubleValue())))
                return val;

            if (object instanceof IndexedCategoricalDistribution) {
                val = ((IndexedCategoricalDistribution) object).getMean();
            } else if (object instanceof Number) {
                val = ((Number) object).doubleValue();
            } else {
                try {
                    val = object.toString().equals("NaN") ? Double.NaN : Double
                            .parseDouble(object.toString());
                } catch (Exception e) {
                    if (!errorsPresent) {
                        monitor.error("cannot interpret value: " + object);
                        errorsPresent = true;
                    }
                    return Double.NaN;
                }
            }

            return currency.convert(val, other.getCurrency());
        }
    }

    public KIMValuationObserver(KIMContext context, KIMModel model, Observer statement) {
        super(context, model, statement);

        double from = Double.NaN, to = Double.NaN;

        try {
            if (observable != null) {
                ((Observable) observable)
                        .setType(this.getObservedType(context, this.observable.getTypeAsConcept()));
            }
        } catch (ThinklabRuntimeException e) {
            context.error(e.getMessage(), getFirstLineNumber());
        }

        if (statement.getFrom() != null) {
            from = KIM.processNumber(statement.getFrom()).doubleValue();
            to = KIM.processNumber(statement.getTo()).doubleValue();
        }

        if (statement.getOther() != null) {
            comparisonObservable = new KIMKnowledge(context.get(KIMContext.COMPARISON_OBSERVABLE), statement
                    .getOther(), this).getConcept();
        }

        if (statement.getCurrency() != null || statement.getFrom() != null) {
            this.currency = new KIMCurrency(context.get(KIMContext.CURRENCY), statement.getCurrency(), from, to);
        } else {
            context.error("either a currency ('in ....') or a range of values must be specified", getFirstLineNumber());
        }
    }

    public KIMValuationObserver(IObserver observer) {
        super(observer);
        if (!(observer instanceof IValuingObserver)) {
            throw new ThinklabRuntimeException("cannot initialize a valuing observer from a "
                    + observer.getClass().getCanonicalName());
        }
        this.currency = ((IValuingObserver) observer).getCurrency();
    }

    public KIMValuationObserver(String string, IObservable obs) {
        String[] defs = string.split("\\|");
        currency = new Currency(defs[1]);
        if (defs.length > 2) {
            discretization = new org.integratedmodelling.common.classification.Classification(defs[2]);
        }
        observable = obs;
    }

    public KIMValuationObserver(Map<?, ?> map) {
        if (map.containsKey("discretization")) {
            discretization = map.get("discretization") instanceof IClassification
                    ? (IClassification) map.get("discretization")
                    : new Classification((Map<?, ?>) map.get("discretization"));
        }
        // FIXME need to use proper json.
        // currency = new Currency(map.get("currency").toString());
    }

    @Override
    public IObserver copy() {
        return new KIMValuationObserver(this);
    }

    KIMValuationObserver(ISemantic observable, ICurrency currency2) {
        // TODO Auto-generated constructor stub
    }

    @Override
    public ICurrency getCurrency() {
        return currency;
    }

    @Override
    public IObserver getMediatedObserver() {
        // TODO Auto-generated method stub
        return mediated;
    }

    @Override
    public IStateActuator getMediator(IObserver observer, IMonitor monitor) throws ThinklabException {

        observer = getRepresentativeObserver(observer);

        IActuator ret = getActuator(monitor);
        if (ret != null)
            return (IStateActuator) ret;

        if (!(observer instanceof IValuingObserver))
            throw new ThinklabValidationException("valuations can only mediate other valuations");

        if (getActions().size() == 0 && ((IValuingObserver) observer).getCurrency().equals(getCurrency())) {
            return null;
        }

        return new ValueActuator((IValuingObserver) observer, getActions(), monitor, false);
    }

    @Override
    public IStateActuator getInterpreter(IStateActuator accessor, IMonitor monitor) {
        return new ValueActuator(this, getActions(), monitor, true);
    }

    @Override
    public boolean canInterpretDirectly(IActuator actuator) {
        return getActions().size() == 0 && discretization == null;
    }

    @Override
    public IConcept getObservationType() {
        return KLAB.c(NS.VALUE_OBSERVATION);
    }

    @Override
    public IKnowledge getValuedObservable() {
        return valuedObservable;
    }

    @Override
    public IKnowledge getComparisonObservable() {
        return comparisonObservable;
    }

    @Override
    public boolean isPairwise() {
        return comparisonObservable != null;
    }

    @Override
    protected IConcept getObservedType(KIMContext context, IConcept knowledge) {
        valuedObservable = knowledge;
        return NS.makeValue(knowledge, comparisonObservable);
    }

    public static String asString(IValuingObserver observer) {
        String ret = "oVL|" + observer.getCurrency();
        if (observer.getDiscretization() != null) {
            ret += "|"
                    + org.integratedmodelling.common.classification.Classification.asString(observer
                            .getDiscretization());
        }
        return ret;
    }

    @Override
    public Object adapt() {
        return MapUtils.of("currency", currency.toString(), "discretization", discretization);
    }

    @Override
    public String toString() {
        return "VAL/" + getObservable() + " (" + currency + ")";
    }

}
