/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.common.kim;

import java.lang.reflect.Constructor;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.integratedmodelling.api.factories.IModelFactory;
import org.integratedmodelling.api.knowledge.ICodeExpression;
import org.integratedmodelling.api.knowledge.IConcept;
import org.integratedmodelling.api.knowledge.IExpression;
import org.integratedmodelling.api.knowledge.IKnowledge;
import org.integratedmodelling.api.knowledge.ISemantic;
import org.integratedmodelling.api.lang.IParsingContext;
import org.integratedmodelling.api.modelling.IAction;
import org.integratedmodelling.api.modelling.IClassifyingObserver;
import org.integratedmodelling.api.modelling.ICurrency;
import org.integratedmodelling.api.modelling.IDataSource;
import org.integratedmodelling.api.modelling.IDirectActiveObservation;
import org.integratedmodelling.api.modelling.IDirectObserver;
import org.integratedmodelling.api.modelling.IDistanceObserver;
import org.integratedmodelling.api.modelling.IExtent;
import org.integratedmodelling.api.modelling.IFunctionCall;
import org.integratedmodelling.api.modelling.IMeasuringObserver;
import org.integratedmodelling.api.modelling.IModel;
import org.integratedmodelling.api.modelling.IModelBean;
import org.integratedmodelling.api.modelling.INamespace;
import org.integratedmodelling.api.modelling.IObjectSource;
import org.integratedmodelling.api.modelling.IObservable;
import org.integratedmodelling.api.modelling.IObserver;
import org.integratedmodelling.api.modelling.IObservingObject;
import org.integratedmodelling.api.modelling.IPresenceObserver;
import org.integratedmodelling.api.modelling.IProbabilityObserver;
import org.integratedmodelling.api.modelling.IProportionObserver;
import org.integratedmodelling.api.modelling.IRankingObserver;
import org.integratedmodelling.api.modelling.IRatioObserver;
import org.integratedmodelling.api.modelling.IRemoteServiceCall;
import org.integratedmodelling.api.modelling.IScale;
import org.integratedmodelling.api.modelling.IValuingObserver;
import org.integratedmodelling.api.modelling.runtime.IActuator;
import org.integratedmodelling.api.modelling.runtime.IContextualizer;
import org.integratedmodelling.api.modelling.runtime.IEventInstantiator;
import org.integratedmodelling.api.modelling.runtime.IProcessContextualizer;
import org.integratedmodelling.api.modelling.runtime.IStateContextualizer;
import org.integratedmodelling.api.modelling.runtime.ISubjectContextualizer;
import org.integratedmodelling.api.modelling.runtime.ISubjectInstantiator;
import org.integratedmodelling.api.modelling.scheduling.ITransition;
import org.integratedmodelling.api.modelling.storage.IDataset;
import org.integratedmodelling.api.modelling.storage.IStorage;
import org.integratedmodelling.api.monitoring.IKnowledgeLifecycleListener;
import org.integratedmodelling.api.monitoring.IMonitor;
import org.integratedmodelling.api.monitoring.IMonitorable;
import org.integratedmodelling.api.network.INetworkSerializable;
import org.integratedmodelling.api.services.IPrototype;
import org.integratedmodelling.api.space.IShape;
import org.integratedmodelling.api.space.ISpatialExtent;
import org.integratedmodelling.api.space.ISpatialIndex;
import org.integratedmodelling.common.command.ServiceManager;
import org.integratedmodelling.common.configuration.KLAB;
import org.integratedmodelling.common.model.actuators.Actuator;
import org.integratedmodelling.common.model.actuators.EventInstantiatorActuator;
import org.integratedmodelling.common.model.actuators.ProcessActuator;
import org.integratedmodelling.common.model.actuators.ProcessContextualizerActuator;
import org.integratedmodelling.common.model.actuators.StateContextualizerActuator;
import org.integratedmodelling.common.model.actuators.SubjectContextualizerActuator;
import org.integratedmodelling.common.model.actuators.SubjectInstantiationActuator;
import org.integratedmodelling.common.model.runtime.Scale;
import org.integratedmodelling.common.model.runtime.Space;
import org.integratedmodelling.common.model.runtime.Time;
import org.integratedmodelling.common.space.ShapeValue;
import org.integratedmodelling.common.utils.CamelCase;
import org.integratedmodelling.common.vocabulary.NS;
import org.integratedmodelling.common.vocabulary.Observable;
import org.integratedmodelling.exceptions.ThinklabException;
import org.integratedmodelling.exceptions.ThinklabRuntimeException;
import org.integratedmodelling.exceptions.ThinklabValidationException;

import com.vividsolutions.jts.geom.Geometry;

/**
 * Basic model factory, not functional for an engine but OK for a client or testing.
 * 
 * @author Ferd
 *
 */
public class ModelFactory implements IModelFactory {

    List<IKnowledgeLifecycleListener> knowledgeListeners = new ArrayList<>();

    @Override
    public IParsingContext getRootParsingContext() {
        return new KIMContext();
    }

    @Override
    public IStorage<?> createStorage(IObserver observer, IScale scale, IDataset dataset, boolean isDynamic) {
        return null;
    }

    @Override
    public IDirectActiveObservation createSubject(IDirectObserver observer, IDirectActiveObservation context, IMonitor monitor)
            throws ThinklabException {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public IScale createScale(Collection<IFunctionCall> definition) {
        try {
            List<IExtent> exts = new ArrayList<>();
            for (IFunctionCall f : definition) {
                Object o = callFunction(f, KLAB.CMANAGER.getMonitor(), null);
                if (!(o instanceof IExtent)) {
                    throw new ThinklabRuntimeException("function call " + f + " did not produce an extent");
                }
                exts.add((IExtent) o);
            }
            return Scale.create(exts.toArray(new IExtent[definition.size()]));
        } catch (ThinklabException e) {
            throw new ThinklabRuntimeException(e);
        }
    }

    public static IObserver measureObserver(ISemantic observable, String unit)
            throws ThinklabException {
        return new KIMMeasurementObserver(observable, unit);
    }

    public static IObserver countObserver(ISemantic observable, String unit)
            throws ThinklabException {
        return new KIMCountObserver(observable, unit);
    }

    public static IModel count(ISemantic observable, String unit) throws ThinklabException {
        return new KIMModel((KIMObserver) countObserver(observable, unit));
    }

    public static IObserver rankObserver(ISemantic observable) {
        return new KIMRankingObserver(observable);
    }

    public static IModel rank(ISemantic observable) {
        return new KIMModel((KIMObserver) rankObserver(observable));
    }

    public static IObserver presenceObserver(ISemantic observable) {
        IObservable obs = new Observable(NS.makePresence(observable.getType()), KLAB
                .c(NS.PRESENCE_OBSERVATION), CamelCase.toLowerCase(observable.getType().getLocalName(), '-'));
        return new KIMPresenceObserver(obs);
    }

    public static IObserver ratioObserver(ISemantic observable, ISemantic compareTo) {
        return new KIMRatioObserver(observable, compareTo);
    }

    public static IObserver valueObserver(ISemantic observable, ICurrency currency) {
        return new KIMValuationObserver(observable, currency);
    }

    public static IObserver proportionObserver(ISemantic observable, ISemantic comparison, boolean makePercentage) {
        return new KIMProportionObserver(observable, comparison, makePercentage);
    }

    // public static IObserver classifyObserver(ISemantic observable, Object... classifiers) {
    // return new KIMClassificationObserver(observable, classifiers);
    // }

    public static IModel measure(ISemantic observable, String unit)
            throws ThinklabException {
        return new KIMModel((KIMObserver) measureObserver(observable, unit));
    }

    public static IModel presence(ISemantic observable) {
        return new KIMModel((KIMObserver) presenceObserver(observable));
    }

    // public static IModel classify(ISemantic observable, Object... classifiers) {
    // return new KIMModel((KIMObserver) classifyObserver(observable, classifiers));
    // }

    public static IModel probability(ISemantic observable) {
        return new KIMModel((KIMObserver) probabilityObserver(observable));
    }

    public static IObserver probabilityObserver(ISemantic observable) {
        return new KIMProbabilityObserver(observable);
    }

    public static IModel percentage(ISemantic observable, ISemantic generic) {
        return new KIMModel((KIMObserver) proportionObserver(observable, generic, true));
    }

    public static IModel proportion(ISemantic observable, ISemantic generic) {
        return new KIMModel((KIMObserver) proportionObserver(observable, generic, false));
    }

    public static IModel ratio(ISemantic observable, ISemantic compareTo) {
        return new KIMModel((KIMObserver) ratioObserver(observable, compareTo));
    }

    public static IModel value(ISemantic observable, ICurrency currency) {
        return new KIMModel((KIMObserver) valueObserver(observable, currency));
    }

    public static IModel uncertainty(ISemantic observable) {
        return new KIMModel((KIMObserver) uncertaintyObserver(observable));
    }

    public static IObserver uncertaintyObserver(ISemantic observable) {
        return new KIMUncertaintyObserver(observable);
    }

    public static String serialize(IScale scale) {
        return Scale.asString(scale);
    }

    public static String serialize(IObservable observable) {
        return Observable.asString(observable);
    }

    public static String serialize(IObserver observer) {
        if (observer instanceof IClassifyingObserver) {
            return KIMClassificationObserver.asString((IClassifyingObserver) observer);
        } else if (observer instanceof IMeasuringObserver) {
            return KIMMeasurementObserver.asString((IMeasuringObserver) observer);
        } else if (observer instanceof IRankingObserver) {
            return KIMRankingObserver.asString((IRankingObserver) observer);
        } else if (observer instanceof IProbabilityObserver) {
            return KIMProbabilityObserver.asString((IProbabilityObserver) observer);
        } else if (observer instanceof IRatioObserver) {
            return KIMRatioObserver.asString((IRatioObserver) observer);
        } else if (observer instanceof IValuingObserver) {
            return KIMValuationObserver.asString((IValuingObserver) observer);
        } else if (observer instanceof IProportionObserver) {
            return KIMProportionObserver.asString((IProportionObserver) observer);
        } else if (observer instanceof IDistanceObserver) {
            return KIMDistanceObserver.asString((IDistanceObserver) observer);
        } else if (observer instanceof IPresenceObserver) {
            return KIMPresenceObserver.asString((IPresenceObserver) observer);
        }

        throw new ThinklabRuntimeException("internal: cannot serialize "
                + observer.getClass().getCanonicalName()
                + ": add methods to ModelObjectSerializer");
    }

    public static String serialize(ITransition transition) {
        throw new ThinklabRuntimeException("internal: cannot serialize "
                + transition.getClass().getCanonicalName()
                + ": add methods to ModelObjectSerializer");
    }

    public static IObserver deserializeObserver(String string, IObservable observable) {
        if (string.startsWith("oCL|")) {
            return new KIMClassificationObserver(string);
        } else if (string.startsWith("oMS|")) {
            return new KIMMeasurementObserver(string, observable);
        } else if (string.startsWith("oDS|")) {
            return new KIMDistanceObserver(string, observable);
        } else if (string.startsWith("oVL|")) {
            return new KIMValuationObserver(string, observable);
        } // TODO the rest
        throw new ThinklabRuntimeException("internal: cannot deserialize "
                + string
                + ": add methods to ModelObjectSerializer");
    }

    public static IObservable deserializeObservable(String string) {
        return new Observable(string);
    }

    @Override
    public void addKnowledgeLifecycleListener(IKnowledgeLifecycleListener listener) {
        knowledgeListeners.add(listener);
    }

    public static IDirectObserver createDirectObserver(IConcept concept, String name, INamespace namespace, Object... extents)
            throws ThinklabException {

        boolean isLocal = false;
        IDirectObserver ret = null;
        if (namespace == null) {
            namespace = KLAB.MMANAGER.getLocalNamespace();
            isLocal = true;
        }
        if (extents != null && extents.length == 1 && extents[0] instanceof IScale) {
            ret = new KIMDirectObserver(namespace, concept, name, (IScale) extents[0]);
        }
        ret = new KIMDirectObserver(namespace, concept, name, KLAB.MFACTORY.createScale(extents));

        if (ret != null && isLocal) {
            ((KIMNamespace) KLAB.MMANAGER.getLocalNamespace()).addModelObject(ret);
        }

        return ret;
    }

    @Override
    public Object callFunction(IFunctionCall call, IMonitor monitor, IModel model, IConcept... context)
            throws ThinklabException {

        /*
         * 0. if the call is itself an expression, just call it with its own arguments.
         */
        if (call instanceof IExpression) {
            return ((IExpression) call).eval(call.getParameters(), monitor, context);
        }

        /*
         * 1. get prototype
         */
        IPrototype p = ServiceManager.get().getFunctionPrototype(call.getId());

        if (p == null) {
            throw new ThinklabValidationException("cannot find function " + call.getId());
        }

        /*
         * 2. validate args
         */
        ServiceManager.parseCall(call.getId(), call.getParameters(), null, null);

        /*
         * create accessor. If that's for a remotely available service, see if we should use the
         * remote service instead.
         */
        Object ret = null;
        if (IContextualizer.class.isAssignableFrom(p.getExecutorClass())) {
            try {
                ret = p.getExecutorClass().newInstance();
                if (ret instanceof ICodeExpression) {
                    ((ICodeExpression) ret).setNamespace(((KIMFunctionCall) call).getNamespace());
                }
            } catch (Exception e) {
                //
            }
        }

        if (ret instanceof IContextualizer.Remote) {

            boolean useLocally = false;

            if (ServiceManager.get().isServiceAvailableRemotely(call.getId())) {
                p = ServiceManager.get().getRemoteService(call.getId());
                if (IRemoteServiceCall.class.isAssignableFrom(p.getExecutorClass())) {
                    try {
                        Constructor<?> constructor = p.getExecutorClass().getConstructor(IPrototype.class);
                        if (constructor != null) {
                            Object prev = ret;
                            if (((IContextualizer.Remote) prev).useRemote(KLAB.NETWORK.getUser())) {
                                monitor.info("using remote service for " + call.getId(), null);
                                ret = constructor.newInstance(p);
                            }
                        } else {
                            useLocally = true;
                        }
                    } catch (Exception e) {
                        // keep using the local contextualizer if allowed
                        useLocally = true;
                    }
                }
            }

            if (ret /* still*/ instanceof IContextualizer.Remote
                    && !((IContextualizer.Remote) ret).canRunLocally()) {
                monitor.error(call.getId() + " remote service not available; service cannot run locally");
                ret = null;
            }

            if (ret != null && useLocally) {
                monitor.info("using local version of remote-capable service " + call.getId(), null);
            }
        }

        /*
         * 4. return an instance of either the annotated class if an IExpression or a wrapper
         * if not.
         */
        if (ret == null) {
            if (IExpression.class.isAssignableFrom(p.getExecutorClass())) {
                try {
                    ret = p.getExecutorClass().newInstance();
                    if (ret instanceof ICodeExpression) {
                        ((ICodeExpression) ret).setNamespace(call.getNamespace());
                    }

                } catch (Exception e) {
                    //
                }
                if (ret instanceof IExpression) {
                    ret = ((IExpression) ret).eval(call.getParameters(), monitor, context);
                }
            } else if (IContextualizer.class.isAssignableFrom(p.getExecutorClass())) {
                try {
                    ret = p.getExecutorClass().newInstance();
                    if (ret instanceof ICodeExpression) {
                        ((ICodeExpression) ret).setNamespace(((KIMFunctionCall) call).getNamespace());
                    }
                } catch (Exception e) {
                    //
                }
            }
        }

        if (ret == null) {
            throw new ThinklabValidationException("error when calling function " + call.getId());
        }

        if (ret instanceof IMonitorable) {
            ((IMonitorable) ret).setMonitor(monitor);
        }

        if (ret instanceof IContextualizer) {
            ((IContextualizer) ret)
                    .setContext(call.getParameters(), model, model.getNamespace().getProject());
        }

        return ret;
    }

    @Override
    public IActuator getActuator(IFunctionCall call, IObservingObject observer, IMonitor monitor)
            throws ThinklabException {

        IActuator ret = null;
        if (call != null) {
            Object ds = callFunction(call, monitor, observer instanceof IModel ? (IModel) observer
                    : null, observer instanceof IModel
                            ? chooseDirectAccessorType(observer.getObservable().getType())
                            : KLAB.c(NS.STATE_CONTEXTUALIZER));

            if (ds instanceof IActuator) {
                ret = (IActuator) ds;
                for (IAction action : observer.getActions()) {
                    ((Actuator) ret).addAction(action);
                }
                if (ds instanceof IMonitorable) {
                    ((IMonitorable) ds).setMonitor(monitor);
                }
            } else if (ds instanceof IContextualizer) {
                if (ds instanceof IStateContextualizer) {
                    ret = new StateContextualizerActuator((IStateContextualizer) ds, observer
                            .getActions(), monitor);
                } else if (ds instanceof ISubjectContextualizer) {
                    ret = new SubjectContextualizerActuator((ISubjectContextualizer) ds, observer
                            .getActions(), monitor);
                } else if (ds instanceof IProcessContextualizer) {
                    ret = new ProcessContextualizerActuator((IProcessContextualizer) ds, observer
                            .getActions(), monitor);
                } else if (ds instanceof IEventInstantiator) {
                    ret = new EventInstantiatorActuator((IEventInstantiator) ds, observer
                            .getActions(), monitor);
                } else if (ds instanceof ISubjectInstantiator) {
                    ret = new SubjectInstantiationActuator((ISubjectInstantiator) ds, observer
                            .getActions(), monitor);
                }
            } else {
                throw new ThinklabValidationException("function " + call.getId()
                        + " does not return an accessor");
            }
        } else if (observer.getActions().size() > 0) {
            /*
             * give it a default actuator to capture the actions.
             */
            if (NS.isThing(observer.getObservable().getType())) {
                ret = null; // TODO
            } else if (NS.isProcess(observer.getObservable().getType())) {
                ret = new ProcessActuator(observer.getActions());
            } else if (NS.isEvent(observer.getObservable().getType())) {
                ret = null; // TODO
            }
        }

        if (ret instanceof ICodeExpression) {
            ((ICodeExpression) ret).setNamespace(call.getNamespace());
        }

        return ret;
    }

    private IConcept chooseDirectAccessorType(IKnowledge ob) throws ThinklabValidationException {

        if (NS.isThing(ob)) {
            return KLAB.c(NS.SUBJECT_CONTEXTUALIZER);
        } else if (NS.isProcess(ob)) {
            return KLAB.c(NS.PROCESS_CONTEXTUALIZER);
        } else if (NS.isEvent(ob)) {
            return KLAB.c(NS.EVENT_INSTANTIATOR);
        }

        throw new ThinklabValidationException("subject model has observable that doesn't match any accessor type");

    }

    @Override
    public IDataSource createDatasource(IFunctionCall call, IMonitor monitor) throws ThinklabException {
        Object ret = callFunction(call, monitor, null);
        if (!(ret instanceof IDataSource)) {
            throw new ThinklabValidationException("function " + call.getId()
                    + " did not return a datasource");
        }
        if (ret instanceof ICodeExpression) {
            ((ICodeExpression) ret).setNamespace(((KIMFunctionCall) call).getNamespace());
        }
        return (IDataSource) ret;
    }

    @Override
    public IObjectSource createObjectsource(IFunctionCall call, IMonitor monitor)
            throws ThinklabException {
        /*
         * FIXME this is called twice or more - should cache function and parameters and
         * return existing sources.
         */
        Object ret = callFunction(call, monitor, null);
        if (!(ret instanceof IObjectSource)) {
            throw new ThinklabValidationException("function " + call.getId()
                    + " did not return an object source");
        }
        if (ret instanceof ICodeExpression) {
            ((ICodeExpression) ret).setNamespace(((KIMFunctionCall) call).getNamespace());
        }
        return (IObjectSource) ret;
    }

    @Override
    public IScale createScale(Object... objects) {

        List<IExtent> extents = new ArrayList<>();

        for (Object o : objects) {
            if (o == null) {
                continue;
            }
            if (o instanceof ShapeValue) {
                extents.add(new Space(((ShapeValue) o)));
            } else if (o instanceof IShape) {
                ShapeValue shp = new ShapeValue(((IShape) o).asText());
                extents.add(new Space(shp));
            } else if (o instanceof Geometry) {
                ShapeValue shp = new ShapeValue((Geometry) o);
                extents.add(new Space(shp));
            } else if (o instanceof Long) {
                extents.add(new Time((Long) o));
            } else if (o instanceof String) {
                ShapeValue shp = new ShapeValue((String) o);
                extents.add(new Space(shp));
            } else if (o instanceof IExtent) {
                extents.add((IExtent) o);
            } else if (o instanceof IScale) {
                for (IExtent e : ((IScale) o)) {
                    extents.add(e);
                }
            } else {
                throw new ThinklabRuntimeException("internal: cannot create a scale from a " + (o == null
                        ? "null" : o.getClass().getCanonicalName()));
            }
        }

        return createScaleFromExtents(extents);
    }

    private IScale createScaleFromExtents(List<IExtent> extents) {
        return Scale.create(extents.toArray(new IExtent[extents.size()]));
    }

    @Override
    public IDataSource createConstantDataSource(Object inlineValue) {
        return null;
    }

    @Override
    public Object wrapForAction(Object object) {
        return object;
    }

    @Override
    public ISpatialIndex getSpatialIndex(ISpatialExtent space) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public IModelBean adapt(Object o) {
        
        if (o instanceof IModelBean) {
            return (IModelBean)o;
        } else if (o instanceof INetworkSerializable) {
            return ((INetworkSerializable)o).getBean();
        }
        /*
         * TODO hard cases
         */
        return null;
    }

    @Override
    public Object reconstruct(IModelBean o) {
        // TODO Auto-generated method stub
        return o;
    }

}
