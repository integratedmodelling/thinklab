/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.common.kim;

import org.eclipse.xtext.nodemodel.util.NodeModelUtils;
import org.integratedmodelling.api.modelling.IClassifier;
import org.integratedmodelling.api.modelling.ILanguageObject;
import org.integratedmodelling.common.data.LookupTable;
import org.integratedmodelling.thinkQL.Table;

public class KIMLookupTable extends LookupTable implements ILanguageObject {

    int firstLine, lastLine;

    public KIMLookupTable(KIMContext context, Table statement) {

        firstLine = NodeModelUtils.getNode(statement).getStartLine();
        lastLine = NodeModelUtils.getNode(statement).getEndLine();

        rows.clear();
        headers = statement.getArgs();
        if (headers.size() == 0 || (statement.getElements().size() % headers.size()) != 0) {
            context.error("the number of items per row is not equal to the number of headers, or headers are missing", KIMLanguageObject
                    .lineNumber(statement));
        }
        for (int i = 0; i < statement.getElements().size(); i++) {
            IClassifier[] row = new IClassifier[headers.size()];
            for (int n = 0; n < headers.size(); n++) {
                row[n] = new KIMClassifier(context.get(KIMContext.TABLE_CLASSIFIER), statement.getElements()
                        .get(i + n), null, false);
            }
            i += headers.size() - 1;
            rows.add(row);
        }
    }

    @Override
    public int getFirstLineNumber() {
        return firstLine;
    }

    @Override
    public int getLastLineNumber() {
        return lastLine;
    }

}
