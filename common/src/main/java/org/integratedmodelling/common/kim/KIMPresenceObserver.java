/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.common.kim;

import java.util.List;
import java.util.Map;

import org.integratedmodelling.api.knowledge.IConcept;
import org.integratedmodelling.api.modelling.IAction;
import org.integratedmodelling.api.modelling.IObservable;
import org.integratedmodelling.api.modelling.IObserver;
import org.integratedmodelling.api.modelling.IPresenceObserver;
import org.integratedmodelling.api.modelling.runtime.IActuator;
import org.integratedmodelling.api.modelling.runtime.IStateActuator;
import org.integratedmodelling.api.monitoring.IMonitor;
import org.integratedmodelling.common.configuration.KLAB;
import org.integratedmodelling.common.data.IndexedCategoricalDistribution;
import org.integratedmodelling.common.model.actuators.MediatingActuator;
import org.integratedmodelling.common.utils.MapUtils;
import org.integratedmodelling.common.utils.NumberUtils;
import org.integratedmodelling.common.vocabulary.NS;
import org.integratedmodelling.common.vocabulary.Observable;
import org.integratedmodelling.exceptions.ThinklabException;
import org.integratedmodelling.thinkQL.Observer;

public class KIMPresenceObserver extends KIMObserver implements IPresenceObserver {

    boolean isIndirect;

    public class PresenceActuator extends MediatingActuator {

        boolean errorsPresent = false;

        public PresenceActuator(List<IAction> actions, IMonitor monitor, boolean interpreting) {
            super(actions, monitor, interpreting);
        }

        @Override
        public String toString() {
            return "[presence " + getType() + "]";
        }

        /*
         * this turns things into booleans for observers that get their input from datasources.
         * 
         * (non-Javadoc)
         * @see org.integratedmodelling.thinklab.modelling.lang.MediatingAccessor#getValue(java.lang.String)
         */
        @Override
        public Object getValue(String outputKey) {
            Object val = super.getValue(outputKey);
            return mediate(val);
        }

        @Override
        public Object mediate(Object object) {

            Object val = null;

            // not much to say
            if (object instanceof Boolean)
                return object;

            if (object == null || (object instanceof Number && Double.isNaN(((Number) object).doubleValue())))
                return null;

            if (object instanceof IndexedCategoricalDistribution) {

                /*
                 * TODO try to mediate distributions
                 */
                return !(NumberUtils.equal(((IndexedCategoricalDistribution) object).getMean(), 0));

            } else if (object instanceof Number) {
                return !(NumberUtils.equal(((Number) object).doubleValue(), 0));
            } else {
                try {
                    val = object.toString().equals("NaN") ? Double.NaN : Double
                            .parseDouble(object.toString());
                    return !(NumberUtils.equal(((Number) val).doubleValue(), 0));
                } catch (Exception e) {
                    if (!errorsPresent) {
                        monitor.error("cannot interpret value: " + object + " as a number");
                        errorsPresent = true;
                    }
                    return null;
                }
            }
        }
    }

    public KIMPresenceObserver(KIMContext context, KIMModel model, Observer statement) {
        super(context, model, statement);
        isIndirect = statement.isDerived();
        if (observable != null) {
            ((Observable) observable)
                    .setType(this.getObservedType(context, this.observable.getTypeAsConcept()));
        }
    }

    public KIMPresenceObserver(Map<?, ?> map) {
        isIndirect = map.get("indirect?").equals("true");
    }

    public KIMPresenceObserver(IObservable observable) {
        IConcept oob = NS.makePresence(observable.getType());
        this.observable = new Observable(oob, KLAB.c(NS.PRESENCE_OBSERVATION), observable.getFormalName());
    }

    public KIMPresenceObserver(IObserver observer) {
        super(observer);
        IConcept oob = NS.makePresence(observable.getType());
        this.observable = new Observable(oob, KLAB.c(NS.PRESENCE_OBSERVATION), observable.getFormalName());
        if (observer instanceof KIMPresenceObserver) {
            isIndirect = ((KIMPresenceObserver) observer).isIndirect;
        }
    }

    @Override
    public List<IObservable> getAlternativeObservables() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public IStateActuator getAlternativeAccessor() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public IConcept getObservationType() {
        return KLAB.c(NS.PRESENCE_OBSERVATION);
    }

    @Override
    public IConcept getObservedType(KIMContext context, IConcept concept) {

        IConcept ret = concept;
        if (isIndirect) {
            try {
                ret = NS.makePresence(concept);
            } catch (Throwable e) {
                // will be null and caught later.
                ret = null;
            }
            if (ret == null) {
                context.error(concept
                        + ": presence is only assessed for things or agents. Use direct form (without 'of') for observables that are presence qualities.", getFirstLineNumber());
                return concept;
            }
        } else if (!concept.is(KLAB.c(NS.CORE_PRESENCE))) {
            context.error(concept
                    + ": the observable in this statement must be a presence already. Use the indirect form (with 'of') to annotate as the presence of subject observables.", getFirstLineNumber());
        }
        return ret;
    }

    public static String asString(IPresenceObserver observer) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public IObserver copy() {
        return new KIMPresenceObserver(this);
    }

    @Override
    public IStateActuator getMediator(IObserver observer, IMonitor monitor) throws ThinklabException {

        IActuator ret = getActuator(monitor);
        if (ret != null)
            return (IStateActuator) ret;

        return new PresenceActuator(getActions(), monitor, false);
    }

    @Override
    public IStateActuator getInterpreter(IStateActuator accessor, IMonitor monitor) {
        return new PresenceActuator(getActions(), monitor, true);
    }

    @Override
    public boolean canInterpretDirectly(IActuator actuator) {
        return false;
    }

    @Override
    public Object adapt() {
        return MapUtils.of("indirect?", isIndirect);
    }

    @Override
    public String toString() {
        return "PRS/" + getObservable();
    }

}
