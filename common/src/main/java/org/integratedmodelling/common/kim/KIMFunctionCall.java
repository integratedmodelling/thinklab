/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.common.kim;

import java.util.HashMap;
import java.util.Map;

import org.integratedmodelling.api.knowledge.IConcept;
import org.integratedmodelling.api.modelling.IFunctionCall;
import org.integratedmodelling.api.modelling.INamespace;
import org.integratedmodelling.api.project.IProject;
import org.integratedmodelling.api.services.IPrototype;
import org.integratedmodelling.common.configuration.KLAB;
import org.integratedmodelling.exceptions.ThinklabException;
import org.integratedmodelling.thinkQL.Function;
import org.integratedmodelling.thinkQL.KeyValuePair;

public class KIMFunctionCall extends KIMModelObject implements IFunctionCall {

    public KIMFunctionCall(String id, Map<String, Object> parameters, INamespace namespace) {
        super(null);
        this.type = id;
        this.parameters = parameters;
        this.namespace = namespace;
        this.project = namespace.getProject();
    }

    public KIMFunctionCall(KIMContext context, Function statement) {
        super(context, statement, null);
        this.type = statement.getId();
        if (statement.getParameters() != null) {
            if (statement.getParameters().getSingleValue() != null) {
                this.parameters.put(IFunctionCall.DEFAULT_PARAMETER_NAME, defineValue(context, statement
                        .getParameters().getSingleValue()));
            } else if (statement.getParameters().getPairs() != null) {
                for (KeyValuePair kv : statement.getParameters().getPairs()) {
                    this.parameters.put(kv.getKey(), defineValue(context, kv.getValue()));
                }
            }
        }

        IPrototype prot = KLAB.MMANAGER.getFunctionPrototype(type);
        if (prot != null) {
            try {
                prot.validateArguments(this);
            } catch (ThinklabException e) {
                context.error(e.getMessage(), getFirstLineNumber());
            }
        }
    }

    public KIMFunctionCall(KIMContext context, String id) {
        super(context, null, null);
        this.type = id;
    }

    String              type;
    Map<String, Object> parameters = new HashMap<>();
    IProject            project;

    @Override
    public Map<String, Object> getParameters() {
        return parameters;
    }

    @Override
    public String getId() {
        return type;
    }

    @Override
    public IPrototype getPrototype() {
        return KLAB.MMANAGER.getFunctionPrototype(type);
    }

    @Override
    public boolean returns(IConcept resultType) {
        IPrototype p = getPrototype();
        if (p != null) {
            for (IConcept c : p.getReturnTypes()) {
                if (c.is(resultType)) {
                    return true;
                }
            }
        }
        return false;
    }

    @Override
    public String toString() {

        String ret = id;
        if (parameters != null && parameters.size() > 0) {
            ret += "(";
            for (String k : parameters.keySet()) {
                ret += k + " = " + parameters.get(k) + " ";
            }
            ret += ")";
        }

        return ret;
    }

}
