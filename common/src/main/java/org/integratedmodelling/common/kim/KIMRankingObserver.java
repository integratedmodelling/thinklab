/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.common.kim;

import java.util.List;
import java.util.Map;

import org.integratedmodelling.api.data.IRankingScale;
import org.integratedmodelling.api.knowledge.IConcept;
import org.integratedmodelling.api.knowledge.ISemantic;
import org.integratedmodelling.api.modelling.IAction;
import org.integratedmodelling.api.modelling.IClassification;
import org.integratedmodelling.api.modelling.IObserver;
import org.integratedmodelling.api.modelling.IRankingObserver;
import org.integratedmodelling.api.modelling.runtime.IActuator;
import org.integratedmodelling.api.modelling.runtime.IStateActuator;
import org.integratedmodelling.api.monitoring.IMonitor;
import org.integratedmodelling.common.classification.Classification;
import org.integratedmodelling.common.configuration.KLAB;
import org.integratedmodelling.common.data.IndexedCategoricalDistribution;
import org.integratedmodelling.common.data.RankingScale;
import org.integratedmodelling.common.model.actuators.MediatingActuator;
import org.integratedmodelling.common.utils.MapUtils;
import org.integratedmodelling.common.vocabulary.NS;
import org.integratedmodelling.exceptions.ThinklabException;
import org.integratedmodelling.exceptions.ThinklabRuntimeException;
import org.integratedmodelling.thinkQL.Observer;

public class KIMRankingObserver extends KIMNumericObserver implements IRankingObserver {

    IRankingScale scale;

    public class RankingActuator extends MediatingActuator {

        boolean _errorsPresent = false;

        public RankingActuator(List<IAction> actions, IMonitor monitor, boolean interpreting) {
            super(actions, monitor, interpreting);
            if (discretization != null)
                ((org.integratedmodelling.common.classification.Classification) discretization)
                        .reset();

        }

        @Override
        public String toString() {
            return "[ranking " + scale + "]";
        }

        public IRankingScale getRankingScale() {
            return scale;
        }

        @Override
        public Object mediate(Object object) {

            double val = Double.NaN;

            if (object == null || (object instanceof Number && Double.isNaN(((Number) object).doubleValue())))
                return val;

            if (object instanceof IndexedCategoricalDistribution) {

                /*
                 * TODO try to mediate distributions
                 */
                val = ((IndexedCategoricalDistribution) object).getMean();
            } else if (object instanceof Number) {
                val = ((Number) object).doubleValue();
            } else {
                try {
                    val = object.toString().equals("NaN") ? Double.NaN : Double
                            .parseDouble(object.toString());
                } catch (Exception e) {
                    if (!_errorsPresent) {
                        monitor.error("cannot interpret value: " + object + " as a number");
                        _errorsPresent = true;
                    }
                    return Double.NaN;
                }
            }

            if (getMediatedObserver() == null)
                return val;

            return scale.convert(val, ((KIMRankingObserver) (getMediatedObserver())).scale);
        }
    }

    public KIMRankingObserver(KIMContext context, KIMModel model, Observer statement) {
        super(context, model, statement);
        if (statement.getFrom() != null || statement.getTo() != null) {
            scale = new RankingScale(KIM.processNumber(statement.getFrom()), KIM
                    .processNumber(statement.getTo()));
        }

        if (mediated != null) {
            /*
             * TODO must be another ranking and both must have a scale
             */
        }

        if (scale != null) {
            minimumValue = scale.getRange().getFirst().doubleValue();
            minimumValue = scale.getRange().getSecond().doubleValue();
        }
    }

    public KIMRankingObserver(IObserver observer) {
        super(observer);
        if (!(observer instanceof KIMRankingObserver)) {
            throw new ThinklabRuntimeException("cannot initialize a ranking observer from a "
                    + observer.getClass().getCanonicalName());
        }
        this.scale = ((KIMRankingObserver) observer).scale;
        if (scale != null) {
            minimumValue = scale.getRange().getFirst().doubleValue();
            minimumValue = scale.getRange().getSecond().doubleValue();
        }
    }

    public KIMRankingObserver(Map<?, ?> map) {
        if (map.containsKey("discretization")) {
            discretization = map.get("discretization") instanceof IClassification
                    ? (IClassification) map.get("discretization")
                    : new Classification((Map<?, ?>) map.get("discretization"));
        }
        scale = (RankingScale) map.get("scale");
    }

    @Override
    public IObserver copy() {
        return new KIMRankingObserver(this);
    }

    KIMRankingObserver(ISemantic observable) {
        // TODO Auto-generated constructor stub
    }

    @Override
    public IRankingScale getRankingScale() {
        // TODO Auto-generated method stub
        return scale;
    }

    @Override
    public IObserver getMediatedObserver() {
        return mediated;
    }

    @Override
    public IConcept getObservationType() {
        return KLAB.c(NS.RANKING);
    }

    public static String asString(IRankingObserver observer) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public IStateActuator getMediator(IObserver observer, IMonitor monitor) throws ThinklabException {
        IActuator ret = getActuator(monitor);
        if (ret != null)
            return (IStateActuator) ret;
        return new RankingActuator(getActions(), monitor, false);
    }

    @Override
    public IStateActuator getInterpreter(IStateActuator accessor, IMonitor monitor) {
        return new RankingActuator(getActions(), monitor, true);
    }

    @Override
    public boolean canInterpretDirectly(IActuator actuator) {
        return getActions().size() == 0 && discretization == null;
    }

    @Override
    public Object adapt() {
        return MapUtils.of("scale", scale, "discretization", discretization);
    }

    @Override
    public String toString() {
        return "RNK/" + getObservable();
    }
}
