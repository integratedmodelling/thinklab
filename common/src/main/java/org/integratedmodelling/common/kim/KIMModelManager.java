/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.common.kim;

import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.integratedmodelling.api.factories.IModelManager;
import org.integratedmodelling.api.knowledge.IConcept;
import org.integratedmodelling.api.knowledge.IKnowledge;
import org.integratedmodelling.api.modelling.IModelObject;
import org.integratedmodelling.api.modelling.INamespace;
import org.integratedmodelling.api.modelling.ISubject;
import org.integratedmodelling.api.runtime.ISession;
import org.integratedmodelling.api.services.IPrototype;
import org.integratedmodelling.collections.Path;
import org.integratedmodelling.common.command.ServiceManager;
import org.integratedmodelling.common.configuration.KLAB;
import org.integratedmodelling.common.data.IntelligentMap;
import org.integratedmodelling.exceptions.ThinklabException;

public class KIMModelManager implements IModelManager {

    Map<String, INamespace>                   namespaces        = new HashMap<>();
    IntelligentMap<Class<? extends ISubject>> subjectClasses    = new IntelligentMap<Class<? extends ISubject>>();
    Map<String, IKnowledge>                   exportedKnowledge = new HashMap<>();

    public void addNamespace(INamespace ns) {
        namespaces.put(ns.getId(), ns);
    }

    public void addExports(Map<String, IKnowledge> map) {
        exportedKnowledge.putAll(map);
    }

    @Override
    public INamespace getNamespace(String ns) {
        return namespaces.get(ns);
    }

    @Override
    public void releaseNamespace(String arg0) {
        synchronized (namespaces) {
            INamespace ns = namespaces.get(arg0);
            if (ns != null) {
                if (ns.getOntology() != null) {
                    KLAB.KM.releaseOntology(ns.getOntology().getConceptSpace());
                }
                namespaces.remove(arg0);
            }
        }
    }

    @Override
    public Collection<INamespace> getNamespaces() {
        return namespaces.values();
    }

    @Override
    public List<INamespace> getScenarios() {

        List<INamespace> ret = new ArrayList<>();
        for (INamespace ns : namespaces.values()) {
            if (ns.isScenario()) {
                ret.add(ns);
            }
        }
        return ret;
    }

    @Override
    public IModelObject findModelObject(String name) {
        String ns = Path.getLeading(name, '.');
        INamespace namespace = namespaces.get(ns);
        return namespace == null ? null : namespace.getModelObject(Path.getLast(name, '.'));
    }

    @Override
    public boolean isModelFile(File f) {
        // TODO smarter?
        return f.toString().endsWith(KIM.FILE_EXTENSION);
    }

    @Override
    public IPrototype getFunctionPrototype(String id) {
        if (KLAB.CLIENT != null && KLAB.CLIENT.getEngine() != null) {
            return KLAB.CLIENT.getEngine().getFunctionPrototype(id);
        }
        return ServiceManager.get().getFunctionPrototype(id);
    }

    @Override
    public INamespace getLocalNamespace() {

        INamespace ret = namespaces.get("local." + KLAB.NAME);
        if (ret == null) {
            namespaces.put("local." + KLAB.NAME, ret = new KIMNamespace("local." + KLAB.NAME));
        }
        return ret;
    }

    static int                  counter                  = 1;
    private static final String OBSERVATION_NAMESPACE_ID = "imobs";

    public INamespace getUserNamespace(ISession session) throws ThinklabException {

        /*
         * FIXME use user data
         */
        String key = session.getId();
        if (namespaces.containsKey(key)) {
            return namespaces.get(key);
        }

        INamespace ret = new KIMNamespace(key);
        namespaces.put(key, ret);
        return ret;
    }

    @Override
    public INamespace getObservationNamespace() {
        INamespace ret = getNamespace(OBSERVATION_NAMESPACE_ID);
        if (ret == null) {
            ret = new KIMNamespace(OBSERVATION_NAMESPACE_ID);
            namespaces.put(OBSERVATION_NAMESPACE_ID, ret);
        }
        return ret;
    }

    @Override
    public void registerSubjectClass(String concept, Class<? extends ISubject> cls) {
        if (!subjectClasses.containsKey(concept)) {
            subjectClasses.put(concept, cls);
        }
    }

    @Override
    public IKnowledge getExportedKnowledge(String id) {
        return exportedKnowledge.get(id);
    }

    @Override
    public Class<? extends ISubject> getSubjectClass(IConcept type) {
        Class<? extends ISubject> ret = subjectClasses.get(type);
        return ret;
    }

    @Override
    public void releaseAll() {
        ArrayList<String> nss = new ArrayList<>(namespaces.keySet());
        for (String id : nss) {
            releaseNamespace(id);
        }
    }

    /**
     * Dump a set of CSV-compatible strings with all the exported names.
     * @return
     */
    public List<String> dumpExports() {

        List<String> ret = new ArrayList<>();
        List<String> nms = new ArrayList<>(exportedKnowledge.keySet());

        Collections.sort(nms);
        for (String id : nms) {
            ret.add(id + "," + exportedKnowledge.get(id));
        }

        return ret;
    }

}
