/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.common.kim;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import org.integratedmodelling.api.knowledge.IConcept;
import org.integratedmodelling.api.knowledge.IKnowledge;
import org.integratedmodelling.api.modelling.IAction;
import org.integratedmodelling.api.modelling.IClassification;
import org.integratedmodelling.api.modelling.IDirectObserver;
import org.integratedmodelling.api.modelling.IDistanceObserver;
import org.integratedmodelling.api.modelling.IEvent;
import org.integratedmodelling.api.modelling.IObservable;
import org.integratedmodelling.api.modelling.IObserver;
import org.integratedmodelling.api.modelling.ISubject;
import org.integratedmodelling.api.modelling.IUnit;
import org.integratedmodelling.api.modelling.resolution.IResolutionContext;
import org.integratedmodelling.api.modelling.runtime.IActuator;
import org.integratedmodelling.api.modelling.runtime.IStateActuator;
import org.integratedmodelling.api.modelling.scheduling.ITransition;
import org.integratedmodelling.api.monitoring.IMonitor;
import org.integratedmodelling.api.space.ISpatialExtent;
import org.integratedmodelling.api.space.ISpatialIndex;
import org.integratedmodelling.collections.Path;
import org.integratedmodelling.common.classification.Classification;
import org.integratedmodelling.common.configuration.KLAB;
import org.integratedmodelling.common.data.IndexedCategoricalDistribution;
import org.integratedmodelling.common.model.actuators.MediatingActuator;
import org.integratedmodelling.common.model.actuators.StateActuator;
import org.integratedmodelling.common.utils.MapUtils;
import org.integratedmodelling.common.utils.StringUtils;
import org.integratedmodelling.common.vocabulary.NS;
import org.integratedmodelling.common.vocabulary.Observable;
import org.integratedmodelling.common.vocabulary.Unit;
import org.integratedmodelling.exceptions.ThinklabException;
import org.integratedmodelling.exceptions.ThinklabRuntimeException;
import org.integratedmodelling.exceptions.ThinklabValidationException;
import org.integratedmodelling.thinkQL.Observer;

public class KIMDistanceObserver extends KIMNumericObserver implements IDistanceObserver {

    IUnit    unit;
    IConcept originalConcept;

    public class DistanceComputer extends StateActuator {

        ISpatialIndex  index;
        ISpatialExtent extent;
        int            lastTransition = -2;
        Unit           meters         = new Unit("m");

        public DistanceComputer(List<IAction> actions, IMonitor monitor) {
            super(actions, monitor);
        }

        @Override
        protected void processState(int stateIndex, ITransition transition) throws ThinklabException {

            Double val = Double.NaN;

            if (index == null) {
                if (extent == null) {
                    if (context.getScale().getSpace() == null) {
                        throw new ThinklabValidationException("distance can not be computed in non-spatial contexts");
                    }
                    extent = context.getScale().getSpace();
                }
                index = extent.getIndex(true);

                if (fromObject.size() > 0) {

                    monitor.info("computing distance from "
                            + StringUtils.joinCollection(fromObject, ','), null);

                    for (IDirectObserver o : getConcreteObjects()) {
                        index.add(o.getCoverage().getSpace(), o.getName());
                    }
                }
            }

            /*
             * subjects and events may be added at any time, so we should check 
             * for new objects at each new transition
             */
            if (originalConcept != null && context instanceof ISubject && fromObject.size() == 0) {

                int transitionIndex = transition == null ? -1 : transition.getTimeIndex();

                if (transitionIndex != lastTransition) {

                    lastTransition = transitionIndex;

                    monitor.info("computing distance from all " + originalConcept, null);

                    for (ISubject s : ((ISubject) context).getSubjects()) {
                        if (s.getType().is(originalConcept) && s.getScale().getSpace() != null
                                && !index.contains(s.getId())) {
                            index.add(s.getScale().getSpace(), s.getId());
                        }
                    }
                    for (IEvent s : ((ISubject) context).getEvents()) {
                        if (s.getType().is(originalConcept) && s.getScale().getSpace() != null
                                && !index.contains(s.getId())) {
                            index.add(s.getScale().getSpace(), s.getId());
                        }
                    }
                }
            }

            /*
             *  process distance for state
             */
            if (index != null && index.size() > 0) {

                int sfs = context.getScale()
                        .getExtentOffset(context.getScale().getExtent(KLAB.c(NS.SPACE_DOMAIN)), stateIndex);
                val = index.distanceToNearestObjectFrom(sfs);
                if (!Double.isNaN(val) && !unit.equals(meters)) {
                    val = (Double) unit.convert(val, meters);
                }
            }

            Object ret = val;
            if (getDiscretization() != null) {
                ret = getDiscretization().classify(val);
            }

            _parameters.put(this.getName(), ret);
        }

    }

    /*
     * -----------------------------------------------------------------------------------
     * accessor - it's always a mediator, either to another measurement or to a datasource
     * whose content was defined explicitly to conform to our semantics
     * -----------------------------------------------------------------------------------
     */
    public class DistanceActuator extends MediatingActuator {

        IDistanceObserver other;
        boolean           errorsPresent = false;
        IMonitor          monitor;

        public DistanceActuator(IDistanceObserver other, List<IAction> actions, IMonitor monitor,
                boolean interpreting) {
            super(actions, monitor, interpreting);
            this.other = other;
            this.monitor = monitor;

            if (discretization != null)
                ((org.integratedmodelling.common.classification.Classification) discretization)
                        .reset();

        }

        @Override
        public String toString() {
            return "[distance: " + unit + "]";
        }

        public IUnit getUnit() {
            return unit;
        }

        @Override
        public Object mediate(Object object) {

            double val = Double.NaN;

            if (object == null || (object instanceof Number && Double.isNaN(((Number) object).doubleValue())))
                return val;

            if (object instanceof IndexedCategoricalDistribution) {
                val = ((IndexedCategoricalDistribution) object).getMean();
            } else if (object instanceof Number) {
                val = ((Number) object).doubleValue();
            } else {
                try {
                    val = object.toString().equals("NaN") ? Double.NaN : Double
                            .parseDouble(object.toString());
                } catch (Exception e) {
                    if (!errorsPresent) {
                        monitor.error("cannot interpret value: " + object);
                        errorsPresent = true;
                    }
                    return Double.NaN;
                }
            }

            return unit.convert(val, other.getUnit());
        }
    }

    public KIMDistanceObserver(KIMContext context, KIMModel model, Observer statement) {
        super(context, model, statement);
        unit = new KIMUnit(context.get(KIMContext.UNIT), statement.getUnit());
        // TODO check it's a length
        try {
            if (observable != null) {
                ((Observable) observable)
                        .setType(this.getObservedType(context, this.observable.getTypeAsConcept()));
            }
        } catch (ThinklabRuntimeException e) {
            context.error(e.getMessage(), getFirstLineNumber());
        }
    }

    public KIMDistanceObserver(IObserver observer) {
        super(observer);
        if (!(observer instanceof IDistanceObserver)) {
            throw new ThinklabRuntimeException("cannot initialize a distance observer from a "
                    + observer.getClass().getCanonicalName());
        }
        this.unit = ((IDistanceObserver) observer).getUnit();
    }

    public KIMDistanceObserver(Map<?, ?> map) {
        if (map.containsKey("discretization")) {
            discretization = map.get("discretization") instanceof IClassification
                    ? (IClassification) map.get("discretization")
                    : new Classification((Map<?, ?>) map.get("discretization"));
        }
        unit = new Unit(map.get("unit").toString());
    }

    public KIMDistanceObserver(String string, IObservable obs) {
        String[] defs = string.split("\\|");
        unit = new Unit(defs[1]);
        if (defs.length > 2) {
            discretization = new org.integratedmodelling.common.classification.Classification(defs[2]);
        }
        observable = obs;
    }

    @Override
    public IObserver copy() {
        return new KIMDistanceObserver(this);
    }

    @Override
    public List<IObservable> getAlternativeObservables() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public IStateActuator getAlternativeAccessor() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public IUnit getUnit() {
        return unit;
    }

    @Override
    public IObserver getMediatedObserver() {
        return mediated;
    }

    @Override
    public IConcept getObservationType() {
        return KLAB.c(NS.DISTANCE_OBSERVATION);
    }

    @Override
    protected IConcept getObservedType(KIMContext context, IConcept knowledge) {

        NS.synchronize();

        if (fromObject.size() == 0 && !knowledge.is(NS.DISTANCE)) {
            originalConcept = knowledge;
        }
        return NS.makeDistance(knowledge);
    }

    public static String asString(IDistanceObserver m) {
        String ret = "oDS|" + m.getUnit();
        if (m.getDiscretization() != null) {
            ret += "|"
                    + org.integratedmodelling.common.classification.Classification.asString(m
                            .getDiscretization());
        }
        return ret;
    }

    @Override
    public IStateActuator getMediator(IObserver observer, IMonitor monitor) throws ThinklabException {

        observer = getRepresentativeObserver(observer);

        IActuator ret = getActuator(monitor);
        if (ret != null)
            return (IStateActuator) ret;

        if (!(observer instanceof IDistanceObserver))
            throw new ThinklabValidationException("distances can only mediate other distances");

        if (getActions().size() == 0 && ((IDistanceObserver) observer).getUnit().equals(getUnit())) {
            return null;
        }

        return new DistanceActuator((IDistanceObserver) observer, getActions(), monitor, false);
    }

    @Override
    public IStateActuator getInterpreter(IStateActuator accessor, IMonitor monitor) {
        return new DistanceActuator(this, getActions(), monitor, true);
    }

    @Override
    public boolean canInterpretDirectly(IActuator actuator) {
        return getActions().size() == 0 && discretization == null;
    }

    @Override
    public IStateActuator getComputingAccessor(IMonitor monitor) throws ThinklabException {
        IStateActuator act = (IStateActuator) getActuator(monitor);
        if (act != null) {
            return act;
        }
        return new DistanceComputer(actions, monitor);
    }

    @Override
    public Object adapt() {
        return MapUtils.of("unit", getUnit().toString(), "discretization", discretization);
    }

    @Override
    public String toString() {
        return "DST/" + getObservable();
    }

    @Override
    public Collection<IDirectObserver> getConcreteObjects() throws ThinklabException {
        return resolveConcreteIds();
    }

    @Override
    protected IKnowledge getObservableConcept(KIMContext context, KIMKnowledge observed) {

        String cns = "";
        if (fromObject.size() > 0) {
            for (String s : fromObject) {
                cns += StringUtils.capitalize(Path.getLast(s, '.'));
            }
        }
        return NS.makeDistanceTo(context.getNamespace(), cns);
    }

    @Override
    public boolean isComputed() {
        /*
         * complicated, but this means that we need to go ahead and compute things ourselves (resolving
         *  the dependencies we will inject if we only have the concept of the objects to look for) 
         * instead of =resolving the observable from the network, if we represent distance
         * from something that we may have (object) or may find (concept). 
         */
        return super.isComputed() ||
                fromObject.size() > 0 ||
                (originalConcept != null && (model == null || (model != null
                        && !((KIMModel) model).hasDatasource() && !((KIMModel) model).hasObjectSource())));
    }

    @Override
    public List<IKnowledge> getInferredDependencies(IResolutionContext context) throws ThinklabException {

        List<IKnowledge> ret = new ArrayList<>();

        Collection<IDirectObserver> objects = resolveConcreteIds();
        if (objects.size() > 0) {

            // ensure the objects are spatial
            for (IDirectObserver ob : objects) {
                if (ob.getCoverage().getSpace() == null) {
                    throw new ThinklabValidationException("cannot compute distance from non-spatial object "
                            + ob.getName());
                }
            }
            // no deps, we'll handle these later.
            return ret;
        }

        if (originalConcept != null && model == null || (model != null && !((KIMModel) model).hasDatasource()
                && !((KIMModel) model).hasObjectSource())) {
            /*
             * add dependency on whatever object it is that we want the distance from.
             */
            ret.add(originalConcept);
        }

        return ret;
    }

}
