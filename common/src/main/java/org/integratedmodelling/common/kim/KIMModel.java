/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.common.kim;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import org.integratedmodelling.api.knowledge.IConcept;
import org.integratedmodelling.api.knowledge.IKnowledge;
import org.integratedmodelling.api.knowledge.IProperty;
import org.integratedmodelling.api.modelling.IAction;
import org.integratedmodelling.api.modelling.IClassifyingObserver;
import org.integratedmodelling.api.modelling.IDataSource;
import org.integratedmodelling.api.modelling.IFunctionCall;
import org.integratedmodelling.api.modelling.IKnowledgeObject;
import org.integratedmodelling.api.modelling.IModel;
import org.integratedmodelling.api.modelling.IModelObject;
import org.integratedmodelling.api.modelling.INamespace;
import org.integratedmodelling.api.modelling.IObjectSource;
import org.integratedmodelling.api.modelling.IObservable;
import org.integratedmodelling.api.modelling.IObserver;
import org.integratedmodelling.api.modelling.IScale;
import org.integratedmodelling.api.monitoring.IMonitor;
import org.integratedmodelling.collections.Pair;
import org.integratedmodelling.common.configuration.KLAB;
import org.integratedmodelling.common.vocabulary.NS;
import org.integratedmodelling.common.vocabulary.Observable;
import org.integratedmodelling.exceptions.ThinklabException;
import org.integratedmodelling.exceptions.ThinklabRuntimeException;
import org.integratedmodelling.lang.Axiom;
import org.integratedmodelling.lang.LogicalConnector;
import org.integratedmodelling.thinkQL.Annotation;
import org.integratedmodelling.thinkQL.ConceptDeclaration;
import org.integratedmodelling.thinkQL.ModelObservable;
import org.integratedmodelling.thinkQL.ModelStatement;

public class KIMModel extends KIMObservingObject implements IModel {

    boolean isPrivate;
    boolean isInactive;
    boolean isInstantiator;
    boolean isPrimary;
    boolean isResolver;

    IFunctionCall dataSourceGenerator;
    IFunctionCall objectSourceGenerator;
    IObserver     observer;
    Object        inlineValue;

    List<IObservable>         observables          = new ArrayList<>();
    List<AttributeTranslator> attributeTranslators = new ArrayList<>();

    /*
     * data structure below support reification models. If the model is a reification model, the _datasource
     * will be a source of objects and the attribute table below is used to associate properties to each
     * object based on their attributes.
     */
    static public class AttributeTranslator {

        public String    attribute;
        public IObserver observer;
        public IProperty property;
    }

    /*
     * Wrap an observer.
     */
    public KIMModel(KIMObserver observer) {
        super(observer);
        setParent(observer.getParent(), false);
        observer.setParent(this, true);
        if (observer.getObservable() != null) {
            this.observables.add(observer.getObservable());
        }
        this.observer = observer;
        this.namespace = observer.getNamespace();
        if (this.id == null && observer.getObservable() != null) {
            name(observer.getObservable().getFormalName());
        }
    }

    /*
     * Wrap an observer and use the object source from a model to produce its data source
     * generator function.
     */
    public KIMModel(KIMObserver observer, KIMModel ds, String attribute, IMonitor monitor) {
        super(observer);
        observer = (KIMObserver) observer.copy();
        observer.model = this;
        // jeez
        ((Observable) observer.getObservable()).setModel(this);
        ((Observable) observer.getObservable()).setObserver(observer);
        observer.setParent(this, false);
        // observer.setParent(this, true);
        if (observer.getObservable() != null) {
            Observable obs = new Observable((Observable) observer.getObservable());
            obs.setModel(this);
            obs.setObserver(observer);
            this.observables.add(obs);
        }
        this.observer = observer;
        this.namespace = observer.getNamespace();
        if (this.id == null && observer.getObservable() != null) {
            name(observer.getObservable().getFormalName());
        }

        if (ds.hasObjectSource()) {
            IObjectSource os;
            try {
                os = ds.getObjectSource(monitor);
                this.dataSourceGenerator = os.getDatasourceCallFor(attribute, getNamespace());
            } catch (ThinklabException e) {
                throw new ThinklabRuntimeException(e.getMessage());
            }
        } else {
            throw new ThinklabRuntimeException("model " + ds.getName() + " has no object source");
        }
    }

    /*
     * Wrap an observer with inline value.
     */
    KIMModel(Object literalValue, KIMObserver observer) {
        super(observer);
        inlineValue = literalValue;
        setParent(observer.getParent(), false);
        observer.setParent(this, true);
        this.observables.add(observer.getObservable());
        this.observer = observer;
    }

    /*
     * only for direct observers. Use makeModel in general
     */
    private KIMModel(INamespace namespace, KIMObservable obs) {
        super(namespace, obs.getFormalName());
        this.observables.add(obs);
        isInstantiator = true;
        if (this.id == null) {
            name(obs.getFormalName());
        }
    }

    /*
     * Wrap an observer using a given observable; does not ensure the observable is OK or make it so.
     */
    KIMModel(IKnowledge observable, IObserver observer, String formalName) {
        super((KIMObserver) observer);
        setParent(observer.getParent(), false);
        ((KIMObserver) observer).setParent(this, true);
        this.observer = observer;
        this.observables.add(new Observable(observable, observer.getObservationType(), formalName));
        // ((Observable) this.observables.get(0)).setObserver(observer);
        name(formalName);
    }

    public KIMModel(KIMContext context, ModelStatement statement, IModelObject parent,
            List<Annotation> annotations) {

        super(context, statement, parent, annotations);

        this.isPrivate = statement.isPrivate() || getNamespace().isPrivate();
        this.isInactive = statement.isInactive() || getNamespace().isInactive();

        // record whether this is a model that comes from a user statement. Used to establish
        // namespace of reference for a context.
        this.isPrimary = true;

        boolean isOk = false;

        if (statement.isReification()) {
            isOk = defineInstantiatorModel(context, statement);
        } else {
            isOk = defineExplanationModel(context, statement);
        }

        defineDependencies(context, statement.getDependencies());

        if (statement.getAccessor() != null) {
            this.actuatorCall = new KIMFunctionCall(context
                    .get(KIMContext.ACTUATOR_FUNCTION_CALL), statement
                            .getAccessor());
            validateFunctionCall(context, this.actuatorCall, getExpectedDirectContextualizer());
        }

        if (statement.getContextualizers() != null) {

            Pair<List<IFunctionCall>, List<IAction>> zio = defineContextActions(context, this, statement
                    .getContextualizers());

            for (IFunctionCall function : zio.getFirst()) {
                scaleGenerators.add(function);
            }

            for (IAction action : zio.getSecond()) {
                actions.add(action);
            }
        }

        if (!isOk) {
            this.isInactive = true;
        }

        /*
         * Quality models: local concepts must be validated for inconsistency and aligned with the observer if necessary
         */
        if (observables.size() > 0 && observables.get(0) != null && observer != null) {

            IKnowledge obb = observables.get(0).getType();
            if (NS.isObservable(obb) || NS.isTrait(obb)) {
                /*
                 * check core concepts are same unless we are classifying
                 */
                IKnowledge c1 = NS.getCoreType(obb);
                IKnowledge c2 = NS.getCoreType(observer.getObservable());
                if (NS.isTrait(obb) && !(observer instanceof IClassifyingObserver)) {

                    context.error("inconsistent semantics: "
                            + obb
                            + ": traits can only be observed through a classify statement", lineNumber(statement));
                } else if (!(NS.isTrait(obb) && observer instanceof IClassifyingObserver) && !c1.equals(c2)) {
                    context.error("wrong observables: cannot mix a " + c1 + " with a "
                            + c2, lineNumber(statement));
                }

            } else if (obb.getConceptSpace().equals(context.getNamespace().getId())) {
                /*
                 * make NS 
                 */
                if (NS.isTrait(obb) && !(observer instanceof IClassifyingObserver)) {

                    context.error("inconsistent semantics for local concept: "
                            + obb
                            + " is a trait and the observer is not a classification", lineNumber(statement));

                } else if (observer.getObservable() != null) {

                    context.getNamespace().addAxiom(Axiom
                            .SubClass(observer.getObservable().getType().toString(), obb.getLocalName()));
                    context.synchronize();
                }

            } else {
                /*
                 * TODO non-observable 
                 */
                context.error("inconsistent semantics for external concept: "
                        + obb + " is not an observable", lineNumber(statement));
            }
        }

        if (getObservable() != null && getObservable().getType() != null
                && (NS.isQuality(getObservable()) || NS.isTrait(getObservable()))
                && observer == null) {
            context.error("quality models must have an observer", lineNumber(statement));
        }

        /*
         * TODO consider making observers and actions children of the model when we have the 
         * silly icons for them.
         */
    }

    private IConcept getExpectedDirectContextualizer() {

        if (observables.size() < 1 || observables.get(0).getType() == null) {
            return null;
        }

        if (NS.isThing(getObservable())) {
            return isInstantiator ? KLAB.c(NS.SUBJECT_INSTANTIATOR) : KLAB.c(NS.SUBJECT_CONTEXTUALIZER);
        } else if (NS.isProcess(getObservable())) {
            return KLAB.c(NS.PROCESS_CONTEXTUALIZER);
        } else if (NS.isEvent(getObservable())) {
            return isInstantiator ? KLAB.c(NS.EVENT_INSTANTIATOR) : KLAB.c(NS.EVENT_CONTEXTUALIZER);
        } else if (NS.isRelationship(getObservable())) {
            return KLAB.c(NS.RELATIONSHIP_CONTEXTUALIZER);
        }
        return null;
    }

    private boolean defineExplanationModel(KIMContext context, ModelStatement statement) {

        boolean isNothing = false;
        IFunctionCall functionCall = null;

        /*
         * observable(s)
         */
        int obsIdx = 0;
        for (ModelObservable observable : statement.getObservables()) {

            KIMObservable ko = null;

            if (observable.getDeclaration() != null) {

                ko = new KIMObservable(context, new KIMKnowledge(context
                        .get(KIMContext.MODEL_OBSERVABLE), observable.getDeclaration(), this));
                if (ko.isNothing()) {
                    isNothing = true;
                }
                // ko.setInherentTypes(context, observable.getDeclaration());
                observables.add(ko);
            } else if (observable.getConceptStatement() != null) {
                ko = new KIMObservable(context, new KIMKnowledge(context
                        .get(KIMContext.MODEL_OBSERVABLE), observable.getConceptStatement(), this));
                if (ko.isNothing()) {
                    isNothing = true;
                }
                observables.add(ko);

            } else if (observable.getFunction() != null) {

                if (obsIdx > 0) {
                    context.error("cannot have a data source function as a secondary observable", lineNumber(observable
                            .getFunction()));
                }
                functionCall = new KIMFunctionCall(context.get(KIMContext.OBSERVED_FUNCTIONCALL), observable
                        .getFunction());
            } else if (observable.getLiteral() != null) {
                // TODO!
                inlineValue = KIM.processLiteral(observable.getLiteral());
            } else if (observable.getInlineModel() != null) {

                IObserver observ = KIMObservingObject
                        .defineObserver(context, null, observable.getInlineModel()
                                .getObserver());

                KIMObservable oob = null;

                if (observable.getInlineModel().getValue() != null) {

                    Object literalValue = KIM.processLiteral(observable.getInlineModel().getValue());
                    oob = new KIMObservable(literalValue, observ);

                } else if (observable.getInlineModel().getConcept() != null) {

                    IKnowledgeObject obs = new KIMKnowledge(context
                            .get(KIMContext.DEPENDENCY_OBSERVABLE), observable
                                    .getInlineModel().getConcept(), null);
                    oob = new KIMObservable(obs, observ);

                } else {
                    oob = new KIMObservable(observ);
                }

                if (oob != null) {
                    if (observable.getObservableName() != null) {
                        oob.setFormalName(observable.getObservableName());
                    }
                    observables.add(oob);
                }

            }

            obsIdx++;
        }

        if (isNothing || (functionCall == null && inlineValue == null && observables.size() < 1
                && !statement.isInterpreter())) {
            return false;
        }

        /*
         * observers
         */
        if (statement.getObservers() != null) {
            this.observer = defineObserver(context, this, statement.getObservers());
        }

        /*
         * if we have a datasource, inline value, or are simply interpreting the observer, our observable is the observer's.
         */
        if (observer != null && (functionCall != null || inlineValue != null || statement.isInterpreter())) {
            observables.add(0, observer.getObservable());
        }

        /*
         * if the observable is a new concept we need to make it a child of the same observable we have in
         * the observer.
         */
        if (observables.size() > 0 && observables.get(0) != null && observables.get(0).getType() != null) {
            IKnowledge type = observables.get(0).getType();

            if (!NS.isObservable(type) && !NS.isTrait(type)) {
                if (type.getConceptSpace().equals(getNamespace().getId())) {
                    if (observer != null && observer.getObservable() != null
                            && observer.getObservable().getType() != null) {
                        type.getOntology().define(Collections.singleton(Axiom
                                .SubClass(observer.getObservable().getType().toString(), type
                                        .getLocalName())));
                        type = KLAB.c(type.toString());
                        /*
                         * reset the observable
                         */
                        ((Observable) observables.get(0)).setObservationType(null);
                        ((Observable) observables.get(0)).setType(new KIMKnowledge(context, type), context);
                        ((Observable) observables.get(0)).setObserver(observer);
                    } else {
                        context.error("undeclared direct observable " + type, lineNumber(statement));
                    }
                } else {
                    context.error("external concept " + type + " is not observable", lineNumber(statement));
                }
            } else {
                if (observer != null) {
                    if (!((KIMObserver) observer).isCompatibleWith(type)) {
                        context.error(type + " is not compatible with the observer's type "
                                + observer.getObservable().getType(), lineNumber(statement));
                    }
                }
            }
        }

        /*
         * name self and every observer in cascade. Bit complicated to accommodate
         * redundancy for fluency in interpreter models.
         */
        String name = statement.getName();
        if (name == null) {
            name = statement.getName2();
        } else if (statement.getName2() != null) {
            context.error("cannot use 'named' twice: please give this model a unique name", lineNumber(statement));
        }
        if (name /* still */ == null) {
            name = context.getNamespace().getNewObjectName(this);
        }

        name(name);

        if (functionCall != null) {

            /*
             * observables are still undefined. Must keep it, load the concept later and postpone validation.
             */
            IKnowledge match = null;
            if (this.observer != null && observer.getObservable() != null
                    && observer.getObservable().getType() != null) {
                match = observer.getObservable().getType();
                if (observer.getObservable().getTraitType() != null) {
                    match = observer.getObservable().getTraitType();
                }
            }

            if (match == null) {
                // ziocan
                KLAB.error("XIOCAN no match in ds");
                return false;
            }

            if (NS.isQuality(match) || NS.isTrait(match)) {
                if (validateFunctionCall(context, functionCall, KLAB.c(NS.DATASOURCE)) != null) {
                    dataSourceGenerator = functionCall;
                }
            } else if (NS.isThing(match)) {
                if (validateFunctionCall(context, functionCall, KLAB.c(NS.OBJECTSOURCE)) != null) {
                    objectSourceGenerator = functionCall;
                }
            }
        }

        if (observables.size() > 0 && observables.get(0) != null
                && observables.get(0).getObservationType() == null) {
            if (observer != null) {
                ((Observable) observables.get(0)).setObservationType(observer.getObservationType());
                ((Observable) observables.get(0)).setObserver(observer);
            } else if (NS.isDirect(observables.get(0).getType())) {
                ((Observable) observables.get(0)).setObservationType(KLAB.c(NS.DIRECT_OBSERVATION));
            }
        }

        for (IObservable obs : observables) {
            if (obs.getType() != null && obs.getType().isAbstract()
                    && !(obs.getObserver() instanceof IClassifyingObserver)) {
                context.error("the observed type for a model cannot be abstract: abstract types are only "
                        + "admitted for classifications, in dependencies or in 'resolve' statements", ((KIMObservable) obs)
                                .getFirstLineNumber());
            }
        }

        /*
         * TODO validate that qualities have their inherent type
         */
        // for (IObservable obs : observables) {
        // if (obs instanceof KIMObservable) {
        // ((KIMObservable) obs).validateTypes(true, context);
        // }
        // }
        return true;

    }

    @Override
    protected IScale makeScale() throws ThinklabException {
        if (this.scale == null) {
            IScale defined = KLAB.MFACTORY.createScale(getScaleGenerators());
            if (defined != null && !defined.isEmpty()) {
                this.scale = defined;// .merge(this.scale, LogicalConnector.INTERSECTION, true);
            }

            IScale dscale = null;
            if (dataSourceGenerator != null) {
                dscale = getDatasource(KLAB.CMANAGER.getMonitor()).getCoverage();
            } else if (objectSourceGenerator != null) {
                dscale = getObjectSource(KLAB.CMANAGER.getMonitor()).getCoverage();
            }

            if (dscale != null) {
                this.scale = this.scale == null ? dscale
                        : this.scale.merge(dscale, LogicalConnector.INTERSECTION, true);
            }
        }
        return scale;
    }

    private Collection<IFunctionCall> getScaleGenerators() {
        if (scaleGenerators.isEmpty() && observer != null
                && !((KIMObservingObject) observer).scaleGenerators.isEmpty()) {
            return ((KIMObservingObject) observer).scaleGenerators;
        }
        return scaleGenerators;
    }

    private boolean defineInstantiatorModel(KIMContext context, ModelStatement statement) {

        isInstantiator = true;

        name(statement.getName() == null ? context.getNamespace().getNewObjectName(this)
                : statement.getName());

        for (ConceptDeclaration observable : statement.getObservable()) {
            KIMKnowledge ko = new KIMKnowledge(context.get(KIMContext.MODEL_OBSERVABLE), observable, null);
            if (!ko.isNothing()) {
                if (!NS.isObject(ko)) {
                    context.error("observables of instantiator models must be direct: subjects, events or processes", ko
                            .getFirstLineNumber());
                } else {
                    observables.add(new Observable(ko.getKnowledge(), KLAB.c(NS.DIRECT_OBSERVATION), id));
                }
            }
        }

        if (statement.getAgentSource() != null) {
            objectSourceGenerator = new KIMFunctionCall(context
                    .get(KIMContext.REIFICATION_FUNCTION_CALL), statement.getAgentSource());
            validateFunctionCall(context, objectSourceGenerator, KLAB.c(NS.OBJECTSOURCE));
        }

        if (statement.getAttributeTranslators() != null) {
            for (org.integratedmodelling.thinkQL.AttributeTranslator at : statement
                    .getAttributeTranslators()) {

                IObserver obser = null;
                if (at.getObservers() != null) {
                    obser = defineObserver(context.get(KIMContext.OBSERVER_ATTRIBUTE), this, at
                            .getObservers());
                    ((Observable) obser.getObservable()).setObserver(obser);
                }

                if (at.getProperty() != null && KLAB.KM.getProperty(at.getProperty()) == null) {
                    KLAB.error("can't translate property " + at.getProperty());
                    continue;
                }

                AttributeTranslator att = new AttributeTranslator();
                att.observer = obser;
                att.attribute = at.getAttributeId();
                att.property = at.getProperty() == null ? null : KLAB.p(at.getProperty());

                attributeTranslators.add(att);

            }
        }

        return true;
    }

    @Override
    public List<IObservable> getObservables() {
        return observables;
    }

    @Override
    public IDataSource getDatasource(IMonitor monitor) throws ThinklabException {

        if (inlineValue != null) {
            return KLAB.MFACTORY.createConstantDataSource(inlineValue);
        }
        return dataSourceGenerator == null ? null
                : KLAB.MFACTORY.createDatasource(dataSourceGenerator, monitor);
    }

    @Override
    public IObjectSource getObjectSource(IMonitor monitor) throws ThinklabException {
        return objectSourceGenerator == null ? null
                : KLAB.MFACTORY.createObjectsource(objectSourceGenerator, monitor);
    }

    @Override
    public Collection<Pair<String, IObserver>> getAttributeObservers() {

        List<Pair<String, IObserver>> ret = new ArrayList<>();

        for (AttributeTranslator t : attributeTranslators) {
            if (t.observer != null) {
                ret.add(new Pair<>(t.attribute, t.observer));
            }
        }

        return ret;
    }

    @Override
    public Collection<Pair<String, IProperty>> getAttributeMetadata() {

        List<Pair<String, IProperty>> ret = new ArrayList<>();

        for (AttributeTranslator t : attributeTranslators) {
            if (t.observer == null) {
                ret.add(new Pair<>(t.attribute, t.property));
            }
        }

        return ret;

    }

    @Override
    public IObserver getObserver() {
        return observer;
    }

    @Override
    public boolean isResolved() {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public boolean isInstantiator() {
        return isInstantiator;
    }

    @Override
    public IObservable getObservable() {
        return observables.size() > 0 ? observables.get(0) : null;
    }

    @Override
    public String toString() {
        return "M/" + getName() + "/" + getObservable();
    }

    @Override
    public IKnowledge getType() {
        return getObservable().getType();
    }

    @Override
    public boolean isPrivate() {
        return isPrivate || super.isPrivate();
    }

    @Override
    public boolean isInactive() {
        return isInactive || super.isInactive();
    }

    @Override
    public void name(String id) {
        super.name(id);
        if (observer != null) {
            ((KIMObserver) observer).name(id);
        }
        if (getObservable() != null) {
            ((Observable) getObservable()).setFormalName(id);
        }
    }

    @Override
    public Object getInlineValue() {
        return inlineValue;
    }

    public boolean hasDatasource() {
        return dataSourceGenerator != null;
    }

    public boolean hasObjectSource() {
        return objectSourceGenerator != null;
    }

    /**
     * Make a default model from an observable. Only works for class/trait and for direct observers. If direct, select an
     * instantiator.
     * 
     * @param context
     * @param oob
     * @return
     */
    public static IModel makeModel(KIMContext context, KIMObservable oob) {

        if (NS.isDirect(oob)) {

            return new KIMModel(context.getNamespace(), oob);

        } else if (NS.isClass(oob) || NS.isTrait(oob)) {

            KIMClassificationObserver co = new KIMClassificationObserver(oob);
            oob.setObserver(co);
            co.namespace = context.getNamespace();
            return new KIMModel(co);

        } else {
            context.error("models can only be created without specifications for direct observables or classes/traits", oob
                    .getFirstLineNumber());
        }
        return null;
    }

    public IObserver getAttributeObserver(String dereifyingAttribute) {
        for (AttributeTranslator z : attributeTranslators) {
            if (z.attribute.equals(dereifyingAttribute)) {
                return z.observer;
            }
        }
        return null;
    }

    @Override
    public boolean isAvailable() {

        if (hasDatasource()) {
            try {
                return getDatasource(KLAB.CMANAGER.getMonitor()).isAvailable();
            } catch (ThinklabException e) {
                return false;
            }
        } else if (hasObjectSource()) {
            try {
                return getObjectSource(KLAB.CMANAGER.getMonitor()).isAvailable();
            } catch (ThinklabException e) {
                return false;
            }
        }

        return true;
    }

    public boolean isPrimary() {
        return isPrimary;
    }

    public boolean isResolver() {
        return isResolver;
    }
}
