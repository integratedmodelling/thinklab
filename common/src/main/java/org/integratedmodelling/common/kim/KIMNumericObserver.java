/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.common.kim;

import org.integratedmodelling.api.modelling.IClassification;
import org.integratedmodelling.api.modelling.IModel;
import org.integratedmodelling.api.modelling.INumericObserver;
import org.integratedmodelling.api.modelling.IObserver;
import org.integratedmodelling.exceptions.ThinklabRuntimeException;
import org.integratedmodelling.thinkQL.Observer;

public abstract class KIMNumericObserver extends KIMObserver implements INumericObserver {

    protected IClassification discretization;
    protected double          minimumValue = Double.NaN;
    protected double          maximumValue = Double.NaN;

    KIMNumericObserver() {
    }

    public KIMNumericObserver(KIMContext context, IModel model, Observer statement) {
        super(context, model, statement);
        if (statement.getDiscretization() != null) {
            discretization = new KIMClassification(context.get(getDiscretizationContext()), statement
                    .getDiscretization(), this.getObservable());
        }
    }

    public KIMNumericObserver(IObserver observer) {
        super(observer);
        if (!(observer instanceof INumericObserver)) {
            throw new ThinklabRuntimeException("cannot initialize a numeric observer from a "
                    + observer.getClass().getCanonicalName());
        }
        this.discretization = ((INumericObserver) observer).getDiscretization();
        this.maximumValue = ((INumericObserver) observer).getMaximumValue();
        this.minimumValue = ((INumericObserver) observer).getMinimumValue();
    }

    protected int getDiscretizationContext() {
        return KIMContext.NUMERIC_DISCRETIZATION;
    }

    @Override
    public IClassification getDiscretization() {
        return discretization;
    }

    @Override
    public double getMinimumValue() {
        return minimumValue;
    }

    @Override
    public double getMaximumValue() {
        return maximumValue;
    }

}
