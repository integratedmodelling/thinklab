/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.common.kim;

import java.util.Collection;

import org.integratedmodelling.api.knowledge.IConcept;
import org.integratedmodelling.api.knowledge.IProperty;
import org.integratedmodelling.api.modelling.IKnowledgeObject;
import org.integratedmodelling.api.modelling.IModelObject;
import org.integratedmodelling.common.configuration.KLAB;
import org.integratedmodelling.common.vocabulary.NS;
import org.integratedmodelling.lang.Axiom;
import org.integratedmodelling.thinkQL.RestrictionDefinition;

public class KIMRestriction extends KIMModelObject {

    public KIMRestriction(KIMContext context, RestrictionDefinition statement, IModelObject parent) {

        super(context, statement, parent);

        String type = getRelationshipType(context);
        boolean functional = false;
        boolean isSubject = false;

        if (type.equals("requires")) {
            type = "has";
            functional = true;
        }

        IKnowledgeObject rootConcept = (IKnowledgeObject) context.get(IKnowledgeObject.class);
        String id = rootConcept.getId();

        IKnowledgeObject property = null;
        IKnowledgeObject concept = null;
        IConcept forconcept = null;
        String pcand = null;

        // if (statement.getStatement() != null) {
        // property = new KIMKnowledge(context /* CHECK */, statement.getStatement());
        // } else if (statement.getConcept() != null) {
        //
        // concept = new KIMKnowledge(context /* CHECK */, statement.getConcept());
        // if (concept.isNothing()) {
        // context.error("unknown concept " + concept.getName()
        // + " in restriction", lineNumber(statement.getConcept()));
        // return;
        // }
        //
        // } else {

        /*
         * may be a concept (which needs to exist) or a property (lowercase first letter if
         * not existing, or existing). If concept, create property based on concept type.
         */
        String pp = statement.getSource();

        /*
         * if it's not a suitable concept ID, skip this step.
         */
        if (isSuitableConceptId(pp)) {
            concept = new KIMKnowledge(context.get(KIMContext.CONCEPT), pp, true, getFirstLineNumber());
        } else {
            pcand = pp;
        }

        if (statement.getSubject() != null && isSuitableConceptId(statement.getSubject())) {
            IKnowledgeObject fc = new KIMKnowledge(context.get(KIMContext.CONCEPT), statement
                    .getSubject(), false, getFirstLineNumber());
            if (!fc.isNothing()) {
                forconcept = (IConcept) fc.getKnowledge();
            }
        } else if (statement.getSubject() != null && isSuitablePropertyId(statement.getSubject())) {
            IKnowledgeObject fc = new KIMKnowledge(context.get(KIMContext.PROPERTY), statement
                    .getSubject(), true, getFirstLineNumber());
            if (!fc.isNothing()) {
                property = fc;
            }
        }
        // }

        if (concept != null && !concept.isNothing()) {
            IConcept c = KLAB.c(concept.getName());
            isSubject = NS.isObject(c);
            // if (c.getLocalName().equals("Ecotype")) {
            // System.out.println("ZOZOOZO");
            // }
        }

        if (concept != null && property == null && !concept.isNothing()) {

            // ensure we have what we created above, if anything
            // resolver.getNamespace().synchronizeKnowledge(resolver);
            IConcept c = KLAB.c(concept.getName());
            IConcept main = context.getNamespace().getOntology().getConcept(id);
            IProperty prop = null;
            // name of property if we have to create it.
            String propName = type + concept.getId();

            /*
             * find property. Check if we're specializing an existing restriction first.
             */
            if (forconcept != null) {
                Collection<IProperty> pr = main.findRestrictingProperty(forconcept);
                if (pr.size() > 0) {
                    prop = pr.iterator().next();
                    if (pr.size() > 1) {
                        context.warning("more than one restriction on concept " + c
                                + "; assuming " + prop, lineNumber(statement));
                    }
                    property = new KIMKnowledge(context, prop);
                }
            } else {
                prop = context.getNamespace().getOntology().getProperty(propName);
            }

            if (prop == null) {

                /*
                 * create property, using concept type and relationship type as a guide for
                 * inheritance. Only creating object restrictions.
                 */

                context.getNamespace().addAxiom(Axiom.ObjectPropertyAssertion(propName));
                context.getNamespace()
                        .addAxiom(Axiom.ObjectPropertyDomain(propName, rootConcept.getName()));
                context.getNamespace()
                        .addAxiom(Axiom.ObjectPropertyRange(propName, concept.getId()));

                if (functional) {
                    context.getNamespace()
                            .addAxiom(Axiom.FunctionalObjectProperty(propName));
                }

                // it's an inverse part-of or an inherency.
                if (type.equals("contains")) {
                    //
                } else {
                    //
                }

                context.synchronize();

            }

            if (property == null) {
                IKnowledgeObject fc = new KIMKnowledge(context
                        .get(KIMContext.PROPERTY), propName, false, getFirstLineNumber());
                if (!fc.isNothing()) {
                    property = fc;
                }
            }
        }

        if (property /* still */ == null && isSuitablePropertyId(pcand)) {
            IKnowledgeObject fc = new KIMKnowledge(context
                    .get(KIMContext.PROPERTY), pcand, false, getFirstLineNumber());
            if (!fc.isNothing()) {
                property = fc;
            }
        }

        if (property == null || property.isNothing() || concept == null || concept.isNothing()) {
            context.error("restriction uses unknown concepts or properties", lineNumber(statement));
            return;
        }

        if (statement.isAtLeast()) {

            context.getNamespace()
                    .addAxiom(Axiom.AtLeastNValuesFrom(id, property.getName(), concept
                            .getName(), statement.getHowmany().getInt()));

        } else if (statement.isAtMost()) {

            context.getNamespace()
                    .addAxiom(Axiom.AtMostNValuesFrom(id, property.getName(), concept
                            .getName(), statement.getHowmany().getInt()));

        } else if (statement.isExactly()) {

            context.getNamespace()
                    .addAxiom(Axiom.ExactlyNValuesFrom(id, property.getName(), concept
                            .getName(), statement.getHowmany().getInt()));

        } else if (statement.isNone()) {

            context.getNamespace()
                    .addAxiom(Axiom.NoValuesFrom(id, property.getName(), concept
                            .getName()));

        } else if (statement.isOnly()) {

            /*
             * all values from
             */
            context.getNamespace().addAxiom(Axiom.AllValuesFrom(id, property.getName(), concept
                    .getName()));

        } else {

            /*
             * some values from if not quantified in other ways 
             */
            context.getNamespace()
                    .addAxiom(Axiom.SomeValuesFrom(id, property.getName(), concept
                            .getName()));
        }

        context.synchronize();
    }

    private String getRelationshipType(KIMContext context) {
        if (context.isWithin(KIMContext.USES_RELATIONSHIP)) {
            return "uses";
        }
        if (context.isWithin(KIMContext.HAS_RELATIONSHIP)) {
            return "has";
        }
        if (context.isWithin(KIMContext.CONTAINS_RELATIONSHIP)) {
            return "contains";
        }
        return "with";
    }

}
