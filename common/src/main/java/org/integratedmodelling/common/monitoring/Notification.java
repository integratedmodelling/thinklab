/*******************************************************************************
 *  Copyright (C) 2007, 2014:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.common.monitoring;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.integratedmodelling.api.monitoring.INotification;
import org.integratedmodelling.api.monitoring.Messages;
import org.integratedmodelling.api.runtime.ISession;
import org.integratedmodelling.lang.IRemoteSerializable;

public class Notification implements INotification, IRemoteSerializable {

    long              _taskId;
    int               _level;
    String            _text;
    long              _time;
    String            _sessionId;
    String            _message   = null;
    ArrayList<Object> _arguments = new ArrayList<Object>();

    public Notification(long task, int level, String text, long time, String sessionId) {
        _taskId = task;
        _level = level;
        _text = text;
        _time = time;
        _sessionId = sessionId;
    }

    public Notification(long task, long time, String message, ISession session, Object... arguments) {
        _taskId = task;
        _time = time;
        _message = message;
        if (session != null) {
            _sessionId = session.getId();
        }
        for (Object o : arguments) {
            _arguments.add(o);
        }
    }

    public Notification(long task, String message, ISession session, Object... arguments) {
        _taskId = task;
        _time = new Date().getTime();
        _message = message;
        if (session != null) {
            _sessionId = session.getId();
        }
        for (Object o : arguments) {
            _arguments.add(o);
        }
    }

    public Notification(String message, ISession session, Object... arguments) {
        _taskId = -1;
        _time = new Date().getTime();
        _message = message;
        if (session != null) {
            _sessionId = session.getId();
        }
        for (Object o : arguments) {
            _arguments.add(o);
        }
    }

    public Notification(Map<?, ?> map) {

        if (map.containsKey("message")) {
            _message = map.get("message").toString();
            List<?> args = (List<?>) map.get("arguments");
            for (Object o : args) {
                _arguments.add(o);
            }
        } else {
            _text = map.containsKey("text") ? map.get("text").toString() : "";
            _level = ((Number) (map.get("level"))).intValue();
        }
        _time = ((Number) (map.get("time"))).longValue();
        _taskId = ((Number) (map.get("taskId"))).intValue();
        _sessionId = map.containsKey("sessionId") ? map.get("sessionId").toString() : null;
    }

    @Override
    public String getMessage() {
        return _message;
    }

    @Override
    public Object get(int n) {
        return _arguments.get(n);
    }

    @Override
    public String getString(int n) {
        return _arguments.get(n).toString();
    }

    @Override
    public int getInt(int n) {
        return Integer.parseInt(_arguments.get(n).toString());
    }

    @Override
    public long getLong(int n) {
        return Long.parseLong(_arguments.get(n).toString());
    }

    @Override
    public double getDouble(int n) {
        return Double.parseDouble(_arguments.get(n).toString());
    }

    @Override
    public long getTaskId() {
        return _taskId;
    }

    public void setTaskId(long taskId) {
        _taskId = taskId;
    }

    @Override
    public int getLevel() {
        return _level;
    }

    @Override
    public String getText() {
        return _text;
    }

    @Override
    public String toString() {
        if (_text != null) {
            return _taskId + ": " + _text;
        } else {
            String a = "";
            if (_arguments != null) {
                for (Object aa : _arguments) {
                    a += (a.isEmpty() ? "" : "#") + aa;
                }
            }
            return _taskId + ": " + _message + "(" + a + ")";
        }
    }

    @Override
    public Object adapt() {

        Map<String, Object> ret = new HashMap<String, Object>();

        if (_message != null) {
            ret.put("message", _message);
            ret.put("arguments", _arguments);
        } else {
            ret.put("text", _text);
            ret.put("level", _level);
        }
        ret.put("time", _time);
        ret.put("taskId", _taskId);
        if (_sessionId != null) {
            ret.put("sessionId", _sessionId);
        }

        return ret;
    }

    @Override
    public String getSessionId() {
        return _sessionId;
    }

    @Override
    public int size() {
        return _arguments.size();
    }
}
