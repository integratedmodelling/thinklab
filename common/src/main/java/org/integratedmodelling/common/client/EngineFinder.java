package org.integratedmodelling.common.client;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.integratedmodelling.api.engine.IEngine;
import org.integratedmodelling.common.network.Broadcaster;

import lombok.Data;

/**
 * Just create one to keep a running list of active engines on the local network. Don't 
 * forget to call close() on shutdown.
 * 
 * @author ferdinando.villa
 *
 */
class EngineFinder extends Broadcaster {

    static @Data class EngineData {
        final String ip;
        final int    port;
    }

    Map<EngineData, Long> engines = new HashMap<>();

    EngineFinder() {
        super(IEngine.MODELER_APPLICATION_ID);
    }

    /**
     * If there is an engine active on the localhost, return its URL. Otherwise return null.
     * 
     * @return
     */
    public String getLocalEngineUrl() {
        
        String addr = null;
        try {
            addr = InetAddress.getLocalHost().getHostAddress();
        } catch (UnknownHostException e) {
            return null;
        }
       
        synchronized(engines) {
            for (EngineData ed : engines.keySet()) {
                if (ed.ip.equals(addr)) {
                    return "http://" + ed.ip + ":" + ed.port + "/" + IEngine.MODELER_APPLICATION_ID;
                }
            }
        }
        return null;
    }
    
    /**
     * Return a list of all engines that were active during the passed amounts of seconds. Don't pass
     * less than 10 seconds if you want to catch all the active ones, unless the server broadcasters
     * were redefined to change the default interval.
     * 
     * @param secondsLastActive
     * @return
     */
    public List<String> getEngineUrls(int secondsLastActive) {
        long ms = new Date().getTime();
        List<String> ret = new ArrayList<>();
        synchronized(engines) {
            for (EngineData ed : engines.keySet()) {
                if ((ms - engines.get(ed)) < secondsLastActive * 1000) {
                    ret.add("http://" + ed.ip + ":" + ed.port + "/" + IEngine.MODELER_APPLICATION_ID);
                }
            }
        }
        return ret;
    }
    
    @Override
    protected void onSignal(String ipAddress, int port, long time) {
        synchronized (engines) {
            engines.put(new EngineData(ipAddress, port), time);
        }
    }

}
