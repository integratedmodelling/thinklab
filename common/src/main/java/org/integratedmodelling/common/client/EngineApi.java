package org.integratedmodelling.common.client;

import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;

import org.integratedmodelling.api.auth.IUser;
import org.integratedmodelling.api.runtime.ISession;
import org.integratedmodelling.common.auth.User;
import org.integratedmodelling.common.beans.auth.AuthenticationResultResource;
import org.integratedmodelling.common.network.API;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.databind.ObjectMapper;

public class EngineApi {

    class Template extends RestTemplate {
        
        ObjectMapper objectMapper = new ObjectMapper();
        
        public Template() {
            super();
            List<HttpMessageConverter<?>> messageConverters = new ArrayList<>();
            MappingJackson2HttpMessageConverter jsonMessageConverter = new MappingJackson2HttpMessageConverter();
            StringHttpMessageConverter utf8 = new StringHttpMessageConverter(Charset.forName("UTF-8"));
            jsonMessageConverter.setObjectMapper(objectMapper);
            messageConverters.add(jsonMessageConverter);
            messageConverters.add(utf8);
            setMessageConverters(messageConverters);
            
            /*
             * TODO setup error handling
             */
        }
    }

    String           url;
    IUser            user;
    ISession         session;
    private Template template;
    private HttpHeaders textHeaders;
    private HttpHeaders jsonHeaders;

    public EngineApi(String url) {
        
        this.url = url.endsWith("/") ? url.substring(0, url.lastIndexOf("/", 0)) : url;
        this.template = new Template();
        this.textHeaders = new HttpHeaders();
        this.textHeaders.setContentType(MediaType.TEXT_PLAIN);
        this.jsonHeaders = new HttpHeaders();
        this.jsonHeaders.setContentType(MediaType.APPLICATION_JSON);
    }

    public IUser authenticate(String certificate, boolean needExclusive) {
        AuthenticationResultResource bean = template
                .postForObject(getEndpoint(API.AUTHENTICATE), textHeaders(certificate), 
                        AuthenticationResultResource.class);
        if (bean != null && (!needExclusive || bean.isExclusive())) {
            return new User(bean);
        }
        return null;
    }

    private String getEndpoint(String service) {
        return url + (service.startsWith("/") ? "" : "/") + service + ".json";
    }

    // boilerplate
    
    HttpEntity<String> textHeaders(String body) {
        HttpEntity<String> entity = new HttpEntity<String>(body, textHeaders);
        return entity;
    }
    
    HttpEntity<String> textHeaders() {
        HttpEntity<String> entity = new HttpEntity<String>(textHeaders);
        return entity;
    }
    
    HttpEntity<String> jsonHeaders(String body) {
        HttpEntity<String> entity = new HttpEntity<String>(body, jsonHeaders);
        return entity;
    }
    
    HttpEntity<String> jsonHeaders() {
        HttpEntity<String> entity = new HttpEntity<String>(jsonHeaders);
        return entity;
    }
}
