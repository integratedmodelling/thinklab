package org.integratedmodelling.common.client;

import java.io.File;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.integratedmodelling.api.auth.IUser;
import org.integratedmodelling.api.engine.IModelingEngine;
import org.integratedmodelling.api.modelling.IDirectObserver;
import org.integratedmodelling.api.modelling.IExtent;
import org.integratedmodelling.api.modelling.IModelObject;
import org.integratedmodelling.api.modelling.INamespace;
import org.integratedmodelling.api.modelling.ISubject;
import org.integratedmodelling.api.monitoring.INotification;
import org.integratedmodelling.api.runtime.IClient;
import org.integratedmodelling.api.runtime.ISession;
import org.integratedmodelling.api.runtime.ITask;
import org.integratedmodelling.api.runtime.IWorkspace;
import org.integratedmodelling.common.configuration.KLAB;
import org.integratedmodelling.common.utils.FileUtils;
import org.integratedmodelling.exceptions.ThinklabException;
import org.integratedmodelling.exceptions.ThinklabIOException;
import org.integratedmodelling.exceptions.ThinklabInternalErrorException;

/**
 * Client implementation for k.LAB 1.0. 
 * 
 * @author ferdinando.villa
 *
 */
public class KlabClient implements IClient {

    /**
     * Sessions indexed by engine URL.
     */
    Map<String, ISession> sessions      = new HashMap<>();
    /*
     * The current engine URL.
     */
    String                currentEngine = null;
    IUser                 user          = null;
    EngineFinder          engineFinder  = null;
    private File certificate;
    private boolean needExclusive;
    private Messenger messenger;

    /**
     * The constructor just sticks out an antenna and starts listening for engines on the 
     * local network. Call boot() to establish connection.
     * 
     * @param certificate
     * @param needExclusive
     * @throws ThinklabException
     */
    public KlabClient(File certificate, boolean needExclusive) throws ThinklabException {
        engineFinder = new EngineFinder();
        this.certificate = certificate;
        this.needExclusive = needExclusive;
    }

    /**
     * Boot functions that require an engine connected.
     * @throws ThinklabException
     */
    protected void performNetworkBoot() throws ThinklabException {
        // TODO Auto-generated method stub

    }

    /**
     * Boot functions that don't require an engine to be connected. Redefine to add anything 
     * necessary - GUI setup etc.
     */
    protected void performLocalBoot() throws ThinklabException {

    }

    /**
     * Implements the default engine choosing logic: choose the local engine if there is one,
     * otherwise call selectEngine() on all 
     * @param needExclusive 
     * @param certificate 
     * @return
     */
    protected boolean connectToEngine(String certificate, boolean needExclusive) {
        String localEngine = engineFinder.getLocalEngineUrl();
        if (localEngine != null) {
            KLAB.info("attempting connection with local engine at " + localEngine);
            EngineApi api = new EngineApi(localEngine);

            IUser user = api.authenticate(certificate, needExclusive);
            if (user != null) {
                
                KLAB.info("user " + user.getUsername() + " authenticated; engine locking "
                        + (user.canLockEngine() ? "enabled" : "disabled"));

                currentEngine = localEngine;
                
                /*  
                 * create local session with user auth key and engine API. Recover previous with this
                 * engine if it already exists and make it current. Engine will have restored same 
                 * session if not expired.
                 */
                
                /*
                 * create messenger for duplex communication and connect it
                 */
                this.messenger = new Messenger(localEngine + "/message");
                return true;
            }
        }
        return false;
    }

    @Override
    public ITask observe(IDirectObserver objectName, IExtent... forceScale) throws ThinklabException {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public ITask observe(Object observable, ISubject context) throws ThinklabException {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public void shutdown() {
        // TODO Auto-generated method stub

    }

    /**
     * This will boot automatically when one or more engines come online. If more than one
     * engine is available, chooseEngine will be called and its result used. We can have independent
     * sessions active for different engines. Only engines that accept our identity through the passed
     * certificate are kept in the list.
     * 
     * If needExclusive is true, only engines that allow exclusive access will be selected for boot,
     * although others will remain available for explicit connection.
     * 
     * If an engine is available on the localhost and it's suitable for the needExclusive setting, it
     * will be automatically connected to, and user can 
     * 
     * @throws ThinklabIOException if certificate file cannot be read
     * 
     */    @Override
    public void boot() throws ThinklabException {
        performLocalBoot();
        try {
            /*
             * wait for connection
             */
            while (!connectToEngine(FileUtils.readFileToString(certificate), needExclusive)) {
                Thread.sleep(1000);
            }
        } catch (Exception e) {
            throw new ThinklabInternalErrorException(e);
        }
        performNetworkBoot();
    }

    @Override
    public IModelingEngine getEngine() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public IWorkspace getWorkspace() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public void reindexKnowledge() {
        // TODO Auto-generated method stub

    }

    @Override
    public void bookmark(IModelObject modelObject, String name, String description)
            throws ThinklabIOException {
        // TODO Auto-generated method stub

    }

    @Override
    public void run(File scriptFile) {
        // TODO Auto-generated method stub

    }

    @Override
    public Collection<INamespace> setScenario(INamespace scenario, boolean active) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public void resetScenarios() {
        // TODO Auto-generated method stub

    }

    @Override
    public List<INotification> getNotifications(ITask task) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public IUser getUser() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public ISession getCurrentSession() {
        return currentEngine == null ? null : sessions.get(currentEngine);
    }

    public static void main(String[] args) throws Exception {

        KlabClient client = new KlabClient(new File(System.getProperty("user.home") + File.separator
                + ".tl/im.cert"), true);
        client.boot();
        int n = 1;
        while (true) {
            try {
                Thread.sleep(1000);
//                KLAB.info("sending /control/ziocardillo " + n);
                client.messenger.send("/control/sperma", "" + (n++));
            } catch (InterruptedException ex) {
                Thread.currentThread().interrupt();
            }
        }
    }

}
