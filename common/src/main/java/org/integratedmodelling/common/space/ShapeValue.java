/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.common.space;

import org.integratedmodelling.api.modelling.ITopologicallyComparable;
import org.integratedmodelling.exceptions.ThinklabException;
import org.integratedmodelling.exceptions.ThinklabRuntimeException;
import org.integratedmodelling.exceptions.ThinklabUnsupportedOperationException;
import org.integratedmodelling.exceptions.ThinklabValidationException;

import com.vividsolutions.jts.geom.Coordinate;
import com.vividsolutions.jts.geom.Envelope;
import com.vividsolutions.jts.geom.Geometry;
import com.vividsolutions.jts.geom.GeometryFactory;
import com.vividsolutions.jts.geom.LineString;
import com.vividsolutions.jts.geom.MultiLineString;
import com.vividsolutions.jts.geom.MultiPoint;
import com.vividsolutions.jts.geom.MultiPolygon;
import com.vividsolutions.jts.geom.Point;
import com.vividsolutions.jts.geom.Polygon;
import com.vividsolutions.jts.io.ParseException;
import com.vividsolutions.jts.io.WKBReader;
import com.vividsolutions.jts.io.WKBWriter;
import com.vividsolutions.jts.io.WKTReader;
import com.vividsolutions.jts.io.WKTWriter;
import com.vividsolutions.jts.simplify.TopologyPreservingSimplifier;

public class ShapeValue implements
        IGeometricShape,
        ITopologicallyComparable<ShapeValue> {

    Geometry value;
    String   crsId = "EPSG:4326";
    String   crs   = null;
    Type     type  = null;

    public ShapeValue(Geometry value) {
        this.value = value;
    }

    public ShapeValue(String s) {
        parse(s);
    }

    /**
     * Construct a rectangular "cell" from two points.
     * @param x1
     * @param y1
     * @param x2
     * @param y2
     */
    public ShapeValue(double x1, double y1, double x2, double y2) {
        if (x1 == x2 && y1 == y2) {
            this.value = makePoint(x1, y1);
        } else {
            this.value = makeCell(x1, y1, x2, y2);
        }
    }

    public ShapeValue(double x1, double y1) {
        this.value = makePoint(x1, y1);
    }

    public ShapeValue(String s, String crs) throws ThinklabValidationException {
        parse(s);
    }

    /* create a polygon from the passed envelope */
    public ShapeValue(Envelope e) {

        GeometryFactory gFactory = new GeometryFactory();
        Coordinate[] pts = {
                new Coordinate(e.getMinX(), e.getMinY()),
                new Coordinate(e.getMaxX(), e.getMinY()),
                new Coordinate(e.getMaxX(), e.getMaxY()),
                new Coordinate(e.getMinX(), e.getMaxY()),
                new Coordinate(e.getMinX(), e.getMinY()) };

        value = gFactory.createPolygon(gFactory.createLinearRing(pts), null);
    }

    public static Geometry makeCell(double x1, double y1, double x2, double y2) {

        /**
         * FIXME
         * We should probably have a static one (checking thread safety) or store it somewhere; see how it works
         * for now. 
         */
        GeometryFactory gFactory = new GeometryFactory();

        Coordinate[] pts = {
                new Coordinate(x1, y1),
                new Coordinate(x2, y1),
                new Coordinate(x2, y2),
                new Coordinate(x1, y2),
                new Coordinate(x1, y1) };

        return gFactory.createPolygon(gFactory.createLinearRing(pts), null);
    }

    public static Geometry makePoint(double x1, double y1) {

        /**
         * FIXME
         * We should probably have a static one (checking thread safety) or store it somewhere; see how it works
         * for now. 
         */
        GeometryFactory gFactory = new GeometryFactory();
        Coordinate coordinate = new Coordinate(x1, y1);
        return gFactory.createPoint(coordinate);
    }

    public void parse(String s) {

        /*
         * first see if we start with a token that matches "EPSG:[0-9]*". If so,
         * set the CRS from it; otherwise it is null (not the plugin default).
         */
        if (s.startsWith("EPSG:") || s.startsWith("urn:")) {
            int n = s.indexOf(' ');
            this.crsId = s.substring(0, n);
            s = s.substring(n + 1);
        }
        try {
            if (s.contains("(")) {
                value = new WKTReader().read(s);
            } else {
                value = new WKBReader().read(WKBReader.hexToBytes(s));
            }
        } catch (ParseException e) {
            throw new ThinklabRuntimeException(e);
        }
    }

    @Override
    public String asText() {
        return crsId + " "
                + (value.isEmpty() ? "GEOMETRYCOLLECTION EMPTY" : getWKB());
    }

    @Override
    public String toString() {
        String ret = new WKTWriter().write(value);
        if (ret.endsWith(" EMPTY")) {
            // for DBs that do not support any other empty WKT
            ret = "GEOMETRYCOLLECTION EMPTY";
        }
        return ret;
    }

    @Override
    public Geometry getStandardizedGeometry() {
        return value;
    }

    @Override
    public Geometry getGeometry() {
        return value;
    }

    public void simplify(double tolerance) {
        value = TopologyPreservingSimplifier.simplify(value, tolerance);
    }

    public int getSRID(int def) {
        int ret = value.getSRID();
        if (ret <= 0)
            ret = def;
        return ret;
    }

    /**
     * Get the referenced bounding box of the shape using the normalized axis order.
     * @return
     */
    public Envelope getEnvelope() {
        return value.getEnvelopeInternal();
    }

    public String getCRS() {
        return crsId;
    }

    // @SuppressWarnings("unchecked")
    // public ShapeValue union(ShapeValue region) throws ThinklabException {
    //
    // if ((crs != null || region.crs != null) && !crs.equals(region.crs))
    // region = region.transform(crs);
    // Geometry gg = value.union(region.value);
    // if ((value instanceof Polygon || value instanceof MultiPolygon)
    // && !(gg instanceof Polygon || gg instanceof MultiPolygon)) {
    // Polygonizer p = new Polygonizer();
    // p.add(gg);
    // gg = value.getFactory().createMultiPolygon((Polygon[]) p.getPolygons().toArray(new Polygon[p
    // .getPolygons()
    // .size()]));
    // }
    // ShapeValue ret = new ShapeValue(gg);
    // ret.crs = crs;
    // return ret;
    // }
    //
    // public ShapeValue intersection(ShapeValue region) throws ThinklabException {
    //
    // if ((crs != null || region.crs != null) && !crs.equals(region.crs))
    // region = region.transform(crs);
    //
    // ShapeValue ret = new ShapeValue(value.intersection(region.value));
    // ret.crs = crs;
    // return ret;
    // }
    //
    // public ShapeValue difference(ShapeValue region) throws ThinklabException {
    //
    // if ((crs != null || region.crs != null) && !crs.equals(region.crs))
    // region = region.transform(crs);
    //
    // ShapeValue ret = new ShapeValue(value.difference(region.value));
    // ret.crs = crs;
    // return ret;
    // }

    public String getWKT() {
        return new WKTWriter().write(value);
    }

    public boolean isValid() {
        return value == null ? true : value.isValid();
    }

    // /**
    // * Return a new ShapeValue transformed to the passed CRS. Must have a crs.
    // *
    // * TODO this version only performs a no-op. Redefine in a derived class to
    // * provide projection support.
    // *
    // * @param ocrs the CRS to transform to.
    // * @return a new shapevalue
    // * @throws ThinklabException if we have no CRS or a transformation cannot be found.
    // */
    // public ShapeValue transform(CoordinateReferenceSystem ocrs) throws ThinklabException {
    //
    // if (crs == null)
    // throw new ThinklabValidationException("shape: cannot compute area of shape without CRS");
    //
    // if (ocrs.equals(this.crs))
    // return this;
    //
    // throw new ThinklabUnsupportedOperationException("cannot project shapes in the common version of
    // ShapeValue");
    //
    // }

    public String getWKB() {
        return new String(WKBWriter.bytesToHex(new WKBWriter().write(value)));
    }

    @Override
    public boolean contains(ShapeValue o) throws ThinklabException {
        throw new ThinklabUnsupportedOperationException("spatial operations unsupported in client version");
    }

    @Override
    public boolean overlaps(ShapeValue o) throws ThinklabException {
        throw new ThinklabUnsupportedOperationException("spatial operations unsupported in client version");
    }

    @Override
    public boolean intersects(ShapeValue o) throws ThinklabException {
        throw new ThinklabUnsupportedOperationException("spatial operations unsupported in client version");
    }

    @Override
    public ITopologicallyComparable<ShapeValue> union(ITopologicallyComparable<?> other)
            throws ThinklabException {
        return union(other);
    }

    @Override
    public ITopologicallyComparable<ShapeValue> intersection(ITopologicallyComparable<?> other)
            throws ThinklabException {
        return intersection(other);
    }

    @Override
    public double getCoveredExtent() {
        return value.getArea();
    }

    @Override
    public Type getGeometryType() {
        if (type == null) {
            if (value instanceof Polygon) {
                type = Type.POLYGON;
            } else if (value instanceof MultiPolygon) {
                type = Type.MULTIPOLYGON;
            } else if (value instanceof Point) {
                type = Type.POINT;
            } else if (value instanceof MultiLineString) {
                type = Type.MULTILINESTRING;
            } else if (value instanceof LineString) {
                type = Type.LINESTRING;
            } else if (value instanceof MultiPoint) {
                type = Type.MULTIPOINT;
            }
        }
        return type;
    }

    @Override
    public int getSRID() {
        return 4326;
    }

    @Override
    public double getArea() {
        return value.getArea();
    }

    @Override
    public boolean isEmpty() {
        return value.isEmpty();
    }

}
