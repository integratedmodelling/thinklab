/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.common.utils;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import org.integratedmodelling.exceptions.ThinklabIOException;
import org.integratedmodelling.exceptions.ThinklabRuntimeException;

/**
 * Simple log appender for debugging. Simplest way to use is Debug.print(....) which
 * will append the string to $HOME/debug.txt.
 * 
 * @author Ferd
 *
 */
public class Debug {

    private File         fname;
    static Debug         _debug;

    /**
     * set this to false to neuter the logger.
     */
    static final boolean DEBUG = true;

    public Debug(String file, boolean append) throws ThinklabIOException {

        if (DEBUG) {
            this.fname = new File(file);
            if (!append && this.fname.exists()) {
                this.fname.delete();
            }
        }
    }

    public void print(String s) {

        if (DEBUG) {
            try {
                FileWriter writer = new FileWriter(fname, true);
                writer.write(s + System.getProperty("line.separator"));
                writer.flush();
                writer.close();
            } catch (IOException e) {
                throw new ThinklabRuntimeException(e);
            }
        }
    }

    public static void println(String s) {

        if (DEBUG) {
            if (_debug == null) {
                try {
                    _debug = new Debug(System.getProperty("user.home") + File.separator + "debug.txt", true);
                } catch (ThinklabIOException e) {
                }
            }
            _debug.print(s);
        }
    }

    public static String describeValues(double[] vals) {

        if (vals == null)
            return "NULL input array";

        double min = Double.NaN;
        double max = Double.NaN;
        int total = 0, nans = 0;

        for (double d : vals) {
            if (Double.isNaN(d)) {
                nans++;
            } else {
                if (Double.isNaN(min) || min > d) {
                    min = d;
                }
                if (Double.isNaN(max) || max < d) {
                    max = d;
                }
            }
            total++;
        }

        return "#" + total + "(" + nans + " NaN): min = " + min + ", max = " + max;

    }
}
