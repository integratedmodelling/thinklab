/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.common.utils;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;

public class MapUtils extends org.apache.commons.collections.MapUtils {

    /**
     * Creates a regular hasmap with the objects passed taken in pairs, but does not
     * add a pair where the value object is null. Does not check that objects come in
     * even number, so use carefully. Also encodes booleans as "true" or "false" when
     * the key ends with ?.
     * 
     * @param o
     * @return
     */
    public static Map<?, ?> of(Object... o) {
        Map<Object, Object> ret = new HashMap<>();
        for (int i = 0; i < o.length; i += 2) {
            Object value = o[i + 1];
            if (o[i].toString().endsWith("?") && value instanceof Boolean) {
                value = (Boolean) value ? "true" : "false";
            }
            if (value != null) {
                ret.put(o[i], value);
            }
        }
        return ret;
    }

    public static String dump(Map<?, ?> map) {

        final ByteArrayOutputStream out = new ByteArrayOutputStream();
        final PrintStream outPrint = new PrintStream(out);

        MapUtils.debugPrint(outPrint, "Print Map", map);

        try {
            return out.toString("java.nio.charset.StandardCharsets.UTF_8");
        } catch (UnsupportedEncodingException e) {
            return "MAP PRINT ERROR: " + e.getMessage();
        }
    }

}
