package org.integratedmodelling.common.utils;

import java.lang.instrument.Instrumentation;

/*
 * not working. A mess to implement generally.
 */
public class MemoryUtils {

    private static Instrumentation instrumentation;

    public static void premain(String args, Instrumentation inst) {
        instrumentation = inst;
    }

    public static long getObjectSize(Object o) {
        return instrumentation.getObjectSize(o);
    }

}
