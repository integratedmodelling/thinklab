/*******************************************************************************
 *  Copyright (C) 2007, 2014:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.common.utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.Socket;
import java.net.URL;
import java.nio.channels.Channels;
import java.nio.channels.FileChannel;
import java.nio.channels.ReadableByteChannel;
import java.util.Properties;

import org.integratedmodelling.exceptions.ThinklabIOException;

public class URLUtils {

    /**
     * Ping the url by requesting the header and inspecting the return code.
     * 
     * @param url
     * @return
     */
    public static boolean ping(String url) {

        // < 100 is undertermined.
        // 1nn is informal (shouldn't happen on a GET/HEAD)
        // 2nn is success
        // 3nn is redirect
        // 4nn is client error
        // 5nn is server error

        HttpURLConnection connection = null;
        boolean ret = false;
        try {
            connection = (HttpURLConnection) new URL(url).openConnection();
            connection.setRequestMethod("HEAD");
            int responseCode = connection.getResponseCode();
            if (responseCode > 100 && responseCode < 400) {
                ret = true;
            }
        } catch (Exception e) {
        }
        return ret;
    }

    /**
     * Return true if the passed host (not URL) responds on port 80.
     * 
     * @param url
     * @return
     */
    public static boolean pingHost(String url) {
        Socket socket = null;
        boolean reachable = false;
        try {
            socket = new Socket(url, 80);
            reachable = true;
        } catch (Exception e) {
        } finally {
            if (socket != null)
                try {
                    socket.close();
                } catch (IOException e) {
                }
        }
        return reachable;
    }

    /**
     * Look for thinklab.resource.path in properties, if found scan
     * the path to resolve the passed name as a file url. If the url is already
     * resolved, just return it. If the path contains a http-based URL prefix
     * just use that without checking.
     * 
     * @param url
     * @param properties
     * @return a resolved url or the original one if not resolved.
     */
    public static String resolveUrl(String url, Properties properties) {

        String ret = url;

        if (ret.contains(":/"))
            return ret;

        String prop = ".";

        for (String path : prop.split(";")) {

            if (path.startsWith("http") && path.contains("/")) {
                ret = path + url;
                break;
            }

            File pth = new File(path + File.separator + url);

            if (pth.exists()) {
                try {
                    ret = pth.toURI().toURL().toString();
                    break;
                } catch (MalformedURLException e) {
                }
            }
        }

        return ret;
    }

    /**
     * Copy the given URL to the given local file, return number of bytes copied.
     * @param url the URL
     * @param file the File
     * @return the number of bytes copied.
     * @throws ThinklabIOException if URL can't be read or file can't be written.
     */
    public static long copy(URL url, File file) throws ThinklabIOException {
        long count = 0;
        int oneChar = 0;

        try {
            InputStream is = url.openStream();
            FileOutputStream fos = new FileOutputStream(file);

            while ((oneChar = is.read()) != -1) {
                fos.write(oneChar);
                count++;
            }

            is.close();
            fos.close();
        } catch (Exception e) {
            throw new ThinklabIOException(e.getMessage());
        }

        return count;
    }

    public static void copyChanneled(URL url, File file) throws ThinklabIOException {

        InputStream is = null;
        FileOutputStream fos = null;

        try {
            is = url.openStream();
            fos = new FileOutputStream(file);
            ReadableByteChannel rbc = Channels.newChannel(is);
            fos.getChannel().transferFrom(rbc, 0, Long.MAX_VALUE);
        } catch (Exception e) {
            throw new ThinklabIOException(e.getMessage());
        } finally {
            if (is != null)
                try {
                    is.close();
                } catch (IOException e) {
                }
            if (fos != null)
                try {
                    fos.close();
                } catch (IOException e) {
                }
        }
    }

    /**
     * Copy the given File to the given local file, return number of bytes copied.
     * @param url the URL
     * @param file the File
     * @return the number of bytes copied.
     * @throws ThinklabIOException if URL can't be read or file can't be written.
     */
    public static long copy(File url, File file) throws ThinklabIOException {
        long count = 0;

        try {
            InputStream is = new FileInputStream(url);
            FileOutputStream fos = new FileOutputStream(file);

            int oneChar;
            while ((oneChar = is.read()) != -1) {
                fos.write(oneChar);
                count++;
            }

            is.close();
            fos.close();
        } catch (Exception e) {
            throw new ThinklabIOException(e.getMessage());
        }

        return count;
    }

    public static void copyBuffered(File src, File dst) throws ThinklabIOException {

        try {
            InputStream in = new FileInputStream(src);
            OutputStream out = new FileOutputStream(dst);

            // Transfer bytes from in to out
            byte[] buf = new byte[1024];
            int len;
            while ((len = in.read(buf)) > 0) {
                out.write(buf, 0, len);
            }
            in.close();
            out.close();
        } catch (Exception e) {
            throw new ThinklabIOException(e.getMessage());
        }

    }

    public static File getFileForURL(URL url) throws ThinklabIOException {
        if (url.toString().startsWith("file:")) {
            return new File(UrlEscape.unescapeurl(url.getFile()));
        } else {
            File temp;
            try {
                temp = File.createTempFile("url", "url");
            } catch (IOException e) {
                throw new ThinklabIOException(e);
            }
            copy(url, temp);
            return temp;
        }
    }

//    public static void copyChanneled(File src, File dst) throws ThinklabIOException {
//
//        try {
//            // Create channel on the source
//            FileChannel srcChannel = new FileInputStream(src).getChannel();
//
//            // Create channel on the destination
//            FileChannel dstChannel = new FileOutputStream(dst).getChannel();
//
//            // Copy file contents from source to destination
//            dstChannel.transferFrom(srcChannel, 0, srcChannel.size());
//
//            // Close the channels
//            srcChannel.close();
//            dstChannel.close();
//
//        } catch (IOException e) {
//            throw new ThinklabIOException(e.getMessage());
//        }
//    }

}
