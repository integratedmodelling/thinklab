/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.common.utils.auth;

import java.io.File;
import java.io.RandomAccessFile;
import java.nio.channels.FileChannel;
import java.nio.channels.FileLock;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.io.FileUtils;
import org.integratedmodelling.common.auth.AuthConfig;
import org.integratedmodelling.exceptions.ThinklabRuntimeException;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.xml.StaxDriver;

/**
 * A synchronized catalog is a map that is automatically serialized to disk at each put(), and 
 * re-read from its serialized version before each get() if the file has been changed. As such,
 * many maps can look at the same file, even in different JVMs. 
 * 
 * Both K and V must be serializable.
 * 
 * @author Ferd
 *
 * @param <T>
 */
public class SynchronizedCatalog<K, V> extends HashMap<K, V> {

	private static final long serialVersionUID = 8364380832989989175L;
	
	private File _path;
	private long _timestamp;
	
	@Override
	public V get(Object key) {
		if (_path.exists() && _path.lastModified() > _timestamp) {
			try {
				read();
			} catch (Exception e) {
				throw new ThinklabRuntimeException(e);
			}
		}
		return super.get(key);
	}

	@Override
	public V put(K key, V value) {
		V ret = super.put(key, value);
		try {
			write();
		} catch (Exception e) {
			throw new ThinklabRuntimeException(e);
		}
		return ret;
	}

	// non-overridden put
	private void add(K key, V value) {
		super.put(key, value);
	}
	
	private void zap() {
		super.clear();
	}
	
    public void read() throws Exception {

    	exec(new Op() {
			
			@SuppressWarnings("unchecked")
			@Override
			public void run() throws Exception {
                XStream xstream = new XStream(new StaxDriver());
                Map<?,?> map = (Map<?, ?>) xstream.fromXML(FileUtils.readFileToString(_path));
                _timestamp = _path.lastModified();
                SynchronizedCatalog.this.zap();
                for (Object o : map.keySet()) {
                	SynchronizedCatalog.this.add((K)o, (V)map.get(o));
                }
			}
		});
    	
    }

    public void write() throws Exception {

    	exec(new Op() {			
			@Override
			public void run() throws Exception {
                File f = new File(AuthConfig.getSharedUserSpace() + File.separator + "collab.groups.xml");
                XStream xstream = new XStream(new StaxDriver());
                FileUtils.writeStringToFile(f, xstream.toXML(this));
                _timestamp = _path.lastModified();

			}
		});
    }
	
	public SynchronizedCatalog(String s) throws Exception {
		_path = new File(AuthConfig.getSharedUserSpace() + File.separator + s);
		if (_path.exists()) {
			read();
		}
	}
	
	private interface Op {
		void run() throws Exception; 
	}

	/**
	 * Exec operation using file lock. Tries to get exclusive hold of the file and
	 * blocks until it succeeds, which makes this suitable only for brief operations.
	 * 
	 * @param operation
	 * @throws Exception
	 */
	public void exec(Op operation) throws Exception {

        FileLock fileLock = null;
        try (RandomAccessFile file = new RandomAccessFile(_path + ".lck", "rw")) {
            FileChannel fileChannel = file.getChannel();

            fileLock = fileChannel.lock();
            if (fileLock != null){
            	operation.run();
            }
        } finally {
            if (fileLock != null){
                fileLock.release();
            }
        }
    }
	
}
