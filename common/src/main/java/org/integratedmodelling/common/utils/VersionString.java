/*******************************************************************************
 *  Copyright (C) 2007, 2014:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.common.utils;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * From a blog posting by Mark Space on comp.java.lang.programmer, rearranged a bit
 * 
 * @author Mark Space
 * 
 */
public class VersionString implements Comparable<VersionString> {

    private String version;

    public VersionString(String version) {
        this.version = version;
    }

    public static class Comparator implements java.util.Comparator<String> {

        public int compare(String s1, String s2) {
            if (s1 == null && s2 == null)
                return 0;
            else if (s1 == null)
                return -1;
            else if (s2 == null)
                return 1;
            String[] arr1 = s1.split("[^a-zA-Z0-9]+"), arr2 = s2.split("[^a-zA-Z0-9]+");
            int i1, i2, i3;
            for (int ii = 0, max = Math.min(arr1.length, arr2.length); ii <= max; ii++) {
                if (ii == arr1.length)
                    return ii == arr2.length ? 0 : -1;
                else if (ii == arr2.length)
                    return 1;
                try {
                    i1 = Integer.parseInt(arr1[ii]);
                } catch (Exception x) {
                    i1 = Integer.MAX_VALUE;
                }
                try {
                    i2 = Integer.parseInt(arr2[ii]);
                } catch (Exception x) {
                    i2 = Integer.MAX_VALUE;
                }
                if (i1 != i2) {
                    return i1 - i2;
                }
                i3 = arr1[ii].compareTo(arr2[ii]);
                if (i3 != 0)
                    return i3;
            }
            return 0;
        }

        public static void main(String[] ss) {

            String[] data = new String[] {
                    "2.0",
                    "1.5.1",
                    "10.1.2.0",
                    "9.0.0.0",
                    "2.0.0.16",
                    "1.6.0_07",
                    "1.6.0_07-b06",
                    "1.6.0_6",
                    "1.6.0_07-b07",
                    "1.6.0_08-a06",
                    "5.10",
                    "Generic_127127-11",
                    "Generic_127127-13" };
            List<String> list = Arrays.asList(data);
            Collections.sort(list, new Comparator());
            for (String s : list)
                System.out.println(s);
        }
    }

    @Override
    public String toString() {
        return version;
    }

    @Override
    protected Object clone() throws CloneNotSupportedException {
        return new VersionString(version);
    }

    @Override
    public boolean equals(Object obj) {
        return version.equals(obj.toString());
    }

    @Override
    public int hashCode() {
        return version.hashCode();
    }

    @Override
    public int compareTo(VersionString o) {
        return new Comparator().compare(this.version, o.version);
    }

}
