/*******************************************************************************
 *  Copyright (C) 2007, 2014:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.common.utils;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.integratedmodelling.api.auth.IUser;
import org.integratedmodelling.common.configuration.KLAB;

/**
 * Uses a lock file to establish a locking status. Locks have a user associated.
 * 
 * @author Ferd
 *
 */
public class Lock {

    final File _file;
    IUser      _user;
    boolean    _forced = false;

    public Lock(String fileName) {
        _file = new File(KLAB.CONFIG.getDataPath() + File.separator + fileName);
    }

    public IUser getLockingUser() {
        return _user;
    }

    public synchronized boolean lock(IUser user, boolean force) {

        if (force) {
            _user = user;
            _forced = true;
            return true;
        }

        synchronized (_file) {
            try {
                if (!_file.exists()) {
                    FileUtils.touch(_file);
                    _user = user;
                    KLAB.info("lock established for " + user);
                } else {
                    return false;
                }

            } catch (IOException e) {
                return false;
            }

            return true;
        }
    }

    public synchronized boolean unlock() {

        if (_forced) {
            _forced = false;
            return true;
        }

        synchronized (_file) {
            if (isLocked()) {
                FileUtils.deleteQuietly(_file);
                KLAB.info("lock from " + _user + " released");
                _user = null;
            }
            return !isLocked();
        }
    }

    public boolean isLocked() {

        if (_forced) {
            return true;
        }

        synchronized (_file) {
            return _file.exists();
        }
    }

}
