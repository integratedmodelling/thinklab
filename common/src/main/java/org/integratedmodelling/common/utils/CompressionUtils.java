/*******************************************************************************
 *  Copyright (C) 2007, 2014:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.common.utils;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.DoubleBuffer;
import java.nio.FloatBuffer;
import java.nio.IntBuffer;
import java.nio.LongBuffer;
import java.nio.ShortBuffer;
import java.util.BitSet;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.codec.binary.StringUtils;
import org.apache.commons.io.IOUtils;
import org.integratedmodelling.collections.Pair;
import org.integratedmodelling.common.configuration.KLAB;
import org.integratedmodelling.exceptions.ThinklabIOException;

import net.jpountz.lz4.LZ4BlockInputStream;
import net.jpountz.lz4.LZ4BlockOutputStream;

public class CompressionUtils {

    /**
     * Take an array of doubles and compress it to a Base64 string that can be POSTed 
     * to a server and decompressed on the fly.
     *
     * @param data
     * @return
     * @throws ThinklabIOException 
     */
    public static String compressToString(double[] data) throws ThinklabIOException {
        ByteBuffer buffer = ByteBuffer.allocate(data.length * (Double.SIZE / 8));
        buffer.asDoubleBuffer().put(data);
        try {
            return compressBytes(buffer.array());
        } catch (IOException e) {
            throw new ThinklabIOException(e);
        }
    }

    public static String compressToString(float[] data) throws ThinklabIOException {
        ByteBuffer buffer = ByteBuffer.allocate(data.length * (Float.SIZE / 8));
        buffer.asFloatBuffer().put(data);
        try {
            return compressBytes(buffer.array());
        } catch (IOException e) {
            throw new ThinklabIOException(e);
        }
    }

    public static String compressToString(int[] data) throws ThinklabIOException {
        ByteBuffer buffer = ByteBuffer.allocate(data.length * (Integer.SIZE / 8));
        buffer.asIntBuffer().put(data);
        try {
            return compressBytes(buffer.array());
        } catch (IOException e) {
            throw new ThinklabIOException(e);
        }
    }

    public static String compressToString(short[] data) throws ThinklabIOException {
        ByteBuffer buffer = ByteBuffer.allocate(data.length * (Integer.SIZE / 8));
        buffer.asShortBuffer().put(data);
        try {
            return compressBytes(buffer.array());
        } catch (IOException e) {
            throw new ThinklabIOException(e);
        }
    }

    public static String compressToString(BitSet data, BitSet nodata) throws ThinklabIOException {
        ByteBuffer buffer = ByteBuffer.allocate(data.size() * (Character.SIZE / 8));
        char[] cdata = new char[data.size()];
        for (int i = 0; i < data.size(); i++) {
            cdata[i] = nodata.get(i) ? 'X' : (data.get(i) ? 'Y' : 'N');
        }
        buffer.asCharBuffer().put(cdata);
        try {
            return compressBytes(buffer.array());
        } catch (IOException e) {
            throw new ThinklabIOException(e);
        }
    }

    public static Pair<BitSet, BitSet> decompressBooleans(String data) throws ThinklabIOException {
        ByteBuffer buffer = null;
        try {
            buffer = ByteBuffer.wrap(uncompressString(data));
        } catch (IOException e) {
            throw new ThinklabIOException(e);
        }
        CharBuffer cbuf = buffer.asCharBuffer();
        BitSet ondata = new BitSet(cbuf.length());
        BitSet nodata = new BitSet(cbuf.length());

        for (int i = 0; i < data.length(); i++) {
            char cc = cbuf.get(i);
            nodata.set(i, cc == 'X');
            ondata.set(i, cc == 'Y');
        }

        return new Pair<BitSet, BitSet>(ondata, nodata);
    }

    public static String compressToString(long[] data) throws ThinklabIOException {
        ByteBuffer buffer = ByteBuffer.allocate(data.length * (Long.SIZE / 8));
        buffer.asLongBuffer().put(data);
        try {
            return compressBytes(buffer.array());
        } catch (IOException e) {
            throw new ThinklabIOException(e);
        }
    }

    public static DoubleBuffer decompressDoubles(String data) throws ThinklabIOException {
        ByteBuffer buffer = null;
        try {
            buffer = ByteBuffer.wrap(uncompressString(data));
        } catch (IOException e) {
            throw new ThinklabIOException(e);
        }
        return buffer.asDoubleBuffer();
    }

    public static FloatBuffer decompressFloats(String data) throws ThinklabIOException {
        ByteBuffer buffer = null;
        try {
            buffer = ByteBuffer.wrap(uncompressString(data));
        } catch (IOException e) {
            throw new ThinklabIOException(e);
        }
        return buffer.asFloatBuffer();
    }

    public static IntBuffer decompressInts(String data) throws ThinklabIOException {
        ByteBuffer buffer = null;
        try {
            buffer = ByteBuffer.wrap(uncompressString(data));
        } catch (IOException e) {
            throw new ThinklabIOException(e);
        }
        return buffer.asIntBuffer();
    }

    public static ShortBuffer decompressShorts(String data) throws ThinklabIOException {
        ByteBuffer buffer = null;
        try {
            buffer = ByteBuffer.wrap(uncompressString(data));
        } catch (IOException e) {
            throw new ThinklabIOException(e);
        }
        return buffer.asShortBuffer();
    }

    public static LongBuffer decompressLongs(String data) throws ThinklabIOException {
        ByteBuffer buffer = null;
        try {
            buffer = ByteBuffer.wrap(uncompressString(data));
        } catch (IOException e) {
            throw new ThinklabIOException(e);
        }
        return buffer.asLongBuffer();
    }

    public static String encodeToString(double[] data) {
        ByteBuffer buffer = ByteBuffer.allocate(data.length * (Double.SIZE / 8));
        buffer.asDoubleBuffer().put(data);
        StringBuilder sb = new StringBuilder();
        sb.append(StringUtils.newStringUtf8(Base64.encodeBase64(buffer.array(), false)));
        return sb.toString();
    }

    public static double[] decodeDoubles(String data) {
        ByteBuffer buffer = ByteBuffer.wrap(Base64.decodeBase64(data));
        double[] ret = buffer.asDoubleBuffer().array();
        KLAB.info("decompressed " + ret.length + " data: some are " + ret[212] + ", " + ret[3442]);
        return ret;
    }

    public static int[] decodeInts(String data) {
        ByteBuffer buffer = ByteBuffer.wrap(Base64.decodeBase64(data));
        int[] ret = buffer.asIntBuffer().array();
        KLAB.info("decompressed " + ret.length + " data: some are " + ret[212] + ", " + ret[3442]);
        return ret;
    }

    public static long[] decodeLongs(String data) {
        ByteBuffer buffer = ByteBuffer.wrap(Base64.decodeBase64(data));
        long[] ret = buffer.asLongBuffer().array();
        KLAB.info("decompressed " + ret.length + " data: some are " + ret[212] + ", " + ret[3442]);
        return ret;
    }

    public static short[] decodeShorts(String data) {
        ByteBuffer buffer = ByteBuffer.wrap(Base64.decodeBase64(data));
        short[] ret = buffer.asShortBuffer().array();
        KLAB.info("decompressed " + ret.length + " data: some are " + ret[212] + ", " + ret[3442]);
        return ret;
    }

    public static String compressBytes(byte[] bytes) throws IOException {
        ByteArrayOutputStream rstBao = new ByteArrayOutputStream();
        LZ4BlockOutputStream zos = new LZ4BlockOutputStream(rstBao);
        zos.write(bytes);
        IOUtils.closeQuietly(zos);
        byte[] bts = rstBao.toByteArray();
        return Base64.encodeBase64String(bts);
    }

    public static byte[] uncompressString(String zippedBase64Str)
            throws IOException {
        byte[] result = null;
        byte[] bytes = Base64.decodeBase64(zippedBase64Str);
        InputStream zi = null;
        try {
            zi = new LZ4BlockInputStream(new ByteArrayInputStream(bytes));
            result = IOUtils.toByteArray(zi);
        } finally {
            IOUtils.closeQuietly(zi);
        }
        return result;
    }

}
