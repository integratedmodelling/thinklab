/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.common.utils;

import java.util.ArrayList;
import java.util.Collection;

import org.integratedmodelling.api.knowledge.IKnowledge;
import org.integratedmodelling.api.knowledge.IProperty;
import org.integratedmodelling.api.metadata.IMetadata;
import org.integratedmodelling.api.modelling.IEvent;
import org.integratedmodelling.api.modelling.INamespace;
import org.integratedmodelling.api.modelling.IObservable;
import org.integratedmodelling.api.modelling.IProcess;
import org.integratedmodelling.api.modelling.IScale;
import org.integratedmodelling.api.modelling.IScale.Locator;
import org.integratedmodelling.api.modelling.IState;
import org.integratedmodelling.api.modelling.IStructure;
import org.integratedmodelling.api.modelling.ISubject;
import org.integratedmodelling.api.monitoring.IMonitor;
import org.integratedmodelling.common.metadata.Metadata;
import org.integratedmodelling.common.vocabulary.Observable;
import org.integratedmodelling.exceptions.ThinklabException;

/**
 * Provides dummies for various things where we just need model objects to carry concepts in
 * remote services and the like.
 * 
 * @author Ferd
 *
 */
public class Dummy {

    public static ISubject subject(IKnowledge observable, IScale scale, INamespace namespace) {
        return new Subject(observable, scale, namespace);
    }

    static class Subject implements ISubject {

        Observable observable;
        IScale     scale;
        INamespace namespace;

        public Subject(IKnowledge observable, IScale scale, INamespace namespace) {
            this.observable = new Observable(observable);
            this.scale = scale;
            this.namespace = namespace;
        }

        @Override
        public IMetadata getMetadata() {
            return new Metadata();
        }

        @Override
        public IObservable getObservable() {
            return observable;
        }

        @Override
        public IScale getScale() {
            return scale;
        }

        @Override
        public Collection<IEvent> getEvents() {
            return new ArrayList<IEvent>();
        }

        //
        // @Override
        // public IKnowledge getDirectType() {
        // return observable.getType();
        // }
        //
        // @Override
        // public INamespace getNamespace() {
        // return namespace;
        // }
        //
        // @Override
        // public boolean is(Object other) {
        // // TODO Auto-generated method stub
        // return false;
        // }

        @Override
        public void setMonitor(IMonitor monitor) {
            // TODO Auto-generated method stub

        }

        @Override
        public IMonitor getMonitor() {
            // TODO Auto-generated method stub
            return null;
        }

        @Override
        public IStructure getStructure(Locator... locators) {
            // TODO Auto-generated method stub
            return null;
        }

        @Override
        public Collection<IState> getStates() {
            // TODO Auto-generated method stub
            return null;
        }

        @Override
        public Collection<ISubject> getSubjects() {
            // TODO Auto-generated method stub
            return null;
        }

        @Override
        public Collection<IProcess> getProcesses() {
            // TODO Auto-generated method stub
            return null;
        }

        @Override
        public String getId() {
            // TODO Auto-generated method stub
            return null;
        }

        @Override
        public IState getState(IObservable observable, Object... data) throws ThinklabException {
            // TODO Auto-generated method stub
            return null;
        }

        @Override
        public ISubject newSubject(IObservable observable, IScale scale, String name, IProperty relationship)
                throws ThinklabException {
            // TODO Auto-generated method stub
            return null;
        }

        @Override
        public IProcess newProcess(IObservable observable, IScale scale, String name)
                throws ThinklabException {
            // TODO Auto-generated method stub
            return null;
        }

        @Override
        public IEvent newEvent(IObservable observable, IScale scale, String name) throws ThinklabException {
            // TODO Auto-generated method stub
            return null;
        }

        @Override
        public IState getStaticState(IObservable observable) throws ThinklabException {
            // TODO Auto-generated method stub
            return null;
        }

        @Override
        public ISubject getSubject(IObservable obs) throws ThinklabException {
            // TODO Auto-generated method stub
            return null;
        }

        @Override
        public IEvent getEvent(IObservable obs) throws ThinklabException {
            // TODO Auto-generated method stub
            return null;
        }

        @Override
        public IProcess getProcess(IObservable obs) throws ThinklabException {
            // TODO Auto-generated method stub
            return null;
        }

        @Override
        public INamespace getNamespace() {
            // TODO Auto-generated method stub
            return null;
        }

        @Override
        public IKnowledge getType() {
            // TODO Auto-generated method stub
            return null;
        }

    }

}
