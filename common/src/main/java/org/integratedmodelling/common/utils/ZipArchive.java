/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
//package org.integratedmodelling.common.utils;
//
//import java.io.File;
//import java.io.FileInputStream;
//import java.io.FileOutputStream;
//import java.io.IOException;
//import java.util.ArrayList;
//import java.util.List;
//import java.util.zip.ZipEntry;
//import java.util.zip.ZipOutputStream;
//
//import org.integratedmodelling.exceptions.ThinklabIOException;
//
///**
// * The n-th zip class until Java provides something decent that does not require using ZipStreams and 
// * catching a thousand exceptions.
// * 
// * @author Ferd
// *
// */
//public class ZipArchive {
//
//    String       _folder;
//    List<String> _blacklist = new ArrayList<String>();
//    List<String> fileList   = new ArrayList<String>();
//
//    /**
//     * Pass the folder to be zipped plus any patterns for files or directories you don't want to include
//     * in the archive. Patterns are wildcard-based, unix-like, NOT regexps: for example, "*.git*" (ok, 
//     * cut and paste it).
//     * 
//     * @param folder
//     * @param blacklistedPatterns
//     */
//    public ZipArchive(File folder, String... blacklistedPatterns) {
//        _folder = folder.toString();
//        for (String s : blacklistedPatterns) {
//            _blacklist.add(s);
//        }
//        generateFileList(folder);
//    }
//
//    public void archive(File zipFile) throws ThinklabIOException
//    {
//        byte[] buffer = new byte[1024];
//        String source = "";
//        try {
//            try {
//                source = _folder.substring(_folder.lastIndexOf("/") + 1, _folder.length());
//            } catch (Exception e) {
//                source = _folder;
//            }
//
//            FileOutputStream fos = new FileOutputStream(zipFile);
//            ZipOutputStream zos = new ZipOutputStream(fos);
//
//            for (String file : this.fileList) {
//
//                ZipEntry ze = new ZipEntry(source + "/" + file);
//                zos.putNextEntry(ze);
//
//                FileInputStream in =
//                        new FileInputStream(_folder + "/" + file);
//
//                int len;
//                while ((len = in.read(buffer)) > 0) {
//                    zos.write(buffer, 0, len);
//                }
//
//                in.close();
//            }
//
//            zos.closeEntry();
//            zos.close();
//        } catch (IOException e) {
//            throw new ThinklabIOException(e);
//        }
//    }
//
//    public void generateFileList(File node) {
//
//        if (node.isFile() && !isBlacklisted(node)) {
//            fileList.add(generateZipEntry(node.toString()));
//        } else if (node.isDirectory() && !isBlacklisted(node)) {
//            String[] subNode = node.list();
//            for (String filename : subNode) {
//                generateFileList(new File(node, filename));
//            }
//        }
//    }
//
//    private boolean isBlacklisted(File node) {
//        for (String f : _blacklist) {
//            if (new WildcardMatcher().match(node.toString(), f)) {
//                return true;
//            }
//        }
//        return false;
//    }
//
//    private String generateZipEntry(String file) {
//        return file.substring(_folder.length() + 1, file.length());
//    }
// }
