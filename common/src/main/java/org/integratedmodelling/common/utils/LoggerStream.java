package org.integratedmodelling.common.utils;

/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.logging.Level;

import org.apache.log4j.Logger;

/** Wrap a Logger as a PrintStream.
    Useful mainly for porting code that previously
    logged to a System PrintStream or to a proxy.
    Calling LoggerStream.println()
    will call Logger.info() for Level.INFO.
    A call to flush() or to a println() method
    will flush previously written text, and
    complete a call to Logger.  You may be surprised
    by extra newlines, if you call print("\n")
    and flush() instead of println();

    @author W.S. Harlan, Landmark Graphics
 */
public class LoggerStream extends PrintStream {
    private Level                 _level  = null;
    private Logger                _logger = null;
    private ByteArrayOutputStream _baos   = null;

    /** Wrap a Logger as a PrintStream .
        @param logger Everything written to this PrintStream
        will be passed to the appropriate method of the Logger
        @param info This indicates which method of the
        Logger should be called.
     */
    public LoggerStream(Logger logger) {
        super(new ByteArrayOutputStream(), true);
        _baos = (ByteArrayOutputStream) (this.out);
        _logger = logger;
    }

    // from PrintStream
    @Override
    public synchronized void flush() {
        super.flush();
        if (_baos.size() == 0)
            return;
        String out1 = _baos.toString();

        _logger.info(out1);

        _baos.reset();
    }

    // from PrintStream
    @Override
    public synchronized void println() {
        flush();
    }

    // from PrintStream
    @Override
    public synchronized void println(Object x) {
        print(x); // flush already adds a newline
        flush();
    }

    // from PrintStream
    @Override
    public synchronized void println(String x) {
        print(x); // flush already adds a newline
        flush();
    }

    // from PrintStream
    @Override
    public synchronized void close() {
        flush();
        super.close();
    }

    // from PrintStream
    @Override
    public synchronized boolean checkError() {
        flush();
        return super.checkError();
    }

    // /** test code
    // @param args command line
    // */
    // public static void main(String[] args) {
    // Logger logger = Logger.getLogger("com.lgc.wsh.util");
    // PrintStream psInfo = new LoggerStream(logger);
    // // PrintStream psWarning = new LoggerStream(logger, Level.WARNING);
    // psInfo.print(3.);
    // psInfo.println("*3.=9.");
    // // if (false) {
    // // psWarning.print(3.);
    // // psWarning.println("*3.=9.");
    // // }
    // psInfo.print(3.);
    // psInfo.flush();
    // psInfo.println("*3.=9.");
    // psInfo.println();
    // psInfo.print("x");
    // psInfo.close();
    // }
}
