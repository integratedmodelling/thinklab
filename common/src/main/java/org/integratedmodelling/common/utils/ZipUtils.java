/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.common.utils;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Collection;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import net.lingala.zip4j.core.ZipFile;
import net.lingala.zip4j.exception.ZipException;
import net.lingala.zip4j.model.ZipParameters;
import net.lingala.zip4j.util.Zip4jConstants;

import org.apache.tools.zip.ZipUtil;
import org.integratedmodelling.exceptions.ThinklabException;
import org.integratedmodelling.exceptions.ThinklabIOException;

public class ZipUtils extends ZipUtil {

    /**
     * Create a zip with the passed directory's contents in it. The directory will be the top entry
     * in the file if storeDirectory is true.
     * 
     * @param zipFile
     * @param directory
     * @throws ThinklabException
     */
    public static void zip(File zipFile, File directory, boolean storeDirectory, boolean readHiddenFiles)
            throws ThinklabException {

        // dest = buildDestinationZipFilePath(srcFile, dest);
        ZipParameters parameters = new ZipParameters();
        parameters.setCompressionMethod(Zip4jConstants.COMP_DEFLATE); //
        parameters.setCompressionLevel(Zip4jConstants.DEFLATE_LEVEL_NORMAL); //
        parameters.setIncludeRootFolder(storeDirectory);
        parameters.setReadHiddenFiles(readHiddenFiles);
        // if (!StringUtils.isEmpty(passwd)) {
        // parameters.setEncryptFiles(true);
        // parameters.setEncryptionMethod(Zip4jConstants.ENC_METHOD_STANDARD); //
        // parameters.setPassword(passwd.toCharArray());
        // }
        try {
            ZipFile zipF = new ZipFile(zipFile);
            zipF.createZipFileFromFolder(directory, parameters, false, 0);
        } catch (ZipException e) {
            throw new ThinklabIOException(e);
        }
    }

    /**
     * Unzip the contents of the zip file in the passed destination directory. If the directory
     * does not exist, create it.
     * 
     * @param zipFile
     * @param directory
     * @throws ThinklabException
     */
    public static void unzip(File zipFile, File destDir) throws ThinklabException {

        ZipFile zFile;
        try {
            zFile = new ZipFile(zipFile);
            if (!zFile.isValidZipFile()) {
                throw new ThinklabIOException("file " + zipFile + " is not a valid archive");
            }

            if (!destDir.exists()) {
                destDir.mkdirs();
            }
            // if (zFile.isEncrypted()) {
            // zFile.setPassword(passwd.toCharArray());
            // }
            zFile.extractAll(destDir.toString());
        } catch (ZipException e) {
            throw new ThinklabIOException(e);
        }
    }

    public static void extractDirectories(File zipFilePath, File destinationPath, Collection<String> pathsWanted)
            throws ThinklabException {
        extractDirectories(zipFilePath, destinationPath, pathsWanted.toArray(new String[pathsWanted.size()]));
    }

    public static void extractDirectories(File zipFilePath, File destinationPath, String... pathsWanted)
            throws ThinklabException {

        destinationPath.mkdirs();

        ZipInputStream zis = null;
        try {

            zis = new ZipInputStream(new FileInputStream(zipFilePath));
            ZipEntry entry;

            while ((entry = zis.getNextEntry()) != null) {

                boolean ok = false;

                for (String ss : pathsWanted) {
                    if (entry.getName().startsWith(ss)) {
                        ok = true;
                        break;
                    }
                }

                if (!ok) {
                    continue;
                }

                File entryFile = new File(destinationPath, entry.getName());
                if (entry.isDirectory()) {

                    if (!entryFile.exists()) {
                        entryFile.mkdirs();
                    }

                } else {
                    copy(zis, entryFile);
                }
            }
        } catch (IOException e) {
            throw new ThinklabIOException(e);
        } finally {
            closeQuietly(zis);
        }
    }

    private static void closeQuietly(ZipInputStream zis) {
        try {
            zis.close();
        } catch (IOException e) {
        }
    }

    /*
     * copy istream to file; do not close the istream
     */
    private static int copy(InputStream iStream, File entryFile) throws IOException {

        // Make sure all folders exists (they should, but the safer, the better ;-))
        if (entryFile.getParentFile() != null && !entryFile.getParentFile().exists()) {
            entryFile.getParentFile().mkdirs();
        }

        // Create file on disk...
        if (!entryFile.exists()) {
            entryFile.createNewFile();
        }

        // Save InputStream to the file.
        BufferedOutputStream fOut = null;
        int bytes = 0;
        try {
            try {
                fOut = new BufferedOutputStream(new FileOutputStream(entryFile));
                byte[] buffer = new byte[32 * 1024];
                int bytesRead = 0;
                if (iStream != null) {
                    while ((bytesRead = iStream.read(buffer)) != -1) {
                        fOut.write(buffer, 0, bytesRead);
                        bytes += bytesRead;
                    }
                }
            } catch (Exception e) {
                throw new IOException("writeToFile failed, got: " + e.toString());
            } finally {
                fOut.close();
            }
        } catch (Exception e) {
            throw new IOException(e);
        }

        return bytes;
    }

    public static int copyAndClose(InputStream iStream, File entryFile) throws IOException {

        // Make sure all folders exists (they should, but the safer, the better ;-))
        if (entryFile.getParentFile() != null && !entryFile.getParentFile().exists()) {
            entryFile.getParentFile().mkdirs();
        }

        // Create file on disk...
        if (!entryFile.exists()) {
            entryFile.createNewFile();
        }

        // Save InputStream to the file.
        BufferedOutputStream fOut = null;
        int bytes = 0;
        try {
            try {
                fOut = new BufferedOutputStream(new FileOutputStream(entryFile));
                byte[] buffer = new byte[32 * 1024];
                int bytesRead = 0;
                if (iStream != null) {
                    while ((bytesRead = iStream.read(buffer)) != -1) {
                        fOut.write(buffer, 0, bytesRead);
                        bytes += bytesRead;
                    }
                }
            } catch (Exception e) {
                throw new IOException("writeToFile failed, got: " + e.toString());
            } finally {
                if (iStream != null)
                    iStream.close();
                fOut.close();
            }
        } catch (Exception e) {
            throw new IOException(e);
        }

        return bytes;
    }

}
