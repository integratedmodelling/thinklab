/*******************************************************************************
 *  Copyright (C) 2007, 2014:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.common.geospace;

import java.awt.geom.Area;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Properties;

import org.integratedmodelling.common.xml.XMLDocument;
import org.integratedmodelling.exceptions.ThinklabException;
import org.integratedmodelling.exceptions.ThinklabInternalErrorException;
import org.integratedmodelling.exceptions.ThinklabValidationException;
import org.w3c.dom.Node;

/**
 * Initial porting of read-only WCS interface - to be adapted.
 * @author Ferd
 *
 */
public class WCSCoverage {

    public static final String WCS_SERVICE_PROPERTY = "wcs.service.url";
    public static final String WCS_FORMAT_PROPERTY  = "wcs.service.format";

    String                     wcsService           = "http://127.0.0.1:8080/geoserver/wcs";
    String                     wcsFormat            = "geotiff";
    String                     layerName;
    double[]                   noData;

    String                     description          = null;
    // read from kvp in coverage keywords
    Properties                 properties           = new Properties();

    public WCSCoverage(String serviceID, String coverageID) throws ThinklabException {

        this.wcsService = serviceID;
        this.layerName = coverageID;

        URL durl = buildDescribeUrl(coverageID);
        XMLDocument desc = new XMLDocument(durl);
        parseDescriptor(desc);

    }

    /**
     * This constructor reads the WCS coverage descriptor and initializes all fields from it. Data are
     * not loaded until loadData(), so the coverage is null.
     * 
     * @param url
     * @param properties should contain the URL of the WCS service; if null, geoserver on
     * localhost:8080 is used (not elegant, but OK for now).
     */
    public WCSCoverage(String coverageID, Properties properties) throws ThinklabException {

        layerName = coverageID;
        URL durl = buildDescribeUrl(coverageID);

        XMLDocument desc = new XMLDocument(durl);
        parseDescriptor(desc);

    }

    /**
     * This constructor creates the coverage by reading the WCS coverage passed from the
     * associated WCS service, reading data only for the specified extent.
     * 
     * @param coverage
     * @param arealExtent
     */
    public WCSCoverage(WCSCoverage coverage, Area extent) {
        layerName = coverage.layerName;
    }

    private void parseDescriptor(XMLDocument desc) throws ThinklabException {

        String[] dimSpecs = new String[2];

        Node exc = desc.findNode("ServiceException");
        if (exc != null) {
            String em = XMLDocument.getNodeValue(exc);
            throw new ThinklabValidationException("WCS: " + em);
        }

        // desc.dump(System.out);

        Node n = desc.findNode("gml:Envelope");

        String srs = XMLDocument.getAttributeValue(n, "srsName").trim();

        // TODO read metadata: description, label, keywords

        int i = 0;
        Node child;
        Node next = (Node) n.getFirstChild();
        while ((child = next) != null) {

            next = child.getNextSibling();
            if (child.getNodeName().equals("gml:pos"))
                dimSpecs[i++] = XMLDocument.getNodeValue(child);
        }

        /*
         * process dims
         */
        double x1, y1, x2, y2;
        String[] z = dimSpecs[0].split("\\ ");
        x1 = Double.parseDouble(z[0]);
        y1 = Double.parseDouble(z[1]);
        z = dimSpecs[1].split("\\ ");
        x2 = Double.parseDouble(z[0]);
        y2 = Double.parseDouble(z[1]);

        n = desc.findNode("gml:GridEnvelope");
        i = 0;
        next = (Node) n.getFirstChild();
        while ((child = next) != null) {

            next = child.getNextSibling();
            if (child.getNodeName().equals("gml:low"))
                dimSpecs[0] = XMLDocument.getNodeValue(child).trim();
            else if (child.getNodeName().equals("gml:high"))
                dimSpecs[1] = XMLDocument.getNodeValue(child).trim();
        }

        /*
         * process pixel size
         */
        int sx1, sy1, sx2, sy2;
        z = dimSpecs[0].split("\\ ");
        sx1 = Integer.parseInt(z[0]);
        sy1 = Integer.parseInt(z[1]);
        z = dimSpecs[1].split("\\ ");
        sx2 = Integer.parseInt(z[0]);
        sy2 = Integer.parseInt(z[1]);

        /*
         * TODO process available formats and extract default or validate given
         */
        // this.crs = Geospace.getCRSFromID(srs);

        /*
         *  read no data values unless they were overridden in initial specifications. 
         */
        n = desc.findNode("nullValues", "wcs");
        if (noData == null && n != null) {

            next = (Node) n.getFirstChild();
            while ((child = next) != null) {
                next = child.getNextSibling();
                if (child.getNodeName().endsWith("singleValue")) {
                    this.noData = new double[1];
                    this.noData[0] = Double.parseDouble(XMLDocument.getNodeValue(child).toString());
                }
            }
        }

        /*
         * keywords: recognize KVP and instantiate properties from them
         */
        n = desc.findNode("keywords", "wcs");
        i = 0;
        next = (Node) n.getFirstChild();
        while ((child = next) != null) {

            next = child.getNextSibling();
            if (child.getNodeName().endsWith("keyword")) {
                String kw = XMLDocument.getNodeValue(child).trim();
                String[] zoz = kw.split("\\ ");
                for (String kz : zoz) {
                    if (kz.contains("=")) {
                        String[] kvp = kz.split("=");
                        if (kvp.length == 2)
                            properties.put(kvp[0], kvp[1]);
                    }
                }
            }
        }
        //
        // this.xCellSize = (x2 - x1)/(sx2 - sx1);
        // this.yCellSize = (y2 - y1)/(sy2 - sy1);
        //
        // this.boundingBox = new ReferencedEnvelope(x1, x2, y1, y2, crs);
        //
        // this.gridGeometry =
        // new GridGeometry2D(
        // new GeneralGridEnvelope( new int[] {sx1, sy1}, new int[] {sx2, sy2}, false),
        // boundingBox);

    }

    private URL buildDescribeUrl(String coverageId) throws ThinklabInternalErrorException {

        URL url = null;
        try {
            url = new URL(wcsService + "?service=WCS&version=1.0.0&request=DescribeCoverage&coverage="
                    + coverageId);
        } catch (MalformedURLException e) {
            throw new ThinklabInternalErrorException(e);
        }

        return url;
    }

    // private URL buildRetrieveUrl(Grid extent) throws ThinklabException {
    //
    // URL url = null;
    // String rcrs = Geospace.getCRSIdentifier(extent.getCRS(), false);
    //
    // int xc = extent.getXCells();
    // int yc = extent.getYCells();
    //
    // if (extent.getXCells() == 0 && extent.getYCells() == 0) {
    //
    // xc = (int)Math.ceil((extent.getEast() - extent.getWest())/this.xCellSize);
    // yc = (int)Math.ceil((extent.getNorth() - extent.getSouth())/this.yCellSize);
    //
    // System.out.println("computed raster size is " + xc + " x " + yc);
    // extent.setResolution(xc, yc);
    // }
    //
    // String s =
    // wcsService +
    // "?service=WCS&version=1.0.0&request=GetCoverage&coverage=" +
    // layerName +
    // "&bbox=" +
    // extent.getWest() +
    // "," +
    // extent.getSouth() +
    // "," +
    // extent.getEast() +
    // "," +
    // extent.getNorth() +
    // "&crs=" +
    // rcrs +
    // "&responseCRS=" +
    // rcrs +
    // "&width=" +
    // xc +
    // "&height=" +
    // yc +
    // "&format=" +
    // wcsFormat;
    //
    // try {
    // url = new URL(s);
    // } catch (MalformedURLException e) {
    // throw new ThinklabInternalErrorException(e);
    // }
    //
    // System.out.println("WCS URL: " + url);
    //
    // return url;
    // }

}
