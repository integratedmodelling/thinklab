/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.common.geospace;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Map;

import org.integratedmodelling.common.engine.JSONdeserializer;
import org.integratedmodelling.common.xml.XMLDocument;
import org.integratedmodelling.exceptions.ThinklabException;
import org.integratedmodelling.exceptions.ThinklabValidationException;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

import com.vividsolutions.jts.geom.Coordinate;

/**
 * Basic capabilities parsers for WFS and WCS. Just list layers and their descriptions.
 * 
 * @author Ferd
 *
 */
public class SpatialServices {

    /**
     * Return the approximate coordinate from the user's IP, assuming there is a network connection and 
     * telize.com is reachable and working. Return null otherwise. Longitude = x in the returned coordinate. 
     * 
     * @return
     */
    public static Coordinate geolocate() {

        Object ret = null;
        BufferedReader in = null;
        try {
            URL whatismyip = new URL("http://checkip.amazonaws.com");
            in = new BufferedReader(new InputStreamReader(whatismyip.openStream()));
            String ip = in.readLine();
            ret = JSONdeserializer.get("http://freegeoip.net/json/" + ip);
        } catch (Throwable e) {
        } finally {
            if (in != null) {
                try {
                    in.close();
                } catch (IOException e) {
                }
            }
        }

        if (ret instanceof Map<?, ?> && ((Map<?, ?>) ret).containsKey("latitude")) {
            return new Coordinate(Double.parseDouble(((Map<?, ?>) ret).get("longitude").toString()), Double
                    .parseDouble(((Map<?, ?>) ret).get("latitude").toString()));
        }

        return null;
    }

    public static class Layer {

        public Layer(String service, String covId, String covTitle, String covAbstract) {
            this.service = service;
            name = covId;
            title = covTitle == null ? "" : covTitle;
            description = covAbstract == null ? "" : covAbstract;
        }

        public String service;
        public String name;
        public String title;
        public String description;
    }

    static public Collection<Layer> listWCS(String url) throws ThinklabException {

        ArrayList<Layer> ret = new ArrayList<SpatialServices.Layer>();
        XMLDocument cap = null;

        try {
            cap = new XMLDocument(new URL(url + "?service=WCS&version=1.0.0&request=getCapabilities"));
        } catch (MalformedURLException e) {
            throw new ThinklabValidationException(e);
        }

        Node n = cap.findNode("ContentMetadata", "wcs");

        int i = 0;
        Node child;
        Node next = n.getFirstChild();
        while ((child = next) != null) {
            next = child.getNextSibling();
            if (child.getNodeName().endsWith("CoverageOfferingBrief")) {

                String covId = XMLDocument.getTextValue((Element) child, "name", "wcs");
                String covTitle = XMLDocument.getTextValue((Element) child, "label", "wcs");
                String covAbstract = XMLDocument.getTextValue((Element) child, "description", "wcs");

                ret.add(new Layer(url, covId, covTitle, covAbstract));
            }
        }

        return ret;
    }

    static public Collection<Layer> listWFS(String url) throws ThinklabException {

        ArrayList<Layer> ret = new ArrayList<SpatialServices.Layer>();
        XMLDocument cap = null;

        try {
            cap = new XMLDocument(new URL(url + "?service=WFS&version=1.0.0&request=getCapabilities"));
        } catch (MalformedURLException e) {
            throw new ThinklabValidationException(e);
        }

        Node n = cap.findNode("FeatureTypeList", "wfs");

        int i = 0;
        Node child;
        Node next = n.getFirstChild();
        while ((child = next) != null) {

            next = child.getNextSibling();
            if (child.getNodeName().endsWith("FeatureType")) {

                String covId = XMLDocument.getTextValue((Element) child, "Name", "wfs");
                String covTitle = XMLDocument.getTextValue((Element) child, "Title", "wfs");
                String covAbstract = XMLDocument.getTextValue((Element) child, "Abstract", "wfs");

                ret.add(new Layer(url, covId, covTitle, covAbstract));
            }
        }

        return ret;
    }

}
