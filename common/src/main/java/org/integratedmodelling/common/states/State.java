/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.common.states;

import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.integratedmodelling.api.knowledge.IKnowledge;
import org.integratedmodelling.api.metadata.IMetadata;
import org.integratedmodelling.api.modelling.IDirectObservation;
import org.integratedmodelling.api.modelling.IObservable;
import org.integratedmodelling.api.modelling.IObserver;
import org.integratedmodelling.api.modelling.IScale;
import org.integratedmodelling.api.modelling.IScale.Index;
import org.integratedmodelling.api.modelling.IState;
import org.integratedmodelling.api.modelling.storage.IDataset;
import org.integratedmodelling.api.modelling.storage.IStorage;
import org.integratedmodelling.api.space.ISpatialExtent;
import org.integratedmodelling.api.time.ITemporalExtent;
import org.integratedmodelling.common.configuration.KLAB;
import org.integratedmodelling.common.metadata.Metadata;
import org.integratedmodelling.common.storage.ConstStorage;
import org.integratedmodelling.common.storage.MemoryDataset;
import org.integratedmodelling.common.utils.NameGenerator;
import org.integratedmodelling.common.vocabulary.NS;
import org.integratedmodelling.lang.IRemoteSerializable;

public class State implements IState, IRemoteSerializable {

    IObservable        observable;
    IScale             scale;
    IStorage<?>        storage;
    IDirectObservation parent;
    IMetadata          metadata   = new Metadata();
    boolean            isRaw      = false;
    String             internalId = NameGenerator.shortUUID();

    /**
     * Create a state with memory storage.
     * 
     * @param observable
     * @param scale
     * @param observer
     */
    public State(IObservable observable, IScale scale, boolean isDynamic,
            IDirectObservation context) {
        this(observable, scale, null, isDynamic, context);
    }

    @Override
    public IKnowledge getType() {
        return getObservable().getType();
    }

    @Override
    public String toString() {
        return "ST/" + getObservable();
    }

    /**
     * Create a state potentially with dataset-backed storage, taking data from an existing object.
     * 
     * @param observable
     * @param scale
     * @param observer
     * @param dataset
     */
    public State(Object data, IObservable observable, IScale scale, IDataset dataset, boolean isDynamic,
            IDirectObservation context) {
        this.observable = observable;
        this.parent = context;
        setScale(scale, isDynamic);
        if (isScalar(data)) {
            this.storage = new ConstStorage(data, scale);
        } else {
            if (dataset instanceof MemoryDataset) {
                this.storage = dataset.getStorage(observable, isDynamic);
            } else {
                this.storage = KLAB.MFACTORY
                        .createStorage(observable.getObserver(), scale, dataset, isDynamic);
            }
            this.storage.set(data);
        }
    }

    /**
     * Create a state with dataset-backed storage.
     * 
     * @param observable
     * @param scale
     * @param observer
     * @param dataset
     */
    public State(IObservable observable, IScale scale, IDataset dataset, boolean isDynamic,
            IDirectObservation context) {
        this.observable = observable;
        this.parent = context;
        setScale(scale, isDynamic);
        if (dataset instanceof MemoryDataset) {
            this.storage = dataset.getStorage(observable, isDynamic);
        } else {
            this.storage = KLAB.MFACTORY.createStorage(observable.getObserver(), scale, dataset, isDynamic);
        }
    }

    /**
     * Create a state to only serve one object without any waste of memory. If the 
     * object is not a scalar, try to build appropriate storage based on the scale.
     * 
     * @param value
     * @param observable
     * @param scale
     * @param observer
     */
    public State(Object data, IObservable observable, IScale scale, boolean isDynamic) {
        this.observable = observable;
        setScale(scale, isDynamic);
        if (isScalar(data)) {
            this.storage = new ConstStorage(data, scale);
        } else {
            this.storage = KLAB.MFACTORY.createStorage(observable.getObserver(), scale, null, isDynamic);
            this.storage.set(data);
        }
    }

    private void setScale(IScale scale, boolean isDynamic) {
        // not dynamic: remove time from scale if it's there.
        if (!isDynamic && scale.getTime() != null && scale.getTime().isTemporallyDistributed()) {
            scale = scale.getSubscale(KLAB.c(NS.TIME_DOMAIN), -1);
        }
        this.scale = scale;
    }

    public IDirectObservation getContext() {
        return parent;
    }

    /*
     * non-public API. These could all be metadata without specialized methods, although using them
     * is way messier, so they stand for now.
     */

    public static boolean isScalar(Object data) {
        return !(data.getClass().isArray() || data instanceof Collection);
    }

    public String getInternalId() {
        return internalId;
    }

    /*
     * metadata to check if state is the direct output of a data source.
     */
    public boolean isRaw() {
        return isRaw;
    }

    public void setRaw(boolean raw) {
        isRaw = raw;
    }

    @Override
    public IMetadata getMetadata() {
        return metadata;
    }

    @Override
    public IObservable getObservable() {
        return observable;
    }

    @Override
    public IScale getScale() {
        return scale;
    }

    @Override
    public long getValueCount() {
        return scale.getMultiplicity();
    }

    @Override
    public IObserver getObserver() {
        return observable.getObserver();
    }

    @Override
    public boolean isSpatiallyDistributed() {
        return scale.getSpace() != null && scale.getSpace().getMultiplicity() > 1;
    }

    @Override
    public boolean isTemporallyDistributed() {
        return scale.getTime() != null && scale.getTime().getMultiplicity() > 1;
    }

    @Override
    public boolean isTemporal() {
        return scale.getTime() != null;
    }

    @Override
    public boolean isSpatial() {
        return scale.getSpace() != null;
    }

    @Override
    public ISpatialExtent getSpace() {
        return scale.getSpace();
    }

    @Override
    public ITemporalExtent getTime() {
        return scale.getTime();
    }

    @Override
    public Object getValue(int index) {
        return storage.get(index);
    }

    @Override
    public Iterator<?> iterator(Index index) {
        return new It(index.iterator());
    }

    @Override
    public IStorage<?> getStorage() {
        return storage;
    }

    /**
     * used in Groovy code to get the value if possible. TODO use
     * some meaningful on-demand aggregation for multiple values; see
     * what to do with categories and distributions.
     */
    public Object getValue() {
        return getValueCount() == 1 ? getValue(0) : "[multiple values]";
    }

    // / ===================================

    class It implements Iterator<Object> {

        Iterator<Integer> _sit;

        It(Iterator<Integer> sit) {
            _sit = sit;
        }

        @Override
        public boolean hasNext() {
            return _sit.hasNext();
        }

        @Override
        public Object next() {
            return getValue(_sit.next());
        }

        @Override
        public void remove() {
        }

    }

    @Override
    public Object adapt() {
        Map<String, Object> ret = new HashMap<String, Object>();

        ret.put("observer", getObserver());
        ret.put("observable", getObservable());
        ret.put("internal-id", internalId);
        ret.put("dynamic", storage.isDynamic() ? "true" : "false");
        ret.put("constant", isConstant() ? "true" : "false");
        Object latestValue = storage.getLatestAggregatedValue();
        if (latestValue != null) {
            ret.put("latest-value", latestValue);
        }

        return ret;
    }

    @Override
    public boolean isConstant() {
        return storage instanceof ConstStorage;
    }

}
