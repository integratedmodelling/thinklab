/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.common.owl;

import org.integratedmodelling.api.knowledge.IConcept;
import org.integratedmodelling.api.knowledge.IKnowledge;
import org.integratedmodelling.api.lang.IParseable;
import org.integratedmodelling.api.modelling.INamespace;
import org.integratedmodelling.common.configuration.KLAB;
import org.integratedmodelling.common.vocabulary.NS;

/**
 * The only functions this provides are the parsing ones, which separate out traits and
 * concepts/properties so that we avoid problems from concepts being legitimate but not
 * having been seen at one side of two communicating endpoints that adopt the same 
 * ontologies. When this situation can happen, send ((IParseable)k).asText() and
 * receive k = Knowledge.parse(string). 
 * 
 * This will allow to send, say, im.geography:AverageMinimumTemperature as 
 *  
 *  im.geography:AverageMinimumTemperature=im.geography:Temperature+im.Average+im:Minimum
 * 
 * and have it reassembled from the traits as im.geography:AverageMinimumTemperature if 
 * the latter has not been seen and created already.
 * 
 * TODO add methods to browse the asserted hierarchy and specialize them in derived
 * classes.
 * 
 * @author ferdinando.villa
 *
 */
public abstract class Knowledge implements IKnowledge, IParseable {

    public static IKnowledge parse(String string) {

        if (string == null || string.isEmpty()) {
            return null;
        }

        if (!string.contains("=")) {
            return KLAB.KM.getKnowledge(string);
        }

        String[] ct = string.split("=");
        IKnowledge base = KLAB.KM.getKnowledge(ct[0]);
        if (base != null) {
            return base;
        }

        return NS.reconstructDerivedConcept(ct[0], ct[1]);
    }

    @Override
    public String asText() {

        String ret = this.toString();
        if (NS.isDerived(this)) {
            ret += "=" + NS.getDefinition(this);
        }
        return ret;
    }

    @Override
    public boolean equals(Object obj) {
        return obj instanceof Knowledge ? toString().equals(obj.toString())
                : (obj instanceof String ? ((String) obj).equals(toString())
                        : false);
    }

    @Override
    public int hashCode() {
        return toString().hashCode();
    }

    @Override
    public IConcept getDomain() {
        INamespace ns = KLAB.MMANAGER.getNamespace(getConceptSpace());
        return ns == null ? null : ns.getDomain();
    }

}
