/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.common.owl;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import org.integratedmodelling.api.knowledge.IConcept;
import org.integratedmodelling.api.metadata.IMetadata;
import org.integratedmodelling.common.vocabulary.NS;
import org.semanticweb.owlapi.model.OWLAnnotation;
import org.semanticweb.owlapi.model.OWLEntity;
import org.semanticweb.owlapi.model.OWLLiteral;
import org.semanticweb.owlapi.model.OWLOntology;
import org.semanticweb.owlapi.vocab.OWLRDFVocabulary;

/**
 * Implements metadata for ontologies, concepts and properties by providing an interface to
 * annotation properties of an OWL entity and exposing them with their fully qualified
 * property IDs as keys.
 * 
 * @author Ferd
 *
 */
public class OWLMetadata implements IMetadata {

    OWLEntity                      _owl;
    OWLOntology                    _ontology;
    HashMap<String, Object>        __data;

    static HashMap<String, String> _metadataVocabulary = new HashMap<String, String>();

    static {
        _metadataVocabulary.put(OWLRDFVocabulary.RDFS_LABEL.getIRI().toString(), IMetadata.DC_LABEL);
        _metadataVocabulary.put(OWLRDFVocabulary.RDFS_COMMENT.getIRI().toString(), IMetadata.DC_COMMENT);
        _metadataVocabulary
                .put("http://protege.stanford.edu/plugins/owl/dc/protege-dc.owl#label", IMetadata.DC_LABEL);
        _metadataVocabulary
                .put("http://protege.stanford.edu/plugins/owl/dc/protege-dc.owl#comment", IMetadata.DC_COMMENT);
        _metadataVocabulary
                .put("http://www.integratedmodelling.org/ks/imcore.owl#baseDeclaration", NS.BASE_DECLARATION);
        _metadataVocabulary
                .put("http://www.integratedmodelling.org/ks/imcore.owl#isAbstract", NS.IS_ABSTRACT);
        _metadataVocabulary
                .put("http://www.integratedmodelling.org/ks/imcore.owl#orderingRank", NS.ORDER_PROPERTY);
    }

    public OWLMetadata(OWLEntity owl, OWLOntology ontology) {
        _owl = owl;
        _ontology = ontology;
    }

    /*
     * mah
     */
    public static Object literal2obj(OWLLiteral l) {

        if (l.isBoolean())
            return l.parseBoolean();
        else if (l.isInteger())
            return l.parseInteger();
        else if (l.isFloat())
            return l.parseFloat();
        else if (l.isDouble())
            return l.parseDouble();

        return l.getLiteral();
    }

    private Map<String, Object> getData() {

        if (__data == null) {
            __data = new HashMap<String, Object>();

            for (OWLAnnotation zio : _owl.getAnnotations(_ontology)) {
                OWLLiteral val = (OWLLiteral) zio.getValue();
                String piri = zio.getProperty().getIRI().toString();
                String prop = _metadataVocabulary.get(piri);
                if (prop != null) {
                    __data.put(prop, literal2obj(val));
                }
            }
        }
        return __data;
    }

    @Override
    public Object get(String string) {
        return getData().get(string);
    }

    @Override
    public Collection<String> getKeys() {
        return getData().keySet();
    }

    @Override
    public void merge(IMetadata md, boolean overwriteExisting) {
    }

    @Override
    public String getString(String field) {
        Object o = get(field);
        return o != null ? o.toString() : null;
    }

    @Override
    public Integer getInt(String field) {
        Object o = get(field);
        if (o instanceof String) {
            try {
                o = Integer.parseInt(o.toString());
            } catch (Throwable e) {
                return null;
            }
        }
        return o != null && o instanceof Number ? ((Number) o).intValue() : null;
    }

    @Override
    public Long getLong(String field) {
        Object o = get(field);
        return o != null && o instanceof Number ? ((Number) o).longValue() : null;
    }

    @Override
    public Double getDouble(String field) {
        Object o = get(field);
        return o != null && o instanceof Number ? ((Number) o).doubleValue() : null;
    }

    @Override
    public Float getFloat(String field) {
        Object o = get(field);
        return o != null && o instanceof Number ? ((Number) o).floatValue() : null;
    }

    @Override
    public Boolean getBoolean(String field) {
        Object o = get(field);
        return o != null && o instanceof Boolean ? (Boolean) o : null;
    }

    @Override
    public IConcept getConcept(String field) {
        Object o = get(field);
        return o != null && o instanceof IConcept ? (IConcept) o : null;
    }

    @Override
    public String getString(String field, String def) {
        Object o = get(field);
        return o != null ? o.toString() : def;
    }

    @Override
    public int getInt(String field, int def) {
        Object o = get(field);
        if (o instanceof String) {
            try {
                o = Integer.parseInt(o.toString());
            } catch (Throwable e) {
                return def;
            }
        }
        return o != null && o instanceof Number ? ((Number) o).intValue() : def;
    }

    @Override
    public long getLong(String field, long def) {
        Object o = get(field);
        return o != null && o instanceof Number ? ((Number) o).longValue() : def;
    }

    @Override
    public double getDouble(String field, double def) {
        Object o = get(field);
        return o != null && o instanceof Number ? ((Number) o).doubleValue() : def;
    }

    @Override
    public float getFloat(String field, float def) {
        Object o = get(field);
        return o != null && o instanceof Number ? ((Number) o).floatValue() : def;
    }

    @Override
    public boolean getBoolean(String field, boolean def) {
        Object o = get(field);
        return o != null && o instanceof Boolean ? (Boolean) o : def;
    }

    @Override
    public IConcept getConcept(String field, IConcept def) {
        Object o = get(field);
        return o != null && o instanceof IConcept ? (IConcept) o : def;
    }

    @Override
    public Collection<Object> getValues() {
        return getData().values();
    }

    @Override
    public void put(String key, Object value) {
        __data.put(key, value);
    }

    @Override
    public void remove(String key) {
        __data.remove(key);
    }

    @Override
    public Map<? extends String, ? extends Object> getDataAsMap() {
        return __data;
    }

    @Override
    public boolean contains(String key) {
        return __data.containsKey(key);
    }

}
