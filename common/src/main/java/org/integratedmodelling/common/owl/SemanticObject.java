/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
//package org.integratedmodelling.common.owl;
//
//import java.util.List;
//
//import org.integratedmodelling.api.knowledge.IConcept;
//import org.integratedmodelling.api.knowledge.IProperty;
//import org.integratedmodelling.api.knowledge.ISemanticObject;
//import org.integratedmodelling.api.lang.IList;
//import org.integratedmodelling.api.modelling.INamespace;
//import org.integratedmodelling.collections.Pair;
//import org.integratedmodelling.collections.Triple;
//import org.integratedmodelling.common.configuration.Env;
//import org.integratedmodelling.exceptions.ThinklabCircularDependencyException;
//
///**
// * This semantic object is a dummy - will just have is() working when the ontology functions are
// * integrated.
// *
// *
// * @author Ferd
// *
// */
//public class SemanticObject implements ISemanticObject<Object> {
//
//    IList _list;
//
//    public SemanticObject(IList list) {
//        _list = list;
//    }
//
//    @Override
//    public IList getSemantics() {
//        return _list;
//    }
//
//    @Override
//    public Object demote() {
//        // TODO Auto-generated method stub
//        return null;
//    }
//
//    @Override
//    public IConcept getDirectType() {
//        return Env.c(_list.first().toString());
//    }
//
//    @Override
//    public boolean is(Object other) {
//        // TODO Auto-generated method stub
//        return false;
//    }
//
//    @Override
//    public int getRelationshipsCount() {
//        // TODO Auto-generated method stub
//        return 0;
//    }
//
//    @Override
//    public int getRelationshipsCount(IProperty _subject) {
//        // TODO Auto-generated method stub
//        return 0;
//    }
//
//    @Override
//    public ISemanticObject<?> get(IProperty property) {
//        // TODO Auto-generated method stub
//        return null;
//    }
//
//    @Override
//    public List<Pair<IProperty, ISemanticObject<?>>> getRelationships() {
//        // TODO Auto-generated method stub
//        return null;
//    }
//
//    @Override
//    public List<ISemanticObject<?>> getRelationships(IProperty property) {
//        // TODO Auto-generated method stub
//        return null;
//    }
//
//    @Override
//    public boolean isLiteral() {
//        // TODO Auto-generated method stub
//        return false;
//    }
//
//    @Override
//    public boolean isConcept() {
//        // TODO Auto-generated method stub
//        return false;
//    }
//
//    @Override
//    public boolean isObject() {
//        // TODO Auto-generated method stub
//        return true;
//    }
//
//    @Override
//    public boolean isCyclic() {
//        // TODO Auto-generated method stub
//        return false;
//    }
//
//    @Override
//    public boolean isValid() {
//        // TODO Auto-generated method stub
//        return true;
//    }
//
//    @Override
//    public List<ISemanticObject<?>> getSortedRelationships(IProperty property)
//            throws ThinklabCircularDependencyException {
//        // TODO Auto-generated method stub
//        return null;
//    }
//
//    @Override
//    public INamespace getNamespace() {
//        // TODO Auto-generated method stub
//        return null;
//    }
//
//    @Override
//    public List<Triple<IProperty, ISemanticObject<?>, Integer>> getCountedRelationships() {
//        // TODO Auto-generated method stub
//        return null;
//    }
//
// }
