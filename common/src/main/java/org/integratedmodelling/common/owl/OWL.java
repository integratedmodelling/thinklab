/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.common.owl;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.regex.Pattern;

import org.apache.commons.lang.exception.ExceptionUtils;
import org.integratedmodelling.api.knowledge.IConcept;
import org.integratedmodelling.api.knowledge.IOntology;
import org.integratedmodelling.api.knowledge.IProperty;
import org.integratedmodelling.api.modelling.INamespace;
import org.integratedmodelling.api.monitoring.IMonitor;
import org.integratedmodelling.common.kim.KIMNamespace;
import org.integratedmodelling.common.utils.CamelCase;
import org.integratedmodelling.common.utils.MiscUtilities;
import org.integratedmodelling.common.utils.URLUtils;
import org.integratedmodelling.exceptions.ThinklabException;
import org.integratedmodelling.exceptions.ThinklabIOException;
import org.integratedmodelling.exceptions.ThinklabRuntimeException;
import org.integratedmodelling.lang.SemanticType;
import org.reflections.Reflections;
import org.reflections.scanners.ResourcesScanner;
import org.reflections.util.ClasspathHelper;
import org.reflections.util.ConfigurationBuilder;
import org.semanticweb.owlapi.apibinding.OWLManager;
import org.semanticweb.owlapi.model.IRI;
import org.semanticweb.owlapi.model.OWLClass;
import org.semanticweb.owlapi.model.OWLOntology;
import org.semanticweb.owlapi.model.OWLOntologyAlreadyExistsException;
import org.semanticweb.owlapi.model.OWLOntologyCreationException;
import org.semanticweb.owlapi.model.OWLOntologyManager;
import org.semanticweb.owlapi.reasoner.OWLReasoner;
import org.semanticweb.owlapi.util.AutoIRIMapper;

/**
 * Import concepts and properties from OWL ontologies.
 *
 * @author Ferd
 *
 */
public class OWL {

    public static final String INTERNAL_ONTOLOGY_PREFIX = "http://integratedmodelling.org/ks/internal/";

    private final HashMap<String, INamespace> _namespaces    = new HashMap<String, INamespace>();
    private final HashMap<String, INamespace> _resourceIndex = new HashMap<String, INamespace>();
    HashMap<String, IOntology>                _ontologies    = new HashMap<String, IOntology>();

    HashMap<String, String>         _iri2ns         = new HashMap<String, String>();
    HashMap<SemanticType, OWLClass> _systemConcepts = new HashMap<SemanticType, OWLClass>();
    HashMap<String, IConcept>       _xsdMappings    = new HashMap<String, IConcept>();

    IConcept _thing;
    IConcept _nothing;

    public IOntology requireOntology(String id, String prefix) {

        if (_ontologies.get(id) != null) {
            return _ontologies.get(id);
        }

        IOntology ret = null;
        try {
            OWLOntology o = manager.createOntology(IRI
                    .create(prefix + "/" + id));
            ret = new Ontology(o, id, this);
            _ontologies.put(id, ret);
            _iri2ns.put(o.getOntologyID().getDefaultDocumentIRI().toString(), id);
        } catch (OWLOntologyCreationException e) {
            throw new ThinklabRuntimeException(e);
        }

        return ret;
    }

    /*
     * package-visible, never null.
     */
    OWLOntologyManager manager = null;

    /*
     * package-visible, may be null.
     */
    OWLReasoner  reasoner = null;
    private File _loadPath;

    public static String getFileName(String s) {

        String ret = s;

        int sl = ret.lastIndexOf(File.separator);
        if (sl < 0) {
            sl = ret.lastIndexOf('/');
        }
        if (sl > 0) {
            ret = ret.substring(sl + 1);
        }

        return ret;
    }

    /**
     * This one will create a manager, so all knowledge loaded is local to this
     * parser. Using this object as a singleton will (for now) enforce this
     * behavior.
     * 
     * @throws ThinklabException
     */
    public OWL(/* IModelResolver resolver, */File loadPath) throws ThinklabException {
        // _resolver = resolver;
        manager = OWLManager.createOWLOntologyManager();
        this._loadPath = loadPath;
        initialize();
    }

    /**
     * Use this to read knowledge into an existing manager
     * 
     * @param manager
     * @throws ThinklabException
     */
    public OWL(OWLOntologyManager manager, File loadPath) throws ThinklabException {
        // this._resolver = resolver;
        this.manager = manager;
        this._loadPath = loadPath;
        initialize();
    }

    private void initialize() throws ThinklabException {

        /*
         * TODO insert basic datatypes as well
         */
        _systemConcepts.put(new SemanticType("owl:Thing"), manager
                .getOWLDataFactory().getOWLThing());
        _systemConcepts.put(new SemanticType("owl:Nothing"), manager
                .getOWLDataFactory().getOWLNothing());

        if (_loadPath == null) {
            throw new ThinklabIOException("owl resources cannot be found: knowledge load directory does not exist");
        }

        load(_loadPath);

        /*
         * all namespaces so far are internal, and just these.
         */
        for (INamespace ns : _namespaces.values()) {
            ((KIMNamespace) ns).setInternal(true);
            ((Ontology) (ns.getOntology())).setInternal(true);
        }
    }

    // @Override
    // public INamespace parseInNamespace(InputStream input, String namespace, IModelResolver resolver)
    // throws ThinklabException {
    // throw new ThinklabUnsupportedOperationException("cannot parse OWL ontology fragments in namespace "
    // + namespace
    // + ": only direct ontology import is supported");
    // }

    String importOntology(OWLOntology ontology, String resource, String namespace, boolean imported, IMonitor monitor)
            throws ThinklabException {

        if (!_ontologies.containsKey(namespace)) {
            _ontologies.put(namespace, new Ontology(ontology, namespace, this));
        }

        /*
         * seen already?
         */
        if (_namespaces.containsKey(namespace)) {
            return namespace;
        }

        KIMNamespace ns = new KIMNamespace(namespace, new File(resource), _ontologies.get(namespace));

        // INamespaceDefinition ns = (INamespaceDefinition) resolver
        // .newLanguageObject(INamespace.class, monitor);
        // resolver.onNamespaceDeclared();
        // ns.setOntology(_ontologies.get(namespace));
        // resolver.onNamespaceDefined();

        _namespaces.put(namespace, ns);

        return namespace;
    }

    // @Override
    // public INamespace parse(String namespace, String resource, IModelResolver resolver, IMonitor monitor)
    // throws ThinklabException {
    //
    // String ns = "__not__found";
    // InputStream input = null;
    //
    // Throwable exception = null;
    //
    // try {
    //
    // if (_resourceIndex.containsKey(resource)) {
    // return _resourceIndex.get(resource);
    // }
    //
    // input = resolver.openStream();
    // OWLOntology ontology = manager
    // .loadOntologyFromOntologyDocument(input);
    // input.close();
    //
    // /*
    // * import ontology and all its imports. Return namespace ID.
    // */
    // ns = importOntology(ontology, resolver, resource, namespace, false, monitor);
    //
    // /*
    // * do not load this again, it's in the manager.
    // */
    // _resourceIndex.put(resource, _namespaces.get(ns));
    //
    // } catch (OWLOntologyCreationIOException e) {
    // // IOExceptions during loading get wrapped in an
    // // OWLOntologyCreationIOException
    // exception = e;
    //
    // IOException ioException = e.getCause();
    // if (ioException instanceof FileNotFoundException) {
    // System.out.println("Could not load ontology. File not found: "
    // + ioException.getMessage());
    // } else if (ioException instanceof UnknownHostException) {
    // System.out.println("Could not load ontology. Unknown host: "
    // + ioException.getMessage());
    // } else {
    // System.out.println("Could not load ontology: "
    // + ioException.getClass().getSimpleName() + " "
    // + ioException.getMessage());
    // }
    // } catch (UnparsableOntologyException e) {
    //
    // exception = e;
    //
    // // If there was a problem loading an ontology because there are
    // // syntax errors in the document (file) that
    // // represents the ontology then an UnparsableOntologyException is
    // // thrown
    // System.out.println("Could not parse the ontology: "
    // + e.getMessage());
    // // A map of errors can be obtained from the exception
    // Map<OWLParser, OWLParserException> exceptions = e.getExceptions();
    // // The map describes which parsers were tried and what the errors
    // // were
    // for (OWLParser parser : exceptions.keySet()) {
    // System.out.println("Tried to parse the ontology with the "
    // + parser.getClass().getSimpleName() + " parser");
    // System.out.println("Failed because: "
    // + exceptions.get(parser).getMessage());
    // }
    // } catch (UnloadableImportException e) {
    //
    // exception = e;
    //
    // // If our ontology contains imports and one or more of the imports
    // // could not be loaded then an
    // // UnloadableImportException will be thrown (depending on the
    // // missing imports handling policy)
    // System.out.println("Could not load import: "
    // + e.getImportsDeclaration());
    // // The reason for this is specified and an
    // // OWLOntologyCreationException
    // OWLOntologyCreationException cause = e
    // .getOntologyCreationException();
    // System.out.println("Reason: " + cause.getMessage());
    // } catch (OWLOntologyAlreadyExistsException e) {
    //
    // /*
    // * OK, then wrap it and screw that.
    // */
    // OWLOntology ontology = manager.getOntology(e.getOntologyID()
    // .getOntologyIRI());
    // if (ontology != null) {
    // ns = importOntology(ontology, resolver, resource, namespace, false, monitor);
    // _resourceIndex.put(resource, _namespaces.get(ns));
    // }
    //
    // } catch (OWLOntologyCreationException e) {
    // exception = e;
    // System.out.println("Could not load ontology: " + e.getMessage());
    // } catch (IOException e) {
    // exception = e;
    // } finally {
    // if (input != null) {
    // try {
    // input.close();
    // } catch (IOException e) {
    // exception = e;
    // }
    // }
    // }
    //
    // if (exception != null) {
    // resolver.onException(exception, 0);
    // }
    //
    // /*
    // * get the finished namespace (with the ontology in it) and set the
    // * remaining fields.
    // */
    // INamespace nns = _namespaces.get(ns);
    // ((KIMNamespace) nns).setResourceUrl(resource);
    // ((KIMNamespace) nns).setId(namespace);
    //
    // OWLOntologyID oid = ((Ontology) (nns.getOntology()))._ontology
    // .getOntologyID();
    // _iri2ns.put(oid.getDefaultDocumentIRI().toString(), namespace);
    //
    // return _namespaces.get(ns);
    // }

    // @Override
    public void writeNamespace(INamespace namespace, File outputFile)
            throws ThinklabException {
        /*
         * TODO save all axioms in namespace as OWL axioms
         */

        /*
         * TODO turn the model structure into axioms?
         */

    }

    /*
     * the three knowledge manager methods we implement, so we can serve as
     * delegate to a KM for these.
     */
    public IConcept getConcept(String concept) {
        IConcept result = null;

        if (SemanticType.validate(concept)) {
            SemanticType st = new SemanticType(concept);
            IOntology o = _ontologies.get(st.getConceptSpace());
            if (o == null) {
                OWLClass systemConcept = _systemConcepts.get(st);
                if (systemConcept != null) {
                    result = new Concept(systemConcept, this, st.getConceptSpace());
                }
            } else {
                result = o.getConcept(st.getLocalName());
            }
        }

        return result;
    }

    public IProperty getProperty(String concept) {
        IProperty result = null;

        if (SemanticType.validate(concept)) {
            String[] conceptSpaceAndLocalName = SemanticType
                    .splitIdentifier(concept);
            IOntology o = _ontologies.get(conceptSpaceAndLocalName[0]);
            if (o != null) {
                result = o.getProperty(conceptSpaceAndLocalName[1]);
            }
        }
        return result;
    }

    public IConcept getLeastGeneralCommonConcept(Collection<IConcept> cc) {
        IConcept ret = null;
        IConcept tmpConcept;

        for (IConcept concept : cc) {
            if (ret == null) {
                ret = concept;
            } else {
                tmpConcept = ret.getLeastGeneralCommonConcept(concept);
                if (tmpConcept != null) {
                    ret = tmpConcept;
                }
            }
        }

        return ret;
    }

    /**
     * Return the ontology for the given namespace ID (short name).
     * 
     * @param _cs
     * @return
     */
    public IOntology getOntology(String ns) {
        return _ontologies.get(ns);
    }

    public IConcept getRootConcept() {
        if (_thing == null) {
            _thing = new Concept(manager.getOWLDataFactory().getOWLThing(), this, "owl");
        }
        return _thing;
    }

    public String getConceptSpace(IRI iri) {

        String r = MiscUtilities.removeFragment(iri.toURI()).toString();
        String ret = _iri2ns.get(r);

        if (ret == null) {
            /*
             * happens, whenever we depend on a concept from a server ontology
             * not loaded yet. Must find a way to deal with this.
             */
            // System.out.println("NULL: " + iri);
            ret = MiscUtilities.getNameFromURL(r);
        }

        return ret;
    }

    /**
     * Works as long as the classpath reported by forPackage uses kosher URLs,
     * which it doesn't if we're running under OSGI. So at the moment it is not
     * used, in favor of an explicit installation dir (see initialize()).
     * 
     * @param dir
     * @return
     * @throws ThinklabIOException
     */
    public boolean extractCoreOntologies(File dir) throws ThinklabIOException {

        dir.mkdirs();

        try {
            Reflections reflections = new Reflections(new ConfigurationBuilder()
                    .setUrls(ClasspathHelper.forPackage("knowledge"))
                    .setScanners(new ResourcesScanner()));

            for (String of : reflections.getResources(Pattern
                    .compile(".*\\.owl"))) {

                /*
                 * remove knowledge/ prefix for output
                 */
                String oof = of;
                if (oof.startsWith("knowledge/")) {
                    oof = oof.substring("knowledge/".length());
                }

                File output = new File(dir + File.separator + oof);
                new File(MiscUtilities.getFilePath(output.toString())).mkdirs();
                URLUtils.copy(this.getClass().getClassLoader().getResource(of), output);
            }
        } catch (Throwable e) {
            return false;
        }

        return true;
    }

    /**
     * Load OWL files from given directory and in its subdirectories, using a
     * prefix mapper to resolve URLs internally and deriving ontology names from
     * the relative paths.
     *
     * This uses the resolver passed at initialization only to create the
     * namespace. It's only meant for core knowledge not seen by users.
     *
     * @param kdir
     * @throws ThinklabIOException
     */
    public void load(File kdir) throws ThinklabException {

        AutoIRIMapper imap = new AutoIRIMapper(kdir, true);
        manager.addIRIMapper(imap);

        File[] files = kdir.listFiles();
        // null in error
        if (files != null) {
            for (File fl : files) {
                loadInternal(fl, "", null);
            }
        } else {
            throw new ThinklabIOException("Errors reading core ontologies: system will be nonfunctional. Check server distribution.");
        }
    }

    private void loadInternal(File f, String path, IMonitor monitor)
            throws ThinklabException {

        String pth = path == null ? ""
                : (path + (path.isEmpty() ? "" : ".")
                        + CamelCase.toLowerCase(MiscUtilities.getFileBaseName(f.toString()), '-'));

        if (f.isDirectory()) {

            for (File fl : f.listFiles()) {
                loadInternal(fl, pth, monitor);
            }

        } else if (MiscUtilities.getFileExtension(f.toString()).equals("owl")) {

            InputStream input;

            try {
                input = new FileInputStream(f);
                OWLOntology ontology = manager
                        .loadOntologyFromOntologyDocument(input);
                input.close();
                Ontology ont = new Ontology(ontology, pth, this);
                ont.setResourceUrl(f.toURI().toURL().toString());
                _ontologies.put(pth, ont);
                _iri2ns.put(ontology.getOntologyID().getDefaultDocumentIRI()
                        .toString(), pth);

            } catch (OWLOntologyAlreadyExistsException e) {

                /*
                 * already imported- wrap it and use it as is.
                 */
                OWLOntology ont = manager.getOntology(e.getOntologyID()
                        .getOntologyIRI());
                if (ont != null && _ontologies.get(pth) == null) {
                    Ontology o = new Ontology(ont, pth, this);
                    try {
                        o.setResourceUrl(f.toURI().toURL().toString());
                    } catch (MalformedURLException e1) {
                    }
                    _ontologies.put(pth, o);
                    _iri2ns.put(ont.getOntologyID().getDefaultDocumentIRI()
                            .toString(), pth);
                }

            } catch (Exception e) {

                /*
                 * everything else is probably an error
                 */
                throw new ThinklabIOException("reading " + f + ": " + ExceptionUtils.getRootCauseMessage(e));
            }

            IOntology o = _ontologies.get(pth);
            if (o != null) {
                /*
                 * create namespace
                 */
                KIMNamespace ns = new KIMNamespace(pth, f, o);
                // ns.setId(pth);
                // ns.setResourceUrl(f.toString());
                // ns.setOntology(o);
                _namespaces.put(pth, ns);
            }
        }
    }

    public IOntology refreshOntology(URL url, String id)
            throws ThinklabException {

        InputStream input;
        Ontology ret = null;

        try {
            input = url.openStream();
            OWLOntology ontology = manager
                    .loadOntologyFromOntologyDocument(input);
            input.close();
            ret = new Ontology(ontology, id, this);
            ret.setResourceUrl(url.toString());
            _ontologies.put(id, ret);
            _iri2ns.put(ontology.getOntologyID().getDefaultDocumentIRI()
                    .toString(), id);

        } catch (OWLOntologyAlreadyExistsException e) {

            /*
             * already imported- wrap it and use it as is.
             */
            OWLOntology ont = manager.getOntology(e.getOntologyID()
                    .getOntologyIRI());
            if (ont != null && _ontologies.get(id) == null) {
                _ontologies.put(id, new Ontology(ont, id, this));
                _iri2ns.put(ont.getOntologyID().getDefaultDocumentIRI()
                        .toString(), id);
            }

        } catch (Exception e) {

            /*
             * everything else is probably an error
             */
            throw new ThinklabIOException(e);
        }

        IOntology o = _ontologies.get(id);
        if (o != null) {
            /*
             * create namespace
             */
            KIMNamespace ns = new KIMNamespace(id, new File(url.getFile()), o);
            // INamespaceDefinition ns = (INamespaceDefinition) _resolver
            // .newLanguageObject(INamespace.class, null);
            // ns.setId(id);
            // ns.setResourceUrl(url.toString());
            // ns.setOntology(o);
            _namespaces.put(id, ns);
        }

        return ret;
    }

    public IConcept getDatatypeMapping(String string) {
        return _xsdMappings.get(string);
    }

    public IConcept registerDatatypeMapping(String xsd, IConcept concept) {
        return _xsdMappings.put(xsd, concept);
    }

    public void releaseOntology(IOntology ontology) {

        // TODO remove from _csIndex - should be harmless to leave for now
        INamespace ns = _namespaces.get(ontology.getConceptSpace());
        if (ns != null) {
            _resourceIndex.remove(ns.getResourceUrl());
        }
        _namespaces.remove(ontology.getConceptSpace());
        _ontologies.remove(ontology.getConceptSpace());
        _iri2ns.remove(((Ontology) ontology)._ontology.getOntologyID()
                .getDefaultDocumentIRI().toString());
        manager.removeOntology(((Ontology) ontology)._ontology);
    }

    public void clear() {
        Collection<String> keys = new HashSet<String>(_ontologies.keySet());
        for (String o : keys) {
            releaseOntology(getOntology(o));
        }
    }

    public Collection<IOntology> getOntologies(boolean includeInternal) {
        ArrayList<IOntology> ret = new ArrayList<>();
        for (IOntology o : _ontologies.values()) {
            if (((Ontology) o).isInternal() && !includeInternal)
                continue;
            ret.add(o);
        }
        return ret;
    }

    public INamespace getNamespace(String ns) {
        return _namespaces.get(ns);
    }

    public Collection<INamespace> getNamespaces() {
        return _namespaces.values();
    }

    public Collection<IConcept> listConcepts(boolean includeInternal) {

        ArrayList<IConcept> ret = new ArrayList<>();

        for (IOntology o : _ontologies.values()) {
            if (((Ontology) o).isInternal() && !includeInternal)
                continue;
            ret.addAll(o.getConcepts());
        }
        return ret;
    }

    public IConcept getNothing() {
        if (_nothing == null) {
            _nothing = new Concept(manager.getOWLDataFactory().getOWLNothing(), this, "owl");
        }
        return _nothing;
    }

}
