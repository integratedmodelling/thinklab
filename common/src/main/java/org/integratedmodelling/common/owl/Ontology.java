/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.common.owl;

import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.integratedmodelling.api.knowledge.IAxiom;
import org.integratedmodelling.api.knowledge.IConcept;
import org.integratedmodelling.api.knowledge.IKnowledge;
import org.integratedmodelling.api.knowledge.IOntology;
import org.integratedmodelling.api.knowledge.IProperty;
import org.integratedmodelling.api.lang.IList;
import org.integratedmodelling.api.metadata.IMetadata;
import org.integratedmodelling.api.modelling.INamespace;
import org.integratedmodelling.common.configuration.KLAB;
import org.integratedmodelling.common.utils.StringUtils;
import org.integratedmodelling.common.vocabulary.NS;
import org.integratedmodelling.exceptions.ThinklabException;
import org.integratedmodelling.exceptions.ThinklabIOException;
import org.integratedmodelling.exceptions.ThinklabRuntimeException;
import org.integratedmodelling.lang.Axiom;
import org.integratedmodelling.lang.SemanticType;
import org.semanticweb.owlapi.io.OWLXMLOntologyFormat;
import org.semanticweb.owlapi.model.AddImport;
import org.semanticweb.owlapi.model.IRI;
import org.semanticweb.owlapi.model.OWLAnnotation;
import org.semanticweb.owlapi.model.OWLAnnotationProperty;
import org.semanticweb.owlapi.model.OWLClass;
import org.semanticweb.owlapi.model.OWLClassExpression;
import org.semanticweb.owlapi.model.OWLDataFactory;
import org.semanticweb.owlapi.model.OWLDataProperty;
import org.semanticweb.owlapi.model.OWLEntity;
import org.semanticweb.owlapi.model.OWLImportsDeclaration;
import org.semanticweb.owlapi.model.OWLLiteral;
import org.semanticweb.owlapi.model.OWLObjectProperty;
import org.semanticweb.owlapi.model.OWLOntology;
import org.semanticweb.owlapi.model.OWLOntologyChangeException;
import org.semanticweb.owlapi.model.OWLOntologyFormat;
import org.semanticweb.owlapi.model.OWLOntologyStorageException;
import org.semanticweb.owlapi.model.OWLProperty;

/**
 * A proxy for an ontology. Holds a list of concepts and a list of axioms. Can
 * be turned into a list and marshalled to a server for actual knowledge
 * creation. Contains no instances, properties or restrictions directly, just
 * concepts for indexing and axioms for the actual stuff.
 * 
 * @author Ferd
 * 
 */
public class Ontology implements IOntology {

    String      _id;
    List<IList> _axioms = new ArrayList<IList>();

    Set<String>                 _imported       = new HashSet<String>();
    Map<String, Integer>        _conceptOptions = new HashMap<>();
    Map<IKnowledge, IKnowledge> _aliases        = new HashMap<>();

    OWLOntology _ontology;
    String      _prefix;
    OWL         _manager;
    Set<String> _conceptIDs = new HashSet<String>();

    /*
     * all property
     */
    Set<String> _propertyIDs = new HashSet<String>();

    /*
     * property IDs by class - no other way to return the OWL objects quickly.
     * 
     * what a pain
     */
    Set<String>     _opropertyIDs = new HashSet<String>();
    Set<String>     _dpropertyIDs = new HashSet<String>();
    Set<String>     _apropertyIDs = new HashSet<String>();
    private String  _resourceUrl;
    private boolean _isInternal   = false;

    Ontology(OWLOntology ontology, String id, OWL manager) {

        _id = id;
        _ontology = ontology;
        _manager = manager;
        _prefix = ontology.getOntologyID().getDefaultDocumentIRI().toString();
        while (_prefix.endsWith("#"))
            _prefix = StringUtils.chop(_prefix);

        scan();
    }

    /*
     * build a catalog of names, as there seems to be no way to quickly assess
     * if an ontology contains a named entity or not. This needs to be kept in
     * sync with any changes, which is a pain.
     * 
     * FIXME reintegrate the conditionals when define() works properly.
     */
    private void scan() {

        for (OWLClass c : _ontology.getClassesInSignature()) {
            if (c.getIRI().toString().contains(_prefix)) {
                _conceptIDs.add(c.getIRI().getFragment());
            }
        }
        for (OWLProperty<?, ?> p : _ontology.getDataPropertiesInSignature()) {
            if (p.getIRI().toString().contains(_prefix)) {
                _dpropertyIDs.add(p.getIRI().getFragment());
                _propertyIDs.add(p.getIRI().getFragment());
            }
        }
        for (OWLProperty<?, ?> p : _ontology.getObjectPropertiesInSignature()) {
            if (p.getIRI().toString().contains(_prefix)) {
                _opropertyIDs.add(p.getIRI().getFragment());
                _propertyIDs.add(p.getIRI().getFragment());
            }
        }
        for (OWLAnnotationProperty p : _ontology
                .getAnnotationPropertiesInSignature()) {
            if (p.getIRI().toString().contains(_prefix)) {
                _apropertyIDs.add(p.getIRI().getFragment());
                _propertyIDs.add(p.getIRI().getFragment());
            }
        }
    }

    @Override
    public IOntology getOntology() {
        return this;
    }

    @Override
    public Collection<IConcept> getConcepts() {

        ArrayList<IConcept> ret = new ArrayList<IConcept>();
        // for (OWLClass c : _ontology.getClassesInSignature(false)) {
        // if (!c.isAnonymous() && !c.isBuiltIn())
        // if (c.getIRI().toString().contains(_prefix)) {
        // ret.add(new Concept(c, _manager, _id));
        // }
        // }
        for (String s : _conceptIDs) {
            ret.add(getConcept(s));
        }
        return ret;
    }

    @Override
    public Collection<IProperty> getProperties() {
        ArrayList<IProperty> ret = new ArrayList<IProperty>();
        for (OWLProperty<?, ?> p : _ontology
                .getDataPropertiesInSignature(false)) {
            ret.add(new Property(p, _manager, _id));
        }
        for (OWLProperty<?, ?> p : _ontology
                .getObjectPropertiesInSignature(false)) {
            ret.add(new Property(p, _manager, _id));
        }
        for (OWLAnnotationProperty p : _ontology
                .getAnnotationPropertiesInSignature()) {
            ret.add(new Property(p, _manager, _id));
        }

        return ret;
    }

    /**
     * Special purpose function: return all concepts that have no parents that
     * belong to this ontology - making them "top-level" in it. Optionally, just
     * return those whose ONLY parent is owl:Thing.
     * 
     * @param noParent
     *            if true, only return those concept that derive directly and
     *            exclusively from owl:Thing.
     * @return
     */
    public synchronized List<IConcept> getTopConcepts(boolean observables) {
        ArrayList<IConcept> ret = new ArrayList<IConcept>();
        for (IConcept c : getConcepts()) {
            boolean ok = true;
            if (!observables || NS.isParticular(c)) {
                Collection<IConcept> parents = c.getParents();
                for (IConcept cc : parents) {
                    if (cc.getConceptSpace().equals(_id)) {
                        ok = false;
                        break;
                    }
                }
                if (ok) {
                    ret.add(c);
                }
            }
        }
        return ret;
    }

    @Override
    public IConcept getConcept(String ID) {
        if (_conceptIDs.contains(ID)) {
            return new Concept(_ontology.getOWLOntologyManager()
                    .getOWLDataFactory()
                    .getOWLClass(IRI.create(_prefix + "#" + ID)), _manager, _id);
        }
        return null;
    }

    @Override
    public IProperty getProperty(String ID) {
        if (_opropertyIDs.contains(ID)) {
            return new Property(_ontology.getOWLOntologyManager()
                    .getOWLDataFactory()
                    .getOWLObjectProperty(IRI.create(_prefix + "#" + ID)), _manager, _id);
        }
        if (_dpropertyIDs.contains(ID)) {
            return new Property(_ontology.getOWLOntologyManager()
                    .getOWLDataFactory()
                    .getOWLDataProperty(IRI.create(_prefix + "#" + ID)), _manager, _id);
        }
        if (_apropertyIDs.contains(ID)) {
            return new Property(_ontology.getOWLOntologyManager()
                    .getOWLDataFactory()
                    .getOWLAnnotationProperty(IRI.create(_prefix + "#" + ID)), _manager, _id);
        }
        return null;
    }

    @Override
    public String getURI() {
        return _ontology.getOWLOntologyManager()
                .getOntologyDocumentIRI(_ontology).toString();
    }

    @Override
    public boolean write(File file) throws ThinklabException {
        OWLOntologyFormat format = _ontology.getOWLOntologyManager().getOntologyFormat(_ontology);
        OWLXMLOntologyFormat owlxmlFormat = new OWLXMLOntologyFormat();
        if (format.isPrefixOWLOntologyFormat()) {
            owlxmlFormat.copyPrefixesFrom(format.asPrefixOWLOntologyFormat());
        }
        try {
            _ontology.getOWLOntologyManager().saveOntology(_ontology, owlxmlFormat, IRI.create(file.toURI()));
        } catch (OWLOntologyStorageException e) {
            throw new ThinklabIOException(e);
        }
        return true;
    }

    @Override
    public Collection<String> define(Collection<IAxiom> axioms) {

        ArrayList<String> errors = new ArrayList<String>();

        /*
         * ACHTUNG remember to add IDs to appropriate catalogs as classes and
         * property assertions are encountered. This can be called
         * incrementally, so better not to call scan() every time.
         */
        OWLDataFactory factory = _ontology.getOWLOntologyManager()
                .getOWLDataFactory();

        for (IAxiom axiom : axioms) {

            // System.out.println(_id + " << " + axiom);

            try {

                if (axiom.is(IAxiom.CLASS_ASSERTION)) {

                    OWLClass newcl = factory.getOWLClass(IRI.create(_prefix
                            + "#" + axiom.getArgument(0)));
                    _ontology.getOWLOntologyManager()
                            .addAxiom(_ontology, factory.getOWLDeclarationAxiom(newcl));
                    _conceptIDs.add(axiom.getArgument(0).toString());

                    // _conceptOptions.put(axiom.getArgument(0).toString(),
                    // ((Axiom) axiom).getOptions());

                } else if (axiom.is(IAxiom.SUBCLASS_OF)) {

                    OWLClass subclass = findClass(axiom.getArgument(1)
                            .toString(), errors);
                    OWLClass superclass = findClass(axiom.getArgument(0)
                            .toString(), errors);

                    if (subclass != null && superclass != null) {
                        _manager.manager.addAxiom(_ontology, factory
                                .getOWLSubClassOfAxiom(subclass, superclass));
                    }

                } else if (axiom.is(IAxiom.ANNOTATION_PROPERTY_ASSERTION)) {

                    OWLAnnotationProperty p = factory
                            .getOWLAnnotationProperty(IRI.create(_prefix + "#"
                                    + axiom.getArgument(0)));
                    // _ontology.getOWLOntologyManager().addAxiom(_ontology,
                    // factory.getOWLDeclarationAxiom(p));
                    _propertyIDs.add(axiom.getArgument(0).toString());
                    _apropertyIDs.add(axiom.getArgument(0).toString());
                    OWLMetadata._metadataVocabulary
                            .put(p.getIRI().toString(), getConceptSpace() + ":" + axiom.getArgument(0));

                } else if (axiom.is(IAxiom.DATA_PROPERTY_ASSERTION)) {

                    OWLDataProperty p = factory.getOWLDataProperty(IRI
                            .create(_prefix + "#" + axiom.getArgument(0)));
                    _ontology.getOWLOntologyManager().addAxiom(_ontology, factory.getOWLDeclarationAxiom(p));
                    _propertyIDs.add(axiom.getArgument(0).toString());
                    _dpropertyIDs.add(axiom.getArgument(0).toString());

                } else if (axiom.is(IAxiom.DATA_PROPERTY_DOMAIN)) {

                    OWLEntity property = findProperty(axiom.getArgument(0)
                            .toString(), true, errors);
                    OWLClass classExp = findClass(axiom.getArgument(1)
                            .toString(), errors);

                    if (property != null && classExp != null) {
                        _manager.manager
                                .addAxiom(_ontology, factory
                                        .getOWLDataPropertyDomainAxiom(property
                                                .asOWLDataProperty(), classExp));
                    }

                } else if (axiom.is(IAxiom.DATA_PROPERTY_RANGE)) {

                    OWLEntity property = findProperty(axiom.getArgument(0)
                            .toString(), true, errors);
                            /*
                             * TODO XSD stuff
                             */

                    // _manager.manager.addAxiom(
                    // _ontology,
                    // factory.getOWLDataPropertyRangeAxiom(property.asOWLDataProperty(),
                    // classExp));

                } else if (axiom.is(IAxiom.OBJECT_PROPERTY_ASSERTION)) {

                    OWLObjectProperty p = factory.getOWLObjectProperty(IRI
                            .create(_prefix + "#" + axiom.getArgument(0)));
                    _ontology.getOWLOntologyManager().addAxiom(_ontology, factory.getOWLDeclarationAxiom(p));
                    _propertyIDs.add(axiom.getArgument(0).toString());
                    _opropertyIDs.add(axiom.getArgument(0).toString());

                } else if (axiom.is(IAxiom.OBJECT_PROPERTY_DOMAIN)) {

                    OWLEntity property = findProperty(axiom.getArgument(0)
                            .toString(), false, errors);
                    OWLClass classExp = findClass(axiom.getArgument(1)
                            .toString(), errors);

                    if (property != null && classExp != null) {
                        _manager.manager.addAxiom(_ontology, factory
                                .getOWLObjectPropertyDomainAxiom(property.asOWLObjectProperty(), classExp));
                    }

                } else if (axiom.is(IAxiom.OBJECT_PROPERTY_RANGE)) {

                    OWLEntity property = findProperty(axiom.getArgument(0)
                            .toString(), false, errors);
                    OWLClass classExp = findClass(axiom.getArgument(1)
                            .toString(), errors);

                    if (property != null && classExp != null) {
                        _manager.manager.addAxiom(_ontology, factory
                                .getOWLObjectPropertyRangeAxiom(property.asOWLObjectProperty(), classExp));
                    }

                } else if (axiom.is(IAxiom.ALL_VALUES_FROM_RESTRICTION)) {

                    OWLEntity property = findProperty(axiom.getArgument(1)
                            .toString(), false, errors);
                    OWLClass target = findClass(axiom.getArgument(0).toString(), errors);
                    OWLClass filler = findClass(axiom.getArgument(2).toString(), errors);

                    // ehm. Should never be data. It's just not possible to represent the
                    // restriction as data in owl. Problem is, we use data in ways that are only
                    // possible for objects in OWL.
                    boolean isData = property instanceof OWLDataProperty;

                    OWLClassExpression restr = factory
                            .getOWLObjectAllValuesFrom(property.asOWLObjectProperty(), filler);

                    if (property != null && filler != null && target != null
                            && restr != null) {
                        _manager.manager.addAxiom(_ontology, factory.getOWLSubClassOfAxiom(target, restr));
                    }

                } else if (axiom.is(IAxiom.AT_LEAST_N_VALUES_FROM_RESTRICTION)) {

                    int n = ((Number) axiom.getArgument(3)).intValue();

                    OWLEntity property = findProperty(axiom.getArgument(1)
                            .toString(), false, errors);
                    OWLClass target = findClass(axiom.getArgument(0).toString(), errors);
                    OWLClass filler = findClass(axiom.getArgument(2).toString(), errors);

                    OWLClassExpression restr = factory
                            .getOWLObjectMinCardinality(n, property.asOWLObjectProperty(), filler);

                    if (property != null && filler != null && target != null
                            && restr != null) {
                        _manager.manager.addAxiom(_ontology, factory.getOWLSubClassOfAxiom(target, restr));
                    }

                } else if (axiom.is(IAxiom.AT_MOST_N_VALUES_FROM_RESTRICTION)) {

                    int n = ((Number) axiom.getArgument(3)).intValue();

                    OWLEntity property = findProperty(axiom.getArgument(1)
                            .toString(), false, errors);
                    OWLClass target = findClass(axiom.getArgument(0).toString(), errors);
                    OWLClass filler = findClass(axiom.getArgument(2).toString(), errors);

                    OWLClassExpression restr = factory
                            .getOWLObjectMaxCardinality(n, property.asOWLObjectProperty(), filler);

                    if (property != null && filler != null && target != null
                            && restr != null) {
                        _manager.manager.addAxiom(_ontology, factory.getOWLSubClassOfAxiom(target, restr));
                    }

                } else if (axiom.is(IAxiom.EXACTLY_N_VALUES_FROM_RESTRICTION)) {

                    int n = ((Number) axiom.getArgument(3)).intValue();

                    OWLEntity property = findProperty(axiom.getArgument(1)
                            .toString(), false, errors);
                    OWLClass target = findClass(axiom.getArgument(0).toString(), errors);
                    OWLClass filler = findClass(axiom.getArgument(2).toString(), errors);

                    OWLClassExpression restr = factory
                            .getOWLObjectExactCardinality(n, property.asOWLObjectProperty(), filler);

                    if (property != null && filler != null && target != null
                            && restr != null) {
                        _manager.manager.addAxiom(_ontology, factory.getOWLSubClassOfAxiom(target, restr));
                    }

                } else if (axiom.is(IAxiom.SOME_VALUES_FROM_RESTRICTION)) {

                    OWLEntity property = findProperty(axiom.getArgument(1)
                            .toString(), false, errors);
                    OWLClass target = findClass(axiom.getArgument(0).toString(), errors);
                    OWLClass filler = findClass(axiom.getArgument(2).toString(), errors);

                    OWLClassExpression restr = factory
                            .getOWLObjectSomeValuesFrom(property.asOWLObjectProperty(), filler);

                    if (property != null && filler != null && target != null
                            && restr != null) {
                        _manager.manager.addAxiom(_ontology, factory.getOWLSubClassOfAxiom(target, restr));
                    }

                } else if (axiom.is(IAxiom.DATATYPE_DEFINITION)) {

                } else if (axiom.is(IAxiom.DISJOINT_CLASSES)) {

                    Set<OWLClassExpression> classExpressions = new HashSet<OWLClassExpression>();
                    for (Object arg : axiom) {
                        OWLClass p = factory.getOWLClass(IRI.create(_prefix
                                + "#" + arg));
                        classExpressions.add(p);
                    }
                    _manager.manager.addAxiom(_ontology, factory
                            .getOWLDisjointClassesAxiom(classExpressions));

                } else if (axiom.is(IAxiom.ASYMMETRIC_OBJECT_PROPERTY)) {

                } else if (axiom.is(IAxiom.DIFFERENT_INDIVIDUALS)) {

                } else if (axiom.is(IAxiom.DISJOINT_OBJECT_PROPERTIES)) {

                } else if (axiom.is(IAxiom.DISJOINT_DATA_PROPERTIES)) {

                } else if (axiom.is(IAxiom.DISJOINT_UNION)) {

                } else if (axiom.is(IAxiom.EQUIVALENT_CLASSES)) {

                    Set<OWLClassExpression> classExpressions = new HashSet<OWLClassExpression>();
                    for (Object arg : axiom) {
                        OWLClass classExp = findClass(arg.toString(), errors);
                        classExpressions.add(classExp);
                    }
                    _manager.manager.addAxiom(_ontology, factory
                            .getOWLEquivalentClassesAxiom(classExpressions));

                } else if (axiom.is(IAxiom.EQUIVALENT_DATA_PROPERTIES)) {

                } else if (axiom.is(IAxiom.EQUIVALENT_OBJECT_PROPERTIES)) {

                } else if (axiom.is(IAxiom.FUNCTIONAL_DATA_PROPERTY)) {

                    OWLDataProperty prop = factory.getOWLDataProperty(IRI
                            .create(_prefix + "#" + axiom.getArgument(0)));
                    _manager.manager.addAxiom(_ontology, factory.getOWLFunctionalDataPropertyAxiom(prop));

                } else if (axiom.is(IAxiom.FUNCTIONAL_OBJECT_PROPERTY)) {

                    OWLObjectProperty prop = factory.getOWLObjectProperty(IRI
                            .create(_prefix + "#" + axiom.getArgument(0)));
                    _manager.manager.addAxiom(_ontology, factory.getOWLFunctionalObjectPropertyAxiom(prop));

                } else if (axiom.is(IAxiom.INVERSE_FUNCTIONAL_OBJECT_PROPERTY)) {

                } else if (axiom.is(IAxiom.INVERSE_OBJECT_PROPERTIES)) {

                } else if (axiom.is(IAxiom.IRREFLEXIVE_OBJECT_PROPERTY)) {

                } else if (axiom.is(IAxiom.NEGATIVE_DATA_PROPERTY_ASSERTION)) {

                } else if (axiom.is(IAxiom.NEGATIVE_OBJECT_PROPERTY_ASSERTION)) {

                } else if (axiom.is(IAxiom.REFLEXIVE_OBJECT_PROPERTY)) {

                } else if (axiom.is(IAxiom.SUB_ANNOTATION_PROPERTY_OF)) {

                } else if (axiom.is(IAxiom.SUB_DATA_PROPERTY)) {

                    OWLDataProperty subdprop = (OWLDataProperty) findProperty(axiom.getArgument(1)
                            .toString(), true, errors);
                    OWLDataProperty superdprop = (OWLDataProperty) findProperty(axiom.getArgument(0)
                            .toString(), true, errors);

                    if (subdprop != null && superdprop != null) {
                        _manager.manager.addAxiom(_ontology, factory
                                .getOWLSubDataPropertyOfAxiom(subdprop, superdprop));
                    }

                } else if (axiom.is(IAxiom.SUB_OBJECT_PROPERTY)) {

                    OWLObjectProperty suboprop = (OWLObjectProperty) findProperty(axiom.getArgument(1)
                            .toString(), false, errors);
                    OWLObjectProperty superoprop = (OWLObjectProperty) findProperty(axiom.getArgument(0)
                            .toString(), false, errors);

                    if (suboprop != null && superoprop != null) {
                        _manager.manager.addAxiom(_ontology, factory
                                .getOWLSubObjectPropertyOfAxiom(suboprop, superoprop));
                    }
                } else if (axiom.is(IAxiom.SUB_PROPERTY_CHAIN_OF)) {

                } else if (axiom.is(IAxiom.SYMMETRIC_OBJECT_PROPERTY)) {

                } else if (axiom.is(IAxiom.TRANSITIVE_OBJECT_PROPERTY)) {

                } else if (axiom.is(IAxiom.SWRL_RULE)) {

                } else if (axiom.is(IAxiom.HAS_KEY)) {

                } else if (axiom.is(IAxiom.ANNOTATION_ASSERTION)) {

                    OWLAnnotationProperty property = findAnnotationProperty(axiom.getArgument(1)
                            .toString(), errors);
                    Object value = axiom.getArgument(2);
                    OWLLiteral literal = null;
                    OWLEntity target = findKnowledge(axiom.getArgument(0).toString(), errors);

                    if (target != null) {

                        if (value instanceof String)
                            literal = factory
                                    .getOWLLiteral(org.integratedmodelling.common.utils.StringUtils
                                            .pack((String) value));
                        else if (value instanceof Integer)
                            literal = factory.getOWLLiteral((Integer) value);
                        else if (value instanceof Long)
                            literal = factory.getOWLLiteral((Long) value);
                        else if (value instanceof Float)
                            literal = factory.getOWLLiteral((Float) value);
                        else if (value instanceof Double)
                            literal = factory.getOWLLiteral((Double) value);
                        else if (value instanceof Boolean)
                            literal = factory.getOWLLiteral((Boolean) value);

                        /*
                         * TODO determine literal from type of value and property
                         */
                        if (property != null && literal != null) {
                            OWLAnnotation annotation = factory.getOWLAnnotation(property, literal);
                            _manager.manager.addAxiom(_ontology, factory
                                    .getOWLAnnotationAssertionAxiom(target.getIRI(), annotation));
                        }
                    }

                } else if (axiom.is(IAxiom.ANNOTATION_PROPERTY_DOMAIN)) {

                } else if (axiom.is(IAxiom.ANNOTATION_PROPERTY_RANGE)) {

                }

            } catch (OWLOntologyChangeException e) {
                errors.add(e.getMessage());
            }

        }

        scan();
        return errors;
    }

    /* must exist, can be property or class */
    private OWLEntity findKnowledge(String string, ArrayList<String> errors) {

        if (_conceptIDs.contains(string)) {
            return findClass(string, errors);
        } else if (_propertyIDs.contains(string)) {
            if (_apropertyIDs.contains(string)) {
                return findAnnotationProperty(string, errors);
            }
            return findProperty(string, _dpropertyIDs.contains(string), errors);
        }

        return null;
    }

    private OWLClass findClass(String c, ArrayList<String> errors) {

        OWLClass ret = null;

        if (c.contains(":")) {

            IConcept cc = _manager.getConcept(c);
            if (cc == null) {
                errors.add("concept " + cc + " not found");
                return null;
            }

            /*
             * ensure ontology is imported
             */
            if (!cc.getConceptSpace().equals(_id)
                    && !_imported.contains(cc.getConceptSpace())) {

                _imported.add(cc.getConceptSpace());
                IRI importIRI = ((Ontology) cc.getOntology())._ontology
                        .getOntologyID().getOntologyIRI();
                OWLImportsDeclaration importDeclaraton = _ontology
                        .getOWLOntologyManager().getOWLDataFactory()
                        .getOWLImportsDeclaration(importIRI);
                _manager.manager.applyChange(new AddImport(_ontology, importDeclaraton));
            }

            ret = ((Concept) cc)._owl;

        } else {

            ret = _ontology.getOWLOntologyManager().getOWLDataFactory()
                    .getOWLClass(IRI.create(_prefix + "#" + c));

            _conceptIDs.add(c);
        }

        return ret;
    }

    private OWLEntity findProperty(String c, boolean isData, ArrayList<String> errors) {

        OWLEntity ret = null;

        if (c.contains(":")) {

            IProperty cc = _manager.getProperty(c);
            if (cc == null) {
                errors.add("property " + cc + " not found");
                return null;
            }

            /*
             * ensure ontology is imported
             */
            if (!cc.getConceptSpace().equals(_id)
                    && !_imported.contains(cc.getConceptSpace())) {

                _imported.add(cc.getConceptSpace());
                IRI importIRI = ((Ontology) cc.getOntology())._ontology
                        .getOntologyID().getOntologyIRI();
                OWLImportsDeclaration importDeclaraton = _ontology
                        .getOWLOntologyManager().getOWLDataFactory()
                        .getOWLImportsDeclaration(importIRI);
                _manager.manager.applyChange(new AddImport(_ontology, importDeclaraton));
            }

            ret = ((Property) cc)._owl;

            if (isData && ret instanceof OWLObjectProperty) {
                throw new ThinklabRuntimeException(cc + " is an object property: data expected");
            }
            if (!isData && ret instanceof OWLDataProperty) {
                throw new ThinklabRuntimeException(cc + " is a data property: object expected");
            }

        } else {
            ret = isData ? _ontology.getOWLOntologyManager()
                    .getOWLDataFactory()
                    .getOWLDataProperty(IRI.create(_prefix + "#" + c))
                    : _ontology
                            .getOWLOntologyManager()
                            .getOWLDataFactory()
                            .getOWLObjectProperty(IRI.create(_prefix + "#" + c));

            if (isData) {
                _dpropertyIDs.add(c);
            } else {
                _opropertyIDs.add(c);
            }
            _propertyIDs.add(c);
        }

        return ret;
    }

    private OWLAnnotationProperty findAnnotationProperty(String c, ArrayList<String> errors) {

        OWLAnnotationProperty ret = null;

        if (c.contains(":")) {

            IProperty cc = _manager.getProperty(c);
            if (cc != null) {
                OWLEntity e = ((Property) cc)._owl;
                if (e instanceof OWLAnnotationProperty)
                    return (OWLAnnotationProperty) e;
            } else {
                SemanticType ct = new SemanticType(c);
                IOntology ontology = _manager.getOntology(ct.getConceptSpace());
                if (ontology == null) {
                    INamespace ns = KLAB.MMANAGER.getNamespace(ct
                            .getConceptSpace());
                    if (ns != null)
                        ontology = ns.getOntology();
                }
                if (ontology != null) {
                    ret = ((Ontology) ontology)._ontology
                            .getOWLOntologyManager()
                            .getOWLDataFactory()
                            .getOWLAnnotationProperty(IRI.create(((Ontology) ontology)._prefix
                                    + "#" + ct.getLocalName()));
                }
            }
        } else {
            ret = _ontology.getOWLOntologyManager().getOWLDataFactory()
                    .getOWLAnnotationProperty(IRI.create(_prefix + "#" + c));
            _apropertyIDs.add(c);
            _propertyIDs.add(c);
        }

        return ret;
    }

    @Override
    public IMetadata getMetadata() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public String getConceptSpace() {
        return _id;
    }

    @Override
    public int getConceptCount() {
        return _conceptIDs.size();
    }

    @Override
    public int getPropertyCount() {
        return _propertyIDs.size();
    }

    public void setResourceUrl(String string) {
        _resourceUrl = string;
    }

    /**
     * Return the URL of the resource this was read from, or null if it was
     * created by the API.
     * 
     * @return
     */
    public String getResourceUrl() {
        return _resourceUrl;
    }

    public IConcept createConcept(String newName) {

        IConcept ret = getConcept(newName);
        if (ret == null) {
            ArrayList<IAxiom> ax = new ArrayList<IAxiom>();
            ax.add(Axiom.ClassAssertion(newName));
            define(ax);
            ret = getConcept(newName);
        }
        return ret;

    }

    public IProperty createProperty(String newName, boolean isData) {

        IProperty ret = getProperty(newName);
        if (ret == null) {
            ArrayList<IAxiom> ax = new ArrayList<IAxiom>();
            ax.add(isData ? Axiom.DataPropertyAssertion(newName) : Axiom.ObjectPropertyAssertion(newName));
            define(ax);
            ret = getProperty(newName);
        }
        return ret;

    }

    public boolean isInternal() {
        return _isInternal;
    }

    void setInternal(boolean b) {
        _isInternal = b;
    }

    @Override
    public IKnowledge getAlias(IKnowledge alias) {
        return _aliases.get(alias);
    }

    public void alias(IKnowledge alias, IKnowledge aliased) {
        _aliases.put(alias, aliased);
    }
}
