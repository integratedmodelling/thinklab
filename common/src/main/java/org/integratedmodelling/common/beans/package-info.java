/**
 * This package contains the common beans for k.LAB 1.0 REST communication. These are built at the
 * server side and reconstructed by Jackson ObjectMapper at the client.
 * 
 * All implement the IModelBean tag interface for type checking. As passing the actual objects is in most cases
 * impossible due to necessary implementation differences between client and server, I prefer to
 * have a uniform strategy to keep code clean.
 * 
 * The Model factory (accessible to all through KLAB.MFACTORY) has adapter method to/from objects 
 * that can be used at client or server side to produce the corresponding versions of the actual
 * model objects.
 */
/**
 * @author ferdinando.villa
 *
 */
package org.integratedmodelling.common.beans; 