package org.integratedmodelling.common.beans;

import lombok.Data;

public @Data class Task {
    private long id;
}
