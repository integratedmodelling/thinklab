package org.integratedmodelling.common.beans;

import java.util.List;

import lombok.Data;

public @Data class Network {
    private long timestamp;
    private List<Node> nodes;
    private List<Relationship> structure;
}
