package org.integratedmodelling.common.beans;

import lombok.Data;
import lombok.EqualsAndHashCode;

@EqualsAndHashCode(callSuper=false)
public @Data class Component extends Directory {
    private String id;
}
