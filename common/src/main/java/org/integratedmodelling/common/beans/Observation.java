package org.integratedmodelling.common.beans;

import lombok.Data;

public @Data class Observation {
    private String     path;
    private String     parentPath;
    private Observable observable;
}
