package org.integratedmodelling.common.beans;

import lombok.Data;

public @Data class Grid {
    private double minX, minY, maxX, maxY;
    private int xDivs, yDivs;
}
