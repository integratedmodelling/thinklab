package org.integratedmodelling.common.beans;

import java.util.List;

import lombok.Data;

public @Data class Service  {
    private String id;
    private String description;
    private List<String> arguments;
    private List<String> argDescriptions;
    private List<String> returnTypes;
}
