package org.integratedmodelling.common.beans;

import java.util.List;

import lombok.Data;

public @Data class DirectObservation extends Observation {
    private String id;
    private List<State> states;
    private List<DirectObservation> children;
}
