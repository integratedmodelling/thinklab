// - BEGIN LICENSE: -3034066901776921602 -
// 
// Copyright (C) 2014-2015 by:
// - J. Luke Scott <luke@cron.works>
// - Ferdinando Villa <ferdinando.villa@bc3research.org>
// - any other authors listed in the various @author annotations
// 
// This program is free software; you can redistribute it and/or
// modify it under the terms of the Affero General Public License 
// Version 3 or any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// Affero General Public License for more details.
// 
// You should have received a copy of the Affero General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
// The license is also available at: https://www.gnu.org/licenses/agpl.html
// 
// - END LICENSE -
package org.integratedmodelling.common.beans.auth;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import lombok.Data;

public @Data class SystemAnnouncement {

    private String       id;
    private String subject;
    private String body;
    private Set<String> groups = new HashSet<>();
    private boolean isPublic;
    private List<String> sentToUsers = new ArrayList<>();
    private String started;
    private String lastSent;
}
