/**
 * These are straight copies of the ones in the collaboration codebase so that we can communicate auth results directly
 * without using straight JSON objects. Obviously this is not good as they need to be kept in sync.
 * 
 * FIXME the beans should be moved here as soon as the collaboration code integrates the k.LAB node; the annotations for
 * Mongo etc make it difficult to do so in the common package, as all those dependencies would need to come here too,
 * causing unnecessary bloat in the jar and OSGI bundle when common is used outside. 
 */
/**
 * @author ferdinando.villa
 *
 */
package org.integratedmodelling.common.beans.auth;
