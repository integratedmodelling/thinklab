package org.integratedmodelling.common.beans;

import java.util.List;

import lombok.Data;

/**
 * Represents status for either the general engine or a session, according to
 * who's calling.
 * 
 * @author ferdinando.villa
 *
 */
public @Data class Status {
    private int uptime;
    private String version;
    private String buildInfo;
    private long   bootTime;
    private long   totalMemory;
    private long   freeMemory;
    private int    availableProcessors;
    private List<Notification> notifications;
}
