package org.integratedmodelling.common.beans;

import java.io.File;

import org.integratedmodelling.api.auth.IUser;
import org.integratedmodelling.api.network.IComponent;
import org.integratedmodelling.api.project.IProject;
import org.integratedmodelling.api.runtime.IContext;
import org.integratedmodelling.api.runtime.ISession;

public class BeanFactory {

    /**
     * 
     * @param serverUrl
     * @param urn
     * @param topDirectory
     * @param user
     * @return
     */
    public File getDirectoryFromURN(String serverUrl, String urn, File topDirectory, IUser user) {
        return null;
    }

    /**
     * 
     * @param serverUrl
     * @param urn
     * @param directory
     * @param user
     * @return
     */
    public File getFileFromURN(String serverUrl, String urn, File directory, IUser user) {
        return null;
    }
    
    /**
     * 
     * @param serverUrl
     * @param urn
     * @param directory
     * @param user
     * @return
     */
    public IProject getProjectFromURN(String serverUrl, String urn, File directory, IUser user) {
        return null;
    }
    
    /**
     * 
     * @param serverUrl
     * @param urn
     * @param user
     * @return
     */
    public IComponent getComponentFromURN(String serverUrl, String urn, IUser user) {
        return null;
    }
    
    public IContext getContext(String serverUrl, int contextId, ISession session, IContext previous) {
        return null;
    }
    
}
