package org.integratedmodelling.common.beans;

import java.util.List;

import lombok.Data;

public @Data class Classification {
    private String           conceptSpace;
    private List<Classifier> classifiers;
}
