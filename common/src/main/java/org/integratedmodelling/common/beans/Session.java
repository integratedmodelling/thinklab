package org.integratedmodelling.common.beans;

import org.integratedmodelling.common.beans.auth.AuthenticationResultResource;

import lombok.Data;

public @Data class Session {
    private String                       id;
    private String                       authToken;
    private AuthenticationResultResource user;
}
