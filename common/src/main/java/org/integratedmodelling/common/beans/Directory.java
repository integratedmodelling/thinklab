package org.integratedmodelling.common.beans;

import java.util.List;

import org.integratedmodelling.api.modelling.IModelBean;

import lombok.Data;

/**
 * Describes a directory of file resources returned by a server. Each resource can be
 * accessed directly using the /get/file/URN method. If a filelist is present, it contains
 * hashes for comparison with a local version so that only the changed files have to be
 * transmitted.
 * 
 * @author ferdinando.villa
 *
 */
public @Data class Directory implements IModelBean {
    private FileResource       filelist;
    private List<FileResource> resources;
}
