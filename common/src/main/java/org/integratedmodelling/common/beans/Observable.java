package org.integratedmodelling.common.beans;

import lombok.Data;

public @Data class Observable {

    private String type;
    private String observationType;
    private String inherentType;
    private String observingSubject;
    private String byTrait;
    private int    downTo;

}
