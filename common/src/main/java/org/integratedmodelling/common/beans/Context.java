package org.integratedmodelling.common.beans;

import lombok.Data;

public @Data class Context {
    private int id;
    private boolean isDelta;
    private Subject subject;
}
