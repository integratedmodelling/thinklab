package org.integratedmodelling.common.beans;

import lombok.Data;

public @Data class Space extends Extent {
	private String crs;
	private String shape;
	private Grid grid;
}
