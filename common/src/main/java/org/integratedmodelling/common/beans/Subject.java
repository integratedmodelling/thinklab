package org.integratedmodelling.common.beans;

import java.util.List;

import lombok.Data;

public @Data class Subject extends DirectObservation {
    private List<Relationship> structure;
}
