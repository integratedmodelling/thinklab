// - BEGIN LICENSE: -3034066901776921602 -
// 
// Copyright (C) 2014-2015 by:
// - J. Luke Scott <luke@cron.works>
// - Ferdinando Villa <ferdinando.villa@bc3research.org>
// - any other authors listed in the various @author annotations
// 
// This program is free software; you can redistribute it and/or
// modify it under the terms of the Affero General Public License 
// Version 3 or any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// Affero General Public License for more details.
// 
// You should have received a copy of the Affero General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
// The license is also available at: https://www.gnu.org/licenses/agpl.html
// 
// - END LICENSE -
package org.integratedmodelling.common.beans.auth;

import java.util.HashSet;
import java.util.Set;

import lombok.Data;

/**
 * The user data we want "live" during a session. Returned upon successful login
 * and only kept at the server side.
 * @author Ferd
 * @author Luke
 */
public @Data class UserData  {

    public static final String GLOBAL_GROUP = "REGISTERED";

    String        id;
    String        username;
    String        email;
    String        affiliation;
    String        comments;
    String        firstName;
    String        lastName;
    String        initials;
    String        address;
    String        jobTitle;
    String        phone;
    String        serverUrl;
    String        registrationDate;
    String        lastLogin;
    String        lastEngineConnection;
    boolean       sendUpdates   = true;
    Set<Role>     roles         = new HashSet<>();                // LDAP security roles
    Set<String>   groups        = new HashSet<String>();          // research groups, etc. in web tool
    Set<String>   applications  = new HashSet<String>();
    AccountStatus accountStatus = AccountStatus.pendingActivation;

    public enum AccountStatus {
        active,
        locked,
        deleted,
        expired,
        pendingActivation,
    };
}
