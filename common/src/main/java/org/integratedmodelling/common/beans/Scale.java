package org.integratedmodelling.common.beans;

import java.util.List;

import org.integratedmodelling.api.modelling.IModelBean;

import lombok.Data;

public @Data class Scale implements IModelBean {
	private long multiplicity;
	private List<Extent> extents;
}
