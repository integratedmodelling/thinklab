package org.integratedmodelling.common.beans;

import lombok.Data;

public @Data class Time extends Extent {
    private long start;
    private long end;
    private long step;
}
