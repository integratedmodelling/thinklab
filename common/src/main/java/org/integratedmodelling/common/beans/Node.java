package org.integratedmodelling.common.beans;

import java.util.List;

import org.integratedmodelling.api.network.INode;

import lombok.Data;

public @Data class Node {
    private String          id;
    private String          authToken;
    private List<Service>   services;
    private List<Component> components;
    private boolean         canSearch;
    private boolean         canSubmit;
    
    public static Node getInstance(INode node) {
        Node ret = new Node();
        
        ret.id = node.getId();
        ret.authToken = node.getKey();
        
        return ret;
    }
}
