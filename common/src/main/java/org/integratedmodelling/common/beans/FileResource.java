package org.integratedmodelling.common.beans;

import org.integratedmodelling.api.modelling.IModelBean;

import lombok.Data;

public @Data class FileResource implements IModelBean {
    
    private String  urn;
    private String  path;
    private boolean isClasspath;
    private long    timestamp;
    
    public static FileResource newFromClasspath(String path, String locator, long timestamp) {
        
        FileResource ret = new FileResource();
        ret.path = path;
        ret.timestamp = timestamp;
        ret.urn = "local:classpath:" + locator;
        ret.isClasspath = true;
        return ret;
    }
    
    public static FileResource newFromFile(String path, String locator, long timestamp) {
        
        FileResource ret = new FileResource();
        ret.path = path;
        ret.timestamp = timestamp;
        ret.urn = "local:file:" + locator.replaceAll("\\/", "..");
        ret.isClasspath = false;
        return ret;
    }
}
