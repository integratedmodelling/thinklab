package org.integratedmodelling.common.beans;

import lombok.Data;

public @Data class Unit {
    private String specification;
}
