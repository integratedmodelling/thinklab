/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.common.model.runtime;

import java.util.Arrays;
import java.util.Collections;
import java.util.Iterator;

import org.integratedmodelling.api.knowledge.IConcept;
import org.integratedmodelling.api.modelling.IExtent;
import org.integratedmodelling.api.modelling.IScale;
import org.integratedmodelling.api.space.ISpatialExtent;
import org.integratedmodelling.api.time.ITemporalExtent;
import org.integratedmodelling.collections.ImmutableList;
import org.integratedmodelling.common.HashableObject;
import org.integratedmodelling.exceptions.ThinklabRuntimeException;

public abstract class AbstractScale extends HashableObject implements IScale {

    class ScaleIndex extends ImmutableList<Integer>implements Index {

        int[]             _dimensions;
        int               _dmax = -1;
        int               _dind = -1;
        int               _type = 0;  // 1 = temporal; 2 = spatial
        IExtent           _extent;
        Iterable<Integer> cursor;

        public ScaleIndex(Iterable<Integer> dimensionScanner, int dimIndex) {

            /*
             * TODO dimIndex may be -1, in which case we only specify ONE point and must
             * behave properly.
             */

            cursor = dimensionScanner;
            _dind = dimIndex;
            int i = 0;
            for (IExtent e : AbstractScale.this) {
                if (i == _dind) {
                    _dmax = (int) e.getMultiplicity();
                    if (e instanceof ITemporalExtent) {
                        _type = 1;
                    } else if (e instanceof ISpatialExtent) {
                        _type = 2;
                    }
                    _extent = e;
                    break;
                }
                i++;
            }
        }

        /*
         * creates an index that only has one offset
         */
        public ScaleIndex(int elementOffset) {
            cursor = Collections.singleton(elementOffset);
        }

        @Override
        public boolean equals(Object obj) {
            return obj instanceof ScaleIndex && Arrays.equals(_dimensions, ((ScaleIndex) obj)._dimensions);
        }

        @Override
        public int hashCode() {
            return Arrays.hashCode(_dimensions);
        }

        int getOffsetFor(int i) {
            _dimensions[_dind] = i;
            return getCursor().getElementOffset(_dimensions);
        }

        @Override
        public boolean contains(Object arg0) {
            throw new UnsupportedOperationException("operation not allowed");
        }

        @Override
        public Integer get(int i) {
            return getOffsetFor(i);
        }

        @Override
        public Iterator<Integer> iterator() {
            return cursor.iterator();
        }

        @Override
        public int size() {
            return _dmax;
        }

        @Override
        public Object[] toArray() {
            throw new UnsupportedOperationException("operation not allowed");
        }

        @Override
        public <T> T[] toArray(T[] arg0) {
            throw new UnsupportedOperationException("operation not allowed");
        }

        @Override
        public boolean isSpatial() {
            return _type == 1;
        }

        @Override
        public boolean isTemporal() {
            return _type == 2;
        }

        @Override
        public IConcept getDomainConcept() {
            return _extent.getDomainConcept();
        }

        @Override
        public int[] getOffsets() {
            int[] ret = _dimensions.clone();
            ret[_dind] = -1;
            return ret;
        }

        @Override
        public boolean isActive(int offset) {
            return _extent.isCovered(offset);
        }
    }

    @Override
    public final Index getIndex(int sliceIndex, int sliceNumber, Locator... locators) {

        int variableDimension = -1;
        int[] exts = new int[getExtentCount()];
        Arrays.fill(exts, IExtent.GENERIC_LOCATOR);
        int i = 0;
        for (IExtent e : this) {
            for (Locator o : locators) {
                int n = e.locate(o);
                if (n != IExtent.INAPPROPRIATE_LOCATOR) {
                    exts[i] = n;
                    break;
                }
            }
            i++;
        }

        /*
         * 
         */
        int nm = 0;
        for (i = 0; i < exts.length; i++) {
            if (exts[i] == IExtent.GENERIC_LOCATOR) {
                nm++;
                variableDimension = i;
            }
        }

        // if (nm == 0) {
        // return new ScaleIndex(getCursor().getElementOffset(exts));
        // }

        if (nm > 1) {
            throw new ThinklabRuntimeException("cannot iterate a scale along more than one dimensions");
        }

        return new ScaleIndex(getCursor()
                .getDimensionScanner(variableDimension, exts, sliceIndex, sliceNumber), variableDimension);
    }

    @Override
    public final Index getIndex(Locator... locators) {

        int variableDimension = -1;
        int[] exts = new int[getExtentCount()];
        Arrays.fill(exts, IExtent.GENERIC_LOCATOR);
        int i = 0;
        for (IExtent e : this) {
            for (Locator o : locators) {
                int n = e.locate(o);
                if (n != IExtent.INAPPROPRIATE_LOCATOR) {
                    exts[i] = n;
                    break;
                }
            }
            i++;
        }

        /*
         * 
         */
        int nm = 0;
        for (i = 0; i < exts.length; i++) {
            if (exts[i] == IExtent.GENERIC_LOCATOR) {
                nm++;
                variableDimension = i;
            }
        }

        if (nm == 0) {
            return new ScaleIndex(getCursor().getElementOffset(exts));
        }

        if (nm > 1) {
            throw new ThinklabRuntimeException("cannot iterate a scale along more than one dimensions");
        }

        return new ScaleIndex(getCursor().getDimensionScanner(variableDimension, exts), variableDimension);
    }

    @Override
    public int getExtentOffset(IExtent extent, int overallOffset) {
        int n = 0;
        boolean found = false;
        for (IExtent e : this) {
            if (e.getDomainConcept().equals(extent.getDomainConcept())) {
                found = true;
                break;
            }
            n++;
        }
        if (!found) {
            throw new ThinklabRuntimeException("cannot locate extent " + extent.getDomainConcept()
                    + " in scale");
        }
        return getCursor().getElementIndexes(overallOffset)[n];
    }

    @Override
    public boolean isTemporallyDistributed() {
        return getTime() != null && getTime().getMultiplicity() > 1;
    }

    @Override
    public boolean isSpatiallyDistributed() {
        return getSpace() != null && getSpace().getMultiplicity() > 1;
    }

    @Override
    public long locate(Locator... locators) {

        int[] loc = new int[getExtentCount()];
        int i = 0;
        for (IExtent e : this) {
            for (Locator l : locators) {
                int idx = e.locate(l);
                if (idx >= 0) {
                    loc[i++] = idx;
                    break;
                }
            }
        }
        return getCursor().getElementOffset(loc);
    }

    @Override
    public boolean isCovered(int offset) {
        int[] oofs = getExtentIndex(offset);
        for (int i = 0; i < getExtentCount(); i++) {
            if (!getExtent(i).isCovered(oofs[i])) {
                return false;
            }
        }
        return true;
    }

    @Override
    public boolean isConsistent() {
        for (int i = 0; i < getExtentCount(); i++) {
            if (!getExtent(i).isConsistent()) {
                return false;
            }
        }
        return true;
    }

}
