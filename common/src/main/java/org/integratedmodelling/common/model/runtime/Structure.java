/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.common.model.runtime;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.integratedmodelling.api.modelling.IObservation;
import org.integratedmodelling.api.modelling.IRelationship;
import org.integratedmodelling.api.modelling.IStructure;
import org.integratedmodelling.api.modelling.ISubject;
import org.integratedmodelling.common.vocabulary.NS;
import org.integratedmodelling.lang.IRemoteSerializable;
import org.jgrapht.graph.DefaultDirectedGraph;

public class Structure extends DefaultDirectedGraph<IObservation, IRelationship>implements IStructure,
        IRemoteSerializable {

    boolean           neverSeen = true;
    boolean           changed   = true;
    ISubject          subject   = null;
    Set<IObservation> added     = new HashSet<>();
    Set<IObservation> removed   = new HashSet<>();

    // holds the subjects and relationships before link() when this is created from the network.
    HashMap<String, ISubject> subCatalog = new HashMap<>();
    List<?>                   relCatalog = new ArrayList<>();

    public Structure(Map<?, ?> map) {
        super(IRelationship.class);
        for (Object o : (List<?>) map.get("subjects")) {
            subCatalog.put(((ISubject) o).getId(), (ISubject) o);
        }
        relCatalog = (List<?>) map.get("relationships");
    }

    /*
     * only to be called after the map constructor was used. Quite contorted.
     */
    public void link(ISubject root) {
        subject = root;
        subCatalog.put(root.getId(), root);
        for (ISubject s : subCatalog.values()) {
            addVertex(s);
        }
        if (relCatalog != null) {
            for (Object r : relCatalog) {
                ((IRelationship) r).link(this, subCatalog);
            }
        }

        subCatalog.clear();
        if (relCatalog != null) {
            relCatalog.clear();
        }
    }

    public Structure(ISubject subject) {
        super(IRelationship.class);
        this.subject = subject;
        addVertex(subject);
    }

    /**
     * Return a copy of this structure without the subjects that are connected only to
     * the root subject.
     * @return
     */
    public IStructure getConnectedGraph() {
        return null;
    }

    private static final long serialVersionUID = 204382567041257188L;

    @Override
    public Set<IObservation> getObservations() {
        return this.vertexSet();
    }

    @Override
    public Set<IRelationship> getRelationships() {
        return this.edgeSet();
    }

    @Override
    public Set<IRelationship> getOutgoingRelationships(IObservation observation) {
        return this.outgoingEdgesOf(observation);
    }

    @Override
    public Set<IRelationship> getIncomingRelationships(IObservation observation) {
        return this.incomingEdgesOf(observation);
    }

    public void addSubject(ISubject subject) {
        addVertex(subject);
    }

    @Override
    public void link(ISubject from, ISubject to, IRelationship relationship) {
        if (addVertex(from)) {
            changed = added.add(from);
        }
        if (addVertex(to)) {
            changed = added.add(to);
        }
        if (relationship != null) {
            added.add(relationship);
            addEdge(from, to, relationship);
            changed = true;
        }
    }

    @Override
    public void removeObservation(IObservation observation) {
        if (removeVertex(observation)) {
            removed.add(observation);
        }
        /*
         * TODO must add the removed relationships to the removed set. Not sure how that's
         * done with jGrapht.
         */
        changed = true;
    }

    /**
     * Return true if anything has been modified AND RESET THE CHANGED STATE - so that
     * the next hasChanged() will return false unless things change again after it's called.
     * 
     * @return
     */
    public boolean hasChanged() {
        return changed;
    }

    public void resetChanged() {
        changed = false;
        neverSeen = false;
        added.clear();
        removed.clear();
    }

    @Override
    public ISubject getSubject() {
        return subject;
    }

    @Override
    public Object adapt() {

        Map<String, Object> ret = new HashMap<>();

        // if (neverSeen) {

        ArrayList<ISubject> subjs = new ArrayList<>();
        for (IObservation o : vertexSet()) {
            if (!o.equals(subject)) {
                subjs.add((ISubject) o);
            }
        }

        /*
         * pass around the full contents
         */
        ret.put("subjects", subjs);
        ArrayList<IRelationship> rels = new ArrayList<>();
        for (IRelationship o : getRelationships()) {
            rels.add(o);
        }
        ret.put("relationships", rels);

        // } else {
        //
        // /*
        // * only pass the changes
        // */
        // ret.put("added", added);
        // ret.put("removed", removed);
        // }

        // TODO/FIXME: check that this is ok - every time we pass it around we reset changes, i.e. we
        // can only pass around changes once.
        // resetChanged();

        return ret;
    }

    /**
     * Return whether any of the relationships in this context is a flow relationship. This will
     * be used to enable flow-specific displays.
     * 
     * @return
     */
    public boolean hasFlows() {
        if (!NS.synchronize()) {
            return false;
        }
        for (IRelationship r : getRelationships()) {
            if (r.getObservable().is(NS.FLOW)) {
                return true;
            }
        }
        return false;
    }
}
