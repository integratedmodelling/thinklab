/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.common.model.actuators;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;

import org.integratedmodelling.api.metadata.IMetadata;
import org.integratedmodelling.api.modelling.IDirectObservation;
import org.integratedmodelling.api.modelling.IModel;
import org.integratedmodelling.api.modelling.IObservable;
import org.integratedmodelling.api.modelling.IObserver;
import org.integratedmodelling.api.modelling.IObservingObject;
import org.integratedmodelling.api.modelling.IState;
import org.integratedmodelling.api.modelling.resolution.IProvenance;
import org.integratedmodelling.api.modelling.runtime.IRawActuator;
import org.integratedmodelling.api.modelling.runtime.IStateActuator;
import org.integratedmodelling.api.modelling.scheduling.ITransition;
import org.integratedmodelling.api.modelling.storage.IDataset;
import org.integratedmodelling.common.kim.KIMObservable;
import org.integratedmodelling.common.kim.KIMObserver;
import org.integratedmodelling.common.states.State;
import org.integratedmodelling.common.states.States;
import org.integratedmodelling.common.vocabulary.NS;
import org.integratedmodelling.exceptions.ThinklabException;

public abstract class BaseStateActuator extends Actuator implements IStateActuator {

    protected HashMap<String, IObserver> _outputs    = new HashMap<String, IObserver>();
    protected HashMap<String, IObserver> _inputs     = new HashMap<String, IObserver>();
    ArrayList<String>                    _inputKeys  = new ArrayList<String>();
    ArrayList<String>                    _outputKeys = new ArrayList<String>();

    protected ArrayList<IState> _states;

    /**
     * Return the input formal names.
     * 
     * @return
     */
    protected List<String> getInputKeys() {
        return _inputKeys;
    }

    /**
     * Return all the output formal names. First one will be the main observable, equal to
     * getName().
     * 
     * @return
     */
    protected List<String> getOutputKeys() {
        return _outputKeys;
    }

    /**
     * Return the observer for the passed output keys.
     * 
     * @param key
     * @return
     */
    protected IObserver getObserverForOutput(String key) {
        return _outputs.get(key);
    }

    /**
     * This one will return the observer for the passed input key.
     * 
     * @param key
     * @return
     */
    protected IObserver getObserverForInput(String key) {
        return _inputs.get(key);
    }

    /**
     * Use this one to intercept the expected outputs. 
     * @param observable
     * @param observer
     * @param key
     * @param isMainObservable
     * @throws ThinklabException
     */
    protected void notifyOutput(IObservable observable, IObserver observer, String key, boolean isMainObservable)
            throws ThinklabException {
        // reimplement
    }

    /**
     * Use this one to intercept the available dependencies when process() is called.
     * 
     */
    protected void notifyInput(IObservable observable, IObserver observer, String key, boolean isMediated)
            throws ThinklabException {
    }

    @Override
    public final void notifyExpectedOutput(IObservable observable, IObserver observer, String key)
            throws ThinklabException {
        _outputKeys.add(key);
        IObserver oob = ((KIMObserver) observer).getRepresentativeObserver();
        _outputs.put(key, oob);
        notifyOutput(observable, oob, key, false);
    }

    @Override
    public final void notifyObserver(IObservable observable, IObserver observer) throws ThinklabException {
        _outputKeys.add(getName());
        IObserver oob = ((KIMObserver) observer).getRepresentativeObserver();
        _outputs.put(getName(), oob);
        notifyOutput(observable, oob, getName(), true);
    }

    @Override
    public final void notifyDependency(IObservable observable, IObserver observer, String key, boolean isMediation)
            throws ThinklabException {
        _inputKeys.add(key);
        _inputs.put(key, ((KIMObserver) observer).getRepresentativeObserver());
        notifyInput(observable, ((KIMObserver) observer).getRepresentativeObserver(), key, isMediation);
    }

    protected IDirectObservation getContext() {
        return context;
    }

    public Collection<IState> createStates(IDirectObservation context, IModel model, IObserver obs, IProvenance provenance, IDataset dataset) {

        this.context = context;

        /**
         * Add metadata from context once for all - process() will ensure these are
         * available to the actions.
         */
        if (context != null && contextMetadata == null) {
            contextMetadata = new HashMap<>();
            contextMetadata.put(IMetadata.IM_NAME, context.getId());
            for (String s : context.getMetadata().getKeys()) {
                contextMetadata.put(s, context.getMetadata().get(s));
            }
        }

        if (_outputs.size() == 0)
            return null;

        _states = new ArrayList<IState>();

        for (String key : _outputs.keySet()) {

            IObserver observer = _outputs.get(key);

            /*
             * label with the appropriate observable.
             */
            IObservingObject pkey = observer;
            IObservable c = new KIMObservable(observer);
            IState ret = new State(c, context.getScale(), dataset, hasTemporalActions(), context);
            String label = c.toString();

            if (this instanceof IRawActuator) {
                label = ((IRawActuator) this).getDatasourceLabel();
                ((State) ret).setRaw(true);
            }
            ret.getMetadata().put(NS.DISPLAY_LABEL_PROPERTY, label);

            IMetadata md = provenance.collectMetadata(obs);
            ret.getMetadata().merge(md, false);

            _states.add(ret);
        }

        return _states;
    }

    public void updateStates(int stateIndex, ITransition transition) {

        int i = 0;
        for (String key : _outputs.keySet()) {
            States.set(_states.get(i++), getValue(key), stateIndex);
            // _states.get(i++).getStorage().set(stateIndex, getValue(key));
        }
    }
}
