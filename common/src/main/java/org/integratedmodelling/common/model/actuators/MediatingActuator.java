/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.common.model.actuators;

import java.util.List;

import org.integratedmodelling.api.modelling.IAction;
import org.integratedmodelling.api.modelling.IObservable;
import org.integratedmodelling.api.modelling.IObserver;
import org.integratedmodelling.api.monitoring.IMonitor;
import org.integratedmodelling.exceptions.ThinklabException;

public abstract class MediatingActuator extends StateActuator {

    /*
     * if true, this accessor is used only to interpret data, so it should
     * not mediate.
     */
    boolean _interpreting;

    public MediatingActuator(List<IAction> actions, IMonitor monitor, boolean isInterpreting) {
        super(actions, monitor);
        _interpreting = isInterpreting;
    }

    IObserver _mediatedObserver = null;

    protected abstract Object mediate(Object object);

    @Override
    public void notifyInput(IObservable observable, IObserver observer, String key, boolean isMediation)
            throws ThinklabException {
        if (isMediation) {
            _mediatedObserver = observer;
        }
    }

    @Override
    public Object getValue(String outputKey) {
        return (!_interpreting && outputKey.equals(getName())) ? mediate(super.getValue(outputKey)) : super
                .getValue(outputKey);
    }

    protected IObserver getMediatedObserver() {
        return _mediatedObserver;
    }

}
