/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.common.model.actuators;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.integratedmodelling.api.modelling.IAction;
import org.integratedmodelling.api.modelling.IDirectObservation;
import org.integratedmodelling.api.modelling.runtime.IActuator;
import org.integratedmodelling.api.monitoring.IMonitor;
import org.integratedmodelling.api.monitoring.IMonitorable;
import org.integratedmodelling.common.HashableObject;
import org.integratedmodelling.common.kim.KIMAction;

public abstract class Actuator extends HashableObject implements IActuator, IMonitorable {

    protected String             name;
    protected ArrayList<IAction> initializationActions = new ArrayList<IAction>();
    protected ArrayList<IAction> temporalActions       = new ArrayList<IAction>();
    protected IMonitor           monitor;
    protected IDirectObservation context;

    // metadata from context that we want to make accessible to the actions.
    protected Map<String, Object> contextMetadata;

    public void addAction(IAction action) {
        if (action.getTargetStateId() == null)
            ((KIMAction) action).setTargetStateId(getName());
        if (action.getDomain() == null || action.getDomain().size() == 0) {
            initializationActions.add(action);
        } else {
            temporalActions.add(action);
        }
    }

    public void setContext(IDirectObservation context) {
        this.context = context;
    }

    public boolean hasTemporalActions() {
        return temporalActions.size() > 0;
    }

    @Override
    public void setMonitor(IMonitor monitor) {
        this.monitor = monitor;
    }

    @Override
    public IMonitor getMonitor() {
        return monitor;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public List<IAction> getActions() {
        return initializationActions;
    }

    @Override
    public boolean isTemporal() {
        return temporalActions.size() > 0;
    }

    /**
     * Use in non-temporal accessors to check if the initialized values should 
     * be recomputed after temporal transitions.
     * 
     * @return
     */
    protected boolean inputsChanged() {
        return true;
    }

}
