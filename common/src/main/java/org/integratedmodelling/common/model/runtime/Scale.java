/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.common.model.runtime;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.integratedmodelling.api.knowledge.IConcept;
import org.integratedmodelling.api.modelling.IExtent;
import org.integratedmodelling.api.modelling.IScale;
import org.integratedmodelling.api.modelling.ITopologicallyComparable;
import org.integratedmodelling.api.space.ISpatialExtent;
import org.integratedmodelling.api.time.ITemporalExtent;
import org.integratedmodelling.collections.MultidimensionalCursor;
import org.integratedmodelling.collections.MultidimensionalCursor.StorageOrdering;
import org.integratedmodelling.common.space.SpaceLocator;
import org.integratedmodelling.common.time.TimeLocator;
import org.integratedmodelling.exceptions.ThinklabException;
import org.integratedmodelling.exceptions.ThinklabRuntimeException;
import org.integratedmodelling.exceptions.ThinklabValidationException;
import org.integratedmodelling.lang.IRemoteSerializable;
import org.integratedmodelling.lang.LogicalConnector;

public class Scale extends AbstractScale implements IRemoteSerializable {

    long                           _multiplicity;
    ArrayList<IExtent>             _extents       = new ArrayList<IExtent>();
    ISpatialExtent                 _space;
    ITemporalExtent                _time;
    MultidimensionalCursor         _cursor;
    // the next != null means we derive from a previous scale and are representing one slice of it...
    private MultidimensionalCursor originalCursor = null;
    // ... identified by this offset...
    private int                    sliceOffset    = -1;
    // ... along this dimension
    private int                    sliceDimension = -1;

    // only for package-specific classes that know what they're doing.
    Scale() {
    }

    /**
     * Create a scale from an array of extents.
     * 
     * @param extents
     * @return
     */
    public static Scale create(IExtent... extents) {
        Scale ret = new Scale();
        ret.initialize(extents);
        return ret;
    }

    /*
     * quick access to "current" arbitrary state index for given offset - not in the API for now.
     */
    @Override
    public int[] getExtentIndex(int globalIndex) {
        return _cursor.getElementIndexes(globalIndex);
    }

    /**
     * Force the extents of the passed scale to a correspondent list of extents where
     * the existing ones have been forced to match the correspondent ones passed.
     * 
     * @param scale
     * @param extents
     * @return
     * @throws ThinklabException 
     */
    public static List<IExtent> forceExtents(IScale scale, IExtent... extents) throws ThinklabException {

        ArrayList<IExtent> ret = new ArrayList<>();
        for (IExtent e : scale) {
            if (extents != null && extents.length > 0) {
                for (IExtent forcing : extents) {
                    if (e.getDomainConcept().equals(forcing.getDomainConcept())) {
                        /*
                         * TODO we may want to set a parameter to make this behavior optional.
                         * Only force extents that are not already multiple, i.e. completely specified.
                         */
                        if (e.getMultiplicity() == 1) {
                            e = e.merge(forcing, true);
                        }
                        break;
                    }
                }
                ret.add(e);
            }
        }

        /*
         * add any extents that are sensible and weren't in the original scale
         */
        for (IExtent forcing : extents) {

            if (forcing.getMultiplicity() <= 0) {
                continue;
            }
            boolean found = false;
            for (IExtent e : scale) {
                if (e.getDomainConcept().equals(forcing.getDomainConcept())) {
                    found = true;
                    break;
                }
            }
            if (!found) {
                ret.add(forcing);
            }
        }
        return ret;
    }

    /**
     * Return a string that can be used to instantiate a Scale using the string constructor.
     * @param scale
     * @return
     */
    public static String asString(IScale scale) {
        String ret = "";
        ret += scale.getMultiplicity();
        for (IExtent e : scale) {

            ret += "#";

            if (e instanceof ISpatialExtent) {
                ret += "S|" + Space.asString((ISpatialExtent) e);
            } else if (e instanceof ITemporalExtent) {
                ret += "T|" + Time.asString((ITemporalExtent) e);
            } else {
                /* 
                 * TODO
                 * whatever - just concept and state list, hope for the best.
                 */
                throw new ThinklabRuntimeException("serializing non-t/s extents still unimplemented");
            }

        }
        return ret;
    }

    /**
     * For a service call
     * @param definition
     */
    public Scale(String definition) {

        String[] ss = definition.split("\\#");
        _multiplicity = Long.parseLong(ss[0]);
        for (int i = 1; i < ss.length; i++) {
            if (ss[i].startsWith("S|")) {
                _extents.add(_space = new Space(ss[i]));
            } else if (ss[i].startsWith("T|")) {
                _extents.add(_time = new Time(ss[i].substring(2)));
            } else {
                // TODO
            }
        }

        initializeCursor();

    }

    private void initializeCursor() {

        _cursor = new MultidimensionalCursor(StorageOrdering.ROW_FIRST);
        int[] dims = new int[_multiplicity == INFINITE ? _extents.size() - 1 : _extents.size()];
        int n = 0;
        for (int i = _multiplicity == INFINITE ? 1 : 0; i < _extents.size(); i++) {
            dims[n++] = (int) _extents.get(i).getMultiplicity();
        }
        _cursor.defineDimensions(dims);
    }

    /**
     * 
     * @param map
     */
    public Scale(Map<?, ?> map) {

        if (map.containsKey("multiplicity")) {
            _multiplicity = ((Number) map.get("multiplicity")).longValue();
        }
        if (map.containsKey("extents")) {
            @SuppressWarnings("unchecked")
            List<Object> extents = (List<Object>) map.get("extents");
            for (Object o : extents) {
                _extents.add((IExtent) o);
                if (o instanceof ISpatialExtent) {
                    _space = (ISpatialExtent) o;
                } else if (o instanceof ITemporalExtent) {
                    _time = (ITemporalExtent) o;
                }
            }
        }
        initializeCursor();
    }

    private Scale(IExtent[] extents, MultidimensionalCursor cursor, int oridx, int offset) {
        initialize(extents);
        originalCursor = cursor;
        sliceDimension = oridx;
        sliceOffset = offset;
    }

    protected void initialize(IExtent[] extents) {
        _multiplicity = 1;
        for (IExtent e : extents) {
            if (e == null) {
                continue;
            }
            _extents.add(e);
            if (e instanceof ISpatialExtent) {
                _space = (ISpatialExtent) e;
            } else if (e instanceof ITemporalExtent) {
                _time = (ITemporalExtent) e;
            }
            _multiplicity *= e.getMultiplicity();
        }
        initializeCursor();
    }

    @Override
    public Iterator<IExtent> iterator() {
        return _extents.iterator();
    }

    @Override
    public long getMultiplicity() {
        return _multiplicity;
    }

    @Override
    public IScale intersection(IScale other) throws ThinklabException {
        throw new ThinklabRuntimeException("illegal operation on client object");
    }

    @Override
    public IScale union(IScale other) throws ThinklabException {
        throw new ThinklabRuntimeException("illegal operation on client object");
    }

    @Override
    public boolean contains(IScale o) throws ThinklabException {
        throw new ThinklabRuntimeException("illegal operation on client object");
    }

    @Override
    public boolean overlaps(IScale o) throws ThinklabException {
        throw new ThinklabRuntimeException("illegal operation on client object");
    }

    @Override
    public boolean intersects(IScale o) throws ThinklabException {
        throw new ThinklabRuntimeException("illegal operation on client object");
    }

    @Override
    public ITopologicallyComparable<IScale> union(ITopologicallyComparable<?> other)
            throws ThinklabException {
        throw new ThinklabRuntimeException("illegal operation on client object");
    }

    @Override
    public ITopologicallyComparable<IScale> intersection(ITopologicallyComparable<?> other)
            throws ThinklabException {
        throw new ThinklabRuntimeException("illegal operation on client object");
    }

    @Override
    public double getCoveredExtent() {
        throw new ThinklabRuntimeException("illegal operation on client object");
    }

    @Override
    public ISpatialExtent getSpace() {
        return _space;
    }

    @Override
    public ITemporalExtent getTime() {
        return _time;
    }

    @Override
    public int getExtentCount() {
        return _extents.size();
    }

    @Override
    public IScale merge(IScale scale, LogicalConnector how, boolean adopt) throws ThinklabException {

        Scale other = (Scale) scale;
        Scale ret = new Scale();
        ArrayList<IExtent> common = new ArrayList<IExtent>();
        HashSet<IConcept> commonConcepts = new HashSet<IConcept>();

        for (IExtent e : this) {
            if (other.getExtent(e.getDomainConcept()) != null) {
                common.add(e);
                commonConcepts.add(e.getDomainConcept());
            } else {
                ret.mergeExtent(e, true);
            }
        }

        if (adopt) {
            for (IExtent e : other) {
                if (adopt && ret.getExtent(e.getDomainConcept()) == null
                        && !commonConcepts.contains(e.getDomainConcept())) {
                    ret.mergeExtent(e, true);
                }
            }
        }

        for (IExtent e : common) {
            IExtent oext = other.getExtent(e.getDomainConcept());
            IExtent merged = null;
            if (how.equals(LogicalConnector.INTERSECTION)) {
                merged = e.intersection(oext);
            } else if (how.equals(LogicalConnector.UNION)) {
                merged = e.union(oext);
            } else {
                throw new ThinklabValidationException("extents are being merged with illegal operator" + how);
            }
            ret.mergeExtent(merged, true);
        }

        return ret;
    }

    public void mergeExtent(IExtent extent, boolean force) throws ThinklabException {

        IExtent merged = null;
        int i = 0;
        for (IExtent e : _extents) {
            if (e.getDomainConcept().equals(extent.getDomainConcept())) {
                merged = e.merge(extent, force);
                break;
            }
            i++;
        }

        if (merged != null) {
            _extents.add(i, merged);
        } else {
            _extents.add(extent);
        }

        sort();
    }

    private void sort() {

        ArrayList<IExtent> order = new ArrayList<IExtent>(_extents);

        /*
         * Is it fair to think that if two extent concepts have an ordering relationship, they should know
         * about each other? So that we can implement the ordering as a relationship between extent
         * observation classes?
         *
         * For now, all we care about is that time, if present, comes first. We just check for that using the
         * ontology name, which of course sucks.
         */
        Collections.sort(order, new Comparator<IExtent>() {

            @Override
            public int compare(IExtent o1, IExtent o2) {
                // neg if o1 < o2
                boolean o1t = o1.getDomainConcept().getConceptSpace().equals("time");
                boolean o2t = o2.getDomainConcept().getConceptSpace().equals("time");
                if (o1t && !o2t) {
                    return -1;
                }
                if (!o1t && o2t) {
                    return 1;
                }
                return 0;
            }
        });

        _multiplicity = 1L;
        int idx = 0;
        for (IExtent e : order) {

            if (e.getMultiplicity() == INFINITE) {
                _multiplicity = INFINITE;
            }
            // if (e.getDomainConcept().equals(Time.domainConcept()))
            // _tIndex = idx;
            // if (e.getDomainConcept().equals(Geospace.get().SpatialDomain()))
            // _sIndex = idx;

            if (_multiplicity != INFINITE)
                _multiplicity *= e.getMultiplicity();

            idx++;
        }

        // better safe than sorry. Only time can be infinite so this should be pretty safe as long as
        // the comparator above works.
        if (_multiplicity == INFINITE && _extents.get(0).getMultiplicity() != INFINITE) {
            throw new ThinklabRuntimeException("internal error: infinite dimension not the first in scale");
        }

        // recompute strided offsets for quick extent access
        _cursor = new MultidimensionalCursor(StorageOrdering.ROW_FIRST);
        int[] dims = new int[_multiplicity == INFINITE ? _extents.size() - 1 : _extents.size()];
        int n = 0;
        for (int i = _multiplicity == INFINITE ? 1 : 0; i < _extents.size(); i++) {
            dims[n++] = (int) _extents.get(i).getMultiplicity();
        }
        _cursor.defineDimensions(dims);
        _extents = order;
    }

    @Override
    public boolean isEmpty() {
        return false;
    }

    @Override
    public IScale harmonize(IScale scale) throws ThinklabException {
        // TODO Auto-generated method stub
        return scale;
    }

    @Override
    public MultidimensionalCursor getCursor() {
        return _cursor;
    }

    @Override
    public IScale getSubscale(IConcept extent, int offset) {

        int oridx = -1;
        ArrayList<IExtent> exts = new ArrayList<>();
        for (int i = 0; i < _extents.size(); i++) {
            if (_extents.get(i).getDomainConcept().equals(extent)) {
                oridx = i;
                continue;
            }
            exts.add(_extents.get(i));
        }

        if (oridx < 0) {
            return this;
        }

        return new Scale(exts.toArray(new IExtent[exts.size()]), getCursor(), oridx, offset);
    }

    @Override
    public long getOriginalOffset(long subscaleOffset) {

        if (originalCursor == null) {
            return subscaleOffset;
        }

        int[] slcofs = getCursor().getElementIndexes((int) subscaleOffset);
        int[] orgofs = new int[originalCursor.getDimensionsCount()];
        int on = 0;
        for (int i = 0; i < orgofs.length; i++) {
            orgofs[i] = i == sliceDimension ? sliceOffset : slcofs[on++];
        }
        return originalCursor.getElementOffset(orgofs);
    }

    public static Locator[] parseLocators(String s) {
        String[] ll = s.split("\\|");
        Locator[] ret = new Locator[ll.length];
        int i = 0;
        for (String l : ll) {
            if (l.startsWith("S")) {
                ret[i++] = new SpaceLocator(l);
            } else if (l.startsWith("T")) {
                ret[i++] = new TimeLocator(l);
            }
        }
        return ret;
    }

    @Override
    public Object adapt() {
        Map<String, Object> ret = new HashMap<String, Object>();
        ret.put("extents", new ArrayList<IExtent>(_extents));
        ret.put("multiplicity", _multiplicity);
        return ret;
    }

    public static String locatorsAsText(Iterable<Locator> locators) {
        String ret = "";
        for (Locator l : locators) {
            ret += (ret.isEmpty() ? "" : "|") + l.asText();
        }
        return ret;
    }

    public IExtent[] getExtentArray() {
        return _extents.toArray(new IExtent[_extents.size()]);
    }

    @Override
    public IExtent getExtent(int index) {
        return _extents.get(index);
    }

    @Override
    public IExtent getExtent(IConcept domainConcept) {
        for (IExtent e : _extents) {
            if (e.getDomainConcept().equals(domainConcept)) {
                return e;
            }
        }
        return null;
    }
}
