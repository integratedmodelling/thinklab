/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.common.model.actuators;

import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.integratedmodelling.api.knowledge.IConcept;
import org.integratedmodelling.api.modelling.IAction;
import org.integratedmodelling.api.modelling.IDependency;
import org.integratedmodelling.api.modelling.IDirectObservation;
import org.integratedmodelling.api.modelling.IObservable;
import org.integratedmodelling.api.modelling.IObservation;
import org.integratedmodelling.api.modelling.IScale;
import org.integratedmodelling.api.modelling.IState;
import org.integratedmodelling.api.modelling.ISubject;
import org.integratedmodelling.api.modelling.resolution.IResolutionContext;
import org.integratedmodelling.api.modelling.runtime.ISubjectActuator;
import org.integratedmodelling.api.modelling.runtime.ISubjectInstantiator;
import org.integratedmodelling.api.modelling.scheduling.ITransition;
import org.integratedmodelling.api.monitoring.IMonitor;
import org.integratedmodelling.common.states.States;
import org.integratedmodelling.exceptions.ThinklabException;

public class SubjectInstantiationActuator extends DirectActuator<ISubject>implements ISubjectActuator {

    protected HashMap<IConcept, IState> inputs = new HashMap<IConcept, IState>();
    protected ISubject                  _subject;
    protected IScale                    _scale;
    protected ISubjectInstantiator      _ds;
    private Map<String, IState>         statesToInputs;

    public SubjectInstantiationActuator(ISubjectInstantiator ds,
            List<IAction> actions, IMonitor monitor) {
        _ds = ds;
        this.monitor = monitor;
        for (IAction action : actions) {
            this.addAction(action);
        }
    }

    protected IState getInput(IConcept concept) {
        for (IConcept c : inputs.keySet()) {
            if (c.is(concept)) {
                return inputs.get(c);
            }
        }
        return null;
    }

    protected Collection<IConcept> getInputKeys() {
        return inputs.keySet();
    }

    protected IState getInputState(IConcept observable) {
        return inputs.get(observable);
    }

    protected Collection<IState> getInputStates() {
        return inputs.values();
    }

    @Override
    public ISubject initialize(ISubject subject, IDirectObservation context, IResolutionContext resolutionContext, IMonitor monitor)
            throws ThinklabException {

        Map<String, IObservable> ins = new HashMap<>();
        Map<String, IObservable> ous = new HashMap<>();

        for (IDependency d : model.getDependencies()) {
            ins.put(d.getFormalName(), d.getObservable());
        }

        for (int i = 1; i < model.getObservables().size(); i++) {
            ous.put(model.getObservables().get(i).getFormalName(), model.getObservables().get(i));
        }

        monitor.info("initializing subject instantiator for " + model.getName(), null);

        IConcept subjectType = model.getObservable().getTypeAsConcept();
        Map<String, IObservation> result = _ds
                .initialize(subject, resolutionContext, subjectType, ins, ous, monitor);

        this.statesToInputs = States.matchStatesToInputs(subject, ins);

        monitor.info(result.size() + " new observations of " + subjectType
                + " created within " + subject.getId(), null);

        return subject;
    }

    @Override
    public boolean processTransition(ITransition transition, IMonitor monitor) throws ThinklabException {

        Map<String, IState> inps = new HashMap<>();
        for (String s : statesToInputs.keySet()) {
            if (States.hasChanged(statesToInputs.get(s))) {
                inps.put(s, statesToInputs.get(s));
            }
        }

        Map<String, IObservation> result = _ds.createSubjects(transition, inps);
        if (result != null) {
            for (String s : result.keySet()) {
                /*
                 * TODO resolve each direct observation
                 */
            }
        }
        return true;
    }
}
