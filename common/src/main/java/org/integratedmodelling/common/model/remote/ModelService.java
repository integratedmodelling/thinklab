/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.common.model.remote;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import org.integratedmodelling.api.knowledge.IKnowledge;
import org.integratedmodelling.api.knowledge.IProperty;
import org.integratedmodelling.api.metadata.IMetadata;
import org.integratedmodelling.api.modelling.IEvent;
import org.integratedmodelling.api.modelling.INamespace;
import org.integratedmodelling.api.modelling.IObservable;
import org.integratedmodelling.api.modelling.IObservation;
import org.integratedmodelling.api.modelling.IProcess;
import org.integratedmodelling.api.modelling.IRelationship;
import org.integratedmodelling.api.modelling.IScale;
import org.integratedmodelling.api.modelling.IScale.Locator;
import org.integratedmodelling.api.modelling.IState;
import org.integratedmodelling.api.modelling.IStructure;
import org.integratedmodelling.api.modelling.ISubject;
import org.integratedmodelling.api.modelling.runtime.IContextualizer;
import org.integratedmodelling.api.modelling.runtime.IEventInstantiator;
import org.integratedmodelling.api.modelling.runtime.IProcessContextualizer;
import org.integratedmodelling.api.modelling.runtime.IStateContextualizer;
import org.integratedmodelling.api.modelling.runtime.ISubjectContextualizer;
import org.integratedmodelling.api.modelling.runtime.ISubjectInstantiator;
import org.integratedmodelling.api.monitoring.IMonitor;
import org.integratedmodelling.api.services.IServiceCall;
import org.integratedmodelling.common.command.ServiceCall;
import org.integratedmodelling.common.configuration.KLAB;
import org.integratedmodelling.common.kim.ModelFactory;
import org.integratedmodelling.common.metadata.Metadata;
import org.integratedmodelling.common.model.runtime.Scale;
import org.integratedmodelling.common.model.runtime.Structure;
import org.integratedmodelling.common.model.runtime.Transition;
import org.integratedmodelling.common.states.State;
import org.integratedmodelling.common.states.States;
import org.integratedmodelling.common.vocabulary.NS;
import org.integratedmodelling.exceptions.ThinklabException;

/**
 * Wrapper for any IContextualizer to be executed as a remote model service. Automatically inserted by
 * ServiceManager when the situation warrants.
 * 
 * @author Ferd
 *
 */
public class ModelService {

    // fixed arguments for calls that support model services. These always go through and define the
    // scale and transition in the ServiceCall.
    public static final String SCALE_ARGUMENT              = "__s";
    public static final String MAIN_OBSERVABLE_ARGUMENT    = "__mo";
    public static final String CONTEXT_OBSERVABLE_ARGUMENT = "__co";
    public static final String TRANSITION_ARGUMENT         = "__t";
    public static final String STATE_INDEX_ARGUMENT        = "__i";
    public static final String MODEL_CONTEXT_ARGUMENT      = "__m";
    public static final String ACTUATOR_NAME               = "___nm";
    public static final String MODEL_INPUTS_ARGUMENT       = "__inp";
    public static final String MODEL_OUTPUTS_ARGUMENT      = "__out";

    private String                       name;
    private HashMap<String, IObservable> outputs             = new HashMap<>();
    private HashMap<String, IObservable> inputs              = new HashMap<>();
    private HashMap<String, IState>      inputStates         = new HashMap<>();
    private HashMap<String, Object>      inputValues         = new HashMap<>();
    private Scale                        scale;
    private Transition                   transition;
    private int                          index;
    private String                       modelContext;
    private IMonitor                     monitor;
    private ISubject                     mainSubject;
    private IProcess                     mainProcess;
    private ISubject                     contextSubject;
    private IContextualizer              contextualizer;
    private IObservable                  stateObservable;
    private IObservable                  instanceObservable;
    private IMetadata                    metadata            = new Metadata();
    private boolean                      stateCtxInitialized = false;

    public ModelService(IContextualizer contextualizer) {
        this.contextualizer = contextualizer;
    }

    private boolean parseCall(IServiceCall scall) throws ThinklabException {

        ServiceCall call = (ServiceCall) scall;

        this.transition = null;
        this.monitor = call.getMonitor();

        /*
         * model service call: interpret some arguments we know and let the other
         * internal arguments through without complaints. The rest of the code will
         * validate and pass the arguments that are recognized in the prototype.
         */
        String transitionSpecs = null;
        Map<String, Object> arguments = call.getArguments();
        for (String k : arguments.keySet()) {
            if (k.equals(ACTUATOR_NAME)) {
                this.name = arguments.get(k).toString();
            } else if (k.equals(SCALE_ARGUMENT)) {
                this.scale = new Scale(arguments.get(SCALE_ARGUMENT).toString());
            } else if (k.equals(TRANSITION_ARGUMENT)) {
                transitionSpecs = arguments.get(TRANSITION_ARGUMENT).toString();
            } else if (k.equals(STATE_INDEX_ARGUMENT)) {
                this.index = Integer.parseInt(arguments.get(STATE_INDEX_ARGUMENT).toString());
            } else if (k.equals(MODEL_CONTEXT_ARGUMENT)) {
                this.modelContext = arguments.get(MODEL_CONTEXT_ARGUMENT).toString();
            } else if (k.equals(MODEL_OUTPUTS_ARGUMENT)) {
                String[] outs = call.get(MODEL_OUTPUTS_ARGUMENT).toString().split("\\#");
                for (int i = 0; i < outs.length; i++) {
                    String name = outs[i];
                    IObservable observer = ModelFactory.deserializeObservable(outs[++i]);
                    outputs.put(name, observer);
                }
            } else if (k.equals(MODEL_INPUTS_ARGUMENT)) {
                String[] inps = call.get(MODEL_INPUTS_ARGUMENT).toString().split("\\#");
                for (int i = 0; i < inps.length; i++) {
                    String name = inps[i];
                    IObservable observer = ModelFactory.deserializeObservable(inps[++i]);
                    inputs.put(name, observer);
                }
            }
        }

        if (transitionSpecs != null) {
            this.transition =
            /*
             * FIXME this one does not behave well. Because this is used only in the
             * engine package, we should find a way to put this there and only use the
             * engine scale (I guess using an interface in ServiceCall).
             */
            new Transition(this.scale, Integer.parseInt(transitionSpecs), true);
        }

        for (String name : inputs.keySet()) {
            if (call.has("___" + name)) {
                /*
                 * TODO input values, too
                 * FIXME see what to do with dynamic state
                 */
                State state = new State(inputs.get(name), scale
                        .getSubscale(KLAB.c(NS.TIME_DOMAIN), transition == null ? -1
                                : transition.getTimeIndex()), false, mainSubject);
                state.getStorage().setBytes(call.get("___" + name).toString());
                inputStates.put(name, state);
            }
        }

        /*
         * create observables according to model context
         */
        if (call.has(MAIN_OBSERVABLE_ARGUMENT)) {
            IObservable mobs = ModelFactory.deserializeObservable(call.get(MAIN_OBSERVABLE_ARGUMENT)
                    .toString());
            switch (this.modelContext) {
            case "P":
                mainProcess = new Process(mobs);
                break;
            case "O":
                mainSubject = new Subject(mobs);
                break;
            case "E":
                // TODO
                break;
            case "S":
                stateObservable = mobs;
                break;
            case "I":
                instanceObservable = mobs;
                break;
            }
            if (call.has(CONTEXT_OBSERVABLE_ARGUMENT)) {
                contextSubject = new Subject(ModelFactory.deserializeObservable(call
                        .get(CONTEXT_OBSERVABLE_ARGUMENT).toString()));
            }
        }

        return this.transition == null;
    }

    public Object execute(IServiceCall call) throws ThinklabException {

        boolean isInit = parseCall(call);

        /*
         * TODO pass some resolution context here, with at least the scenarios.
         */

        switch (this.modelContext) {
        case "P":
            return processOutput(isInit ? ((IProcessContextualizer) contextualizer)
                    .initialize(mainProcess, contextSubject, null, inputs, outputs, call.getMonitor())
                    : ((IProcessContextualizer) contextualizer).compute(transition, inputStates), isInit);
        case "O":
            return processOutput(isInit ? ((ISubjectContextualizer) contextualizer)
                    .initialize(mainSubject, contextSubject, null, inputs, outputs, call.getMonitor())
                    : ((ISubjectContextualizer) contextualizer).compute(transition, inputStates), isInit);
        case "S":
            if (!stateCtxInitialized) {
                ((IStateContextualizer) contextualizer)
                        .define(name, stateObservable, mainSubject, null, inputs, outputs, call
                                .getMonitor());
            }
            return processStateValues(isInit ? ((IStateContextualizer) contextualizer)
                    .initialize(index, inputValues) : ((IStateContextualizer) contextualizer)
                            .compute(index, transition, inputValues));
        case "E":
            return processOutput(isInit
                    ? ((IEventInstantiator) contextualizer)
                            .initialize(mainSubject, null, inputs, call.getMonitor())
                    : ((IEventInstantiator) contextualizer).compute(transition, inputStates), isInit);
        case "I":
            return processOutput(isInit ? ((ISubjectInstantiator) contextualizer)
                    .initialize(mainSubject, null, instanceObservable
                            .getTypeAsConcept(), inputs, outputs, call
                                    .getMonitor())
                    : ((ISubjectInstantiator) contextualizer)
                            .createSubjects(transition, inputStates), isInit);
        }

        return null;
    }

    private Object processStateValues(Object object) {
        // TODO Auto-generated method stub
        return null;
    }

    private Object processOutput(Map<String, IObservation> map, boolean isInit)
            throws ThinklabException {

        Map<String, Object> ret = new HashMap<String, Object>();
        for (String s : map.keySet()) {

            if (map.get(s) instanceof State) {
                if (isInit) {
                    ret.put(s + "_dynamic", ((State) map.get(s)).getStorage().isDynamic() ? "true" : "false");
                }
                ret.put(s, ((State) map.get(s)).getStorage().getEncodedBytes());
            } else if (map.get(s) instanceof ISubject) {
                // TODO
            } else if (map.get(s) instanceof IRelationship) {
                // TODO
            } else if (map.get(s) instanceof IEvent) {
                // TODO
            } else if (map.get(s) instanceof IProcess) {
                // TODO
            }

        }
        return ret;
    }

    public boolean isDisposable() {
        return contextualizer.canDispose();
    }

    /*
     * proxies for subjects and processes. The state creation utils
     * should be able to check if this is meant to support a service,
     * and create states optimized for transfer rather than storage if so.
     */
    class Subject implements ISubject {

        IObservable              observable;
        Structure                structure;
        Map<IObservable, IState> states  = new HashMap<>();
        IScale                   myScale = null;
        String                   name    = null;

        Subject(IObservable observable) {
            this.observable = observable;
            structure = new Structure(this);
        }

        Subject(IObservable observable, IScale scale, String name) {
            this.observable = observable;
            this.myScale = scale;
            structure = new Structure(this);
        }

        @Override
        public IKnowledge getType() {
            return getObservable().getType();
        }

        @Override
        public IMetadata getMetadata() {
            return metadata;
        }

        @Override
        public IObservable getObservable() {
            return observable;
        }

        @Override
        public IScale getScale() {
            return myScale == null ? scale : myScale;
        }

        @Override
        public Collection<IEvent> getEvents() {
            return new ArrayList<IEvent>();
        }

        // @Override
        // public IKnowledge getDirectType() {
        // return getObservable().getType();
        // }
        //
        // @Override
        // public INamespace getNamespace() {
        // return null;
        // }
        //
        // @Override
        // public boolean is(Object other) {
        // // TODO Auto-generated method stub
        // return false;
        // }

        @Override
        public void setMonitor(IMonitor monitor) {
        }

        @Override
        public IMonitor getMonitor() {
            return monitor;
        }

        @Override
        public Collection<IState> getStates() {
            return inputStates.values();
        }

        @Override
        public Collection<ISubject> getSubjects() {
            return new ArrayList<ISubject>();
        }

        @Override
        public Collection<IProcess> getProcesses() {
            // TODO Auto-generated method stub
            return null;
        }

        @Override
        public String getId() {
            return name == null ? observable.getFormalName() : name;
        }

        @Override
        public IState getState(IObservable obs, Object... data) throws ThinklabException {
            if (states.get(obs) != null) {
                return states.get(obs);
            }
            IState ret = States.create(obs, this);
            states.put(obs, ret);
            return ret;
        }

        @Override
        public ISubject getSubject(IObservable obs) throws ThinklabException {
            // TODO Auto-generated method stub
            // AARGH don't cry when you get an NPE
            return null;
        }

        @Override
        public IState getStaticState(IObservable observable) throws ThinklabException {
            return States.createStatic(observable, this);
        }

        @Override
        public IEvent getEvent(IObservable obs) throws ThinklabException {
            // TODO Auto-generated method stub
            // AARGH don't cry when you get an NPE
            return null;
        }

        @Override
        public IProcess getProcess(IObservable obs) throws ThinklabException {
            // TODO Auto-generated method stub
            // AARGH don't cry when you get an NPE
            return null;
        }

        @Override
        public IStructure getStructure(Locator... locators) {
            return structure;
        }

        @Override
        public ISubject newSubject(IObservable observable, IScale scale, String name, IProperty relationship)
                throws ThinklabException {
            ISubject ret = new Subject(observable, scale, name);
            structure.addSubject(ret);
            // .link(this, ret, null /* new Observable(relationship) TODO - these should be linked based on
            // semantics, although they're not crucial to functioning for now. */);
            return ret;
        }

        @Override
        public IProcess newProcess(IObservable observable, IScale scale, String name)
                throws ThinklabException {
            return new Process(observable, this, scale, name);
        }

        @Override
        public IEvent newEvent(IObservable observable, IScale scale, String name) throws ThinklabException {
            return new Event(observable, this, scale, name);
        }

        @Override
        public INamespace getNamespace() {
            // TODO Auto-generated method stub
            return null;
        }

    }

    class Process implements IProcess {

        IObservable observable;
        ISubject    context = null;
        IScale      myScale = null;
        String      name    = "";

        Process(IObservable observable, ISubject context, IScale scale, String name) {
            this.observable = observable;
            this.context = context;
            this.myScale = scale;
            this.name = name;
        }

        Process(IObservable observable) {
            this.observable = observable;
        }

        @Override
        public IMetadata getMetadata() {
            return metadata;
        }

        @Override
        public IObservable getObservable() {
            return observable;
        }

        @Override
        public IScale getScale() {
            return myScale == null ? scale : myScale;
        }

        @Override
        public IKnowledge getType() {
            return getObservable().getType();
        }

        //
        // @Override
        // public IKnowledge getDirectType() {
        // return observable.getType();
        // }
        //
        // @Override
        // public INamespace getNamespace() {
        // // TODO Auto-generated method stub
        // return null;
        // }
        //
        // @Override
        // public boolean is(Object other) {
        // // TODO Auto-generated method stub
        // return false;
        // }

        @Override
        public void setMonitor(IMonitor monitor) {
        }

        @Override
        public IMonitor getMonitor() {
            return monitor;
        }

        @Override
        public ISubject getSubject() {
            return context == null ? contextSubject : context;
        }

        @Override
        public INamespace getNamespace() {
            // TODO Auto-generated method stub
            return null;
        }

        @Override
        public String getId() {
            return name;
        }

        @Override
        public Collection<IState> getStates() {
            // TODO Auto-generated method stub
            return null;
        }

        @Override
        public IState getState(IObservable observable, Object... data) throws ThinklabException {
            // TODO Auto-generated method stub
            return null;
        }

        @Override
        public IState getStaticState(IObservable observable) throws ThinklabException {
            // TODO Auto-generated method stub
            return null;
        }

    }

    class Event implements IEvent {

        IObservable observable;
        ISubject    context = null;
        IScale      myScale = null;
        String      name    = "";

        Event(IObservable observable, ISubject context, IScale scale, String name) {
            this.observable = observable;
            this.context = context;
            this.myScale = scale;
        }

        @Override
        public IMetadata getMetadata() {
            return metadata;
        }

        @Override
        public IObservable getObservable() {
            return observable;
        }

        @Override
        public IScale getScale() {
            return myScale == null ? scale : myScale;
        }

        @Override
        public IKnowledge getType() {
            return getObservable().getType();
        }

        // @Override
        // public IKnowledge getDirectType() {
        // return observable.getType();
        // }
        //
        // @Override
        // public INamespace getNamespace() {
        // // TODO Auto-generated method stub
        // return null;
        // }
        //
        // @Override
        // public boolean is(Object other) {
        // // TODO Auto-generated method stub
        // return false;
        // }

        @Override
        public void setMonitor(IMonitor monitor) {
        }

        @Override
        public IMonitor getMonitor() {
            return monitor;
        }

        @Override
        public ISubject getSubject() {
            return context == null ? contextSubject : context;
        }

        @Override
        public INamespace getNamespace() {
            // TODO Auto-generated method stub
            return null;
        }

        @Override
        public String getId() {
            return name;
        }

        @Override
        public Collection<IState> getStates() {
            // TODO Auto-generated method stub
            return null;
        }

        @Override
        public IState getState(IObservable observable, Object... data) throws ThinklabException {
            // TODO Auto-generated method stub
            return null;
        }

        @Override
        public IState getStaticState(IObservable observable) throws ThinklabException {
            // TODO Auto-generated method stub
            return null;
        }

    }
}
