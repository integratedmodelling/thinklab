/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.common.model.actuators;

import java.util.ArrayList;

import org.integratedmodelling.api.metadata.IMetadata;
import org.integratedmodelling.api.modelling.IAction;
import org.integratedmodelling.api.modelling.scheduling.ITransition;
import org.integratedmodelling.api.monitoring.IMonitor;
import org.integratedmodelling.common.states.State;
import org.integratedmodelling.exceptions.ThinklabException;

/**
 * Accessor that simply redirects existing state data to a computation.
 * 
 * @author Ferd
 *
 */
public class RedirectingStateActuator extends StateActuator {

    int   _idx;
    State state;

    /*
     * give it the ID of the model that produced it. If not, things will get out of whack when using as a
     * dependency in computing accessors.
     * 
     */
    public RedirectingStateActuator(State state, IMonitor monitor) {
        super(new ArrayList<IAction>(), monitor);
        this.state = state;
        String id = state.getMetadata().getString(IMetadata.DC_LABEL);
        if (id != null) {
            setName(id);
        }
    }

    @Override
    public void processState(int stateIndex, ITransition transition) throws ThinklabException {
        _idx = stateIndex;
    }

    @Override
    public Object getValue(String outputKey) {
        return state.getValue(_idx);
    }
}
