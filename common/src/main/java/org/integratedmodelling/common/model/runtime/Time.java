/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.common.model.runtime;

import java.util.Map;

import org.integratedmodelling.api.knowledge.IConcept;
import org.integratedmodelling.api.modelling.IExtent;
import org.integratedmodelling.api.modelling.IObservable;
import org.integratedmodelling.api.modelling.IScale.Locator;
import org.integratedmodelling.api.modelling.scheduling.ITransition;
import org.integratedmodelling.api.modelling.storage.IStorage;
import org.integratedmodelling.api.time.ITemporalExtent;
import org.integratedmodelling.api.time.ITimeDuration;
import org.integratedmodelling.api.time.ITimeInstant;
import org.integratedmodelling.api.time.ITimePeriod;
import org.integratedmodelling.common.configuration.KLAB;
import org.integratedmodelling.common.time.DurationValue;
import org.integratedmodelling.common.time.PeriodValue;
import org.integratedmodelling.common.time.TimeLocator;
import org.integratedmodelling.common.time.TimeValue;
import org.integratedmodelling.common.vocabulary.NS;
import org.integratedmodelling.exceptions.ThinklabException;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

/**
 * TODO/FIXME: must accommodate single-period extents in parsing from definition and elsewhere. For now it only
 * works with grids.
 * 
 * @author ferdinando.villa
 *
 */
public class Time extends Extent implements ITemporalExtent {

    long    _start;
    long    _end;
    long    _step      = 0;
    boolean _isForcing = false;

    /**
     * Return a partially specified time that only serves to complete or 
     * force another into a given grid size.
     * 
     * @param linearSize
     * @param unit
     * @return
     */
    public static Time getForcing(long start, long end, long step) {
        Time ret = new Time(start, end, step);
        ret._isForcing = true;
        return ret;
    }

    public boolean isForcing() {
        return _isForcing;
    }

    @Override
    public boolean isEmpty() {
        return _start == 0 && _end == 0;
    }

    public static ITemporalExtent create(long start, long end, long step) {

        if (start == 0 && end == 0) {
            return null;
        }

        if (step == 0) {
            return new PeriodValue(start, end);
        }

        return new Time(start, end, step);
    }

    public static String asString(ITemporalExtent e) {

        if (e instanceof Time && ((Time) e).isForcing()) {
            return "F!" + ((Time) e)._start + "," + ((Time) e)._end + "," + ((Time) e)._step;
        }

        String ret = "";
        ret += e.getValueCount() + ",";
        ret += e.getStart().getMillis() + ",";
        ret += e.getEnd().getMillis() + ",";
        ret += e.getStep().getMilliseconds();
        return ret;
    }

    public Time(String definition) {

        if (definition.startsWith("F!")) {
            String[] ss = definition.substring(2).split(",");
            _start = Long.parseLong(ss[0]);
            _end = Long.parseLong(ss[1]);
            _step = Long.parseLong(ss[2]);
            _isForcing = true;
        } else {
            String[] ss = definition.split(",");
            _multiplicity = Long.parseLong(ss[0]);
            _start = Long.parseLong(ss[1]);
            _end = Long.parseLong(ss[2]);
            _step = Long.parseLong(ss[3]);
            _domain = KLAB.c(NS.TIME_DOMAIN);
        }
    }

    public Time(long start, long end, long step) {
        super((Map<?, ?>) null);
        _start = start;
        _end = end;
        _step = step;
    }

    public Time(long time) {
        super((Map<?, ?>) null);
        _start = time;
        _end = time;
    }

    public Time(Map<?, ?> data) {
        super(data);
        if (data.containsKey("start")) {
            _start = ((Number) data.get("start")).longValue();
        }
        if (data.containsKey("end")) {
            _end = ((Number) data.get("end")).longValue();
        }

        if ((_end - _start) > 0) {
            _step = (_end - _start) / _multiplicity;
        }
    }

    @Override
    public String toString() {
        if (isEmpty()) {
            return "No temporal context";
        }

        /*
         * TODO improve
         */
        DateTimeFormatter format = DateTimeFormat.forPattern("dd/MM/YYYY");
        String dt1 = _start == 0 ? "up" : new DateTime(_start).toString(format);
        String dt2 = _end == 0 ? "infinite" : new DateTime(_end).toString(format);
        return dt1 + " to " + dt2 + (_step <= 0 ? "" : (" [" + getMultiplicity() + " steps]"));
    }

    @Override
    public ITimeInstant getStart() {
        return new TimeValue(_start);
    }

    @Override
    public long getMultiplicity() {
        return _step <= 0 ? 1 : ((_end - _start) / _step);
    }

    @Override
    public ITimeInstant getEnd() {
        return new TimeValue(_end);
    }

    @Override
    public ITemporalExtent getExtent(int index) {
        return (ITemporalExtent) (index == 0 ? getStart()
                : new PeriodValue(_start + (_step * index - 1), _start
                        + (_step * (index)), getDomainConcept()));
    }

    @Override
    public ITimePeriod collapse() {
        return null;
    }

    @Override
    public ITemporalExtent intersection(IExtent other) throws ThinklabException {
        return null;
    }

    @Override
    public ITimeDuration getStep() {
        return _step <= 0 ? null : new DurationValue(_step);
    }

    @Override
    public IStorage<?> getStorage() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public int locate(Locator locator) {
        if (locator == null)
            return 0;
        if (locator instanceof ITransition) {
            return ((ITransition) locator).getTimeIndex();
        }
        if (locator instanceof TimeLocator) {
            return ((TimeLocator) locator).isAll() ? GENERIC_LOCATOR : ((TimeLocator) locator).getSlice();
        }
        return INAPPROPRIATE_LOCATOR;
    }

    @Override
    public ITemporalExtent getExtent() {
        return new PeriodValue(_start, _end);
    }

    @Override
    public IConcept getDomainConcept() {
        return KLAB.c(NS.TIME_DOMAIN);
    }

    public static Time getForcing(ITemporalExtent time) {
        if (time == null) {
            return getForcing(0, 0, 0);
        } else {
            return getForcing(time.getStart() == null ? 0 : time.getStart().getMillis(), time.getEnd() == null
                    ? 0 : time.getEnd().getMillis(), time.getStep() == null ? 0
                            : time.getStep().getMilliseconds());
        }
    }

    @Override
    public Mediator getMediator(IExtent extent, IObservable observable, IConcept trait) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public boolean isConstant() {
        return getMultiplicity() == 1;
    }

}
