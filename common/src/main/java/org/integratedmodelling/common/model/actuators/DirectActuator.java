/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.common.model.actuators;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

import org.integratedmodelling.api.modelling.IDependency;
import org.integratedmodelling.api.modelling.IDirectObservation;
import org.integratedmodelling.api.modelling.IModel;
import org.integratedmodelling.api.modelling.IObservable;
import org.integratedmodelling.api.modelling.IObserver;
import org.integratedmodelling.api.modelling.IProcess;
import org.integratedmodelling.api.modelling.IState;
import org.integratedmodelling.api.modelling.ISubject;
import org.integratedmodelling.api.modelling.runtime.IDirectActuator;
import org.integratedmodelling.api.modelling.scheduling.ITransition;
import org.integratedmodelling.collections.Triple;
import org.integratedmodelling.common.command.ServiceCall;
import org.integratedmodelling.common.kim.ModelFactory;
import org.integratedmodelling.common.model.remote.ModelService;
import org.integratedmodelling.common.states.States;
import org.integratedmodelling.exceptions.ThinklabException;
import org.integratedmodelling.exceptions.ThinklabIOException;
import org.integratedmodelling.exceptions.ThinklabRemoteException;

public abstract class DirectActuator<T extends IDirectObservation> extends Actuator implements
        IDirectActuator<T> {

    private ArrayList<Triple<String, IObserver, IObservable>> outputs           = new ArrayList<>();
    protected IModel                                          model;
    private Set<IObservable>                                  outputObservables = new HashSet<>();

    // we pass this to a remote service through the parameters so it may choose to give us the same
    // object for different calls to the same service.
    String _remoteServiceId = UUID.randomUUID()
            .toString();

    protected IModel getModel() {
        return model;
    }

    /**
     * used only by remote accessors.
     * 
     * @param initialParameters
     */
    protected Map<String, Object> getInitializationParameters(Map<String, Object> initialParameters) {

        Map<String, Object> ret = new HashMap<>(initialParameters);

        String out = "";
        for (Triple<String, IObserver, IObservable> o : outputs) {
            out += (out.isEmpty() ? "" : "#") + o.getFirst() + "#"
                    + ModelFactory.serialize(o.getThird());
        }

        ret.put("__out", out);
        ret.put(ServiceCall.EXECUTOR_ID_ARGUMENT, _remoteServiceId);

        return ret;
    }

    /**
     * Post the input states for a process or subject remote accessor. If we're not initializing only
     * post what has changed (TODO).
     * 
     * @param context
     * @param parameters
     * @param transition
     * @return
     * @throws ThinklabIOException
     */
    protected Map<String, Object> postInputs(IDirectObservation context, Map<String, Object> parameters, ITransition transition)
            throws ThinklabException {
        Map<String, Object> ret = new HashMap<>(parameters);
        ret.put(ServiceCall.EXECUTOR_ID_ARGUMENT, _remoteServiceId);
        String iobservers = "";
        for (IState state : context.getStates()) {

            if (outputObservables.contains(state.getObservable())) {
                continue;
            }

            /*
             * do not post static states unless things have changed.
             */
            if (transition != null && !States.hasChanged(state)) {
                continue;
            }

            // match to model dependency to find name
            String name = null;
            for (IDependency d : model.getDependencies()) {
                if (d.getObservable().equals(state.getObservable())) {
                    name = d.getFormalName();
                    break;
                }
            }
            if (name != null) {
                iobservers += (iobservers.isEmpty() ? "" : "#") + name + "#"
                        + ModelFactory.serialize(state.getObservable());
                ret.put("___" + name, state.getStorage().getEncodedBytes(transition));
            }
        }
        if (!iobservers.isEmpty()) {
            ret.put(ModelService.MODEL_INPUTS_ARGUMENT, iobservers);
        }

        return ret;
    }

    /**
     * Used only in remote accessors to extract the results (states etc) from the returned
     * values and set them in the status of the object.
     * 
     * @param result
     * @throws ThinklabException 
     */
    protected void processRemoteStates(IDirectObservation target, Map<?, ?> result, ITransition transition)
            throws ThinklabException {

        for (Triple<String, IObserver, IObservable> out : outputs) {
            if (result.containsKey(out.getFirst())) {

                Object o = result.get(out.getFirst());
                Object d = result.get(out.getFirst() + "_dynamic");

                if (o == null || (d == null && transition == null)) {
                    throw new ThinklabRemoteException("remote service did not produce output "
                            + out.getFirst());
                }

                boolean isDynamic = d == null || d.equals("true");
                IState state = null;
                IDirectObservation stateCreator = target instanceof IProcess ? ((IProcess) target)
                        .getSubject()
                        : (ISubject) target;
                state = isDynamic ? stateCreator.getState(out.getThird()) : stateCreator.getStaticState(out
                        .getThird());
                state.getStorage().setBytes(o.toString(), transition);
            }
        }
    }

    @Override
    public void notifyExpectedOutput(IObservable observable, IObserver observer, String name) {
        outputs.add(new Triple<String, IObserver, IObservable>(name, observer, observable));
        outputObservables.add(observable);
    }

    @Override
    public void notifyModel(IModel model) {
        this.model = model;
    }

}
