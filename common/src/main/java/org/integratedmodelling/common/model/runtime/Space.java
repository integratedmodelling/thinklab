/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.common.model.runtime;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.integratedmodelling.api.knowledge.IConcept;
import org.integratedmodelling.api.modelling.IExtent;
import org.integratedmodelling.api.modelling.IObservable;
import org.integratedmodelling.api.modelling.IScale.Locator;
import org.integratedmodelling.api.modelling.IUnit;
import org.integratedmodelling.api.modelling.storage.IStorage;
import org.integratedmodelling.api.space.IGrid;
import org.integratedmodelling.api.space.IShape;
import org.integratedmodelling.api.space.ISpatialExtent;
import org.integratedmodelling.api.space.ISpatialIndex;
import org.integratedmodelling.api.space.ITessellation;
import org.integratedmodelling.collections.Path;
import org.integratedmodelling.common.configuration.KLAB;
import org.integratedmodelling.common.space.IGeometricShape;
import org.integratedmodelling.common.space.ShapeValue;
import org.integratedmodelling.common.space.SpaceLocator;
import org.integratedmodelling.common.utils.NumberUtils;
import org.integratedmodelling.common.vocabulary.NS;
import org.integratedmodelling.common.vocabulary.Unit;
import org.integratedmodelling.exceptions.ThinklabRuntimeException;
import org.integratedmodelling.lang.IRemoteSerializable;

import com.vividsolutions.jts.geom.Envelope;
import com.vividsolutions.jts.geom.Geometry;
import com.vividsolutions.jts.io.ParseException;
import com.vividsolutions.jts.io.WKTReader;

public class Space extends Extent implements ISpatialExtent, IGeometricShape, IRemoteSerializable {

    double                _maxx, _maxy, _minx, _miny;
    double                _cellHeight, _cellLength;
    String                _crs;
    private int           _gridx;
    private int           _gridy;
    private String        _shapedef;
    private Grid          _grid;
    private Tessellation  _tessellation;
    private ShapeValue    _shape;
    private IUnit         _forcingUnit;
    private double        _forcingSize;
    private ISpatialIndex index;

    @Override
    public Object adapt() {

        Map<String, Object> ret = new HashMap<String, Object>();

        ret.put("multiplicity", getValueCount());
        ret.put("domain", getDomainConcept().toString());

        ret.put("minx", _minx);
        ret.put("miny", _miny);
        ret.put("maxx", _maxx);
        ret.put("maxy", _maxy);

        if (_grid != null) {
            ret.put("grid", _grid.adapt());
        }

        ShapeValue shape = getShape();
        if (shape != null) {
            ret.put("shape", shape.toString());
        }
        ret.put("crs", _crs);
        return ret;
    }

    /**
     * Return a partially specified space that only serves to complete or 
     * force another into a given grid size.
     * 
     * @param linearSize
     * @param unit
     * @return
     */
    public static Space getForcing(double linearSize, String unit) {
        return new Space(linearSize, new Unit(unit));
    }

    @Override
    public boolean isConsistent() {
        return !getShape().isEmpty() && getShape().isValid();
    }

    class Grid implements IGrid, IRemoteSerializable {

        public Grid() {
        }

        @Override
        public Object adapt() {
            Map<String, Object> ret = new HashMap<String, Object>();
            ret.put("xdivs", _gridx);
            ret.put("ydivs", _gridy);
            if (_shapedef != null) {
                ret.put("shape", _shapedef.toString());
            }
            return ret;
        }

        public Grid(String string) {
            String[] ss = string.split(",");
            _gridx = Integer.parseInt(ss[1]);
            _gridy = Integer.parseInt(ss[2]);
        }

        @Override
        public int getYCells() {
            return _gridy;
        }

        @Override
        public int getXCells() {
            return _gridx;
        }

        @Override
        public int getCellCount() {
            return _gridx * _gridy;
        }

        @Override
        public int getOffset(int x, int y) {
            return (y * getXCells()) + x;
        }

        @Override
        public int[] getXYOffsets(int index) {
            int xx = index % getXCells();
            int yy = getYCells() - (index / getXCells()) - 1;
            return new int[] { xx, yy };
        }

        public double[] getCoordinatesAt(int x, int y) {
            double x1 = getMinX() + (_cellLength * x);
            double y1 = getMinY() + (_cellHeight * (getYCells() - y - 1));
            return new double[] { x1 + (_cellLength / 2), y1 + (_cellHeight / 2) };
        }

        @Override
        public double[] getCoordinates(int index) {
            int[] xy = getXYOffsets(index);
            return getCoordinatesAt(xy[0], xy[1]);
        }

        @Override
        public int getOffsetFromWorldCoordinates(double x, double y) {
            if (x < _minx || x > _maxx || y < _miny || y > _maxy)
                return -1;
            int xx = (int) (((x - _minx) / (_maxx - _minx)) * _gridx);
            int yy = (int) (((y - _miny) / (_maxy - _miny)) * _gridy);
            return getIndex(xx, yy);
        }

        @Override
        public boolean isActive(int x, int y) {
            // TODO Auto-generated method stub
            return true;
        }

        @Override
        public Locator getLocator(int x, int y) {
            return new SpaceLocator(x, _gridy - y - 1);
        }

        @Override
        public double getMinX() {
            return _minx;
        }

        @Override
        public double getMaxX() {
            return _maxx;
        }

        @Override
        public double getMinY() {
            return _miny;
        }

        @Override
        public double getMaxY() {
            return _maxy;
        }

        @Override
        public Iterator<Cell> iterator() {

            return new Iterator<Cell>() {

                int n = 0;

                @Override
                public boolean hasNext() {
                    return n < getCellCount();
                }

                @Override
                public Cell next() {
                    return getCell(n++);
                }

                @Override
                public void remove() {
                }

            };
        }

        protected Cell getCell(int i) {
            // TODO Auto-generated method stub
            return null;
        }

        @Override
        public double getCellWidth() {
            return (_maxx - _minx) / _gridx;
        }

        @Override
        public double getCellHeight() {
            return (_maxy - _miny) / _gridy;
        }

    }

    class Tessellation implements ITessellation {

        List<IShape> shapes = new ArrayList<IShape>();

        public Tessellation(String string) {
            // TODO Auto-generated constructor stub
        }

        @Override
        public Iterator<IShape> iterator() {
            return shapes.iterator();
        }
    }

    public String getShapeDefinition() {
        return _shapedef;
    }

    public String getCRSDefinition() {
        return _crs;
    }

    @Override
    public boolean equals(Object o) {
        if (!(o instanceof Space)) {
            return false;
        }
        Space os = (Space) o;

        // TODO compare shape and representation
        return NumberUtils.equal(_maxx, os._maxx) && NumberUtils.equal(_minx, os._minx)
                && NumberUtils.equal(_miny, os._miny) && NumberUtils.equal(_maxy, os._maxy);

    }

    public Space(String definition) {
        parse(definition);
    }

    public Space(ShapeValue shape) {

        _shape = shape;
        _shapedef = shape.asText();
        _crs = shape.getCRS();
        _domain = KLAB.c(NS.SPACE_DOMAIN);
        Envelope envelope = _shape.getEnvelope();
        _maxx = envelope.getMaxX();
        _maxy = envelope.getMaxY();
        _minx = envelope.getMinX();
        _miny = envelope.getMinY();

        _multiplicity = 1;
    }

    /**
     * This will not create a usable space, but just store the desired grid
     * resolution. In order to use this, it will need to be applied to the
     * shape of another spatial extent.
     * 
     * @param linearSize
     * @param unit
     */
    private Space(double linearSize, IUnit unit) {
        _forcingUnit = unit;
        _forcingSize = linearSize;
    }

    public boolean isForcing() {
        return _forcingUnit != null && _forcingSize != 0;
    }

    /*
     * structure is in field separated by |
     * field content
     *     0 multiplicity
     *     1 srs
     *     2 minx,miny,maxx,maxy
     *     3 G,x,y if grid; T,wkt1,... if tessellation; N if single extent w/o spatial structure
     *    [4 shape - not added for now]
     *    [5 encoded activation layer - not added for now]
     */
    public static String asString(ISpatialExtent e) {

        if (e instanceof Space && ((Space) e).isForcing()) {
            return "F!" + ((Space) e)._forcingSize + "|" + ((Space) e)._forcingUnit;
        }

        String ret = "";
        ret += e.getValueCount() + "|";
        ret += e.getCRSCode() + "|";
        ret += e.getMinX() + "," + e.getMinY() + "," + e.getMaxX() + "," + e.getMaxY() + "|";
        ret += e.getShape().asText() + "|";
        if (e.getGrid() != null) {
            ret += "G," + e.getGrid().getXCells() + "," + e.getGrid().getYCells();
        } else if (e.getTessellation() != null) {
            // TODO add shapes
            ret += "T";
        } else {
            ret += "N";
        }
        return ret;
    }

    public Space(Map<?, ?> data) {
        super(data);

        if (data.containsKey("shape")) {
            _shapedef = data.get("shape").toString();
        }
        if (data.get("crs") != null) {
            _crs = data.get("crs").toString();
        }
        _maxx = ((Number) data.get("maxx")).doubleValue();
        _maxy = ((Number) data.get("maxy")).doubleValue();
        _minx = ((Number) data.get("minx")).doubleValue();
        _miny = ((Number) data.get("miny")).doubleValue();

        if (data.containsKey("grid")) {
            Map<?, ?> grid = (Map<?, ?>) data.get("grid");
            _gridx = ((Number) grid.get("xdivs")).intValue();
            _gridy = ((Number) grid.get("ydivs")).intValue();
            _grid = new Grid();
            _cellLength = (_maxx - _minx) / _gridx;
            _cellHeight = (_maxy - _miny) / _gridy;
        }
    }

    @Override
    public String toString() {

        if (isForcing()) {
            return "Force " + _forcingSize + " " + _forcingUnit + " grid if unassigned";
        }

        /*
         * TODO improve
         */
        if (_gridx != 0) {
            return "Grid " + _gridx + " by " + _gridy + " [" + getMultiplicity() + " grid cells]";
        }
        return _forcingUnit != null ? "As defined" : "Uniform location";
    }

    @Override
    public double getMinX() {
        return _minx;
    }

    @Override
    public double getMinY() {
        return _miny;
    }

    @Override
    public double getMaxX() {
        return _maxx;
    }

    @Override
    public double getMaxY() {
        return _maxy;
    }

    @Override
    public ISpatialExtent getExtent(int index) {
        return null;
    }

    public int getIndex(int x, int y) {
        if (_gridx != 0) {
            return (y * _gridx) + x;
        }
        return 0;
    }

    public double getWidth() {
        return getMaxX() - getMinX();
    }

    public double getHeight() {
        return getMaxY() - getMinY();
    }

    @Override
    public String getCRSCode() {
        return _crs;
    }

    @Override
    public IGrid getGrid() {
        return _grid;
    }

    @Override
    public ITessellation getTessellation() {
        return _tessellation;
    }

    @Override
    public IStorage<?> getStorage() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public int locate(Locator locator) {
        if (locator instanceof SpaceLocator) {
            if (locator.isAll())
                return GENERIC_LOCATOR;
            if (_grid != null) {
                if (((SpaceLocator) locator).isLatLon()) {
                    return getGrid()
                            .getOffsetFromWorldCoordinates(((SpaceLocator) locator).lon, ((SpaceLocator) locator).lat);
                } else {
                    return getIndex(((SpaceLocator) locator).x, ((SpaceLocator) locator).y);
                }
            }
        }
        return INAPPROPRIATE_LOCATOR;
    }

    @Override
    public ShapeValue getShape() {
        if (_shape == null) {
            if (_shapedef != null) {
                try {
                    _shape = new ShapeValue(new WKTReader().read(_shapedef));
                } catch (ParseException e) {
                    throw new ThinklabRuntimeException(e);
                }
            }
            /*
             * last resort - shape is the bounding box or point.
             */
            if (_shape /* still */ == null) {
                _shape = new ShapeValue(_minx, _miny, _maxx, _maxy);
            }
        }
        return _shape;
    }

    @Override
    public ISpatialExtent getExtent() {
        return new Space(getShape());
    }

    @Override
    public Geometry getStandardizedGeometry() {
        return getShape().getStandardizedGeometry();
    }

    @Override
    public Geometry getGeometry() {
        return getShape().getGeometry();
    }

    public void setForcing(double size, String unit) {
        _forcingSize = size;
        _forcingUnit = new Unit(unit);
    }

    public IUnit getForcingUnit() {
        return _forcingUnit;
    }

    public double getForcingSize() {
        return _forcingSize;
    }

    public String getForcingDefinition() {
        return _forcingSize + " " + _forcingUnit;
    }

    @Override
    public IConcept getDomainConcept() {
        return KLAB.c(NS.SPACE_DOMAIN);
    }

    public void parse(String definition) {

        if (definition.startsWith("F!")) {
            String[] ss = definition.substring(2).split("\\|");
            _forcingSize = Double.parseDouble(ss[0]);
            _forcingUnit = new Unit(ss[1]);
        } else {
            String[] ss = definition.split("\\|");
            _domain = KLAB.c(NS.SPACE_DOMAIN);
            _multiplicity = Long.parseLong(ss[1]);
            _crs = ss[2];
            String[] sz = ss[3].split(",");
            _minx = Double.parseDouble(sz[0]);
            _miny = Double.parseDouble(sz[1]);
            _maxx = Double.parseDouble(sz[2]);
            _maxy = Double.parseDouble(sz[3]);
            String shapedef = ss[4].trim();
            if (!shapedef.isEmpty()) {
                _shape = new ShapeValue(shapedef);
            }
            if (ss[5].startsWith("G")) {
                _grid = new Grid(ss[5]);
                _cellLength = (_maxx - _minx) / _gridx;
                _cellHeight = (_maxy - _miny) / _gridy;
            } else if (ss[5].startsWith("T")) {
                _tessellation = new Tessellation(ss[4]);
            }
        }
    }

    @Override
    public String asText() {
        return asString(this);
    }

    @Override
    public Type getGeometryType() {
        return getShape().getGeometryType();
    }

    @Override
    public int getSRID() {
        return Integer.parseInt(Path.getLast(_crs, ':'));
    }

    @Override
    public Mediator getMediator(IExtent extent, IObservable observable, IConcept trait) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public double getArea() {
        return getShape().getArea();
    }

    @Override
    public ISpatialIndex getIndex(boolean makeNew) {

        if (makeNew) {
            return KLAB.MFACTORY.getSpatialIndex(this);
        }

        if (this.index == null) {
            this.index = KLAB.MFACTORY.getSpatialIndex(this);
        }
        return this.index;

    }

    @Override
    public boolean isConstant() {
        return getMultiplicity() == 1;
    }
}
