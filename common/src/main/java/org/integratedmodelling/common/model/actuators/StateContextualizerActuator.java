/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.common.model.actuators;

import java.util.HashMap;
import java.util.List;

import org.integratedmodelling.api.knowledge.ICondition;
import org.integratedmodelling.api.modelling.IAction;
import org.integratedmodelling.api.modelling.runtime.IStateContextualizer;
import org.integratedmodelling.api.modelling.scheduling.ITransition;
import org.integratedmodelling.api.monitoring.IMonitor;
import org.integratedmodelling.common.kim.KIMAction;
import org.integratedmodelling.exceptions.ThinklabException;
import org.integratedmodelling.exceptions.ThinklabValidationException;

public class StateContextualizerActuator extends BaseStateActuator {

    protected HashMap<String, Object> _parameters        = new HashMap<String, Object>();
    int                               _currentStateIndex = -1;
    boolean                           _preprocessed      = false;
    boolean                           _errors            = false;
    IStateContextualizer              contextualizer;

    public StateContextualizerActuator(IStateContextualizer contextualizer,
            List<IAction> actions, IMonitor monitor) {
        if (actions != null) {
            for (IAction action : actions) {
                addAction(action);
            }
        }
        this.monitor = monitor;
    }

    protected int getStateIndex() {
        return _currentStateIndex;
    }

    @Override
    public String toString() {

        return initializationActions.size() == 0 ? ""
                : ("[" + initializationActions.get(0).toString() + "]"
                        + (initializationActions.size() > 1 ? " ..."
                                : ""));
    }

    protected Object call(ITransition transition) throws ThinklabException {

        Object ret = null;

        for (IAction action : initializationActions) {

            if (action.getDomain().isEmpty() && transition == null) {
                ret = doAction(action, null);
            } /* TODO use domains vs. action type*/
        }

        _parameters.clear();

        return ret;
    }

    protected Object doAction(IAction action, ITransition object) throws ThinklabException {

        if (action.getCondition() != null) {
            Object o = action.getCondition().eval(_parameters, getMonitor());
            if (!(o instanceof Boolean))
                throw new ThinklabValidationException("condition in action does not evaluate to true or false");
            if (!((Boolean) o))
                return null;
        }

        return action.getAction() == null ? null : action.getAction().eval(_parameters, getMonitor());
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public List<IAction> getActions() {
        return initializationActions;
    }

    protected synchronized void preprocessActions() {

        for (IAction a : initializationActions) {
            if (a.getTargetStateId() == null) {
                ((KIMAction) a).setTargetStateId(getName());
            }
            ((KIMAction) a).preprocess(_inputs, _outputs);
        }
        _preprocessed = true;
    }

    /*
     * resolves to nothing when we have no actions.
     * (non-Javadoc)
     * @see org.integratedmodelling.thinklab.api.modelling.IStateAccessor#process(int)
     */
    @Override
    public void process(int stateIndex, ITransition transition) throws ThinklabException {

        _currentStateIndex = stateIndex;

        boolean hasErrors = false;

        if (!_preprocessed) {
            preprocessActions();
        }

        /**
         * expose the monitor to the action
         * TODO expose everything else we may need
         */
        if (monitor != null) {
            _parameters.put("_monitor", monitor);
        }

        if (transition != null) {
            _parameters.put("time", transition.getTime());
        }

        for (IAction action : (transition == null ? initializationActions : temporalActions)) {

            if (action.getType() == IAction.Type.CHANGE) {

                String subject = action.getTargetStateId();
                if (checkCondition(action)) {
                    try {
                        Object res = action.getAction().eval(_parameters, getMonitor());
                        _parameters.put(subject, res);
                    } catch (Throwable e) {
                        // only report the first batch of errors
                        if (monitor != null && !_errors) {
                            monitor.error("error computing " + action.getAction() + ": " + e.getMessage());
                            hasErrors = true;
                        }
                    }
                }
            } else if (action.getType() == IAction.Type.DO) {

                if (checkCondition(action)) {
                    action.getAction().eval(_parameters, getMonitor());
                }

            } else if (action.getType() == IAction.Type.INTEGRATE && transition != null) {

                /*
                 * TODO needs to react to a time transition capable of getting to
                 * the previous value.
                 */
            }
        }

        if (hasErrors) {
            _errors = true;
        }
    }

    private boolean checkCondition(IAction a) throws ThinklabException {

        boolean go = true;
        if (a.getCondition() != null) {

            Object iif = null;
            try {
                iif = a.getCondition().eval(_parameters, getMonitor());
            } catch (Exception e) {
                // only report the first batch of errors
                if (monitor != null && !_errors) {
                    monitor.error("error computing " + a.getAction() + ": " + e.getMessage());
                    _errors = true;
                }
            }
            if (!(iif instanceof Boolean)) {
                // only report the first batch of errors
                if (monitor != null && !_errors) {
                    monitor.error(new ThinklabValidationException("condition " + a.getCondition()
                            + " does not return true/false"));
                    _errors = true;
                }
            }
            go = (Boolean) iif;
            if (a.getCondition() instanceof ICondition && ((ICondition) (a.getCondition())).isNegated()) {
                go = !go;
            }
        }
        return go;
    }

    @Override
    public void setValue(String inputKey, Object value) {
        _parameters.put(inputKey, value);
    }

    @Override
    public Object getValue(String outputKey) {
        return _parameters.get(outputKey);
    }

    @Override
    public void reset() {
        _parameters.clear();
    }

}
