/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.common.model.actuators;

import java.util.Collection;
import java.util.HashMap;

import org.integratedmodelling.api.knowledge.IConcept;
import org.integratedmodelling.api.modelling.IAction;
import org.integratedmodelling.api.modelling.IDirectObservation;
import org.integratedmodelling.api.modelling.IProcess;
import org.integratedmodelling.api.modelling.IScale;
import org.integratedmodelling.api.modelling.IState;
import org.integratedmodelling.api.modelling.resolution.IResolutionContext;
import org.integratedmodelling.api.modelling.runtime.IProcessActuator;
import org.integratedmodelling.api.modelling.scheduling.ITransition;
import org.integratedmodelling.api.monitoring.IMonitor;
import org.integratedmodelling.exceptions.ThinklabException;

public class ProcessActuator extends DirectActuator<IProcess>implements IProcessActuator {

    private HashMap<IConcept, IState> inputs = new HashMap<IConcept, IState>();
    private IScale                    scale;

    public ProcessActuator(Iterable<IAction> actions) {
        for (IAction a : actions) {
            addAction(a);
        }
    }

    protected IState getInput(IConcept concept) {
        for (IConcept c : inputs.keySet()) {
            if (c.is(concept)) {
                return inputs.get(c);
            }
        }
        return null;
    }

    protected Collection<IConcept> getInputKeys() {
        return inputs.keySet();
    }

    protected IState getInputState(IConcept observable) {
        return inputs.get(observable);
    }

    protected Collection<IState> getInputStates() {
        return inputs.values();
    }

    protected IScale getScale() {
        return scale;
    }

    @Override
    public IProcess initialize(IProcess subject, IDirectObservation context, IResolutionContext resolutionContext, IMonitor monitor)
            throws ThinklabException {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public boolean processTransition(ITransition transition, IMonitor monitor) throws ThinklabException {
        // TODO Auto-generated method stub
        return false;
    }

}
