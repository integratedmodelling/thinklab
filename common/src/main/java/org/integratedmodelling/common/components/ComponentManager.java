/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.common.components;

import java.io.File;
import java.io.IOException;
import java.lang.annotation.Annotation;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Properties;
import java.util.Set;

import org.apache.commons.lang.exception.ExceptionUtils;
import org.integratedmodelling.api.auth.IUser;
import org.integratedmodelling.api.knowledge.IExpression;
import org.integratedmodelling.api.monitoring.IMonitor;
import org.integratedmodelling.api.monitoring.IMonitorable;
import org.integratedmodelling.api.network.IComponent;
import org.integratedmodelling.api.network.INetwork;
import org.integratedmodelling.api.services.annotations.Prototype;
import org.integratedmodelling.common.command.ServiceManager;
import org.integratedmodelling.common.configuration.KLAB;
import org.integratedmodelling.common.monitoring.Monitor;
import org.integratedmodelling.common.utils.ClassUtils;
import org.integratedmodelling.common.utils.ClassUtils.AnnotationVisitor;
import org.integratedmodelling.common.utils.FileUtils;
import org.integratedmodelling.common.utils.MiscUtilities;
import org.integratedmodelling.common.utils.ZipUtils;
import org.integratedmodelling.exceptions.ThinklabException;
import org.integratedmodelling.exceptions.ThinklabIOException;
import org.integratedmodelling.exceptions.ThinklabValidationException;

import jodd.util.PropertiesUtil;

/**
 * Singleton that manages components. Slated to substitute and extent component
 * functions in Thinklab (engine). Must manage:
 * 
 * 1. harvesting of components from indicated repository 2. harvesting and
 * synchronization of components from the network 3. publishing of components to
 * projects
 * 
 * Components should not be located in the classpath until after the manager has
 * done its work.
 * 
 * @author ferdinando.villa
 *
 */
public class ComponentManager implements IMonitorable {

    public String COMPONENT_JAR_EXTENSION = "jar";

    HashMap<String, IComponent> _components = new HashMap<>();
    ArrayList<String>           _projects   = new ArrayList<>();
    IMonitor                    _monitor    = new Monitor();

    /**
     * Call after the project manager is initialized but before any projects are
     * loaded, and before the classpath scanner for components is called. Will
     * find all component in development path (if set), all jar components in
     * passed local repository, and synchronize every component available from
     * the network for the passed user. Each component will be turned into a
     * project and passed to the project manager for
     * 
     * @param localRepository
     * @param network
     * @param user
     */
    public void initialize(File localRepository, INetwork network, IUser user) throws ThinklabException {

        Set<String> ids = new HashSet<>();
        ArrayList<IComponent> components = new ArrayList<>();

        if (KLAB.CONFIG.getProperties().containsKey("thinklab.dev.components")
                && System.getProperties().containsKey("thinklab.source.distribution")) {
            /*
             * load from directory (development)
             */
            for (String cmp : KLAB.CONFIG.getProperties().getProperty("thinklab.dev.components").split(",")) {

                File f = new File(System.getProperties().getProperty("thinklab.source.distribution")
                        + File.separator
                        + "components" + File.separator + cmp);

                try {
                    IComponent c = loadFromDevDirectory(f);
                    if (c != null && !ids.contains(c.getId())) {
                        components.add(c);
                        ids.add(c.getId());
                    }
                } catch (ThinklabException e) {
                    KLAB.warn("error loading component from development path: " + f);
                }
            }
        }

        for (File f : localRepository.listFiles()) {
            if (f.isFile() && f.canRead() && f.toString().endsWith("." + COMPONENT_JAR_EXTENSION)) {
                try {
                    IComponent c = loadFromJar(f);
                    if (c != null && !ids.contains(c.getId())) {
                        components.add(c);
                        ids.add(c.getId());
                    }
                } catch (ThinklabException e) {
                    KLAB.warn("error loading component from jar: " + f + ": " + e.getMessage());
                }
            }
        }

        // TODO/FIXME: the piece below may need to be put back in some way.

        // for (IComponent c : network.getComponents()) {
        // if (!ids.contains(c.getId())) {
        // components.add(c);
        // ids.add(c.getId());
        // }
        // }

        /*
         * turn all components to projects and leave it to the project manager
         * to link and load the whole thing when load() is called.
         */
        for (IComponent c : components) {
            File f = exportComponentToProject(c);
            if (f != null) {
                _projects.addAll(KLAB.PMANAGER.registerProject(f));
                _components.put(c.getId(), c);
                KLAB.info("component " + c.getId() + " found and scheduled for initialization");
            }
        }

    }

    private File exportComponentToProject(IComponent c) {

        /**
         * TODO if component is remote, sync it with the local path. Component
         * will have a last modification date to compare with the corresponding
         * project's if one is there.
         */
        if (((Component) c).path != null) {
            if (!KLAB.NETWORK.isPersonal()) {
                KLAB.NETWORK.getResourceCatalog().registerComponentPath(c, ((Component) c).path);
            }

            return ((Component) c).path;
        }
        return null;
    }

    private IComponent loadFromDevDirectory(File f) throws ThinklabException {

        Component ret = null;
        File fprop = new File(f + File.separator + "META-INF" + File.separator + "thinklab.properties");
        if (!fprop.exists()) {
            throw new ThinklabValidationException("component in " + f
                    + " is missing configuration in META-INF");
        }
        try {
            Properties p = PropertiesUtil.createFromFile(fprop);
            ret = new Component(f, p);
        } catch (IOException e) {
            throw new ThinklabIOException(e);
        }
        return ret;
    }

    public IComponent loadFromJar(File jarFile) throws ThinklabException {

        File projectFolder = new File(KLAB.CONFIG.getDataPath("deploy") + File.separator
                + MiscUtilities.getFileBaseName(jarFile.toString()));
        projectFolder.mkdirs();
        File libDir = new File(projectFolder + File.separator + "lib");
        libDir.mkdirs();

        KLAB.info("unpacking component " + jarFile + " into " + projectFolder);

        ZipUtils.extractDirectories(jarFile, projectFolder, new String[] {
                "META-INF",
                "src/main/knowledge",
                "lib" });

        try {
            FileUtils.copyFileToDirectory(jarFile, libDir);
        } catch (IOException e) {
            throw new ThinklabIOException(e);
        }

        Component ret = null;
        File fprop = new File(projectFolder + File.separator + "META-INF" + File.separator
                + "thinklab.properties");
        if (!fprop.exists()) {
            throw new ThinklabValidationException("component in " + jarFile
                    + " is missing configuration in META-INF");
        }

        try {
            Properties p = PropertiesUtil.createFromFile(fprop);
            ret = new Component(projectFolder, p);
        } catch (IOException e) {
            throw new ThinklabIOException(e);
        }

        /*
         * create file digest so that we can synchronize this component quickly if
         * we're exporting it.
         */
        if (!KLAB.NETWORK.isPersonal()) {
            KLAB.info("creating file digest for exported component " + ret.getId());
            FileUtils.createMD5Digest(projectFolder, "filelist.txt");
        }

        return ret;
    }

    public Collection<IComponent> getComponents() {
        return _components.values();
    }

    public IComponent getComponent(String id) {
        return _components.get(id);
    }

    public void register(org.integratedmodelling.api.plugin.Component acls, Class<?> target) {
        if (_components.containsKey(acls.id())) {
            ((Component) _components.get(acls.id())).define(acls, target);
        } else {
            _components.put(acls.id(), new Component(acls, target));
        }
    }

    public void link() throws ThinklabException {

        /**
         * Load any prerequisite projects from the network
         */
        for (IComponent component : _components.values()) {
            if (!((Component) component).loadPrerequisites(_monitor)) {
                return;
            }
        }

        /*
         * load the component projects and their prerequisites so that we can
         * initialize the components.
         */
        for (String p : _projects) {
            KLAB.PMANAGER.loadProject(p, KLAB.MFACTORY.getRootParsingContext());
        }

        /**
         * Moved here from visitAnnotations() - to be called after component
         * harvesting and project load.
         */
        ClassUtils
                .visitAnnotations("org.integratedmodelling", org.integratedmodelling.api.plugin.Component.class, new AnnotationVisitor() {
                    @Override
                    public void visit(Annotation acls, Class<?> target) throws ThinklabException {
                        register((org.integratedmodelling.api.plugin.Component) acls, target);
                        // registerComponent();
                    }
                });

        for (IComponent component : _components.values()) {
            try {
                linkComponent(component);
            } catch (Throwable e) {
                KLAB.error("component " + component.getId() + " failed to load: "
                        + ExceptionUtils.getRootCauseMessage(e));
            }
        }

    }

    private void linkComponent(final IComponent component) throws ThinklabException {

        KLAB.info("initializing component " + component.getId());

        if (((Component) component).linked) {
            return;
        }

        ((Component) component).linked = true;

        // /*
        // * TODO this should go away because component dependencies are now
        // managed
        // * through the correspondent projects.
        // */
        // for (String s : component.getPrerequisiteComponents()) {
        // if (_components.get(s) == null) {
        // throw new ThinklabResourceNotFoundException("component " + s +
        // " (required by "
        // + component.getId() + ") is not installed");
        // }
        // }

        final ArrayList<String> apiId = new ArrayList<>();
        // if (component.getExecutor() == null) {
        // throw new ThinklabException();
        // }
        ClassUtils.visitAnnotations(component.getExecutor().getPackage()
                .getName(), Prototype.class, new AnnotationVisitor() {
                    @Override
                    public void visit(Annotation acls, Class<?> target) throws ThinklabException {
                        Prototype prot = (Prototype) acls;
                        /*
                         * TODO - if this is left, remote components will take over local ones. Rather than
                         * defaulting to any behavior, this should be parameterized intelligently.
                         */
                        // if (ServiceManager.get().getPrototype(prot.id()) == null) {
                        ServiceManager.get().processPrototypeDeclaration(prot, target);
                        // }
                        if (!IExpression.class.isAssignableFrom(target)) {
                            apiId.add(prot.id());
                        }
                    }
                });

        for (String service : apiId) {
            ((org.integratedmodelling.common.components.Component) component)
                    .addServicePrototype(ServiceManager
                            .get()
                            .getPrototype(service));
        }

        ((Component) component).initialize(_monitor);
    }

    @Override
    public void setMonitor(IMonitor monitor) {
        _monitor = monitor;
    }

    @Override
    public IMonitor getMonitor() {
        return _monitor;
    }

}
