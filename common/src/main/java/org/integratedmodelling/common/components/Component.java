/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.common.components;

import java.io.File;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import org.apache.commons.lang.exception.ExceptionUtils;
import org.integratedmodelling.Version;
import org.integratedmodelling.api.configuration.IConfiguration;
import org.integratedmodelling.api.monitoring.IMonitor;
import org.integratedmodelling.api.network.IComponent;
import org.integratedmodelling.api.plugin.Initialize;
import org.integratedmodelling.api.plugin.Setup;
import org.integratedmodelling.api.project.IProject;
import org.integratedmodelling.api.services.IPrototype;
import org.integratedmodelling.common.command.Prototype;
import org.integratedmodelling.common.configuration.KLAB;
import org.integratedmodelling.common.project.Workspace;
import org.integratedmodelling.common.utils.StringUtils;
import org.integratedmodelling.exceptions.ThinklabException;
import org.integratedmodelling.exceptions.ThinklabInternalErrorException;
import org.integratedmodelling.exceptions.ThinklabValidationException;
import org.integratedmodelling.lang.IRemoteSerializable;

/**
 * One of these is created per @Component annotation and recorded by each node. The corresponding
 * info is transmitted to the client in a capabilities call.
 * 
 * The API is collected based on the server's package - all prototypes whose implementation is
 * in a (sub)package of the annotated object is part of its API.
 * 
 * @author Ferd
 *
 */
public class Component implements IComponent, IRemoteSerializable {

    private static final String COMPONENT_ID_PROPERTY = "component.id";

    List<IPrototype>            services              = new ArrayList<>();
    Class<?>                    implementation;
    private boolean             isActive;
    private boolean             isInitialized;
    private Version             version;
    private String[]            requiresComponents;
    private String[]            requiresProjects;
    private List<String>        importedKnowledge;
    private boolean             isDistributed;
    private String              id;
    private String              domain;
    // ID of engine that provides the component.
    private String              engineId              = null;

    private String              initMethod            = null;
    private String              setupMethod           = null;
    private boolean             isSetupAsynchronous   = false;
    boolean                     linked                = false;

    // TODO instantiate from jar if it comes from one. This works for a development component, assumed
    // always fresh.
    private long                lastModified          = new Date().getTime();

    File                        path;
    private boolean             _prerequisitesLoaded;

    public Component(org.integratedmodelling.api.plugin.Component component, Class<?> implementation) {
        this.id = component.id();
        define(component, implementation);
    }

    public void define(org.integratedmodelling.api.plugin.Component component, Class<?> implementation) {
        this.implementation = implementation;
        this.domain = component.domain();
        this.isDistributed = component.distributed();
        this.requiresComponents = component.requiresComponents();
        this.requiresProjects = component.requiresProjects();
        this.version = Version.parse(component.version().toString());

        for (Method method : implementation.getMethods()) {
            if (method.isAnnotationPresent(Initialize.class)) {
                initMethod = method.getName();
            }
            if (method.isAnnotationPresent(Setup.class)) {
                setupMethod = method.getName();
                Setup setup = method.getAnnotation(Setup.class);
                isSetupAsynchronous = setup.asynchronous();
            }
        }
    }

    public Component(Map<?, ?> map) {
        id = map.get("id").toString();
        domain = map.get("domain").toString();
        isDistributed = map.get("distributed?").equals("true");
        for (Object s : (List<?>) map.get("api")) {
            services.add((IPrototype) s);
        }
        version = Version.parse(map.get("version").toString());
        lastModified = map.containsKey("last-modified") ? Long.parseLong(map
                .get("last-modified").toString()) : new Date().getTime();
    }

    @Override
    public String toString() {
        return "<C " + id + " (" + services.size() + " services" + ")>";
    }

    /**
     * This is to be used only to create components from local paths. They will be completed
     * later by passing the (necessary) component annotation from the code.
     * 
     * @param cid
     * @param path
     */
    public Component(File path, Properties properties) {
        id = properties.getProperty(COMPONENT_ID_PROPERTY);
        requiresProjects = StringUtils
                .split(properties.getProperty(IProject.PREREQUISITES_PROPERTY, ""), ',');
        importedKnowledge = StringUtils
                .splitOnCommas(properties.getProperty(IProject.IMPORTS_PROPERTY));
        this.path = path;
    }

    @Override
    public Object adapt() {

        HashMap<String, Object> ret = new HashMap<>();
        ret.put("id", id);
        ret.put("domain", domain);
        ret.put("distributed?", isDistributed ? "true" : "false");
        ret.put("version", version.toString());
        ret.put("api", services);
        ret.put("last-modified", lastModified);
        return ret;
    }

    @Override
    public Class<?> getExecutor() {
        return implementation;
    }

    @Override
    public Collection<IPrototype> getAPI() {
        return services;
    }

    @Override
    public boolean isActive() {
        return isActive;
    }

    @Override
    public String getId() {
        return id;
    }

    @Override
    public Version getVersion() {
        return version;
    }

    @Override
    public Collection<String> getPrerequisiteComponents() {
        ArrayList<String> ret = new ArrayList<>();
        if (requiresComponents != null) {
            for (String s : requiresComponents) {
                ret.add(s);
            }
        }
        return ret;
    }

    @Override
    public Collection<String> getPrerequisiteProjects() {
        ArrayList<String> ret = new ArrayList<>();
        if (requiresProjects != null) {
            for (String s : requiresProjects) {
                ret.add(s);
            }
        }
        return ret;
    }

    public void addServicePrototype(IPrototype prototype) {
        ((Prototype) prototype).setComponent(this);
        services.add(prototype);
    }

    public boolean loadPrerequisites(IMonitor monitor) throws ThinklabException {

        if (_prerequisitesLoaded) {
            return true;
        }

        _prerequisitesLoaded = true;

        /**
         * if we need projects, ensure we have a workspace and 
         */
        if (getPrerequisiteProjects().size() > 0) {

            if (KLAB.WORKSPACE == null) {
                KLAB.WORKSPACE = new Workspace(KLAB.CONFIG.getDataPath(IConfiguration.SUBSPACE_PROJECTS));
            }

            boolean needLoad = false;
            for (String projectId : getPrerequisiteProjects()) {

                KLAB.info("trying to satisfy requirement " + projectId + " from component " + getId()
                        + "...");

                if (KLAB.PMANAGER.getProject(projectId) == null) {

                    File pdir = KLAB.WORKSPACE.synchronizeNetworkProject(projectId, KLAB.NETWORK);

                    if (pdir == null) {
                        KLAB.warn("component " + getId()
                                + " requires unavailable project "
                                + projectId + ": deactivated");
                        isActive = false;
                        return false;
                    }

                    needLoad = true;
                    KLAB.info("project " + projectId + " required by component " + getId()
                            + " synchronized successfully");
                } else {
                    /*
                     * ensure it's loaded later
                     */
                    KLAB.info("requisite " + projectId + " is available locally");
                    needLoad = true;
                }
            }

            for (File f : KLAB.WORKSPACE.getProjectLocations()) {
                KLAB.PMANAGER.registerProject(f);
            }

            if (needLoad) {
                KLAB.PMANAGER.load(false, KLAB.MFACTORY.getRootParsingContext());
            }
        }

        return true;
    }

    public void initialize(IMonitor monitor) throws ThinklabException {

        // FIXME - some redundancy here.
        if (isInitialized) {
            return;
        }

        if (!loadPrerequisites(monitor)) {
            return;
        }

        if (implementation == null) {
            isActive = false;
            return;
        }

        if (initMethod == null) {
            isActive = true;
            isInitialized = true;
            return;
        }

        KLAB.info("initializing component " + id);

        Object executor = null;
        try {
            executor = implementation.newInstance();
        } catch (InstantiationException | IllegalAccessException e) {
            throw new ThinklabInternalErrorException("cannot instantiate component executor for " + id);
        }

        if (executor == null) {
            throw new ThinklabInternalErrorException("component " + id
                    + " has not been configured properly");
        }

        /*
         * find executing method and call it
         */
        Method method = null;
        try {
            method = executor.getClass().getMethod(initMethod);
        } catch (NoSuchMethodException | SecurityException e) {
            return;
        }

        try {
            isActive = (Boolean) method.invoke(executor);
        } catch (Exception e) {
            throw (e instanceof ThinklabException ? (ThinklabException) e
                    : new ThinklabInternalErrorException("error calling component initializer for "
                            + id + ": " + ExceptionUtils.getRootCauseMessage(e)));
        }

        String missing = "";
        if (importedKnowledge != null) {
            for (String imp : importedKnowledge) {
                if (KLAB.MMANAGER.getExportedKnowledge(imp) == null) {
                    missing += (missing.isEmpty() ? "" : ", ") + imp;
                }
            }
        }

        if (!missing.isEmpty()) {
            throw new ThinklabValidationException("component " + id
                    + " is missing the following exported knowledge IDs: " + missing);
        }

        isInitialized = true;
    }

    public void setup() throws ThinklabException {

        if (implementation == null) {
            return;
        }
        if (setupMethod == null) {
            return;
        }
        Object executor = null;
        try {
            executor = implementation.newInstance();
        } catch (InstantiationException | IllegalAccessException e) {
            throw new ThinklabInternalErrorException("cannot instantiate component executor for " + id);
        }

        if (executor == null) {
            throw new ThinklabInternalErrorException("component " + id
                    + " has not been configured properly");
        }

        /*
         * find executing method and call it
         */
        Method method = null;

        try {
            method = executor.getClass().getMethod(setupMethod);
        } catch (NoSuchMethodException | SecurityException e) {
            return;
        }

        try {
            /*
             * TODO do the task thing if isSetupAsynchronous
             */
            isActive = (Boolean) method.invoke(executor);
        } catch (Exception e) {
            throw (e instanceof ThinklabException ? (ThinklabException) e
                    : new ThinklabInternalErrorException("error calling component initializer for "
                            + id + ": " + ExceptionUtils.getRootCauseMessage(e)));
        }
    }

    public void setActive(boolean b) {
        isActive = b;
    }

    public void setEngineId(String id) {
        engineId = id;
    }

    public String getEngineId() {
        return engineId;
    }

    @Override
    public long lastModified() {
        return lastModified;
    }

}
