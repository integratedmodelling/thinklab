package org.integratedmodelling.ecology.biomass.lpjguess;

import org.integratedmodelling.exceptions.ThinklabRuntimeException;

public enum HydrologyType {

    RAINFED,
    IRRIGATED;

    public static HydrologyType get(String s) {

        switch (s.toUpperCase()) {
        case "RAINFED":
            return RAINFED;
        case "IRRIGATED":
            return IRRIGATED;
        }

        throw new ThinklabRuntimeException("unknown irrigation type: " + s);
    }
}
