set(headers 
  config.h 
  guess.h 
  guessmath.h 
  outputchannel.h
  framework.h
  shell.h
  )

set(source  
  guess.cpp 
  outputchannel.cpp
  framework.cpp
  shell.cpp
  )

include(add_guess_sources)