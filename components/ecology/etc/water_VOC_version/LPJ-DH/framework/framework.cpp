///////////////////////////////////////////////////////////////////////////////////////
/// \file framework.cpp
/// \brief Implementation of the framework() function
///
/// \author Ben Smith
/// $Date: 2012-01-24 11:33:51 +0100 (Tue, 24 Jan 2012) $
///
///////////////////////////////////////////////////////////////////////////////////////

#include "config.h"
#include "framework.h"

#include "guessio.h"
#include "driver.h"
#include "canexch.h"
#include "soilwater.h"
#include "somdynam.h"
#include "growth.h"
#include "vegdynam.h"
#include "landcover.h"
#include "bvoc.h"


int framework(int argc,char* argv[]) {

	// The 'mission control' of the model, responsible for maintaining the 
	// primary model data structures and containing all explicit loops through 
	// space (grid cells/stands) and time (days and years).

	bool dogridcell;

	// The one and only linked list of Pft objects	
	Pftlist pftlist;

	// Call input/output module to obtain PFT static parameters and simulation
	// settings and initialise input/output
	initio(argc,argv,pftlist);

	// bvoc
	if(ifbvoc){
	  initbvoc(pftlist);
	}

	// Assume there is at least one grid cell to simulate

    int j=0;
    //Date date; // date object used for interpolation (local to this function)
    date.init(468);    //jing add it. set the year of the whole simulation.  
   	dogridcell=true;
  	int k=0;
    
    const int gridnum =1;    //!!!need to set it based on the number of the gridcells
    Gridcell *mygridcell[gridnum];   // jing add it, allocate memory at the beginning of the programme.Due to no empty construstor function, so using pointer. depending on simulating cells.
 //jing add it ..make array to store all cells in the gridlist. each gridcell with its own spinup data and climate data.
   while (dogridcell)
   	{

   	  Gridcell *new_gridcell = new Gridcell(pftlist);
	  
       if (getgridcell(*new_gridcell)){  //get grid will make sure whether all points in gridlist can be found the climate data. all the climate data will bu put into the hist_temp[168][12],hist_mprec[168][12].....  and then the function of getclimate will read data in different way in spinup, historical and future period!!!!

		   

    	            (*new_gridcell).climate.initdrivers((*new_gridcell).climate.lat);// Initialise certain climate and soil drivers
    	   			mygridcell[k]=new_gridcell;
    				if(run_landcover) {
    					//Read static landcover and cft fraction data from ins-file and/or from data files for the spinup peroid and create stands.
    					landcover_init(*new_gridcell,pftlist);
						
    				}
					k++;

        }
       else
		      dogridcell=false;
			  
			  date.init(468);  //let's date record go back to the first day.

    }


   

 while(date.islastyear==false || date.islastmonth==false || date.islastday==false || (date.islastday && date.islastmonth && date.islastyear))
 {
	
	//go through all the grid cells for one day!!!

		

		// START OF LOOP THROUGH GRID CELLS

		// (argument nyear not used in this implementation)
		// Create and initialise a new Gridcell object for each locality
        
        //Gridcell gridcell(pftlist);

	        // Call input/output to obtain latitude and soil driver data for this grid cell.
		// Function getgridcell returns false if no further grid cells remain to be simulated
             
        	for (gi=0; gi<gridnum;gi++){

        		Gridcell &gridcell = *mygridcell[gi];// use reference to refer to the *mygridcell object

				/*if(run_landcover) {
					//Read static landcover and cft fraction data from ins-file and/or from data files for the spinup peroid and create stands.
					landcover_init(gridcell,pftlist);
				}*/
				if (date.day==0){
					//seed=12345678;
					gethistclimate(gridcell);
				} 
			
				//printf("�Before getclimate: %f, %f \n", gridcell.lat, gridcell.lon);
        		if (getclimate(gridcell)) {
					
        			//k++;
					//extern double dprec[gi][365];

			// Call input/output to obtain climate, insolation and CO2 for this
			// day of the simulation. Function getclimate returns false if last year
			// has already been simulated for this grid cell
               dailyaccounting_gridcell(gridcell,pftlist);

				// Calculate daylength, insolation and potential evapotranspiration
		       daylengthinsoleet(gridcell.climate);

				if(run_landcover && date.day==0) {
					// Update dynamic landcover and crop fraction data during historical period and create/kill stands.
					if(date.year>=nyear_spinup)
						landcover_dynamics(gridcell,pftlist);
				}

				gridcell.firstobj();
				while (gridcell.isobj) {

					// START OF LOOP THROUGH STANDS

					Stand& stand=gridcell.getobj();  //initial all the varialbes in pft.

					dailyaccounting_stand(stand,pftlist);

					stand.firstobj();
					
					//int npatch=0; need to add it when npatch is greater than 1
					while (stand.isobj) {
						// START OF LOOP THROUGH PATCHES
						
						// Get reference to this patch
						Patch& patch=stand.getobj(); 
						// Update daily soil drivers including soil temperature
					
						dailyaccounting_patch(patch,pftlist);
						// Leaf phenology for PFTs and individuals
						leaf_phenology(patch,gridcell.climate);
						// Interception
						interception(patch, gridcell.climate);
						initial_infiltration(patch, gridcell.climate);
						// Photosynthesis, respiration, evapotranspiration
						canopy_exchange(patch, gridcell.climate);
						// Soil water accounting, snow pack accounting
						soilwater(patch, gridcell.climate);
						// Soil organic matter and litter dynamics
						som_dynamics(patch);
					

						if (date.islastday && date.islastmonth) {
							
							// LAST DAY OF Month
							// Tissue turnover, allocation to new biomass and reproduction,
							// updated allometry
							growth(stand,patch);
							

						}
						 
						
						stand.nextobj();
					//	npatch++;
					}// End of loop through patches
					//gridcell.surf_runoff/=npatch;
					//outannual(gridcell,pftlist);

				   if (date.islastday && date.islastmonth) {
								// LAST DAY OF Year
								stand.firstobj();
								while (stand.isobj) {

									// For each patch ...
									Patch& patch=stand.getobj();
									// Establishment, mortality and disturbance by fire
									seed=gridcell.grid_seed;//jing add it
									vegetation_dynamics(stand,patch,pftlist);
									gridcell.grid_seed=seed;//jing add it
									stand.nextobj();
									
								}
							}

				   // get the sum of average surface runoff for all the patches in one stand;

				gridcell.nextobj();
				
				//recording the number of stands in one grid cell
				} // end of going through all stands in one grid cell
				

					if (date.islastday && date.islastmonth) {
									// LAST DAY OF YEAR
						// Call input/output module to output results for end of year
						// or end of simulation for this grid cell	
						outannual(gridcell,pftlist);
						gridcell.grid_seed=seed;
						
						
							// Check whether to abort
								if (abort_request_received()) {
									termio();
									return 99;
							
								}

						}
				}
				   
			}
	 
     int m=0;
	 
// calculate the window-generalized runoff for each gridcell.
 // generalized with the neighbouring cells within drainage basin
	 // based on DA, sort grid cells in the ascending DA value.

    for (m=0; m<gridnum;m++){   //go through all cells
	
	Gridcell &gridcell2 = *mygridcell[m];
	   gridcell2.dsurf_ave=0.0;
	   gridcell2.dsurf_sum=0.0;
	   gridcell2.status=0;
	   int current_row=gridcell2.row;
	   int current_col=gridcell2.col;
		
	  if(gridcell2.DA==0){     //firstly finish the first few rows of gridcell with DA=0
			gridcell2.status=1;
				//gridcell2.dsurf_sum=0.0;
			//gridcell2.dsurf_ave=0.0;   //no water drain to those cells.

		}

	  if(gridcell2.DA>0&&gridcell2.status==0)
	  {          int n=0;  
		       //  int Drain_num=0;
				  for (n=0; n<gridnum;n++){ 
					  Gridcell &gridcell3 = *mygridcell[n];
	
						// generalized runoff with the  cells within drainage basin
				
				 if ((gridcell3.col<=(current_col+1) &&gridcell3.col>=(current_col-1))&&(gridcell3.row>=(current_row-1)&&gridcell3.row<=(current_row+1)))
		
					 {
						 if(gridcell3.drain_row==current_row&&gridcell3.drain_col==current_col&&gridcell3.status==1){
							 if(gridcell3.surf_perco>=0)
								 gridcell2.dsurf_sum+=gridcell3.dsurf_sum+gridcell3.surf_perco;
							 else 
								 gridcell2.dsurf_sum+=gridcell3.dsurf_sum-gridcell3.dsurf_ave;
								 //check the surf_perco values. after spinup 						
					
							 //Drain_num++;
						 }
				     }
				 
				
				}
				 gridcell2.status=1;
				  if(gridcell2.dsurf_sum>0)
					  if(gridcell2.surf_perco<0&&gridcell2.dsurf_sum>=(-gridcell2.surf_perco))
						  gridcell2.dsurf_ave=(-gridcell2.surf_perco);
					  else if (gridcell2.surf_perco<0 && gridcell2.dsurf_sum<(-gridcell2.surf_perco))
						  gridcell2.dsurf_ave=gridcell2.dsurf_sum;
					  else if (gridcell2.surf_perco>=0)
						  gridcell2.dsurf_ave=0;   //dsurf_ave is storing the addtional water for the next day.
				  else{
					  gridcell2.dsurf_ave=0.0;
				      gridcell2.dsurf_sum=0.0;
				  }
				   
				
	  }
		
    }           




	  // test the water balance of the whole catchment.

	
 // End of loop through grid cells
            // finish one-day simulation, now redistribute water for the whole area.
      if (date.islastday && date.islastmonth && date.islastyear)
    	  break;

	  date.next();
	  gi=0;
       // Advance timer to next simulation day

            //  ??????should put the gridlist pointer to the beginning again....
        	//dogridcell=true;
 }//end of all days simulation




	// Call to input/output module to perform any necessary clean up
	termio();

	// END OF SIMULATION
	return 0;
}

