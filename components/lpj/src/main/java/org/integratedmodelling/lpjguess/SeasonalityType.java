package org.integratedmodelling.lpjguess;

public enum SeasonalityType {
	SEASONALITY_NO, SEASONALITY_PREC, SEASONALITY_PRECTEMP, SEASONALITY_TEMP, SEASONALITY_TEMPPREC
}
