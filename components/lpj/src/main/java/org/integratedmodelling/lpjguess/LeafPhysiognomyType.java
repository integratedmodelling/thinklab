package org.integratedmodelling.lpjguess;

public enum LeafPhysiognomyType {
	NOLEAFTYPE, NEEDLELEAF, BROADLEAF
}
