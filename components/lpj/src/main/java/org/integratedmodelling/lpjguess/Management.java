package org.integratedmodelling.lpjguess;

public class Management {

	public Management() {
		pftname = "";
		hydrology = HydrologyType.RAINFED;
//		firr = 0.0;
		sdate = -1;
		hdate = -1;
		nfert = 0.0;
		fallow = false;
	}

	public String pftname;
	/// hydrology (RAINFED,IRRIGATED) 
	public HydrologyType hydrology;
	/// irrigation efficiency
//		double firr;
	public int sdate;
	public int hdate;
	/// Nitrogen fertilisation amount
	public double nfert;

	public Boolean fallow;	
}
