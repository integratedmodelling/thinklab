package org.integratedmodelling.lpjguess;

public enum LandcoverType {
	
	URBAN, // 0
	CROP, // 1
	PASTURE, // 2
	FOREST, // 3
	NATURAL, // 4
	PEATLAND, // 5
	BARREN // 6

}
