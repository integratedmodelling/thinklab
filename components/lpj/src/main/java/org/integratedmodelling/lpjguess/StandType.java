package org.integratedmodelling.lpjguess;

public class StandType {

	int NROTATIONPERIODS_MAX = 3;
	
	public StandType(String n) {
		name = n;
		frac = 1.0;
		frac_old = 0.0;
		protected_frac = 0.0;
		frac_change = 0.0;
		gross_frac_increase = 0.0;
		gross_frac_decrease = 0.0;
		nstands = 0;
		intercrop = IntercropType.NOINTERCROP;
		naturalveg = NaturalVeg.NONE;
		naturalgrass = false;
		restrictpfts = false;
	}
	
	/// Returns position of crop in rotation list if present. Returns -1 if not.
		int pftinrotation(String name) {

			int cropno = -1;
			for(int i=0; i<rotation.ncrops; i++) {
				if(name == management[i].pftname)
					cropno = i;
			}

			return cropno;
		}
	
	/// id code (should be zero based and sequential, 0...nst-1)
	public int id;
	public String name;

	/// specifies type of landcover
	public LandcoverType landcover;	// specifies type of landcover (0 = URBAN, 1 = CROP, 2 = PASTURE, 3 = FOREST, 4 = NATURAL, 5 = PEATLAND)
	/// hydrology (RAINFED,IRRIGATED) 
//		hydrologytype hydrology;
	///
	public CropRotation rotation;
	// List of management types in a rotation cycle
	public Management management[] = new Management[NROTATIONPERIODS_MAX];

	/// intercrop (NOINTERCROP,NATURALGRASS)
	public IntercropType intercrop;
	/// whether natural pft:s are allowed to grow in stand type
	public NaturalVeg naturalveg; // or NONE, GRASSONLY, ALL
	/// whether natural grass pft:s are allowed to grow in stand type
	public Boolean naturalgrass;
	// whether only pft:s defined in management are allowed (plus intercrop or naturalveg/grass)
	public Boolean restrictpfts;

	/// fraction of this stand type relative to the gridcell
	public double frac;
	/// old fraction of this stand type relative to the gridcell before update
	public double frac_old;

	public double protected_frac;

	/// net fraction change
	public double frac_change;

	public double gross_frac_increase;
	public double gross_frac_decrease;

	// current number of stands of this stand type
	public int nstands;

}
