package org.integratedmodelling.lpjguess;

public enum ForceAutumnSowing {
	NOFORCING, AUTUMNSOWING, SPRINGSOWING
}
