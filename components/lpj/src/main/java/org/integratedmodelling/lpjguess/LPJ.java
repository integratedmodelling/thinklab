package org.integratedmodelling.lpjguess;

import java.util.List;
import java.util.Random;

import org.integratedmodelling.engine.introspection.DataRecorder;
import org.integratedmodelling.engine.time.literals.TimeValue;
import org.integratedmodelling.lpjguess.common.Output;
import org.integratedmodelling.lpjguess.processes.CanopyExchange;
import org.integratedmodelling.lpjguess.processes.Growth;
import org.integratedmodelling.lpjguess.processes.GrowthDaily;
import org.integratedmodelling.lpjguess.processes.InterceptionInfiltration;
import org.integratedmodelling.lpjguess.processes.Irrigation;
import org.integratedmodelling.lpjguess.processes.LeafPhenology;
import org.integratedmodelling.lpjguess.processes.PlantGrowth;
import org.integratedmodelling.lpjguess.processes.SoilWater;
import org.integratedmodelling.lpjguess.processes.SomDynamics;
import org.integratedmodelling.lpjguess.processes.VegDynamics;
import org.integratedmodelling.procsim.api.IConfiguration;

/**
 *
 * Main simulation driver. Create a configuration, pass it to the constructor
 * and call run().
 *
 * TODO create and pass an output configuration too.
 *
 * // // LPJF refers to the original FORTRAN implementation of LPJ as described
 * by Sitch // et al 2000 // Fulton, MR 1991 Adult recruitment rate as a
 * function of juvenile growth in size- // structured plant populations. Oikos
 * 61: 102-105. // Haxeltine A & Prentice IC 1996 BIOME3: an equilibrium
 * terrestrial biosphere // model based on ecophysiological constraints,
 * resource availability, and // competition among plant functional types.
 * Global Biogeochemical Cycles 10: // 693-709 // Lloyd, J & Taylor JA 1994 On
 * the temperature dependence of soil respiration // Functional Ecology 8:
 * 315-323 // Monsi M & Saeki T 1953 Ueber den Lichtfaktor in den
 * Pflanzengesellschaften und // seine Bedeutung fuer die Stoffproduktion.
 * Japanese Journal of Botany 14: 22-52 // Prentice, IC, Sykes, MT & Cramer W
 * (1993) A simulation model for the transient // effects of climate change on
 * forest landscapes. Ecological Modelling 65: 51-70. // Reich, PB, Walters MB &
 * Ellsworth DS 1997 From tropics to tundra: global // convergence in plant
 * functioning. Proceedings of the National Academy of Sciences // USA 94:
 * 13730-13734. // Sitch, S, Prentice IC, Smith, B & Other LPJ Consortium
 * Members (2000) LPJ - a // coupled model of vegetation dynamics and the
 * terrestrial carbon cycle. In: // Sitch, S. The Role of Vegetation Dynamics in
 * the Control of Atmospheric CO2 // Content, PhD Thesis, Lund University, Lund,
 * Sweden. // Sykes, MT, Prentice IC & Cramer W 1996 A bioclimatic model for the
 * potential // distributions of north European tree species under present and
 * future climates. // Journal of Biogeography 23: 209-233.
 *
 *
 * @author Ferd
 *
 */
public class LPJ {

	IConfiguration _configuration;
	public Gridcell gridcell;

	public LPJ(IConfiguration configuration, Gridcell g) {

		_configuration = configuration;
		gridcell = g;

		/*
		 * TODO create and register processes based on configuration
		 */
	}

	// Priestley-Taylor coefficient (conversion factor from equilibrium
	// evapotranspiration to PET)

	public static void fail(String string, Object o) {
		// TODO Auto-generated method stub

	}



//	public void setRandomClimate() {
//		Random r = new Random();
//		double temp = r.nextDouble() * 30;
//		double prec = r.nextDouble() * 100;
//		double perc_sunshine = r.nextDouble() * 100;
//
//		setClimate(temp, prec, perc_sunshine);
//	}

	public Output getOutputs() {
		// At the moment only works if we have ONE and ONLY ONE stand in each
		// instance of LPJ
		// TODO: Fix this, or make it clear that this is how we have to use it!
		Output o = gridcell.stands.get(0).output;
		return o;
	}

	public void run() {

		// Always run the daily stuff
		run_daily();

		if (_configuration.getSchedule().isLastDayOfMonth()) {
			// If it's the last day of the month then do the monthly stuff
			run_monthly();

			if (_configuration.getSchedule().isLastMonthOfYear()) {
				// If it's the last month of the year as well then do the
				// yearly stuff
				run_yearly();
			}
		}
	}

	public double run_daily() {
		// Do daily processes here
		double tot_apet = 0.0;
		
		// Update daily climate drivers etc
		gridcell.dailyAccounting(_configuration.getPFTs());
		
		// Update crop sowing date calculation framework
		gridcell.crop_sowing();
		
		// Calculate daylength, insolation and potential
		// evapotranspiration
		gridcell.climate.dayLengthInsolEET();
		
		// TODO: Put dynamic LC stuff here

		for (Stand stand : gridcell.stands) {

			for (Patch patch : stand) {
				// DataRecorder.get().info("Doing run_daily for a patch");
				// Update daily soil drivers including soil temperature
				patch.dailyAccounting();
				patch.dailyAccountingLC();
				
				if (stand.landcover == LandcoverType.CROP) {
					patch.crop_nfert();
					patch.crop_sowing();
					patch.crop_phenology();
					patch.update_fpc();
				}

				
				// Leaf phenology for PFTs and individuals
				new LeafPhenology(_configuration).process(patch);
				
				// Interception
				new InterceptionInfiltration(_configuration).process(patch);

				// Photosynthesis, respiration, evapotranspiration
				new CanopyExchange(_configuration).process(patch);
				
				new Irrigation(_configuration).process(patch);

				new GrowthDaily(_configuration).process(patch);
				
				// Soil water accounting, snow pack accounting
				new SoilWater(_configuration).process(patch);

				// Soil organic matter and litter dynamics
				new SomDynamics(_configuration).process(patch);

				// DataRecorder.get().info(patch.apet);
				tot_apet += patch.apet;

				// // TODO: Bad idea!
				// new PlantGrowth(_configuration).process(patch);
				//
				// // TODO: Bad idea!
				// new VegDynamics(_configuration).process(patch);
			}
			
			// Update crop rotation status
			stand.doCropRotation();
		}

		return tot_apet;
	}

	public void run_monthly() {

		for (Stand stand : gridcell.stands) {
			for (Patch patch : stand) {
				new PlantGrowth(_configuration).process(patch);
			}
		}
	}

	public void run_yearly() {

		for (Stand stand : gridcell.stands) {
			for (Patch patch : stand) {
				new Growth(_configuration).process(patch);
				
				new VegDynamics(_configuration).process(patch);
				// outannual(stand);
			}
		}
	}


}
