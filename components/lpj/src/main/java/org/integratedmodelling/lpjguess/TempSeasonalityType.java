package org.integratedmodelling.lpjguess;

public enum TempSeasonalityType {
	COLD, COLD_WARM, COLD_HOT, WARM, WARM_HOT, HOT
}
