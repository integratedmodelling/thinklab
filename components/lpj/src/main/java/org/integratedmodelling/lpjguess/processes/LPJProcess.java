package org.integratedmodelling.lpjguess.processes;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.integratedmodelling.api.modelling.IDirectObservation;
import org.integratedmodelling.api.modelling.IModel;
import org.integratedmodelling.api.modelling.INumericObserver;
import org.integratedmodelling.api.modelling.IObservable;
import org.integratedmodelling.api.modelling.IObservation;
import org.integratedmodelling.api.modelling.IProcess;
import org.integratedmodelling.api.modelling.IScale;
import org.integratedmodelling.api.modelling.IState;
import org.integratedmodelling.api.modelling.resolution.IResolutionContext;
import org.integratedmodelling.api.modelling.runtime.IProcessContextualizer;
import org.integratedmodelling.api.modelling.scheduling.ITransition;
import org.integratedmodelling.api.monitoring.IMonitor;
import org.integratedmodelling.api.project.IProject;
import org.integratedmodelling.api.services.annotations.Prototype;
import org.integratedmodelling.api.services.types.IEngineService;
import org.integratedmodelling.api.services.types.ILocalService;
import org.integratedmodelling.api.space.ISpatialExtent;
import org.integratedmodelling.common.space.IGeometricShape;
import org.integratedmodelling.common.states.States;
import org.integratedmodelling.common.utils.parallel.Parallel;
import org.integratedmodelling.common.utils.parallel.ParallelOp;
import org.integratedmodelling.common.vocabulary.EcologyNS;
import org.integratedmodelling.common.vocabulary.NS;
import org.integratedmodelling.engine.time.literals.TimeValue;
import org.integratedmodelling.exceptions.ThinklabException;
import org.integratedmodelling.exceptions.ThinklabValidationException;
import org.integratedmodelling.lpjguess.Configuration;
import org.integratedmodelling.lpjguess.Gridcell;
import org.integratedmodelling.lpjguess.LPJ;
import org.integratedmodelling.lpjguess.Soiltype;

import com.vividsolutions.jts.geom.Point;

/**
 * This is a copy of the example Process Contextualiser, to use as a starting
 * point for
 */
@Prototype(
        id = "lpj.p",
        returnTypes = { NS.PROCESS_CONTEXTUALIZER },
        // leave published to false or this will be advertised on all servers of the
        // network
        published = false,
        componentId = "thinklab.lpj",
        args = { "? m|multiplier", Prototype.INT })
public class LPJProcess implements IProcessContextualizer, IEngineService, ILocalService {
    boolean                   runInParallel = false;
    boolean                   canDispose    = false;
    int                       multiplier    = 1;
    IScale                    scale         = null;
    Map<String, IObservation> outputStates  = new ConcurrentHashMap<>();

    Configuration config;

    // Input states
    private IState precipitation;
    private IState max_temperature;
    private IState min_temperature;
    private IState perc_sunshine;

    // Output states
    private IState aboveGroundBiomassState;

    // Stores the individual instances of the LPJ objects, for each grid cell
    private Map<Integer, Gridcell> gridcells = new HashMap<>();

    // Stores the monitor object passed in initialize() so that we can use it
    // for debugging messages later
    private IMonitor monitor;

    @Override
    public boolean canDispose() {
        return canDispose;
    }

    @Override
    public Map<String, IObservation> initialize(IProcess process, IDirectObservation context, IResolutionContext resolutionContext, Map<String, IObservable> expectedInputs, Map<String, IObservable> expectedOutputs, IMonitor monitor)
            throws ThinklabException {

        // Store the scale and monitor
        this.scale = process.getScale();
        this.monitor = monitor;

        // We can get rid of this object after initialisation if there is no
        // temporal aspect to
        // the context
        canDispose = !context.getScale().isTemporallyDistributed();

        for (String inp : expectedInputs.keySet()) {
            IObservable o = expectedInputs.get(inp);
            if (o.getObserver() != null && !(o.getObserver() instanceof INumericObserver)) {
                throw new ThinklabValidationException("example.process: input state " + inp
                        + " is not numeric");
            }
        }

        // Get the input states here, and put them in sensible members
        // using States.findState()
        // TODO: Will return null if it can't find it - so make sure we check
        // here
        this.max_temperature = States
                .findStateWith(context, EcologyNS.ATMOSPHERIC_TEMPERATURE, EcologyNS.MAXIMUM_TRAIT);
        this.min_temperature = States
                .findStateWith(context, EcologyNS.ATMOSPHERIC_TEMPERATURE, EcologyNS.MINIMUM_TRAIT);
        this.precipitation = States
                .findStateWithout(context, EcologyNS.PRECIPITATION_VOLUME, EcologyNS.YEARLY_TRAIT);

        EcologyNS.synchronize();

        IObservable aboveGroundBiomassObservation = null;
        String aboveGroundBiomassId = null;
        for (String iname : expectedOutputs.keySet()) {
            if (expectedOutputs.get(iname).is(EcologyNS.ABOVE_GROUND_BIOMASS)) {
                aboveGroundBiomassId = iname;
                aboveGroundBiomassObservation = expectedOutputs.get(iname);
                aboveGroundBiomassState = context.getState(aboveGroundBiomassObservation);
            }
        }

        // Create one Configuration object
        this.config = new Configuration();

        // Set the initial timestamp of the simulation to be the start time
        // of the temporal range
        TimeValue tv = (TimeValue) scale.getTime().getStart();
        config.getSchedule().setInitialTimestamp(tv);

        // For each of the spatial areas (ie. grid cells)
        // initialise LPJ
        for (int n : scale.getIndex((IScale.Locator) null)) {

            int spaceOffset = scale.getExtentOffset(scale.getSpace(), n);

            monitor.info("Creating a stand (for a gridcell)", null);

            // // Get latitude of centre of current spatial area (grid cell)
            // // to give to the Climate object eventually
            ISpatialExtent currentSpace = scale.getSpace().getExtent(spaceOffset);
            Point point = ((IGeometricShape) currentSpace).getStandardizedGeometry().getCentroid();

            double latitude = point.getY();
            double longitude = point.getX();

            monitor.info("Location: Lat = " + latitude + ", Lon = " + longitude, null);
            Gridcell gridcell = new Gridcell(config, Soiltype.getLPJSoiltype(2), latitude, longitude);

            // Set constant LC fractions - which also creates the required stands of the required StandTypes
            // TODO: Update this to do dynamic LC
            gridcell.setLCFractions(0.1, 0.3, 0.1, 0.1, 0.3, 0.1);

            monitor.info("Created Stands", null);

            // Store the list of stands so we can use it in the compute calls
            gridcells.put(spaceOffset, gridcell);

            // Set all outputs to zero
            States.set(aboveGroundBiomassState, 0.0, n);
        }

        outputStates.put(aboveGroundBiomassId, aboveGroundBiomassState);
        return outputStates;
    }

    @Override
    public Map<String, IObservation> compute(ITransition transition, Map<String, IState> inputs)
            throws ThinklabException {

        Map<String, IObservation> ret = new HashMap<>();

        canDispose = transition.isLast();

        // Set the current timestamp in the config object
        TimeValue tv = (TimeValue) transition.getTime().getStart();
        // DataRecorder.get().info("Setting timestamp to: " + tv.asText());
        config.getSchedule().setTimestamp(tv);
        // DataRecorder.get().info("Day of month (0-based): " + config.getSchedule().dayofmonth());
        // DataRecorder.get().info("Day of month): " + tv.getDayOfMonth());
        // DataRecorder.get().info("Days in month?" + tv.getNDaysInMonth());
        // DataRecorder.get().info("End of month?" + config.getSchedule().isLastDayOfMonth());

        if (runInParallel) {
            Parallel.ForEach(scale.getIndex(transition), new TestParallel(transition, inputs), 8);
        } else {
            for (int n : scale.getIndex(transition)) {
                TestParallel tp = new TestParallel(transition, inputs);
                tp.run(n);
            }
        }

        ret.putAll(outputStates);
        return ret;
    }

    @Override
    public void setContext(Map<String, Object> parameters, IModel model, IProject project) {

        if (parameters.containsKey("multiplier")) {
            multiplier = ((Number) parameters.get("multiplier")).intValue();
        }
    }

    public class TestParallel implements ParallelOp {

        ITransition         transition;
        Map<String, IState> inputs;

        public TestParallel(ITransition transition, Map<String, IState> inputs) {
            super();
            this.transition = transition;
            this.inputs = inputs;
        }

        @Override
        public void run(Object i) {
            int n = (int) i;

            // Get the gridcell
            int spaceOffset = scale.getExtentOffset(scale.getSpace(), n);
            Gridcell gridcell = gridcells.get(spaceOffset);

            // Get the climatic data from the states
            double precip = States.getDouble(precipitation, n);
            double avg_temp = (States.getDouble(max_temperature, n) + States.getDouble(min_temperature, n))
                    / 2.0;
                    // DataRecorder.get().info("Precip = " + precip);
                    // DataRecorder.get().info("Avg temp = " + avg_temp);

            // Add this in when we can get percentage sunshine data from weather
            // (or somewhere else)
            // double sunshine = States.getDouble(perc_sunshine, n);
            // Random r = new Random();
            // double sunshine = r.nextDouble() * 100;
            double sunshine = 70.0;
            gridcell.setClimate(avg_temp, precip, sunshine);

            LPJ lpj = new LPJ(config, gridcell);

            // Actually do the LPJ run!
            lpj.run();

            lpj.getOutputs().annualStandOutput();

            // DataRecorder.get().info("C mass:" +
            // lpj.getOutputs().cmass_stand);
            // DataRecorder.get().info("LAI: " + lpj.getOutputs().lai_stand);
            // DataRecorder.get().info(lpj.getOutputs().anpp_stand);

            States.set(aboveGroundBiomassState, lpj.getOutputs().cmass_stand, n);
        }

    }

}
