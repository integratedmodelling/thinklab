package org.integratedmodelling.lpjguess;

public enum PrecSeasonalityType {
	DRY, DRY_INTERMEDIATE, DRY_WET, INTERMEDIATE, INTERMEDIATE_WET, WET
}
