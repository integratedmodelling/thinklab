package org.integratedmodelling.lpjguess;

public enum HydrologyType {
	RAINFED,
	IRRIGATED
}
