set(headers canexch.h driver.h growth.h guessio.h soilwater.h somdynam.h vegdynam.h)
set(source  canexch.cpp driver.cpp growth.cpp guessio.cpp soilwater.cpp somdynam.cpp vegdynam.cpp)

include(add_guess_sources)
