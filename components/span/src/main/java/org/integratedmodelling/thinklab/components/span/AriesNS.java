package org.integratedmodelling.thinklab.components.span;

import org.integratedmodelling.api.knowledge.IConcept;
import org.integratedmodelling.api.modelling.INamespace;
import org.integratedmodelling.common.configuration.KLAB;
import org.integratedmodelling.engine.KLABEngine;
import org.integratedmodelling.exceptions.ThinklabRuntimeException;

public class AriesNS {

    private static final String ARIES_NAMESPACE = "aries.es";

    public static IConcept TRAIL_PRESENCE;
    public static IConcept DIFFICULTY_OF_ACCESS;

    static final String TRAIL_PRESENCE_CONCEPT       = "im.infrastructure:TrailPresence";
    static final String DIFFICULTY_OF_ACCESS_CONCEPT = "im.recreation:DifficultyOfAccess";

    public static void synchronize() {

        // TODO move concepts here
        INamespace namespace = KLAB.MMANAGER.getNamespace(ARIES_NAMESPACE);

        if (namespace == null) {
            throw new ThinklabRuntimeException("namespace " + ARIES_NAMESPACE
                    + " is not available: ARIES functionalities cannot be activated");
        }

        TRAIL_PRESENCE = KLABEngine.get().getConcept(TRAIL_PRESENCE_CONCEPT);
        DIFFICULTY_OF_ACCESS = KLABEngine.get().getConcept(DIFFICULTY_OF_ACCESS_CONCEPT);

    }

}
