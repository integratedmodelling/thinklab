package org.integratedmodelling.thinklab.components.span;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;

import org.integratedmodelling.api.modelling.IDirectObservation;
import org.integratedmodelling.api.modelling.IExtent;
import org.integratedmodelling.api.modelling.IModel;
import org.integratedmodelling.api.modelling.IObservable;
import org.integratedmodelling.api.modelling.IObserver;
import org.integratedmodelling.api.modelling.IState;
import org.integratedmodelling.api.modelling.ISubject;
import org.integratedmodelling.api.modelling.resolution.IResolutionContext;
import org.integratedmodelling.api.modelling.scheduling.ITransition;
import org.integratedmodelling.api.monitoring.IMonitor;
import org.integratedmodelling.common.model.actuators.SubjectActuator;
import org.integratedmodelling.common.states.State;
import org.integratedmodelling.common.vocabulary.GeoNS;
import org.integratedmodelling.engine.KLABEngine;
import org.integratedmodelling.engine.geospace.extents.Grid;
import org.integratedmodelling.engine.geospace.extents.SpaceExtent;
import org.integratedmodelling.engine.modelling.runtime.Subject;
import org.integratedmodelling.engine.visualization.VisualizationFactory;
import org.integratedmodelling.exceptions.ThinklabException;
import org.integratedmodelling.exceptions.ThinklabIOException;

/*
 * this one does all the work. The individual SPAN accessor functions link to differently
 * configured instances for the various classes of behavior.
 */
public class SPANActuator extends SubjectActuator {

    // public static final String STREAM_CONCEPT = "im.hydrology:River";
    // public static final String ELEVATION_CONCEPT = "im.geo:ElevationSeaLevel";

    public static final String FINITE_CONCEPT   = "im:Finite";
    public static final String INFINITE_CONCEPT = "im:Infinite";

    public static final String RIVAL_BENEFIT_CONCEPT    = "im.eserv:Rival";
    public static final String NONRIVAL_BENEFIT_CONCEPT = "im.eserv:NonRival";

    static final String THEORETICAL_CONCEPT  = "aries.es:Theoretical";
    static final String INACCESSIBLE_CONCEPT = "aries.es:Inaccessible";
    static final String BLOCKED_CONCEPT      = "aries.es:Blocked";
    static final String ACTUAL_CONCEPT       = "aries.es:Actual";
    static final String POSSIBLE_CONCEPT     = "aries.es:Possible";

    static final String SOURCE_CONCEPT = "im.eserv:BenefitProduced";
    static final String SINK_CONCEPT   = "im.eserv:BenefitDepleted";
    static final String USE_CONCEPT    = "im.eserv:BenefitUsed";
    static final String FLOW_CONCEPT   = "im.eserv:BenefitFlowed";

    // /*
    // * thresholds
    // */
    // static final String SOURCE_THRESHOLD_CONCEPT = "aries.span:SPANSourceThreshold";
    // static final String USE_THRESHOLD_CONCEPT = "aries.span:SPANUseThreshold";
    // static final String SINK_THRESHOLD_CONCEPT = "aries.span:SPANSinkThreshold";
    // static final String TRANSITION_THRESHOLD_CONCEPT = "aries.span:SPANTransitionThreshold";

    /*
     * SPAN keys
     */

    /*
     * keys to select models
     */
    public static final String LINE_OF_SIGHT                  = "LineOfSight";
    public static final String PROXIMITY                      = "Proximity";
    public static final String CO2_REMOVED                    = "CO2Removed";
    public static final String FLOODWATER_MOVEMENT            = "FloodWaterMovement";
    public static final String SURFACE_WATER_MOVEMENT         = "SurfaceWaterMovement";
    public static final String FLOW_DIRECTION                 = "FlowDirection";
    public static final String SEDIMENT_TRANSPORT             = "SedimentTransport";
    public static final String COASTAL_STORM_MOVEMENT         = "CoastalStormMovement";
    public static final String SUBSISTENCE_FISH_ACCESSIBILITY = "SubsistenceFishAccessibility";

    static final String SOURCE_LAYER       = "source-layer";
    static final String SINK_LAYER         = "sink-layer";
    static final String USE_LAYER          = "use-layer";
    static final String FLOW_LAYERS        = "flow-layers";
    static final String ROWS               = "rows";
    static final String COLS               = "cols";
    static final String SOURCE_THRESHOLD   = "source-threshold";
    static final String SINK_THRESHOLD     = "sink-threshold";
    static final String USE_THRESHOLD      = "use-threshold";
    static final String TRANS_THRESHOLD    = "trans-threshold";
    static final String CELL_WIDTH         = "cell-width";
    static final String CELL_HEIGHT        = "cell-height";
    static final String RV_MAX_STATES      = "rv-max-states";
    static final String DOWNSCALING_FACTOR = "downscaling-factor";
    static final String SOURCE_TYPE        = "source-type";
    static final String SINK_TYPE          = "sink-type";
    static final String USE_TYPE           = "use-type";
    static final String BENEFIT_TYPE       = "benefit-type";
    static final String VALUE_TYPE         = "value-type";
    static final String FLOW_MODEL         = "flow-model";
    static final String RESULT_LAYERS      = "result-layers";

    /*
     * keys for result layers
     */
    static final String THEORETICAL_SOURCE  = "theoretical-source";
    static final String INACCESSIBLE_SOURCE = "inaccessible-source";
    static final String BLOCKED_SOURCE      = "blocked-source";
    static final String ACTUAL_SOURCE       = "actual-source";
    static final String POSSIBLE_SOURCE     = "possible-source";
    static final String THEORETICAL_SINK    = "theoretical-sink";
    static final String INACCESSIBLE_SINK   = "inaccessible-sink";
    static final String ACTUAL_SINK         = "actual-sink";
    static final String THEORETICAL_USE     = "theoretical-use";
    static final String INACCESSIBLE_USE    = "inaccessible-use";
    static final String BLOCKED_USE         = "blocked-use";
    static final String ACTUAL_USE          = "actual-use";
    static final String POSSIBLE_USE        = "possible-use";
    static final String BLOCKED_FLOW        = "blocked-flow";
    static final String ACTUAL_FLOW         = "actual-flow";
    static final String POSSIBLE_FLOW       = "possible-flow";

    String  _type;
    double  _sourceThreshold;
    double  _useThreshold;
    double  _sinkThreshold;
    double  _transitionThreshold;
    double  _downscalingFactor;
    boolean _animate;

    IObservable _benefitType;

    HashMap<String, IObservable>    _outputs         = new HashMap<String, IObservable>();
    HashMap<IObservable, IObserver> _outputObservers = new HashMap<IObservable, IObserver>();

    public SPANActuator(String type, double sourceThreshold, double sinkThreshold, double useThreshold,
            double transitionThreshold, double downscalingFactor, boolean animate, String version) {

        _type = type;
        _sourceThreshold = sourceThreshold;
        _sinkThreshold = sinkThreshold;
        _useThreshold = useThreshold;
        _transitionThreshold = transitionThreshold;
        _downscalingFactor = downscalingFactor;
        _animate = animate;
    }

    @Override
    public void notifyModel(IModel model) {
        _benefitType = model.getObservable();
    }

    @Override
    public void notifyExpectedOutput(IObservable observable, IObserver observer, String name) {

        // we need concepts from GeoNS and SpanNS
        GeoNS.synchronize();
        AriesNS.synchronize();

        if (observable.is(KLABEngine.c(INACCESSIBLE_CONCEPT)) && observable.is(KLABEngine.c(SOURCE_CONCEPT))) {
            _outputs.put(INACCESSIBLE_SOURCE, observable);
        } else if (observable.is(KLABEngine.c(BLOCKED_CONCEPT)) && observable.is(KLABEngine.c(SOURCE_CONCEPT))) {
            _outputs.put(BLOCKED_SOURCE, observable);
        } else if (observable.is(KLABEngine.c(ACTUAL_CONCEPT)) && observable.is(KLABEngine.c(SOURCE_CONCEPT))) {
            _outputs.put(ACTUAL_SOURCE, observable);
        } else if (observable.is(KLABEngine.c(POSSIBLE_CONCEPT))
                && observable.is(KLABEngine.c(SOURCE_CONCEPT))) {
            _outputs.put(POSSIBLE_SOURCE, observable);
        } else if (observable.is(KLABEngine.c(INACCESSIBLE_CONCEPT))
                && observable.is(KLABEngine.c(SINK_CONCEPT))) {
            _outputs.put(INACCESSIBLE_SINK, observable);
        } else if (observable.is(KLABEngine.c(ACTUAL_CONCEPT)) && observable.is(KLABEngine.c(SINK_CONCEPT))) {
            _outputs.put(ACTUAL_SINK, observable);
        } else if (observable.is(KLABEngine.c(INACCESSIBLE_CONCEPT))
                && observable.is(KLABEngine.c(USE_CONCEPT))) {
            _outputs.put(INACCESSIBLE_USE, observable);
        } else if (observable.is(KLABEngine.c(ACTUAL_CONCEPT)) && observable.is(KLABEngine.c(USE_CONCEPT))) {
            _outputs.put(ACTUAL_USE, observable);
        } else if (observable.is(KLABEngine.c(BLOCKED_CONCEPT)) && observable.is(KLABEngine.c(USE_CONCEPT))) {
            _outputs.put(BLOCKED_USE, observable);
        } else if (observable.is(KLABEngine.c(POSSIBLE_CONCEPT)) && observable.is(KLABEngine.c(USE_CONCEPT))) {
            _outputs.put(POSSIBLE_USE, observable);
        } else if (observable.is(KLABEngine.c(BLOCKED_CONCEPT)) && observable.is(KLABEngine.c(FLOW_CONCEPT))) {
            _outputs.put(BLOCKED_FLOW, observable);
        } else if (observable.is(KLABEngine.c(ACTUAL_CONCEPT)) && observable.is(KLABEngine.c(FLOW_CONCEPT))) {
            _outputs.put(ACTUAL_FLOW, observable);
        } else if (observable.is(KLABEngine.c(POSSIBLE_CONCEPT)) && observable.is(KLABEngine.c(FLOW_CONCEPT))) {
            _outputs.put(POSSIBLE_FLOW, observable);
        } else {
            monitor.error("output " + name + " cannot be classified a a SPAN output");
        }

        /*
         * we need these to reconstruct the states later
         */
        _outputObservers.put(observable, observer);
    }

    @SuppressWarnings("unchecked")
    @Override
    public ISubject initialize(ISubject subject, IDirectObservation context, IResolutionContext resolutionContext, IMonitor monitor)
            throws ThinklabException {

        GeoNS.synchronize();
        AriesNS.synchronize();

        HashMap<String, Object> spanParams = new HashMap<String, Object>();

        IState sourceState = null;
        IState sinkState = null;
        IState useState = null;

        IState elevation = null;
        IState streamNetwork = null;
        IState flowDirection = null;

        IExtent space = subject.getScale().getSpace();
        if (!(space instanceof SpaceExtent) || ((SpaceExtent) space).getGrid() == null) {
            monitor.error("SPAN models can only be computed on spatial grids for now");
            return null;
        } else {

        }

        Grid grid = ((SpaceExtent) space).getGrid();
        ArrayList<Object> debugStates = new ArrayList<Object>();

        /**
         * Classify the states and find source, use flow etc.
         */
        for (IState state : subject.getStates()) {

            if (state.getObservable().getType().is(KLABEngine.c(SOURCE_CONCEPT))
                    && state.getObservable().getType().is(KLABEngine.c(THEORETICAL_CONCEPT))) {
                if (sourceState != null) {
                    monitor.warn("multiple alternatives for SPAN supply: "
                            + sourceState.getObservable().getType()
                            + " ignored");
                } else {
                    sourceState = state;
                    monitor.info("using " + sourceState.getObservable().getType() + " as SPAN supply", null);
                    debugStates.add("source-layer");
                    debugStates.add(sourceState);
                }
            }
            if (state.getObservable().getType().is(KLABEngine.c(SINK_CONCEPT))
                    && state.getObservable().getType().is(KLABEngine.c(THEORETICAL_CONCEPT))) {
                if (sinkState != null) {
                    monitor.warn("multiple alternatives for SPAN depletion: "
                            + sinkState.getObservable().getType()
                            + " ignored");
                } else {
                    sinkState = state;
                    monitor.info("using " + sinkState.getObservable().getType() + " as SPAN depletion", null);
                    debugStates.add("sink-layer");
                    debugStates.add(sinkState);
                }
            }
            if (state.getObservable().getType().is(KLABEngine.c(USE_CONCEPT))
                    && state.getObservable().getType().is(KLABEngine.c(THEORETICAL_CONCEPT))) {
                if (useState != null) {
                    monitor.warn("multiple alternative for SPAN demand: "
                            + useState.getObservable().getType()
                            + " ignored");
                } else {
                    useState = state;
                    monitor.info("using " + useState.getObservable().getType() + " as SPAN demand", null);
                    debugStates.add("use-layer");
                    debugStates.add(useState);
                }
            }
            if (state.getObservable().getType().equals(GeoNS.PIT_FILLED_ELEVATION)
                    ||
                    (AriesNS.DIFFICULTY_OF_ACCESS != null && state.getObservable().getType()
                            .is(AriesNS.DIFFICULTY_OF_ACCESS))) {
                if (elevation != null) {
                    monitor.warn("multiple alternative for elevation: " + elevation.getObservable().getType()
                            + " ignored");
                } else {
                    elevation = state;
                    monitor.info("using " + state.getObservable().getType() + " as SPAN elevation", null);
                    debugStates.add("elevation-layer");
                    debugStates.add(elevation);
                }
            }

            if (state.getObservable().getType().equals(GeoNS.FLOW_DIRECTION)) {
                if (flowDirection != null) {
                    monitor.warn("multiple alternative for flow direction: "
                            + flowDirection.getObservable().getType()
                            + " ignored");
                } else {
                    flowDirection = state;
                    monitor.info("using " + state.getObservable().getType()
                            + " as SPAN flow direction", null);
                    debugStates.add("flow-direction-layer");
                    debugStates.add(flowDirection);
                }
            }
            /*
             * TODO this must check for the concept that observes the presence of a stream, using the
             * knowledge validator.
             */
            if (state.getObservable().is(GeoNS.STREAM_PRESENCE)
                    ||
                    (AriesNS.TRAIL_PRESENCE != null && state.getObservable().getType()
                            .is(AriesNS.TRAIL_PRESENCE))) {

                if (streamNetwork != null) {
                    monitor.warn("multiple alternative for flow network: "
                            + streamNetwork.getObservable().getType()
                            + " ignored");
                } else {
                    streamNetwork = state;
                    monitor.info("using " + state.getObservable().getType() + " as SPAN flow network", null);
                    debugStates.add("stream-network");
                    debugStates.add(streamNetwork);
                }
            }
            // if (state.getDirectType().is(Thinklab.c(SOURCE_THRESHOLD_CONCEPT))) {
            // double t = VisualizationFactory.get().getStateDataAsNumbers(state)[0];
            // if (!Double.isNaN(t)) {
            // _sourceThreshold = t;
            // }
            // }
            // if (state.getDirectType().is(Thinklab.c(SINK_THRESHOLD_CONCEPT))) {
            // double t = VisualizationFactory.get().getStateDataAsNumbers(state)[0];
            // if (!Double.isNaN(t)) {
            // _sinkThreshold = t;
            // }
            // }
            // if (state.getDirectType().is(Thinklab.c(USE_THRESHOLD_CONCEPT))) {
            // double t = VisualizationFactory.get().getStateDataAsNumbers(state)[0];
            // if (!Double.isNaN(t)) {
            // _useThreshold = t;
            // }
            // }
            // if (state.getDirectType().is(Thinklab.c(TRANSITION_THRESHOLD_CONCEPT))) {
            // double t = VisualizationFactory.get().getStateDataAsNumbers(state)[0];
            // if (!Double.isNaN(t)) {
            // _transitionThreshold = t;
            // }
            // }
        }

        ArrayList<String> outputKeys = new ArrayList<String>();
        if (_outputs.size() == 0) {
            monitor.warn("no SPAN outputs requested: model will not produce any output");
        } else {
            outputKeys.addAll(_outputs.keySet());
        }

        if (sourceState == null || useState == null) {
            monitor.error("source and/or use inputs were not provided. SPAN exiting");
            return null;
        }

        /*
         * an empty stream network gets the models stuck forever, so check.
         */
        if (streamNetwork != null) {
            boolean isEmpty = true;
            for (int i = 0; isEmpty && i < streamNetwork.getValueCount(); i++) {
                Object val = ((State) streamNetwork).getValue(i);
                if (val != null
                        && ((val instanceof Number && ((Number) val).doubleValue() > 0)
                                || (val instanceof Boolean && ((Boolean) val))))
                    isEmpty = false;
            }

            if (isEmpty) {
                monitor.error("stream network is empty. SPAN cannot proceed.");
                return null;
            }
        }

        /*
         * call pit filling algorithm on the DEM if we're dealing with water
         * Not anymore - this only runs in a watershed and has the pit-filled
         * DEM as a dependency.
         */
        // if (elevation != null
        // && (_type == FLOODWATER_MOVEMENT || _type == SURFACE_WATER_MOVEMENT || _type ==
        // SEDIMENT_TRANSPORT)) {
        // monitor.info("filling sinks", GISOperations.INFO_CLASS);
        // elevation = GISOperations.fillSinks(elevation, subject, monitor);
        // }

        /*
         * types: rival and final
         */
        spanParams.put(SOURCE_TYPE, getStateType(sourceState));
        spanParams.put(SINK_TYPE, getStateType(sinkState));
        spanParams.put(USE_TYPE, getStateType(useState));
        spanParams.put(BENEFIT_TYPE, getBenefitType(_benefitType));

        /*
         * there you go
         */
        spanParams.put(FLOW_MODEL, _type);
        spanParams.put(ROWS, grid.getYCells());
        spanParams.put(COLS, grid.getXCells());
        spanParams.put(CELL_WIDTH, grid.getCellWidthMeters());
        spanParams.put(CELL_HEIGHT, grid.getCellHeightMeters());
        spanParams.put(RESULT_LAYERS, outputKeys.toArray());
        spanParams.put(SOURCE_THRESHOLD, _sourceThreshold);
        spanParams.put(USE_THRESHOLD, _useThreshold);
        spanParams.put(SINK_THRESHOLD, _sinkThreshold);
        spanParams.put(TRANS_THRESHOLD, _transitionThreshold);
        spanParams.put(DOWNSCALING_FACTOR, (int) (_downscalingFactor + 0.1));
        spanParams.put("animation?", _animate);

        HashMap<String, double[]> flowLayers = new HashMap<String, double[]>();
        if (elevation != null) {
            flowLayers.put("Altitude", VisualizationFactory.get().getStateDataAsNumbers(elevation));
        }
        if (streamNetwork != null) {
            flowLayers.put("River", VisualizationFactory.get().getStateDataAsNumbers(streamNetwork));
        }
        if (flowDirection != null) {
            flowLayers.put("FlowDirection", VisualizationFactory.get().getStateDataAsNumbers(flowDirection));
        }

        spanParams.put(FLOW_LAYERS, flowLayers);

        /*
         * these later, OK?
         */
        spanParams.put("rv-max-states", 15);
        spanParams.put(VALUE_TYPE, "numbers");
        spanParams.put("monitor", monitor);

        double[] sourceValues = VisualizationFactory.get().getStateDataAsNumbers(sourceState);
        double[] useValues = VisualizationFactory.get().getStateDataAsNumbers(useState);

        // String sourcefuc = Debug.describeValues(sourceValues);
        // String usefuc = Debug.describeValues(useValues);
        // String sinfuc = null;

        spanParams.put(SOURCE_LAYER, sourceValues);
        spanParams.put(USE_LAYER, useValues);
        if (sinkState != null) {
            spanParams.put(SINK_LAYER, VisualizationFactory.get().getStateDataAsNumbers(sinkState));
            // sinfuc = Debug.describeValues(useValues);
        }

        /*
         * TODO remove or condition 
         */
        saveForClojure(new File("span-debug.clj"), grid, grid.getXCells(), grid.getYCells(), debugStates
                .toArray());

        if (monitor.hasErrors())
            return null;

        /*
         * good luck man
         */
        monitor.info("running SPAN model " + _type, null);

        HashMap<String, Object> resultMap = null;

        try {
            resultMap = clj_span.java_span_bridge.runSpan(spanParams);
        } catch (Throwable e) {
            monitor.error(e.getMessage());
            return null;
        }

        monitor.info("SPAN computation finished " + _type, null);

        /*
         * TODO add observables
         */
        if (resultMap != null) {
            for (String key : resultMap.keySet()) {

                Object data = resultMap.get(key);

                if (!(data instanceof Double[])) {
                    monitor.warn("unrecognized SPAN output for " + _outputs.get(key));
                } else {
                    ((Subject) subject)
                            .addState(new State(data, _outputs.get(key), subject.getScale(), true));
                }
            }
        }

        return subject;
    }

    private String getStateType(IState state) {

        if (state == null)
            return null;

        if (state.getObservable().getType().is(KLABEngine.c(FINITE_CONCEPT))) {
            return "finite";
        } else if (state.getObservable().getType().is(KLABEngine.c(INFINITE_CONCEPT))) {
            return "infinite";
        }

        monitor.error("SPAN input " + state.getObservable().getType()
                + " could not be categorized as finite or infinite");

        return null;
    }

    private String getBenefitType(IObservable observable) {

        if (observable.is(KLABEngine.c(RIVAL_BENEFIT_CONCEPT))) {
            return "rival";
        } else if (observable.is(KLABEngine.c(NONRIVAL_BENEFIT_CONCEPT))) {
            return "non-rival";
        }

        monitor.error("cannot categorize SPAN process " + observable + " as rival or non-rival");

        return null;
    }

    /**
     * Save Clojure arrays for all the passed states. Expects a pair String, IState per state to save.
     * 
     * @param file
     * @param states
     * @throws ThinklabException 
     */
    private void saveForClojure(File file, Grid grid, int x, int y, Object... states)
            throws ThinklabException {

        PrintWriter out = null;
        try {
            out = new PrintWriter(new FileOutputStream(file, false));

            out.println("(ns clj-span.debug-data)\n");

            out.println("(def rows " + y + ")");
            out.println("(def cols " + x + ")\n");

            for (int i = 0; i < states.length; i++) {

                String name = (String) states[i];
                IState stat = (IState) states[++i];

                double[] data = VisualizationFactory.get().getStateDataAsNumbers(stat);

                if (data != null) {

                    out.println("(def " + name + " [");

                    for (int row = 0; row < y; row++) {
                        out.print("   [");
                        for (int col = 0; col < x; col++) {
                            double d = data[grid.getOffset(col, row)];
                            out.print((col == 0 ? "" : " ") + (Double.isNaN(d) ? 0.0 : d));
                        }
                        out.println("]");
                    }

                    out.println("])\n\n");
                }
            }

        } catch (FileNotFoundException e) {
            throw new ThinklabIOException(e);
        } finally {
            if (out != null)
                out.close();
        }

    }

    @Override
    public boolean processTransition(ITransition transition, IMonitor monitor) throws ThinklabException {
        // TODO Auto-generated method stub
        return true;
    }

}
