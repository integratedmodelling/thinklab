package org.integratedmodelling.thinklab.components.span.functions;

import java.util.Map;

import org.integratedmodelling.api.knowledge.IConcept;
import org.integratedmodelling.api.knowledge.IExpression;
import org.integratedmodelling.api.monitoring.IMonitor;
import org.integratedmodelling.api.services.annotations.Prototype;
import org.integratedmodelling.common.vocabulary.NS;
import org.integratedmodelling.exceptions.ThinklabException;
import org.integratedmodelling.thinklab.components.span.SPANActuator;

@Prototype(
        id = "span.sediment-transport",
        args = {
                "# span-version",
                Prototype.TEXT,
                "# transition-threshold",
                Prototype.FLOAT,
                "# downscaling-factor",
                Prototype.FLOAT,
                "# animate",
                Prototype.BOOLEAN,
                "# sink-threshold",
                Prototype.FLOAT,
                "# source-threshold",
                Prototype.FLOAT,
                "# use-threshold",
                Prototype.FLOAT },
        returnTypes = { NS.PROCESS_CONTEXTUALIZER })
public class SEDIMENT_TRANSPORT implements IExpression {

    @Override
    public Object eval(Map<String, Object> parameters, IMonitor monitor, IConcept... context)
            throws ThinklabException {

        /*
         * these will be overridden by observations
         */
        double transitionThreshold = parameters.containsKey("transition-threshold") ? Double
                .parseDouble(parameters.get("transition-threshold").toString()) : 1.0;
        double sourceThreshold = parameters.containsKey("source-threshold") ? Double
                .parseDouble(parameters.get("source-threshold").toString()) : 0.0;
        double sinkThreshold = parameters.containsKey("sink-threshold") ? Double
                .parseDouble(parameters.get("sink-threshold").toString()) : 0.0;
        double useThreshold = parameters.containsKey("use-threshold") ? Double
                .parseDouble(parameters.get("use-threshold").toString()) : 0.0;

        double downscalingFactor = parameters.containsKey("downscaling-factor") ? Double
                .parseDouble(parameters.get("downscaling-factor").toString()) : 1.0;

        boolean animate = parameters.containsKey("animate") ? (Boolean) parameters.get("animate")
                : Boolean.FALSE;

        String version = parameters.containsKey("span-version") ? parameters.get("span-version").toString()
                : "1.0";

        return new SPANActuator(SPANActuator.SEDIMENT_TRANSPORT, sourceThreshold, sinkThreshold, useThreshold, transitionThreshold, downscalingFactor, animate, version);
    }

}
