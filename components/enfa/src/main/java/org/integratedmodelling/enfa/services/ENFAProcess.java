/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.enfa.services;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.Vector;

import org.integratedmodelling.api.modelling.IDirectObservation;
import org.integratedmodelling.api.modelling.IModel;
import org.integratedmodelling.api.modelling.INumericObserver;
import org.integratedmodelling.api.modelling.IObservable;
import org.integratedmodelling.api.modelling.IObservation;
import org.integratedmodelling.api.modelling.IObserver;
import org.integratedmodelling.api.modelling.IPresenceObserver;
import org.integratedmodelling.api.modelling.IProcess;
import org.integratedmodelling.api.modelling.IScale;
import org.integratedmodelling.api.modelling.IState;
import org.integratedmodelling.api.modelling.resolution.IResolutionContext;
import org.integratedmodelling.api.modelling.runtime.IProcessContextualizer;
import org.integratedmodelling.api.modelling.scheduling.ITransition;
import org.integratedmodelling.api.monitoring.IMonitor;
import org.integratedmodelling.api.project.IProject;
import org.integratedmodelling.api.services.annotations.Prototype;
import org.integratedmodelling.common.states.States;
import org.integratedmodelling.common.vocabulary.NS;
import org.integratedmodelling.enfa.math.IMatrix;
import org.integratedmodelling.enfa.math.MatrixFactory;
import org.integratedmodelling.enfa.math.NicheFactorAnalysis;
import org.integratedmodelling.engine.KLABEngine;
import org.integratedmodelling.exceptions.ThinklabException;
import org.integratedmodelling.exceptions.ThinklabValidationException;

@Prototype(
        id = "im.enfa.go",
        componentId = "im.enfa",
        published = false,
        returnTypes = { NS.PROCESS_CONTEXTUALIZER },
        args = { "? nf", Prototype.INT })
public class ENFAProcess implements IProcessContextualizer {

    boolean                    canDispose      = false;
    IMonitor                   monitor;
    private IScale             scale;
    public static final String OCCURANCE_TRAIT = "im.enfa:Occurrence";

    Map<String, IObservation> outputStates = new HashMap<>();

    private Set<String>     occurrenceLayers  = new HashSet<String>();
    private Set<String>     numericInputs     = new HashSet<String>();
    private Set<String>     booleanInputs     = new HashSet<String>();


    int             nf                = 0;
    int             rows              = 0;
    int             cols              = 0;
    private boolean occurrenceNumeric = true;
    private IMatrix egvs;
    private IMatrix pres;

    @Override
    public boolean canDispose() {
        return canDispose;
    }

    @Override
    public void setContext(Map<String, Object> parameters, IModel model, IProject project) {
        if (parameters.containsKey("nf")) {
            nf = ((Number) parameters.get("nf")).intValue();
        }
    }

    @Override
    public Map<String, IObservation> initialize(IProcess process, IDirectObservation context, IResolutionContext resolutionContext, Map<String, IObservable> expectedInputs, Map<String, IObservable> expectedOutputs, IMonitor monitor)
            throws ThinklabException {
    	
        monitor.info("enfa called", "Message Detail");

        canDispose = !context.getScale().isTemporallyDistributed();
        this.monitor = monitor;
        this.scale = context.getScale();
        // if (scale.isSpatiallyDistributed() || scale.getSpace().getGrid() == null) {
        // throw new
        // ThinklabUnsupportedOperationException("the ENFA process contextualizer only works on gridded
        // contexts");
        // }
        if (expectedOutputs.isEmpty())
            throw new ThinklabException("enfa needs at least one output observable");
        if (expectedInputs.isEmpty() || expectedInputs.size() < 2)
            throw new ThinklabException("enfa needs at least two input observables");

        // Go through inputs and find the presence and EGV layers
        for (String inp : expectedInputs.keySet()) {
            IObservable o = expectedInputs.get(inp);
            monitor.info("enfa:" + inp + " (scanning)", "Message Detail");
            if (o.getTypeAsConcept().is(KLABEngine.c(OCCURANCE_TRAIT))
                    || o.getTypeAsConcept().toString().toLowerCase().contains("occurrence")) {
                occurrenceLayers.add(inp);
            } else {// it is Input (EGV)
                IObserver obsrv = o.getObserver();
                if (obsrv instanceof INumericObserver)
                    // Could it be measurement, probability, count, etc
                    numericInputs.add(inp);
                else if (obsrv instanceof IPresenceObserver)
                    booleanInputs.add(inp);
                else
                    throw new ThinklabValidationException("enfa: input state " + inp + " is not numeric");
            }
        }
        { // find out the size of the tables
            cols = numericInputs.size() + booleanInputs.size();
            rows = (int) scale.getMultiplicity();
        }
        monitor.info("enfa: rows " + rows + " cols " + cols, "Message Detail");

        Map<String, IState> states = States.matchStatesToInputs(context, expectedInputs);

        Set<Integer> missingIndexes = new HashSet<Integer>();
        
        int col = 0;     
        for (String inp : numericInputs) {
            monitor.info("enfa:" + inp + " (processing)", "Message Detail");

            IState state = states.get(inp);
            int row = 0;
            for (int n : scale.getIndex((IScale.Locator) null)) {
                double value = States.getDouble(state, n);
                if (Double.isNaN(value)) {
                	missingIndexes.add(n);
                }
                row++;
            }
            monitor.info("Missing " + missingIndexes.size() + " in " + inp , null);
            col++;
        }
        
        
        egvs = MatrixFactory.createMatrix(rows-missingIndexes.size(), cols);

        

        
        col = 0;     
        for (String inp : numericInputs) {
            monitor.info("enfa:" + inp + " (reading values)", "Message Detail");

            IState state = states.get(inp);
            int row = 0;
            for (int n : scale.getIndex((IScale.Locator) null)) {
            	if(!missingIndexes.contains(n)){
            		double value = States.getDouble(state, n);
                    egvs.setElement(row, col, value);
                    row++;
                    }
            }
            monitor.info("Missing " + missingIndexes.size() + " in " + inp , null);
            col++;
        }

        for (String inp : booleanInputs) {
            monitor.info("enfa:" + inp + " (processing)", "Message Detail");
            IState state = context.getState(expectedOutputs.get(inp));
            int row = 0;
            for (int n : scale.getIndex((IScale.Locator) null)) {
                boolean value = States.getBoolean(state, n);
                egvs.setElement(row, col, (value ? 1 : 0));
                row++;
            }
            col++;
        }

        // // TODO: Presence data should be filtered according to Output traits
        pres = MatrixFactory.createMatrix(rows-missingIndexes.size(), 1);

        for (String inp : occurrenceLayers) {
            monitor.info("enfa: occurrence " + inp + " (processing)", "Message Detail");

            IState state = states.get(inp);
            int row = 0;
            for (int n : scale.getIndex((IScale.Locator) null)) {
                if(!missingIndexes.contains(n)){
                	double value = pres.getElement(row, 0);
                	if (occurrenceNumeric)
                		value += States.getDouble(state, n);
                	else{
                		boolean bool = States.getBoolean(state, n);
                		value += (bool ? 1 : 0);
                		}
                	pres.setElement(row, 0, value);
                	row++;
                }
                
            }
        }

        pres.printStatistics("Occurrence");

        for (String out : expectedOutputs.keySet()) {
            monitor.info("enfa:" + out + " (calc)", "Message Detail");

            IObservable obs = expectedOutputs.get(out);
            // TODO: Based on the expected output traits, need to filter the Occurrence Data in Matrix pres.

            NicheFactorAnalysis model = new NicheFactorAnalysis();
            IMatrix maha = model.train(pres, egvs);
            monitor.info("enfa: result " + maha.getNumberOfRows() + " (states)", "Message Detail");

            IState outState = context.getState(obs);

            /**
             * To access all the states within a transition (e.g. all points in space at the time) we use 
             * the scale index. and pass the transition, which implements IScale.Locator - an interface that
             * "locks" one or more dimensions and returns an iterator for the states along the others.
             * We pass a null here, which is understood as the initialization transition. There are locators
             * for space.
              */
            int row = 0;
            for (int n : scale.getIndex((IScale.Locator) null)) {
            	if(!missingIndexes.contains(n)){
            		double value = maha.getElement(row, 0);
            		States.set(outState, value, n);
            		row++;
            	} else{
            		States.set(outState, Double.NaN, n);
            	}
            }

            /**
             * Set the state as an output. This phase isn't strictly necessary as createState() has already
             * created it in the subject - API may change later.
             */
            outputStates.put(out, outState);

        }

        /** 
         * the software will take care of setting these inputs in the context or 
         * streaming them back to the calling engine if we're a remote service.
         */
        return outputStates;
    }



    private IMatrix prepareEGVs(Map<String, IState> inputs) throws ThinklabException {
        if (inputs.isEmpty())
            throw new ThinklabException("enfa cant create states");
        // Check that all states have the same number of values.
        String firstkey = (String) inputs.keySet().toArray()[0];
        long length = inputs.get(firstkey).getValueCount();
        IMatrix m = MatrixFactory.createMatrix(inputs.size(), (int) length);
        int col = 0;
        for (String key : inputs.keySet()) {
            IState state = inputs.get(key);
            if (state.getValueCount() != length)
                throw new ThinklabException("enfa states do not have the same dimentions");
            for (int row = 0; row < length; row++) {
                m.setElement(row, col, (double) state.getValue(row));
            }
        }
        return m;
    }

    @Override
    public Map<String, IObservation> compute(ITransition transition, Map<String, IState> inputs)
            throws ThinklabException {
        canDispose = transition.isLast();
        return null;
    }

}
