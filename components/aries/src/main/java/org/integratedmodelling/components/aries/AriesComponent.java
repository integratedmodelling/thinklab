/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.components.aries;

import org.integratedmodelling.Version;
import org.integratedmodelling.api.knowledge.IConcept;
import org.integratedmodelling.api.knowledge.IProperty;
import org.integratedmodelling.api.plugin.Component;
import org.integratedmodelling.api.plugin.Initialize;
import org.integratedmodelling.common.configuration.KLAB;
import org.integratedmodelling.common.vocabulary.EcologyNS;
import org.integratedmodelling.exceptions.ThinklabException;

@Component(
        id = "aries",
        version = Version.CURRENT,
        domain = "im",
        requiresProjects = { "im", "im.aries" },
        allowedGroups = { "ISU", "ARIES", "SES" })
public class AriesComponent {

    public static class NS extends org.integratedmodelling.common.vocabulary.EcologyNS {

        public static IConcept  WATER_FLOWED;
        public static IConcept  WATER_CONSUMED;
        public static IConcept  RIVAL_TRAIT;
        public static IProperty VISUAL_CONNECTION;
        public static IProperty WATER_CONSUMPTION_CONNECTION;
        public static IProperty WATER_NON_CONSUMPTION_CONNECTION;
        public static IConcept  PROVIDER_TRAIT;
        public static IConcept  TRANSACTOR_TRAIT;
        public static IConcept  BENEFICIARY_TRAIT;
        public static IConcept  VISUAL_BLIGHT;

        static boolean _initialized;
        static boolean _initedOk;

        public static boolean synchronize() {

            if (!_initialized) {

                if (!EcologyNS.synchronize()) {
                    return false;
                }

                _initialized = false;

                try {

                    PROVIDER_TRAIT = checkClass(KLAB.MMANAGER
                            .getExportedKnowledge("ARIES.PROVIDER_TRAIT"), IConcept.class);
                    RIVAL_TRAIT = checkClass(KLAB.MMANAGER
                            .getExportedKnowledge("ARIES.RIVAL_TRAIT"), IConcept.class);
                    WATER_CONSUMED = checkClass(KLAB.MMANAGER
                            .getExportedKnowledge("ARIES.WATER_CONSUMED"), IConcept.class);
                    WATER_FLOWED = checkClass(KLAB.MMANAGER
                            .getExportedKnowledge("ARIES.WATER_FLOWED"), IConcept.class);
                    TRANSACTOR_TRAIT = checkClass(KLAB.MMANAGER
                            .getExportedKnowledge("ARIES.TRANSACTOR_TRAIT"), IConcept.class);
                    BENEFICIARY_TRAIT = checkClass(KLAB.MMANAGER
                            .getExportedKnowledge("ARIES.BENEFICIARY_TRAIT"), IConcept.class);
                    VISUAL_BLIGHT = checkClass(KLAB.MMANAGER
                            .getExportedKnowledge("ARIES.VISUAL_BLIGHT"), IConcept.class);

                    VISUAL_CONNECTION = checkClass(KLAB.MMANAGER
                            .getExportedKnowledge("ARIES.VISUAL_CONNECTION"), IProperty.class);
                    WATER_CONSUMPTION_CONNECTION = checkClass(KLAB.MMANAGER
                            .getExportedKnowledge("ARIES.WATER_CONSUMPTION_CONNECTION"), IProperty.class);
                    WATER_NON_CONSUMPTION_CONNECTION = checkClass(KLAB.MMANAGER
                            .getExportedKnowledge("ARIES.WATER_NON_CONSUMPTION_CONNECTION"), IProperty.class);

                    _initedOk = true;

                } catch (ThinklabException e) {
                    return false;
                }
            }
            return true;
        }
    }

    @Initialize
    public boolean initialize() {
        return NS.synchronize();
    }

}
