/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.components.aries.es.services;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import org.integratedmodelling.api.knowledge.IConcept;
import org.integratedmodelling.api.modelling.IDirectObservation;
import org.integratedmodelling.api.modelling.IModel;
import org.integratedmodelling.api.modelling.IObservable;
import org.integratedmodelling.api.modelling.IObservation;
import org.integratedmodelling.api.modelling.IProcess;
import org.integratedmodelling.api.modelling.IRelationship;
import org.integratedmodelling.api.modelling.IScale;
import org.integratedmodelling.api.modelling.IState;
import org.integratedmodelling.api.modelling.ISubject;
import org.integratedmodelling.api.modelling.resolution.IResolutionContext;
import org.integratedmodelling.api.modelling.runtime.IProcessContextualizer;
import org.integratedmodelling.api.modelling.scheduling.ITransition;
import org.integratedmodelling.api.monitoring.IMonitor;
import org.integratedmodelling.api.project.IProject;
import org.integratedmodelling.api.services.annotations.Prototype;
import org.integratedmodelling.common.configuration.KLAB;
import org.integratedmodelling.common.kim.ModelFactory;
import org.integratedmodelling.common.space.IGeometricShape;
import org.integratedmodelling.common.states.States;
import org.integratedmodelling.common.vocabulary.GeoNS;
import org.integratedmodelling.common.vocabulary.NS;
import org.integratedmodelling.common.vocabulary.Observable;
import org.integratedmodelling.components.aries.AriesComponent;
import org.integratedmodelling.components.aries.FlowHelper;
import org.integratedmodelling.engine.geospace.gis.SextanteOperations;
import org.integratedmodelling.engine.geospace.literals.ShapeValue;
import org.integratedmodelling.engine.modelling.runtime.Relationship;
import org.integratedmodelling.engine.modelling.runtime.Scale;
import org.integratedmodelling.exceptions.ThinklabException;
import org.integratedmodelling.exceptions.ThinklabUnsupportedOperationException;

@Prototype(
        id = "im.aries.aesthetic-flow",
        componentId = "aries",
        args = { "? d|distance-threshold", Prototype.FLOAT },
        argDescriptions = {
                "distance threshold in km (no connections created beyond this) - default 100km"
        },
        returnTypes = { NS.PROCESS_CONTEXTUALIZER })
public class AestheticViewBenefitContextualizer implements IProcessContextualizer {

    IProject           project;
    boolean            canDispose;
    IScale             scale;
    IMonitor           monitor;
    IDirectObservation context;
    IResolutionContext resolutionContext;

    IState   elevation;
    IState   visualBlight;
    IConcept viewshedObservable;

    // SpatialDisplay debug;

    Map<ISubject, IState> viewsheds = new HashMap<>();
    private IProcess      process;

    /*
     * TODO set into a parameter
     */
    private double DISTANCE_THRESHOLD_KM = 100;

    @Override
    public boolean canDispose() {
        return canDispose;
    }

    @Override
    public void setContext(Map<String, Object> parameters, IModel model, IProject project) {
        this.project = project;
    }

    /**
     * Get the viewshed of a particular transactor, computing it if necessary.
     * 
     * @param transactor
     * @return
     * @throws ThinklabException 
     */
    public IState getViewshed(ISubject transactor) throws ThinklabException {

        IState ret = viewsheds.get(transactor);
        if (ret == null) {
            Observable observable = new Observable(ModelFactory
                    .presenceObserver(viewshedObservable), transactor, "visible-region");
            monitor.info("computing viewshed of " + transactor.getId(), null);
            ret = SextanteOperations.getRasterViewshed(elevation, context, transactor, observable, monitor);
            viewsheds.put(transactor, ret);
        }
        return ret;
    }

    @Override
    public Map<String, IObservation> initialize(IProcess process, IDirectObservation context, IResolutionContext resolutionContext, Map<String, IObservable> expectedInputs, Map<String, IObservable> expectedOutputs, IMonitor monitor)
            throws ThinklabException {

        this.resolutionContext = resolutionContext;

        Map<String, IObservation> ret = new HashMap<>();

        /*
         * FIXME! now makes the client choke because the reconstruction of concepts created remotely isn't finished.
         */
        IConcept visregion = NS.addTraits(GeoNS.PLAIN_REGION, Collections.singleton(GeoNS.VISIBLE_TRAIT));
        this.viewshedObservable = NS.makePresence(visregion);

        this.context = context;
        this.process = process;
        this.scale = context.getScale();
        this.canDispose = !scale.isTemporallyDistributed();
        this.monitor = monitor;

        // debug = new SpatialDisplay((SpaceExtent) scale.getSpace());

        if (!this.scale.isSpatiallyDistributed() || this.scale.getSpace().getGrid() == null) {
            throw new ThinklabUnsupportedOperationException("the aesthetic benefit contextualizer only works on gridded contexts for now");
        }

        /*
         * find elevation and visual blight state if any.
         */
        this.elevation = States.findState(context, AriesComponent.NS.ELEVATION);
        this.visualBlight = States.findState(context, AriesComponent.NS.VISUAL_BLIGHT);

        for (IRelationship rel : buildFlowNetwork(context)) {
            ret.put(rel.getId(), rel);
        }

        /*
         * temporary - may want to associate to an option
         */
        for (ISubject s : viewsheds.keySet()) {
            ret.put(s.getId() + "_viewshed", s);
        }

        return ret;
    }

    private Iterable<IRelationship> buildFlowNetwork(IDirectObservation context) throws ThinklabException {

        ArrayList<IRelationship> ret = new ArrayList<>();

        int nc = 0;
        for (ISubject provider : FlowHelper
                .getProviders((ISubject) context, process.getObservable(), resolutionContext)) {

            // debug.add((IGeometricShape) provider.getScale().getSpace().getShape(), "providers");

            for (ISubject transactor : FlowHelper
                    .getTransactors((ISubject) context, process.getObservable(), resolutionContext)) {
                IRelationship rel = buildVisualConnection(provider, transactor);
                if (rel != null) {
                    ((ISubject) context).getStructure().link(provider, transactor, rel);
                    ret.add(rel);
                    nc++;
                }
            }
        }

        if (nc == 0) {
            monitor.warn("no visual connections: no aesthetic flows in the context");
        } else {
            monitor.info(nc + " visual connections made", null);
        }

        /*
         * TODO use a generalized connector function to link beneficiaries to transactors. At the
         * moment we have no beneficiaries. The model used should be a use-in-place, come-from-anywhere
         * type.
         */

        // debug.show();

        return ret;
    }

    private IRelationship buildVisualConnection(ISubject provider, ISubject transactor)
            throws ThinklabException {

        /*
         * create or retrieve the viewshed for the transactor...
         */
        IState viewshed = getViewshed(transactor);
        /*
         * ...and the provider-specific piece of it
         */
        IState tviewshed = States.getView(viewshed, provider);

        /*
         * false = not a bit of provider in view of this transactor.
         */
        boolean inView = (Boolean) tviewshed.getValue(0);
        if (!inView) {
            return null;
        }

        double percentInView = 1.0;
        if (tviewshed.getMetadata().contains(IState.Mediator.SPACE_TOTAL_VALUES)) {
            percentInView = (double) tviewshed.getMetadata().getInt(IState.Mediator.SPACE_VALUE_SUM)
                    / (double) tviewshed.getMetadata().getInt(IState.Mediator.SPACE_TOTAL_VALUES);
        }

        /*
         * Visual connection's spatial context: polygon joining each viewpoint and the whole feature.
         */
        ShapeValue connectionField = FlowHelper
                .createJoiningShape(provider.getScale().getSpace().getShape(), transactor
                        .getScale().getSpace().getShape());

        // clip to overall context - there's a slight change that the convex hull operation creates weird
        // things.
        connectionField = ((ShapeValue) scale.getSpace().getShape()).intersection(connectionField);

        // debug.add(connectionField, "connections");

        /*
         * intersect the connection with the viewshed and visual blight
         */

        IState cviewfield = States.getView(viewshed, Scale.substituteExtent(this.scale, connectionField
                .asExtent()));

        double opennessValue = 1.0; // (double) visib / (double) total;
        // this is obviously true but we need the side effects.
        cviewfield.getValue(0);
        if (cviewfield.getMetadata().contains(IState.Mediator.SPACE_TOTAL_VALUES)) {
            opennessValue = (double) cviewfield.getMetadata().getInt(IState.Mediator.SPACE_VALUE_SUM)
                    / (double) cviewfield.getMetadata().getInt(IState.Mediator.SPACE_TOTAL_VALUES);
        }

        /*
         * TODO visual blight
         */
        double totalBlight = Double.NaN;
        if (visualBlight != null) {
            IState ugly = States.getView(visualBlight, Scale.substituteExtent(this.scale, connectionField
                    .asExtent()));
            totalBlight = (double) ugly.getValue(0);
        }

        /*
         * value of provider depends on distance, relative area seen, total area, and visual blight in between.
         * TODO the way these are aggregated should be configurable through an expression in parameters.
         */
        double providerValue = provider.getScale().getSpace().getShape().getArea();
        double distance = GeoNS
                .getDistance((IGeometricShape) provider.getScale().getSpace()
                        .getShape(), (IGeometricShape) transactor
                                .getScale()
                                .getSpace().getShape());

        if (distance > DISTANCE_THRESHOLD_KM) {
            return null;
        }

        monitor.info(transactor.getId() + " sees "
                + NumberFormat.getPercentInstance().format(percentInView) + " of " + provider.getId()
                + " in a "
                + NumberFormat.getPercentInstance().format(opennessValue)
                + " open field", null);

        /*
         * create the relationship
         */
        String cname = provider.getId() + "->" + transactor.getId();
        Relationship ret = new Relationship(((ISubject) context)
                .getStructure(), new Observable(AriesComponent.NS.VISUAL_CONNECTION), Scale
                        .substituteExtent(context.getScale(), connectionField.asExtent()), KLAB.MMANAGER
                                .getLocalNamespace(), provider, transactor, cname, monitor);

        /*
         * add states for all the info
         */
        IState dState = ret
                .getStaticState(new Observable(ModelFactory
                        .measureObserver(NS.DISTANCE, "km"), null, "viewing-distance"));
        States.set(dState, distance, 0);
        // IState bState = ret
        // .getStaticState(new Observable(ModelFactory
        // .proportionObserver(AriesComponent.NS.VISUAL_BLIGHT), ret, "viewing-distance"));
        // States.set(bState, distance, 0);

        return ret;
    }

    @Override
    public Map<String, IObservation> compute(ITransition transition, Map<String, IState> inputs)
            throws ThinklabException {

        canDispose = transition.isLast();
        // TODO Auto-generated method stub
        return null;
    }

}
