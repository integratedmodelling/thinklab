/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.components.aries;

import java.util.ArrayList;

import org.integratedmodelling.api.modelling.IObservable;
import org.integratedmodelling.api.modelling.ISubject;
import org.integratedmodelling.api.modelling.resolution.IResolutionContext;
import org.integratedmodelling.api.space.IShape;
import org.integratedmodelling.common.space.IGeometricShape;
import org.integratedmodelling.common.vocabulary.NS;
import org.integratedmodelling.engine.geospace.Geospace;
import org.integratedmodelling.engine.geospace.literals.ShapeValue;

import com.vividsolutions.jts.algorithm.ConvexHull;
import com.vividsolutions.jts.geom.Coordinate;
import com.vividsolutions.jts.geom.Geometry;

public class FlowHelper {

    static public Iterable<ISubject> getProviders(ISubject subject, IObservable process, IResolutionContext context) {
        ArrayList<ISubject> ret = new ArrayList<>();
        for (ISubject s : subject.getSubjects()) {
            if (NS.hasRole(s, AriesComponent.NS.PROVIDER_TRAIT, subject
                    .getObservable(), process, context)) {
                ret.add(s);
            }
        }
        return ret;
    }

    static public Iterable<ISubject> getTransactors(ISubject subject, IObservable process, IResolutionContext resolutionContext) {
        ArrayList<ISubject> ret = new ArrayList<>();
        for (ISubject s : subject.getSubjects()) {
            if (NS.hasRole(s, AriesComponent.NS.TRANSACTOR_TRAIT, subject
                    .getObservable(), process, resolutionContext)) {
                ret.add(s);
            }
        }
        return ret;
    }

    static public Iterable<ISubject> getBeneficiaries(ISubject subject, IObservable process, IResolutionContext resolutionContext) {
        ArrayList<ISubject> ret = new ArrayList<>();
        for (ISubject s : subject.getSubjects()) {
            if (NS.hasRole(s, AriesComponent.NS.BENEFICIARY_TRAIT, subject
                    .getObservable(), process, resolutionContext)) {
                ret.add(s);
            }
        }
        return ret;
    }

    static public ShapeValue createJoiningShape(IShape shape, IShape point) {

        String srs = "EPSG:" + ((IGeometricShape) shape).getSRID();
        Geometry boundary = ((IGeometricShape) shape).getGeometry().getBoundary();
        Geometry pt = ((IGeometricShape) point).getGeometry().getCentroid();

        Coordinate[] cloud = new Coordinate[boundary.getCoordinates().length + 1];
        int i = 1;
        cloud[0] = pt.getCoordinate();
        for (Coordinate c : boundary.getCoordinates()) {
            cloud[i++] = c;
        }
        ConvexHull hull = new ConvexHull(cloud, boundary.getFactory());
        return new ShapeValue(hull.getConvexHull().buffer(0), Geospace.getCRSFromID(srs));
    }
}
