/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.components.weather;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.math3.analysis.UnivariateFunction;
import org.apache.commons.math3.analysis.interpolation.SplineInterpolator;
import org.apache.commons.math3.analysis.interpolation.UnivariateInterpolator;
import org.integratedmodelling.api.lang.IMetadataHolder;
import org.integratedmodelling.api.metadata.IMetadata;
import org.integratedmodelling.api.time.ITemporalExtent;
import org.integratedmodelling.collections.Pair;
import org.integratedmodelling.common.configuration.KLAB;
import org.integratedmodelling.common.metadata.Metadata;
import org.integratedmodelling.common.time.Time;
import org.integratedmodelling.common.utils.FixedReader;
import org.integratedmodelling.common.vocabulary.GeoNS;
import org.integratedmodelling.components.weather.api.IWeatherStation;
import org.integratedmodelling.engine.geospace.literals.ShapeValue;
import org.integratedmodelling.exceptions.ThinklabException;
import org.integratedmodelling.exceptions.ThinklabRuntimeException;
import org.integratedmodelling.exceptions.ThinklabValidationException;
import org.integratedmodelling.lang.IRemoteSerializable;
import org.joda.time.DateTime;
import org.mapdb.DB;
import org.mapdb.DBMaker;

import com.vividsolutions.jts.geom.Point;

/**
 * A weather station with all the data known for it. The closest station to any point on
 * the globe can be retrieved using the weather factory. Each Weather has one (and possibly
 * in the future more than one) station associated.
 * 
 * @author Ferd
 *
 */
// @Concept(Weather.NS.WEATHER_STATION)
public class WeatherStation implements IMetadataHolder, IRemoteSerializable,
        IWeatherStation {

    // MapDB-backed storage so we only do URL retrieval the first time.
    static DB                    db;
    static Map<String, double[]> dataMap;
    static Set<String>           stationSet;
    static Map<String, Integer>  nansMap;

    public class Data {

        String           variable;
        HashSet<Integer> generated = new HashSet<>();
        int              startYear = 0;
        int              endYear   = 0;

        /*
         * Data are one pair <year, daily data> per year, in increasing year
         * order. Nodata are NaNs. Array may have either 365 or 366 elements,
         * according to year.
         */
        HashMap<Integer, double[]> data = new HashMap<Integer, double[]>();

        public Data(String variable) {
            this.variable = variable;
        }

        public int getEndYear() {
            if (endYear == 0) {
                for (int i : data.keySet()) {
                    if (endYear < i) {
                        endYear = i;
                    }
                }
            }
            return endYear;
        }

        public int getStartYear() {
            if (startYear == 0) {
                startYear = 30000;
                for (int i : data.keySet()) {
                    if (startYear > i) {
                        startYear = i;
                    }
                }
            }
            return startYear;
        }

        public String dump() {
            String s = "";
            for (int y : data.keySet()) {
                s += (s.isEmpty() ? "" : ",") + y;
            }
            return variable + "(" + s + ")";
        }

        public void setNodata(int year) {
            requireYearData(variable, year);
        }
    }

    HashMap<String, Data>                   _variables    = new HashMap<String, Data>();
    Metadata                                _metadata     = new Metadata();
    WeatherGenerator                        _wg           = null;
    boolean                                 _wgFailure    = false;
    int                                     _maxRecordAge = 15;
    String                                  _id;
    double                                  _altitude;
    double                                  _longitude;
    double                                  _latitude;
    HashMap<String, Pair<Integer, Integer>> _provided     = new HashMap<String, Pair<Integer, Integer>>();

    /**
     * Generate the variables supported by the weather generator. Only useful if we have
     * the three variables we need for training.
     * 
     * TODO/FIXME: this is using a WG trained on the full dataset. We should only train it up to the year
     * of interest.
     * 
     * @param year
     * @throws ThinklabException
     */
    public void generateWeather(int year) throws ThinklabException {

        if (_wgFailure) {
            return;
        }

        if (_wg == null) {
            KLAB.info("generating weather for " + _id);
            _wg = new WeatherGenerator(this, 0, year);
        }
        List<double[]> vars = new ArrayList<double[]>();
        for (String variable : new String[] {
                Weather.PRECIPITATION_MM,
                Weather.MIN_TEMPERATURE_C,
                Weather.MAX_TEMPERATURE_C }) {
            vars.add(requireYearData(variable, year));
        }
        _wg.generateDaily(vars.get(1), vars.get(2), vars.get(0), true);
        _variables.get(Weather.PRECIPITATION_MM).generated.add(year);
        _variables.get(Weather.MIN_TEMPERATURE_C).generated.add(year);
        _variables.get(Weather.MAX_TEMPERATURE_C).generated.add(year);
    }

    private void generateDataFromMostRecentRecord(int year) throws ThinklabException {

        for (String variable : new String[] {
                Weather.PRECIPITATION_MM,
                Weather.MIN_TEMPERATURE_C,
                Weather.MAX_TEMPERATURE_C }) {
            double[] data = generateDataFromMostRecentRecord(variable, year, _maxRecordAge);
            if (data != null) {
                setYearData(variable, year, data);
            }
        }
    }

    /**
     * true if there are data stored up to _maxRecordAge less than the passed year for 
     * all the passed variables.
     * 
     * @param variables
     * @return
     */
    public boolean hasUsableDataFor(int year, String... variables) {

        for (String v : variables) {
            Pair<Integer, Integer> bnds = _provided.get(v);
            if (bnds == null || (bnds.getSecond() + _maxRecordAge) < year) {
                return false;
            }
        }
        return true;
    }

    /**
     * similar check to hasUsableDataFor, but looks at the percentage of no-data
     * instead of the existence of the record. Coded as a separate method because
     * this one needs the dataset to have been stored by storeData(), which is not
     * necessarily the general situation. If storeData() was not called, this will
     * return false even if enough data exist.
     * 
     * @param variables
     * @return
     */
    public boolean hasEnoughDataFor(ITemporalExtent time, int maxNodataPercentage, String... variables) {

        for (int year : Time.yearsBetween(time.getStart().getMillis(), time.getEnd().getMillis())) {

            for (String v : variables) {
                if (dataMap == null || !dataMap.containsKey(_id + ":" + v + "@" + year)) {
                    return false;
                }
                if (nansMap.get(_id + ":" + v + "@" + year) > maxNodataPercentage) {
                    return false;
                }
            }
        }
        return true;
    }

    private double[] generateDataFromMostRecentRecord(String var, int year, int maxYearsBehind)
            throws ThinklabException {
        double[] ret = null;
        int olderYear = 0;
        int maxAllowed = year - (maxYearsBehind < 0 ? _provided.get(var).getFirst() : maxYearsBehind);
        for (int i = year; i >= maxAllowed; i--) {
            if ((ret = getDataFromDB(var, i)) != null) {
                olderYear = i;
                KLAB.info(_id + " using " + var + " data from " + olderYear
                        + " to represent year "
                        + year);
                break;
            }
        }
        return Time.adjustLeapDays(ret, year, olderYear);
    }

    /**
     * Get the data series for the passed variable and year. If the station hasn't been seen before, store
     * all of its data in the DB. If null is returned, the station doesn't have the requested data.
     * 
     * @param variable
     * @param year
     * @return
     */
    double[] getDataFromDB(String variable, int year) throws ThinklabException {
        cacheData();
        String id = _id + ":" + variable + "@" + year;
        return dataMap.get(id);
    }

    void cacheData() throws ThinklabException {

        if (!stationSet.contains(_id)) {
            storeData();
        }
    }

    /**
     * 
     * @param variable
     * @param year
     * @param useWeatherGenerator
     * @param returnNoData
     * @return
     */
    double[] requireYearData(String variable, int year) {

        double[] ret = null;
        Data dr = _variables.get(variable);
        if (dr == null) {
            dr = new Data(variable);
            dr.variable = variable;
            // dr.startYear = dr.endYear = year;
            ret = new double[Time.daysInYear(year)];
            for (int i = 0; i < ret.length; i++)
                ret[i] = Double.NaN;
            dr.data.put(year, ret);
            _variables.put(variable, dr);
        } else {
            // if (dr.startYear > year)
            // dr.startYear = year;
            // if (dr.endYear < year)
            // dr.endYear = year;
            if (dr.data.containsKey(year)) {
                ret = dr.data.get(year);
            } else {
                ret = new double[Time.daysInYear(year)];
                for (int i = 0; i < ret.length; i++)
                    ret[i] = Double.NaN;
                dr.data.put(year, ret);
            }
        }

        return ret;
    }

    void setYearData(String variable, int year, double[] data) {

        Data dr = _variables.get(variable);
        if (dr == null) {
            dr = new Data(variable);
            dr.variable = variable;
            _variables.put(variable, dr);
        }
        dr.data.put(year, data);
    }

    /*
     * set to the distance from the original request when this is coming from a
     * nearest neighbor search.
     */
    double _distanceKm = -1;

    ShapeValue _location;

    // @Property(Weather.NS.HAS_START_YEAR)
    int _firstKnownYear;
    // @Property(Weather.NS.HAS_LAST_YEAR)
    int _lastKnownYear;

    // @Property(Weather.NS.HAS_DATA_URL)
    String _dataURL;

    // for the DB
    public WeatherStation() {
        checkStorage();
    }

    public WeatherStation(String baseURL, String id, double lat, double lon, double alt) {

        checkStorage();

        _id = id;
        _altitude = alt;
        _longitude = lon;
        _latitude = lat;

        /*
         * URL of data file for this station. Specific to structure of GHCN
         * archive.
         */
        _dataURL = baseURL + "/ghcnd_all/" + id + ".dly";

        try {
            _location = new ShapeValue("EPSG:4326 POINT (" + lon + " " + lat + ")");
        } catch (ThinklabException e) {
            throw new ThinklabRuntimeException(e);
        }
    }

    public void checkStorage() {
        if (db == null) {
            db = DBMaker
                    .newFileDB(new File(KLAB.CONFIG.getDataPath("weather") + File.separator
                            + "stationdata"))
                    .closeOnJvmShutdown()
                    .make();
            dataMap = db.getTreeMap("datamap");
            nansMap = db.getTreeMap("nansmap");
            stationSet = db.getTreeSet("stationset");
            Runtime.getRuntime().addShutdownHook(new Thread() {
                @Override
                public void run() {
                    // NPE?
                    // db.close();
                }
            });
        }
    }

    public void initializeFromStorage() {
        try {
            _location = new ShapeValue("EPSG:4326 POINT (" + _longitude + " " + _latitude + ")");
        } catch (ThinklabValidationException e) {
            throw new ThinklabRuntimeException(e);
        }
    }

    public Point getPoint() {
        return (Point) _location.getGeometry();
    }

    void storeData() throws ThinklabException {

        /*
         * set up storage for yearly data as appropriate - we read the
         * whole thing in once, then store the years we have. Key for the
         * hash is VAR|YEAR.
         */
        Map<String, double[]> data = new HashMap<>();

        for (FixedReader fr : FixedReader.parse(_dataURL, new int[] {
                0,
                11,
                15,
                17,
                21,
                26,
                27,
                28,
                29,
                34,
                35,
                36,
                37,
                261,
                266,
                267,
                268 })) {

            String ID = fr.nextString();
            int YEAR = fr.nextInt();
            int MONTH = fr.nextInt();
            String ELEMENT = fr.nextString();

            for (int day = 1; day <= 31; day++) {

                int VALUE = fr.nextInt(5);
                String MFLAG = fr.nextString(1);
                String QFLAG = fr.nextString(1);
                String SFLAG = fr.nextString(1);

                /*
                 * determine if this is a real date; skip if not
                 */
                int yearDay = -1;
                try {
                    DateTime dm = new DateTime(YEAR, MONTH, day, 0, 0);
                    yearDay = dm.getDayOfYear();
                } catch (Throwable e) {
                    // not a real date, continue
                }

                if (yearDay < 0) {
                    continue;
                }

                double value = Double.NaN;
                if (VALUE != -9999) {

                    /*
                     * process data to final representation according to
                     * type
                     */
                    if (ELEMENT.equals(Weather.PRECIPITATION_MM) || ELEMENT.equals(Weather.MAX_TEMPERATURE_C)
                            || ELEMENT.equals(Weather.MIN_TEMPERATURE_C)
                            || ELEMENT.equals(Weather.WIND_SPEED_M_SEC)
                    // TODO whatever else we want
                    ) {
                        value = VALUE / 10.0;
                    } else {
                        value = VALUE;
                    }

                }

                /*
                 * set month data in year dataseries
                 */
                double[] dd = data.get(ELEMENT + "@" + YEAR);
                if (dd == null) {
                    dd = new double[Time.daysInYear(YEAR)];
                    for (int i = 0; i < dd.length; i++) {
                        dd[i] = Double.NaN;
                    }
                    data.put(ELEMENT + "@" + YEAR, dd);
                }
                dd[yearDay - 1] = value;
            }
        }

        /*
         * Store everything in local cache
         * TODO store flags and an index of what's available + statistics etc.
         */
        for (String s : data.keySet()) {
            double[] dd = data.get(s);
            int nans = 0;
            for (int i = 0; i < dd.length; i++) {
                if (Double.isNaN(dd[i])) {
                    nans++;
                }
            }
            int pnans = (int) (((double) nans / (double) dd.length) * 100);
            dataMap.put(_id + ":" + s, data.get(s));
            nansMap.put(_id + ":" + s, pnans);
        }
        stationSet.add(_id);
        db.commit();
    }

    @Override
    public String toString() {

        /*
         * list variables
         */
        String vdesc = "[";
        for (String d : _provided.keySet()) {
            vdesc += (vdesc.length() == 1 ? "" : ",") + d;
        }
        vdesc += "]";

        return _id + " @" + _latitude + "," + _longitude + " " + vdesc
                + (_distanceKm < 0.0 ? "" : (" (" + _distanceKm + "km away)"));

    }

    // @Override
    // public void addStorageMetadata(IMetadata metadata, IMonitor monitor) throws ThinklabException {
    // ((Metadata) metadata).put(Weather.NS.HAS_LOCATION, _location);
    // }

    @Override
    public IMetadata getMetadata() {
        return _metadata;
    }

    /**
     * This point is the one that we wanted originally; compute the distance in
     * km to record accuracy from the original request.
     * 
     * @param location
     */
    public void setRequestedPoint(ShapeValue location) {
        _distanceKm = GeoNS.getDistance(location, _location);
    }

    /**
     * Load up every year of the passed variable from storage.
     * @param variable
     * @return
     * @throws ThinklabException 
     */
    Data fetch(String variable) throws ThinklabException {

        if (_variables.containsKey(variable)) {
            return _variables.get(variable);
        }
        if (!provides(variable)) {
            return null;
        }
        Data ret = new Data(variable);
        for (int year = _provided.get(variable).getFirst(); year <= _provided.get(variable)
                .getSecond(); year++) {
            double[] data = getDataFromDB(variable, year);
            if (data != null) {
                ret.data.put(year, data);
            }
        }
        _variables.put(variable, ret);

        return ret;
    }

    @Override
    public double[][] getYearlyData(boolean wholeYears, boolean interpolateMissing, boolean skipLeapDays, boolean consecutiveYears, String... variables)
            throws ThinklabException {

        Data[] records = new Data[variables.length];
        int i = 0;
        for (String v : variables) {
            Data dr = fetch(v);
            if (dr == null)
                return null;
            records[i++] = dr;
        }

        /*
         * Determine years where all variables have records.
         */
        int minYear = 3000, maxYear = 0;
        for (Data dr : records) {
            if (dr.getStartYear() < minYear) {
                minYear = dr.getStartYear();
            }
            if (dr.getEndYear() > maxYear) {
                maxYear = dr.getEndYear();
            }
        }

        List<Integer> years = new ArrayList<Integer>();
        int prev = -1;
        for (int y = minYear; y <= maxYear; y++) {

            if (consecutiveYears && prev != -1 && y != prev + 1)
                break;

            prev = y;
            int n = 0;
            for (Data dr : records) {

                if (!dr.data.containsKey(y))
                    continue;

                if (wholeYears) {
                    /*
                     * TODO skip non-whole years - this is taken care of by interpolate() using the 
                     * ratio criterion.
                     */
                }
                n++;
            }
            if (n == records.length) {
                years.add(y);
            }
        }

        /*
         * build dataset. First collect the data arrays, skipping any year
         * when one or more is null due to too many no-data or not matching
         * expectations.
         */
        ArrayList<Integer> yearsFinal = new ArrayList<Integer>();
        ArrayList<ArrayList<double[]>> dataFinal = new ArrayList<ArrayList<double[]>>();

        for (Integer year : years) {
            ArrayList<double[]> dholder = new ArrayList<double[]>();
            for (int v = 0; v < records.length; v++) {
                double[] data = records[v].data.get(year);
                if (interpolateMissing) {
                    data = interpolate(data);
                }
                if (data != null && skipLeapDays) {
                    data = skipLeapDay(data);
                }
                if (data != null) {
                    dholder.add(data);
                }
            }

            if (dholder.size() == variables.length) {
                yearsFinal.add(year);
                dataFinal.add(dholder);
            }
        }

        /*
         * now create the final 
         */
        double[][] ret = null;
        int len = 0;
        for (Integer year : yearsFinal) {
            len += skipLeapDays ? 365 : Time.daysInYear(year);
        }

        ret = new double[len][variables.length];
        int ofs = 0;
        for (int y = 0; y < yearsFinal.size(); y++) {

            int year = yearsFinal.get(y);
            int days = skipLeapDays ? 365 : Time.daysInYear(year);
            ArrayList<double[]> dholder = dataFinal.get(y);

            for (int v = 0; v < records.length; v++) {
                for (int day = 0; day < days; day++) {
                    ret[ofs + day][v] = dholder.get(v)[day];
                }
            }

            ofs += days;
        }

        return ret;
    }

    private double[] skipLeapDay(double[] data) {
        if (data.length == 365)
            return data;

        double[] ret = new double[365];
        int n = 0;
        for (int i = 0; i < 365; i++) {
            ret[n++] = i <= 60 ? data[i] : data[i + 1];
        }
        return ret;
    }

    private double[] interpolate(double[] data) {

        int nans = 0;
        double firstValid = Double.NaN, lastValid = Double.NaN;
        for (double d : data) {
            if (Double.isNaN(d)) {
                nans++;
            } else {
                if (Double.isNaN(firstValid))
                    firstValid = d;
                lastValid = d;
            }
        }

        if (nans == 0)
            return data;

        /*
         * if last point is a NaN, set it to the first valid point to enable
         * interpolation. Do same for first point.
         */

        if (Double.isNaN(data[0])) {
            nans--;
        }
        if (Double.isNaN(data[data.length - 1])) {
            nans--;
        }

        /*
         * accept only 10% missing data max
         * FIXME using 90% (no less) because Malawi is the first case study :(
         */
        double nanProportion = (double) nans / (double) data.length;
        if (nanProportion > 0.9)
            return null;

        double[] x = new double[data.length - nans];
        double[] y = new double[data.length - nans];

        int n = 0;
        for (int i = 0; i < data.length; i++) {
            double val = data[i];
            if (Double.isNaN(val)) {
                if (i == 0) {
                    val = lastValid;
                } else if (i == data.length - 1) {
                    val = firstValid;
                }
            }

            if (!Double.isNaN(val)) {
                int idx = n++;
                x[idx] = i;
                y[idx] = val;
            }
        }

        UnivariateInterpolator interpolator = new SplineInterpolator();
        UnivariateFunction function = interpolator.interpolate(x, y);

        double[] ret = new double[data.length];
        for (int i = 0; i < data.length; i++) {
            if (Double.isNaN(data[i])) {
                ret[i] = function.value(i);
            } else {
                ret[i] = data[i];
            }
        }

        return ret;
    }

    @Override
    public boolean equals(Object o) {
        return o instanceof WeatherStation && ((WeatherStation) o)._id.equals(_id);
    }

    @Override
    public int hashCode() {
        return _id.hashCode();
    }

    /**
     * TODO return true if records for the passed variable are available. May be a multiple one,
     * if so they're in OR.
     * 
     * @param var
     * @return
     */
    @Override
    public boolean provides(String var) {

        String[] vv = var.split(",");

        for (String v : vv) {
            if (_provided.containsKey(v)) {
                return true;
            }
        }
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public Object adapt() {
        // TODO Auto-generated method stub
        return null;
    }

    public int getLastKnownYear() {
        return _lastKnownYear;
    }

    public int getFirstKnownYear() {
        return _firstKnownYear;
    }

    @Override
    public String getId() {
        return _id;
    }

    /**
     * Get a data record that may or is guaranteed to contain the given var for the given year,
     * according to parameters.
     * 
     * @param var
     * @param year
     * @param useWeatherGenerator
     * @param returnNoData
     * @param useMostRecentData
     * @return never null if returnNoData is passed.
     * @throws ThinklabException
     */
    public Data getYearData(String var, int year, boolean useWeatherGenerator, boolean returnNoData, boolean useMostRecentData)
            throws ThinklabException {

        Data ret = fetch(var);

        if (ret.data.get(year) == null && useWeatherGenerator) {
            /*
             * try with the weather generator, and see if after that we have the variable.
             */
            try {
                generateWeather(year);
            } catch (Throwable e) {
                // can't generate weather: just log error and use nodata if allowed downstream.
                KLAB.error("weather generation for year " + year + " in " + _id
                        + " failed: data may be insufficient");
                _wg = null;
                _wgFailure = true;
            }
        }

        if (ret.data.get(year) == null && useMostRecentData) {
            generateDataFromMostRecentRecord(year);
        }

        if (ret.data.get(year) == null && returnNoData) {
            ret.setNodata(year);
        }

        return ret;
    }

    public void setMaxYearsBack(int maxYearsBack) {
        _maxRecordAge = maxYearsBack;
    }

    // for the DB serializer
    String[] getProvidedVarsDescriptors() {

        String names = "", start = "", end = "";

        for (String s : _provided.keySet()) {
            Pair<Integer, Integer> yse = _provided.get(s);
            names += (names.isEmpty() ? "" : ",") + s;
            start += (start.isEmpty() ? "" : ",") + yse.getFirst();
            end += (end.isEmpty() ? "" : ",") + yse.getSecond();
        }

        return new String[] { names, start, end };
    }

    // for the DB deserializer
    void parseVarsDescriptors(String names, String start, String end) {

        String[] n = names.split(",");
        String[] s = start.split(",");
        String[] e = end.split(",");

        _provided.clear();
        for (int i = 0; i < n.length; i++) {
            _provided.put(n[i], new Pair<Integer, Integer>(Integer.parseInt(s[i]), Integer.parseInt(e[i])));
        }
    }
}
