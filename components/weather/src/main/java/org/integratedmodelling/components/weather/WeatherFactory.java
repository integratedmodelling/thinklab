/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.components.weather;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.integratedmodelling.api.modelling.IScale;
import org.integratedmodelling.api.modelling.IState;
import org.integratedmodelling.api.monitoring.IMonitor;
import org.integratedmodelling.api.space.ISpatialExtent;
import org.integratedmodelling.collections.Pair;
import org.integratedmodelling.common.configuration.KLAB;
import org.integratedmodelling.common.utils.FixedReader;
import org.integratedmodelling.common.utils.URLUtils;
import org.integratedmodelling.engine.geospace.Geospace;
import org.integratedmodelling.exceptions.ThinklabException;
import org.integratedmodelling.exceptions.ThinklabIOException;
import org.integratedmodelling.exceptions.ThinklabRuntimeException;

import com.ibm.icu.util.Calendar;

public class WeatherFactory {

    public static String          baseURL = "http://150.241.222.1/ghcn";
    private static WeatherFactory _this;

    public static WeatherFactory get() {
        if (_this == null) {
            _this = new WeatherFactory();
        }
        return _this;
    }

    /**
     * Pass the base URL that will find the GHCN dataset right under it. Do not add a slash at
     * the end. If this is not used prior to calling checkDatabase(), the default (slow) will
     * be used.
     * 
     * @param url
     */
    public static void setBaseURL(String url) {
        baseURL = url;
    }

    /*
     * ensures the passed kbox contains the full index to the weather stations.
     * CAUTION: very long-running operation - about 3h on a powerful desktop. Better
     * done on a real server and used through REST.
     */
    public void checkDatabase() throws ThinklabException {

        /*
         * Format of GHCN station file (ghcnd-stations.txt)
         * ------------------------------
           Variable   Columns   Type
           ------------------------------
           ID            1-11   Character
           LATITUDE     13-20   Real
           LONGITUDE    22-30   Real
           ELEVATION    32-37   Real
           STATE        39-40   Character
           NAME         42-71   Character
           GSNFLAG      73-75   Character
           HCNFLAG      77-79   Character
           WMOID        81-85   Character
           ------------------------------
         */
        // IKbox kbox = Thinklab.get().requireKbox(Thinklab.DEFAULT_KBOX);
        ;
        File leftover = new File(KLAB.CONFIG.getDataPath() + File.separator + ".ws_lasts");
        long wscount = WeatherKbox.get().count();

        if (wscount > 100 && !leftover.exists()) {
            KLAB.warn("setup was not performed: no lock file and " + wscount
                    + " stations are present in the database");
            return;
        }

        File fInv = new File(KLAB.CONFIG.getDataPath("weather") + File.separator
                + "ghcnd-inventory.txt");
        File fSta = new File(KLAB.CONFIG.getDataPath("weather") + File.separator
                + "ghcnd-stations.txt");

        /**
         * Try to get these locally, to avoid keeping a URL connection open too long.
         */
        try {
            URLUtils.copy(new URL(baseURL + "/ghcnd-stations.txt"), fSta);
            URLUtils.copy(new URL(baseURL + "/ghcnd-inventory.txt"), fInv);
        } catch (MalformedURLException e1) {
            // do nothing, try directly.
        }

        List<FixedReader> stations = null;
        List<FixedReader> inventor = null;
        int startAt = 0;
        if (leftover.exists()) {
            try {
                startAt = Integer.parseInt(FileUtils.readFileToString(leftover).trim());
            } catch (NumberFormatException | IOException e) {
                throw new ThinklabIOException(e);
            }
        }

        if (fInv.exists() && fSta.exists()) {
            stations = FixedReader.parse(fSta, new int[] { 0, 12, 21, 31, 38, 41, 72, 76, 80 });
            inventor = FixedReader.parse(fInv, new int[] { 0, 12, 21, 31, 35, 40, 45 });
        } else {
            stations = FixedReader.parse(baseURL + "/ghcnd-stations.txt", new int[] {
                    0,
                    12,
                    21,
                    31,
                    38,
                    41,
                    72,
                    76,
                    80 });
            inventor = FixedReader.parse(baseURL + "/ghcnd-inventory.txt", new int[] {
                    0,
                    12,
                    21,
                    31,
                    35,
                    40,
                    45 });
        }

        // reader for the inventory file; the following depends on it being ordered exactly like
        // the stations file.
        Iterator<FixedReader> inventory = inventor.iterator();
        FixedReader invLine = inventory.next();

        int i = 0;
        for (FixedReader fr : stations) {

            String id = fr.nextString().trim();
            double lat = fr.nextDouble();
            double lon = fr.nextDouble();
            double alt = fr.nextDouble();

            /*
             * DO NOT REMOVE - side effects of nextString() are crucial.
             */
            String state = fr.nextString().trim();
            String name = fr.nextString().trim();

            /*
             * make a new object and store it
             */
            WeatherStation ws = new WeatherStation(baseURL, id, lat, lon, alt);

            try {
                /*
                 * read the available data series from the inventory.
                 */
                while (invLine.nextString().trim().equals(id)) {

                    invLine.nextDouble();
                    invLine.nextDouble();
                    String varId = invLine.nextString();
                    int firstYear = invLine.nextInt();
                    int lastYear = invLine.nextInt();

                    ws._provided.put(varId, new Pair<Integer, Integer>(firstYear, lastYear));

                    if (inventory.hasNext()) {
                        invLine = inventory.next();
                    }
                }
            } catch (Exception e) {
                throw new ThinklabIOException(e);
            }

            /*
             * we peeked already, so reset counter for next inventory line if we have more
             */
            if (inventory.hasNext()) {
                invLine.reset();
            }

            if (i >= startAt) {
                i++;
                try {
                    ws.storeData();
                } catch (Throwable e) {
                    KLAB.error("Data for station " + i + "th station: " + ws
                            + " are unreadable; skipping");
                    continue;
                }
                KLAB.info("Storing " + i + "th station: " + ws);
                WeatherKbox.get().store(ws);
                try {
                    FileUtils.writeStringToFile(leftover, "" + i);
                } catch (IOException e) {
                    throw new ThinklabIOException(e);
                }
            } else {
                KLAB.info("Skipping " + i++ + "th station: " + ws);
            }

            /*
             * I know. If I don't do this, Geotools will release the projection database at some point and make the 
             * spatial database go nuts. I have no idea how to stop that.
             */
            if (i % 5000 == 0) {
                Geospace.getCRSFromID("EPSG:32633");
            }
        }

        FileUtils.deleteQuietly(leftover);
    }

    /**
     * Return the weather at the passed scale. That should include time and space, definitely
     * space is required.
     *
     * @param scale
     * @return
     * @throws ThinklabException
     */
    public static Weather getWeather(IScale scale, boolean adjust, IMonitor monitor)
            throws ThinklabException {

        ISpatialExtent space = scale.getSpace();
        if (space == null) {
            KLAB.error("cannot get weather in a non-spatial context");
            return null;
        }

        List<WeatherStation> wss = WeatherKbox
                .get()
                .getStationsAround(space, 0.5, Weather.PRECIPITATION_MM, Weather.MAX_TEMPERATURE_C, Weather.MIN_TEMPERATURE_C);
        if (wss.size() == 0)
            return null;

        return new Weather(scale, null, wss, 15, adjust, monitor);
    }

    /**
     * Return the weather at the passed scale using a precipitation layer to adjust the precipitation in each
     * point. That should include time and space.
     *
     * @param scale
     * @return
     * @throws ThinklabException
     */
    public static Weather getWeather(IScale scale, Collection<IState> states, int maxYearsBack, boolean adjust, IMonitor monitor)
            throws ThinklabException {

        ISpatialExtent space = scale.getSpace();
        if (space == null)
            return null;

        List<WeatherStation> wss = WeatherKbox
                .get()
                .getStationsAround(space, 0.5, Weather.PRECIPITATION_MM, Weather.MAX_TEMPERATURE_C, Weather.MIN_TEMPERATURE_C);
        if (wss.size() == 0)
            return null;

        return new Weather(scale, states, wss, maxYearsBack, adjust, monitor);
    }

    /**
     * Compute day length in hours based on day of the year and latitude.
     * 
     * @param time
     * @param latitude
     * @return
     */
    public static double dayLength(Calendar time, double latitude) {

        final int[] DAYS = { 15, 45, 74, 105, 135, 166, 196, 227, 258, 288, 319, 349 };

        int month = time.get(java.util.Calendar.MONTH);

        double dayl = DAYS[month] - 80.;
        if (dayl < 0.0) {
            dayl = 285. + DAYS[month];
        }

        double decr = 23.45 * Math.sin(dayl / 365. * 6.2832) * 0.017453;
        double alat = latitude * 0.017453;
        double csh = (-0.02908 - Math.sin(decr) * Math.sin(alat)) / (Math.cos(decr) * Math.cos(alat));

        return 24.0 * (1.570796 - Math.atan(csh / Math.sqrt(1. - csh * csh))) / Math.PI;
    }

    public long countStations() {
        try {
            return WeatherKbox.get().count();
        } catch (ThinklabException e) {
            throw new ThinklabRuntimeException(e);
        }
    }

}
