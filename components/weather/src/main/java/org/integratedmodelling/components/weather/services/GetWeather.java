/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.components.weather.services;

import java.util.HashMap;
import java.util.Map;

import org.integratedmodelling.api.auth.IUser;
import org.integratedmodelling.api.modelling.IDirectObservation;
import org.integratedmodelling.api.modelling.IModel;
import org.integratedmodelling.api.modelling.IObservable;
import org.integratedmodelling.api.modelling.IObservation;
import org.integratedmodelling.api.modelling.IProcess;
import org.integratedmodelling.api.modelling.IScale;
import org.integratedmodelling.api.modelling.IState;
import org.integratedmodelling.api.modelling.resolution.IResolutionContext;
import org.integratedmodelling.api.modelling.runtime.IContextualizer;
import org.integratedmodelling.api.modelling.runtime.IProcessContextualizer;
import org.integratedmodelling.api.modelling.scheduling.ITransition;
import org.integratedmodelling.api.monitoring.IMonitor;
import org.integratedmodelling.api.project.IProject;
import org.integratedmodelling.api.services.types.IEngineService;
import org.integratedmodelling.api.services.types.ILocalService;
import org.integratedmodelling.api.time.ITemporalExtent;
import org.integratedmodelling.common.model.runtime.Transition;
import org.integratedmodelling.common.states.States;
import org.integratedmodelling.components.weather.Weather;
import org.integratedmodelling.components.weather.WeatherFactory;
import org.integratedmodelling.components.weather.WeatherKbox;
import org.integratedmodelling.engine.modelling.runtime.Scale;
import org.integratedmodelling.exceptions.ThinklabException;
import org.integratedmodelling.exceptions.ThinklabRuntimeException;
import org.integratedmodelling.exceptions.ThinklabValidationException;

/**
 * Create the weather in a scale; store it in the session for quicker future reference. Runs remotely
 * for ARIES/ISU users.
 * 
 * @author Ferd
 *
 */
//@Prototype(
//        id = "weather.get-weather",
//        returnTypes = { NS.PROCESS_CONTEXTUALIZER },
//        published = true,
//        args = {
//                "? yb|years-back",
//                Prototype.INT,
//                "? a|adjust-data",
//                Prototype.BOOLEAN,
//                "? nd|no-data-percentage",
//                Prototype.INT,
//                "? s|create-stations",
//                Prototype.BOOLEAN
//        },
//        argDescriptions = {
//                "maximum years in the past accepted to fill in gap years in stations (default 15)",
//                "adjust daily data around weather stations based on long-term records in input",
//                "maximum percentage of no-data values for a station in a year (0-100; default 100)",
//                "create weather stations as subjects at initialization (default false)"
//        })
public class GetWeather implements IProcessContextualizer, IEngineService, ILocalService,
        IContextualizer.Remote {

    boolean  dispose = false;
    Weather  weather = null;
    IMonitor monitor = null;

    private Map<String, IObservable> expectedOutputs;
    private IDirectObservation       context;
    private IScale                   scale;

    int     maxYearsBack      = 15;
    int     maxNodataAccepted = 100;
    boolean createStations    = false;
    boolean adjust            = true;

    // FIXME this shouldn't be necessary but if we run with the scale in the commons package (used when run
    // remotely) things
    // don't work right.
    boolean sanitize;

    @Override
    public boolean canDispose() {
        return dispose;
    }

    @Override
    public Map<String, IObservation> initialize(IProcess process, IDirectObservation context, IResolutionContext resolutionContext, Map<String, IObservable> expectedInputs, Map<String, IObservable> expectedOutputs, IMonitor monitor)
            throws ThinklabException {

        this.monitor = monitor;
        Map<String, IObservation> ret = new HashMap<>();

        /*
         * FIXME - make the "other" scale work right (particularly at transitions) and remove.
         */
        this.scale = process.getScale();
        if (!(process.getScale() instanceof Scale)) {
            sanitize = true;
            this.scale = Scale.sanitize(this.scale);
        }

        if (scale.getTime() == null) {
            throw new ThinklabValidationException("cannot compute weather without a temporal context");
        }

        /*
         * create the weather. It will automatically look into the states passed to see if there is a 
         * version of any outputs, to use for distributing point observations in the area of influence.
         */
        weather = WeatherFactory
                .getWeather(scale, context.getStates(), maxYearsBack, adjust, monitor);

        if (weather == null) {
            throw new ThinklabException("not enough weather stations in this area");
        }

        // if (scale.getTime() == null) {
        // int year = weather.getMostRecentAvailableYear();
        // monitor.warn("weather service called with no temporal context: will use observations from year "
        // + year);
        // dispose = true;
        // }

        weather.initializeObservables(expectedOutputs.values()
                .toArray(new IObservable[expectedOutputs.size()]));

        ITemporalExtent time = scale.getTime().getExtent(0);
        Map<IObservable, double[]> observations = weather.getObservations(time.getStart(), time.getEnd());

        for (String s : expectedOutputs.keySet()) {
            IState state = context.getState(expectedOutputs.get(s));
            state.getStorage().set(observations.get(expectedOutputs.get(s)));
            ret.put(s, state);
        }

        /*
         * save list of expected observables and context
         */
        this.expectedOutputs = expectedOutputs;
        this.context = context;

        return ret;
    }

    @Override
    public Map<String, IObservation> compute(ITransition transition, Map<String, IState> inputs)
            throws ThinklabException {

        /*
         * FIXME TEMPORARY - the original transition from the remote service makes a mess at getIndex()
         */
        if (this.sanitize) {
            transition = new Transition(this.scale, transition.getTimeIndex(), true);
        }

        // Env.logger.info("WEATHER EXECUTING " + (transition == null ? "DIOCANE" : ("" + transition
        // .getTimeIndex())));

        Map<String, IObservation> ret = new HashMap<>();
        dispose = transition.isLast();

        Map<IObservable, double[]> observations = weather
                .getObservations(transition.getTime().getStart(), transition
                        .getTime().getEnd());

        for (String s : expectedOutputs.keySet()) {
            IState state = context.getState(expectedOutputs.get(s));
            double[] data = observations.get(expectedOutputs.get(s));
            int i = 0;
            for (int n : scale.getIndex(transition)) {
                States.set(state, data[i++], n);
            }
            ret.put(s, state);
        }

        return ret;
    }

    @Override
    public void setContext(Map<String, Object> parameters, IModel model, IProject project) {
        if (parameters.containsKey("years-back")) {
            maxYearsBack = ((Number) parameters.get("years-back")).intValue();
        }
        if (parameters.containsKey("no-data-percentage")) {
            maxNodataAccepted = ((Number) parameters.get("no-data-percentage")).intValue();
        }
        if (parameters.containsKey("create-stations")) {
            createStations = (Boolean) parameters.get("create-stations");
        }
        if (parameters.containsKey("adjust-data")) {
            adjust = (Boolean) parameters.get("adjust-data");
        }

    }

    @Override
    public boolean useRemote(IUser user) {

        /*
         * default for this one is to run remotely if we're the ARIES or ISU groups and we don't have
         * the weather station data available locally.
         */
        try {
            return (user.getGroups().contains("ARIES") || user.getGroups().contains("ISU")) &&
                    WeatherKbox.get().count() < 60000;
        } catch (ThinklabException e) {
            throw new ThinklabRuntimeException(e);
        }
    }

    @Override
    public boolean canRunLocally() {
        try {
            return WeatherKbox.get().count() > 70000;
        } catch (ThinklabException e) {
            throw new ThinklabRuntimeException(e);
        }
    }
}
