/*******************************************************************************
 *  Copyright (C) 2007, 2014:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.components.weather.api;

import org.integratedmodelling.exceptions.ThinklabException;

public interface IWeatherStation {

    String getId();

    /**
     * Return all the available daily data for the given variables in one
     * two-dimensional array.
     * 
     * @param wholeYears
     *            only return whole year time series
     * @param interpolateMissing
     *            interpolate missing data (stupidly)
     * @param skipLeapDays
     *            skip Feb 29 in leap years so that all years are 365 days
     * @param variables
     * @return array with an array per day, with the values of all variables arranged in the order requested.
     * @throws ThinklabException
     */
    double[][] getYearlyData(boolean wholeYears, boolean interpolateMissing, boolean skipLeapDays, boolean consecutiveYears,
            String... variables)
            throws ThinklabException;

    /**
     * True if it provides the given variable.
     * 
     * @param var
     * @return
     */
    boolean provides(String var);

}
