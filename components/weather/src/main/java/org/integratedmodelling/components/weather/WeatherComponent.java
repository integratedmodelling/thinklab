/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.components.weather;

import org.integratedmodelling.Version;
import org.integratedmodelling.api.knowledge.IConcept;
import org.integratedmodelling.api.plugin.Component;
import org.integratedmodelling.api.plugin.Initialize;
import org.integratedmodelling.api.plugin.Setup;
import org.integratedmodelling.common.configuration.KLAB;
import org.integratedmodelling.exceptions.ThinklabException;

/**
 * The weather component provides weather stations access, interpolation, weather generation and
 * dynamic weather scenarios.
 * 
 * Accessible to ARIES, ISU and SES groups.
 * 
 * @author ferdinando.villa
 *
 */
@Component(
        id = "im.weather",
        version = Version.CURRENT,
        domain = "im",
        requiresProjects = { "im" },
        allowedGroups = { "ISU", "ARIES", "SES" })
public class WeatherComponent {

    @Initialize
    public boolean initialize() throws ThinklabException {

        /*
         *  check if we have all the required vocabulary. Assume that the presence of 
         *  one trait means we have its siblings - TODO may want to list them all.
         */
        if (!Weather.NS.synchronize()) {
            return false;
        }

        /*
         * FIXME
         * dumb check for presence of weather stations in db. Should
         * memorize an object at the end of setup and look for that
         * instead. Object should contain date of last record, number
         * of stations, bounding box, etc.
         */
        long count = WeatherFactory.get().countStations();
        KLAB.info("im.weather component initializing: " + count + " weather stations are available");
        return (count > 70000);
    }

    @Setup(asynchronous = true)
    public boolean setup() throws ThinklabException {
        WeatherFactory.get().checkDatabase();
        return true;
    }

    public static String getVariableFor(IConcept varc) {

        return null;
    }
}
