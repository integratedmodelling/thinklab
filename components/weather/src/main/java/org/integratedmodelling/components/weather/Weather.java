/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.components.weather;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.integratedmodelling.api.knowledge.IConcept;
import org.integratedmodelling.api.modelling.IObservable;
import org.integratedmodelling.api.modelling.IScale;
import org.integratedmodelling.api.modelling.IState;
import org.integratedmodelling.api.monitoring.IMonitor;
import org.integratedmodelling.api.space.ISpatialExtent;
import org.integratedmodelling.api.time.ITimeInstant;
import org.integratedmodelling.collections.Pair;
import org.integratedmodelling.common.configuration.KLAB;
import org.integratedmodelling.common.time.Time;
import org.integratedmodelling.common.vocabulary.GeoNS;
import org.integratedmodelling.components.weather.WeatherStation.Data;
import org.integratedmodelling.engine.KLABEngine;
import org.integratedmodelling.engine.geospace.Geospace;
import org.integratedmodelling.engine.geospace.extents.SpaceExtent;
import org.integratedmodelling.engine.geospace.literals.ShapeValue;
import org.integratedmodelling.engine.geospace.utils.SpatialDisplay;
import org.integratedmodelling.exceptions.ThinklabException;
import org.integratedmodelling.exceptions.ThinklabUnsupportedOperationException;
import org.integratedmodelling.exceptions.ThinklabValidationException;

import com.vividsolutions.jts.geom.Coordinate;
import com.vividsolutions.jts.geom.Geometry;
import com.vividsolutions.jts.geom.GeometryFactory;
import com.vividsolutions.jts.geom.Point;
import com.vividsolutions.jts.geom.Polygon;
import com.vividsolutions.jts.triangulate.VoronoiDiagramBuilder;

/**
 * The weather in a location. Also serves as a factory to obtain the weather given a lat/long pair, a frequency
 * of observation, a date range and a set of variables. Will use the GHCN dataset and if necessary
 * a weather generator to fulfill the request, picking the closest record to the requested
 * location. Weather can be modified with point events or bias and trends.
 * 
 * @author Ferd
 *
 */
public class Weather {

    /*
     * the concepts we need from the weather ontology and pointers to the required
     * im concepts.
     * 
     */
    public static class NS extends org.integratedmodelling.common.vocabulary.GeoNS {

        public static IConcept WEATHER_STATION = null;

        public static boolean synchronize() {
            if (!GeoNS.synchronize()) {
                return false;
            }
            try {
                WEATHER_STATION = checkClass(KLAB.MMANAGER
                        .getExportedKnowledge("IM.WEATHER_STATION"), IConcept.class);
            } catch (Exception e) {
                return false;
            }

            return true;
        }

    }

    // used in data production to define whether we should aggregate observations in any way.
    enum Reduction {
        RAW,
        MIN,
        MAX
    }

    enum Frequency {
        HOURLY,
        DAILY,
        WEEKLY,
        MONTHLY,
        YEARLY
    }

    // constants for aggregation or propagation
    double UNKNOWN = Double.NaN;
    double HOUR    = 1.0 / 24.0;
    double DAY     = 1.0;
    double WEEK    = 7;
    double MONTH   = 30;        // special treatment
    double YEAR    = 365;       // special treatment

    /*
     * structure to hold the observations we want, initialized at initializeObservables()
     */
    class Observation {
        public Observation(IObservable observable, String var, Reduction reduction, Frequency frequency,
                IState refstate) {
            this.observable = observable;
            this.var = var;
            this.reduction = reduction;
            this.frequency = frequency;
            this.refstate = refstate;
        }

        IObservable observable;
        String      var;
        Reduction   reduction;
        Frequency   frequency;
        IState      refstate;
    }

    /*
     * initialized before making any observations. Can be redefined at any time.
     */
    List<Observation> _observables = new ArrayList<>();

    /*
     * variable keys matched to GHCN codes. There's more.
     * TODO need cloudiness, insolation/shortwave radiation 
     */
    public final static String PRECIPITATION_MM  = "PRCP";
    public final static String SNOWFALL_MM       = "SNOW";
    public final static String SNOWDEPTH_MM      = "SNDP";
    public final static String MAX_TEMPERATURE_C = "TMAX";
    public final static String MIN_TEMPERATURE_C = "TMIN";

    // this one will (should) get the first available of the 4 choices.
    public final static String AVG_CLOUDINESS_PCT = "ACMC,ACMH,ACSC,ACSH";
    public final static String WIND_SPEED_M_SEC   = "AWND";

    /*
     * keys for frequency
     */
    public final static int HOURLY  = 0;
    public final static int DAILY   = 0;
    public final static int MONTHLY = 0;
    public final static int YEARLY  = 0;

    /*
     * minimum number of stations we'll use before taking those with lots of no-data. We will
     * use even one station in the end, this is just to filter out the bad ones if there is
     * enough information in others.
     */
    private static final int MIN_STATIONS = 5;

    /*
     * used to return observations within a session - this way we don't allocate large
     * arrays more than once.
     */
    Map<IObservable, double[]> observations = new HashMap<>();

    /*
     * the stations we use. If more than one, there must be a polygon of influence
     * in the _polygons array. These are generated from the spatial scale using 
     * Voronoi tessellation.
     */
    List<WeatherStation>                     _stations               = new ArrayList<WeatherStation>();
    List<Polygon>                            _polygons               = null;
    short[]                                  _index                  = null;
    Scenario                                 _scenario               = null;
    ISpatialExtent                           _space;
    ArrayList<Pair<Polygon, WeatherStation>> _pairs;
    List<Map<String, Double>>                _adjustmentFactors      = null;
    Map<String, IState>                      _historicalStatesForVar = new HashMap<>();
    IMonitor                                 _monitor;
    boolean                                  _adjust                 = false;

    /**
     * Generate weather according to the passed station. Will use a weather generator for
     * any year where the weather is unknown (this feature is limited to precipitation and
     * min/max temperatures for now).
     * 
     * @param station
     */
    public Weather(WeatherStation station) {
        _stations.add(station);
    }

    /**
     * This one is used to create spatially adjusted weather based on how many stations as
     * available and a map of average precipitation used to correct the weather proportionally
     * in each point. All stations must be in the range; if using a station outside of the
     * range, it will be adjusted to "pretend" it's in the center of the region.
     * 
     * @param scale
     * @param precipitationAverages
     * @param stations
     * @throws ThinklabException 
     */
    public Weather(IScale scale, Collection<IState> states, Collection<WeatherStation> stations,
            int maxYearsBack, boolean adjustData, IMonitor monitor)
                    throws ThinklabException {

        this._monitor = monitor;
        this._adjust = adjustData;

        if (stations.size() == 0) {
            throw new ThinklabException("cannot create weather with no weather stations available");
        }

        if (scale.getTime() == null) {
            throw new ThinklabValidationException("cannot create weather without a temporal context");
        }

        int unusable = 0;
        int year = Time.getYear(scale.getTime().getStart().getMillis());
        ArrayList<WeatherStation> rejected = new ArrayList<>();

        for (WeatherStation s : stations) {
            s.setMaxYearsBack(maxYearsBack);
            try {
                if (s.hasUsableDataFor(year, PRECIPITATION_MM, MAX_TEMPERATURE_C, MIN_TEMPERATURE_C)) {

                    s.cacheData();

                    if (!s.hasEnoughDataFor(scale
                            .getTime(), 25, PRECIPITATION_MM, MAX_TEMPERATURE_C, MIN_TEMPERATURE_C)) {
                        /*
                         * temporarily discard; will get them later if we have no other option.
                         */
                        rejected.add(s);
                    } else {
                        _stations.add(s);
                    }
                }
            } catch (Throwable e) {
                // just don't use it.
                unusable++;
            }
        }

        if (_stations.size() < MIN_STATIONS) {
            int needed = Math.min(MIN_STATIONS, rejected.size()) - _stations.size();
            if (needed > 0) {
                _monitor.warn("using " + needed
                        + " weather stations with high no-data percentage due to lack of better options");
            }
            for (int i = 0; i < needed; i++) {
                _stations.add(rejected.get(i));
            }
        }

        _monitor.info("using " + _stations.size() + " weather stations ("
                + rejected.size() + " had insufficient data, " + unusable + " had errors)", null);

        _space = scale.getSpace();
        if (_space.getGrid() == null) {
            throw new ThinklabUnsupportedOperationException("weather generator only supports gridded space contexts for now.");
        }

        SpatialDisplay debug = new SpatialDisplay((SpaceExtent) _space);

        if (_stations.size() > 0) {

            /*
             * generate station influence polygons and associate them to the stations in the order identified.
             */
            VoronoiDiagramBuilder db = new VoronoiDiagramBuilder();
            ArrayList<Coordinate> sites = new ArrayList<>();
            for (WeatherStation s : _stations) {
                sites.add(new Coordinate(s._longitude, s._latitude));
                debug.add(new ShapeValue(s.getPoint(), Geospace.get().getDefaultCRS()));
            }
            db.setSites(sites);
            Geometry diag = db.getDiagram(new GeometryFactory());

            /*
             * attribute stations to polygons
             */
            _pairs = new ArrayList<>();
            int pols = 0;
            for (pols = 0; pols < diag.getNumGeometries(); pols++) {
                Geometry g = diag.getGeometryN(pols);
                debug.add(new ShapeValue(g, Geospace.get().getDefaultCRS()), "original");
                for (int s = 0; s < _stations.size(); s++) {
                    if (g.intersects(_stations.get(s).getPoint()) && g instanceof Polygon) {
                        _pairs.add(new Pair<>((Polygon) g, _stations.get(s)));
                    }
                }
            }

            /*
             * build _index of which station is where. Drop those that fall out of
             * the context shape in the process. 
             */
            GeometryFactory pm = new GeometryFactory();
            _index = new short[(int) _space.getMultiplicity()];
            Set<Integer> usedStations = new HashSet<>();
            for (int i = 0; i < _space.getMultiplicity(); i++) {
                double[] xy = _space.getGrid().getCoordinates(i);
                Point point = pm.createPoint(new Coordinate(xy[0], xy[1]));
                for (int j = 0; j < _pairs.size(); j++) {
                    if (_pairs.get(j).getFirst().intersects(point)) {
                        _index[i] = (short) (j + 1);
                        usedStations.add(j);
                        break;
                    }
                }
            }

            /*
             * if we're adjusting, build the array of adjustment factors for each station
             */
            if (_adjust) {

                String varsForLog = "";

                _adjustmentFactors = new ArrayList<>();
                for (IState state : states) {
                    String var = getVariableForObservable(state.getObservable());
                    if (var != null) {

                        _historicalStatesForVar.put(var, state);

                        varsForLog += (varsForLog.isEmpty() ? "" : ", ")
                                + state.getObservable().getLocalName();

                        int i = 0;
                        for (Pair<Polygon, WeatherStation> wp : _pairs) {
                            int ofs = _space
                                    .getGrid()
                                    .getOffsetFromWorldCoordinates(wp.getSecond()._longitude, wp
                                            .getSecond()._latitude);
                            Map<String, Double> values = null;
                            if (_adjustmentFactors.size() <= i) {
                                values = new HashMap<>();
                                _adjustmentFactors.add(values);
                            } else {
                                values = _adjustmentFactors.get(i);
                            }

                            if (state.getValue(ofs) != null
                                    && !Double.isNaN(((Number) (state.getValue(ofs))).doubleValue())) {
                                values.put(var, ((Number) (state.getValue(ofs))).doubleValue());
                            }

                            i++;
                        }
                    }
                }

                if (_historicalStatesForVar.size() > 0) {
                    _monitor.info("adjusting values based on " + varsForLog, null);
                }
            }

            /**
             * Clean up
             * NO - sorry, this breaks the index. We may just want to set them to null so they can
             * be garbage collected, but for now let's just skip it.
             */
            // ArrayList<Integer> toRemove = new ArrayList<>();
            // for (int i = 0; i < _pairs.size(); i++) {
            // if (!usedStations.contains(i)) {
            // toRemove.add(i);
            // }
            // }
            // for (int i : toRemove) {
            // _pairs.remove(i);
            // }

            // if (toRemove.size() > 0) {
            // Env.logger.info("removed " + toRemove.size() + " unused stations");
            // }

            debug.show();
        }
    }

    public int getMostRecentAvailableYear() {

        int ret = -1;
        for (WeatherStation ws : _stations) {

            KLAB.info("last year for " + ws.getId() + " is " + ws.getLastKnownYear());

            if (ret > 0 && ret > ws.getLastKnownYear()) {
                ret = ws.getLastKnownYear();
            } else if (ret < ws.getLastKnownYear()) {
                ret = ws.getLastKnownYear();
            }
        }
        return ret;
    }

    public List<WeatherStation> getWeatherStations() {
        return _stations;
    }

    private String getVariableForObservable(IObservable o) {

        /*
         * TODO others
         */
        String var = null;
        if (o.getType().is(NS.PRECIPITATION_VOLUME)) {
            var = PRECIPITATION_MM;
        } else if (o.getType().is(NS.ATMOSPHERIC_TEMPERATURE)) {
            if (NS.hasTrait(o.getType(), NS.MAXIMUM_TRAIT)) {
                var = MAX_TEMPERATURE_C;
            } else if (NS.hasTrait(o.getType(), NS.MINIMUM_TRAIT)) {
                var = MIN_TEMPERATURE_C;
            }
        } else if (o.getType().is(NS.SNOWFALL_VOLUME)) {
            var = SNOWFALL_MM;
        }
        return var;
    }

    /**
     * Call before calling getObservations() to define the type of weather observations we
     * request.
     * 
     * @param observables
     * @throws ThinklabException
     */
    public void initializeObservables(IObservable... observables) throws ThinklabException {

        _observables.clear();

        for (IObservable o : observables) {

            String var = null;
            Reduction reduction = Reduction.RAW;
            Frequency frequency = null;
            IState refstate = null;

            /*
             * determine the variable from concept w/o traits.
             */
            if (o.getType().is(NS.PRECIPITATION_VOLUME)) {
                var = PRECIPITATION_MM;
                if (NS.hasTrait(o.getType(), NS.MAXIMUM_TRAIT)) {
                    reduction = Reduction.MAX;
                } else if (NS.hasTrait(o.getType(), NS.MINIMUM_TRAIT)) {
                    reduction = Reduction.MIN;
                }
            } else if (o.getType().is(NS.ATMOSPHERIC_TEMPERATURE)) {
                if (NS.hasTrait(o.getType(), NS.MAXIMUM_TRAIT)) {
                    var = MAX_TEMPERATURE_C;
                } else if (NS.hasTrait(o.getType(), NS.MINIMUM_TRAIT)) {
                    var = MIN_TEMPERATURE_C;
                }
            } else if (o.getType().is(NS.SNOWFALL_VOLUME)) {
                var = SNOWFALL_MM;
                if (NS.hasTrait(o.getType(), NS.MAXIMUM_TRAIT)) {
                    reduction = Reduction.MAX;
                } else if (NS.hasTrait(o.getType(), NS.MINIMUM_TRAIT)) {
                    reduction = Reduction.MIN;
                }
            }

            /**
             * parse desired frequency
             */
            if (NS.hasTrait(o.getType(), NS.HOURLY_TRAIT)) {
                frequency = Frequency.HOURLY;
            } else if (NS.hasTrait(o.getType(), NS.DAILY_TRAIT)) {
                frequency = Frequency.DAILY;
            } else if (NS.hasTrait(o.getType(), NS.WEEKLY_TRAIT)) {
                frequency = Frequency.WEEKLY;
            } else if (NS.hasTrait(o.getType(), NS.MONTHLY_TRAIT)) {
                frequency = Frequency.MONTHLY;
            } else if (NS.hasTrait(o.getType(), NS.YEARLY_TRAIT)) {
                frequency = Frequency.YEARLY;
            }

            if (frequency == null || var == null) {
                throw new ThinklabValidationException("cannot establish how to observe " + o.getType());
            }

            _observables.add(new Observation(o, var, reduction, frequency, refstate));

        }

    }

    /**
     * Return weather observations for the specified period and variables, using aggregation levels and
     * data reduction according to traits in the observables. It will use data from
     * the weather station if the period is known, interpolating if necessary, or train a weather
     * generator to produce data if the period is unknown.
     * 
     * @param start
     * @param end may be null (at initialization).
     * @param variables
     * @return
     * @throws ThinklabException 
     */
    public Map<IObservable, double[]> getObservations(ITimeInstant start, ITimeInstant end)
            throws ThinklabException {

        if (_observables.isEmpty()) {
            throw new ThinklabValidationException("weather: requested observations not specified.");
        }

        /*
         * the years in the requested period.
         */
        int[] years = Time.yearsBetween(start.getMillis(), end.getMillis());

        for (Observation obs : _observables) {

            double[] data = observations.get(obs.observable);
            if (data == null) {
                data = new double[_index.length];
                observations.put(obs.observable, data);
            }

            boolean isExtensive = obs.observable.getType()
                    .is(KLABEngine.c(NS.CORE_EXTENSIVE_PHYSICAL_PROPERTY));

            for (int i = 0; i < data.length; i++) {

                /*
                 * the station we're using in this point. Once polygon attribution is fixed, there should be no more 
                 * null stations.
                 */
                WeatherStation station = _index[i] == 0 ? null : _pairs.get(_index[i] - 1).getSecond();
                double value = Double.NaN;
                int ndata = 0;

                if (station != null && station.provides(obs.var)) {
                    /*
                     * loop over all the days we need and reduce as required
                     */
                    for (int year : years) {
                        Data obsdata = station.getYearData(obs.var, year, false, true, true);
                        for (int day : Time.daysBetween(start.getMillis(), end.getMillis(), year)) {

                            double d = obsdata.data.get(year)[day];
                            if (!Double.isNaN(d)) {

                                /*
                                 * if requested and possible, adjust number based on historical records.
                                 */
                                if (_adjust && _adjustmentFactors.get(_index[i] - 1) != null) {
                                    Double factor = _adjustmentFactors.get(_index[i] - 1).get(obs.var);
                                    if (factor != null && !Double.isNaN(factor)) {
                                        Object hv = _historicalStatesForVar.get(obs.var).getValue(i);
                                        if (hv instanceof Number) {
                                            double hvd = ((Number) hv).doubleValue();
                                            if (!Double.isNaN(hvd)) {
                                                d *= hvd / factor;
                                            }
                                        }
                                    }
                                }

                                if (Double.isNaN(value)) {
                                    value = d;
                                } else {
                                    value += d;
                                }
                                ndata++;
                            }
                        }
                    }
                }
                /*
                 * adjust based on variable character. Use this type of check as the default for
                 * non-physical variables (e.g. ranks) is to be intensive.
                 */
                if (!isExtensive && ndata > 1 && !Double.isNaN(value)) {
                    value /= ndata;
                }

                data[i] = value;
            }

            /*
             * TODO adjust if we have annual precipitation and temperature data.
             */
            observations.put(obs.observable, data);
        }

        return observations;
    }

    /**
     * Apply a scenario. Returns the same scenario so that a fluent idiom can be used to apply
     * multiple changes easily.
     * 
     * @param scenario
     * @return
     */
    public Scenario apply(Scenario scenario) {
        _scenario = scenario;
        return scenario;
    }

    /**
     * Return the raster state with the index of the station that is supplying
     * data for each point. Add 1 to the index so we can use 0 as nodata. Used
     * only for testing.
     * 
     * @return
     */
    public double[] getStationIndexes() {

        double[] ret = new double[(int) _space.getMultiplicity()];
        for (int i = 0; i < ret.length; i++) {
            ret[i] = _index == null ? Double.NaN : (_index[i] == 0 ? Double.NaN : (double) _index[i]);
        }
        return ret;
    }

}
