/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/

package org.integratedmodelling.thinklab.components.hydrology.weather;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import org.geotools.geometry.jts.ReferencedEnvelope;
import org.integratedmodelling.api.modelling.IObservable;
import org.integratedmodelling.api.modelling.IScale;
import org.integratedmodelling.api.modelling.IState;
import org.integratedmodelling.api.monitoring.IMonitor;
import org.integratedmodelling.api.space.ISpatialExtent;
import org.integratedmodelling.common.configuration.KLAB;
import org.integratedmodelling.common.engine.JSONdeserializer;
import org.integratedmodelling.common.space.IGeometricShape;
import org.integratedmodelling.common.vocabulary.GeoNS;
import org.integratedmodelling.common.vocabulary.NS;
import org.integratedmodelling.engine.geospace.literals.ShapeValue;
import org.integratedmodelling.exceptions.ThinklabException;
import org.integratedmodelling.exceptions.ThinklabRuntimeException;

import com.vividsolutions.jts.geom.Geometry;

public class WeatherFactory {

    private static final String DEFAULT_SERVICE_URL = "http://www.integratedmodelling.org/wmengine";

    private static String       serviceUrl;

    enum Endpoints {

        GET_DATA("/get-data"),
        GET_STATIONS("/get-stations"),
        GET_STATION("/get-station"),
        CAPABILITIES("/capabilities");

        Endpoints(String s) {
            this.endpoint = s;
        }

        String endpoint;

    }

    public final static String PRECIPITATION_MM   = "PRCP";
    public final static String SNOWFALL_MM        = "SNOW";
    public final static String SNOWDEPTH_MM       = "SNDP";
    public final static String MAX_TEMPERATURE_C  = "TMAX";
    public final static String MIN_TEMPERATURE_C  = "TMIN";

    // this one will (should) get the first available of the 4 choices.
    public final static String AVG_CLOUDINESS_PCT = "ACMC,ACMH,ACSC,ACSH";
    public final static String WIND_SPEED_M_SEC   = "AWND";

    private static String getServiceUrl() {

        if (serviceUrl == null) {
            if (KLAB.CONFIG.getProperties().containsKey("weather.service.url")) {
                serviceUrl = KLAB.CONFIG.getProperties().getProperty("weather.service.url");
            }
            if (serviceUrl == null) {
                serviceUrl = DEFAULT_SERVICE_URL;
            }
        }

        return serviceUrl;
    }

    private static String getServiceUrl(Endpoints endpoint) {
        String sUrl = getServiceUrl();
        return sUrl + endpoint.endpoint;
    }

    private static String getBboxSpecs(IScale scale) {

        ISpatialExtent space = scale.getSpace();
        if (space == null) {
            throw new ThinklabRuntimeException("cannot find the bounding box in a non-spatial extent");
        }
        Geometry shape = ((IGeometricShape) space.getShape()).getStandardizedGeometry();
        ReferencedEnvelope shv = new ShapeValue(shape).getEnvelope();

        return shv.getMinX() + "," + shv.getMinY() + "," + shv.getMaxX() + "," + shv.getMaxY();

    }

    public static Weather getWeather(IScale scale, Collection<IState> contextData, int maxYearsBack, boolean adjust, IMonitor monitor) {

        List<WeatherStation> stations = new ArrayList<>();

        try {
            Object rmap = JSONdeserializer
                    .getArray(getServiceUrl(Endpoints.GET_DATA), null, "bbox", getBboxSpecs(scale), "variables", PRECIPITATION_MM
                            + "," + MIN_TEMPERATURE_C + ","
                            + MAX_TEMPERATURE_C, "start", scale.getTime().getStart().getMillis(), "end", scale
                                    .getTime().getEnd().getMillis(), "step", scale.getTime().getStep().getMilliseconds());

            if (rmap instanceof Collection) {
                for (Object rm : ((Collection<?>) rmap)) {
                    if (rm instanceof Map) {
                        stations.add(new WeatherStation((Map<?, ?>) rm));
                    }
                }
            }

        } catch (ThinklabException e) {
            return null;
        }

        Collection<IState> yearlyData = new ArrayList<>();        
        for (IState state : contextData) {
            if (state.getObservable().is(GeoNS.ELEVATION) || NS.hasTrait(state.getObservable(), NS.YEARLY_TRAIT)) {
                yearlyData.add(state);
            }
        }
        
        return new Weather(scale, yearlyData, stations, monitor);
    }

    /**
     * Return all weather stations in a context (including those without usable data for it). If
     * the context has time, also get temporal data for the stations.
     * 
     * @param scale
     * @return
     */
    public static Collection<WeatherStation> getAllStations(IScale scale) {
        
        List<WeatherStation> ret = new ArrayList<>();
        try {
            
            /*
             * all stations with just a data summary
             */
            Object rmap = null;

            if (scale.getTime() == null) {
                rmap = JSONdeserializer
                        .getArray(getServiceUrl(Endpoints.GET_STATIONS), null, "bbox", getBboxSpecs(scale));
            } else {
                rmap = JSONdeserializer
                        .getArray(getServiceUrl(Endpoints.GET_DATA), null, "bbox", getBboxSpecs(scale), "variables", PRECIPITATION_MM
                                + "," + MIN_TEMPERATURE_C + "," + MAX_TEMPERATURE_C, "start", scale.getTime()
                                        .getStart().getMillis(), "end", scale.getTime().getEnd()
                                                .getMillis(), "step", scale.getTime().getStep().getMilliseconds());
            }

            /*
             * TODO build subjects with the correspondent states if there.
             */
            if (rmap instanceof List) {
                for (Object rm : ((Collection<?>) rmap)) {
                    if (rm instanceof Map) {
                        ret.add(new WeatherStation((Map<?, ?>) rm));
                    }
                }
            }
            
        } catch (ThinklabException e) {
            KLAB.error("error retrieving weather stations from service at " + serviceUrl);
            return ret;
        }

        return ret;
    }

    public static String getVariableForObservable(IObservable o) {

        String var = null;
        if (o.getType().is(GeoNS.PRECIPITATION_VOLUME)) {
            var = PRECIPITATION_MM;
        } else if (o.getType().is(GeoNS.ATMOSPHERIC_TEMPERATURE)) {
            if (NS.hasTrait(o.getType(), NS.MAXIMUM_TRAIT)) {
                var = MAX_TEMPERATURE_C;
            } else if (NS.hasTrait(o.getType(), NS.MINIMUM_TRAIT)) {
                var = MIN_TEMPERATURE_C;
            }
        } else if (o.getType().is(GeoNS.SNOWFALL_VOLUME)) {
            var = SNOWFALL_MM;
        }
        return var;
    }

}
