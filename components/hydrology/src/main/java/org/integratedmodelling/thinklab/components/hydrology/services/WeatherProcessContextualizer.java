/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.thinklab.components.hydrology.services;

import java.util.HashMap;
import java.util.Map;

import org.integratedmodelling.api.auth.IUser;
import org.integratedmodelling.api.modelling.IDirectObservation;
import org.integratedmodelling.api.modelling.IModel;
import org.integratedmodelling.api.modelling.IObservable;
import org.integratedmodelling.api.modelling.IObservation;
import org.integratedmodelling.api.modelling.IProcess;
import org.integratedmodelling.api.modelling.IScale;
import org.integratedmodelling.api.modelling.IState;
import org.integratedmodelling.api.modelling.resolution.IResolutionContext;
import org.integratedmodelling.api.modelling.runtime.IContextualizer;
import org.integratedmodelling.api.modelling.runtime.IProcessContextualizer;
import org.integratedmodelling.api.modelling.scheduling.ITransition;
import org.integratedmodelling.api.monitoring.IMonitor;
import org.integratedmodelling.api.project.IProject;
import org.integratedmodelling.api.services.annotations.Prototype;
import org.integratedmodelling.api.services.types.IEngineService;
import org.integratedmodelling.api.services.types.ILocalService;
import org.integratedmodelling.common.vocabulary.NS;
import org.integratedmodelling.engine.modelling.runtime.Scale;
import org.integratedmodelling.exceptions.ThinklabException;
import org.integratedmodelling.exceptions.ThinklabValidationException;
import org.integratedmodelling.thinklab.components.hydrology.weather.Weather;
import org.integratedmodelling.thinklab.components.hydrology.weather.WeatherFactory;

/**
 * Create the weather in a scale; store it in the session for quicker future reference. Runs remotely
 * for ARIES/ISU users.
 * 
 * @author Ferd
 *
 */
@Prototype(
        id = "weather.get-weather",
        returnTypes = { NS.PROCESS_CONTEXTUALIZER },
        published = true,
        args = {
                "? yb|years-back",
                Prototype.INT,
                "? a|adjust-data",
                Prototype.BOOLEAN,
                "? nd|no-data-percentage",
                Prototype.INT,
                "? s|create-stations",
                Prototype.BOOLEAN
        },
        argDescriptions = {
                "maximum years in the past accepted to fill in gap years in stations (default 15)",
                "adjust daily data around weather stations based on long-term records in input",
                "maximum percentage of no-data values for a station in a year (0-100; default 100)",
                "create weather stations as subjects at initialization (default false)"
        })
public class WeatherProcessContextualizer implements IProcessContextualizer, IEngineService, ILocalService,
        IContextualizer.Remote {

    boolean                          dispose           = false;
    IMonitor                         monitor           = null;

    private Map<String, IObservable> expectedOutputs;
    private IDirectObservation       context;
    private IScale                   scale;

    int                              maxYearsBack      = 15;
    int                              maxNodataAccepted = 100;
    boolean                          createStations    = false;
    boolean                          adjust            = true;
    String                           serviceUrl;

    Weather                          weather;

    @Override
    public boolean canDispose() {
        return dispose;
    }

    @Override
    public Map<String, IObservation> initialize(IProcess process, IDirectObservation context, IResolutionContext resolutionContext, Map<String, IObservable> expectedInputs, Map<String, IObservable> expectedOutputs, IMonitor monitor)
            throws ThinklabException {

        this.monitor = monitor;
        Map<String, IObservation> ret = new HashMap<>();

        /*
         * FIXME - make the "other" scale work right (particularly at transitions) and remove.
         */
        this.scale = process.getScale();
        if (!(process.getScale() instanceof Scale)) {
            this.scale = Scale.sanitize(this.scale);
        }

        if (scale.getTime() == null) {
            throw new ThinklabValidationException("cannot compute weather without a temporal context");
        }

        /*
         * create the weather. It will automatically look into the states passed to see if there is a 
         * version of any outputs, to use for distributing point observations in the area of influence.
         */
        this.weather = WeatherFactory
                .getWeather(scale, context.getStates(), maxYearsBack, adjust, monitor);

        if (this.weather == null) {
            throw new ThinklabException("not enough weather stations in this area");
        }

        for (String s : expectedOutputs.keySet()) {
            IState state = context.getState(expectedOutputs.get(s));
            weather.defineState(state, null);
            ret.put(s, state);
        }

        /*
         * save list of expected observables and context
         */
        this.expectedOutputs = expectedOutputs;
        this.context = context;

        return ret;
    }

    @Override
    public Map<String, IObservation> compute(ITransition transition, Map<String, IState> inputs)
            throws ThinklabException {

        Map<String, IObservation> ret = new HashMap<>();
        dispose = transition.isLast();

        for (String s : expectedOutputs.keySet()) {
            IState state = context.getState(expectedOutputs.get(s));
            weather.defineState(state, transition);
            ret.put(s, state);
        }

        return ret;
    }

    @Override
    public void setContext(Map<String, Object> parameters, IModel model, IProject project) {
        if (parameters.containsKey("years-back")) {
            maxYearsBack = ((Number) parameters.get("years-back")).intValue();
        }
        if (parameters.containsKey("no-data-percentage")) {
            maxNodataAccepted = ((Number) parameters.get("no-data-percentage")).intValue();
        }
        if (parameters.containsKey("create-stations")) {
            createStations = (Boolean) parameters.get("create-stations");
        }
        if (parameters.containsKey("adjust-data")) {
            adjust = (Boolean) parameters.get("adjust-data");
        }

    }

    @Override
    public boolean useRemote(IUser user) {
        return false;
    }

    @Override
    public boolean canRunLocally() {
        return true;
    }
}
