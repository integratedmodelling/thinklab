/*******************************************************************************
 *  Copyright (C) 2007, 2014:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.integratedmodelling.thinklab.components.hydrology.thornwaite;

import oms3.annotations.*;

import oms3.io.CSTable;
import oms3.io.DataIO;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Iterator;

/**
 *
 * @author Olaf David
 */
public class Climate {

    @In
    public File climateInput;
    @Out
    public double temp;
    @Out
    public double precip;
    @Out
    public boolean moreData;
    @Out
    public Calendar time = new GregorianCalendar();

    /** Row Input iterator*/
    Iterator<String[]> inp;
    /** data formatter */
    private SimpleDateFormat f;

    public void init() throws Exception {
        CSTable table = DataIO.table(climateInput, "Climate");
        f = new SimpleDateFormat(table.getColumnInfo(1).get("Format"));
        inp = table.rows().iterator();
    }

    @Execute
    public void execute() throws Exception {
        if (inp == null) {
            init();
        }
        if (inp.hasNext()) {
            String[] row = inp.next();
            time.setTime(f.parse(row[1]));
            temp = Double.parseDouble(row[2]);
            precip = Double.parseDouble(row[3]);
        }
        moreData = inp.hasNext();
    }

    @Finalize
    public void done() {
        //        DataIO.dispose(inp);
    }
}
