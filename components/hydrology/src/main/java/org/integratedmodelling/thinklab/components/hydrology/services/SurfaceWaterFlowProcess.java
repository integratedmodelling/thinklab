/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.thinklab.components.hydrology.services;

import java.util.Arrays;
import java.util.BitSet;
import java.util.HashMap;
import java.util.Map;

import org.integratedmodelling.api.knowledge.IConcept;
import org.integratedmodelling.api.modelling.IDirectObservation;
import org.integratedmodelling.api.modelling.IModel;
import org.integratedmodelling.api.modelling.IObservable;
import org.integratedmodelling.api.modelling.IObservation;
import org.integratedmodelling.api.modelling.IProcess;
import org.integratedmodelling.api.modelling.IScale;
import org.integratedmodelling.api.modelling.IState;
import org.integratedmodelling.api.modelling.resolution.IResolutionContext;
import org.integratedmodelling.api.modelling.runtime.IProcessContextualizer;
import org.integratedmodelling.api.modelling.scheduling.ITransition;
import org.integratedmodelling.api.monitoring.IMonitor;
import org.integratedmodelling.api.project.IProject;
import org.integratedmodelling.api.services.annotations.Prototype;
import org.integratedmodelling.api.space.IGrid;
import org.integratedmodelling.common.states.States;
import org.integratedmodelling.common.vocabulary.GeoNS;
import org.integratedmodelling.common.vocabulary.NS;
import org.integratedmodelling.exceptions.ThinklabException;
import org.integratedmodelling.exceptions.ThinklabUnsupportedOperationException;
import org.integratedmodelling.thinklab.components.hydrology.HydrologyComponent;
import org.integratedmodelling.thinklab.components.hydrology.algorithms.WatershedOperations;
import org.jgrasstools.gears.libs.modules.Direction;

@Prototype(
        id = "im.hydrology.surface-water-flow",
        componentId = "im.hydrology",
        returnTypes = { NS.PROCESS_CONTEXTUALIZER })
public class SurfaceWaterFlowProcess implements IProcessContextualizer {

    IProject                    project;
    boolean                     canDispose = false;
    private WatershedOperations ws;
    private IGrid               grid;
    private IState              precipitation;
    private IState              runoffState;
    private IScale              scale;
    private IState              slope;
    private byte[]              curveNumbers;
    private byte[]              flowDirections;
    private double[]            runoffCache;
    private String              runoffId;
    private BitSet              stream;

    /*
     * if we have these inputs at the scale we run on, we don't use curve numbers. We
     * don't put them in dependencies for now - just leave it to models upstream to
     * provide them. Putting them in as optional dependencies would not make a difference
     * and may pull in models that are lower in quality than the curve number method.
     */
    private IState infiltration = null;
    private IState evaporation  = null;
    private String locallyConsumedId;
    private IState locallyConsumedState;

    @Override
    public boolean canDispose() {
        return canDispose;
    }

    @Override
    public void setContext(Map<String, Object> parameters, IModel model, IProject project) {
        this.project = project;
    }

    @Override
    public Map<String, IObservation> initialize(IProcess process, IDirectObservation context, IResolutionContext resolutionContext, Map<String, IObservable> expectedInputs, Map<String, IObservable> expectedOutputs, IMonitor monitor)
            throws ThinklabException {

        canDispose = !context.getScale().isTemporallyDistributed();
        Map<String, IObservation> ret = new HashMap<>();

        this.ws = (WatershedOperations) context.getMetadata().get(WatershedOperations.METADATA_FIELD);

        /**
         * FIXME this shouldn't happen for a while, but it may when the watershed formation process dependency
         * isn't there or is resolved by non-Thinklab models. At this point all that would be needed is a 
         * dependency on elevation and a call to the WS operations, which we leave off for now, so no big deal to fix.
         */
        if (this.ws == null) {
            throw new ThinklabUnsupportedOperationException("watershed has not been initialized by recognized models: this is unsupported at the time being");
        }

        /*
         * if we get here we have a grid.
         */
        this.scale = context.getScale();
        this.grid = scale.getSpace().getGrid();

        /*
         * analyze expected outputs - for now only pick runoff, then later we'll add the
         * various others. TODO should check units etc. - for now assume we are using our
         * own model for this and the units will be consistent with assumptions.
         */
        IObservable runoffObservable = null;
        for (String iname : expectedOutputs.keySet()) {
            if (expectedOutputs.get(iname).is(GeoNS.RUNOFF_VOLUME)) {
                runoffId = iname;
                runoffObservable = expectedOutputs.get(iname);
            }
        }
        IObservable locallyConsumedObservable = null;
        for (String iname : expectedOutputs.keySet()) {
            if (expectedOutputs.get(iname).is(GeoNS.LOCALLY_EXCHANGED_VOLUME)) {
                locallyConsumedId = iname;
                locallyConsumedObservable = expectedOutputs.get(iname);
            }
        }

        /*
         * TODO/FIXME: the findStateWithout below should be substituted with the findStateWith that
         * uses the temporal trait corresponding to our output. Also, temporal traits should be 
         * attributed automatically based on scale.
         */
        IState hsg = States.findState(context, GeoNS.HYDROLOGIC_SOIL_GROUP);
        IState landcover = States.findState(context, GeoNS.GLOBCOVER_CLASS);
        IState flowdir = States.findState(context, GeoNS.FLOW_DIRECTION);
        IState strnet = States.findState(context, GeoNS.STREAM_PRESENCE);

        this.slope = States.findState(context, GeoNS.SLOPE);
        this.infiltration = States.findStateWithout(context, GeoNS.INFILTRATED_VOLUME, GeoNS.YEARLY_TRAIT);
        this.evaporation = States
                .findStateWithout(context, GeoNS.EVAPORATED_VOLUME, GeoNS.YEARLY_TRAIT);
        this.precipitation = States.findStateWithout(context, GeoNS.PRECIPITATION_VOLUME, GeoNS.YEARLY_TRAIT);
        this.runoffState = context.getState(runoffObservable);
        if ((this.infiltration == null || this.evaporation == null) && locallyConsumedObservable != null) {
            this.locallyConsumedState = context.getState(locallyConsumedObservable);
        }

        ret.put(runoffId, this.runoffState);

        this.curveNumbers = new byte[grid.getCellCount()];
        this.flowDirections = new byte[grid.getCellCount()];
        this.stream = new BitSet(grid.getCellCount());

        for (int n : scale.getIndex((IScale.Locator) null)) {
            int spaceOffset = scale.getExtentOffset(scale.getSpace(), n);

            Object lcov = States.get(landcover, n);
            Object hsgt = States.get(hsg, n);
            Object isst = States.get(strnet, n);
            Object fdir = States.get(flowdir, n);

            curveNumbers[spaceOffset] = (byte) HydrologyComponent
                    .getCurveNumber((IConcept) lcov, (IConcept) hsgt);
            flowDirections[spaceOffset] = (byte) (fdir instanceof Number ? ((Number) fdir).byteValue()
                    : 0xff);
            stream.set(spaceOffset, isst instanceof Boolean ? (Boolean) isst : false);
        }

        /*
         * initial runoff
         */
        moveWater(null);

        return ret;
    }

    @Override
    public Map<String, IObservation> compute(ITransition transition, Map<String, IState> inputs)
            throws ThinklabException {

        /*
         * TODO if land cover or hsg have changed and we're using them, 
         * recompute curve numbers
         */

        Map<String, IObservation> ret = new HashMap<>();
        canDispose = transition.isLast();
        moveWater(transition);
        ret.put(runoffId, runoffState);
        if (locallyConsumedState != null) {
            ret.put(locallyConsumedId, locallyConsumedState);
        }
        return ret;
    }

    public void moveWater(ITransition transition) throws ThinklabException {

        if (runoffCache == null) {
            runoffCache = new double[grid.getCellCount()];
        } else {
            Arrays.fill(runoffCache, 0);
        }

        /**
         * TODO this can be parallelized as long as writing to the states is atomic. Best to 
         * use a local atomic collection for storage and write synchronously to the states 
         * afterwards, if so.
         */
        for (IGrid.Cell cell : ws.getStreamHeads()) {

            double runoff = 0;
            while (cell != null) {

                /*
                 * this will only return a singleton index for the foreseeable future, but it allows extents
                 * beyond space/time with no modification.
                 */
                IScale.Index index = scale.getIndex(transition, grid.getLocator(cell.getX(), cell.getY()));
                int spaceOfs = grid.getOffset(cell.getX(), cell.getY());

                for (int ofs : index) {

                    boolean isRiver = stream.get(spaceOfs);
                    double precip = States.toDouble(precipitation.getValue(ofs));
                    double input = runoff + precip;

                    /*
                     * use infiltration/evaporation if something provides it upstream, or
                     * curve numbers if not.
                     */
                    if (infiltration != null && evaporation != null) {

                        if (isRiver) {
                            runoffCache[spaceOfs] += input;
                        } else {
                            double infil = States.toDouble(infiltration.getValue(ofs));
                            double evapo = States.toDouble(evaporation.getValue(ofs));
                            runoffCache[spaceOfs] += clamp(input - infil - evapo);
                        }

                    } else {

                        runoffCache[spaceOfs] += (isRiver ? input
                                : HydrologyComponent.getRunoffAmount(input, curveNumbers[spaceOfs]));
                        /*
                         * 
                         */
                        if (locallyConsumedState != null) {
                            locallyConsumedState.getStorage().set(ofs, input - runoff);
                        }
                    }
                }

                /*
                 * compute runoff to next cell downwards and move there unless the
                 * cell ends up draining outside the basin.
                 */
                Direction direction = Direction.forFlow(flowDirections[spaceOfs]);
                cell = cell.move(direction.col, direction.row);
            }
        }

        /*
         * set cumulated runoff values into state
         */
        for (int n : scale.getIndex(transition)) {
            int spaceOfs = scale.getExtentOffset(scale.getSpace(), n);
            States.set(runoffState, runoffCache[spaceOfs], n);
        }

    }

    private double clamp(double d) {
        return d < 0 ? 0 : d;
    }
}
