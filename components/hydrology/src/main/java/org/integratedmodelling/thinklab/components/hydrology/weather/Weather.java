package org.integratedmodelling.thinklab.components.hydrology.weather;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Nullable;

import org.integratedmodelling.api.modelling.IObservable;
import org.integratedmodelling.api.modelling.IScale;
import org.integratedmodelling.api.modelling.IState;
import org.integratedmodelling.api.modelling.scheduling.ITransition;
import org.integratedmodelling.api.monitoring.IMonitor;
import org.integratedmodelling.common.states.States;
import org.integratedmodelling.common.time.Time;
import org.integratedmodelling.common.vocabulary.GeoNS;
import org.integratedmodelling.engine.geospace.utils.ThiessenLocator;

public class Weather {

    IScale                          scale;
    List<WeatherStation>            stations;
    ThiessenLocator<WeatherStation> locator;
    Map<String, IState>             statesForVar = new HashMap<>();
    IState                          elevation;

    enum Reduction {
        RAW,
        MIN,
        MAX
    }

    enum Frequency {
        HOURLY,
        DAILY,
        WEEKLY,
        MONTHLY,
        YEARLY
    }

    /*
     * structure to hold the observations we want, initialized at initializeObservables()
     */
    class Observation {
        public Observation(IObservable observable, String var, Reduction reduction, Time.Frequency frequency,
                IState refstate) {
            this.observable = observable;
            this.var = var;
            this.reduction = reduction;
            this.frequency = frequency;
            this.refstate = refstate;
        }

        IObservable    observable;
        String         var;
        Reduction      reduction;
        Time.Frequency frequency;
        IState         refstate;
    }

    /*
     * initialized before making any observations. Can be redefined at any time.
     */
    List<Observation> observables = new ArrayList<>();

    public Weather(IScale scale, Collection<IState> states, List<WeatherStation> stations, IMonitor monitor) {

        this.scale = scale;
        this.stations = stations;
        this.locator = new ThiessenLocator<>(scale, stations);

        /*
         * record adjustment factors for available reference maps in each station
         */
        for (IState state : states) {

            if (state.getObservable().is(GeoNS.ELEVATION)) {
                this.elevation = state;
                continue;
            }

            String var = WeatherFactory.getVariableForObservable(state.getObservable());
            if (var != null) {

                statesForVar.put(var, state);

                for (WeatherStation ws : stations) {
                    int ofs = scale.getSpace()
                            .getGrid()
                            .getOffsetFromWorldCoordinates(ws.longitude, ws.latitude);
                    if (state.getValue(ofs) != null
                            && !Double.isNaN(((Number) (state.getValue(ofs))).doubleValue())) {
                        ws.refData.put(var, ((Number) (state.getValue(ofs))).doubleValue());
                    }
                }
            }
        }
    }

    /**
     * Workhorse of weather observations: take data from the representative station, adjust as necessary
     * based on lat/lon/altitude of point, and call setValue on the passed state for all values in a 
     * temporal slice.
     * 
     * @param observable
     * @param time
     */
    public void defineState(IState state, @Nullable ITransition transition) {

        String var = WeatherFactory.getVariableForObservable(state.getObservable());
        for (int n : scale.getIndex(transition)) {

            int spaceOffset = scale.getExtentOffset(scale.getSpace(), n);
            WeatherStation representativeStation = locator.get(spaceOffset);
            double data = representativeStation.data.get(var)[transition == null ? 0 : transition.getTimeIndex()];

            if (elevation != null && var.equals(WeatherFactory.MAX_TEMPERATURE_C)
                    || var.equals(WeatherFactory.MIN_TEMPERATURE_C)) {
                
                double refvalue = States.getDouble(elevation, spaceOffset);
                if (!Double.isNaN(refvalue)) {
                    data += ((representativeStation.elevation - refvalue) * 6.4 / 1000.0);
                }
            } else if (representativeStation.refData.containsKey(var)) {
                IState refstate = statesForVar.get(var);
                double refvalue = States.getDouble(refstate, spaceOffset);
                if (!Double.isNaN(refvalue)) {
                    data = (data * refvalue) / representativeStation.refData.get(var);
                }
            }

            States.set(state, data, n);
        }
    }

}
