/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.thinklab.components.hydrology.services;

import java.util.Map;

import org.integratedmodelling.api.knowledge.IConcept;
import org.integratedmodelling.api.modelling.IModel;
import org.integratedmodelling.api.modelling.IObservable;
import org.integratedmodelling.api.modelling.IObservation;
import org.integratedmodelling.api.modelling.IState;
import org.integratedmodelling.api.modelling.ISubject;
import org.integratedmodelling.api.modelling.resolution.IResolutionContext;
import org.integratedmodelling.api.modelling.runtime.ISubjectInstantiator;
import org.integratedmodelling.api.modelling.scheduling.ITransition;
import org.integratedmodelling.api.monitoring.IMonitor;
import org.integratedmodelling.api.project.IProject;
import org.integratedmodelling.api.services.annotations.Prototype;
import org.integratedmodelling.common.vocabulary.NS;
import org.integratedmodelling.exceptions.ThinklabException;

/**
 * TODO this becomes the instantiator for watershed in a region if 
 * data are not available. Will also compute all qualities, so the
 * following watershed resolver can skip resolution.
 * 
 * Should have a parameter to allow partial watershed generation, limit
 * partial generation to the main watershed found (default), or require
 * complete watershed in context.
 * 
 * @author ferdinando.villa
 *
 */
@Prototype(
        id = "hydrology.watersheds",
        componentId = "im.hydrology",
        returnTypes = { NS.SUBJECT_INSTANTIATOR })
public class WatershedSubjectInstantiator implements ISubjectInstantiator {

    @Override
    public boolean canDispose() {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public void setContext(Map<String, Object> parameters, IModel model, IProject project) {
        // TODO Auto-generated method stub

    }

    @Override
    public Map<String, IObservation> initialize(ISubject context, IResolutionContext resolutionContext, IConcept instanceType, Map<String, IObservable> expectedInputs, Map<String, IObservable> expectedOutputs, IMonitor monitor)
            throws ThinklabException {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public Map<String, IObservation> createSubjects(ITransition transition, Map<String, IState> inputs)
            throws ThinklabException {
        // TODO Auto-generated method stub
        return null;
    }

}
