/*******************************************************************************
 *  Copyright (C) 2007, 2014:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.integratedmodelling.thinklab.components.hydrology.prms;

import oms3.annotations.*;

import java.io.PrintWriter;
import java.io.File;
import java.io.IOException;
import java.util.Calendar;
import java.util.Date;
import oms3.io.DataIO;

public class Output {

    @Role(Role.PARAMETER + Role.OUTPUT)
    @In public File outFile;

    @In public Calendar date;
    @In public double basin_cfs;
    @In public double[] runoff;
    PrintWriter w;

    @Execute
    public void execute() {
        if (w == null) {
            try {
                w = new PrintWriter(outFile);
            } catch (IOException E) {
                throw new RuntimeException(E);
            }
            w.println("@T, efc");
            w.println(DataIO.KEY_CREATED_AT + ", " + new Date());
            String v = System.getProperty("oms3.digest");
            if (v != null) {
                w.println(DataIO.KEY_DIGEST +"," + v);
            }
            w.println(DataIO.DATE_FORMAT + ", yyyy-MM-dd");
            w.println("@H, date, basin_cfs, runoff");
            w.println("@H, date, basin_cfs, runoff");
            w.println(" Type  , Date,   Real,   Real");
            w.println(" Format, yyyy-MM-dd,0.00,0.00");
        }

        String s = String.format(",%1$tY-%1$tm-%1$td, %2$7.2f, %3$7.2f", date, basin_cfs, runoff[0]);
        w.println(s);
    }

    @Finalize
    public void done() {
        if (w!=null) {
            w.close();
        }
    }
}
