/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.thinklab.components.hydrology.algorithms;

import java.awt.image.RenderedImage;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.media.jai.iterator.RandomIter;
import javax.media.jai.iterator.RandomIterFactory;

import org.geotools.coverage.grid.GridCoverage2D;
import org.integratedmodelling.api.modelling.IDirectObservation;
import org.integratedmodelling.api.modelling.IExtent;
import org.integratedmodelling.api.modelling.IObservable;
import org.integratedmodelling.api.modelling.IScale;
import org.integratedmodelling.api.modelling.IState;
import org.integratedmodelling.api.modelling.ISubject;
import org.integratedmodelling.api.monitoring.IMonitor;
import org.integratedmodelling.api.monitoring.INotification;
import org.integratedmodelling.api.space.IGrid;
import org.integratedmodelling.api.space.ISpatialExtent;
import org.integratedmodelling.collections.Pair;
import org.integratedmodelling.common.configuration.KLAB;
import org.integratedmodelling.common.space.IGeometricShape;
import org.integratedmodelling.common.utils.StringUtils;
import org.integratedmodelling.common.vocabulary.GeoNS;
import org.integratedmodelling.common.vocabulary.NS;
import org.integratedmodelling.common.vocabulary.Observable;
import org.integratedmodelling.engine.geospace.Geospace;
import org.integratedmodelling.engine.geospace.extents.Grid;
import org.integratedmodelling.engine.geospace.extents.SpaceExtent;
import org.integratedmodelling.engine.geospace.gis.GISOperations;
import org.integratedmodelling.engine.geospace.interfaces.IGridMask;
import org.integratedmodelling.engine.geospace.literals.ShapeValue;
import org.integratedmodelling.engine.introspection.DataRecorder;
import org.integratedmodelling.engine.modelling.runtime.Scale;
import org.integratedmodelling.exceptions.ThinklabException;
import org.integratedmodelling.exceptions.ThinklabValidationException;
import org.jgrasstools.gears.libs.modules.FlowNode;
import org.jgrasstools.gears.libs.modules.Variables;
import org.jgrasstools.gears.utils.RegionMap;
import org.jgrasstools.gears.utils.coverage.CoverageUtilities;
import org.jgrasstools.hortonmachine.modules.demmanipulation.markoutlets.OmsMarkoutlets;
import org.jgrasstools.hortonmachine.modules.demmanipulation.pitfiller.OmsPitfiller;
import org.jgrasstools.hortonmachine.modules.demmanipulation.wateroutlet.OmsExtractBasin;
import org.jgrasstools.hortonmachine.modules.geomorphology.flow.OmsFlowDirections;
import org.jgrasstools.hortonmachine.modules.geomorphology.slope.OmsSlope;
import org.jgrasstools.hortonmachine.modules.geomorphology.tca.OmsTca;
import org.jgrasstools.hortonmachine.modules.network.extractnetwork.OmsExtractNetwork;

import com.vividsolutions.jts.geom.Point;

import es.unex.sextante.core.OutputFactory;
import es.unex.sextante.core.OutputObjectsSet;
import es.unex.sextante.core.ParametersSet;
import es.unex.sextante.dataObjects.IRasterLayer;
import es.unex.sextante.geotools.GTOutputFactory;
import es.unex.sextante.hydrology.fillSinks.FillSinksAlgorithm;
import es.unex.sextante.outputs.Output;

public class WatershedOperations extends GISOperations {

    class OutletData {
        double   x;
        double   y;
        double   importance;
        ISubject predefined;

        OutletData(double x, double y, double importance, IDirectObservation s) {
            this.x = x;
            this.y = y;
            this.importance = importance;
            this.predefined = (ISubject) s;
        }
    }

    Collection<IGrid.Cell>        streamHeads        = null;
    private GridCoverage2D        pitFilledElevation = null;
    private GridCoverage2D        flowDirections     = null;
    private GridCoverage2D        drainageArea       = null;
    private ArrayList<OutletData> outlets            = new ArrayList<>();
    private GridCoverage2D        streamNetwork;
    private IDirectObservation    context;
    ISubject                      predefinedOutlet   = null;

    public final static String  INFO_CLASS                 = "GIS_PROCESS";
    private static final double BASIN_IMPORTANCE_THRESHOLD = 0.05;
    private static final int    WATERSHEDS_TO_EXTRACT      = 1;
    public static final String  METADATA_FIELD             = "WATERSHED_OPS";
    private double              TCA_THRESHOLD              = 0.09;
    private Grid                grid;

    /**
     * Admits a map of parameters. For now only 
     * @param parameters
     */
    public WatershedOperations(Map<String, Object> parameters) {
        if (parameters != null) {
            for (String s : parameters.keySet()) {
                if (s.equals("tca-threshold")) {
                    TCA_THRESHOLD = ((Number) parameters.get(s)).doubleValue();
                } // TODO more parameters
            }
        }
    }

    public Collection<IGrid.Cell> getStreamHeads()
            throws ThinklabException {

        if (streamHeads != null) {
            return streamHeads;
        }

        List<IGrid.Cell> ret = new ArrayList<IGrid.Cell>();
        RegionMap regionMap = CoverageUtilities.getRegionParamsFromGridCoverage(flowDirections);
        int cols = regionMap.getCols();
        int rows = regionMap.getRows();

        RenderedImage flowRI = flowDirections.getRenderedImage();
        RandomIter flowIter = RandomIterFactory.create(flowRI, null);

        for (int r = 0; r < rows; r++) {
            for (int c = 0; c < cols; c++) {
                int y = grid.getYCells() - r - 1;
                if (grid.getActivationLayer().isActive(c, y)) {
                    FlowNode flowNode = new FlowNode(flowIter, cols, rows, c, r);
                    if (flowNode.isSource()) {
                        ret.add(grid.getCell(c, y));
                    }
                }
            }
        }
        flowIter.done();

        streamHeads = ret;

        return ret;
    }

    /**
     * Simply pass a non-corrected DEM to receive the same DEM with pits filled. Won't do
     * any checking on the input semantics. Of course it will complain vigorously if not 
     * a spatial raster state.
     * 
     * TODO switch to JGrassTools.
     * 
     * @param dem
     * @return
     * @throws ThinklabException 
     */
    public static IState fillSinks(IState demState, ISubject context, IMonitor monitor)
            throws ThinklabException {

        FillSinksAlgorithm alg = new FillSinksAlgorithm();
        ParametersSet parms = alg.getParameters();
        Output fdem = null;
        TaskMonitor mon = new TaskMonitor(monitor);

        IRasterLayer dem = stateToRaster(demState);

        try {
            parms.getParameter(FillSinksAlgorithm.DEM).setParameterValue(dem);
            OutputFactory outputFactory = new GTOutputFactory();

            alg.execute(mon, outputFactory);

            OutputObjectsSet outputs = alg.getOutputObjects();
            fdem = outputs.getOutput(FillSinksAlgorithm.RESULT);

        } catch (Exception e) {
            monitor.error(e);
            throw new ThinklabValidationException(e);
        }
        return rasterToState((IRasterLayer) fdem.getOutputObject(), demState.getObservable(), context);
    }

    /**
     * Perform the classic set of watershed computations and return all states produced. This one uses
     * JGrass.
     * 
     * @param dem
     * @param context
     * @param monitor
     * @param dataset if not null, used as a backup for states computed.
     * @return
     */
    public Map<IObservable, GridCoverage2D> watershedAnalysis(IState demState, IDirectObservation context, IMonitor monitor)
            throws ThinklabException {

        this.context = context;
        this.context.getMetadata().put(METADATA_FIELD, this);

        Map<IObservable, GridCoverage2D> ret = new HashMap<IObservable, GridCoverage2D>();
        this.grid = ((SpaceExtent) (demState.getSpace())).getGrid();
        GridCoverage2D dem = stateToCoverage(demState);

        /*
         * Pit filling.
         * 
         * TODO intercept pit estimate and provide feedback - this one is the 
         * longest running. Also there should be a provision for caching.
         */
        OmsPitfiller pitfiller = new OmsPitfiller();
        pitfiller.inElev = dem;
        pitfiller.pm = new TaskMonitor(monitor);
        pitfiller.doProcess = true;
        pitfiller.doReset = false;
        monitor.info("filling hydrological sinks...", INFO_CLASS);
        try {
            pitfiller.process();
        } catch (Exception e) {
            throw new ThinklabException(e);
        }

        this.pitFilledElevation = pitfiller.outPit;

        /*
         * TODO these must also contain the observer
         */
        ret.put(new Observable(GeoNS.PIT_FILLED_ELEVATION, KLAB
                .c(NS.MEASUREMENT), "pit-filled-elevation"), pitfiller.outPit);

        /*
         * flow directions
         */
        OmsFlowDirections omsflowdirections = new OmsFlowDirections();
        omsflowdirections.inPit = pitfiller.outPit;
        omsflowdirections.pm = pitfiller.pm;
        omsflowdirections.doProcess = true;
        omsflowdirections.doReset = false;
        monitor.info("computing flow directions...", INFO_CLASS);
        try {
            omsflowdirections.process();
        } catch (Exception e) {
            throw new ThinklabException(e);
        }

        this.flowDirections = omsflowdirections.outFlow;

        ret.put(new Observable(GeoNS.FLOW_DIRECTION, KLAB
                .c(NS.RANKING), "flow-direction"), omsflowdirections.outFlow);

        /*
         * omstca!
         */
        OmsTca omstca = new OmsTca();
        omstca.inFlow = omsflowdirections.outFlow;
        omstca.doProcess = true;
        omstca.doReset = false;
        omstca.pm = omsflowdirections.pm;
        monitor.info("computing drainage area per cell...", INFO_CLASS);
        try {
            omstca.process();
        } catch (Exception e) {
            throw new ThinklabException(e);
        }

        this.drainageArea = omstca.outTca;

        /*
         * TODO intercept warnings from watershed delineation that doesn't end at the boundaries
         * and push them up to the monitor.
         */
        ret.put(new Observable(GeoNS.TOTAL_CONTRIBUTING_AREA, KLAB
                .c(NS.RANKING), "total-contributing-area"), omstca.outTca);

        /*
         * outlets
         */
        OmsMarkoutlets out = new OmsMarkoutlets();
        out.inFlow = omsflowdirections.outFlow;
        out.doProcess = true;
        out.doReset = false;
        out.pm = omsflowdirections.pm;
        monitor.info("finding outlets...", INFO_CLASS);
        try {
            out.process();
        } catch (Exception e) {
            throw new ThinklabException(e);
        }

        /*
         * extract all outlets as single coordinates first, then make objects out of them.
         */
        RenderedImage image = out.outFlow.getRenderedImage();
        int npix = image.getHeight() * image.getWidth();
        RandomIter itera = RandomIterFactory.create(image, null);
        RenderedImage imtca = omstca.outTca.getRenderedImage();
        RandomIter itca = RandomIterFactory.create(imtca, null);

        List<IDirectObservation> outs = findExistingOutlets(context);

        if (outs.size() > 0) {

            for (IDirectObservation o : outs) {
                Point pt = ((IGeometricShape) o.getScale().getSpace().getShape()).getGeometry().getCentroid();
                int[] xy = grid.getGridCoordinatesAt(pt.getX(), pt.getY());
                double importance = (itca.getSampleDouble(xy[0], xy[1], 0) / (npix));
                this.outlets.add(new OutletData(pt.getX(), pt.getY(), importance, o));
            }

            monitor.info("using " + outs.size() + " predefined outlet"
                    + (outs.size() == 1 ? "" : "s"), INFO_CLASS);

        } else {

            for (int x = 0; x < image.getWidth(); x++) {
                for (int y = 0; y < image.getHeight(); y++) {
                    if (itera.getSampleDouble(x, y, 0) == FlowNode.OUTLET) {
                        double importance = (itca.getSampleDouble(x, y, 0) / (npix));
                        if (importance >= BASIN_IMPORTANCE_THRESHOLD) {
                            DataRecorder.debug("outlet at " + x + "," + y + " drains " + (importance * 100)
                                    + "% of context");
                            double[] xy = grid.getCoordinatesAt(x, y);
                            this.outlets.add(new OutletData(xy[0], xy[1], importance, null));
                        }
                    }
                }
            }

            monitor.info("found " + outlets.size() + " basins covering "
                    + StringUtils.percent(BASIN_IMPORTANCE_THRESHOLD) + " or more", INFO_CLASS);

        }

        IGridMask mask = null;
        if (outlets.size() > 0) {

            /*
             * sort outlets in descending drainage area order and take the one(s) with highest drainage.
             */
            Collections.sort(outlets, new Comparator<OutletData>() {

                @Override
                public int compare(OutletData arg0, OutletData arg1) {
                    return new Double(arg1.importance).compareTo(arg0.importance);
                }
            });

            if (outs.size() > 0) {
                predefinedOutlet = outlets.get(0).predefined;
            }

            /*
             * if there's no activation layer, make a fully active one.
             */
            mask = grid.requireActivationLayer(true);

            for (int i = 0; i < WATERSHEDS_TO_EXTRACT && i < outlets.size(); i++) {

                monitor.info("basin " + (i + 1) + " covers " + StringUtils.percent(outlets.get(0).importance)
                        + " of area", INFO_CLASS);

                /*
                 * extract the basin. TODO make this and the threshold configurable.
                 */
                OmsExtractBasin ebasin = new OmsExtractBasin();
                ebasin.inFlow = omsflowdirections.outFlow;
                ebasin.pEast = outlets.get(0).x;
                ebasin.pNorth = outlets.get(0).y;
                ebasin.doProcess = true;
                ebasin.doReset = false;
                ebasin.doVector = false; // TODO set to true when we generate the shape;
                try {
                    ebasin.process();
                } catch (Exception e) {
                    throw new ThinklabException(e);
                }

                /*
                 * load watershed mask into scale's activation layer, merging with
                 * any current mask already set.
                 */
                RenderedImage bimg = ebasin.outBasin.getRenderedImage();
                RandomIter bit = RandomIterFactory.create(bimg, null);
                for (int x = 0; x < image.getWidth(); x++) {
                    for (int y = 0; y < image.getHeight(); y++) {
                        if (bit.getSampleDouble(x, y, 0) == 1.0) {
                            if (mask.isActive(x, y)) {
                                mask.activate(x, y);
                            }
                        } else {
                            mask.deactivate(x, y);
                        }
                    }
                }
            }

        } else {
            monitor.warn("no watersheds found: check DEM");
        }

        /*
         * slope for the channel network 
         */
        OmsSlope slope = new OmsSlope();
        slope.inFlow = omsflowdirections.outFlow;
        slope.inPit = pitfiller.outPit;
        slope.doProcess = true;
        slope.doReset = false;
        slope.pm = pitfiller.pm;
        try {
            slope.process();
        } catch (Exception e) {
            throw new ThinklabException(e);
        }

        monitor.info("extracting channel network...", INFO_CLASS);

        /*
         * channel network
         * TODO try to observe a stream layer and use it to anchor the streams if available 
         * (it's another jgrass module - see HM presentation).
         */
        OmsExtractNetwork onet = new OmsExtractNetwork();
        onet.inFlow = omsflowdirections.outFlow;
        onet.inTca = omstca.outTca;
        // onet.inSlope = slope.outSlope;

        // TODO pass area threshold from parameters and in future, scale analysis. For now, take the cells
        // that drain
        // at least 9% of the basin area.
        onet.pThres = (mask == null ? (grid.getXCells() * (double) grid.getYCells())
                : (double) mask.totalActiveCells()) * TCA_THRESHOLD;
        onet.pMode = Variables.TCA;

        monitor.info("TCA threshold is " + onet.pThres, INFO_CLASS);

        onet.pm = pitfiller.pm;
        onet.doProcess = true;
        onet.doReset = false;
        try {
            onet.process();
        } catch (Exception e) {
            throw new ThinklabException(e);
        }

        monitor.info("watershed analysis completed.", INFO_CLASS);

        this.streamNetwork = onet.outNet;

        ret.put(new Observable(GeoNS.STREAM_PRESENCE, KLAB
                .c(NS.PRESENCE_OBSERVATION), "stream-presence"), onet.outNet);

        if (KLAB.CONFIG.getNotificationLevel() >= INotification.DEBUG) {
            for (GridCoverage2D g : ret.values()) {
                g.show();
            }
        }

        return ret;
    }

    private List<IDirectObservation> findExistingOutlets(IDirectObservation context2) {

        List<IDirectObservation> ret = new ArrayList<>();
        for (ISubject s : ((ISubject) context2).getSubjects()) {
            if (s.getObservable().is(GeoNS.STREAM_OUTLET)) {
                ret.add(s);
            }
        }
        return ret;
    }

    /**
     * After initialization, create the main outlet subject in the context and return it.
     * 
     * @return
     * @throws ThinklabException 
     */
    public ISubject getOutlet() throws ThinklabException {

        if (predefinedOutlet != null) {
            return predefinedOutlet;
        }

        if (this.context == null || outlets.size() < 1) {
            return null;
        }

        List<IExtent> exts = new ArrayList<>();
        for (IExtent e : context.getScale()) {
            if (e instanceof ISpatialExtent) {
                exts.add(new ShapeValue(outlets.get(0).x, outlets.get(0).x, Geospace.get()
                        .getDefaultCRS()).asExtent());
            } else {
                exts.add(e);
            }
        }
        IScale scale = new Scale(exts.toArray(new IExtent[exts.size()]));

        /*
         * TODO use a flows-into relationship so we can account for it automatically.
         */
        if (context instanceof ISubject) {
            return ((ISubject) context)
                    .newSubject(new Observable(GeoNS.STREAM_OUTLET), scale, context.getId()
                            + "-stream-outlet", KLAB
                                    .p(NS.PART_OF));
        }

        return null;

    }

    /**
     * After initialization, create all outlet subjects in the context and return them along
     * with the basin area each of them drains.
     * 
     * @return
     * @throws ThinklabException 
     */
    public List<Pair<ISubject, Double>> getOutlets() throws ThinklabException {

        List<Pair<ISubject, Double>> ret = new ArrayList<>();

        if (this.context == null || outlets.size() < 1) {
            return null;
        }

        for (OutletData outlet : outlets) {

            if (outlet.predefined != null) {
                ret.add(new Pair<ISubject, Double>(outlet.predefined, outlet.importance));
                continue;
            }

            List<IExtent> exts = new ArrayList<>();
            for (IExtent e : context.getScale()) {
                if (e instanceof ISpatialExtent) {
                    exts.add(new ShapeValue(outlet.x, outlet.y, Geospace
                            .get()
                            .getDefaultCRS()).asExtent());
                } else {
                    exts.add(e);
                }
            }
            IScale scale = new Scale(exts.toArray(new IExtent[exts.size()]));

            /*
             * TODO use a flows-into relationship so we can account for it automatically.
             */
            if (context instanceof ISubject) {

                ret.add(new Pair<>(((ISubject) context)
                        .newSubject(new Observable(GeoNS.STREAM_OUTLET), scale, context.getId()
                                + "-stream-outlet", KLAB
                                        .p(NS.PART_OF)), outlet.importance));
            }
        }

        return ret;
    }
}
