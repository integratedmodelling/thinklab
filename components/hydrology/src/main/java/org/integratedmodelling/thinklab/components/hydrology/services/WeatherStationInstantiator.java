/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.thinklab.components.hydrology.services;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.integratedmodelling.api.knowledge.IConcept;
import org.integratedmodelling.api.modelling.IExtent;
import org.integratedmodelling.api.modelling.IModel;
import org.integratedmodelling.api.modelling.IObservable;
import org.integratedmodelling.api.modelling.IObservation;
import org.integratedmodelling.api.modelling.IScale;
import org.integratedmodelling.api.modelling.IState;
import org.integratedmodelling.api.modelling.ISubject;
import org.integratedmodelling.api.modelling.resolution.IResolutionContext;
import org.integratedmodelling.api.modelling.runtime.ISubjectInstantiator;
import org.integratedmodelling.api.modelling.scheduling.ITransition;
import org.integratedmodelling.api.monitoring.IMonitor;
import org.integratedmodelling.api.project.IProject;
import org.integratedmodelling.api.services.annotations.Prototype;
import org.integratedmodelling.api.services.types.IEngineService;
import org.integratedmodelling.api.services.types.ILocalService;
import org.integratedmodelling.api.space.ISpatialExtent;
import org.integratedmodelling.common.configuration.KLAB;
import org.integratedmodelling.common.vocabulary.GeoNS;
import org.integratedmodelling.common.vocabulary.NS;
import org.integratedmodelling.common.vocabulary.Observable;
import org.integratedmodelling.engine.geospace.Geospace;
import org.integratedmodelling.engine.geospace.literals.ShapeValue;
import org.integratedmodelling.engine.modelling.runtime.Scale;
import org.integratedmodelling.exceptions.ThinklabException;
import org.integratedmodelling.thinklab.components.hydrology.weather.WeatherFactory;
import org.integratedmodelling.thinklab.components.hydrology.weather.WeatherStation;

@Prototype(
        id = "weather.get-stations",
        published = true,
        args = { "# varlist", Prototype.LIST },
        returnTypes = { NS.SUBJECT_INSTANTIATOR },
        argDescriptions = { "variables requested" })
public class WeatherStationInstantiator implements ISubjectInstantiator, IEngineService, ILocalService {

    @Override
    public boolean canDispose() {
        // this only needs to run at initialization
        return true;
    }

    @Override
    public void setContext(Map<String, Object> parameters, IModel model, IProject project) {
    }

    @Override
    public Map<String, IObservation> initialize(ISubject context, IResolutionContext resolutionContext, IConcept instanceType, Map<String, IObservable> expectedInputs, Map<String, IObservable> expectedOutputs, IMonitor monitor)
            throws ThinklabException {

        HashMap<String, IObservation> ret = new HashMap<>();

        for (WeatherStation ws : WeatherFactory.getAllStations(context.getScale())) {
            IScale wscale = getScale(ws, context);
            ret.put(ws.id, context.newSubject(new Observable(GeoNS.WEATHER_STATION), wscale, ws
                    .id, KLAB.p(NS.PART_OF)));
        }

        return ret;
    }

    private IScale getScale(WeatherStation ws, ISubject context) throws ThinklabException {

        List<IExtent> exts = new ArrayList<>();
        for (IExtent e : context.getScale()) {
            if (e instanceof ISpatialExtent) {
                exts.add(new ShapeValue(ws.location.getGeometry(), Geospace.get().getDefaultCRS()).asExtent());
            } else {
                exts.add(e);
            }
        }
        return new Scale(exts.toArray(new IExtent[exts.size()]));
    }

    @Override
    public Map<String, IObservation> createSubjects(ITransition transition, Map<String, IState> inputs)
            throws ThinklabException {
        return null;
    }

}
