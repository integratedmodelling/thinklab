/*******************************************************************************
 *  Copyright (C) 2007, 2014:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.integratedmodelling.thinklab.components.hydrology.thornwaite;

import oms3.annotations.*;

public class SoilMoisture {
    
    private double prestor = 150.0;
    
    @In  public double soilMoistStorCap;
    @In  public double potET;
    @In  public double precip;
    @In  public double temp;
    
    @Out public double surfaceRunoff;
    @Out public double pmpe;
    @Out public double actET;
    @Out public double dff;
    @Out public double soilMoistStor;
    
    @Initialize
    public void init() {
        prestor = 150.0;
    }
    
    @Execute
    public void execute() {        
        pmpe = precip - potET;
        surfaceRunoff = 0.0;
        soilMoistStor = 0.0;
        actET = 0.0;
        
        if (temp < 0.0 && pmpe > 0.0) {
            surfaceRunoff = 0.0;
            soilMoistStor = prestor;
            actET = 0.0;
        } else if (pmpe > 0.0 || pmpe == 0.0) {
            actET = potET;
            //  SOIL MOISTURE RECHARGE
            if (prestor < soilMoistStorCap) 
                soilMoistStor = prestor + pmpe;
            
            // SOIL MOISTURE STORAGE AT CAPACITY
            if (prestor == soilMoistStorCap)
                soilMoistStor = soilMoistStorCap;
            if (soilMoistStor > soilMoistStorCap)
                soilMoistStor = soilMoistStorCap;
            // CALCULATE SURPLUS
            surfaceRunoff = (prestor + pmpe) - soilMoistStorCap;
            if (surfaceRunoff < 0.0)
                surfaceRunoff = 0.0;
            //  CALCULATE MONTHLY CHANGE IN SOIL MOISTURE
            prestor = soilMoistStor;
        } else {
            soilMoistStor = prestor - Math.abs(pmpe * (prestor / soilMoistStorCap));
            if (soilMoistStor < 0.0)
                soilMoistStor = 0.0;
            double delstor = soilMoistStor - prestor;
            prestor =soilMoistStor;
            actET = precip + (delstor * (-1.0));
            surfaceRunoff = 0.0;
        }
        dff = potET - actET;
    }
}