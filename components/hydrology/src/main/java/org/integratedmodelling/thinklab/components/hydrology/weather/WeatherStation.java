/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.thinklab.components.hydrology.weather;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.integratedmodelling.common.space.IGeometricShape;
import org.integratedmodelling.engine.geospace.Geospace;
import org.integratedmodelling.engine.geospace.literals.ShapeValue;

import com.vividsolutions.jts.geom.Geometry;

public class WeatherStation implements IGeometricShape {

    public double     longitude;
    public double     latitude;
    public double     elevation;
    public String     id;
    public int        lastKnownYear;
    public ShapeValue location;

    // var -> local yearly data, set externally from data to use for matching to other locations
    public Map<String, Double> refData = new HashMap<>();

    /**
     * If we have initialized the station in a temporal context, this
     * will contain the data available in it, either for weather computation
     * or for creating the state of a weather station subject.
     */
    public Map<String, double[]> data = new HashMap<>();

    public WeatherStation(Map<?, ?> rm) {
        for (Object o : rm.keySet()) {
            switch (o.toString()) {
            case "lon":
                longitude = ((Number) rm.get(o)).doubleValue();
                break;
            case "lat":
                latitude = ((Number) rm.get(o)).doubleValue();
                break;
            case "id":
                id = rm.get(o).toString();
                break;
            case "elevation":
                elevation = ((Number) rm.get(o)).doubleValue();
                break;
            default:
                if (rm.get(o) instanceof List) {
                    data.put(o.toString(), getDoubleData((List<?>) rm.get(o)));
                }
            }
        }

        this.location = new ShapeValue(longitude, latitude, Geospace.get().getDefaultCRS());

    }

    private double[] getDoubleData(List<?> data) {
        double[] ret = new double[data.size()];
        for (int i = 0; i < data.size(); i++) {
            ret[i] = data.get(i) instanceof Number ? ((Number) data.get(i)).doubleValue() : Double.NaN;
        }
        return ret;
    }

    @Override
    public Type getGeometryType() {
        return location.getGeometryType();
    }

    @Override
    public double getArea() {
        return location.getArea();
    }

    @Override
    public String asText() {
        return location.asText();
    }

    @Override
    public Geometry getStandardizedGeometry() {
        return location.getGeometry();
    }

    @Override
    public Geometry getGeometry() {
        return location.getGeometry();
    }

    @Override
    public int getSRID() {
        return location.getSRID();
    }

    @Override
    public boolean isEmpty() {
        return location.isEmpty();
    }

}
