/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.thinklab.components.hydrology;

import java.util.HashMap;
import java.util.Map;

import org.integratedmodelling.Version;
import org.integratedmodelling.api.knowledge.IConcept;
import org.integratedmodelling.api.plugin.Component;
import org.integratedmodelling.api.plugin.Initialize;
import org.integratedmodelling.common.vocabulary.GeoNS;

@Component(
        id = "im.hydrology",
        version = Version.CURRENT,
        domain = "im",
        requiresProjects = { "im" },
        allowedGroups = { "ISU", "ARIES", "SES" })
public class HydrologyComponent {

    /*
     * holds mappings between a concept name (just local ID) and the corresponding
     * index in the CN table below.
     */
    private static Map<String, Integer> globCoverIndex   = new HashMap<>();
    private static Map<String, Integer> hsgIndex         = new HashMap<>();

    /*
     * index with hsgIndex, globCoverIndex after looking up both from the 
     * im.landcover.lccs and im.hydrology concept IDs in the maps above.
     */
    private static int[][]              curveNumberTable = {
            { 73, 71, 64, 63, 51, 60, 48, 44, 55, 55, 100, 100, 100, 75 },
            { 82, 80, 74, 73, 68, 76, 62, 65, 66, 66, 100, 100, 100, 80 },
            { 88, 86, 81, 82, 78, 81, 73, 77, 74, 74, 100, 100, 100, 85 },
            { 90, 86, 84, 87, 82, 89, 78, 82, 79, 79, 700, 100, 100, 90 } };

    @Initialize
    public boolean initialize() {
        try {

            GeoNS.synchronize();

            /*
             * initialize lookup tables for CN calculation
             */

            hsgIndex.put("SoilGroupA", 0);
            hsgIndex.put("SoilGroupB", 1);
            hsgIndex.put("SoilGroupC", 2);
            hsgIndex.put("SoilGroupD", 3);

            /*
             * best guess to match Table 1 in Mutua et al.
             * S. Willcock, P.C.
             */
            globCoverIndex.put("IrrigatedOrFloodedCropland", 1);
            globCoverIndex.put("RainfedCropland", 2);
            globCoverIndex.put("MosaicCropland", 3);
            globCoverIndex.put("MosaicVegetation", 7);
            globCoverIndex.put("ClosedEvergreenOrSemiDeciduousForest", 8);
            globCoverIndex.put("ClosedBroadleavedDeciduousForest", 8);
            globCoverIndex.put("OpenBroadleavedDeciduousForest", 8);
            globCoverIndex.put("ClosedNeedleleavedEvergreenForest", 8);
            globCoverIndex.put("OpenNeedleleavedDeciduousOrEvergreenForest", 9);
            globCoverIndex.put("MixedBroadleavedAndNeedleleavedForest", 9);
            globCoverIndex.put("MosaicForestShrublandWithGrassland", 7);
            globCoverIndex.put("MosaicGrasslandWithForestShrubland", 7);
            globCoverIndex.put("Shrubland", 6);
            globCoverIndex.put("Grassland", 5);
            globCoverIndex.put("SparseVegetation", 13);
            globCoverIndex.put("ClosedFloodedForestFreshwater", 12);
            globCoverIndex.put("ClosedFloodedForestSalineWater", 12);
            globCoverIndex.put("ClosedFloodedVegetation", 11);
            globCoverIndex.put("ArtificialSurfaces", 0);
            globCoverIndex.put("BareAreas", 13);
            globCoverIndex.put("WaterBodies", 10);
            globCoverIndex.put("PermanentSnowAndIce", 10);

        } catch (Throwable e) {
            return false;
        }
        return true;
    }

    /**
     * Return the curve number for the given landcover/soil group combination according to
     * Mutua et al. Assume that if no match is found with known classes, the runoff is 100,
     * i.e. total.
     * 
     * @param landcover
     * @param hsg
     * @return
     */
    public static int getCurveNumber(IConcept landcover, IConcept hsg) {

        if (landcover == null || hsg == null) {
            return 100;
        }

        Integer h = hsgIndex.get(hsg.getLocalName());
        Integer l = globCoverIndex.get(landcover.getLocalName());

        if (h != null && l != null) {
            return curveNumberTable[h][l];
        }

        return 100;
    }

    /**
     * Get the amount of the passed precipitation that runs off a cell with the passed types.
     * 
     * @param precipitation
     * @param landcover
     * @param hsg
     * @return
     */
    public static double getRunoffAmount(double precipitation, IConcept landcover, IConcept hsg) {
        return getRunoffAmount(precipitation, getCurveNumber(landcover, hsg));
    }

    /**
     * Get the amount of the passed precipitation that runs off a cell with this curve number.
     * @param precipitation
     * @param landcover
     * @param hsg
     * @return
     */
    public static double getRunoffAmount(double precipitation, int curveNumber) {

        double retention = 254.0 * ((100.0 / curveNumber) - 1);
        if (precipitation > (0.2 * retention)) {
            return ((precipitation - (0.2 * retention)) * (precipitation - (0.2 * retention)))
                    / (precipitation + (0.8 * retention));
        }
        return 0;
    }
}
