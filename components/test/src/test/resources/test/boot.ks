//## TODO macros to define engine startup, properties etc

// scenario IDs to try in loops; empty string = baseline
scenarios = ['' 'scenario1' 'scenario2']
contexts =  ['ctx1' 'ctx2']
observables = ['geography:Elevation' 'geography:Slope' /* etc */]

// main loop testing the various scenarios
for (context in contexts) {
    for (scenario in scenarios) {
    
        resetScenarios()
        if (!scenario.isEmpty()) {
            setScenario(scenario);
        }
    
        ctx = observe(context)
        observations = []
        for (observable in observables) {
            observations << (ctx << observable)
        }
    }
}