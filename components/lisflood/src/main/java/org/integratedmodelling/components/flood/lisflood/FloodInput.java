/*******************************************************************************
 *  Copyright (C) 2007, 2014:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.components.flood.lisflood;

/**
 * The transposition of the CAESAR-LISFLOOD input form, with the possible defaults
 * already filled in.
 * 
 * @author Ferd
 *
 */
public class FloodInput {

    // page 1 - files
    boolean CATCHMENT_MODE = true;
    boolean REACH_MODE = false;

    boolean GENERATE_TIMESERIES_OUTPUT = false;
    boolean GENERATE_TRACER_OUTPUT = false;
    boolean GENERATE_GOOGLE_EARTH_ANIMATION = false;
    boolean GENERATE_ITERATION_OUTPUT = false;
    boolean READ_DISCHARGE_FROM_RAINFALL_FILE = false;

    int OUTPUT_SAVE_INTERVAL_MINS = 1000;
    int KMZ_SAVE_INTERVAL_MINS = 1000;
    int TIMESERIES_SAVE_INTERVAL_MINS = 60;

    String DEM_DATA_FILE = "";
    String GRAIN_DATA_FILE = "";
    String VARIABLE_M_DATA_FILE = "";
    String BEDROCK_DATA_FILE = "";
    String TRACER_SEDIMENT_VOLUME_FILE = "";
    String RAINFALL_DATA_FILE = "";
    int RAINFALL_STEP_MIN = 60;

    boolean GRID_IS_UTM;
    int UTM_ZONE;
    boolean SOUTHERN_EMISPHERE;

    // page 2 - numerical
    int MAX_ITERATIONS = 100000;
    int MIN_TIME_STEP_SECS = 1; // caution! ?
    int MAX_TIME_STEP_SECS = 3600;
    int RUN_START_TIME_H = 0;
    int RUN_MAX_DURATION_H = 1000;
    int MEMORY_LIMIT = 1; // ?

    // page 3 - sediment. Sum of percentages must equal 1.
    double GRAIN_SIZE_1 = 0.0005;
    double GRAIN_PROPORTION_1 = 0.144;
    double GRAIN_SIZE_2 = 0.001;
    double GRAIN_PROPORTION_2 = 0.022;
    double GRAIN_SIZE_3 = 0.002;
    double GRAIN_PROPORTION_3 = 0.019;
    double GRAIN_SIZE_4 = 0.004;
    double GRAIN_PROPORTION_4 = 0.029;
    double GRAIN_SIZE_5 = 0.008;
    double GRAIN_PROPORTION_5 = 0.068;
    double GRAIN_SIZE_6 = 0.016;
    double GRAIN_PROPORTION_6 = 0.146;
    double GRAIN_SIZE_7 = 0.032;
    double GRAIN_PROPORTION_7 = 0.220;
    double GRAIN_SIZE_8 = 0.064;
    double GRAIN_PROPORTION_8 = 0.231;
    double GRAIN_SIZE_9 = 0.128;
    double GRAIN_PROPORTION_9 = 0.121;

    boolean COMPUTE_SUSPENDED = false;
    double FALL_VELOCITY_M_SEC = 0.066;

    boolean WILLCOCK_CROWE_METHOD = true;
    boolean EINSTEIN_METHOD = false;

    boolean COMPUTE_RECIRCULATION = false;
    double MAX_VELOCITY = 5;
    double MAX_ERODE_LIMIT = 0.02;
    double ACTIVE_LAYER_THICKNESS_m = 0.1; // must be >= max erode limit * 4
    double RECIRCULATION_PROPORTION = 1.0; // if recirculate = true;
    int IN_CHANNEL_LATERAL_EROSION_RATE = 20;

    // lateral erosion
    boolean COMPUTE_LATERAL_EROSION = false;
    double LATERAL_EROSION_RATE = 0;
    int EDGE_SMOOTHING_PASSAGE_COUNT = 100; // avge-smoothing
    int CELLS_TO_SHIFT_DOWNSTREAM = 5; // downstream_shift_box
    double MAX_SMOOTHING_DIFFERENCE_CROSS_CHANNEL = 0.0001; //textBox 7

    // grid
    int XCELLS = 593; // xbox
    int YCELLS = 358; // ybox
    int CELL_SIZE_M = 5; // dxbox

    // hydrology
    class HydrologyInput {
        public int x;
        public int y;
        public String file;
    }

    HydrologyInput[] HYDROLOGY_INPUTS = new HydrologyInput[8];
    int INPUT_DATA_TIMESTEP_MINUTES = 1440;
    double M_VALUE = 0.01;

    // vegetation
    double VEGETATION_CRIT_SHEAR = 180.0;
    int GRASS_MATURITY_YEARS = 5;
    double EROSION_PROPORTION_FULLGROWN_VEGETATION = 0.1;

    // slope processes
    double SLOPE_CREEP_RATE = 0.0025;
    int SLOPE_FAILURE_THRESHOLD_ANGLE = 45;

    boolean COMPUTE_LANDSLIDES = false;

    /**
     * Slope * SoilErosionRate * (drainage_area ^ .5) * time(yr) / DX (cellsize)
     */
    double SOIL_EROSION_RATE = 0.0;
    boolean EROSION_DEPENDS_ON_J_MEAN = false;

    // Siberia submodel
    boolean USE_SIBERIA_SUBMODEL = false;
    double SIBERIA_BETA_1 = 1067;
    double SIBERIA_BETA_3 = 0.000186;
    double SIBERIA_M1 = 1.7;
    double SIBERIA_M3 = 0.79;
    double SIBERIA_N1 = 0.69;

    // dune model
    boolean COMPUTE_DUNES = false;
    int SLABS_ADDED_PER_COL_PER_ITER = 4;
    double MAX_SLAB_THICKNESS_M = 0.5;
    int SHADOW_LANDSLIP_ANGLE_DEG = 15;
    int SHADOW_CHECK_DISTANCE_CELLS = 40;
    int DEPOSITION_PROBABILITY_PERCENT = 50;
    int DOWNSTREAM_OFFSET = 1;
    int TIMESTEPS_BETWEEN_DUNE_CALLS = 144;
    int GRID_SIZE_OF_DUNES = 2;
    int FRACTION_OF_DUNE = 1;

    // flow model

    boolean COMPUTE_FLOW_ONLY = false;
    int INPUT_OUTPUT_DIFFERENCE_ALLOWED = 1;
    double MIN_Q_FOR_DEPTH_CALC = 0.01;
    double WATER_DEPTH_EROSION_THRESHOLD = 0.01;
    double EDGE_CELL_SLOPE = 0.005;
    double EVAPORATION_RATE_M_DAY = 0.0;
    double COURANT_NUMBER = 0.7;
    double HFLOW_THRESHOLD = 0.001;
    double FROUDE_N_FLOW_LIMIT = 0.8;
    double MANNINGS_N = 0.04;

}
