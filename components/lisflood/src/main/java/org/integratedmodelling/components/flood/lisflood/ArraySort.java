/*******************************************************************************
 *  Copyright (C) 2007, 2014:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.components.flood.lisflood;

public class ArraySort {

    Double[] _a2;

    void sortMatched(Double[] a1, Double[] a2) {
        quickSort(a1, 0, a1.length - 1);
    }

    private void swap(Double[] a, int lft, int rt) {
        Double temp;
        temp = a[lft];
        a[lft] = a[rt];
        a[rt] = temp;

        // swap the matched array
        temp = _a2[lft];
        _a2[lft] = _a2[rt];
        _a2[rt] = temp;
    }

    public int pivot(int firstpl, int lastpl) {
        if (firstpl >= lastpl)
            return -1;
        else
            return firstpl;
    }

    private void quickSort(Double[] a, int first, int last) {
        int left, right;
        int pivindex = pivot(first, last);
        if (pivindex >= 0) {
            left = pivindex + 1;
            right = last;
            do {
                while (a[left] < a[pivindex] && left <= right)
                    left++;
                while (a[right] > a[pivindex])
                    right--;
                if (right > left)
                    swap(a, left, right);
            } while (left < right);
            swap(a, pivindex, right);
            quickSort(a, first, right - 1);
            quickSort(a, right + 1, last);
        }
    }

}
