package org.integratedmodelling.components.luc;

import org.integratedmodelling.Version;
import org.integratedmodelling.api.plugin.Component;
import org.integratedmodelling.api.plugin.Initialize;

@Component(id = "im.search", version = Version.CURRENT)
public class LUCComponent {

    @Initialize
    public boolean initialize() {

        /*
         * Check configuration to ensure that this engine serves one and only one
         * domain.
         */

        /*
         * synchronize and if necessary reload the latest core ontologies for the groups we 
         * have configured.
         */

        /*
         * check projects in deploy directory
         */

        /*
         * load them; ignore errors
         */

        /*
         * we're online if we have anything in the kbox
         */

        return false;
    }
}
