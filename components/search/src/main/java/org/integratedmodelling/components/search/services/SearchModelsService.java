/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.components.search.services;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.integratedmodelling.api.knowledge.IConcept;
import org.integratedmodelling.api.knowledge.IKnowledge;
import org.integratedmodelling.api.metadata.IMetadata;
import org.integratedmodelling.api.modelling.IScale;
import org.integratedmodelling.api.services.IServiceCall;
import org.integratedmodelling.api.services.annotations.Execute;
import org.integratedmodelling.api.services.annotations.Prototype;
import org.integratedmodelling.api.services.types.IEngineService;
import org.integratedmodelling.common.configuration.KLAB;
import org.integratedmodelling.common.metadata.Metadata;
import org.integratedmodelling.common.model.runtime.Scale;
import org.integratedmodelling.common.network.Endpoints;
import org.integratedmodelling.common.owl.Knowledge;
import org.integratedmodelling.engine.modelling.kbox.ModelData;
import org.integratedmodelling.engine.modelling.kbox.ModelKbox;
import org.integratedmodelling.exceptions.ThinklabException;

@Prototype(
        id = Endpoints.QUERY_MODELS,
        args = {
                "# is-instantiator",
                Prototype.BOOLEAN,
                // don't validate as concept because it's legitimate to be asking for unknown concepts.
                "subject-type",
                Prototype.TEXT,
                "observable-type",
                Prototype.TEXT,
                "observation-type",
                Prototype.CONCEPT,
                "# inherent-subject-type",
                Prototype.CONCEPT,
                "# inherent-type",
                Prototype.CONCEPT,
                "context-traits",
                Prototype.TEXT,
                "# scenarios",
                Prototype.TEXT,
                "scale",
                Prototype.TEXT,
                "criteria",
                Prototype.TEXT,
                "# detail-level",
                Prototype.INT,
                "# engine-capabilities",
                Prototype.TEXT
        })
public class SearchModelsService implements IEngineService {

    @Execute
    public Object execute(IServiceCall command) throws ThinklabException {

        List<ModelData> ret = new ArrayList<>();
        Set<String> groups = null;

        KLAB.info("search service called");

        if (!KLAB.NETWORK.getResourceCatalog().hasAuthorizedContent(groups)) {
            return ret;
        }

        IScale scale = new Scale(command.get("scale").toString());
        String otypes = command.get("observable-type").toString();
        IMetadata criteria = Metadata.parse(command.get("criteria").toString());
        boolean isInstantiator = command.has("is-instantiator")
                && command.getString("is-instantiator").equals("true");

        List<IKnowledge> types = splitKnowledge(otypes);
        if (types.isEmpty()) {
            return ret;
        }

        IKnowledge subjectType = Knowledge.parse(command.get("subject-type").toString());
        if (subjectType == null) {
            return ret;
        }

        // Env.logger.info("search for " + command.get("scale") + " was " + scale);
        IConcept observationType = (IConcept) command.get("observation-type");
        IConcept contextType = null;
        if (command.has("inherent-subject-type")) {
            contextType = (IConcept) Knowledge.parse(command.get("inherent-subject-type").toString());
        }
        IConcept inherentType = null;
        if (command.has("inherent-type")) {
            inherentType = (IConcept) Knowledge.parse(command.get("inherent-type").toString());
        }

        int detailLevel = -1;
        if (command.has("detail-level")) {
            detailLevel = Integer.parseInt(command.get("detailLevel").toString());
        }

        List<IKnowledge> traits = splitKnowledge("context-traits");
        String[] scenarios = command.get("scenarios").toString().isEmpty() ? null : command.get("scenarios")
                .toString().split(",");

        /*
         * TODO filter out those not matching engine capabilities if any were passed
         */
        ret = ModelKbox
                .get()
                .query(subjectType, groups, types, scale, isInstantiator, observationType, contextType, inherentType, detailLevel, traits, scenarios, criteria);

        KLAB.info("query for models of " + types + " retrieved " + ret.size() + " results");

        return ret;
    }

    private List<IKnowledge> splitKnowledge(String string) {
        List<IKnowledge> ret = new ArrayList<>();
        for (String s : string.split(",")) {
            IKnowledge c = Knowledge.parse(s);
            if (c != null) {
                ret.add(c);
            }
        }
        return ret;
    }
}
