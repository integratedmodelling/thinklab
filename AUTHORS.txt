Ferdinando Villa <ferdinando.villa@bc3research.org>  Concept, design and vast majority of implementation
Luke Scott <luke@cronworks.net>                      Agent scheduling and runtime synchronization
Ioannis N. Athanasiadis <ioannis@athanasiadis.info>  Early reasoning API and implementation, ENFA component
Robin T. Wilson <R.T.Wilson@soton.ac.uk>             Port of LPJ-GUESS model into lpj component

