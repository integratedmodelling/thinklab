/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.api.factories;

import java.util.Collection;

import org.integratedmodelling.api.knowledge.IConcept;
import org.integratedmodelling.api.lang.IParsingContext;
import org.integratedmodelling.api.modelling.IDataSource;
import org.integratedmodelling.api.modelling.IDirectActiveObservation;
import org.integratedmodelling.api.modelling.IDirectObserver;
import org.integratedmodelling.api.modelling.IFunctionCall;
import org.integratedmodelling.api.modelling.IModel;
import org.integratedmodelling.api.modelling.IModelBean;
import org.integratedmodelling.api.modelling.IObjectSource;
import org.integratedmodelling.api.modelling.IObserver;
import org.integratedmodelling.api.modelling.IObservingObject;
import org.integratedmodelling.api.modelling.IScale;
import org.integratedmodelling.api.modelling.runtime.IActuator;
import org.integratedmodelling.api.modelling.storage.IDataset;
import org.integratedmodelling.api.modelling.storage.IStorage;
import org.integratedmodelling.api.monitoring.IKnowledgeLifecycleListener;
import org.integratedmodelling.api.monitoring.IMonitor;
import org.integratedmodelling.api.space.ISpatialExtent;
import org.integratedmodelling.api.space.ISpatialIndex;
import org.integratedmodelling.exceptions.ThinklabException;

/**
 * Encapsulates all functions that make modeling possible, like the creation of observations, accessors, storage
 * and execution of runtime functions. A model factory is always available as Env.MFACTORY; according to implementation,
 * it may create dummy objects or functional ones.
 * 
 * @author Ferd
 *
 */
public interface IModelFactory {

    /**
     * Add a listener to intercept generation of k.IM knowledge.
     * 
     * @param listener
     */
    void addKnowledgeLifecycleListener(IKnowledgeLifecycleListener listener);

    /**
     * Get a top-level context for parsing a model tree.
     * 
     * @return
     */
    IParsingContext getRootParsingContext();

    /**
     * Call the passed function using whatever mechanism for code resolution is available in
     * the runtime.
     * 
     * @param function
     * @param monitor
     * @param model
     * @param context
     * @return
     * @throws ThinklabException
     */
    Object callFunction(IFunctionCall function, IMonitor monitor, IModel model, IConcept... context)
            throws ThinklabException;

    /**
     * Create the storage for a state of this observer in this scale, using a backing dataset if
     * passed. If state is dynamic, prepare for storage of timeslices.
     * 
     * @param observer
     * @param scale
     * @param dataset
     * @param isDynamic
     * @return
     */
    IStorage<?> createStorage(IObserver observer, IScale scale, IDataset dataset, boolean isDynamic);

    /**
     * 
     * @param observer
     * @param context
     * @param monitor
     * @return
     * @throws ThinklabException
     */
    IDirectActiveObservation createSubject(IDirectObserver observer, IDirectActiveObservation context, IMonitor monitor)
            throws ThinklabException;

    /**
     * Create the favorite implementation of IScale based on the extent definition in the passed functions.
     * 
     * @param definition
     * @return
     */
    IScale createScale(Collection<IFunctionCall> definition);

    /**
     * Create a scale out of any objects, turning each into extents the way they are supposed to in the
     * implementation. Could be passed extents, WKT strings, function calls, time locations or intervals, etc. If
     * any of the objects can't be turned into an extent, throw a runtime exception.
     */
    IScale createScale(Object... objects);

    /**
     * Create an actuator for the passed object based on its definition; if actions are defined, wrap any
     * result into an action actuator. The passed function call may be null - it will be invoked anyway if
     * the observer has actions. If the function call returns a contextualizer, wrap it into a contextualizing
     * actuator.
     * 
     * @param observer
     * @param monitor
     * @return
     * @throws ThinklabException 
     */
    IActuator getActuator(IFunctionCall call, IObservingObject observer, IMonitor monitor)
            throws ThinklabException;

    /**
     * Call the function and return the created datasource.
     * 
     * @param datasource
     * @return
     */
    IDataSource createDatasource(IFunctionCall datasource, IMonitor monitor) throws ThinklabException;

    /**
     * Call the function and return the created object.
     * 
     * @param datasource
     * @return
     */
    IObjectSource createObjectsource(IFunctionCall objectsource, IMonitor monitor) throws ThinklabException;

    /**
     * Create a constant data source that will return the passed literal no matter what.
     * 
     * @param inlineValue
     * @return
     */
    IDataSource createConstantDataSource(Object inlineValue);

    /**
     * If needed, wrap the passed object into a suitable wrapper for use within
     * actions. Otherwise return the unmodified object. Called before actions are
     * executed.
     * 
     * @param oubject
     * @return
     */
    Object wrapForAction(Object object);

    /**
     * Create and return a spatial index for this object. Will only be used in model engines so
     * safe to return null in a client.
     * 
     * @param space
     * @return
     */
    ISpatialIndex getSpatialIndex(ISpatialExtent space);
    
    /**
     * Turn the passed object into a model bean that describes it and can be passed around
     * the network. If no model bean exists to describe it, return it unmodified.
     * 
     * @param o
     * @return
     */
    IModelBean adapt(Object o);
    
    /**
     * Turn the passed model bean into the object that incarnates it in the runtime context
     * of the call. If no translation exist, return the unmodified object.
     * 
     * @param o
     * @return
     */
    Object reconstruct(IModelBean o);

}
