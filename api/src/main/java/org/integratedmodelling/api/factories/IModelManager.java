/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.api.factories;

import java.io.File;
import java.util.Collection;
import java.util.List;

import org.integratedmodelling.api.knowledge.IConcept;
import org.integratedmodelling.api.knowledge.IKnowledge;
import org.integratedmodelling.api.modelling.IModelObject;
import org.integratedmodelling.api.modelling.INamespace;
import org.integratedmodelling.api.modelling.ISubject;
import org.integratedmodelling.api.services.IPrototype;

/**
 * The model factory contains the register of all model objects and namespaces. It's capable of
 * reconstructing dependency structure and source code of all objects. It can load model definitions
 * from resources and release objects on a namespace basis.
 * 
 * The paradigm "one resource, one namespace" is mandatory and enforced.
 * 
 * The namespaces handled by the model manager are not the only namespaces in the system. Core
 * knowledge not loaded from projects is not seen by the model manager. The Knowledge manager 
 * sees every namespace. Also it must be able to provide a privileged namespace where all
 * model observables are created.
 * 
 * @author Ferdinando
 *
 */
public interface IModelManager {

    /**
     * 
     * @param ns
     * @return
     */
    INamespace getNamespace(String ns);

    /**
     * 
     * @param namespace
     */
    void releaseNamespace(String namespace);

    /**
     * Return all namespaces.
     * @return
     */
    Collection<INamespace> getNamespaces();

    /**
     * Return all namespaces tagged as scenarios.
     * 
     * @return
     */
    List<INamespace> getScenarios();

    // /**
    // * Load all model objects defined in the given file, adding them to the model kbox as appropriate.
    // *
    // * @param resourceId
    // * @param namespaceId
    // * @param monitor
    // * @param the higher-level resolver (normally a project resolver obtained by a call to
    // IResolver.getProjectResolver())
    // * @param set of the namespace IDs already loaded in this session, so that no recursive loading of
    // dependencies is done. Start empty.
    // * @return the namespace defined
    // * @throws ThinklabIOException
    // */
    // INamespace loadFile(final File resourceId, final String namespaceId, IModelResolver resolver,
    // boolean substituteExisting, IMonitor monitor, Set<String> context) throws ThinklabException;

    /**
     * Lookup the model object identified by this ID. For now only lookup
     * those in the NSs loaded directly as files. Later we can query the kbox too.
     * 
     * @param name
     * @return
     */
    IModelObject findModelObject(String name);

    /**
     * Return the observation namespace where all observable concepts reside. It must always return
     * the same non-null namespace. It's created and used at runtime only, so it starts empty and 
     * doesn't need to be written anywhere.
     * 
     * @return
     */
    INamespace getObservationNamespace();

    // /**
    // * Return the root resolver. This is normally a single instance per manager.
    // * The normal usage pattern is to ask for a context resolver from it, which is then
    // * used to resolve groups of projects and/or namespaces.
    // *
    // * @return
    // *
    // */
    // IModelResolver getRootResolver();

    /**
     * Check if this is a file that we can turn into a namespace. Normally it
     * will be based on extension checking, but feel free to make it smarter (e.g.
     * only accepting files from a set of trusted locations).
     * 
     * @param f
     * @return
     */
    boolean isModelFile(File f);

    /**
     * Return the function prototype for the given function ID, or null if not registered.
     * 
     * @param id
     * @return
     */
    public abstract IPrototype getFunctionPrototype(String id);

    /**
     * Associate a subject class to a concept and its subclasses (until overridden by
     * another association). 
     * 
     * @param concept
     * @param cls
     */
    void registerSubjectClass(String concept, Class<? extends ISubject> cls);

    /**
     * Return the Java class that incarnates the passed subject type.
     * 
     * @param type
     * @return
     */
    Class<? extends ISubject> getSubjectClass(IConcept type);

    /**
     * Return the namespace where all local objects and models are kept. This namespace
     * must exist, and should have the id of the user in personal servers and clients, and
     * an unambiguous name for any public server. Objects loaded from anything other than
     * a project (e.g. the observation kbox) are created in this namespace.
     * 
     * @return
     */
    INamespace getLocalNamespace();

    /**
     * Release every namespace and all the associated knowledge.    
     */
    void releaseAll();

    /**
     * Return the knowledge corresponding to the passed ID if a namespace exports it.
     * 
     * @param id
     * @return
     */
    IKnowledge getExportedKnowledge(String id);

}
