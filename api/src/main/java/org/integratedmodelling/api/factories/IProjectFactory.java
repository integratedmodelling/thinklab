/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.api.factories;

import java.io.File;
import java.util.Collection;

import org.integratedmodelling.api.modelling.INamespace;
import org.integratedmodelling.api.network.INode;
import org.integratedmodelling.api.project.IProject;
import org.integratedmodelling.exceptions.ThinklabException;

/**
 * A project factory is a project manager that can also create, delete and modify projects on
 * the filesystem. 
 * <p>
 * The general usage pattern should be 
 * 
 * <code>
 * if (projectManager instanceof IProjectFactory) {
 * 	// do factory things
 * }
 * </code>
 * 
 * @author Ferd
 *
 */
public interface IProjectFactory extends IProjectManager {

    /**
     * Create project files in the passed path, which should be the actual project path, not
     * a directory that contains it. Update the workspace and call listeners.
     * 
     * TODO move to workspace and have that call listeners.
     * 
     * @param projectPath project path; doesn't have to exist.
     * @param prerequisites array of project IDs the new project will depend on. May be null.
     * @return a newly created and registered (but not loaded) project.
     */
    IProject createProject(File projectPath, String[] prerequisites) throws ThinklabException;

    /**
     * Delete the passed project from the filesystem. If loaded unload it first; then unregister it.
     * Call any listeners on the project manager and the workspace.
     * 
     * @param projectId
     * @throws ThinklabException
     */
    void deleteProject(String projectId) throws ThinklabException;

    /**
     * Create an archive file suitable for remote deployment and backup in temporary storage 
     * for the passed project, which must be registered.
     * 
     * @return
     * @throws ThinklabException
     */
    File archiveProject(String projectId) throws ThinklabException;

    /**
     * Rename the passed namespace (within the same project). Create/delete all necessary
     * files. Call all listeners. Return a list of all namespaces affected by the change, the
     * new one first.
     * 
     * Fails if a namespace with the new   name already exists.
     * 
     * @param namespace
     * @param newName
     * @return
     */
    Collection<INamespace> renameNamespace(INamespace namespace, String newName) throws ThinklabException;

    /**
     * Import a project from a network node. The resulting project should minimize
     * pre-loading and only load knowledge on demand.
     * 
     * @param node
     * @param projectId
     * @return
     * @throws Exception 
     */
    IProject importProject(INode node, String projectId) throws ThinklabException;
}
