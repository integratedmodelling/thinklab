/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.api.factories;

import org.integratedmodelling.api.knowledge.IAuthority;
import org.integratedmodelling.api.knowledge.IConcept;
import org.integratedmodelling.api.knowledge.IKnowledge;
import org.integratedmodelling.api.knowledge.IOntology;
import org.integratedmodelling.api.knowledge.IProperty;
import org.integratedmodelling.api.modelling.INamespace;

/**
 * Handles creation, management, storage and introspection on semantic objects.
 * 
 * @author Ferd
 *
 */
public interface IKnowledgeManager {

    /**
     * Find and return the named concept. If not found, return null.
     * @param prop
     * @return
     */
    IConcept getConcept(String concept);

    /**
     * Find and return the passed property. If not found, return null.
     * 
     * @param prop
     * @return
     */
    IProperty getProperty(String concept);

    /**
     * Find and return a concept or property with this id. If not found, return null.
     * 
     * @param fullId
     * @return
     */
    IKnowledge getKnowledge(String fullId);

    /**
     * Return the concept at the root of all the concept hierarchy.
     * 
     * @return
     */
    IConcept getRootConcept();

    /**
     * This may be overkill, but for now we distinguish some "upper" abstract knowledge from the
     * namespaces returned by the model manager, which contain user knowledge and model content.
     * This one is expected to return namespaces corresponding to all the core ontologies installed
     * outside the modeling and project subsystems. Typically that will correspond to upper ontologies
     * that users do not necessarily need to see, hence the distinction.
     * 
     * @param ns the namespace (ontology name).
     * @return A namespace or null if not found.
     */
    INamespace getCoreNamespace(String ns);

    /**
     * Return the authority corresponding to the passed ID, or null if it was not installed or authorized.
     * 
     * @param id
     * @return
     */
    IAuthority getAuthority(String id);

    /**
     * Return or create the passed ontology.
     * 
     * @param id
     * @return
     */
    IOntology requireOntology(String id);

}
