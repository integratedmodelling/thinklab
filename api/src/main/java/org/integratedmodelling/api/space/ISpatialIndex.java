package org.integratedmodelling.api.space;

import java.util.Collection;

import org.integratedmodelling.api.modelling.IScale;
import org.integratedmodelling.collections.Pair;

public interface ISpatialIndex {
    
    void add(ISpatialExtent extent, String name);
    
    double distanceBetween(IScale.Locator position, String objectId);

    double distanceBetween(int offset, String objectId);
    
    Collection<Pair<String,ISpatialExtent>> getNearest(IScale.Locator position, int maxResults);
    
    ISpatialExtent getExtent();
    
    boolean contains(String objectId);

    double distanceToNearestObjectFrom(int sfs);

    double distanceToNearestObjectFrom(IScale.Locator sfs);

    int size();
}
