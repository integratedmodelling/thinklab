/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.api.project;

import java.io.File;
import java.util.Collection;
import java.util.List;
import java.util.Properties;

import org.integratedmodelling.Version;
import org.integratedmodelling.api.lang.IParsingContext;
import org.integratedmodelling.api.modelling.INamespace;
import org.integratedmodelling.exceptions.ThinklabException;

/**
 * Thinklab projects are plug-in packages that provide Thinklab resources such as models, annotations etc. 
 * This interface is shared by the client and server side. Projects at the server side will need more
 * methods to manage their lifetime in Thinklab.
 * @author  Ferd
 */
public interface IProject {

    public static final String THINKLAB_META_INF        = "META-INF";
    public static final String THINKLAB_PROPERTIES_FILE = "thinklab.properties";

    // properties for META-INF/thinklab.properties file. May also have metadata
    // according to IMetadata fields.
    public static final String SOURCE_FOLDER_PROPERTY               = "thinklab.source.folder";
    public static final String ONTOLOGY_NAMESPACE_PREFIX_PROPERTY   = "thinklab.ontology.prefix";
    public static final String PREREQUISITES_PROPERTY               = "thinklab.prerequisites";
    public static final String IMPORTS_PROPERTY                     = "thinklab.imports";
    public static final String VERSION_PROPERTY                     = "project.version";
    public static final String DEFAULT_NAMESPACE_PREFIX             = "http://www.integratedmodelling.org/ns";
    public static final String REMOTELY_SYNCHRONIZED_GROUP_PROPERTY = "thinklab.remote.group";
    public static final String IS_WORLDVIEW_PROPERTY                = "thinklab.worldview";

    /**
    * Plugins have a name.
    * 
    * @return
    */
    public String getId();

    /**
     * Find a resource file or directory in the plugin. Extract it to a scratch
     * area if necessary so it is a File that can be copied and 
     * used locally. Expected to be read only. Return null if not found.
     * 
     * @param resource
     * @return
     */
    public File findResource(String resource);

    /**
     * Installation directory. It needs to be non-null even if the plugin doesn't have resources.
     * 
     * @return
     */
    public File getLoadPath();

    /**
     * Also must not return null.
     * 
     * @return
     */
    public Properties getProperties();

    /**
     * 
     * @return
     */
    public Version getVersion();

    /**
     * Return all the namespaces defined in the project.
     * 
     * @return
     */
    public Collection<INamespace> getNamespaces();

    /**
     * Source folders are scanned at startup and monitored during development. The location of
     * source folders is in the SOURCE_FOLDER_PROPERTY of the thinklab properties. There is
     * one source folder per project.
     * 
     * @return the path of the source directory, relative to the project workspace.
     */
    public String getSourceDirectory();

    /**
     * Any ontologies in the source folder must have a URL that matches the project's 
     * ontology namespace prefix (from properties, defaulting to http://www.integratedmodelling.org/ns)
     * followed by the path corresponding to the ontology namespace and file name.
     * 
     * @return
     */
    public String getOntologyNamespacePrefix();

    /**
     * Find the resource file that defines the passed namespace. If not
     * found, return null.
     * 
     * @param namespace
     * @return
     */
    public File findResourceForNamespace(String namespace);

    /**
     * Find a namespace either in this project or in any of the 
     * prerequisites, using the context to load it if necessary. Return
     * null if nothing is found. 
     * 
     * @param namespace
     * @return
     * @throws ThinklabException for errors during load
     */
    public INamespace findNamespaceForImport(String namespace, IParsingContext context)
            throws ThinklabException;

    /**
     * Get all projects we depend on. These should be ordered in load order.
     * 
     * @return
     * @throws ThinklabException 
     */
    public abstract List<IProject> getPrerequisites() throws ThinklabException;

    /**
     * Return when this was last modified, so that we can load efficiently. This should
     * return the modification time of the newest resource.
     * 
     * @return
     */
    public long getLastModificationTime();

    /**
     * Return true if this provides a definition for the named namespace. Must be
     * able to answer without loading anything.
     * 
     * @param namespaceId
     * @return
     */
    boolean providesNamespace(String namespaceId);

    /**
     * Return the name of each top-level resource folder in the project that
     * is not one of the automatically managed ones.
     * 
     * @return
     */
    public abstract List<String> getUserResourceFolders();

    /**
     * Return true if any of the resources in the project has caused compilation errors. If
     * this is true, the project should not be used.
     * 
     * @return
     */
    public boolean hasErrors();

    /**
     * 
     * @return
     */
    public boolean hasWarnings();

    /**
     * Return all the script files in the project. Those should be in their own directory.
     * 
     * @return
     */
    public Collection<File> getScripts();

    /**
     * 
     * @param resource
     * @return
     */
    INamespace findNamespaceForResource(File resource);

    /**
     * A project that is closed is invisible to the project manager. The setting is
     * permanent and persists across sessions and engines until open(true) is passed.
     * 
     * @param open
     */
    void open(boolean open);

    /**
     * Report the open/close status of this project.
     * 
     * @return
     */
    boolean isOpen();

    /**
     * If true, the project has been downloaded and synchronized from a remote
     * location at authentication. For this reason it should be considered read-only
     * and not shown in editors.
     * 
     * @return
     */
    boolean isRemote();

    /**
     * Worldview status is indicated by a thinklab.worldview = true property. Only one worldview 
     * is permitted and a worldview must exist.
     * 
     * @return
     */
    boolean isWorldview();
}
