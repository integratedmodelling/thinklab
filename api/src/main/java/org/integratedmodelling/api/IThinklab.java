/// *******************************************************************************
// * Copyright (C) 2007, 2015:
// *
// * - Ferdinando Villa <ferdinando.villa@bc3research.org>
// * - integratedmodelling.org
// * - any other authors listed in @author annotations
// *
// * All rights reserved. This file is part of the k.LAB software suite,
// * meant to enable modular, collaborative, integrated
// * development of interoperable data and model components. For
// * details, see http://integratedmodelling.org.
// *
// * This program is free software; you can redistribute it and/or
// * modify it under the terms of the Affero General Public License
// * Version 3 or any later version.
// *
// * This program is distributed in the hope that it will be useful,
// * but without any warranty; without even the implied warranty of
// * merchantability or fitness for a particular purpose. See the
// * Affero General Public License for more details.
// *
// * You should have received a copy of the Affero General Public License
// * along with this program; if not, write to the Free Software
// * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
// * The license is also available at: https://www.gnu.org/licenses/agpl.html
// *******************************************************************************/
// package org.integratedmodelling.api;
//
// import org.integratedmodelling.api.configuration.IConfiguration;
// import org.integratedmodelling.api.factories.IKnowledgeManager;
// import org.integratedmodelling.api.factories.IModelManager;
// import org.integratedmodelling.api.factories.IProjectManager;
// import org.integratedmodelling.exceptions.ThinklabException;
//
/// **
// * An object that can be returned to provide a pointer to the necessary factories. Not necessary
// * within individual applications, but it can be used by anything needing thinklab as a central
// * point to access Thinklab. Each implementation can create one of these and publish it as
// * appropriate. For example IResolver can be asked for one of these so that
// * language parsers can validate concepts, and the thinklab-common library has methods in
// * Env to set and get the IThinklab for common applications.
// *
// * @author Ferd
// * @deprecated definitely obsolete (not just the name). Remove and rename Env to something sensible when
/// there is time.
// */
// @Deprecated
// public interface IThinklab {
//
// /**
// *
// * @return
// */
// public IKnowledgeManager getKnowledgeManager();
//
// /**
// *
// * @return
// */
// public IProjectManager getProjectManager();
//
// /**
// *
// * @return
// */
// public IConfiguration getConfiguration();
//
// /**
// *
// * @return
// */
// public IModelManager getModelManager();
//
// /**
// * Rescan services and prototypes after new binary components have been loaded. Should be a
// * no-op in a client.
// *
// * TODO move to component manager and call it something w/o classpath in it.
// *
// * @throws ThinklabException
// */
// public void rescanClasspath() throws ThinklabException;
// }
