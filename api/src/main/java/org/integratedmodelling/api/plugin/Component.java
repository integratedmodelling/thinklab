/*******************************************************************************
 *  Copyright (C) 2007, 2014:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.api.plugin;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Defines a component by annotating a Java class that becomes the component's
 * initializer. The class may be empty or have methods for initialization, setup
 * and shutdown. The package that class belongs on tells Thinklab where to look for
 * services, functions, and TQL files to load with the component.
 * 
 * @author ferdinando.villa
 *
 */
@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
public @interface Component {

    /**
     * ID of the component. Must be unique, please use unambiguous paths like package or
     * project names.
     * 
     * @return
     */
    String id();

    /**
     * Version number, parseable by {@link org.integratedmodelling.Version}.
     * 
     * @return
     */
    String version();

    /**
     * The groups that are allowed to use the component. If empty, component is public.
     * 
     * @return
     */
    String[] allowedGroups() default {};

    /**
     * List of other component IDs that this one depends on.
     * @return
     */
    String[] requiresComponents() default {};

    /**
     * List of projects that need to be loaded before this can be initialized. Assumes they're
     * available through a node that advertises them on the network.
     * 
     * @return
     */
    String[] requiresProjects() default {};

    /**
     * No need for much sophistication for the next few years, but the reference
     * domain should be mentioned.
     * @return
     */
    String domain() default "";

    /**
     * Distributed means that this component can be used along with the same
     * component in other servers that provide it, and that the supporting 
     * implementation provides a merge() method to merge results.
     * 
     * @return
     */
    boolean distributed() default false;

}
