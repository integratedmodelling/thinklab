/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.api.data;

public interface IDataAsset {

    // types - extendible of course
    public final static String TYPE_WFS     = "wfs";
    public final static String TYPE_WCS     = "wcs";
    public final static String TYPE_RASTER  = "raster";
    public final static String TYPE_VECTOR  = "vector";
    public final static String TYPE_OPENDAP = "opendap";
    public final static String TYPE_INLINE  = "inline";
    public final static String TYPE_TABLE   = "table";

    // data keys - not all used, more will be needed
    public final static String KEY_URL              = "url";
    // TODO this is what this should become once the promised sanity has been implemented for real.
    public final static String KEY_PUBLISHED_DATAID = "publishedName";
    public final static String KEY_DATAID           = "name";
    public final static String KEY_NAMESPACE        = "geoserverNamespace";
    public final static String KEY_USER             = "user";
    public final static String KEY_PASSWORD         = "password";
    public final static String KEY_ATTRIBUTE        = "attribute";
    public final static String KEY_VALUE            = "value";
    public final static String KEY_TABLE            = "table";
    public final static String KEY_FILTER           = "filter";
    public final static String KEY_SERVER           = "serverId";

    /**
     * The primary identifier of the asset.
     * 
     * @return
     */
    String getUrn();

    /**
     * 
     * @return
     */
    String getType();

    /**
     * 
     * @param key
     * @return
     */
    String getAttribute(String key);
}
