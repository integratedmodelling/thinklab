/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.api.knowledge;

import java.io.File;
import java.util.Collection;

import org.integratedmodelling.exceptions.ThinklabException;

/**
 * Ontologies are not first-class objects in Thinklab. This interface isn't deprecated at the moment, but
 * it may go away any time. All retrieval of concepts should be done through INamespace.
 */
public interface IOntology extends IResource {

    /**
     * Iterate over all concepts
     * @return an iterator over all the concepts contained in the ontology. 
     */
    Collection<IConcept> getConcepts();

    /**
     * Iterate over all properties
     * @return an iterator over all the properties contained in the ontology. 
     */
    Collection<IProperty> getProperties();

    /**
     * Return a concept, or null if not found.
     * @param ID the concept's ID
     * @return the concept or null
     */
    IConcept getConcept(String ID);

    /**
     * Return a property, or null if not found.
     * @param ID the property's ID
     * @return the property or null
     */
    IProperty getProperty(String ID);

    /**
     */
    @Override
    String getURI();

    /**
     * Write the ontology to the passed physical location.
     * 
     * @param uri
     * @throws ThinklabException 
     */
    boolean write(File file) throws ThinklabException;

    /**
     * Define the ontology from a collection of axioms. Must work incrementally.
     * @param axioms
     * @return a list of error messages if any happened. Should not throw exceptions.
     * 
     * TODO provide a quicker define(Axiom ... axioms) 
     * 
     * @throws ThinklabException
     */
    Collection<String> define(Collection<IAxiom> axioms);

    /**
     * Return the number of (named, useful) concepts, hopefully quickly. 
     * 
     * @return
     */
    int getConceptCount();

    /**
     * Return the number of (named, useful) properties, hopefully quickly.
     * 
     * @return
     */
    int getPropertyCount();

    /**
     * We provide a way for ontologies to alias an external concept. This is needed so that we can internalize
     * concepts from outside authorities that need to remain interoperable across applications. getConcept() should
     * return the aliased concept if it's available.
     * 
     * @param id
     * @return
     */
    IKnowledge getAlias(IKnowledge id);
}
