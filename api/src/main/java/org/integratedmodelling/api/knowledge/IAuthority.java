/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.api.knowledge;

import java.util.List;

import org.integratedmodelling.api.metadata.IMetadata;
import org.integratedmodelling.exceptions.ThinklabValidationException;

/**
 * An authority processes an ID into an identity. It must have a unique name and correspond to
 * the "official" view of a controlled vocabulary or repository of unique IDs that correspond
 * to unambiguous identities. The Thinklab counterpart is a trait concept that can be used to
 * identify any fundamental observable.
 * 
 * @author Ferd
 *
 */
public interface IAuthority {

    /**
     * Simple and unique string. Will be shown to users and selected by them. All uppercase is the convention.
     * 
     * @return
     */
    String getAuthorityId();

    /**
     * Return a trait corresponding to the identifier recognized by the authority. Should validate
     * the ID and throw an exception if it's not recognized.
     * 
     * @param id
     * @return
     * @throws ThinklabValidationException
     */
    IConcept getIdentity(String id) throws ThinklabValidationException;

    /**
     * Return metadata for a given textual query. Use DC ontology identifiers in the metadata as
     * specified in {@link IMetadata}.
     * 
     * @param query
     * @return
     */
    List<IMetadata> search(String query);

    // /**
    // * Identify the concept. If the passed concept is a trait, it will be made a subclass of the
    // * identity. If it's an observable, it will be identified with the identity and the result will
    // * be returned.
    // *
    // * @param observable
    // * @return
    // * @throws ThinklabValidationException
    // */
    // IConcept identify(IConcept identity) throws ThinklabValidationException;

    /**
     * Provide a longish description of what this authority does.
     * 
     * @return
     */
    String getDescription();

    /**
     * True if search() is going to be returning anything useful. If this returns false, no
     * search will ever be attempted.
     * 
     * @return
     */
    boolean canSearch();

    /**
     * Passed the concept being aliased to the identity passed. If this is OK return null; otherwise
     * return an error message to be displayed to the user. Do not throw exceptions.
     * 
     * @param knowledge
     * @return
     */
    String validateCoreConcept(IKnowledge knowledge, String id);
}
