/*******************************************************************************
 *  Copyright (C) 2007, 2014:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.api.knowledge;

import org.integratedmodelling.api.lang.IMetadataHolder;

/**
 * Basic methods that any knowledge resource must implement.Resources
 * are metadata holders, which is the way they publish their annotation
 * properties - no annotation property methods such as getLabel is 
 * provided anymore.
 * 
 * @author  Ferdinando Villa, Ecoinformatics Collaboratory, UVM
 */
public interface IResource extends IMetadataHolder {

    /**
     * The ID of the concept space (the first field or the semantic type). All Resources have (or ARE) a concept space.
     * @return  the concept space ID. Can't fail.
     */
    public String getConceptSpace();

    /**
     * The fully specified URI of the resource. All resources have a URI.
     * @return  the URI. Can't fail.
     */
    public String getURI();

    /**
     * Return the ontology this comes from (or is).
     */
    public abstract IOntology getOntology();

}
