/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.api.knowledge;

import java.util.Collection;

/**
 * IKnowledge defines the methods that are common to both IConcept and IProperty. As IConcept can be both concrete and abstract, and IInstances are always concrete, IKnowledge has all the methods that pertain to IRelationships of this with other IKnowledge objects. 
 * TODO consider adding specialization and generalization methods to specialize for concepts and properties.
 * @author  Ferdinando Villa
 */
public interface IKnowledge extends IResource, ISemantic {

    /**
     * All IKnowledge objects have a local name 
     * @return
     * @uml.property  name="localName"
     */
    String getLocalName();

    /**
     * True if this is subsumed by the passed resource.
     */
    boolean is(ISemantic concept);

    /**
     * The notion of an abstract concept is important where we create instances that have implementations. 
     * @return true if concept is abstract (no instances can be created).
     */
    boolean isAbstract();

    /**
     * Get the semantic closure in the current environment (may be using a reasoner or
     * not).
     * 
     * @return
     */
    Collection<IKnowledge> getSemanticClosure();

    /**
     * Return the domain of the owning namespace, or null if the 
     * knowledge is not part of any domain.
     * 
     * @return
     */
    IConcept getDomain();

}
