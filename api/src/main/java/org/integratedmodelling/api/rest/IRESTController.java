/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.api.rest;

import org.integratedmodelling.api.auth.IUser;
import org.integratedmodelling.exceptions.ThinklabException;

/**
 * Generic interface for a REST controller that will appropriately insert authorization tokens and
 * anything necessary to handle the target endpoints. Supports get() and post() with generic arguments.
 * Allows the various IEngine classes to be implemented generically in the common package and simply
 * specialized in the client. 
 * 
 * @author ferdinando.villa
 *
 */
public interface IRESTController {

    /**
     * Base URL of the target service.
     * 
     * @return
     */
    String getUrl();

    /**
     * Call a GET service at the target.
     * @param endpoint
     * @param args
     * @return
     * @throws ThinklabException
     */
    Object get(String endpoint, Object... args) throws ThinklabException;

    /**
     * Call a POST service at the target.
     * 
     * @param endpoint
     * @param args
     * @return
     * @throws ThinklabException
     */
    Object post(String endpoint, Object... args) throws ThinklabException;

    /**
     * Get the user we're acting for. Will be null in node controllers.
     * 
     * @return
     */
    IUser getUser();

    /**
     * Ask the quickest possible question to answer, and return whether there was a response.
     * CAUTION: this can take a long time to answer false, particularly on Windows.
     * 
     * @return
     */
    boolean responds();

    /**
     * Connect this security context to our endpoint, storing and returning a security key. Return
     * null without error if connection fails for any reason.
     * 
     * @return
     */
    String connect();

    /**
     * Get a security context that will communicate with this node as the current 
     * user.
     * 
     * @param url
     * @param key
     * @return
     */
    IRESTController forNode(String url, String key);

}
