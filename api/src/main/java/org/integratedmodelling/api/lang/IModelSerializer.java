/*******************************************************************************
 *  Copyright (C) 2007, 2014:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.api.lang;

import java.io.File;

import org.integratedmodelling.api.modelling.INamespace;
import org.integratedmodelling.exceptions.ThinklabException;

/**
 * If a IModelParser is also a IModelSerializer, its translation services may be offered in an
 * "import" dialog in a GUI. Translation means that a namespace parsed from a supported language can be written to the
 * "official" Thinklab language, which may be implementation defined but in the existing implementations
 * is assumed to be TQL.
 * 
 * @author Ferd
 *
 */
public interface IModelSerializer {

    /**
     * Write the namespace to the given output file.
     * 
     * @param namespace
     * @param outputFile
     * @throws ThinklabException
     */
    public abstract void writeNamespace(INamespace namespace, File outputFile) throws ThinklabException;
}
