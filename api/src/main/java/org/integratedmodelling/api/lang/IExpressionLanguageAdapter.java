/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
// package org.integratedmodelling.api.lang;
//
// import org.integratedmodelling.api.knowledge.IConcept;
// import org.integratedmodelling.api.knowledge.IExpression;
// import org.integratedmodelling.api.modelling.IFunctionCall;
// import org.integratedmodelling.api.modelling.INamespace;
//
/// **
// * Objects of this class are used to turn language instructions into expressions that
// * can be compiled and run. The IResolver will return one of these for the namespace
// * it's handling; language specifications in the namespace definition may be used to
// * select specific languages, but there should be a default that works with all
// * namespaces.
// *
// * @author Ferd
// *
// */
// public interface IExpressionLanguageAdapter {
//
// /**
// * Return an expression that will evaluate to true.
// *
// * @return
// */
// IExpression getTrueExpression();
//
// /**
// * Create an expression that is aware of the call context pertaining to the
// * passed domains (those are the DomainConcept of the extents over which the
// * expression is evaluated).
// *
// * @param expression
// * @param domain
// * @return
// */
// IExpression getExpression(String expression, INamespace namespace, IConcept... domain);
//
// /**
// * Return an expression that will call the function as passed when evaluated.
// *
// * @param processFunctionCall
// * @return
// */
// IExpression compileFunctionCall(IFunctionCall processFunctionCall);
//
// /**
// * Return an expression that will return the passed object when evaluated.
// *
// * @param processLiteral
// * @return
// */
// IExpression compileObject(Object processLiteral);
//
// }
