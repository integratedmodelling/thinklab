/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.api.lang;

import org.integratedmodelling.api.modelling.INamespace;
import org.integratedmodelling.api.project.IProject;
import org.integratedmodelling.exceptions.ThinklabException;

/**
 * The parsing context for the k.IM language. One is passed to any parsing call and used to 
 * keep track of context and influence the resulting beand.
 * 
 * @author Ferd
 *
 */
public interface IParsingContext {

    /**
     * 
     * @param message
     * @param line
     */
    void warning(String message, int line);

    void info(String message, int line);

    void error(String message, int line);

    boolean isWithin(int type);

    IProject getProject();

    INamespace getNamespace();

    long getTimestamp();

    IParsingContext get(int type);

    IParsingContext get(IProject project);

    /**
     * This one opens the resource, and if a namespace ID is not set by an upstream context, defined
     * the namespace ID based on the resource name. Implementations must ensure that the resource is
     * closed when this context is popped from the stack.
     * 
     * @param resource
     * @return
     * @throws ThinklabException
     */
    IParsingContext forResource(Object resource) throws ThinklabException;

    /**
     * Define the namespace ID that will be parsed downstream. This will disable detection of
     * namespace ID from the resource that will follow.
     * 
     * @param namespaceId
     * @return
     */
    IParsingContext forNamespace(String namespaceId);

    /**
     * True if this context has parsed or is parsing said namespace. Used to avoid needless
     * recursion in project loading.
     * 
     * @param namespaceId
     * @return
     */
    boolean hasSeen(String namespaceId);

}
