/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.api.network;

import org.integratedmodelling.api.auth.IResourceCatalog;
import org.integratedmodelling.api.engine.IRemoteEngine;

/**
 * A INode is a connected Thinklab server in a INetwork. All the nodes in a network have an identical
 * INetwork object that gives access to all nodes. Each server that contains a INetwork will have at
 * least the "self" INode in it.
 * 
 * @author ferdinando.villa
 *
 */
public interface INode extends IRemoteEngine {

    /**
     * The server ID is unique within the INetwork. It should be the same as
     * IEngine.getName() but with the uniqueness constraint enforced.
     * 
     * @return
     */
    String getId();

    /**
     * This identifies a node as the primary server (which hosts an authentication service that
     * recognized us and may provide other services). It will only return true on personal engines
     * and for one node in the network. The primary server URL is in the user certificate.
     * 
     * @return
     */
    boolean isPrimaryNode();

    /**
    * Return the resources available through this node. Only used at client side, filtered
    * through user privileges.
    * 
    * @return
    */
    IResourceCatalog getResourceCatalog();

    /**
     * Nodes should be polled for active status and transparently added/dropped as their status
     * changes.
     * 
     * @return
     */
    boolean isActive();

    /**
     * The key that we must know in order to demonstrate that we can use this node and how. 
     * Communicated by the server itself upon authentication.
     * 
     * @return
     */
    String getKey();

    /**
     * Optional description from certificate. Please return an empty string, not null, if
     * there's no description.
     * 
     * @return
     */
    String getDescription();

}
