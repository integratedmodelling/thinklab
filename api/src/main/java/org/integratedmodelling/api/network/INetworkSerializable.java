package org.integratedmodelling.api.network;

import org.integratedmodelling.api.modelling.IModelBean;

/**
 * Implement this one to return a bean that can be sent and reconstructed through REST. The
 * ModelFactory methods will do the rest.
 * 
 * @author ferdinando.villa
 *
 */
public interface INetworkSerializable {

    /*
     * TODO change to adapt() when we factor out the deprecated IRemoteSerializable interface.
     */
    IModelBean getBean();
}
