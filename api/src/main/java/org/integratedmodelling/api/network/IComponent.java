/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.api.network;

import java.util.Collection;

import org.integratedmodelling.Version;
import org.integratedmodelling.api.services.IPrototype;

/**
 * A Component is a set of functionalities available in a node. Each node can rank its willingness to provide
 * its components in a scale 0-1. Components make IPrototytpes available for all their API. They are declared
 * using the Component annotation.
 * 
 * @author ferdinando.villa
 *
 */
public interface IComponent {

    /**
     * Obligatory, unique ID.
     * @return
     */
    String getId();

    /**
     * The API, as a set of prototypes for all the services provided by this component.
     * 
     * @return
     */
    Collection<IPrototype> getAPI();

    /**
     * Called to establish if the component has been properly initialized and is ready to be used.
     * 
     * @return
     */
    boolean isActive();

    /**
     * All components this one depends on. The components must be installed in the
     * same engine.
     * 
     * @return
     */
    Collection<String> getPrerequisiteComponents();

    /**
     * All components this one depends on. The components must be installed in the
     * same engine.
     * 
     * @return
     */
    Collection<String> getPrerequisiteProjects();

    /**
     * The component's version.
     * 
     * @return
     */
    Version getVersion();

    /**
     * Date of last modification of component. Important for synchronization, because 
     * components may be large we don't want to download everything.
     * 
     * @return
     */
    long lastModified();

    /**
     * The class that may contain initialization and setup methods for the component.
     * Will never be null in the current implementation, but may be empty.
     * 
     * @return
     */
    Class<?> getExecutor();

}
