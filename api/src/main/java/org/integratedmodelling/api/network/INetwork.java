/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.api.network;

import java.util.Collection;

import org.integratedmodelling.api.auth.IResourceCatalog;
import org.integratedmodelling.api.auth.IUser;
import org.integratedmodelling.api.monitoring.IMonitor;
import org.integratedmodelling.api.services.IPrototype;
import org.integratedmodelling.api.services.IServiceCall;
import org.integratedmodelling.exceptions.ThinklabException;

/**
 * Each server builds a network by a mutual certificate sharing process. The hand-shaking between two connected
 * nodes notifies capabilities, components and permissions. A network is a maximally connected, undirected 
 * graph of INodes.
 * 
 * Users authenticate on one node only, and the nodes do not share authentication information. The various
 * capabilities are allowed based only on groups, which are shared across the network.
 * 
 * Embedded servers CONNECT to a network through their primary server if allowed, but are not part of it; only
 * publicly reachable ones are. 
 * 
 * @author ferdinando.villa
 *
 */
public interface INetwork {

    /**
     * Return all the nodes in a network. Within a server, one of the nodes always represents
     * self.
     * 
     * @return
     */
    Collection<INode> getNodes();

    /**
     * Get the node for 
     * @param s
     * @return
     */
    INode getNode(String s);

    /**
     * Return a URL to synchronize a given project, looking up a node that serves it authoritatively 
     * and asking to export it.
     * 
     * @param projectId
     * @return
     * @throws ThinklabException 
     */
    String getUrlForProject(String projectId) throws ThinklabException;

    /**
     * Send the command to all nodes that provide this component in the network and return a merged result.
     * 
     * @param command
     * @return
     * @throws ThinklabException
     */
    Object broadcast(IServiceCall command, IMonitor monitor) throws ThinklabException;

    /**
     * Return the key that identifies this network to anyone wanting to connect with it. If
     * this is a personal engine, return the key for the primary server.
     * 
     * TODO this should be callable with another key as parameter, allowing to check for 
     * authorization for the caller.
     * 
     * @return
     */
    String getKey();

    /**
     * Networks are dynamic as nodes may go up and down in time. This should return true when the
     * network has changed, and may be called by a thread any time. After checking for changes, it
     * should reset the status to unchanged until another event happens.
     * 
     * @return
     */
    boolean hasChanged();

    /**
     * The URL for the current instance if public, or that of the primary server if
     * personal. Client networks return the URL of the personal engine.
     * 
     * @return
     */
    String getUrl();

    /**
     * If true, this is a personal engine which only reads a public network.
     * 
     * @return
     */
    boolean isPersonal();

    /**
     * true if one or more remote nodes provide the named component.
     * 
     * @param string
     * @return
     */
    boolean providesComponent(String string);

    /**
     * Find the prototype for a service published by any connected node. Return null if not
     * found.
     * 
     * @param id
     * @return
     */
    IPrototype findPrototype(String id);

    /**
     * Get the URL of the authentication endpoint for this network.
     * 
     * @return
     */
    String getAuthenticationEndpoint();

    /**
     * On a public engine, get the published resource catalog for the engine. On a personal engine, get the catalog of all
     * shared resources and components published along the network and visible by the connected
     * user.
     * 
     * @return
     */
    IResourceCatalog getResourceCatalog();

    /**
     * On a public engine, return the groups that are authorized to submit observations to this node.
     * 
     * @return
     */
    Collection<String> getAuthorizedSubmitters();

    /**
     * A personal network (either on a client or on a personal engine) will always return
     * a valid user. A public network will return null.
     * 
     * @return
     */
    IUser getUser();

    /**
     * Return the node that provides the passed service, or null. Will just scan the nodes until one
     * responds. TODO we may need some prioritization strategy when more than one node provide the
     * same service.
     * 
     * @param id
     * @return
     */
    INode getNodeProviding(String id);

    /**
     * True if we are authenticated and connected.
     * 
     * @return
     */
    boolean isOnline();

}
