/*******************************************************************************
 *  Copyright (C) 2007, 2014:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.api.monitoring;

/**
 * Collects the message types sent around through IMonitor.send().
 * 
 * @author ferdinando.villa
 *
 */
public class Messages {

    public static final String TASK_STARTED             = "TASK_STARTED";
    public static final String TASK_FAILED              = "TASK_FAILED";
    public static final String TASK_FINISHED            = "TASK_FINISHED";
    public static final String TASK_INTERRUPTED         = "TASK_INTERRUPTED";
    public static final String TIME_TRANSITION          = "TIME_TRANSITION";
    public static final String ENGINE_BOOTING           = "ENGINE_BOOTING";
    public static final String ENGINE_AVAILABLE         = "ENGINE_AVAILABLE";
    public static final String ENGINE_STOPPED           = "ENGINE_STOPPED";
    public static final String ENGINE_EXCEPTION         = "ENGINE_EXCEPTION";
    public static final String ENGINE_STATUS            = "ENGINE_STATUS";

    // these below are for the task introspector.
    public static final String CURRENT_PROVENANCE_GRAPH = "CURRENT_PROVENANCE_GRAPH";
    public static final String CURRENT_WORKFLOW_GRAPH   = "CURRENT_WORKFLOW_GRAPH";

}
