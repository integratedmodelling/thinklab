/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.api.monitoring;

import org.integratedmodelling.api.runtime.ISession;

/**
 * Glorified listener with control capabilities. A monitor is passed to observe(). If it also implements
 * any other listener interfaces, these will also be honored appropriately.
 * 
 * @author Ferd
 *
 */
public interface IMonitor {

    /**
     * Pass a string. Will also take an exception, but usually exceptions shouldn't turn into warnings.
     * These will be reported to the user unless the verbosity is set lowest.
     * 
     * @param o
     */
    void warn(Object o);

    /**
     * Pass a string. Will also take an exception, but usually exceptions shouldn't turn into warnings.
     * These will be reported to the user unless the verbosity is set low. Do not abuse of these -
     * there should be only few, really necessary info messages so that things do not get lost. The
     * class parameter is used by the client to categorize messages so they can be shown in special
     * ways and easily identified in a list of info messages. You can leave it null or devise your own 
     * class.
     * 
     * @param o
     */
    void info(Object info, String infoClass);

    /**
     * Pass a string or an exception (usually the latter as a reaction to an exception in the execution). 
     * These will interrupt execution from outside, so you should return after raising one of these.
     * 
     * @param o
     */
    void error(Object o);

    /**
     * Any message that is just for you or is too verbose to be an info message should be sent as
     * debug, which is not shown by default unless you enable a higher verbosity. Don't abuse of 
     * these, either - it's still passing stuff around through REST so it's not cheap to show a 
     * hundred messages.
     *  
     * @param o
     */
    void debug(Object o);

    /**
     * In most circumstances, this can be called with a Markdown-formatted string that will be added
     * to the "report" for the session and/or context. The report is meant to document the operations of
     * the software extensively and readably - it is not a log and client may offer to publish it as 
     * html or pdf for external users. It is expected to include explanations on assumptions, data 
     * sources, etc. The attachments may be images or files that are references as local links in the
     * markdown string.
     * 
     * @param markdown
     */
    void report(String markdown, Object... attachments);

    /**
     * This is called from the OUTSIDE as a reaction to a user action. You should not call this, but
     * simply raise an error and return if you encounter an error condition.
     * 
     * @param o
     */
    public void stop(Object o);

    /**
     * When this returns true, the user has asked to interrupt the process. You should call it 
     * regularly at strategic points in your program and react appropriately if it ever returns
     * true. The only processes that can dispense with checking it are those that are guaranteed
     * to run to completion quickly.
     * 
     * @return
     */
    boolean isStopped();

    /**
     * Call this once before the processing starts to define how many steps your process will
     * take. You are required to report a total of n steps using addProgress() after you do this
     * unless you raise an error. 
     * 
     * @param n
     */
    void defineProgressSteps(int n);

    /**
     * Add the given amount of steps to the progress monitor. The total reported in normal
     * processing should add up to the parameter passed to defineProgressSteps and never remain
     * below it unless errors are thrown. Descriptions are optional (pass a null) but they can
     * be used to document "milestones" in execution, they just work like info messages with a
     * special class.
     * 
     * @param steps
     * @param description
     */
    void addProgress(int steps, String description);

    /**
     * Send a message with special meaning. Used to communicate events that would normally
     * use a listener but need to be passed between client and server. Messages are not
     * free-format but should come from a known list of constants (see Messages in API package).
     * 
     * @param message
     * @param args
     */
    void send(INotification notification);

    /*
     * pass INotification.DEBUG, INotification.INFO or INotification.ERROR. Only for CLIENT use.
     */
    void setLogLevel(int level);

    /*
     * return one of INotification.DEBUG, INotification.INFO or INotification.ERROR, taken from
     * the appropriate preferences and with sensible defaults. Only for CLIENT use.
     */
    int getDefaultLogLevel();

    /*
     * true if at least one error has been reported
     */
    boolean hasErrors();

    /**
     * Monitors may be associated to a task. If not, this will return -1.
     * 
     * @return
     */
    long getTaskId();

    /**
     * If the monitor publishes a task introspector, it will send/receive any info directed to it.
     * 
     * @return
     */
    ITaskIntrospector getTaskIntrospector();

    /**
     * If the monitor is specific of a session, return it; may be null.
     * 
     * @return
     */
    ISession getSession();
}
