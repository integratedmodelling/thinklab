/*******************************************************************************
 *  Copyright (C) 2007, 2014:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.api.monitoring;

import java.io.File;

import org.integratedmodelling.api.project.IProject;

/**
 * Project listener. Register these with the Project Manager.
 * 
 * @author Ferd
 *
 */
public interface IProjectLifecycleListener {

    /*
     * 
     */
    void onReload(boolean full);
    
    /**
     * 
     * @param project
     */
    void projectRegistered(IProject project);

    /**
     * 
     * @param project
     */
    void projectUnregistered(IProject project);

    // /**
    // *
    // * @param project
    // */
    // void projectOpened(IProject project);
    //
    // /**
    // *
    // * @param project
    // */
    // void projectClosed(IProject project);

    /**
     * 
     * @param ns
     * @param project
     */
    void namespaceModified(String ns, IProject project);

    /**
     * 
     * @param ns
     * @param project
     */
    void namespaceAdded(String ns, IProject project);

    /**
     * 
     * @param ns
     * @param project
     */
    void namespaceDeleted(String ns, IProject project);

    /**
     * Call when project properties were modified. This may have changed anything related to the 
     * project, primarily its prerequisites.
     * 
     * @param file The file that was modified. Currently it can be thinklab.properties or the .project file
     * that Eclipse uses (we reserve that for Eclipse to simplify the client code).
     * 
     * @param properties
     */
    void projectPropertiesModified(IProject project, File file);

    /**
     * Any non-namespace, non-properties file created, including directories.
     * @param project 
     * 
     * @param file
     */
    void fileCreated(IProject project, File file);

    /**
     * Any non-namespace, non-properties file deleted, including directories.
     * @param project 
     * 
     * @param file
     */
    void fileDeleted(IProject project, File file);

    /**
     * Any non-namespace, non-properties file modified, including directories.
     * @param project 
     * 
     * @param file
     */
    void fileModified(IProject project, File file);
}
