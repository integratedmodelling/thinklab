/*******************************************************************************
 *  Copyright (C) 2007, 2014:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.api.modelling.agents;

import java.util.Map;

import org.integratedmodelling.api.knowledge.IProperty;
import org.integratedmodelling.api.modelling.IDirectActiveObservation;
import org.integratedmodelling.api.modelling.IState;
import org.integratedmodelling.api.time.ITimeInstant;
import org.integratedmodelling.api.time.ITimePeriod;
import org.integratedmodelling.exceptions.ThinklabException;

/**
 * a temporary class to describe what is being called "agent-state" in the multiple-scale paper. It represents
 * a state (which can be a constant value, temporal function, or probability over either) valid for some time
 * period.
 * 
 * @author luke
 * 
 */
public interface IAgentState {

    IDirectActiveObservation getSubject();

    Map<IProperty, IState> getStates();

    ITimePeriod getTimePeriod();

    /**
     * accept a collision time as a termination time. Normally, the only thing to do is set the end-time for
     * the agent-state, but this method allows agent-states to perform other cleanup or logic when this
     * happens.
     * 
     * NOTE: This is NOT where the agent processes a collision. The collision is considered to happen
     * "the moment immediately following" this agent-state end time.
     * 
     * @param interruptionTime
     * @return
     * @throws ThinklabException
     */
    IAgentState terminateEarly(ITimeInstant interruptionTime) throws ThinklabException;

}
