/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.api.modelling;

import java.util.List;

import org.integratedmodelling.api.knowledge.IConcept;
import org.integratedmodelling.api.lang.IParseable;
import org.integratedmodelling.collections.MultidimensionalCursor;
import org.integratedmodelling.exceptions.ThinklabException;
import org.integratedmodelling.lang.LogicalConnector;

/**
 * A Scale is an immutable list of the Extents seen by a Subject, which can be compared with others
 * topologically. When the extents include time, Scales can be discretized into Transitions, which in the eye
 * of a Subject/Process may trigger Events. Those are merged in ISchedules for contextualization.
 *
 * The IExtents must be automatically internally sorted in an appropriate and stable order for
 * contextualization.
 *
 * A IScale implementation must be properly hashable and comparable in order to be added/removed to/from
 * ISchedule and used to understand transitions.
 * 
 * Extent offsets are constrained to ints for individual extents, so it will be safe to convert an offset
 * to int if it's computed on a slice along an extent dimension. The overall multiplicity is a long.
 *
 * @author Ferd
 */
public interface IScale extends IObservationTopology, ITopology<IScale> {

    /**
     * Simple object to indicate a dimension or an extent along it. Used in getIndex() and
     * locate(). 
     */
    public interface Locator extends IParseable {

        /**
         * Should be a constant but no way to ask for that in an interface. Number of 
         * dimension offsets to locate one extent.
         * 
         * @return
         */
        public int getDimensionCount();

        /**
         * If true, this is locating a full dimension or subset, with multiple extents.
         * 
         * @return
         */
        public boolean isAll();

        /**
         * If the locator only covers the granule partially, return a value less than
         * one, reflecting the amount of active coverage. This will only return anything other than 1 when
         * computed by a IState.Mediator, which matches two scales and may find partial coverage
         * when checking one index against another. It should normally return 1 and never return
         * 0.
         * 
         * @return
         */
        public double getWeight();

    }

    /**
     * Returned by getIndex, it returns offset indices along only one
     * of the scale extents. It's also capable of checking if the correspondent
     * location is masked (i.e. not really part of the topology) or active.
     * 
     * @author Ferd
     *
     */
    public interface Index extends List<Integer> {

        /**
         * true if the index is browsing through a spatial extent.
         * @return
         */
        boolean isSpatial();

        /**
         * true if the index is browsing through a temporal extent.
         * @return
         */
        boolean isTemporal();

        /**
         * Return the domain concept of the extent we're indexing.
         * @return
         */
        IConcept getDomainConcept();

        /**
         * Get the fixed offsets of all the extents. The one we're browsing will be < 0.
         * @return
         */
        int[] getOffsets();

        /**
         * Check if the passed offset (relative to this index's dimension) correspond
         * to a "live" observation when the extents have regular topologies but may have
         * inactive subdivisions.
         * 
         * @param offset
         * @return
         */
        boolean isActive(int offset);
    }

    /**
     * Merge all common extents from the given scale, using the force parameter to define how the extents are
     * merged (see IExtent.merge). Extents in common are merged according to the passed operator to compute
     * the merged extent. The adopt parameter controls whether extents in the passed scale that are not in the
     * original one appear in the result. All extents in the original scale will appear in the result.
     *
     * Must not modify the original scales.
     *
     * @param scale
     * @param force
     * @return
     * @throws ThinklabException
     */
    IScale merge(IScale scale, LogicalConnector how, boolean adopt) throws ThinklabException;

    /**
     * Get an index to loop over one dimension (set as -1) given fixed position for all others.
     * 
     * @param dimensions
     * @return
     */
    Index getIndex(Locator... locators);

    /**
     * Get an index to loop over one dimension (set as -1) given fixed position for all others, only
     * considering the sliceIndex-th part of the field from a total number of slices = sliceNumber. Used
     * for parallelization of loops.
     * 
     * @param dimensions
     * @return
     */
    Index getIndex(int sliceIndex, int sliceNumber, Locator... locators);

    /**
     * Take an array of objects that can locate a position in each extent, using the order of the extents
     * in the scale, and return the overall offset of the correspondent state (or -1 if not compatible). The
     * objects will be coordinates in the native reference system of each extent, and should be interpreted
     * correctly; each extent may take one or more objects from the list according to which object is passed
     * (e.g. a spatial point vs. lat and lon doubles).
     * 
     * @param locators
     * @return
     */
    long locate(Locator... locators);

    /**
     * Get a scale that has either a 1-dimensional extent for the passed concept or doesn't have the
     * extent at all (if offset < 0). Ensure this scale remembers its offset and previous multiplicity
     * along the extent's dimension so that it will respond properly to getOffset() below.
     * 
     * TODO Possible improvement: allow passing an IExtent instead of an int, to accommodate variable
     * scales and ease the use of the API in some circumstances. That will require a getExtentOffset(IExtent)
     * method.
     * 
     * @param extent
     * @param offset
     * @return
     */
    IScale getSubscale(IConcept extent, int offset);

    /**
     * Call it on a scale returned by getSubscale to reapply the original offset and obtain the one
     * corresponding to the same granule on the full scale it derives from. If called on a full scale
     * just return the passed offset.
     * 
     * @param subscaleOffset
     * @return
     */
    long getOriginalOffset(long subscaleOffset);

    /**
     * Take in another scale and complete what's left of our specs by merging in its
     * details. E.g., we're a bounding box, we get a grid without extent, and we become
     * a grid in that bounding box. Will only be called during resolution, so the 
     * queries should have selected compatible scales, but throw an exception if
     * anything is not compatible.
     * 
     * @param scale
     * @return
     * @throws ThinklabException
     */
    IScale harmonize(IScale scale) throws ThinklabException;

    /**
     * Get a properly initialized cursor to ease navigating the extents and dimensions according to
     * each extent's inherent dimensionality.
     * @return
     */
    MultidimensionalCursor getCursor();

    /**
     * Get the offset in the specified extent that correspond to the overall offset passed.
     *  
     * @param extent
     * @param overallOffset
     * @return
     */
    int getExtentOffset(IExtent extent, int overallOffset);

    /**
     * Get the individual extent offsets corresponding to the overall offset passed.
     * 
     * @param overallOffset
     * @return
     */
    int[] getExtentIndex(int overallOffset);

    /**
     * True if we have time AND it will determine more than a single state. It's also
     * in IObservation, but it's convenient to duplicate it here too.
     * 
     * @return
     */
    boolean isTemporallyDistributed();

    /**
     * True if we have space AND it will determine more than a single state. It's also
     * in IObservation, but it's convenient to duplicate it here too.
     * 
     * @return
     */
    boolean isSpatiallyDistributed();

    /**
     * 
     * @param offset
     * @return
     */
    boolean isCovered(int offset);

    /**
     * Check that all extents are consistent and meaningful - e.g. empty intervals, degenerate shapes etc.
     * 
     * @return
     */
    boolean isConsistent();

}
