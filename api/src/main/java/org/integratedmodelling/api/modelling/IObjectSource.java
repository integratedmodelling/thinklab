/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.api.modelling;

import java.util.List;

import org.integratedmodelling.exceptions.ThinklabException;

public interface IObjectSource {

    /**
     * Return the list of objects in this scale. A IReifiableSubject is a data structure that allows subjects to be
     * created as required.
     * 
     * @param scale
     * @return
     */
    public abstract List<IReifiableObject> getObjects(IScale scale);

    /**
     * Get the scale for this object source.
     * @return
     */
    IScale getCoverage();

    /**
    * Return an accessor that will observe the quality defined by the observer over the
    * totality of the subjects visible over the passed scale. This is a de-reifying
    * operation that will turn an aspect of the objects into a quality for another
    * subject. Must be passed the attribute of the objects that serves as the basis
    * for the conversion. Some attribute IDs (e.g. for presence/absence) may be
    * predefined by the infrastructure.
    *
    * @param attribute
    * @param observer
    * @param context
    * @param monitor
    * @return
    * @throws ThinklabException
    */
    public abstract IFunctionCall getDatasourceCallFor(String attribute, INamespace namespace);

    /**
     * Yes if source is online. Used at resolution.
     * 
     * @return
     */
    public boolean isAvailable();

}
