/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.api.modelling;

/**
 * Reifiable objects are returned by {@link IObjectSource} and used in reification
 * models. They're the "dumb data" of objects - have scale and can match attribute
 * values to names, but don't know what they or their attributes are.
 * 
 * @author Ferd
 *
 */
public interface IReifiableObject {

    /**
     * The ID is not used for anything user-accessible, but will be reported in error
     * messages so that the object can be recognized in its source.
     * 
     * @return
     */
    public String getId();

    /**
     * Each reified object has its own scale, coming from the source, which must have been
     * made compatible with the context scale passed.
     * 
     * @return
     */
    public IScale getScale(IScale parent);

    /**
     * Annotations may specify observed for string-specified attributes of the
     * object, which must be returned as POD data compatible with the observer
     * passed. Null is a fine return value if the attribute is unknown.
     *
     * Should ensure that any code in the observer (including contextualizers)
     * is honored. This isn't happening at the moment.
     *
     * @param attribute
     * @param observer
     * @return
     */
    public Object getAttributeValue(String attributeName, IObserver observer);
}
