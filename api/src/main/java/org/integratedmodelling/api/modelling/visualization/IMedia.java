/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.api.modelling.visualization;

import javax.activation.MimeType;

/**
 * IMedia is an abstract interface for the product of any kind of visualization (which may involve other
 * media than just visual).
 * 
 * @author Ferd
 *
 */
public interface IMedia {

    /**
     * These types look quite silly, but because media are for an observation,
     * they sort of help saying what we want in a more structured way than
     * MIME or a silly string. Of course they need further specification or
     * observation type-dependent defaults before they're translated into
     * a media artifact.
     * 
     * @author ferdinando.villa
     *
     */
    enum Type {
        // map or plot
        IMAGE,
        // only really applicable to spatial and temporal states
        VIDEO,
        // may be anything, but returns scientific formats useful for analysis
        DATA,
        // when appropriate, an artifact that describes the observation, e.g. report or presentation
        FILE
    }

    /**
     * MIME type for the media.
     * @return
     */
    MimeType getMIMEType();

    /**
     * Additional info to interpret the media. May be null.
     * @return
     */
    ILegend getLegend();

    /**
     * Fit to a different viewport if possible, or return null.
     * 
     * @param viewport
     * @return
     */
    IMedia scale(IViewport viewport);

}
