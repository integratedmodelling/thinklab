/*******************************************************************************
 *  Copyright (C) 2007, 2014:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.api.modelling.visualization;

import java.util.Map;

import org.integratedmodelling.api.modelling.IObservation;
import org.integratedmodelling.api.modelling.IScale;

/**
 * The visualization factory produces media for an observation according to type and 
 * (optional) preferences. Should use caching sensibly as the same media may be requested
 * multiple times.
 * 
 * @author Ferd
 *
 */
public interface IVisualizationFactory {

    /**
     * Create the appropriate media for the passed observation and the MIME type requested, or null
     * if the media cannot be created. Used for visualization, export or anything else where observations
     * must be exported to other applications.
     * 
     * @param observation
     * @param index the slice of the data we want to visualize (e.g. a temporal or spatial snapshot of a T/S
     *        context). Pass null if not applicable (e.g. on a subject) or if all data are requested (which 
     *        of course must match the requested media type).
     * @param viewport the viewport (whatever that means for the media type). May be null even in media that
     *        need one, such as images - in that case, a default should be provided.
     * @param mimeType
     * @param options parameters that may influence the result. May be null.
     * 
     * @return the media, or null if the media cannot be produced.
     */
    public IMedia getMedia(IObservation observation, IScale.Index index, IViewport viewport, String mimeType,
            Map<String, Object> options);

}
