/*******************************************************************************
 *  Copyright (C) 2007, 2014:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.api.modelling.resolution;

import org.integratedmodelling.api.modelling.scheduling.ITransition;

/**
 * The result of compiling a IProvenance into a sequence of processing steps. Running the workflow
 * will compute initialized states for the subject it refers to. A IProvenance and the corresponding
 * IWorkflow constitute a step in a IResolutionStrategy.
 * 
 * Just a tag interface for now. Should be fleshed out when possible. Ideally a IWorkflow could
 * bridge to any scientific workflow system or other virtual machine.
 * 
 * @author Ferd
 *
 */
public interface IWorkflow  {
    
    boolean run(ITransition transition);

}
