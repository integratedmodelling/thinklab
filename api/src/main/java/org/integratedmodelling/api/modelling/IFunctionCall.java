/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.api.modelling;

import java.util.Map;

import org.integratedmodelling.api.knowledge.IConcept;
import org.integratedmodelling.api.services.IPrototype;

/***A function call.Must be able to resolve the function and return the result object when requested.*Validation of this object happens in the resolver.**IServiceCall is very similar but has some differences and is not a language element.For*now the two are independent.**

@author Ferd
 *
 */
public interface IFunctionCall extends ILanguageObject {

    /**
    * name given to a single parameter passed by itself, outside of a named list.
    */
    String DEFAULT_PARAMETER_NAME = "__default__";

    /**
    * this may be set as an argument value to flag that 'unknown' was passed as a parameter,
    * resulting in a null value that cannot be contained in the parameter map.
    */
    String NULL = "__null__";

    /**
     * ID of the function being called. Never null.
     * 
     * @return
     */
    String getId();

    /**
     * Prototype of the function. May be null also for valid functions if we have no connection to an engine.
     * 
     * @return
     */
    IPrototype getPrototype();

    /**
     * Parameters passed to the call. If a function call was passed, it is invoked before this
     * is returned.
     * 
     * @return
     */
    Map<String, Object> getParameters();

    /**
     * True if the return types stated in the prototype include a concept that is() the 
     * passed one.
     * 
     * @param resultType
     * @return
     */
    boolean returns(IConcept resultType);

    /**
     * Function calls come from a namespace.
     * 
     * @return
     */
    INamespace getNamespace();

}
