/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.api.modelling;

import java.util.Set;

import org.integratedmodelling.api.knowledge.IConcept;
import org.integratedmodelling.api.monitoring.IMonitor;
import org.integratedmodelling.exceptions.ThinklabException;

/**
 * An observer is the observation semantics of an observable quality. It is responsible for interpreting the
 * states that will describe the observable indirectly. 
 * 
 * Observers may generate a concept other than the stated observable to allow more intuitive specification
 * semantics. In all cases, Thinklab ensures that the semantics of the observation concept is consistent
 * and complete enough for validation of any further use in semantic mediation.
 * 
 */
public abstract interface IObserver extends IObservingObject {

    /**
     * Returns the core type that defines the observation this observer is making and how it matches the final
     * observable. The type should be specialized enough to allow correct reasoning and matching; for example,
     * for measurements it should specialize to include the physical dimensions of the observer (e.g.
     * LengthMeasurement) so that we can lookup compatible observations. It will end up in the IObservable
     * returned by getObservable().
     * 
     * @return
     */
    IConcept getObservationType();

    /**
     * Train the model to match any output state that can be observed in the kbox/context. Details of how to
     * choose or subset the output states are left to the implementation. Not all observer will be trainable -
     * if not trainable, this one should return the same model (with the option of logging a warning).
     * 
     * It should never modify the observer it's called on - it would be a const if there was such a thing in
     * Java.
     * @param monitor TODO
     * @param params
     * @return a new trained model that has learned to reproduce the output states observed on the passed
     * kbox.
     * @throws ThinklabException
     */
    IObserver train(ISubject context, IMonitor monitor) throws ThinklabException;

    /**
     * Return the model we're part of. Tried to avoid this forever, but visualization made it necessary.
     * 
     * @return
     */
    IModel getModel();

    /**
     * If this is true, the observer describes an integrated quality over the explicit scale given in the
     * definition (e.g. kgs of carbon per "cell" instead of per unit of area). Mediation and user validation
     * must use this information along with the semantics of the observable and the extent being observed to 
     * ensure correct results.
     * 
     * If this returns true, getAggregationDomains() will return the extent(s) over which the aggregation happened.
     * 
     * @return
     */
    boolean isAggregating();

    /**
     * This should return a non-empty set of extent concepts describing the extents of aggregation when isAggregating() 
     * returns true.
     * 
     * @return
     */
    Set<IConcept> getAggregationDomains();

}
