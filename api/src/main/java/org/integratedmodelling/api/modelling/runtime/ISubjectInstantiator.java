/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.api.modelling.runtime;

import java.util.Map;

import org.integratedmodelling.api.knowledge.IConcept;
import org.integratedmodelling.api.modelling.IObservable;
import org.integratedmodelling.api.modelling.IObservation;
import org.integratedmodelling.api.modelling.IState;
import org.integratedmodelling.api.modelling.ISubject;
import org.integratedmodelling.api.modelling.resolution.IResolutionContext;
import org.integratedmodelling.api.modelling.scheduling.ITransition;
import org.integratedmodelling.api.monitoring.IMonitor;
import org.integratedmodelling.exceptions.ThinklabException;

/**
 * Specialized contextualizer for models that generate subjects (the "model each x" type). These contextualizers
 * generate subject, which are later resolved by Thinklab, so their purpose is solely to produce the subjects
 * themselves in the context. They are the programmatic equivalent of an object source. After this one has 
 * created the subjects, Thinklab will find a model, possibly linked to an ISubjectContextualizer, to resolve 
 * each of them in the sequence returned by createSubjects().
 * 
 * @author ferdinando.villa
 *
 */
public interface ISubjectInstantiator extends IContextualizer {

    /**
     * Called at the beginning. Typically returns an empty collection as there are normally
     * no events when time has started, but for now we allow creating some initial ones (may
     * change). Passing a subject (never null) as the context for the events.
     * 
     * @param subject
     * @param process
     * @return
     */
    Map<String, IObservation> initialize(ISubject contextSubject, IResolutionContext context, IConcept subjectType, Map<String, IObservable> expectedInputs, Map<String, IObservable> expectedOutputs, IMonitor monitor)
            throws ThinklabException;

    /**
     * Called at transitions only. Any new subject will be resolved and contextualized as
     * the simulation progresses.
     * 
     * @param transition
     * @return
     */
    Map<String, IObservation> createSubjects(ITransition transition, Map<String, IState> inputs)
            throws ThinklabException;
}
