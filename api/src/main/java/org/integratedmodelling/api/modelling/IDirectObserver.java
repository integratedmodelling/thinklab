/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.api.modelling;

import java.util.List;

import org.integratedmodelling.collections.Pair;

/**
 * Base of the direct observation records defined with 'observe' in the language.
 * Specialized for subjects, events and processes.
 * 
 * The getChildren() method will return any other IDirectObservers defined inside this one. At the
 * moment the k.IM parser only allows one level of nesting.
 * 
 * @author ferdinando.villa
 *
 */
public abstract interface IDirectObserver extends IObservingObject {

    /**
     * Get the definition for any dependent states to be created. The object in each pair can only be
     * a literal value or a function call returning one. The literal must match the scale of the object.
     * 
     * @return
     */
    List<Pair<IObserver, Object>> getStatesDefinition();

    /**
     * Get the definitions for any dependent observer. Dependent observers may have any scale and
     * there's no requirement for matching anything with their parent.
     * 
     * @return
     */
    List<IDirectObserver> getChildrenDefinition();

}
