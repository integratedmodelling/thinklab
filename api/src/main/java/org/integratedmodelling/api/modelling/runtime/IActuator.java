/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.api.modelling.runtime;

import java.util.List;

import org.integratedmodelling.api.modelling.IAction;

/**
 * Accessors give access to the raw values of observations and are used to process their data or objects. A model
 * may have zero or more accessors; all state models have an accessor, not all subject models need to. When an accessor
 * is specified in the language, it is chained to the "natural" accessor for state models, and it's used as the 
 * natural accessor in subject models, which do not have one by default.
 * 
 * IStateAccessors operate serially in a context, being passed each state in the sequence determined by the context's internal 
 * topologies. State models always have a default accessor and may be chained to a user-provided one for further processing.
 * 
 * ISubjectAccessors can be attached to subject models and when present, receive the fully computed
 * state of the subject over the whole context. They do not exist by default, but user accessors may be
 * defined to provide specific computations. Any complex model being wrapped semantically is essentially
 * a ISubjectAccessor as an entry point. 
 * 
 * @author Ferdinando
 */
public abstract interface IActuator {

    /**
     * All accessors must have a name. Their name may be translated to a formal name in
     * a dependency or mediation link.
     * 
     * @return
     */
    String getName();

    /**
     * Accessors may have a sequence of IActions connected to them. If so, they are
     * triggered if the current computation context is the result of the transitions
     * the action responds to.
     * 
     * @return
     */
    List<IAction> getActions();

    /**
     * If true, the accessor has actions not associated to initialization, which 
     * will only be executed at temporal transitions.
     * 
     * @return
     */
    boolean isTemporal();

}
