/*******************************************************************************
 *  Copyright (C) 2007, 2014:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.api.modelling.agents;

import java.io.Serializable;

import org.integratedmodelling.api.modelling.IDirectActiveObservation;
import org.integratedmodelling.api.modelling.scheduling.ITransition;
import org.integratedmodelling.api.time.ITimeInstant;
import org.integratedmodelling.exceptions.ThinklabException;

/**
 * A task item which will go on the Observation Queue. It can be as lightweight as possible -
 *
 * @author luke
 *
 */
public interface IObservationTask extends Comparable<IObservationTask>, Serializable {
    /**
     * Perform the observation
     * @param controller
     *
     * @throws ThinklabException
     */
    ITransition run() throws ThinklabException;

    /**
     * the instant in time in which the observation is made
     *
     * @return
     */
    ITimeInstant getObservationTime();

    /**
     * remember when work started, so that the currentlyProcessing queue can express staleness (not implemented yet).
     *
     * NOTE: this is set from the outside, so that any failure during task startup will not result in a lost task
     *
     * @param systemTimeMilliseconds
     */
    void startedWorkAt(long systemTimeMilliseconds);

    /**
     * get the time that work started (set by startedWorkAt())
     *
     * @return
     */
    long getStartTime();

    /**
     * set the task to be invalid, so that it will not be processed if pulled off the queue and no result will be saved
     * if one is returned by a worker.
     */
    void setInvalid();

    /**
     * get the validity state of the task (should it be processed? should a result be saved for it?)
     * @return
     */
    boolean isValid();

    public IObservationGraphNode getParentNode();

    void setParentNode(IObservationGraphNode node);

    IDirectActiveObservation getSubject();
}
