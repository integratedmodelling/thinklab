/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.api.modelling.runtime;

import org.integratedmodelling.api.modelling.IObserver;
import org.integratedmodelling.api.monitoring.IMonitor;
import org.integratedmodelling.exceptions.ThinklabException;

/**
 * A IObserver that can produce the necessary IActuators so that states may be computed and mediations done.
 * 
 * @author Ferd
 *
 */
public interface IActiveObserver {

    /**
     * Return the accessor that will mediate to this observer. Do return one even if the mediation is
     * unnecessary or trivial - it will be simplified later. Only throw an exception if the observer passed is
     * incompatible.
     * 
     * @param observer
     * @param monitor report errors and warnings here - this is a runtime monitor that is different in each
     * session
     * @return
     */
    IStateActuator getMediator(IObserver observer, IMonitor monitor) throws ThinklabException;

    /**
     * Return an accessor to interpret, not mediate, the data provided by the passed datasource accessor. This
     * means that the accessor will be linked to another not of the same type (i.e. a measurement will not be
     * linked to another measurement but to a raw data accessor). Called only when
     * canInterpretDirectly(accessor) returned true.
     * 
     * @param accessor
     * @param monitor report errors and warnings here - this is a runtime monitor that is different in each
     * session
     * @return
     */
    IStateActuator getInterpreter(IStateActuator accessor, IMonitor monitor);

    /**
     * Called in observers whose {@link isComputing} returns true. These do not take any input directly, but
     * only know the dependencies stated in the observer definition. These are usually observers with actions
     * to be computed. This must return an accessor that will carry out the computations, without expecting to
     * have a state input for the main observable to modify.
     * 
     * @param monitor report errors and warnings here - this is a runtime monitor that is different in each
     * session
     * 
     * @return
     * @throws ThinklabException
     */
    IStateActuator getComputingAccessor(IMonitor monitor) throws ThinklabException;

    /**
     * Passed the accessor from a downstream datasource that we are asked to provide observation semantics
     * for. If this returns true, the datasource accessor is used unmodified. Otherwise, the observer is asked
     * to provide an accessor by calling {@link getInterpreter} and a mediation link is used to join the two
     * in the resulting workflow. In general practice only numeric and string observers without actions should
     * respond true, but special accessor types may require more sophistication.
     * 
     * @param actuator
     * @return
     */
    boolean canInterpretDirectly(IActuator actuator);
}
