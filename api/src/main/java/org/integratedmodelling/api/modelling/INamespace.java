/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.api.modelling;

import java.io.File;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import org.integratedmodelling.api.errormanagement.ICompileNotification;
import org.integratedmodelling.api.knowledge.IConcept;
import org.integratedmodelling.api.knowledge.IOntology;
import org.integratedmodelling.api.lang.IMetadataHolder;
import org.integratedmodelling.api.metadata.IMetadata;
import org.integratedmodelling.api.project.IProject;

/**
 * @author  Ferd
 */
public interface INamespace extends IMetadataHolder {

    /**
     * The name of the namespace - full path if dot-separated.
     * @return
     */
    String getId();

    /**
     * Time of creation of the underlying resource if any, time when the object definition 
     * finished otherwise.
     * 
     * @return
     */
    long getTimeStamp();

    /**
     * Namespaces that are part of domain ontologies will return a non-null concept here, which must be
     * a domain trait specified using 'domain root' in the core domain namespace. If this IS the core
     * domain namespace, the concept returned will be the core domain concept from the foundational
     * ontology.
     * 
     * @return
     */
    IConcept getDomain();

    /**
     * Return all first-class objects that were explicitly created in the language statements that 
     * produced this namespace. Implicit concepts/properties and imported objects will not be
     * returned. Child knowledge is available through the getChildren() method in IModelObject. If
     * all objects including children are wanted, use getAllModelObjects.
     *  
     * @return
     */
    List<IModelObject> getModelObjects();

    /**
     * Return all model objects, recursing over child knowledge, as a flat list.
     * 
     * @return
     */
    List<IModelObject> getAllModelObjects();

    /**
     * Get a model object by name. Will not return implicit concepts/properties and anonymous
     * objects.
     * 
     * @param mod
     * @return
     */
    IModelObject getModelObject(String mod);

    /**
     * Return the project that this namespace was created from. Currently it can be null but
     * shouldn't - external import units should be projects and interactive sessions should 
     * operate within a specialized project.
     * 
     * @return
     */
    IProject getProject();

    /**
     * Return all namespaces imported by this one.
     * 
     * @return
     */
    Collection<INamespace> getImportedNamespaces();

    /**
     * String representation of the URL this namespace was read from. Should not be null
     * unless this is an interactive namespace.
     * 
     * TODO rename to getResource()
     * 
     * @return
     */
    File getResourceUrl();

    /**
     * @return
     */
    List<String> getTrainingNamespaces();

    /**
     * @return
     */
    List<String> getLookupNamespaces();

    /**
     * If a model was given a specific coverage in any extent, either directly or through
     * a namespace-wide specification, return the context that
     * expresses that coverage. If no coverage has been specified, return an empty
     * context.
     * 
     * @return
     */
    IScale getCoverage();

    /**
     * Return true if the namespace has any errors that will prevent the use of its
     * model objects.
     * 
     * @return
     */
    boolean hasErrors();

    /**
     * Return true if the namespace has warnings that should be reported before use.
     * 
     * @return
     */
    boolean hasWarnings();

    /**
     * Retrieve all errors, warnings and info annotations. Use instanceof to see which is which.
     * 
     * @return
     */
    Collection<ICompileNotification> getCodeAnnotations();

    // /**
    // * Get all errors in the namespace: error code, message and line number (1-based, 0 for no line)
    // * @deprecated use Error objects and return a list
    // * @return
    // */
    // Collection<Triple<Integer, String, Integer>> getErrors();
    //
    // /**
    // * Get all warnings in the namespace: message and line number (1-based, 0 for no line)
    // *
    // * @deprecated use Warning objects and return a list
    // * @return
    // */
    // Collection<Pair<String, Integer>> getWarnings();

    /**
     * Returns the ontology associated with the namespace. Not that you should do anything
     * with it.
     * 
     * @return
     */
    IOntology getOntology();

    /**
     * The namespace's symbol table should contain any model objects and define's encountered, plus
     * all the imported symbols from any imported namespaces.
     * 
     * @return
     */
    Map<String, Object> getSymbolTable();

    /**
     * Return true if this namespace is a scenario, meaning that its models will only be
     * used (and if so, preferentially) to resolve dependencies when the scenario is
     * made active.
     * 
     * @return
     */
    boolean isScenario();

    /**
     * Return the model resolution criteria defined for the namespace, or the default ones if none were
     * given. These are basically a set of weights that will apply to each model retrieved to resolve
     * a concept in order to rank them and choose the best. Resolution criteria may vary by namespace.
     * 
     * @return
     */
    IMetadata getResolutionCriteria();

    /**
     * Return all the namespaces that this should not be mixed with during
     * resolution or scenario setting.
     * 
     * @return
     */
    Collection<String> getDisjointNamespaces();

    /**
     * Return the local file this namespace was read from, or null if it wasn't read from
     * a file. Returning null means in all likelihood that this namespace will never be
     * reloaded. At this stage of development, nothing should ever return null.
     * 
     * @return
     */
    File getLocalFile();

    /**
     * If the namespace is private, each model in it is private even if not 
     * tagged as such. 
     * 
     * @return
     */
    boolean isPrivate();

    /**
     * If the namespace is inactive, each model in it is inactive even if not 
     * tagged as such. 
     * 
     * @return
     */
    boolean isInactive();

    /**
     * The doc string defined for the namespace. Should never
     * be null.
     * @return
     */
    String getDescription();
}
