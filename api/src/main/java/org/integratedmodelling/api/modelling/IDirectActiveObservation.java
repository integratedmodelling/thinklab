/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.api.modelling;

import java.util.Map;

import org.integratedmodelling.api.knowledge.IProperty;
import org.integratedmodelling.api.modelling.agents.IAgentState;
import org.integratedmodelling.api.modelling.agents.ICollision;
import org.integratedmodelling.api.modelling.agents.IObservationGraphNode;
import org.integratedmodelling.api.modelling.scheduling.ITransition;
import org.integratedmodelling.api.monitoring.IMonitorable;
import org.integratedmodelling.api.time.ITimePeriod;
import org.integratedmodelling.exceptions.ThinklabException;

/**
 * A direct observation - subject, event or process - that is alive and operating in a context.
 * 
 * @author Ferd
 * @author Luke
 *
 */
public interface IDirectActiveObservation extends IDirectObservation, IMonitorable {

    /**
     * Added by Luke - this is where an agent determines whether or not two agent-states cause a collision. 
     * It is left to each agent (i.e. processing is done twice for every pair of overlapping
     * agent-states) to decide whether a collision happens, and why, and when, and what is the result. This is
     * because an agent might have a subjective idea of what types of collisions might happen, might have
     * domain-specific knowledge or detection algorithms, etc.
     *
     * This should always return the FIRST collision in the overlapping time period if there are more than
     * one, because the two overlapping agents' results (collisions) will be compared and the LATER one will
     * be discarded.
     *
     * @param myAgentState
     * @param otherAgentState
     * @return
     */
    ICollision detectCollision(IObservationGraphNode myAgentState, IObservationGraphNode otherAgentState);

    /**
     * Added by Luke - this is where an agent will determine whether or not its agent-state will be affected
     * by the collision. If it answers true, then the agent-state will be partitioned into an
     * un-affected pre-collision partition, and a new observation task will be created for the collision time
     * which takes the collision into consideration. The post-collision agent-state may or may not be changed
     * from the original, and its duration may be the same or different.
     *
     * All causal relationships which depended on this agent-state AFTER the collision time will be
     * invalidated and so must be re-observed. This implies that the agent must re-create any causal links
     * (and observation tasks) which are a result of the new post-collision agent-state.
     *
     * @param agentState
     * @param collision
     * @return
     */
    boolean doesThisCollisionAffectYou(IAgentState agentState, ICollision collision);

    /**
     * 
     * @param timePeriod
     * @return
     */
    ITransition reEvaluateStates(ITimePeriod timePeriod);

    /**
     * 
     * @return
     * @throws ThinklabException 
     */
    ITransition performTemporalTransitionFromCurrentState() throws ThinklabException;

    /**
     * Return the linked states as a map keyed by the properties that are represented
     * by each state. Only return the IObjectState states, because that is how
     * these results are used in calling code. (abstract states
     * like topologies are not relevant to callers)
     * @return
     */
    public Map<IProperty, IState> getObjectStateCopy();

    /**
     * An active observation has a model - an exception is a ISubject which may not have one, and can
     * return null.
     * 
     * @return
     */
    public IModel getModel();

}
