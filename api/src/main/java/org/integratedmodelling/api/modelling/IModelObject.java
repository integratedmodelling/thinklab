/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.api.modelling;

import java.util.Collection;

import org.integratedmodelling.api.lang.IMetadataHolder;
import org.integratedmodelling.api.lang.INamespaceQualified;

/**
 * @author Ferdinando
 *
 */
public interface IModelObject extends ILanguageObject, INamespaceQualified, IMetadataHolder {

    /**
     * All model objects have an ID, although not all need to use it.
     * 
     * @return
     */
    String getId();

    /**
     * true if flagged as namespace-private in the definition. For now only used in
     * models.
     * 
     * @return
     */
    boolean isPrivate();

    /**
     * If the object is inactive, it's not used for anything. Same specs as isPrivate(). 
     * 
     * @return
     */
    boolean isInactive();

    /**
     * Any number of annotations may be added at runtime. These are Java-like annotations, not
     * semantic anything. At the moment only concepts, observations and first-class models can
     * be annotated in k.IM.
     * 
     * @return
     */
    Collection<IAnnotation> getAnnotations();

    /**
     * Total number of errors notified when parsing this object. Relevant to first-class objects
     * only, which should cumulate the errors in their component objects if any. The actual errors
     * are stored in the namespace.
     * 
     * @return
     */
    int getErrorCount();

    /**
     * Total number of warnings notified when parsing this object. Relevant to first-class objects
     * only, which should cumulate the warnings in their component objects if any.  The actual warnings
     * are stored in the namespace.
     * 
     * @return
     */
    int getWarningCount();

    /**
     * True if this object is defined at namespace level in a k.IM source file.
     * 
     * @return
     */
    boolean isFirstClass();

    /**
     * True if this was given a "deprecated" annotation.
     * 
     * @return
     */
    boolean isDeprecated();

    /**
     * Get all first-class declared children. Only instance for now when this will not return an empty
     * collection is with IKnowledgeObjects (child concepts) and IDirectObservers (child observations).
     * 
     * NOTE: this reflects the hierarchy in the DECLARATION, not the objects. So knowledge will not be
     * organized according to subsumption - parents should be top-level even if they have a 'is X' clause
     * and X is in the same namespace.
     * 
     * @return
     */
    Collection<IModelObject> getChildren();

    /**
     * Null unless this object is a child of another.
     * 
     * @return
     */
    IModelObject getParent();

}
