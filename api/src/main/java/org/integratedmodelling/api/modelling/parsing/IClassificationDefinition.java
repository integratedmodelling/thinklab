/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
// package org.integratedmodelling.api.modelling.parsing;
//
// import org.integratedmodelling.api.knowledge.IConcept;
// import org.integratedmodelling.api.lang.IModelResolver;
// import org.integratedmodelling.api.modelling.IClassification;
// import org.integratedmodelling.api.modelling.IClassifier;
// import org.integratedmodelling.api.modelling.ILanguageObject;
// import org.integratedmodelling.api.modelling.IObservable;
// import org.integratedmodelling.exceptions.ThinklabException;
//
/// **
// * Augments IClassification with setting methods for parsers and languages to use.
// *
// * @author Ferd
// *
// */
// public interface IClassificationDefinition extends ILanguageObject, IClassification {
//
// /**
// * Set the main concept space. It will be called AFTER all the classifiers are in.
// *
// * @param concept
// */
// public void setConceptSpace(IObservable concept, IModelResolver resolver);
//
// /**
// * Add a classifier. Called in order of definition.
// *
// * @param concept
// * @param classifier
// * @throws ThinklabException
// */
// public void addClassifier(IConcept concept, IClassifier classifier) throws ThinklabException;
//
// /**
// * This is a user definition, which needs to be validated on the classifiers.
// *
// * @param isDiscretization
// */
// public void setDiscretization(boolean isDiscretization);
//
// /**
// * Called as last thing in the definition process, when all other definitions
// * and notifications have happened.
// */
// public void initialize();
//
// /**
// * If this is called with true, the classification is used to mediate a "select" observer,
// * so it should prepare to deal with binary classifiers.
// *
// * @param b
// */
// public void setBooleanRanking(boolean b);
//
// /**
// * If this classification is expected to classify according to a trait, set the
// * generic trait type here. This will modify the classifiers to return a trait-enriched
// * root type and will validate the stated traits against the type passed.
// *
// * @param traitType
// */
// public void setTraitType(IConcept traitType);
//
// }
