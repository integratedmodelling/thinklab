/*******************************************************************************
 *  Copyright (C) 2007, 2014:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.api.modelling.visualization;

import java.io.File;
import java.util.List;

import org.integratedmodelling.exceptions.ThinklabException;

/**
 * Visualization for a subject. Produces media for the context and visualizations for
 * all states and subject linked to it.
 * 
 * TODO rediscuss
 * 
 * @author Ferd
 *
 */
public interface IVisualization {

    /**
     * Get the requested media type for the context. This will be e.g. a satellite
     * image for a spatial object, a timeline for a temporal, etc.
     * 
     * @param directory
     * @param mediaType
     * @param viewportWidth
     * @param viewportHeight
     * @param flags
     * @return
     * @throws ThinklabException
     */
    public IMedia getMedia(File directory, int mediaType, int viewportWidth, int viewportHeight, int flags)
            throws ThinklabException;

    /**
     * Get visualizations for all states. 
     * 
     * @return
     */
    public List<IStateVisualization> getStateVisualizations();

    /**
     * Get visualizations for all subjects.
     * 
     * @return
     */
    public List<IVisualization> getSubjectVisualizations();

    /**
     * Persist to given location. Return a location URI where this or another visualization 
     * can be restored from. Should be capable of taking null for a
     * location, creating its own storage in a default area.
     * 
     * @param location
     * @return
     * @throws ThinklabException
     */
    public abstract String persist(String location) throws ThinklabException;

    /**
     * Name of the subject being described, or the dataset being visualized.
     * 
     * @return
     */
    String getName();

}
