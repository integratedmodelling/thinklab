/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.api.modelling.runtime;

import org.integratedmodelling.api.modelling.IDirectObservation;
import org.integratedmodelling.api.modelling.IModel;
import org.integratedmodelling.api.modelling.IObservable;
import org.integratedmodelling.api.modelling.IObserver;
import org.integratedmodelling.api.modelling.resolution.IResolutionContext;
import org.integratedmodelling.api.modelling.scheduling.ITransition;
import org.integratedmodelling.api.monitoring.IMonitor;
import org.integratedmodelling.exceptions.ThinklabException;

/**
 * Base interface for any accessor that supports direct observations: event, subject or process.
 * 
 * @author Ferd
 *
 */
public abstract interface IDirectActuator<T extends IDirectObservation> extends IActuator {

    /**
     * Perform any initialization tasks. If the subject had data or process dependencies, the correspondent
     * initialization steps will have been completed successfully by the time this is called.
     * 
     * @param subject the direct observation we are handling.
     * @param context the father object (may be null)
     * @param property the property linking us to the father object (null if the latter is null)
     * @param monitor use to communicate with the client or to check for interruption.
     * 
     * @return the same direct observation passed, or another one defined for the context to take its place.
     */
    public T initialize(T subject, IDirectObservation context, IResolutionContext resolutionContext, IMonitor monitor)
            throws ThinklabException;

    /**
     * Process the passed transition. In most situations this will correspond to a temporal
     * transition.
     * 
     * @param transition
     * @param monitor
     * @return false if we should not continue with the contextualization.
     * @throws ThinklabException
     */
    public boolean processTransition(ITransition transition, IMonitor monitor)
            throws ThinklabException;

    /**
     * Called for each observable/observer that the result subject is expected to have
     * observed.
     * 
     * @param observable
     * @param observer
     * @return
     */
    public void notifyExpectedOutput(IObservable observable, IObserver observer, String name);

    /**
     * Called at the very beginning of a model's processing with the model this accessor was given to.
     * 
     * @param model
     */
    void notifyModel(IModel model);
}
