/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.api.modelling;

import java.util.Set;

import org.integratedmodelling.api.knowledge.IConcept;
import org.integratedmodelling.api.knowledge.IExpression;

/**
 * Actions may be connected to accessors to operate after their values have been computed. 
 * In state accessors, actions may change, integrate, reinterpret observable values, or compute
 * the value of the observable in computing observers. In
 * subject accessors, they may modify the state or context of the subject. Actions are 
 * triggered by transitions; a set of domain concepts identifies the specific transitions
 * that the action is sensitive to.
 * 
 * @author Ferd
 *
 */
public interface IAction {

    enum Type {
        SET,
        CHANGE,
        INTEGRATE,
        DO,
        MOVE,
        DESTROY
    };

    /**
     * The type of action.
     * 
     * @return
     */
    Type getType();

    /**
     * Return the array of domain concepts that select the transitions upon which 
     * this action will be run. If the return value is empty, the action
     * is expected to run at initialization only.
     * 
     * @return
     */
    Set<IConcept> getDomain();

    /**
     * If the action has a target state, return its formal name in the model. 
     * 
     * @return
     */
    String getTargetStateId();

    /**
     * Get the expression that will execute the action when evaluated in context.
     * 
     * @return
     */
    IExpression getAction();

    /**
     * Get the condition for the action to take place. May be null and may be an ICondition.
     * 
     * @return
     */
    IExpression getCondition();

}
