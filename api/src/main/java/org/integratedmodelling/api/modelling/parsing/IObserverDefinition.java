/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
// package org.integratedmodelling.api.modelling.parsing;
//
// import org.integratedmodelling.api.lang.IModelResolver;
// import org.integratedmodelling.api.modelling.IKnowledgeObject;
// import org.integratedmodelling.api.modelling.IObservable;
// import org.integratedmodelling.api.modelling.IObserver;
//
// public interface IObserverDefinition extends IObservingObjectDefinition, IObserver {
//
// void addObservable(IModelDefinition mediated);
//
// /**
// * Set the observable from the passed concept definition, which may contain inherency and contexts. In
// observers
// * this will be called once (or never if the other is used). In a model, it may be called multiple times for
// * outputs beyond the primary observable.
// *
// * @param observable
// */
// void addObservable(IKnowledgeObject observable);
//
// /**
// * For a number of reasons we pass the model this observer is part of.
// *
// * @param model
// */
// void setModel(IModelDefinition model);
//
// /**
// * Notify the model observable. This may or may not be the same as the observer's according to whether the
// * model names an observable concept (unresolved models) or not. If not the same, it will have been made a
// * subclass of it at initialization (which happens before this is called). Things like classifications may
// * want to use this one to properly handle inheritance of any internal concepts.
// *
// * @param observableConcept
// */
// void notifyModelObservable(IObservable observable, IModelResolver resolver);
//
// /**
// * Classifications and discretization can be done according to an inherited trait inherited by
// * their main type, as well as the subtypes of the type itself. If so, pass the trait concept
// * here.
// *
// * @param trait
// */
// void setTraitType(IKnowledgeObject trait);
// }
