/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.api.modelling;

import java.util.List;

import org.integratedmodelling.api.knowledge.IConcept;

/**
 * 
 * @author Ferd
 *
 */
public interface IClassification {

    /**
     * @author   Ferd
     */
    public enum Type {
        UNORDERED,
        BOOLEAN_RANKING,
        ORDERED_RANKING,
        RANGE_DISCRETIZATION
    }

    /**
     * This must be known after initialization. The classification should use hints coming from
     * the concept, the observer type etc. to determine the type. There is also a type hint that
     * can be explicitly submitted in the IClassificationDefinition interface.
     * 
     * @return
     */
    public Type getType();

    /**
     * Return the root concept for the classification. All concepts that can be attributed
     * to the classifiers should be direct, disjoint children of it.
     * 
     * @return
     */
    public IConcept getConceptSpace();

    /**
     * Return the concept that the passed object classifies to.
     * 
     * @param o
     * @return
     */
    public IConcept classify(Object o);

    /**
     * Return all the classifiers that define the classification. If the classifiers have
     * an intrinsic ordering, this function should return them in an order that reflects it.
     * @return
     */
    public List<IClassifier> getClassifiers();

    /**
     * Get the concept ordering implied by the definition - which may be the order
     * of declaration, but also have been redefined based e.g. on the discretization
     * intervals, and may not correspond to the ordering of the classifiers.
     * 
     * @return
     */
    public List<IConcept> getConceptOrder();

    /**
     * If the classification discretizes a numeric range, this must return
     * the breakpoints of the range, i.e. an array of n+1 elements (n = number
     * of concepts) where each interval {r[n], r[n+1]} corresponds to the numeric range
     * of concept n. 
     * 
     * @return
     */
    public double[] getDistributionBreakpoints();

    /**
     * Return true if one of the possible rank concepts corresponds to a logical
     * zero - e.g. NoHousehold or WetlandAbsent.
     * 
     * @return
     */
    public boolean hasZeroRank();

    /**
     * Return true if the classes in this classification are supposed to mean just
     * that - disjoint classes not representing a numeric range or a logical ranking.
     * 
     * @return
     */
    public boolean isCategorical();

    /**
     * Return true if all the intervals are contiguous and the extreme intervals have
     * finite boundaries.
     * 
     * @return
     */
    public boolean isContiguousAndFinite();

    /**
     * True if this classification has been explicitly indicated as a discretization. If
     * this is true, isContiguousAndFinite() must be also true.
     * @return
     */
    public boolean isDiscretization();

    /**
     * Check with another classification and ensure they do exactly the same things. 
     * Could be "equals"- FIXME check if that creates any problem.
     * 
     * @param _classification
     * @return
     */
    public boolean isIdentical(IClassification classification);

    /**
     * Return a sensible numeric value for the passed concept. NaN should be reserved for no-data,
     * "absent" classifiers or unrecognized concepts; ranking order for orderings should be respected. 
     * If the data encode a discretization, it is OK to return the undiscretized values.
     * 
     * @param o
     * @return
     */
    public double getNumericCode(IConcept o);

    /**
     * Classify to the numeric ranking of the concept instead of the concept.
     * 
     * @param o
     * @return
     */
    public int classifyToIndex(Object o);


}
