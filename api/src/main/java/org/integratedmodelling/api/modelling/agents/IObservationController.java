/*******************************************************************************
 *  Copyright (C) 2007, 2014:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.api.modelling.agents;

import org.integratedmodelling.api.modelling.IDirectActiveObservation;
import org.integratedmodelling.api.modelling.scheduling.ITransition;
import org.integratedmodelling.api.time.ITimePeriod;
import org.integratedmodelling.exceptions.ThinklabException;
import org.integratedmodelling.exceptions.ThinklabValidationException;

/**
 * Dispatcher for all observation tasks which need to happen in a simulation.
 *
 * For now, this interface, and its implementation in Thinklab, are just a simple wrapper around an
 * Observation queue and causal graph, but are intended to eventually become a server thread which worker
 * threads call to request observation tasks.
 *
 * @author luke
 *
 */
public interface IObservationController extends IObservationGraphNodePublisher {

    IObservationGraphNode createAgent(IDirectActiveObservation subject, IAgentState initialState,
            ITimePeriod initialTimePeriod, IObservationGraphNode parentNode, IObservationTask task,
            boolean enqueueSubsequentTask);

    /**
     * remove and return the head of the observation queue. Return null if the queue is empty.
     *
     * @return
     */
    public IObservationTask getNext();

    /**
     * after an observation has been done, set the result in the graph & agent-state map so that it is
     * readable by others. Also, by setting it in the graph, it is modifiable in the case of a collision; new
     * observation tasks can be created and executed to update the observation(s).
     *
     * @param task
     * @param result
     * @throws ThinklabValidationException
     */
    public void setResult(IObservationTask task, ITransition result) throws ThinklabValidationException;

    /**
     * This is how agents' temporal scales get disrupted from the outside. Agent states must be divided into
     * "before" and "after" the collision event, and agents must decide how to handle the collision, possibly
     * modifying the "after" state.
     *
     * @throws ThinklabException
     */
    public void collide(IObservationGraphNode agentState1, IObservationGraphNode agentState2,
            ICollision collision) throws ThinklabException;

    public void collide(IObservationGraphNode agentState, ICollision collision) throws ThinklabException;

}
