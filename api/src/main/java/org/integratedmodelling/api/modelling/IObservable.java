/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.api.modelling;

import org.integratedmodelling.api.knowledge.IConcept;
import org.integratedmodelling.api.knowledge.IKnowledge;
import org.integratedmodelling.api.knowledge.IProperty;
import org.integratedmodelling.api.knowledge.ISemantic;

/**
 * For tightness and efficiency, we define a specialized concept that describes the semantics of the observed
 * incarnation of another concept. It provides specialized and efficient access to the restrictions that
 * define the observation type and the inherent subject type (if any). If there is a model associated with the
 * observation (anywhere except in a resolved IState) the IObservable provides access to that as well.
 * 
 * If getSemantics() is called, the observable concepts reside in the observation namespace returned by
 * {@link IModelManager.getObservationNamespace()}.
 * 
 * @author Ferd
 * 
 */
public interface IObservable extends ISemantic {

    /**
     * An observable can be used to "explain" something already instantiated (or a state, which isn't 
     * really instantiated) or to instantiate its type, for subsequent explanation. The latter only
     * applies to direct observables. We keep this distinction (corresponding to the presence of the
     * 'each' keyword in the language) here for simplicity. Two observables must have the same action
     * as well as the same observable in order to be equal.
     * @author Ferd
     *
     */
    enum Action {
        EXPLAIN,
        INSTANTIATE
    }

    /**
     * Use when we know the type is a concept (e.g. when the language has validated a concept model). Throws
     * an unchecked exception if not.
     * 
     * @return
     */
    IConcept getTypeAsConcept();

    /**
     * Use when we know the type is a property (e.g. when the language has validated a relationship model). Throws
     * an unchecked exception if not.
     * 
     * @return
     */
    IProperty getTypeAsProperty();

    /**
     * Return the action context in which this observable is employed. If INSTANTIATE, we are using it to
     * create instances of the direct observable it describes. Otherwise we are applying it to an existing
     * observation to compute its states.
     */
    Action getAction();

    /**
     * Return the model we're using to interpret the observable. This is only defined in model observables,
     * and will return null in IStates.
     * 
     * @return
     */
    IModel getModel();

    /**
     * Return the observer that made this observation. It will normally return getModel().getObserver(), but
     * will be defined in IStates where getModel() returns null, and will return null when getModel() returns
     * a subject model. To investigate the semantics of the observation, getObservationType() should be used
     * instead of getObserver(), as that will be available in every situation, including past
     * serialization/deserialization.
     * 
     */
    IObserver getObserver();

    /**
     * Return the type of the observation that the un-observed type is restricted with. E.g. this may return
     * NS.RANKING if we're looking at a numeric way of observing the observable. For subject observations, it
     * will return NS.DIRECT_OBSERVATION. This will never return null.
     * 
     * @return
     */
    IConcept getObservationType();

    /**
     * Returns the type that the IObservable is inherent to. This correspond to the narrative description
     * "X of Y" and is defined for qualities, that can only exist inherently to something. The returned type
     * is usually a direct observable (subject, event, relationship - e.g. Elevation of a Mountain) 
     * but it could be a quality for "observation of observation" situations (e.g. the Uncertainty inherent 
     * to the observation of a quality) although in the latter case it would be more appropriate to use the 
     * Measurement concept instead (but we don't specialize them at the moment). All quality models must
     * specify an inherent type for their quality to be usable. Quality concepts observed within contexts
     * are automatically made inherent to them.
     * 
     * @return
     */
    IKnowledge getInherentType();

    /**
     * If given, the type of the subject that provides the context for this observation. In queries, it 
     * should be an optional match. Corresponds to the 'within' part of the observation. Used to restrict
     * observables to specific contexts; matched preferentially if results are available.
     *  
     * @return
     */
    IConcept getContextType();

    /**
     * Get the level of detail at which we are looking at the main observable, which must be a 
     * trait of a class for this to be different from -1. It will also be illegal for this to be
     * zero as that would mean "no detail".
     * 
     * @return
     */
    int getDetailLevel();

    /**
     * An observable is always given a formal name that it can be referred to with. In models, this is the
     * formal name specified by the modeler. If no formal name is specified, a sensible and decent-looking
     * default should be provided.
     * 
     * @return
     */
    String getFormalName();

    /**
     * This will return a trait type if the observation is relative to a specific trait of the main type. It
     * will only return non-null in classified or discretized observables that classify 'by' a given trait. 
     * 
     * If not null, the matching of another observable (using the {@link IObservable.is(IObservable)} method) is done by 
     * checking that the passed observable inherits the trait in question.
     */
    IConcept getTraitType();

    /**
     * When this return null, the observation has been made by the user/client. Otherwise, a subject in
     * the root hierarchy made it. When a IObservable is used as a key, having different observing 
     * agents will make two observables of the same concept different as they reflect different points of view
     * on the higher-level subject - it is the only way for two states of the same concept to coexist in a subject.
     * 
     * @return
     */
    IDirectObservation getObservingSubject();

    /**
     * Check identity of all concept known in both objects. All common concept must be what expected, no
     * concepts must be not in common. For traits, we check that the trait we contain, if any, is 
     * inherited by the passed observable.
     * 
     * @param concept
     * @return
     */
    boolean is(IObservable observable);

    /**
     * Name of either the main type or the delegate type, if any.
     * 
     * @return
     */
    String getLocalName();

    /**
     * Check identity of the main type only. Implies identity of the delegate type, which is forced to be a
     * subclass of it.
     * 
     * @param concept
     * @return
     */
    boolean is(IKnowledge concept);

    /**
     * Check identity of observation and observed type. Good to inquire whether e.g. we're quantifying or
     * classifying.
     * 
     * @param observationType
     * @param observedType
     * @return
     */
    boolean is(IConcept observationType, IConcept observedType);

    /**
     * Check identity of all types except trait.
     * 
     * @param observationType
     * @param observedType
     * @param inherentToType
     * @return
     */
    boolean is(IConcept observationType, IConcept observedType, IConcept inherentToType);

    /**
     * Check identity of all types. Like passing the correspondent observable.
     * 
     * @param observationType
     * @param observedType
     * @param inherentToType
     * @param traitType
     * @return
     */
    boolean is(IConcept observationType, IConcept observedType, IConcept inherentToType, IConcept traitType);

    /**
     * True if an existing observation of this observable can be used to resolve the passed one.
     * @param o
     * @return
     */
    boolean canResolve(IObservable o);

    /**
     * If we are classifying 'by' trait, we can specify the detail level of the trait in k.IM (classify X by T down to Z).
     * 
     * @return
     */
    int getTraitDetailLevel();

}
