package org.integratedmodelling.api.modelling;

/**
 * Tag interface to identify a model bean as such. Used only to ease translation in the
 * model factory.
 * 
 * The model beans use the mildly controversial Project Lombok annotations to keep the
 * code clean. This means that developers using Eclipse will need to instrument their installation
 * with the Project Lombox jar before the beans can be compiled.
 * 
 * @author ferdinando.villa
 *
 */
public interface IModelBean {

}
