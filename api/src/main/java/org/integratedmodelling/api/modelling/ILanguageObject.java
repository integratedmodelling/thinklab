/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.api.modelling;

/**
 * Includes all the methods that language objects need to implement in order to be 
 * defined by a parser. These will be implemented by classes implementing the corresponding
 * immutable interfaces, so we don't have to put setters in the modeling API.
 * 
 * Objects of this kind should be created by the IResolver passed to the parsers, and
 * implementations will know what to do with them to turn them into "active" model
 * objects.
 * 
 * @author Ferd
 *
 */
public abstract interface ILanguageObject {

    /**
     * First line in parsed text.
     * 
     * @return
     */
    public abstract int getFirstLineNumber();

    /**
     * Last line in parsed text.
     * 
     * @return
     */
    public abstract int getLastLineNumber();

}
