/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.api.modelling.runtime;

import org.integratedmodelling.api.modelling.IObservable;
import org.integratedmodelling.api.modelling.IObserver;
import org.integratedmodelling.api.modelling.scheduling.ITransition;
import org.integratedmodelling.exceptions.ThinklabException;

/**
 * Accessors implementing this one are serial accessors that can use dependencies and
 * produce more than an output state. The invocation logics for their methods is 
 * more complex - first the dependencies and expected outputs are notified once per
 * observable; then at each context state, all the dependency values are set with 
 * setValue, then process() is called, after which all the output values are extracted
 * with getValue(). Each of these will use the keys notified beforehand.
 * 
 * @author Ferd
 *
 */
public interface IStateActuator extends IActuator {

    /**
     * Notifies the accessor of the observation semantics it is expected to express: the 
     * observer passed is the one that the accessor has been linked to. In most cases this
     * will be expected to produce states corresponding to the key returned by
     * getSelfID().
     * 
     * @param observable
     * @param observer
     * @throws ThinklabException 
     */
    void notifyObserver(IObservable observable, IObserver observer) throws ThinklabException;

    /**
     * This method is called once per data dependency before any values are extracted, passing
     * the key that will be available for get() when values are extracted.
     * 
     * @param key the formal name of the parameter that will be passed to the 
     * @param accessor the observer that will be used to get the dependency.
     * 
     * @throws ThinklabException 
     */
    void notifyDependency(IObservable observable, IObserver observer, String key,
            boolean isMediation) throws ThinklabException;

    /**
     * Called at initialization to inform the accessor that it's expected to produce
     * states for the passed observable, and make them accessible by passing the
     * given key string to getValue(). Note that notifyObserver() will ALSO be passed the same
     * observer for the "main" observable of the model but this will be called also for 
     * all other observers.
     * 
     * @param observable
     * @param observer 
     * @param key
     */
    void notifyExpectedOutput(IObservable observable, IObserver observer, String key)
            throws ThinklabException;

    /**
     * Compute anything the accessor computes over the expected context (which will
     * be one state of the overall context our observer may have passed us)
     * 
     * After this is called, the appropriate getValue will
     * be called to retrieve the output(s).
     * 
     * NOTE: this may be called more than once with the same observable and 
     * different names. It must be capable of handling that correctly.
     * 
     * @return
     */
    void process(int stateIndex, ITransition transition) throws ThinklabException;

    /**
     * Pass the current value of a dependency, which will be made available for process()
     * to use.
     * 
     * @param inputKey
     * @param value
     */
    void setValue(String inputKey, Object value);

    /**
     * Compute or retrieve the value for the passed context index. Any dependencies have
     * been passed as independent accessors using notifyDependency before this is called.
     * May be called more than once with the same key and should be efficient in that case.
     * 
     * @return
     */
    Object getValue(String outputKey);

    /**
     * Called to reset known values before a new cycle of computation. 
     */
    void reset();
}
