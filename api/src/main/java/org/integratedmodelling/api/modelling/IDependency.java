/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.api.modelling;

import org.integratedmodelling.api.knowledge.IConcept;
import org.integratedmodelling.api.knowledge.IExpression;
import org.integratedmodelling.api.knowledge.IProperty;
import org.integratedmodelling.api.knowledge.ISemantic;

public interface IDependency extends ILanguageObject, ISemantic {

    /**
     * The object that we need to observe to satisfy this dependency.
     * @return
     */
    IObservable getObservable();

    /**
     * Whether we can do without this dependency or not.
     * @return
     */
    boolean isOptional();

    /**
     * The property that expresses the counterpart of this dependency in the conceptual model for the
     * observable of the embedding model. May be null. If not, the dependency resolution strategy must
     * honor the restrictions of the property in the context of each matched observable for the embedding
     * model.
     * 
     * @return
     */
    IProperty getProperty();

    /**
     * If this does not return null, the observations resulting from resolving the dependency must
     * be reinterpreted as the trait returned, i.e., their observables must be redefined by inheriting
     * the trait.
     * 
     * @return
     */
    IConcept reinterpretAs();

    /**
     * If true, the dependency has the "every" qualifier, meaning we want to observe not one
     * concept, but all the disjoint concrete children of it as separate observations.
     * 
     * @return
     */
    boolean isGeneric();

    /**
     * Return whatever object was passed to contextualize the dependency (the 'at' part of the
     * specification). It can be anything that generates context - either extents, models, or a function
     * call that creates that.
     * 
     * @return
     */
    Object getContextModel();

    /**
     * If this is true, the dependency will refer to 'each' subject of the type returned by
     * getDistributionContext(), which will need to be observed before the dependency can be
     * computed. It may also have a 'where' clause to filter the subjects.
     *  
     * @return
     */
    boolean isDistributing();

    /**
     * This will only return non-null in distributing dependencies, and will be applied to the subjects 
     * in the distribution context to select the ones that match.
     * 
     * @return
     */
    IExpression getWhereCondition();

    /**
     * This is non-null in distributing dependencies, and returns the direct observable for the
     * objects that make up the distribution context.
     * 
     * @return
     */
    IConcept getDistributionContext();

    /**
     * All dependencies have a formal name, either given by the user or assigned automatically. It is an error
     * to have non-unique formal names in an observing object.
     * 
     */
    String getFormalName();

}