/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.api.modelling.runtime;

import java.util.Map;

import org.integratedmodelling.api.modelling.IObservable;
import org.integratedmodelling.api.modelling.IObservation;
import org.integratedmodelling.api.modelling.IState;
import org.integratedmodelling.api.modelling.ISubject;
import org.integratedmodelling.api.modelling.resolution.IResolutionContext;
import org.integratedmodelling.api.modelling.scheduling.ITransition;
import org.integratedmodelling.api.monitoring.IMonitor;
import org.integratedmodelling.exceptions.ThinklabException;

public interface ISubjectContextualizer extends IContextualizer {

    /**
     * Called at the beginning with the process this contextualizer will be 
     * handling. The process contains its scale and context subject. The
     * initializer has the option of modifying the process or changing it
     * altogether and returning a new one, which may have a different scale as
     * long as it's contained within the original one.
     * 
     * The expected input and outputs corresponds to states that will be passed to 
     * compute() or are expected back from it.
     * 
     * @param process
     */
    Map<String, IObservation> initialize(ISubject process, ISubject contextSubject, IResolutionContext context, Map<String, IObservable> expectedInputs, Map<String, IObservable> expectedOutputs, IMonitor monitor)
            throws ThinklabException;

    /**
     * React to the passed transition by computing the necessary states for the subject passed at initialization.
     * Transition will be null if we're initializing the subject.
     * 
     * @param transition
     * @param inputs
     * @return
     */
    Map<String, IObservation> compute(ITransition transition, Map<String, IState> inputs)
            throws ThinklabException;
}
