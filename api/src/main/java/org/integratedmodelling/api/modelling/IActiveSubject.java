/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.api.modelling;

import java.util.Collection;

import org.integratedmodelling.api.modelling.resolution.IResolutionContext;
import org.integratedmodelling.api.modelling.resolution.IResolutionStrategy;
import org.integratedmodelling.api.modelling.storage.IDataset;
import org.integratedmodelling.api.monitoring.IMonitor;
import org.integratedmodelling.exceptions.ThinklabException;

/**
 * The in-memory version of a ISubject. We can count on any ISubject being an IActiveSubject while
 * it's being contextualized. 
 * 
 * @author Ferd
 *
 */
public interface IActiveSubject extends ISubject, IDirectActiveObservation {

    /**
     * Observe the passed observable (possibly containing a model). If initialize() hasn't been called yet, just validate and 
     * record the subject dependency. Otherwise, start the observation immediately. Throw an
     * exception if contextualize() has been called before.
     * 
     * Returns the coverage if the observation is made, and null if not (in that case the coverage will
     * be returned by initialize()).
     * 
     * @param observable
     * @param isOptional raise warnings instead of errors if the observation cannot be made.
     * @throws ThinklabException
     */
    ICoverage observe(IObservable observable, Collection<String> scenarios, boolean isOptional, IMonitor monitor)
            throws ThinklabException;

    /**
    * Resolve the subject, finding a model for it first and resolving that, then resolving any
    * dependencies added with addDependency(). Can be called repeatedly as long as contextualize()
    * hasn't been called. As resolution proceeds, the resolution strategy returned by
    getResolutionStrategy()
    * grows. Must be mindful that the subject may be in the context of another.
    *
    * FIXME should not be in API.
    *
    * @return
    * @throws ThinklabException
    */
    ICoverage initialize(IResolutionContext scenarios, IMonitor monitor) throws ThinklabException;

    /**
     * Return the subject that this is in the context of, or null if it is the primary subject of an
     * observation.
     * 
     * @return
     */
    IDirectObservation getContextSubject();

    /**
     * Call after initialize() to complete covering the context, i.e. run any time transitions. 
     * After this has run, the subject is immutable.
     */
    void contextualize() throws ThinklabException;

    /**
     * Return the resolution strategy, empty if initialize() wasn't called.
     * 
     * @return
     */
    IResolutionStrategy getResolutionStrategy();

    /**
     * Adds a computed state, determining the property to link with using the semantics. Throw an exception if
     * adding the state would result in semantic inconsistency.
     * 
     * @param s
     * @throws ThinklabException
     */
    void addState(IState s) throws ThinklabException;

    /**
     * Subjects during contextualization may have a backing dataset for access to history. This may
     * return null.
     * 
     * @return
     */
    IDataset getBackingDataset();

}
