/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
// package org.integratedmodelling.api.modelling.parsing;
//
// import org.integratedmodelling.api.lang.IList;
// import org.integratedmodelling.api.modelling.IDirectObserver;
// import org.integratedmodelling.api.modelling.IModel;
//
// public interface IDirectObserverDefinition extends IDirectObserver, IModelObjectDefinition {
//
// /**
// * Set the definition of the observable that specifies the semantic object. It may be partial - functional
// * properties will be observed automatically.
// *
// * @param odef
// */
// void setObservable(IList odef);
//
// /**
// * Add a function that will be run when createSubject() is called to provide pre-observed state.
// *
// * * TODO move to parsing
// *
// * @param ff
// */
// void addObservationGeneratorFunction(IFunctionCall ff);
//
// /**
// * Force use of a specific model to choose when observing a given property, which should be functional.
// *
// * * TODO move to parsing
// * * TODO remove - don't think this should be used.
// *
// * @param property
// * @param observer
// */
// void addModelDependency(IKnowledgeDefinition property, IModel observer);
//
// }
