/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.api.modelling;

import java.util.Collection;

import org.integratedmodelling.api.lang.IMetadataHolder;
import org.integratedmodelling.exceptions.ThinklabException;

/**
 * A direct observation - subject, event or process.
 * 
 * @author Ferd
 *
 */
public interface IDirectObservation extends IObservation, IMetadataHolder {

    /**
     * Direct observations are stated in a namespace. This is necessary
     * so they can be resolved properly (using possibly customized 
     * resolution criteria). Normally this will be the namespace of
     * the observer that stated them, or the namespace of the parent
     * subject when generated outside.
     * 
     * @return
     */
    INamespace getNamespace();

    /**
     * All direct relationships have a name.
     * 
     * @return
     */
    String getId();

    /**
     * Return any states linked to this observation at its current location in space/time.
     * The subject should also store the properties that link the states to it.
     *
     * Although states link to IScale, which will have a temporal component for agents which experience time,
     * the states will only have meaning in one single time period within the subject's time scale.
     *
     * @return
     */
    Collection<IState> getStates();

    /**
     * Request a state for the passed observable, which must be a quality. The state is created
     * if not already present in the subject. Optional "data" (of any kind) may be passed and it
     * should be an error to provide them when the state already exists. If it does not and data
     * are passed, the state should be created from them, appropriately. For example, if a single
     * number is passed, the state should be created as a constant value. If data are not passed,
     * an empty (no-data) state capable of expressing values over the full scale should be returned.
     *
     * TODO this one should probably go in the "active" subclass as it's only relevant when the subject is
     * "live" (during conceptualization) - but for now it stays here for a simpler API in contextualizers.
     * 
     * 
     * @param obs
     * @return
     * @throws ThinklabException 
     */
    IState getState(IObservable observable, Object... data) throws ThinklabException;

    /**
     * Request a state for the passed observable, which must be a quality. The state is created
     * if not already present in the subject. This state will not change in time, so it can only
     * be written at initialization. It's important to use this one at initialize() when appropriate,
     * or much space will be wasted at runtime.
     * 
     * @param obs
     * @return
     * @throws ThinklabException 
     */
    IState getStaticState(IObservable observable) throws ThinklabException;

}
