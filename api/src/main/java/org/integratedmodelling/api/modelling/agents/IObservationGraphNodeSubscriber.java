/*******************************************************************************
 *  Copyright (C) 2007, 2014:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.api.modelling.agents;

import org.integratedmodelling.api.time.ITimeInstant;

public interface IObservationGraphNodeSubscriber {

    /**
     * invalidate any locally-held state (i.e. intermediate vector representation of an agent-state) as of the
     * interruptTime. This allows the collision mechanism in Thinklab to maintain consistency between the
     * (authoritative) Observation Graph and any cached and/or scale-mediated results which have been derived
     * from it.
     * 
     * @param interruptTime
     */
    public void invalidate(ITimeInstant interruptTime);

    /**
     * As agents progress forward in time, they update the Observation Graph, which then notifies all
     * listeners (i.e. scale mediators). This method handles one single state change, which is passed in as a
     * graph node containing agent state(s), state transition object, etc.
     * 
     * @param node
     */
    public void notify(IObservationGraphNode node);

}
