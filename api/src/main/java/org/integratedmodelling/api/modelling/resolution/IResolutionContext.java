/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.api.modelling.resolution;

import java.util.Collection;
import java.util.Set;

import org.integratedmodelling.api.knowledge.IConcept;
import org.integratedmodelling.api.knowledge.IExpression;
import org.integratedmodelling.api.knowledge.IKnowledge;
import org.integratedmodelling.api.knowledge.IProperty;
import org.integratedmodelling.api.metadata.IModelMetadata;
import org.integratedmodelling.api.modelling.ICoverage;
import org.integratedmodelling.api.modelling.IDataSource;
import org.integratedmodelling.api.modelling.IDependency;
import org.integratedmodelling.api.modelling.IDirectObservation;
import org.integratedmodelling.api.modelling.IModel;
import org.integratedmodelling.api.modelling.INamespace;
import org.integratedmodelling.api.modelling.IObjectSource;
import org.integratedmodelling.api.modelling.IObservable;
import org.integratedmodelling.api.modelling.IObserver;
import org.integratedmodelling.api.modelling.IScale;
import org.integratedmodelling.api.modelling.ISubject;
import org.integratedmodelling.api.modelling.scheduling.ITransition;
import org.integratedmodelling.api.monitoring.IMonitorable;
import org.integratedmodelling.api.project.IProject;
import org.integratedmodelling.exceptions.ThinklabException;
import org.integratedmodelling.lang.LogicalConnector;

/**
 * Created at ISubjectResolver.resolve() and passed around during resolution, notably to IObservationKbox.query().
 * Contains all the contextual information gathered during resolution, including scale, traits and resolution
 * criteria for all models being contextualized. The resolution contexts compute the total coverage, 
 * build the provenance graph and harmonize the merged scale as new models are accepted.
 * 
 * @author ferdinando.villa
 *
 */
public interface IResolutionContext extends IMonitorable {

    /**
     * Execute the resolution strategy computed for this transition. At initialization (with
     * transition == null), set any observations made into caches so they can be used in further 
     * resolution.
     * 
     * @param transition
     * @throws ThinklabException
     */
    void execute(ITransition transition) throws ThinklabException;

    /**
     * Scale of resolution. This may change as new constraints are brought in by each resolved
     * model, although it should remain fully included in the original scale and maximal in resolution.
     * The scale in the context passed to ISubjectResolver.resolve() should be the final one for the resolved model.
     * 
     * @return
     */
    IScale getScale();

    /**
     * Scenarios we're resolving into. These should be set in the root  context and passed around to 
     * child contexts without modification.
     * 
     * @return
     */
    Collection<String> getScenarios();

    /**
     * The model being resolved should only be null when resolving the "root" subject level.
     * @return
     */
    IModel getModel();

    /**
     * Return an appropriate prioritizer to choose a model among many.
     * 
     * @return
     */
    IModelPrioritizer<IModelMetadata> getPrioritizer();

    /**
     * Return the namespace of reference for this context. It should never be null; if we're resolving a
     * model's dependency, it should be the model's namespace, otherwise it should be that of the subject 
     * or concept we're resolving. The namespace provides semantic distance, ranking criteria, white/blacklist
     * for resolution, etc.
     * @return
     */
    INamespace getResolutionNamespace();

    /**
     * The direct observation being resolved; if we're resolving a quality, the subject that the quality is inherent to.
     * 
     * @return
     */
    IDirectObservation getSubject();

    /**
     * The property linking the current object being resolved to the subject it's inherent to, or 
     * null if we're resolving a root context.
     * 
     * @return
     */
    IProperty getPropertyContext();

    /**
     * Called from within the finish() method of a sub-context when the sub-context has enough coverage 
     * to be useful. Result will depend on what is being resolved. May be called multiple times. It is
     * always called with context arguments that have been returned by one of the forXXX methods on this.
     * 
     * In situations where accept() may be called multiple times, the coverage.isSufficient() method will
     * decide whether it is called again.
     * 
     * Must merge provenance and model cache from the passed context and link our root node to
     * the child. Must also harmonize the common scale being used across resolution with that, if any, of
     * the object just resolved.
     * 
     * @param child a context that was generated by one of the forXXX() methods.
     * @return the merged coverage after acceptance.
     * @throws ThinklabException 
     */
    ICoverage accept(IResolutionContext child) throws ThinklabException;

    /**
     * Called on the context when an accessor redefines the scale for a subject. This should set the
     * scale to the passed one, and if necessary use the monitor to complain of any incompatibilities.
     * @param scale
     */
    void forceScale(IScale scale);

    /**
     * Get the provenance graph containing the outcome of resolution so far.
     * 
     * @return
     */
    IProvenance getProvenance();

    /**
     * Get the closure of all possible observables for the passed observable in this context.
     * 
     * @param observable
     * @return
     * @throws ThinklabException 
     */
    Set<IKnowledge> getObservableClosure(IObservable observable) throws ThinklabException;

    /**
     * Return the final coverage for this resolution. 
     * 
     * @return
     */
    ICoverage getCoverage();

    /**
     * If the context is for a subject that is in the context of another, return the other,
     * otherwise return null.
     * 
     * @return
     */
    ISubject getContextSubject();

    /**
     * Return the traits that our resolved target is expected to inherit.
     * @return
     */
    Set<IConcept> getTraits();

    /**
     * Call after resolution of this context to merge in results into the parent context. If the
     * merged coverage is acceptable, the parent's accept() is called with this context as an
     * argument.
     *
     * @return the merged coverage of this context. Never null.
     * @throws ThinklabException 
     */
    ICoverage finish() throws ThinklabException;

    /**
     * Get a context that accepts models according to how much additional coverage they
     * provide. May have a model to start with (subjects), in which case sets the coverage
     * appropriately at creation. 
     */
    IResolutionContext forObservable(IObservable observable);

    /**
     * Same as forObservable but allows to specify scenarios for this particular resolution.
     * @param observable
     * @return
     */
    IResolutionContext forObservable(IObservable observable, Collection<String> scenarios);

    IResolutionContext forDependency(IDependency dependency, INamespace namespace);

    IResolutionContext forCondition(IExpression expression, int n);

    IResolutionContext forModel(IModel model);

    IResolutionContext forSubject(IDirectObservation subject);

    IResolutionContext forSubject(IDirectObservation subject, Collection<String> scenarios);

    IResolutionContext forObserver(IObserver observer, IDataSource datasource);

    IResolutionContext forMediatedObserver(IObserver observer);

    /**
     * Context for specific datasource (will intersect the coverage with that of the DS). The model
     * in the context may be restricting the DS's coverage.
     * 
     * @param datasource
     * @param model
     * @return
     * @throws ThinklabException
     */
    IResolutionContext forDatasource(IDataSource datasource) throws ThinklabException;

    /**
     * Context for specific objectsource (will intersect the coverage with that of the OS). The model
     * in the context may be restricting the OS's coverage.
     * 
     * @param datasource
     * @param model
     * @return
     * @throws ThinklabException
     */
    IResolutionContext forObjectSource(IObjectSource objectsource) throws ThinklabException;

    /**
     * Serves as the joining context for a group of dependencies or conditional observers, linking 
     * the resolved ones to the parent context's node. The group coverage may be in AND or in OR
     * according to the calling context; if OR, additional coverage rather than total coverage is
     * the base for an additional acceptance choice within accept().
     * 
     * @param dependencies
     * @param connector
     * @return
     */
    IResolutionContext forGroup(LogicalConnector connector);

    /**
     * If true, this context is trying to resolve an observable for direct observations that have not
     * been instantiated, i.e. it will be resolved by models that instantiate them ('model each' models). 
     * If false, we already have an instance, and we're looking for a model that can complete its
     * observation.
     * 
     * @return
     */
    boolean isForInstantiation();

    /**
     * If we have resolved this observable before, or can resolve it based on pre-existing states of our
     * subject, link it up and return the resulting coverage. Otherwise return null.
     * 
     * @param observable
     * @param matchInstantiators if true, search for models that will create instances of the (direct) observable; otherwise
     *        find models that will observe a pre-existing instance.
     * @return
     */
    IModel getModelFor(IObservable observable, boolean matchInstantiators);

    /**
     * Any metadata set into the resolution context will be passed on to the provenance
     * node it's handling, when the resolution is successful.
     * 
     * @param key
     * @param value
     */
    void setMetadata(String key, Object value);

    /**
     * true if we're downstream of an optional dependency.
     * 
     * @return
     */
    boolean isOptional();

    /**
     * Call after successful resolution to retrieve the resolution strategy that will
     * initialize the observation.
     * 
     * @return
     */
    IResolutionStrategy getResolutionStrategy();

    /**
     * Accept the current provenance graph as a resolution step to observe this context.
     *  
     * @throws ThinklabException
     */
    void defineWorkflow() throws ThinklabException;

    /**
     * Use the current model to provide a resolution step if it creates objects or has a non-empty
     * process accessor.
     */
    void acceptModel();

    /**
     * Return how far away in the resolution is the passed namespace; -1 if not found.
     * 
     * @param ns
     * @return
     */
    int getNamespaceDistance(INamespace ns);

    /**
     * Return how far away in the resolution is the passed namespace; -1 if not found.
     * 
     * @param ns
     * @return
     */
    int getProjectDistance(IProject ns);

}
