///*******************************************************************************
// *  Copyright (C) 2007, 2015:
// *  
// *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
// *    - integratedmodelling.org
// *    - any other authors listed in @author annotations
// *
// *    All rights reserved. This file is part of the k.LAB software suite,
// *    meant to enable modular, collaborative, integrated 
// *    development of interoperable data and model components. For
// *    details, see http://integratedmodelling.org.
// *    
// *    This program is free software; you can redistribute it and/or
// *    modify it under the terms of the Affero General Public License 
// *    Version 3 or any later version.
// *
// *    This program is distributed in the hope that it will be useful,
// *    but without any warranty; without even the implied warranty of
// *    merchantability or fitness for a particular purpose.  See the
// *    Affero General Public License for more details.
// *  
// *     You should have received a copy of the Affero General Public License
// *     along with this program; if not, write to the Free Software
// *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
// *     The license is also available at: https://www.gnu.org/licenses/agpl.html
// *******************************************************************************/
//package org.integratedmodelling.api.modelling.runtime;
//
//import java.util.List;
//
//import org.integratedmodelling.api.modelling.IDependency;
//
///**
// * An interface that, if adopted by an actuator, will cause the contextualizeDependencies() 
// * method to be called, allowing to contextualize the dependencies from the model and the
// * semantics and to modify the list of dependencies according to context.
// * 
// * @author ferdinando.villa
// *
// */
//public interface IContextualActuator {
//
//    /**
//     * Examine the dependencies and if necessary, modify them. Whatever is returned
//     * by this method is the final list of dependencies Thinklab will go with. Easy
//     * to screw things up here, so use with care.
//     * 
//     * @param dependencies
//     * @return
//     */
//    List<IDependency> contextualizeDependencies(List<IDependency> dependencies);
//
//}
