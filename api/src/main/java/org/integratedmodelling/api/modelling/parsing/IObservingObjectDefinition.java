/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
// package org.integratedmodelling.api.modelling.parsing;
//
// import org.integratedmodelling.api.knowledge.IConcept;
// import org.integratedmodelling.api.knowledge.IExpression;
// import org.integratedmodelling.api.lang.IModelResolver;
// import org.integratedmodelling.api.modelling.IFunctionCall;
// import org.integratedmodelling.api.modelling.IKnowledgeObject;
//
// public abstract interface IObservingObjectDefinition extends IModelObjectDefinition {
//
// /**
// * Dependencies must be stated in terms of qualities or objects whose observation semantics is known. The
// * observed object (first parameter) can therefore be a model or an observation concept that can be linked
// * to one. If the object is a concept, we may also pass a trait and a subject, which together define a
// * dependency on a classification by trait.
// *
// * When the dependency is on subjects (with the option of using the 'at each' construct to define
// * a context generator model) the subject concept should be stated and a model will be found using the
// * common heuristics.
// *
// * It's a messy call - maybe improve one day, for now it's OK as is.
// *
// * @param model or concept - main observable
// * @param trait if not null, model must be a concept and the dependency is for a classification of it
// according to this trait.
// * @param subject if not null, model must be a concept and the dependency is inherent to this subject (can
// only happen when trait != null).
// * @param formalName may be null; needs to be checked for ambiguity
// * @param property
// * @param isOptional
// * @param distribute
// * @param generic
// * @param whereCondition
// * @param whenCondition
// * @param contextGenerator
// */
// public void addDependency(Object model, IKnowledgeObject trait, IKnowledgeObject subject, String
// formalName, IKnowledgeObject property, boolean isOptional, boolean generic, Object contextGenerator, Object
// whereCondition, IKnowledgeObject reinterpretAs, IModelResolver resolver, int lineNumber);
//
// /**
// * Specific extents may be mentioned in action specifications to trigger actions on transition. We set
// * function calls so they can be created at initialization and not at parsing. This one must return the
// * domain concept of the extent that the function adds - which may require to call it, according to
// * implementation. Avoid that if possible.
// *
// * @param extentGenerator
// * @return the domain concept for the extent. If this returns null, the extent and all the corresponding
// * actions will be ignored.
// */
// public IConcept addExtentConstraintFunction(IFunctionCall extentGenerator);
//
// /**
// * Accessors are not created directly in the language, but referred to through an external function
// * definition which may have parameters.
// *
// * @param function
// */
// public void setAccessorGeneratorFunction(IFunctionCall function);
//
// /**
// * Add an action, consisting of a "what to do" and a "when to do it" parts. Both are IExpressions, defined
// * with appropriate receivers and arguments so that they do their thing without further intervention.
// *
// * @param target the string key of the object that this action will change, if any. May be null for
// * side-effect actions.
// * @param type one of the numeric constants in IObservingObject, to indicate which action type we target
// * @param action what to do - never null.
// * @param condition when to do it - returns boolean and may be null.
// * @param domains the domains of the events that trigger this action. Null means initialization.
// */
// public void addAction(String target, int type, IExpression action, IExpression condition, int lineNumber,
// IConcept[] domains);
//
// /**
// * Flag the observer as capable of computing its state autonomously or through the dependencies, but
// * without being linked to other observations for its main observable. This applies (I think) only to data
// * observer for now.
// *
// * @param b
// */
// public void setComputed(boolean b);
//
// /**
// * States that this object represents qualities that are integrated over the domains adopted explicitly.
// * Units of measurement etc should be validated accordingly, and mediation should consider that. It is
// * only relevant for observers as aggregation of this kind can only apply to qualities. For this reason,
// * the corresponding getter is in IObserver.
// *
// * @param aggr
// */
// public void setIntegrated(boolean aggr);
//
// /**
// * Set the type of the object our observed quality is inherent to. This corresponds to a DOLCE inherent-in
// * restriction on the observable (follows an 'of' specification), e.g. 'presence of' or 'measure
// * im.geo:ElevationSeaLevel of im.geo:Mountain'.
// *
// * @param concept
// * @param objectGenerator
// */
// public void setInherentSubjectType(IConcept concept);
//
// }
