/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.api.modelling;

import java.util.List;

import org.integratedmodelling.api.knowledge.IConcept;
import org.integratedmodelling.api.knowledge.ISemantic;
import org.integratedmodelling.api.modelling.runtime.IActuator;
import org.integratedmodelling.api.monitoring.IMonitor;
import org.integratedmodelling.exceptions.ThinklabException;

/**
 * Anything that observes anything must implement this interface, normally through another that extends it. At the moment
 * all observers and IModel are subinterfaces of this one.
 * 
 * @author Ferd
 * 
 */
public abstract interface IObservingObject extends IModelObject, ISemantic {

    /**
     * Observing objects may need to observe something else in order to make their observation.
     * 
     * @return
     */
    List<IDependency> getDependencies();

    /**
     * Given the observable for a known observable (output or dependency) and an extent implemented, return whether the model has
     * any actions that modify the state of that observable at the correspondent scale transitions.
     *  
     * @param observable
     * @param domainConcept
     * @return
     */
    boolean hasActionsFor(IConcept observable, IConcept domainConcept);

    /**
     * If a model was given a specific coverage in any extent, either directly through its actions or through
     * a namespace-wide specification, return the extent generators for the scale that expresses that coverage. 
     * If no coverage has been specified, return an empty list.
     * 
     * @return
     * @throws ThinklabException
     */
    IScale getCoverage();

    /**
     * Return any action given in the specifications. If none was given, return an empty list.
     * 
     * @return
     */
    List<IAction> getActions();

    /**
     * This one is fundamental for proper operation: if it returns false, the observer is expected to be
     * linked to a source of states for its concept (data or other model). If it returns true, the observer is
     * treated as capable of computing its data based only on its dependencies, and no resolution of the
     * observable is attempted.
     * 
     * @return
     */
    boolean isComputed();

    /**
     * Return the observable (i.e. the dolce:quality) being observed. In IModel, this returns 
     * the first observable even if the model has more than one.
    
     * @return
     */
    IObservable getObservable();

    /**
     * Return the function call that produces the actuator, if any. This is the validated result of what
     * is given after the 'using' clause in k.IM.
     * 
     * @return
     * @throws ThinklabException 
     */
    IActuator getActuator(IMonitor monitor) throws ThinklabException;

}
