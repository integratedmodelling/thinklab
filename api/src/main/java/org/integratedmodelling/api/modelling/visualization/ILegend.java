/*******************************************************************************
 *  Copyright (C) 2007, 2014:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.api.modelling.visualization;

/**
 * A Legend is a specialized media that makes the associated media understandable. Not all media
 * need legends.
 * 
 * @author Ferd
 *
 */
public interface ILegend extends IMedia {

    /*
     * flags for description types
     */
    /**
     * Short description (40 characters or less)
     */
    public static int SHORT_DESCRIPTION = 0;

    /**
     * Long description (arbitrary length)
     */
    public static int VERBOSE_DESCRIPTION = 1;

    /**
     * One word
     */
    public static int LABEL = 2;

    /**
     * Same colormap returned by IMedia.getColormap(), for convenience.
     * @return
     */
    public IColormap getColormap();

    /**
     * Obtain a textual description of the passed object. The object will usually
     * be returned by IMedia.getValue(). Flags can be used to customize the description.
     * 
     * @param data
     * @param type one of the constants above
     * @return
     */
    public String getDescription(Object data, int type);

    /**
     * Obtain a textual description of the passed type for the whole object described
     * by this.
     * 
     * @param flags
     * @return
     */
    public String getDescription(int type);

    /**
     * Get the object at the given offset.
     * 
     * @param offset
     * @return
     */
    public Object getValue(int offset);
}
