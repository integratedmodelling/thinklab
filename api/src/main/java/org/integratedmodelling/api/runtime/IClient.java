/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.api.runtime;

import java.io.File;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.integratedmodelling.api.auth.IUser;
import org.integratedmodelling.api.engine.IEngine;
import org.integratedmodelling.api.engine.IModelingEngine;
import org.integratedmodelling.api.modelling.IDirectObserver;
import org.integratedmodelling.api.modelling.IExtent;
import org.integratedmodelling.api.modelling.IModelObject;
import org.integratedmodelling.api.modelling.INamespace;
import org.integratedmodelling.api.modelling.ISubject;
import org.integratedmodelling.api.modelling.scheduling.ITransition;
import org.integratedmodelling.api.monitoring.INotification;
import org.integratedmodelling.api.monitoring.ITaskIntrospector;
import org.integratedmodelling.api.project.IProject;
import org.integratedmodelling.api.ui.IBookmark;
import org.integratedmodelling.exceptions.ThinklabException;
import org.integratedmodelling.exceptions.ThinklabIOException;

public interface IClient {

    /**
     * Communicates any relevant events to the user side. Most operations in the client
     * are managed through this callback, which a client is expected to have installed
     * from construction.
     * 
     * @author Ferd
     *
     */
    public static interface Listener {

        /**
         * Called upon authentication; user may be anonymous if failed or no certificate.
         * 
         * @param user
         */
        public void notifyUser(IUser user);

        /**
         * Debug message. If task isn't null, it's about this task.
         * @param o
         * @param task
         */
        void debug(Object o, ITask task);

        /**
         * Error message, exception or other object. If task isn't null, it's about this task.
         * @param o
         * @param task
         */
        void error(Object error, ITask task);

        /**
         * Info message. If task isn't null, it's about this task.
         * @param o
         * @param task
         */
        void info(Object info, ITask task);

        /**
         * Warning message. If task isn't null, it's about this task.
         * @param o
         * @param task
         */
        void warn(Object o, ITask task);

        /**
         * 
         * @param server
         */
        void notifyEngineBooting(IEngine engine);

        /**
         * 
         * @param server
         */
        void notifyEngineOperational(IEngine engine);

        /**
         * 
         * @param server
         */
        void notifyEngineStopped(IEngine engine);

        /**
         * 
         * @param server
         */
        void notifyEngineFailure(IEngine engine);

        void notifyTaskStarted(ITask task);

        void notifyTaskFinished(ITask task);

        public void notifyTaskInterrupted(ITask task);

        /**
         * Called when a context has been computed. 
         */
        void notifyContextAvailable(IContext context);

        /**
         * Notify a temporal transition computed.
         * 
         * @param context
         * @param start
         * @param end
         */
        void notifyTimeTransition(IContext context, ITransition transition);

        /**
         * Called at startup (after server initialization) and any changes in client properties.
         * 
         * @param substring the property ID without the thinklab.client prefix.
         * @param property property value.
         */
        void notifyClientProperty(String substring, String property);

        /**
         * Called by file monitor when a new namespace is created.
         * 
         * @param namespaceId
         * @param project
         */
        void notifyNamespaceCreated(String namespaceId, IProject project);

        /**
         * Called by file monitor when a namespace is deleted.
         * @param namespaceId
         * @param project
         */
        void notifyNamespaceDeleted(String namespaceId, IProject project);

        /**
         * Called by file monitor when a namespace modification causes a chain of dependent
         * namespaces to be reloaded. The root cause of the modification is included in the
         * list and is also passed separately. This is called after the model manager has
         * updated.
         * 
         * @param namespaceId
         * @param project
         */
        void notifyNamespacesModified(Collection<INamespace> namespaces, INamespace root);

        /**
         * Called when a project's properties are modified, after a modified
         * property file is read.
         * 
         * @param project
         * @param file The file that was modified. Can be thinklab.properties or .project for now. 
         * Actual project properties are only changed if the file is thinklab.properties.
         */
        void notifyPropertiesChanged(IProject project, File file);

        /**
         * Called when user activates/deactivates any scenarios. 
         * @param _currentScenarios
         */
        public void scenariosChanged(Set<INamespace> currentScenarios);

        /**
         * User has bookmarked a model object. Do what she expects.
         * 
         * @param bookmark
         */
        void notifyBookmarkAdded(IBookmark bookmark);

        /**
         * Called whenever a polling cycle updates the status of the server. If this
         * implies a change of status beyond uptime and other obvious time-changing
         * parameters, one of the other notifiers will also be called. Use 
         * IEngine.getStatus() to retrieve the status.
         * 
         * @param engine
         */
        void notifyEngineStatusUpdated(IEngine engine, Map<?, ?> status);

        /**
         * If this is not null, the introspector will be passed any info about the running
         * task that the engine sends us.
         * 
         * @return
         */
        ITaskIntrospector getTaskIntrospector();

        /**
         * Called after the initializeComponents() call was issued. That usually
         * makes available more services published as functions, so we want to 
         * reload the workspace.
         * 
         * @param engine
         */
        void notifyComponentsInitialized(IEngine engine);

        /**
         * Called periodically (every 30 seconds as a default) to notify whether the primary server is responding.
         * In order for this to be called, pollNetworkStatus() must be called once.
         * 
         * @param urlResponds
         */
        public void notifyNetworkStatus(boolean urlResponds);

        /**
         * Called before components are initialized - if there are many components, there may be a need for
         * telling the user.
         * 
         * @param engine
         */
        public void notifyEngineInitializing(IEngine engine);

    }

    /**
     * Observe the passed subject generator, incarnating the observation it
     * specifies. Returns immediately and produce a task, already running, which
     * if successful will communicate the resulting context through the 
     * listener when the engine has resolved and computed it.
     * 
     * See IEngineClient.observe() for the meaning of the forceScale extents.
     * 
     * Further observations are made directly by calling observe() on the
     * subject(s) in the context.
     * 
     * @throws ThinklabException 
     */
    ITask observe(IDirectObserver objectName, IExtent... forceScale) throws ThinklabException;

    /**
     * 
     * @param observable
     * @param context
     * @param reset
     * @return
     * @throws ThinklabException
     */
    ITask observe(Object observable, ISubject context) throws ThinklabException;

    /**
     * 
     */
    void shutdown();

    /**
     * Boot the client, reading the workspace and indexing the knowledge. According to configuration,
     * this may entail starting an embedded server and connecting to it (or to a remote one).
     * 
     * @throws ThinklabException
     */
    void boot() throws ThinklabException;

    /**
     * Return the current server, which may be managing more than one session at
     * once. Server operations should be called on the returned server; the client
     * listener is called back by the server whenever anything relevant happens.
     * 
     * @return
     */
    IModelingEngine getEngine();

    /**
     * Local project workspace, automatically synchronized with server and cloud services. 
     * May be null.
     * 
     * @return
     */
    IWorkspace getWorkspace();

    /**
     * The client is expected to hold a knowledge index. TODO expose the index after having
     * defined its API.
     */
    void reindexKnowledge();

    /**
     * Client maintains bookmarks, which can be observed just like model objects and
     * knowledge.
     * 
     * @param modelObject
     * @param name
     * @param description
     * @throws ThinklabIOException 
     */
    void bookmark(IModelObject modelObject, String name, String description) throws ThinklabIOException;

    /**
     * Client can run Thinklab (Groovy) scripts.
     * 
     * @param scriptFile
     */
    void run(File scriptFile);

    /**
     * Activate or deactivate a particular scenario. Returns all the active scenarios. Listener
     * is notified.
     * 
     * @param element
     * @param active
     * @return
     */
    Collection<INamespace> setScenario(INamespace element, boolean active);

    /**
     * Reset all scenarios activated so far. Listener is notified.
     * 
     */
    void resetScenarios();

    /**
     * Get all the notifications pertaining to a particular task.
     * 
     * @param task
     * @return
     */
    List<INotification> getNotifications(ITask task);

    /**
     * A client always has a user (at worst this returns an anonymous user).
     * 
     * @return
     */
    IUser getUser();

    /**
     * Current session if there is one.
     * 
     * @return
     */
    ISession getCurrentSession();

}
