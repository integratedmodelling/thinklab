/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.api.runtime;

import java.io.File;
import java.util.Collection;
import java.util.List;

import org.integratedmodelling.api.auth.IUser;
import org.integratedmodelling.api.monitoring.IMonitor;
import org.integratedmodelling.api.network.INetwork;
import org.integratedmodelling.api.project.IProject;
import org.integratedmodelling.exceptions.ThinklabException;

/**
 * A workspace exists in a client environment and refers to a directory (or more than one) where projects
 * are found. The workspace registers every project with the project manager and has callbacks for project
 * creation and deletion. Fine-grained event control for workspace modification is provided by the project
 * manager.
 * 
 * A workspace is created on client startup and returned by the IClient.getWorkspace() call if the client
 * property "thinklab.client.workspace" is given a valid value. Workspace changes are communicated to
 * the running engine(s). The project manager updates the workspace when projects are registered that the
 * workspace didn't know about.
 * 
 * @author ferdinando.villa
 *
 */
public interface IWorkspace {

    /**
     * Directories on the local filesystem where each project managed under this workspace. Each project
     * has its own individual location - there is no requirement for a "root" workspace directory.
     * 
     * @return
     */
    Collection<File> getProjectLocations();

    /**
     * Create an empty project with this ID in this workspace.
     * 
     * @param project
     * @return
     */
    IProject create(String project);

    /**
     * Delete the passed project - no warnings or anything, so make sure you call this
     * when decided.
     * 
     * @param project
     */
    void delete(String project);

    /**
     * The location where to create a new project. 
     * 
     * @return
     */
    File getDefaultFileLocation();

    /**
         * Call after initialization and authentication. The projects are synchronized in any case, but
         * only included in the workspace if there isn't a client-visible project
         * that overrides them. The properties will contain the IProject.REMOTELY_SYNCHRONIZED_PROPERTY
         * true/false flag so that the isRemote() project method returns true.
         * 
         * @param user
         * @return the list of imported project IDs, irrespective of whether they will be used or not.
         * @throws ThinklabException 
         */
    List<String> synchronizeUserProjects(IUser user, IMonitor monitor) throws ThinklabException;

    /**
     * Synchronize a project from the network, finding the authoritative node that distributes it. 
     * Return the file location of the project so it can be registered, or null if it cannot be 
     * synchronized.
     * 
     * @param projectId
     * @param network
     * @return
     * @throws ThinklabException
     */
    File synchronizeNetworkProject(String projectId, INetwork network) throws ThinklabException;

    /**
     * True if the passed project is one of those that are remotely synchronized based on the
     * groups the current user belongs to. If so, it should be considered read-only and loaded 
     * but not visualized in any IDE. 
     * @param projectId
     * @return
     */
    boolean isRemotelySynchronized(String projectId);
}
