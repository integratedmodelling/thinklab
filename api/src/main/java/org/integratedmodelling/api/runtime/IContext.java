/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.api.runtime;

import java.io.File;
import java.util.List;

import org.integratedmodelling.api.modelling.ICoverage;
import org.integratedmodelling.api.modelling.IObservation;
import org.integratedmodelling.api.modelling.ISubject;
import org.integratedmodelling.api.modelling.visualization.IMedia;
import org.integratedmodelling.api.monitoring.IMonitorable;
import org.integratedmodelling.exceptions.ThinklabException;

/**
 * The context holds the state of an ongoing observation process. Each observation in it has a path that can 
 * be used to identify it. Contexts are initialized with a ISubjectObserver, and each further observation
 * made "in" them becomes part of an observation tree starting at the subject observed by that, and gets
 * a unique path with which it can be referred to.
 * 
 * @author ferdinando.villa
 * 
 */
public interface IContext extends IMonitorable {

    /**
     * Each context has an ID that's unique within a session.
     * 
     * @return
     */
    long getId();

    /**
     * True if the context has been created without any concrete observation in it. It may contain abstracts
     * (space, time etc) or not at this stage, but no root object.
     * 
     * @return
     */
    boolean isEmpty();

    /**
     * Contexts take their name from the root subject they observe. The name does not need to be unique and
     * will appear in visualizations, so it should be user-acceptable and informative.
     * 
     * @return
     */
    String getName();

    /**
     * True if context is not empty and all states have been fully contextualized; this means that either
     * there is no time scale, or if so, run() was called to complete observation in time and has finished. It
     * will also return true if the schedule was interrupted from the monitor or by internal error conditions.
     * 
     * Monitor functions should be used to understand whether the execution had errors or was interrupted
     * before accessing states and objects.
     * 
     * @return
     */
    boolean isFinished();

    /**
     * True if run() has been called and isFinished() returns false.
     * 
     * @return
     */
    boolean isRunning();

    /**
     * Return a proxy to this context that will make all the observations called on it affected by the passed
     * scenarios. It will reset any scenarios previously defined.
     * 
     * @param scenarios
     * @return a new IContext that wraps the original object, but whose model choices will be affected by the
     * scenarios. The states created will be in the same subject.
     */
    IContext inScenario(String... scenarios);

    /**
     * Observe this context, which has been made from an initial observation. This involves computing
     * the root observation and all the dependencies implied by its semantics. Further observations can
     * be made directly on the subject returned by getSubject().
     * 
     * @return self to enable fluent idioms. Implementations may be as functional as they want here, but a
     * IContext is all about building state incrementally, so no need to make it return a new object.
     * @throws ThinklabException 
     */
    IContext observe() throws ThinklabException;

    /**
     * Run all temporal transitions (or do nothing if there is no time in the scale). Call only on
     * the root context or be punished. Can only be called once. Blocks until all transitions have
     * ran or the root subject has died. Most applications except scripts will want to use the 
     * asynchronous version, as this may run for very long times. After this runs, isFinished() will
     * return true. Any errors and events will be communicated to the monitor in the context.
     * 
     * @return the finalized context, which is very likely the same one this was called on.
     * 
     * @throws ThinklabException
     */
    IContext run() throws ThinklabException;

    /**
     * If context is empty, set it to passed observable (which must be a ISubjectGenerator or an endurant
     * concept); else observe the passed observable in this context. Return the task associated with the
     * observation, which has been started and may be finished or ongoing.
     * 
     * @param observable
     * @return self to enable fluent idioms. Implementations may be as functional as they want here, but a
     * IContext is all about building state incrementally, so no need to make it return a new object.
     * @throws ThinklabException 
     */
    ITask observeAsynchronous() throws ThinklabException;

    /**
     * Start contextualization by creating the temporal observation strategy and running it. This may do
     * nothing if there is no time scale or contextualization has already happened.
     * 
     * It should operate asynchronously and return without blocking. It is possible to observe() things while
     * contextualization is ongoing. The time scale may be infinite (real time) - in such cases, the monitor
     * is used to interrupt explicitly.
     * 
     * It is an error to call run() on anything other than the root context for an observation session. If a
     * dependent subject exists when run() is called, that is run as well; if a new subject is observed within
     * this context and contextualization is running, it should be initialized, inserted and run immediately.
     * @throws ThinklabException
     */
    ITask runAsynchronous() throws ThinklabException;

    /**
     * Get the coverage after initialization or after of the last operation on the context.
     * 
     * @return
     */
    ICoverage getCoverage();

    /**
     * The root subject.
     * 
     * @return
     */
    ISubject getSubject();

    /**
     * Return all the tasks that have applied to this context in order of execution.
     * 
     * @return
     */
    List<ITask> getTasks();

    /**
     * Get the path of the passed observation in this context, or null if the observation is not
     * part of this context.
     * 
     * @param observation
     * @return
     */
    String getPathFor(IObservation observation);

    /**
     * Return the subject or state identified by the passed path. Paths are returned by {@link
     * getPathFor()}. Used to identify observations in an observation context.
     * 
     * @param path
     * @return
     */
    IObservation get(String path);

    /**
     * Persist to passed file (may be a directory, according to implementation). A new read-only IContext
     * should be able to be reconstructed from the resulting file.
     * 
     * Options is reserved for the many objects that may affect artifacts produced: a viewport,
     * slice locators, etc. Hard to predefine those. Implementations, be good with those.
     * 
     * @param file
     * @param path persist the passed observation or the whole context if path is null or "/".
     */
    void persist(File file, String path, IMedia.Type mediaType, Object... options) throws ThinklabException;

    /**
     * If true is passes, return a context that will reset the root subject to a pristine observation before
     * making the next one. Otherwise just return self.
     * 
     * @param reset
     * @return
     */
    IContext resetContext(boolean reset);

    /**
     * Contexts exist in a session.
     * 
     * @return
     */
    ISession getSession();

}
