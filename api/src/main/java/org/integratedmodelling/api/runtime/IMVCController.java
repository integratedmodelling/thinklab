/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.api.runtime;

import java.util.Map;

/**
 * The interface for a Thinklab controller in a MVC sense. The common package provides a
 * tiny REST server that can be bound to a IController, so that the actions of a user in an
 * independent UI interface (like the future GWT runtime or the embedded spatial editor
 * in ThinkCap) can be handled.
 * 
 * @author Ferd
 *
 */
public interface IMVCController {

    public static final int DEFAULT_PORT = 8109;

    /**
     * Process command and respond to visualization. Respond null if no response is required.
     * 
     * @param command
     * @param arguments
     * @return
     */
    public Object commandReceived(String command, Map<String, String> arguments);

}
