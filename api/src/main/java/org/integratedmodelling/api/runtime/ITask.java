/*******************************************************************************
 *  Copyright (C) 2007, 2014:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.api.runtime;

/**
 * A ITask is the process corresponding to an atomic observation in a context.
 * It is returned by {@link IContext.observe()} immediately; it will return 
 * a result when finished. If a synchronous behavior is desired, finish() will
 * return when the computation is finished, and return the context so that
 * other observations can be called on it (context.observe().finish().observe())
 * 
 * A task may create its own context. This is because we may not know the observable
 * the moment the task is created: all tasks should update the model space before 
 * resolving the observable. So the first task will have no context associated until
 * finished.
 *  
 * @author ferdinando.villa
 *
 */
public interface ITask {

    public static enum Status {
        WAITING,
        RUNNING,
        INITIALIZING,
        FINISHED,
        ERROR,
        INTERRUPTED
    }

    /**
     * Peek current status and return it.
     * @return
     */
    Status getStatus();

    /**
     * Task have an ID that's unique within a session. getId() would be a better
     * name, but at the server side we want tasks to be threads, and those have
     * a conflicting getId() that we don't want to mess with.
     * 
     * @return
     */
    long getTaskId();

    /**
     * ID of the user session that originated the task, or null if it's a system task.
     * 
     * @return
     */
    String getSessionId();

    /**
     * The command that has started the task. May be null.
     * 
     * @return
     */
    String getCommand();

    /**
     * Task description. May be null.
     * 
     * @return
     */
    String getDescription();

    /**
     * Interrupt task at the first chance. Task cannot be resumed
     * after this. Use the listener to check for actual interruption.
     * 
     */
    void interrupt();

    /**
     * Return the context this task belongs to. Will return null until the context is
     * set.
     * 
     * @return
     */
    IContext getContext();

    /**
     * Wait until end of task and return the context after the
     * task has completed.
     * 
     * @return
     */
    IContext finish();

    /**
     * Return the start time. If the task is waiting, return when we started waiting.
     * 
     * @return
     */
    long getStartTime();

    /**
     * Return the end time. This is only not zero if the task status is finished.
     * 
     * @return
     */
    long getEndTime();

}
