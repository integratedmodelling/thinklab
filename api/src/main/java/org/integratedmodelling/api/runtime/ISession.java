/*******************************************************************************
 *  Copyright (C) 2007, 2014:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.api.runtime;

import java.io.Closeable;
import java.io.InputStream;
import java.io.PrintStream;
import java.util.List;

import org.integratedmodelling.api.auth.IUser;
import org.integratedmodelling.api.monitoring.IMonitorable;
import org.integratedmodelling.api.monitoring.INotification;

/**
 * A Session is a temporary concept space that contains all instances that are created during a user session. A session should be
 *  an ontology or contain one, but its identity as a IOntology is not mandated. Sessions are normally memory-based but should be 
 *  able to persist themselves. All operations on the same session are synchronous so there's no need to worry about concurrency.
 * 
 * @author  Ferdinando Villa
 */
public interface ISession extends IMonitorable, Closeable {

    /**
     * An output to write to - according to the type of session it may be a terminal, 
     * a logger or a null stream. Should not be null - it's always legal to write to
     * it.
     * 
     * @return
     */
    PrintStream out();

    /**
     * Input to read user's. May be a null stream, should not be null (although reading
     * from a null stream may have unintended consequences - TODO)
     * 
     * @return
     */
    InputStream in();

    /**
     * Each session has a unique ID 
     */
    String getId();

    /**
     * User is never null, although it may be anonymous.
     * 
     * @return
     */
    IUser getUser();

    /**
     * Register a notification. Used internally and by any IMonitor object passed to task operations.
     * 
     * @param notification
     */
    void notify(INotification notification);

    /**
     * Return all notifications available for this session. Clear the notifications if true is 
     * passed, so that they won't be reported at the next call.
     * 
     * @param clear
     * @return
     */
    List<INotification> getNotifications(boolean clear);

    /**
     * Return the most current context created. If no observation were made or the last context has
     * expired, return null without error.
     * 
     * @return
     */
    IContext getContext();

    /**
     * Return all the contexts in the history of the session in order of creation (i.e., the "current" 
     * context will be last). These may be less than the ones actually computed if the history size is 
     * limited by the implementation.
     * 
     * @return
     */
    List<IContext> getContexts();

    /**
     * Return the context with the specified ID. It may return null even for a valid ID if the context has
     * expired.
     * 
     * @return
     */
    IContext getContext(long contextId);

    /**
     * Return the list of all tasks that we have executed.
     * 
     * @return
     */
    List<ITask> getTasks();
}
