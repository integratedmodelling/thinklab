/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.api.persistence;

import java.util.List;

import org.integratedmodelling.exceptions.ThinklabException;

/**
 * Overhaul of the old semantically-driven kbox. An object persister that knows about semantics but
 * does not need it.
 * 
 * @author Ferd
 */
public interface IKbox {

    /**
     * Query kbox. The list returned should be read-only and of course implement lazy access. 
     * Sorting, grouping or any other query option should be specified within the query
     * object, using metadata or other strategy.
     * 
     * @param query
     * @return
     * @throws ThinklabException
     */
    public List<Object> query(IQuery query) throws ThinklabException;

    /**
     * Store object, return handle. Any object can be passed, as long as it can be
     * annotated. Return an ID that can be passed to retrieve() to reconstruct the
     * object.
     * 
     * @param o
     * @return
     * @throws ThinklabException
     */
    public abstract long store(Object o) throws ThinklabException;

    /**
     * Retrieve the object pointed to by this id, or null if it's not there.
     * 
     * @param o
     * @return
     * @throws ThinklabException
     */
    public abstract Object retrieve(long id) throws ThinklabException;

    /**
     * Remove object identified by handle
     * 
     * @param handle
     * @throws ThinklabException
     */
    public abstract void remove(long id) throws ThinklabException;

    /**
     * Remove all objects matching the query.
     * 
     * @param query
     * @throws ThinklabException 
     */
    public abstract void remove(IQuery query) throws ThinklabException;

    /**
     * Remove everything in kbox.
     * 
     * @throws ThinklabException
     */
    public abstract void clear() throws ThinklabException;

}
