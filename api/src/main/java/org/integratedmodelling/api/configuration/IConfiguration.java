/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.api.configuration;

import java.io.File;
import java.util.Properties;

import org.integratedmodelling.collections.OS;

/**
 * This interface tags objects that have configuration properties and need workspace areas, such as sessions,
 * project and plugins. Objects implementing this interface can have installation directories and must have
 * workspaces, temp areas and scratch areas.
 * 
 * @author Ferd
 * 
 */
public interface IConfiguration {

    // use these subspace names for extra consistency.
    // TODO weed out obsolete stuff
    String SUBSPACE_CONFIG    = "config";
    String SUBSPACE_KNOWLEDGE = "knowledge";
    String SUBSPACE_SOURCE    = "src";
    String SUBSPACE_PLUGINS   = "plugins";
    String SUBSPACE_LIB       = "lib";
    String SUBSPACE_PROJECTS  = "deploy";
    String SUBSPACE_INDEX     = "index";

    /*
     * properties that can be set to define what states to store
     * TODO weed out obsolete stuff
     */
    String STORE_RAW_DATA_PROPERTY                = "thinklab.store.raw";
    String STORE_INTERMEDIATE_DATA_PROPERTY       = "thinklab.store.intermediate";
    String STORE_CONDITIONAL_DATA_PROPERTY        = "thinklab.store.conditional";
    String STORE_MEDIATED_DATA_PROPERTY           = "thinklab.store.mediated";
    String THINKLAB_PROJECT_DIR_PROPERTY          = "thinklab.project.dir";
    String THINKLAB_ASSETS_DIR_PROPERTY           = "thinklab.assets.dir";
    String THINKLAB_ASSETS_PROJECTS_PROPERTY      = "thinklab.project.assets";
    String THINKLAB_CONTENT_PROJECTS_PROPERTY     = "thinklab.project.content";
    String THINKLAB_PUBLISHED_COMPONENTS_PROPERTY = "thinklab.published.components";
    String THINKLAB_ALLOWED_GROUPS_PROPERTY       = "thinklab.allowed.groups";
    String THINKLAB_LOG_FILE_PROPERTY             = "thinklab.log.file";
    String SESSION_TIMEOUT_MINUTES_PROPERTY       = "thinklab.session.timeout";
    String THINKLAB_WORK_DIRECTORY_PROPERTY       = "thinklab.work.directory";
    String SUBMITTING_GROUPS_PROPERTY             = "thinklab.submitting.groups";
    String THINKLAB_ADMINISTRATION_KEY            = "thinklab.admin.key";
    String THINKLAB_AUTHENTICATION_ENDPOINT       = "thinklab.authentication.endpoint";
    String THINKLAB_SUPERUSER_ID_PROPERTY         = "thinklab.superuser.id";
    String THINKLAB_SUPERUSER_PASSWORD_PROPERTY   = "thinklab.superuser.password";
    String THINKLAB_DATA_DIRECTORY_PROPERTY       = "thinklab.data.directory";

    /**
     * Configurable objects should have properties that can be persisted across invocations.
     * 
     * @return
     */
    Properties getProperties();

    /**
     * the data path is the root of the other system-oriented paths, which are assumed not to be relevant to
     * the user. Used by both client and server.
     * 
     * @return
     */
    File getDataPath();

    /**
     * A scratch area is for places where stuff that's not going to be understandable to the user will get
     * written. It should remain there across sessions.
     * 
     * @return a valid, writable scratch path.
     */
    File getScratchArea();

    /**
     * A scratch area is for places where stuff that's not going to be understandable to the user will get
     * written. It should remain there across sessions. Use this one for specific subdirectories.
     * 
     * @return a valid, writable scratch path.
     */
    File getScratchArea(String subArea);

    /**
     * A temp area is like a scratch path but the object implementing this configuration should ensure that
     * it's removed either at the end of a session or at the beginning of a new one, ideally at both points.
     * 
     * @param subArea
     * @return
     */
    File getTempArea(String subArea);

    void persistProperties();

    /**
     * The OS we run on.
     * 
     * @return
     */
    OS getOS();

    /**
     * True if debugging mode has been enabled.
     * @return
     */
    boolean isDebug();

    /**
     * Return the debug level if any has been supplied. If no debug level is set,
     * return 0.
     * 
     * @return
     */
    int getDebugLevel();

    /**
     * Return a global notification level, using the INotification constants.
     * @return
     */
    int getNotificationLevel();

    /**
     * 
     * @param string
     * @return
     */
    File getDataPath(String string);

    /**
     * Only the name of the main work directory, expected to be found under $HOME in personal engines and
     * clients.
     * 
     * @return
     */
    String getWorkDirectoryName();

    Object getAdminKey();
}
