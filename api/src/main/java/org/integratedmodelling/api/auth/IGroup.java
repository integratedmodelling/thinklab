/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.api.auth;

import java.util.Collection;

/**
 * A group is only a string ID, but it is given rights and permissions for assets and components through server configuration. 
 * Users may belong to one or more of these.
 * 
 * @author Ferd
 *
 */
public interface IGroup {

    /**
     * Group ID - customarily a short identifier in uppercase.
     * @return
     */
    String getId();

    /**
     * Primary server URL. Can be null and should be in all but the one group that defines
     * the server that handles it.
     * @return
     */
    String getPrimaryServer();

    /**
     * Core projects, downloaded and synchronized from the primary server to each client at startup.
     * @return
     */
    Collection<String> getCoreProjects();

    /**
     * Searched project, not downloaded but enabled for knowledge search and retrieval for users
     * belonging to this group.
     * 
     * @return
     */
    Collection<String> getSearchedProjects();

    /**
     * Get URLs of the model servers available for the group. Should have more info like whether the
     * group can search, run models and lock - perhaps as a concatenated string for now.
     * 
     * @return
     */
    Collection<String> getModelServers();

}
