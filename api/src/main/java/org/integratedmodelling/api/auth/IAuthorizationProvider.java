package org.integratedmodelling.api.auth;

import org.integratedmodelling.api.modelling.IModelBean;
import org.integratedmodelling.exceptions.ThinklabException;

/**
 * Turns a certificate into a user. Used in Spring servers with both
 * direct and indirect implementations.
 * 
 * @author ferdinando.villa
 *
 */
public interface IAuthorizationProvider {

    /**
     * Pass the contents of a certificate file; return the user if authentication
     * is successful, null if not.
     * 
     * @param certificateContents
     * @return
     * @throws ThinklabException in case of internal failure (e.g. no setup for
     *         decryption).
     */
    IModelBean authorize(String certificateContents) throws ThinklabException;
}
