/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.api.auth;

import java.io.File;
import java.util.Collection;
import java.util.Set;

import org.integratedmodelling.api.network.IComponent;
import org.integratedmodelling.api.project.IProject;

/**
 * A resource catalog. Each network node has one, accessible through INetwork.getResourceCatalog(). Configured
 * through properties (later with a web interface).
 * 
 * @author Ferd
 *
 */
public interface IResourceCatalog {

    /**
     * Return the names of all projects that we have access to through the
     * network. One node will be serving each of them.
     * 
     * @return
     */
    Collection<String> getSynchronizedProjectIds();

    /**
     * 
     * @return
     */
    Collection<String> getContentProjectIds();

    /**
     * 
     * @return
     */
    Collection<String> getComponentIds();

    /**
     * 
     * @return
     */
    Set<String> getAuthorizedGroups();

    /**
     * Check if a particular project ID is authorized for the passed groups. The 
     * project may also specify a loaded component.
     * 
     * @param projectId
     * @param groups
     * @return
     */
    boolean isAuthorized(String projectId, Set<String> groups);

    /*
     * Called to notify where each component has been unpacked. If the component is
     * exported, set up the authorization info for isAuthorized to respond properly.
     * 
     * @param c
     * @param path
     */
    void registerComponentPath(IComponent c, File path);

    /**
     * True if this engine has any authorized content to share with these groups (or 
     * at all if groups == null).
     * 
     * @return
     */
    boolean hasAuthorizedContent(Set<String> groups);

    /**
     * Return the asset projects that have been authorized for the passed groups. If the
     * requesterGroups set is null, return all assets projects that actually exist.
     * 
     * @param requesterGroups
     * @return
     */
    Collection<IProject> getAuthorizedProjects(Set<String> requesterGroups);

    /**
     * Return the asset projects that have been authorized for the passed groups. If the
     * requesterGroups set is null, return all components that actually exist and were initialized
     * successfully.
     * 
     * @param requesterGroups
     * @return
     */
    Collection<IComponent> getAuthorizedComponents(Set<String> requesterGroups);

}
