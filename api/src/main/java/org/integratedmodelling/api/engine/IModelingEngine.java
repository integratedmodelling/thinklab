/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.api.engine;

import java.io.File;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import org.integratedmodelling.api.modelling.IDirectObserver;
import org.integratedmodelling.api.modelling.IExtent;
import org.integratedmodelling.api.modelling.IScale.Locator;
import org.integratedmodelling.api.modelling.ISubject;
import org.integratedmodelling.api.modelling.visualization.IMedia.Type;
import org.integratedmodelling.api.project.IProject;
import org.integratedmodelling.api.runtime.IContext;
import org.integratedmodelling.api.runtime.ITask;
import org.integratedmodelling.exceptions.ThinklabException;
import org.integratedmodelling.exceptions.ThinklabIOException;

/**
 * A modeling engine is dedicated exclusively to one user and is typically on the same
 * network.
 * 
 * 
 * @author ferdinando.villa
 *
 */
public interface IModelingEngine extends IEngine {

    /**
     * Start the engine if it wasn't running. Return false if it was
     * running and nothing needed to be done.
     * 
     * @return
     * @throws ThinklabException
     */
    boolean start() throws ThinklabException;

    /**
     * Stop the engine if it was running. Return false if it was not
     * running.
     * 
     * @return
     * @throws ThinklabException
     */
    boolean stop() throws ThinklabException;

    /**
     * Open a modeling session with the engine, return the ID.
     * 
     * @return
     * @throws ThinklabException
     */
    String openSession() throws ThinklabException;

    /**
     * Observe an object within an existing observation context, potentially choosing a subject different from the
     * root one and a set of scenarios. Return the already started observation task.
     *  
     * @return
     * @throws ThinklabIOException 
     */
    ITask observe(Object observable, IContext context, ISubject subcontext, Collection<String> scenarios)
            throws ThinklabException;

    /**
    * Observe a subject generator to create a context. Further observations must be
    * made directly in the subjects resulting from this.
    *
    * Optional extents may be passed to force the final subject's scale to adapt to
    * them. For example we may pass a geographical region which a passed forcing
    * extent may turn into a grid of a given resolution, or we could pass a temporal
    * context to adopt when it wasn't originally there.
    *
    * @return the observation task, already started, that will create the context and
    * the subject in the engine.
    */
    ITask observe(IDirectObserver observer, IExtent... forceScale) throws ThinklabException;

    /**
     * 
     * @param project
     * @param deployPrerequisites
     * @param loadAfterDeploy
     * @return
     * @throws ThinklabException
     */
    boolean deployProject(IProject project, boolean deployPrerequisites, boolean loadAfterDeploy)
            throws ThinklabException;

    /**
    * Unregister the project with the engine.
    *
    * @param project
    * @return
    */
    boolean undeployProject(String project) throws ThinklabException;

    /**
     * Schedule the given task for interruption as soon as possible. Return 
     * immediately. Listeners will be notified of task interruption.
     * 
     * @param taskId
     */
    public void interruptTask(long taskId);

    /**
     * Import observations from local file, return resulting list of objects.
     * 
     * TODO improve result semantics.
     * 
     * @param file
     * @return
     * @throws ThinklabException
     */
    List<Map<String, Object>> importObservations(File file) throws ThinklabException;

    /**
     * Get the state value at offset for the given state in the given context.
     * 
     * @param id
     * @param index
     * @param path
     * @return
     * @throws ThinklabException 
     */
    Object getStateValue(long contextId, int offset, String statePath) throws ThinklabException;

    /**
     * Launch temporal contextualization of said context and return the corresponding running 
     * task.
     * 
     * @param contextId
     * @return
     * @throws ThinklabException 
     */
    ITask contextualize(long contextId) throws ThinklabException;

    /**
     * 
     * @param _contextId
     * @param file
     * @param path
     * @param mediaType
     * @param options
     * @throws ThinklabException
     */
    void persistContext(File file, long _contextId, String path, Type mediaType, Object... options)
            throws ThinklabException;

    /**
     * 
     * @param contextId
     * @param timeAfter
     * @param statePath
     * @param locators
     * @return
     * @throws ThinklabException
     */
    Map<?, ?> getStatistics(long contextId, long timeAfter, String statePath, Iterable<Locator> locators)
            throws ThinklabException;

}
