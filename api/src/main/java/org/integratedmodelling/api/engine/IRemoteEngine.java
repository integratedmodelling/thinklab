/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.api.engine;

import java.util.Set;

import org.integratedmodelling.api.project.IProject;
import org.integratedmodelling.exceptions.ThinklabException;

/**
 * A remote engine is part of a Thinklab node and does not serve any particular user. It only allows
 * incoming connections from servers whose certificates are in its <config>/network directory. 
 * 
 * A <b>primary server</b> (no specific API for now) also allows one specific call, Endpoints.CONNECT, 
 * from personal engines and must be able to authenticate the requesting user. Upon authentication,
 * the user is returned a view of the Thinklab network corresponding to its access privileges. The
 * primary server URL is found in the user certificate. This will change with the merging of those
 * functions with the node software.
 * 
 * @author Ferd
 *
 */
public interface IRemoteEngine extends IEngine {

    /**
     * Import a remote project that holds knowledge we have retrieved from a query.
     * 
     * @param projectId
     * @return
     * @throws ThinklabException
     */
    IProject importProject(String projectId) throws ThinklabException;

    /**
     * Return the set of group names that are authorized to submit observations to
     * this node. Must never return null.
     * 
     * @return
     */
    Set<String> getSubmittingGroupSet();

}
