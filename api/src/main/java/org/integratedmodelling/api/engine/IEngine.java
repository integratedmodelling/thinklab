/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.api.engine;

import java.util.Collection;
import java.util.List;
import java.util.Map;

import org.integratedmodelling.api.metadata.IModelMetadata;
import org.integratedmodelling.api.metadata.IObservationMetadata;
import org.integratedmodelling.api.modelling.IDirectObserver;
import org.integratedmodelling.api.modelling.IObservable;
import org.integratedmodelling.api.modelling.resolution.IResolutionContext;
import org.integratedmodelling.api.monitoring.IMonitorable;
import org.integratedmodelling.api.network.INode;
import org.integratedmodelling.api.rest.IRESTController;
import org.integratedmodelling.api.runtime.ITask;
import org.integratedmodelling.api.services.IPrototype;
import org.integratedmodelling.exceptions.ThinklabException;

/**
 * API controller for a k.LAB modeling engine. The base interface is quite basic; services become available
 * in sub-interfaces for local, remote, and primary/remote engines. A {@link INode} is a networked
 * engine.
 * 
 * @author ferdinando.villa
 * FIXME lose the IRESTController interface - this is just an engine and may be not used through REST at all.
 */
public interface IEngine extends IMonitorable, IRESTController {

    /**
     * Used by modeler implementations to advertise themselves on the network so that
     * clients can discover them. Also determines the context path of the application,
     * so don't change it.
     * 
     * FIXME make the context path a variable in the broadcasted info.
     */
    public static final String MODELER_APPLICATION_ID = "kmodeler";

    
    /*
    * constants that can be used by client to broadcast status changes in the engine. Not
    * used internally. 
    * FIXME change to an appropriate enum and don't call it Status.
    */
    public static final int RUNNING      = 0;
    public static final int STOPPED      = 1;
    public static final int BOOTING      = 2;
    public static final int EXCEPTION    = 3;
    public static final int UNLOCKED     = 4;
    public static final int INITIALIZING = 5;

    /**
     * All engines have a name. Only network node engines must have a unique and
     * sensible name, the others may have descriptive names with no requirement
     * of being unique. The requirement of uniqueness is enforced in INode.getId().
     * 
     * @return
     */
    String getName();

    /**
     * Return the URL the server responds on.
     * 
     * @return
     */
    @Override
    String getUrl();

    /**
     * Status at time of inquiry (if monitored), using all the applicable constants above
     * as keys. This corresponds to the ping call to the REST interface - guaranteed to be
     * fast and small.
     * 
     * @return
     */
    Map<String, Object> getStatus();

    /**
     * Use to quickly check if the engine is running or not.
     * 
     * @return
     */
    boolean isRunning();

    /**
     * Get all the service prototypes published by this engine (may vary according to
     * privileges).
     * 
     * @return
     */
    Collection<IPrototype> getServices();

    /**
     * Get all the function prototypes published by this engine (may vary according to
     * privileges).
     * 
     * @return
     */
    Collection<IPrototype> getFunctions();

    /**
     * True if this engine provides the named component to the active user.
     * 
     * @param id
     * @return
     */
    boolean providesComponent(String id);

    /**
     * True if this engine provides the named service to the active user.
     * 
     * @param id
     * @return
     */
    boolean providesService(String id);

    /**
     * Get the prototype corresponding to the passed service ID. Null without error
     * if not existing or not allowed.
     * 
     * @param id
     * @return
     */
    IPrototype getServicePrototype(String id);

    /**
     * Get the prototype corresponding to the passed function ID. Null without error
     * if not existing or not allowed.
     * 
     * @param id
     * @return
     */
    IPrototype getFunctionPrototype(String id);

    /**
     * Submit a locally created direct observer for use and optionally storage in this engine.
     * 
     * @param observer
     * @param store
     * @return
     * @throws ThinklabException
     */
    String submitObservation(IDirectObserver observer, boolean store) throws ThinklabException;

    /**
     * Return descriptors for all observations that match a textual query. The modeling engine
     * should also interrogate all authorized nodes on the network.
     * 
     * @param text
     * @param localOnly if true, only lookup those in local kbox; otherwise also look in all nodes that
     *        authorize us to search.
     * @return
     * @throws ThinklabException
     */
    public List<IObservationMetadata> queryObservations(String text, boolean localOnly)
            throws ThinklabException;

    /**
     * Retrieve a direct observer by fully qualified name.
     * 
     * @param observationid
     * @return
     * @throws ThinklabException
     */
    public IDirectObserver retrieveObservation(String observationid, String nodeId)
            throws ThinklabException;

    /**
     * Return descriptors for models that match the passed info. 
     *  
     * @param observable
     * @param context
     * @return
     * @throws ThinklabException
     */
    public List<IModelMetadata> queryModels(IObservable observable, IResolutionContext context)
            throws ThinklabException;

    /**
     * Start the task of setting up a component using its setup conventions; return the task that
     * is accomplishing that. Throw exceptions if unauthorized or errors happened launching setup.
     * 
     * @param componentId
     * @return
     */
    public ITask setupComponent(String componentId) throws ThinklabException;

    /**
     * Remove all observations with passed fully qualified names.
     * 
     * @param observationNames
     * @throws ThinklabException
     */
    void removeObservations(Collection<String> observationNames) throws ThinklabException;

    /**
     * rescan the local runtime to pick up any changes in plug-ins and the like.
     * TODO rename
     * @throws ThinklabException 
     */
    void rescanClasspath() throws ThinklabException;

    /**
     * Metadata keys relevant to (most) engines.
     */
    public static final String CURRENT_USERNAME     = "IEngine.CURRENT_USERNAME";
    public static final String BOOT_TIME_MS         = "IEngine.BOOT_TIME_MS";
    public static final String TOTAL_MEMORY_MB      = "IEngine.TOTAL_MEMORY_MB";
    public static final String FREE_MEMORY_MB       = "IEngine.FREE_MEMORY_MB";
    public static final String LOAD_FACTOR_PERCENT  = "IEngine.LOAD_FACTOR_PERCENT";
    public static final String VERSION_STRING       = "IEngine.VERSION_STRING";
    public static final String AVAILABLE_PROCESSORS = "IEngine.AVAILABLE_PROCESSORS";
    public static final String EXCEPTION_CLASS      = "IEngine.EXCEPTION_CLASS";
    public static final String STACK_TRACE          = "IEngine.STACK_TRACE";
    public static final String UPTIME_TEXT          = "IEngine.UPTIME_TEXT";
    // public static final String IS_LOCKED = "IEngine.IS_LOCKED";
    public static final String AUTH_ENDPOINT        = "IEngine.AUTH_ENDPOINT";
    public static final String AUTHORIZED_GROUPS    = "IEngine.AUTHORIZED_GROUPS";

    /**
     * Environmental variable that may be substituted in the script that launches an
     * embedded server.
     */
    public static final String ENV_THINKLAB_MAX_MEMORY = "THINKLAB_MAX_MEMORY";
    public static final String ENV_THINKLAB_JRE        = "THINKLAB_JRE";
    public static final String VERSION_INFO_STRING     = "THINKLAB_VERSION_INFO";

}
