/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.api.services;

import java.util.Collection;
import java.util.List;
import java.util.Set;

import org.integratedmodelling.api.knowledge.IConcept;
import org.integratedmodelling.api.modelling.IFunctionCall;
import org.integratedmodelling.exceptions.ThinklabValidationException;

/**
 * Prototypes define callable functions and their parameters. In Thinklab it works for commands and
 * language functions so far, both of which can return one of these. Prototypes are automatically
 * created by the @Prototype annotation, which applies to different types of objects and will create
 * services, functions or commands according to the object it applies to.
 * 
 * @author Ferd
 *
 */
public interface IPrototype {

    /**
     * Command ID.
     * @return
     */
    public String getId();

    /**
     * Full name of each argument.
     * 
     * @return
     */
    public List<String> getArgumentNames();

    /**
     * Long names of each option
     * @return
     */
    public List<String> getOptionNames();

    /**
     * Return the concept(s) of the return type.
     * 
     * @return
     */
    public Set<IConcept> getReturnTypes();

    /**
     * Command general description
     * 
     * @return
     */
    public String getDescription();

    /**
     * If true, the command can only be used with one of the subcommands
     * listed by getSubcommandNames().
     * 
     * @return
     */
    public boolean requiresSubcommand();

    /**
     * Name of each subcommand; empty list if no subcommands
     * @return
     */
    public Collection<String> getSubcommandNames();

    /**
     * Empty string if no description.
     * @param subcommand
     * @return
     */
    public String getSubcommandDescription(String subcommand);

    /**
     * Returns a description if given or the empty string if not. Never null.
     * 
     * @param argumentName
     * @return
     */
    String getArgumentDescription(String argumentName);

    /**
     * Validate passed argument; throw validation exception if invalid; return value (if
     * necessary translated to most appropriate type) if valid.
     * @param key
     * @param value
     * @return
     * @throws ThinklabValidationException
     */
    Object validateArgumentType(String key, Object value) throws ThinklabValidationException;

    /**
     * True if argument is optional
     * @param arg
     * @return
     */
    public boolean isArgumentOptional(String arg);

    /**
     * Get the required arguments for the particular subcommand. If empty array, uses the
     * default arguments from the prototype. It's an error to call it with an empty string.
     * 
     * @param subcommand
     * @return
     */
    String[] getSubcommandRequiredArguments(String subcommand);

    /**
     * Ensure that the arguments passed reflect the required arguments, which may 
     * be different for each subcommand.
     * 
     * @param command
     */
    void validateArguments(IServiceCall command) throws ThinklabValidationException;

    /**
     * Ensure that the arguments passed reflect the required arguments, which may 
     * be different for each subcommand.
     * 
     * TODO we may merge the function and service call or give them a common ancestor.
     * For now it's two separate methods.
     * 
     * @param command
     */
    void validateArguments(IFunctionCall function) throws ThinklabValidationException;

    /**
     * Full synopsis - expecting a multi-line string with full description of
     * options, arguments and subcommands.
     * 
     * @return
     */
    public String getSynopsis();

    /**
     * One-line short synopsis intended to document usage without descriptions.
     * 
     * @return
     */
    public String getShortSynopsis();

    /**
     * Get the class of the object whose API the prototype specifies.
     * 
     * @return
     */
    public Class<?> getExecutorClass();

    /**
     * If distributed, the service identified can be broadcast to multiple
     * endpoints and the results can be merged.
     * 
     * @return
     */
    boolean isDistributed();

    /**
     * True if this service or function is usable by external callers (e.g. remote engines).
     * 
     * @return
     */
    boolean isPublished();

    /**
     * Pass the type of the accessor of interest (see NS) to check if
     * the associated service can be used to provide that kind of model
     * service.
     * 
     * @param accessorType
     * @return
     */
    boolean canModel(IConcept accessorType);
}
