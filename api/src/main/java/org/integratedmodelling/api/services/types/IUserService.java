/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.api.services.types;

/**
 * A user service runs on a public engine only, but is restricted to the primary server that serves a client, and
 * must be able to verify a client's authentication token before proceeding. The primary server knows the authentication
 * endpoints, communicates it to the client, and uses it to verify authentication tokens before allowing the client 
 * access to the rest of the network.
 * 
 * In Thinklab 0.9.6+: these require a UserAuthorization token in the headers that can be matched to a valid profile
 * by the authenticated endpoint.
 * 
 * @author ferdinando.villa
 *
 */
public interface IUserService {

}
