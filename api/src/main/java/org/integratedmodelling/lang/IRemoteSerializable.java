/*******************************************************************************
 *  Copyright (C) 2007, 2014:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.lang;

/**
 * May be implemented by objects returned by commands. Simple data structures
 * with POD members don't need to implement this.
 * 
 * When an object implements this interface, it will be serialized to the
 * object returned by adapt() before being transmitted as the result of a
 * command. This will be transmitted as result in REST resources and printed
 * after command execution.
 * 
 * @author ferdinando.villa
 * @deprecated will switch to bean serialization provided by INetworkSerializable
 */
public interface IRemoteSerializable {

    /**
     * Use this key in maps to reference the most general representative interface for
     * the result. This should be the simpleName() of an API interface visible at all sides.
     */
    String RESULT_TYPE_KEY = "result-type";

    /**
     * Return the object that we want to use as the return value. This
     * should be readily serializable to JSON - i.e. it should be a simple
     * POD value, or a list/map with simple members.
     * 
     * @return
     */
    Object adapt();
}
