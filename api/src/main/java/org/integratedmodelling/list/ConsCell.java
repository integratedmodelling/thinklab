/*******************************************************************************
 *  Copyright (C) 2007, 2014:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
// author:  Robert Keller
// purpose: polycell used in implementation of Polylists

package org.integratedmodelling.list;

import org.integratedmodelling.api.lang.IList;

/**
  *  NonEmptyList is the sub-class of List consisting of non-empty 
  *  lists.  Every NonEmptyList has a first and a rest.
 **/
class ConsCell {
    private Object First;
    private Object Rest;

    /**
      *  first() returns the first element of a NonEmptyList.
     **/

    Object first() {
        return First;
    }

    /**
      *  rest() returns the rest of a NonEmptyList.
     **/

    IList rest() {
        if (Rest instanceof Seed) {
            Rest = ((Seed) Rest).grow();
        }
        return (IList) Rest;
    }

    /**
      *  polycell is the constructor for the cell of a Polyist, 
      *  given a First and a Rest.
      *
      *  Use static method cons of class Polylist to avoid using 'new' 
      *  explicitly.
     **/

    ConsCell(Object First, Object Rest) {
        this.First = First;
        this.Rest = Rest;
    }

    /**
      *  setFirst() sets the first element of a NonEmptyList.
     **/

    void setFirst(Object value) {
        First = value;
    }
} // class polycell
