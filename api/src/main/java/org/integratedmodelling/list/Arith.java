/*******************************************************************************
 *  Copyright (C) 2007, 2014:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
// author:  Robert Keller
// purpose: Class Arith of polya package
//          Implements "polymorphic arithmetic"

package org.integratedmodelling.list;

import org.integratedmodelling.api.lang.IList;

public class Arith {
    static public boolean greaterThan(Object v1, Object v2) {
        if (!(v1 instanceof Number))
            throw new IllegalArgumentException("NotNumber: " + v1);

        if (!(v2 instanceof Number))
            throw new IllegalArgumentException("NotNumber: " + v2);

        if (v1 instanceof Long) {
            if (v2 instanceof Long) {
                return ((Long) v1).longValue() > ((Long) v2).longValue();
            } else {
                return ((Long) v1).longValue() > ((Number) v2).doubleValue();
            }
        } else {
            if (v2 instanceof Long) {
                return ((Number) v1).doubleValue() > ((Long) v2).longValue();
            } else {
                return ((Number) v1).doubleValue() > ((Number) v2).doubleValue();
            }
        }
    }

    static public boolean greaterThanOrEqual(Object v1, Object v2) {
        if (!(v1 instanceof Number))
            throw new IllegalArgumentException("NotNumber: " + v1);

        if (!(v2 instanceof Number))
            throw new IllegalArgumentException("NotNumber: " + v2);

        if (v1 instanceof Long) {
            if (v2 instanceof Long) {
                return ((Long) v1).longValue() >= ((Long) v2).longValue();
            } else {
                return ((Long) v1).longValue() >= ((Number) v2).doubleValue();
            }
        } else {
            if (v2 instanceof Long) {
                return ((Number) v1).doubleValue() >= ((Long) v2).longValue();
            } else {
                return ((Number) v1).doubleValue() >= ((Number) v2).doubleValue();
            }
        }
    }

    static public boolean equal(Object v1, Object v2) {
        if (v1.equals(v2))
            return true;

        if (v1 instanceof IList) {
            if (v2 instanceof IList)
                return PolyList.equals((PolyList) v1, (PolyList) v2);
            else
                return false;
        }

        if (v2 instanceof IList) {
            return false;
        }

        if (v1 instanceof String) {
            if (v2 instanceof String)
                return v1.equals(v2);
            else
                return false;
        }

        if (v2 instanceof String) {
            return false;
        }

        if (v1 instanceof Boolean) {
            if (v2 instanceof Boolean)
                return v1.equals(v2);
            else
                return false;
        }

        if (v2 instanceof Boolean) {
            return false;
        }

        if (v1 instanceof Number) {
            if (v1 instanceof Long) {
                if (v2 instanceof Long) {
                    return ((Long) v1).longValue() == ((Long) v2).longValue();
                } else {
                    return ((Long) v1).longValue() == ((Number) v2).doubleValue();
                }
            } else {
                if (v2 instanceof Long) {
                    return ((Number) v1).doubleValue() == ((Long) v2).longValue();
                } else {
                    return ((Number) v1).doubleValue() == ((Number) v2).doubleValue();
                }
            }
        }

        return false;
    }

    static public Number add(Object v1, Object v2) {
        if (!(v1 instanceof Number))
            throw new IllegalArgumentException("NotNumber: " + v1);

        if (!(v2 instanceof Number))
            throw new IllegalArgumentException("NotNumber: " + v2);

        if (v1 instanceof Long) {
            if (v2 instanceof Long) {
                return new Long(((Long) v1).longValue() + ((Long) v2).longValue());
            } else {
                return new Double(((Long) v1).longValue() + ((Double) v2).doubleValue());
            }
        } else {
            if (v2 instanceof Long) {
                return new Double(((Double) v1).doubleValue() + ((Long) v2).longValue());
            } else {
                return new Double(((Double) v1).doubleValue() + ((Double) v2).doubleValue());
            }
        }
    }

    static public Number subtract(Object v1, Object v2) {
        if (!(v1 instanceof Number))
            throw new IllegalArgumentException("NotNumber: " + v1);

        if (!(v2 instanceof Number))
            throw new IllegalArgumentException("NotNumber: " + v2);

        if (v1 instanceof Long) {
            if (v2 instanceof Long) {
                return new Long(((Long) v1).longValue() - ((Long) v2).longValue());
            } else {
                return new Double(((Long) v1).longValue() - ((Double) v2).doubleValue());
            }
        } else {
            if (v2 instanceof Long) {
                return new Double(((Double) v1).doubleValue() - ((Long) v2).longValue());
            } else {
                return new Double(((Double) v1).doubleValue() - ((Double) v2).doubleValue());
            }
        }
    }

    static public Number multiply(Object v1, Object v2) {
        if (!(v1 instanceof Number))
            throw new IllegalArgumentException("NotNumber: " + v1);

        if (!(v2 instanceof Number))
            throw new IllegalArgumentException("NotNumber: " + v2);

        if (v1 instanceof Long) {
            if (v2 instanceof Long) {
                return new Long(((Long) v1).longValue() * ((Long) v2).longValue());
            } else {
                return new Double(((Long) v1).longValue() * ((Double) v2).doubleValue());
            }
        } else {
            if (v2 instanceof Long) {
                return new Double(((Double) v1).doubleValue() * ((Long) v2).longValue());
            } else {
                return new Double(((Double) v1).doubleValue() * ((Double) v2).doubleValue());
            }
        }
    }

    static public Number divide(Object v1, Object v2) {
        if (!(v1 instanceof Number))
            throw new IllegalArgumentException("NotNumber: " + v1);

        if (!(v2 instanceof Number))
            throw new IllegalArgumentException("NotNumber: " + v2);

        if (v1 instanceof Long) {
            if (v2 instanceof Long) {
                return new Long(((Long) v1).longValue() / ((Long) v2).longValue());
            } else {
                return new Double(((Long) v1).longValue() / ((Double) v2).doubleValue());
            }
        } else {
            if (v2 instanceof Long) {
                return new Double(((Double) v1).doubleValue() / ((Long) v2).longValue());
            } else {
                return new Double(((Double) v1).doubleValue() / ((Double) v2).doubleValue());
            }
        }
    }

    static public Number mod(Object v1, Object v2) {
        if (!(v1 instanceof Long))
            throw new IllegalArgumentException("NotNumber: " + v1);

        if (!(v2 instanceof Long))
            throw new IllegalArgumentException("NotNumber: " + v2);

        return new Long(((Long) v1).longValue() % ((Long) v2).longValue());
    }
}
