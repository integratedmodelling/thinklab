/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.collections;

import java.io.IOException;
import java.io.StreamTokenizer;
import java.io.StringReader;

import org.integratedmodelling.api.lang.IParseable;
import org.integratedmodelling.api.modelling.ITopologicallyComparable;
import org.integratedmodelling.exceptions.ThinklabException;
import org.integratedmodelling.exceptions.ThinklabRuntimeException;

/**
 * A numeric interval parsed from conventional syntax (e.g. "[12 34)" ). Can be
 * set to be open or close on each end and check if a number is contained in it.
 * 
 * @author Ferdinando Villa
 *
 */
public class NumericInterval implements IParseable, ITopologicallyComparable<NumericInterval> {

    double  lowerBound     = 0.0;
    double  upperBound     = 0.0;
    boolean lowerOpen      = false;
    boolean upperOpen      = false;
    boolean lowerUndefined = true;
    boolean upperUndefined = true;

    /*
     * for the semantic instantiator
     */
    public NumericInterval() {
    }

    public NumericInterval(String intvs) {
        parse(intvs);
    }

    public NumericInterval(Double left, Double right, boolean leftOpen, boolean rightOpen) {

        if (!(lowerUndefined = (left == null)))
            lowerBound = left;
        if (!(upperUndefined = (right == null)))
            upperBound = right;

        lowerOpen = leftOpen;
        upperOpen = rightOpen;
    }

    public void parse(String s) {

        StreamTokenizer scanner = new StreamTokenizer(new StringReader(s));
        int token = 0;
        double high = 0.0, low = 0.0;
        int nnums = 0;
        boolean lowdef = false, highdef = false;

        while (true) {

            try {
                token = scanner.nextToken();
            } catch (IOException e) {
                throw new ThinklabRuntimeException("invalid interval syntax: " + s);
            }

            if (token == StreamTokenizer.TT_NUMBER) {

                if (nnums > 0) {
                    high = scanner.nval;
                } else {
                    low = scanner.nval;
                }
                nnums++;

            } else if (token == StreamTokenizer.TT_EOF || token == StreamTokenizer.TT_EOL) {
                break;
            } else if (token == '(') {
                if (nnums > 0)
                    throw new ThinklabRuntimeException("invalid interval syntax: " + s);
                lowdef = true;
                lowerOpen = true;
            } else if (token == '[') {
                if (nnums > 0)
                    throw new ThinklabRuntimeException("invalid interval syntax: " + s);
                lowdef = true;
                lowerOpen = false;
            } else if (token == ')') {
                if (nnums == 0)
                    throw new ThinklabRuntimeException("invalid interval syntax: " + s);
                highdef = true;
                upperOpen = true;
            } else if (token == ']') {
                if (nnums == 0)
                    throw new ThinklabRuntimeException("invalid interval syntax: " + s);
                highdef = true;
                upperOpen = false;
            } else if (token == ',') {
                /* accept and move on */
            } else {
                throw new ThinklabRuntimeException("invalid interval syntax: " + s);
            }
        }

        /*
         * all read, assemble interval info
         */
        if (lowdef && highdef && nnums == 2) {
            lowerUndefined = upperUndefined = false;
            lowerBound = low;
            upperBound = high;
        } else if (lowdef && !highdef && nnums == 1) {
            lowerUndefined = false;
            lowerBound = low;
        } else if (highdef && !lowdef && nnums == 1) {
            upperUndefined = false;
            upperBound = low;
        } else {
            throw new ThinklabRuntimeException("invalid interval syntax: " + s);
        }
    }

    public int compare(NumericInterval i) {

        if (lowerUndefined == i.lowerUndefined && lowerOpen == i.lowerOpen
                && upperUndefined == i.upperUndefined && upperOpen == i.upperOpen
                && lowerBound == i.lowerBound && upperBound == i.upperBound)
            return 0;

        if (this.upperBound <= i.lowerBound)
            return -1;

        if (this.lowerBound >= i.upperBound)
            return 1;

        throw new ThinklabRuntimeException("error: trying to sort overlapping numeric intervals");

    }

    public boolean isRightInfinite() {
        return upperUndefined;
    }

    public boolean isLeftInfinite() {
        return lowerUndefined;
    }

    /**
     * true if the upper boundary is closed, i.e. includes the limit
     * @return
     */
    public boolean isRightBounded() {
        return !upperOpen;
    }

    /**
     * true if the lower boundary is closed, i.e. includes the limit
     * @return
     */
    public boolean isLeftBounded() {
        return !lowerOpen;
    }

    public double getMinimumValue() {
        return lowerBound;
    }

    public double getMaximumValue() {
        return upperBound;
    }

    public boolean contains(double d) {

        if (lowerUndefined)
            return (upperOpen ? d < upperBound : d <= upperBound);
        else if (upperUndefined)
            return (lowerOpen ? d > lowerBound : d >= lowerBound);
        else
            return (upperOpen ? d < upperBound : d <= upperBound)
                    && (lowerOpen ? d > lowerBound : d >= lowerBound);
    }

    @Override
    public String toString() {

        String ret = "";

        if (!lowerUndefined) {
            ret += lowerOpen ? "(" : "[";
            ret += lowerBound;
        }
        if (!upperUndefined) {
            if (!lowerUndefined)
                ret += " ";
            ret += upperBound;
            ret += upperOpen ? ")" : "]";
        }

        return ret;
    }

    @Override
    public String asText() {
        return toString();
    }

    @Override
    public boolean contains(NumericInterval o) throws ThinklabException {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public boolean overlaps(NumericInterval o) throws ThinklabException {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public boolean intersects(NumericInterval o) throws ThinklabException {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public ITopologicallyComparable<NumericInterval> union(ITopologicallyComparable<?> other)
            throws ThinklabException {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public ITopologicallyComparable<NumericInterval> intersection(ITopologicallyComparable<?> other)
            throws ThinklabException {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public double getCoveredExtent() {
        // TODO Auto-generated method stub
        return 0;
    }

}
