package org.integratedmodelling.doc;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;

import org.markdownj.MarkdownProcessor;

public class Document {

    static MarkdownProcessor markup        = new MarkdownProcessor();

    static final String      SOURCE_FOLDER = "docs/en";
    static final String      OUTPUT_FOLDER = "build";

    static HashSet<String>   keywords      = new HashSet<String>();

    static {
        keywords.add("%title");
        keywords.add("%label");
    }

    static class Section {

        File   file;
        int[]  level;
        String title;
        String text;
        String outputId;
        String label;

        int getIndentLevel() {
            return level.length - 1;
        }

        public void process(String kw, String substring) {

            if (kw.equals("%title")) {
                title = substring;
            } else if (kw.equals("%label")) {
                label = substring;
            }
        }
    }

    /*
     * table of contents
     */
    private ArrayList<Section> toc = new ArrayList<Section>();

    /*
     * folders for source and destination
     */
    private String             _sourceFolder;
    private String             _outputFolder;

    private int                _maxSectionIndent;

    public Document(String sourceFolder, String outputFolder) {
        _sourceFolder = sourceFolder;
        _outputFolder = outputFolder;
    }

    /**
     * @param args
     * @throws Exception 
     */
    public static void main(String[] args) throws Exception {

        Document document = new Document(SOURCE_FOLDER, OUTPUT_FOLDER);
        document.buildTOC();

        document.dump();

        document.createEclipseFiles();
        document.createOutputFile();

    }

    private void createEclipseFiles() throws Exception {

        File eOut = new File(_outputFolder + File.separator + "eclipse");
        File eOutHelp = new File(eOut + File.separator + "help");
        File eOutHTML = new File(eOut + File.separator + "html");

        eOut.mkdirs();
        eOutHelp.mkdirs();
        eOutHTML.mkdirs();

        createEclipseIndex(eOutHelp);
        for (Section s : toc) {
            write(toHTML(s.text), new File(eOutHTML + File.separator
                    + s.outputId.substring(0, s.outputId.indexOf('-')) + ".html"));
        }
    }

    public void createOutputFile() throws IOException {

        File eOut = new File(_outputFolder + File.separator + "md");
        eOut.mkdirs();

        File f = new File(eOut + File.separator + "output.md");
        for (Section s : toc) {
            append(s.text, f);
        }
    }

    public void createEclipseIndex(File out) throws Exception {
        buildEclipseIndex(null, out, "Thinklab documentation");
    }

    private String buildEclipseIndex(int[] level, File dir, String label) throws Exception {

        String link = levelToFilename(level) + ".xml";
        File xout = new File(dir + File.separator + link);
        PrintWriter out = new PrintWriter(new FileOutputStream(xout));

        System.out.println("  <toc file=\"help/" + link + "\" primary=\""
                + (level == null ? "true" : "false") + "\"></toc>");

        out.println("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
        out.println("<?NLS TYPE=\"org.eclipse.help.toc\"?>");
        out.println("<toc label =\"" + Escape.forDoubleQuotedString(label, true) + "\">\n");

        for (Section s : listSubsections(level)) {

            out.println(
                    "  <topic label=\"" + Escape.forDoubleQuotedString(s.title, true) + "\"" +
                            " href=\"html/" + s.outputId.substring(0, s.outputId.indexOf('-')) + ".html\"" +
                            ">");

            if (s.getIndentLevel() <= _maxSectionIndent) {
                String lnk = buildEclipseIndex(s.level, dir, s.title);
                out.println("    <link toc=\"help/" + lnk + "\"/>");
            }

            out.println("  </topic>");
        }

        out.println("</toc>");

        out.close();

        return link;
    }

    /**
     * Build the section index and fill each section with the reprocessed 
     * text according to the section levels. Return the maximum indentation
     * level in the document.
     * 
     * @return
     * @throws IOException
     */
    public int buildTOC() throws IOException {

        /*
         * establish the maximum indentation level so that we know
         * where to stop generating tables of contents for Eclipse. In the meantime
         * extract the first section header and build a title for the section.
         */
        _maxSectionIndent = 0;
        for (File f : getSourceFiles()) {
            Section section = new Section();
            section.file = f;
            section.outputId = getFileBaseName(f);
            section.level = getSectionNumbering(section.outputId);
            section.text = slurp(f, section.getIndentLevel(), section);
            if (_maxSectionIndent < section.getIndentLevel()) {
                _maxSectionIndent = section.getIndentLevel();
            }
            toc.add(section);
        }
        return _maxSectionIndent;
    }

    /**
     * Return the list of source files sorted as necessary.
     * 
     * @return
     */
    public List<File> getSourceFiles() {
        ArrayList<File> ret = new ArrayList<File>();
        for (File f : new File(_sourceFolder).listFiles()) {
            if (f.toString().endsWith(".md")) {
                ret.add(f);
            }
        }
        Collections.sort(ret, new Comparator<File>() {

            @Override
            public int compare(File arg0, File arg1) {
                // TODO Auto-generated method stub
                return arg0.toString().compareTo(arg1.toString());
            }
        });
        return ret;
    }

    public List<Section> listSubsections(int[] level) {

        ArrayList<Section> ret = new ArrayList<Document.Section>();
        for (Section s : toc) {
            boolean match = false;
            if (level == null) {
                match = s.level.length == 1;
            } else {
                if (s.level.length == level.length + 1) {
                    match = true;
                    for (int i = 0; i < level.length; i++) {
                        if (s.level[i] != level[i]) {
                            match = false;
                            break;
                        }
                    }
                }
            }

            if (match) {
                ret.add(s);
            }
        }
        return ret;
    }

    public static void convertToHTML(File file) throws IOException {

        if (file.isDirectory()) {
            for (File f : file.listFiles()) {
                convertToHTML(f);
            }
        } else if (file.getName().endsWith(".md")) {

            String mrkdText = slurp(file);
            String htmlText = markup.markdown(mrkdText);
            writeHTML(file, htmlText);
        }
    }

    public static String toHTML(String mdText) throws IOException {
        return markup.markdown(mdText);
    }

    private static void writeHTML(File file, String htmlText) throws IOException {

        String s = file.getParentFile().toString().substring(1);
        File dir = new File(System.getProperty("user.home")
                + "/bitbucket/org.integratedmodelling.thinkcap/html" + File.separator + s);
        dir.mkdirs();
        File dest = new File(dir + File.separator + file.getName().substring(0, file.getName().length() - 3)
                + ".html");
        System.out.println(dest);
        BufferedWriter writer = null;
        try {
            writer = new BufferedWriter(new FileWriter(dest));
            writer.write(htmlText);
        } finally {
            try {
                if (writer != null)
                    writer.close();
            } catch (IOException e) {
            }
        }
    }

    public static void write(String text, File file) throws IOException {

        BufferedWriter writer = null;
        try {
            writer = new BufferedWriter(new FileWriter(file));
            writer.write(text);
        } finally {
            try {
                if (writer != null)
                    writer.close();
            } catch (IOException e) {
            }
        }
    }

    public static void append(String text, File file) throws IOException {

        BufferedWriter writer = null;
        try {
            writer = new BufferedWriter(new FileWriter(file, true));
            writer.write(text);
        } finally {
            try {
                if (writer != null)
                    writer.close();
            } catch (IOException e) {
            }
        }
    }

    private static String slurp(File file) throws IOException {
        BufferedReader in = new BufferedReader(new FileReader(file));
        StringBuilder sb = new StringBuilder();
        String line;
        while ((line = in.readLine()) != null) {
            sb.append(line).append("\n");
        }
        in.close();
        return sb.toString();
    }

    public void dump() {
        for (Section s : listSubsections(null))
            dumpSections(s);
    }

    private void dumpSections(Section section) {

        String lev = printLevel(section.level);
        lev += repeat(' ', 10 - lev.length());
        System.out.println(lev + section.title);
        for (Section s : listSubsections(section.level)) {
            dumpSections(s);
        }
    }

    private String printLevel(int[] level) {
        String ret = "";
        for (int i : level) {
            ret += i + ".";
        }
        return ret;
    }

    private String levelToFilename(int[] level) {
        String ret = "toc";
        if (level != null) {
            for (int i : level) {
                ret += (i == 0 ? "" : "_") + i;
            }
        }
        return ret;
    }

    /**
     * Return the section numbering correspondent to the filename passed. Assumes
     * the file starts with a (HH)*- prefix where each couple of digits identifies 
     * a section level. The length of the array minus one is the indentation level
     * for the headers in the file.
     * 
     * @param basename
     * @return
     */
    public static int[] getSectionNumbering(String basename) {
        int n = basename.toString().indexOf('-');
        String nums = basename.toString().substring(0, n);
        int[] ret = new int[nums.length() / 2];
        for (int i = 0; i < nums.length(); i += 2) {
            ret[i / 2] = Integer.parseInt(nums.substring(i, i + 2));
        }
        return ret;
    }

    /**
     * Return the file as a string, replacing any section header with the same
     * at the indentation level passed. Use the first section header to define
     * the title for the passed section.
     * 
     * @param file
     * @param indentLevel
     * @param section 
     * @return
     * @throws IOException
     */
    public static String slurp(File file, int indentLevel, Section section) throws IOException {

        String filler = repeat('#', indentLevel);

        BufferedReader in = new BufferedReader(new FileReader(file));
        StringBuilder sb = new StringBuilder();
        String line;
        while ((line = in.readLine()) != null) {
            if (line.startsWith("%")) {
                /*
                 * if the first word is one of the keyword lines, define the section
                 * and continue without adding it. Otherwise proceed regularly.
                 */
                int spp = line.indexOf(' ');
                if (spp > 1) {
                    String kw = line.substring(0, spp);
                    if (keywords.contains(kw)) {
                        section.process(kw, line.substring(spp + 1));
                        continue;
                    }
                }
            }
            if (line.startsWith("#")) {

                if (section.title == null) {
                    String l = line;
                    while (l.charAt(0) == '#') {
                        l = l.substring(1);
                    }
                    section.title = l.trim();
                }
                line = filler + line;
            }
            sb.append(line).append("\n");
        }
        in.close();
        return sb.toString();
    }

    /**
     * Return file name with no path or extension
     * @param ss
     * @return
     */
    public static String getFileBaseName(File s) {

        String ret = s.toString();

        int sl = ret.lastIndexOf(File.separator);
        if (sl > 0)
            ret = ret.substring(sl + 1);
        sl = ret.lastIndexOf(".");
        if (sl > 0)
            ret = ret.substring(0, sl);

        return ret;
    }

    public static String repeat(char c, int n) {
        String s = "";
        for (int i = 0; i < n; i++) {
            s += c;
        }
        return s;
    }
}
