# Reference sheets

This part of the documentation contains quick reference sheets that can be useful during modeling practice. 

The [unit reference](0201.html) details the syntax for units of measurement understood by Thinklab's unit parser.
The [function reference](0202.html) briefly describes the most common functions in Thinklab and their arguments.
The [glossary](0203.html) lists the terms used most often in Thinklab documentation and provides a brief definition for each of them.

This section is far from complete and cannot substitute person-to-person instruction yet.

