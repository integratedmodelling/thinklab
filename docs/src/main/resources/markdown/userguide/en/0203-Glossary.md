﻿# Glossary of terms used in Thinklab-ARIES modeling

**Abstract knowledge:** Concepts; abstract knowledge provide a general definition of each concept and is contained within _ontologies_.

**Accessor:** A function that links Thinklab’s core code and functionality to an external method or program for data and model handling. The accessor transfers inputs from Thinklab to the external program and returns outputs to Thinklab for analysis by the modeler. 

**Action:** A change in the state of a model element that may occur, for example, during a _transition_.

**Annotation property:**  [UPDATE DEFINITION]

**Bayesian network:** A probabilistic graphical model (a type of statistical model) that represents a set of random variables and their conditional dependencies via a directed acyclic graph.

**Bayes’ Theorem:** A mathematical theorem that allows estimation of the likelihood of one event that is conditional on another. It allows the probabilities of linked events to be updated from a state where information about the likelihood of events is lacking (and only _prior probabilities_ exist) to a state where an outcome is known (_evidence of an event_ is submitted and _posterior probabilities_ are estimated via _Bayesian updating_).

**Bayesian updating:** The process by which _prior_ and/or _conditional probabilities_ are replaced by _evidence_ (knowledge of the state of an event), and are updated using _Bayes’ Theorem_ to become _posterior probabilities_.

**Child node:** A child node in a Bayesian network is influenced by the state of one or more parent nodes via an edge (arrow) that indicates influence. Child nodes may include _intermediate nodes_ whose outcomes determine the state of additional child nodes and _top nodes_ whose states do not influence other nodes, and are typically a final output of a Bayesian network model. 

**Computed observation:** An observation that has been calculated deterministically, i.e., using equations.

**Concept:** The description of an entity [UPDATE DEFINITION]

**Conditional probability:** The probability of outcomes in a child node, which depend on the states of all possible combinations of values for the parent nodes. Conditional probabilities are updated to _posterior probabilities_ once evidence is submitted, during the process of Bayesian updating, and can also be updated by _training_ the Bayesian network.

**Context:** The conditions under which a model may be run (in Thinklab terms, conditions under which an _observable_ may be _observed_). In Thinklab this will most commonly be a set of geographic and/or temporal constraints under which a model may be run.

**Data model:** A model statement that directly references a specific dataset to be observed, as opposed to a model that requires Thinklab to search for other means to resolve the concept.

**Deterministic model:** A model where every set of outcomes is determined by predefined model parameters and input data states, and identical results are obtained every time when input data values are the same. 

**Event**: An observable that has both a subject and a temporal component. [UPDATE DEFINITION]

**Extensive physical property:** When measuring an extensive property, the amount of the substance being measured (or more frequently in Thinklab, the area over which it is measured) greatly matter. Mass and volume are examples of extensive physical properties – so when aggregating e.g., tons of biomass or volumes of water, the extent of the analysis must be very carefully considered.

**Extent:** In Thinklab, a given subset of time and/or space that can be used to generate the scale under which a model can be run.

**Identity:** [UPDATE DEFINITION] 

**Indirect observation:** Qualities produce indirect observations: since a quality cannot be observed without an associated subject, an observation of a quality with an associated subject is an indirect observation. [UPDATE DEFINITION]

**Inherency:** The association of specific qualities or properties with a given subject. 

**Intensive physical property:** When measuring an intensive property, the amount of the substance being measured (or more frequently in Thinklab, the area over which it is measured) does not matter. Temperature or density are examples of intensive physical properties.

**Knowledge graph:** A graphical display in Thinkcap that shows the relationship between a selected concept and all other related concepts (i.e., within an ontology).

**Literal:** [UPDATE DEFINITION] 

**Mediation:** A method for defining different states that could be observed for a model, typically used with classifications, in a way that links an observable to traits and inherencies to produce more readable, compact modeling statements.

**Model knowledge:** Models; each piece of model knowledge describes a potential way to resolve a concept (i.e., through data or models). Models may: 1) directly reference and annotate a piece of data (which is itself simply a way to observe an observable) or 2) include equations, algorithms, or external processes to compute an observable, which may include dependencies on additional observables to compute the state of the model’s observable.

**Namespace:** Each project in Thinkcap can contain multiple namespaces – individual files that contain abstract and model knowledge. A namespace can be thought of as an individual file within a folder (i.e., Thinkcap project).

**Object:** [UPDATE DEFINITION]

**Observable:** A subject, process, quality, or event that could be viewed and quantified using a model.

**Observation:** The viewing and quantification of a concept within a given context. A modeling process may yield multiple observations (i.e., instances) of the concept within a given context. Multiple methods may also exist to observe an observable (i.e., various data and models).

**Observer:** Thinklab uses seven observer types, which are included in an observer statement for each model. Observer types include rankings, measurements, counts, values, classifications, proportions, and ratios. Selection of the appropriate observer type for each data or model is critical. 

**Ontology:** A file that contains abstract knowledge of concepts. This includes definitions for concepts, spatial and temporal constraints on concepts, and relationships between concepts. Ontologies in Thinklab are organized by general thematic areas (e.g., hydrology, landcover, soils).

**Parent node:** A parent node in a Bayesian network influences the state of one or more child nodes via an edge (arrow) that indicates influence. The state of the parent node is, however, not influenced by the state of any other node. 

**Posterior probability:** The probability of outcomes following Bayesian updating, which occurs once evidence is submitted for one or more nodes in the Bayesian network. Following the updating process, posterior probabilities thus replace prior and conditional probabilities.

**Prior probability:** The probability of the occurrence of different states of a parent node, prior to the submittal of evidence on the Bayesian network.

**Probabilistic model:** A model where input data are defined using probability distributions rather than constant values. Monte Carlo simulation and Bayesian modeling are two examples of common probabilistic modeling approaches. 

**Process: A process always has an inherent subject.** [UPDATE DEFINITION]

**Project:** A Thinklab project can contain multiple namespaces, as well as other files. Thinklab currently contains several core projects (im and org.aries), a tutorial project (thinklab.tutorial), and individual projects that contain data and models for various ecosystem services (org.aries.carbon, org.aries.sediment, org.aries.water, etc.). Additionally other projects may be created as testing and development spaces.

**Property:** [UPDATE DEFINITION] 

**Quality:** The result of a process; qualities are indirect observations of phenomena and cannot “stand alone” in observations. For example, the sediment load of a river would be a quality (the sediment load is the quality and cannot be independently observed without a subject, in this case the river).

**Raster data:** A cell-based configuration for spatial data, where the extent covered by the data includes a grid of cells at a specified spatial resolution (e.g., 25x25 m), and each cell carries a certain value (e.g., for elevation, land cover, or precipitation).

**Reification** and **De-reification**: [UPDATE DEFINITION]

**Resolution:** The process by which Thinklab applies search algorithms to iteratively match concepts to models as many times as necessary, and uses heuristics and artificial intelligence to define the most suitable models at each step. Once the appropriate model(s) are selected, they will be run and the appropriate observations will be passed back to the modeler. Successful resolution of the model yields a _resolved observation_.

**Restriction:** Within ontologies, a restriction specifies a “requirement” for a concept that links together related concepts. [CLARIFY]

**Scale:** In Thinklab, the spatial and/or temporal constraints on a model, i.e., conditions under which it can be considered valid to run a given model to generate observations.

**Semantic modeling:** A paradigm that maintains the meaning of all entities being modeled through the use of ontologies that provide clear, consistent definitions of concepts and the relationships between concepts (i.e., entities that can be modeled, and data and models to observe them).

**State:** Successful observation of a concept will produce a state for each observation. Individual observations of this state will be distributed across the spatial and temporal scale set by the context of analysis.

**Structural learning:** An advanced form of Bayesian training where an algorithm determines the structure of the Bayesian network (i.e., dependencies between parent and child nodes) based on the data, as well as their associated probabilities. Structural learning is a complex process and is not planned for integration with ARIES/Thinklab in the near future.

**Subject:** The thing to be observed during the modeling process. Subjects may also have associated qualities or properties, some of which may be inherent to a specific subject. 

**Thinkcap:** A graphical user interface (GUI)-based client run within the Eclipse software development environment. Thinkcap communicates with the Thinklab server, which parses and runs the Thinklab modeling language.

**Thinklab:** A semantic modeling language and the system that parses and runs it (i.e., the Thinklab server).

**Training:**The application of an algorithm, e.g., expectation maximization, to learn and quantify the relationships between the nodes of a Bayesian network based on data for at least one parent and one child node within the network. Training replaces user-supplied conditional probabilities with those derived from the data. 

**Trait:** A trait provides further descriptive information about an observable, yielding semantic precision while keeping the size of Thinklab’s core ontologies tractable. Traits can be used, for example, to provide finer temporal specification or to specify levels or frequencies of the observable.

**Transition:** A change from one state to another. For example, in a temporally dynamic model, a transition marks the movement of a simulation from one time step to the next, and is associated with an _action_ that is coded into the model for a given transition.

**Uninformed prior:** A state of total absence of knowledge about a system, in which the likelihood of each of several states are equal (i.e., given four possible states, each would be assumed to have a probability of occurrence of 0.25).

**Vector data:** A non-cell based configuration for spatial data. Data may take the form of polygons, lines, or points. Each of these features will have one or more associated attributes.

