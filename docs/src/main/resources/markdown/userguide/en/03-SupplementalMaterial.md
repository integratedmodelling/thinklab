# Supplemental material

This section contains material that relates to the practice of modeling with Thinklab but is not directly related to the language or its implementation.

At the moment the only content available is a [primer on Bayesian networks](0301.html), given the common usage of these methods in models developed with Thinklab.

We also added the beginning of a Thinklab [cookbook](0302.html) to show some examples of commonly used expressions and programming patterns. This section will be expanded with time.
