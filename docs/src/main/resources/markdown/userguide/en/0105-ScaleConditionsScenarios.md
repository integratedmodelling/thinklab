﻿# Module 5. How to make model choices depend on context

In previous modules, we have often hinted that "Thinklab chooses the best model" to observe concepts directly requested by the modeler or model dependencies. We have not, however, described in detail how that process occurs, or how to instruct Thinklab under what conditions a given model should be used. This section explains how models are chosen and how to control the model selection process.

Five fundamental topics will explain this process:

1. How to restrict the scope of a model to a specific scale (i.e., space or time; in this module we will only give examples about spatial regions);
2. How to use conditional statements to choose between different observers at each computed state;
3. How to use lookup tables to direct model selection when multiple methods exist to observe an observable;
4. How to use scenarios to "force" the use of certain models when particular model elements in an observation should reflect non-default assumptions;
5. How to tell Thinklab how much to trust a given dataset or model, which can become a factor in the resolution process.


## Scale constraints for models and namespaces

One of the most useful things in modeling, particularly when we contribute to a shared model base, is to constrain a model for use only in a particular region. By _region_ we refer to any constraints on the model's scale, so we could refer to a particular _extent_ of space or time (e.g., falling within a certain polygon or temporal period), to a given _resolution_ of either, or both. Thinklab uses a very general definition of scale, which extends to time, space, and even other "conceptual" dimensions, such as the hypothesis space (or multiple spaces) that is reflected in a model's assumptions. Those aspects are experimental and not discussed here, so in this documentation, we only describe how to set spatial constraints. The remaining aspects of scale and scale constraints will be covered in the full documentation.

The following examples describe spatial coverage and constraints, with limited discussion of temporal specifications. In general, anything that we say for space can be applied to time in a similar way, but the resolver may not be yet prepared to deal with all cases of temporal specification.

### Constraining a model

When data, or an object source, are inherently spatial or temporal, it may not be necessary to specify constraints on the model’s usage. In this case, their _coverage_ is automatically recorded by Thinklab and becomes the "default" spatial or temporal constraint for all models that annotate it. For example, since the following data specification:

	model wfs(urn = "im:af.tz.landcover:tanzanialandcover",
		    attribute = "lc") 
	named tanzania-lulc
	as classify im.landcover:LandCoverType into
		im.landcover:AgriculturalArea        if "AG",
		im.landcover:ForestSeminaturalArea   if "NVT",
		im.landcover:VegetatedStillWaterBody if "NVW",
		im.landcover:UrbanFabric             if "UR",
		im.landcover:WaterBody               if "WAT";

specifies WFS access to a vector file for Tanzanian land cover, the bounding box reported by the WFS service for it will be used as its spatial constraint. Because of this, the `tanzania-lulc` model will only be used as a candidate for resolving the observable concept `im.landcover:LandCoverType` when the context of the request overlaps this bounding box. This is usually enough for _raster_ spatial sources, whose coverage is _exactly_ that rectangular bounding box – or should be (“no-data” borders are a different issue that can be dealt with using conditional statements, discussed later). 

In some cases, it may be necessary to "correct" the coverage, either because 1) the data contain regions where they are unreliable, or 2) because (e.g., in a vector file covering a region that is far from rectangular) we want to ensure that other models will be used in regions covered by the bounding box of a preferred dataset, where we know that the first dataset is unavailable or unreliable. In such cases, we can use an `over space` keyword in a similar way as the `observe` statement:

	model wcs(id = "san_pedro:swregap_lulc")
	named vegetation-type-swregap
	as classify aries.carbon:VegetationType into
		...
	over space (shape = "EPSG:4326 POLYGON((-114.816209 42.002018,..))");

(other ways to specify polygons aside from WKT include the use of shapefiles stored either 1) locally on the user’s machine, 2) within a Thinklab project, or 3) on a GeoServer; complete descriptions and examples are provided in the [full documentation]()). Importantly, the spatial coverage specified after `over space` is _intersected_ with the coverage of the namespace, when one has been given. That is, if a data source is used that does not cover the given polygon at all, the intersection will be empty and Thinklab will generate a warning message. Otherwise, the model will only be used in this example to resolve `aries.carbon:VegetationType` in the intersected spatial coverage. This can be useful when it is desirable to select _only_ a specific portion of a larger coverage.

When not working with a data source (i.e., when annotating an _unresolved_ model), nothing changes, except that there will be no _native_ coverage with which to intersect. The `over space` notation can still be used, and the model will only be applied within the specified polygon.

A very common use of scale constraints occurs when annotating computed models that are meant to be used only in a specific region. This could  either be a large range such as the tropical or temperate zone, or a smaller range where certain assumptions about a model’s applicability can be considered valid. The cleanest way to do that is to constrain the whole _namespace_, adding additional constraints to individual models when necessary. The syntax for that is similar but uses the keyword `covering`:

	namespace aries.carbon.local.sw-north-american-deserts
		using im, im.hydrology
		covering space( shape = "....");

Because WKT specifications can be long and messy, a common strategy to keep the code clean is to use a specific namespace in a project to hold _definitions_ for these locations:

	namespace aries.carbon.locations;

	define COASTAL_CALIFORNIA as
		space(shape = "EPSG:4326 POLYGON((-122.01075303165209 38.46721456396898, ...))");

	define MADAGASCAR as
		space(shape = "EPSG:4326 POLYGON((52.778320305152796 -27.644606378394307, ... ))");

	define NORTHERN_ROCKIES as
		space(shape = "EPSG:4326 POLYGON((-111.05 45.01, -104 45.01, ...))");

	define ONTARIO as 
		space(shape = "EPSG:4326 POLYGON((-95.35682310773775 50.520669204331895,...))");


then import the needed definitions into namespaces that need them, using defined identifiers to reference them:

	namespace aries.carbon.local.northern-rockies
		using (NORTHERN_ROCKIES) from aries.carbon.locations,im.soil, im, im.hydrology
		covering NORTHERN_ROCKIES;

The extended form of the `using` clause in the ‘namespace’ statement has been seen before, and can also be used to import symbols such as model names. In addition to improved readability, the ‘using’ clause has the advantage that definitions need be provided only in one place (i.e., per Thinklab project). If it is changed later (e.g., to a detailed polygon after testing it with a simple polygonal bounding box), it will automatically affect all the namespaces that use it.

When a namespace is constrained to a particular region, all the models within it will be constrained to that region. If a model in a constrained namespace also incorporates an `over space` statement, Thinklab will intersect the namespace-level coverage with the model-specific one, further restricting its coverage, as seen before for data sources. So a namespace coverage can be _restricted_ but not _redefined_ on a model-by-model basis.

### Temporal coverage

While we don't yet provide full support for temporal constraints, the `over time` statement should be used when appropriate to specify the time period covered by models or data. The most typical example is to identify the year to which a data source refers:

	model ...
	over time(start = 1995)

This indicates that the data are valid from 1995 _onwards_: they will not be used if the context has an `over time` definition that specifies an earlier year. An `end` year can also be specified, as can  both a ‘start’ and an ‘end’ year. When faced with a choice of two models that are both temporally suitable to resolve a concept, Thinklab will give preference, all else being equal, to the most _current_ one. If the context is temporal, this will be the one whose date is closest to the context's time; otherwise the model with the most recent start date will be used. 

The `time` syntax is much more powerful than indicated here. It is possible, for instance, to specify full dates and times, resolutions etc., but as mentioned above, temporal support in Thinklab is still under development and full details will be provided in a future release. 

## Conditional choice of observer

As we learned earlier, any computations specified for quality models are carried on _each state_ implied by the scale of the context. For example, a 10x10 spatial grid will be computed 100 times, once per cell, and the observer will be called upon to produce a value every time. So far we have seen models in the form:

		model <quality observable>
			as <observer> .... ;

Quality models may have more than one observer, which computes the value in different ways. We can thus assign conditions for choosing an observer, which may depend on other observations. These observers must be _compatible_, i.e., produce the same kind of observer/observation type. For example, measurements and proportions cannot be mixed, because that would break the model’s semantic consistency. The general form for these _conditional_ models is:

	model <quality observable> 
		[observing
			<model dependency> named <name>, ....]
		as
			( <observer 1> ) [if <condition> ], ....
		;

where the part in square brackets should be read as "optional." Each observer is in parentheses and may optionally be followed by the keyword `if` and an expression (using the square bracket notation). If the set of dependencies following the `observing` keyword is given before any observers are specified (i.e., before the `as` keyword), they will be computed before the observers are called in, and their value will become available for use in the expressions. The next example should clarify the syntax. In the (rather twisted!) model below, elevation and slope data are queried and the model will return values of zero, except where elevation is greater than 1000 m, where it will return the elevation as a value:

	model CrazyElevation 
	
	/*
	 * model dependencies - used only to select observers..
	 */
	observing (Elevation as measure im.geography:Elevation in m) named el
	
	/*
	 * two observers with conditionals. Parentheses are not required in this
	 * case but are good practice, as the condition for the observer could be
	 * wrongly attributed to the preceding observer's action if the action
	 * itself is unconditional. 
	 */
	as
		(measure im.geography:Elevation in m
			observing 
				(Slope as measure im.geography:Slope in degree_angle) named pslope1
			on definition
				change to 0 if [pslope1 < 10] )
  		if [el < 1000],

		(measure im.geography:Elevation in m
			observing 
				(Slope as measure im.geography:Slope in degree_angle) named pslope2
			on definition
				change to 0 if [pslope2 > 10] )
		otherwise;

(note that everything between `/*` and `*/` is interpreted as a comment, i.e., ignored by Thinklab). This example also shows how `otherwise` can be used as a catch-all condition instead of `if.` Both the `if` part and the model dependencies are optional; the behavior of this form when neither are supplied is very useful, because the "chain" of observers will be followed in the specified order until one of the observers produces a valid result. This is very useful to yield an alternative model when the preferred one produces `unknown` (no data) values, as it often happens when using spatial datasets. This form can also be used in resolved models. The only thing that cannot be done is to use incompatible types of observers as alternatives.

## Lookup tables

While the conditional form shown above is useful, in some cases it will be easier and cleaner to just _tabulate_ alternative values. Thinklab provides a powerful _lookup table_ syntax, where the values in a column of the table can be returned on the basis of values in other columns. 

Here is an example of a lookup table:

<table>
    <tr>
        <td><b>Landcover</b></td>
        <td><b>Slope</b></td>
        <td><b>Erosion factor</b></td>
    </tr>
    <tr><td>Rock</td><td>*</td><td>0.0</td></tr>
    <tr><td>Sand</td><td> &lt; 1</td><td>0.2</td></tr>
    <tr><td>Grassland</td><td> &lt; 1</td><td>0.04</td></tr>
    <tr><td>Sand</td><td> 1 to 4</td><td>0.4</td></tr>
    <tr><td>Sand</td><td> 4 to 7</td><td>0.6</td></tr>
    <tr><td>Sand</td><td> &gt; 7</td><td>0.8</td></tr>
</table>

In a model, it might be desirable to produce erosion factors that correspond to dependencies for land cover and slope. The standard statement to define a lookup table is similar to a function. It can be used directly in a model or as the argument of a `define` statement, to be referenced in other models and namespaces as shown earlier in the specification of spatial constraints. The previous definition can be stated as follows:

    define EROSION_TABLE as table (landcover, slope, erosion-factor):
        Rock,        *, 0.0,
        Sand,      < 1, 0.2,
        Grassland, < 1, 0.04,
        Sand,   1 to 4, 0.4,
        Sand,   4 to 7, 0.6,
        Sand,      > 7, 0.8;

and used in a model as follows:

    model ErosionFactor as
        proportion ErosionFactor 
        observing
            (LandCover as classify im.landcover:LandCoverType) named land-cover,
            (Slope as measure im.geo:DegreeSlope in degree_angle) named slope
        using lookup (land-cover, slope) into EROSION_TABLE;

While the form above should be intuitive, there are several things to note. First of all, table definition can be set directly in the model if it is only needed there, by typing the 'table' and what follows instead of the EROSION_TABLE identifier:

	  model ErosionFactor as
        proportion ErosionFactor 
        observing
            (LandCover as classify im.landcover:LandCoverType) named land-cover,
            (Slope as measure im.geo:DegreeSlope in degree_angle) named slope
        using lookup (land-cover, slope) into table (landcover, slope, erosion-factor):
        		Rock,        *, 0.0,
      		Sand,      < 1, 0.2,
        		Grassland, < 1, 0.04,
        		Sand,   1 to 4, 0.4,
        		Sand,   4 to 7, 0.6,
        		Sand,      > 7, 0.8;

The choice of which syntax to use is only one of convenience, and should be dictated by the need to reuse the table elsewhere. In general, inline (latter, model-embedded) specifications should be used unless the table is "official" (e.g., reflects accepted standards) or would need to be reused through the code.

The rows of the table can contain simple values to be matched, but it is also possible to use a classifier specified with the `classify` statement as a table entry. Each dependency will be matched following the order of the column list indicated after the keyword 'lookup.' In the previous example, the land-cover value for each point within the context will be matched to values in the first column and the slope to values in the second. The result of the lookup operation will yield values in the last column for the first row that matches both classifiers.

The lookup values to be computed as output can be associated with a column other than the last one by using the `?` identifier in the `lookup` call. For example, the call above is equivalent to `lookup (land-cover, slope, ?)` and the `?` could be used in any position (i.e., land-cover, ?, erosion-factor). This way, a lookup table can be used with greater flexibility.

A `*` classifier will match any value. Be careful when using it - it should always be the last choice within each set of otherwise equivalent combinations, which can be ambiguous when there are several columns. When in doubt, remember that choices are always matched top to bottom.

The names chosen for the columns after the `table` keyword do not influence the way the lookup table works: the list is only used to define the _number of elements required for each row. This is crucial to the proper functioning of the table, as Thinklab does not rely on or mandate indentation and formatting. As with any component of a semantic modeling system, however, it is important that descriptive, unambiguous names are used for column headings, so that the meaning of the table is clear to anyone reading it.

Lookup tables can be even more powerful because each classifier or value can also be an _expression_. Expressions are normally used to compute the values to be returned; in such cases, they will be computed before the value is assigned, and these computations can use all the model dependencies. If they are _matched_ instead, the match will be successful only when the dependency associated with the column and the result of evaluating the expression is the same.

The choice of whether to use a lookup table or a conditional statement (described earlier in this module) depends on the context. One approach or the other may be the cleaner method depending on the model, its purposes, and the modeler’s preferred coding style.

## Scenarios

Scenarios in Thinklab are sets of alternative models used only when the scenario is explicitly activated. When one or more scenarios are active, the models within them will always be chosen preferentially to resolve their concepts. Conversely, models in scenarios that are not active will never be used. For example, a climate change scenario may contain alternative datasets for temperature, precipitation, or other climatic variables. Activating this scenario will guarantee that any observation of precipitation and temperature will reflect the scenario’s assumptions. As a concept can describe observables at any level in the model dependency chain, applying a scenario can affect an entire modeling session or just limited elements of it.

Specifying a scenario in Thinklab is as simple as creating a namespace using the keyword **scenario** instead of **namespace** in the first statement. 

    scenario aries.ipcc.scenarios.hadley.b2 
       using im.geography;
    
    model wcs(id = "usa:sum_hi_wint_lo_hadley_B2")
   	    named summer-high-winter-low-hadley-b2-north-america
        as measure im.geo:SummerHighWinterLow in Celsius;

    ...

Scenarios are activated explicitly by the modeler (in the Thinkcap GUI this is done by ticking the appropriate checkbox for the scenario in the “Scenarios” view) before observing the concept(s) of interest. When observations are made with any scenarios active, any dependency associated with the concepts modeled in the scenarios will then be resolved using the scenario instead of the "regular" knowledge base. If, for example, a scenario contains a land cover change model that observes the im.landcover:LandCoverType concept, any model that depends on im.landcover:LandCoverType will link to that land cover change model, attempting to resolve it using data or models from the scenario namespace, before resorting to the standard data layers. No models or data included in scenarios will ever be used unless the scenario is active.

Some scenarios are inherently incompatible with others; for example, different climate change scenarios should not be mixed together, because they reflect different assumptions about emissions trajectories. Other scenarios could be appropriate for combined use. For example, it might be appropriate to run a scenario for climate change and one for land cover change individually and then in combination to explore their synergistic effects. When scenarios are mutually exclusive, as in the case of multiple climate or land-cover change scenarios, the **disjoint with** clause can be added to the scenario specification to ensure that the listed scenarios are mutually exclusive: 

    scenario aries.ipcc.scenarios.hadley.b2 
        disjoint with aries.ipcc.scenarios.hadley.a2, aries.ipcc.scenarios.hadley.b1
                using im.geography;

    ...
    
As explained, the navigator in the modeler interface we provide (Thinkcap) has a "Scenarios" view with checkboxes that allows their activation. Since we defined the IPCC scenarios as disjoint, ticking the checkbox for one of them will automatically deselect the others. Scenarios not declared disjoint can be used together without restrictions.


## Influencing the model ranking: subjective metrics of quality

Thinklab uses a fairly sophisticated ranking algorithm to select which model to use when more than one model is found that could observe an observable. We do not give full details on the algorithm here, though it is important to list the criteria that it uses. One of these criteria can be influenced by the user-supplied metadata for each model, reflecting the modeler’s "rating" of e.g., data quality or model reliability, so we will explain how to use this feature. Modelers can influence the ranking algorithm in much deeper ways, but that's an advanced (and potentially dangerous) topic that we will not discuss here.

Thinklab currently uses the following criteria to rank models. Criteria are listed in the default order of importance that Thinklab gives them when computing the rank of a model (note that this is a very active area of development, so the criteria, ordering, or definition may change):

1. **lexical scope** reflects whether the model is in a scenario, in the same namespace of the dependency that makes the observation, or in the same project; models that are located "closer" to where they are needed are given preference. Models that are in active scenarios are always chosen above all others. Otherwise, for instance, a model in the same namespace as one that requires its observable will be preferentially chosen. 
2. **trait concordance** reflects the number of attributes (traits) that the candidate model's observable shares with the observable to be resolved. Attributes "percolate" through a model chain starting with the context. So if we are modeling in a im.geography:Region that has been tagged with an attribute (e.g., im:May), models that share that attribute (e.g., data that refer to the month of May) will be chosen preferentially.
3. **scale coverage** reflects how much of the scale defined in the selected context is covered by the model.
4. **scale specificity** reflects the ratio between the total coverage of the candidate model vs. that of the context. Models that are more specific will be prioritized over models that have been constrained to larger contexts or have not been constrained at all. For example, a regional-scale model (dataset) would typically be selected ahead of a national-scale model, which would be selected ahead of a global-scale model.
5. **inherency**: models that are specifically meant to be observed in the particular type of context being used will be chosen preferentially over more general models.
6. **subjective concordance**: this criterion uses a multiple-criteria ranking of user-defined metadata vs. a weight structure that can be redefined on a namespace-by-namespace basis (see below).
7. **evidence**: resolved models with data sources will be chosen preferentially vs. computed models.

Aside from the choice to activate or deactivate scenarios, number 6 is the only criterion that is under direct user control, i.e., the "subjective concordance” criterion. These can be defined by users, but the current convention in Thinklab uses only one criterion on a routine basis, named "im:reliability." Such criteria are specified in metadata at the end of a model statement (before the final semicolon), like so:

		model 
			..... (full model definition)
        with metadata {
        	dc:originator "NCAR GIS Climate Change Scenarios"
            dc:url "http://www.gisclimatechange.org"
            im:reliability 75
            im:distribution "public"}
        ;

Metadata specification is fairly flexible, and _any_ metadata tag or value could theoretically be used without generating a syntax error (though a consistently defined and applied set of metadata conventions is of course highly desirable in a collaborative modeling environment). For these criteria, we use the convention of specifying values using positive integers between 1 and 100. The default intermediate value for any criterion that is evaluated but not given in metadata is 50. So each model will have im:reliability = 50 unless the modeler enters a different value. Unless the default ranking priorities are changed (see below), user-specified reliability will then be used as the value to assess the above-defined criterion 6. If a model is thought to be of particularly poor quality (e.g., coarse resolution, minimally documented, or with other known limitations), it should receive a lower value; models of high quality should receive a higher one. Conventionally we have preferred using the 25-75 range, leaving extreme values for special situations, though for certain well-known, methodologically robust data or models higher values (e.g., 90) may be warranted.

Thinklab provides a vocabulary for other criteria, including for example "openness" that may be used to nudge the model choice towards those that are open source. The current version, however, only uses im:reliability. Each namespace can redefine the entire ranking strategy using the following syntax:

	namespace my.namespace
		resolve using {
			im:lexical-scope 1
			im:evidence 3
			im:trait-concordance 2 
			im:subjective-concordance 4
			im:scale-coverage 5
			im:scale-specificity 6 
			im:inherency 7
			im:scale-coherency 8
			im:network-remoteness 0
			im:reliability 100
		};

where each criterion name not corresponding to one of the "core" criteria (1-7 above) [CLARIFY] is matched to the metadata using the indicated weight. **Use of this form is very dangerous unless the implications of doing so are well understood**. If multiple subjective criteria are present, they will be aggregated using a multiplicative weighted multiple criteria algorithm that we do not discuss here. The modified ranking strategy will be used to resolve any model included in the namespace for which the modified ranking has been created.

Lastly, "blacklist" and "whitelist" namespaces can be added for use in model resolution by using the following syntax:

		namespace picky.namespace1
			resolve from
				good.namespace1,
				good.namespace2

		namespace picky.namespace2
			resolve outside
				bad.namespace1,
				bad.namespace2;

The blacklist (`resolve outside`...) and whitelist (`resolve from`...) are not needed together, as the whitelist will select _only_ those namespaces for resolution. It will effectively ignore the blacklist, which tells Thinklab to _avoid_ resolving from blacklisted namespaces. Conversely, using only a blacklist would eliminate models in blacklisted namespaces from use, making a whitelist unnecessary. When ranking instructions are provided together with a black/white list, `resolve` is only used once:

		namespace my.namespace
			resolve from
				good.namespace1,
				good.namespace2
			using {
				im:lexical-scope 1
				im:evidence 3
				im:trait-concordance 2 
				im:subjective-concordance 4
				im:scale-coverage 5
				im:scale-specificity 6 
				im:inherency 7
				im:scale-coherency 8
				im:network-remoteness 0
				im:reliability 100
			};

Using such specification can give provide power and flexibility to over the way in which a model is resolved. However, it is also likely to lead to situations that are confusing and difficult to manage unless great care is taken, so we suggest that they be avoided by all but expert users.


