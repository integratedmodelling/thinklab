﻿# Module 3. Connecting data to models: semantic annotation and observation semantics

So far, we have seen that _models_ produce observations of _observables_, which can be specified using a concept, identified with an identity if abstract, and optionally augmented with one or more attributes or realms and an inherent subject. All observations, except the "root" one made with the `observe` statement, happen in the context of a _subject_. A model therefore represents a strategy to observe a concept in a context. The result of the observation depends on the observable type (i.e., subject, process, quality, or event): observing a quality results in "data" being produced to represent a _state_, which will be distributed over the scale (space/time) set for the subject.

Writing _models_ is the way to extend the power of Thinklab. The more inclusive the model library, the better the ability of the artificial intelligence engine to select an appropriate model for the specified context that best represents the observable. This definition of a model has the following consequences:

1. "Raw" numbers can never be used to represent the result of an observation in Thinklab: every number will always "be" something, i.e., have an observable associated with it.
2. Models 'annotate' values, data sources, equations, or external computations using the same syntax: in other words, data and models are merely two different ways of observing an observable (_data are models_).
3. In the case of computed models (e.g., equations), "inputs" are expressed as concepts. Thinklab resolves the concepts at run time based on the context.

Writing models consists of the following main steps:

1. Choosing the semantics for the observable. 
2. Choosing and describing the "source" for the result of observing the observable: e.g., a value, data set, equation, external program.
3. Choosing the _observation semantics_, i.e., the type of observation made. In the case of _things_ this is trivial, as these are _direct observations_, semantically equivalent to simply _acknowledging_ the thing observed. For qualities, observation semantics requires more specification, for example of measurements, classifications, rankings, as will be seen later. 
4. Creating metadata to help Thinklab track the provenance (i.e., origin) of the information during resolution, and choose the proper model when more than one model is available for the same concept. This will also be discussed in detail in [Module 5](0105.html).

Based on these principles, we see that an observation can be made in different ways, including extracting numbers from a data file, computing an equation, or calling an external program. This section only refers to _resolved_ observations, where the states of the observation (numbers, categories, etc.) are known at the time the model is written: this applies equally to simple data values as to external data files, databases or data retrieval services. [Module 4](0104.html) describes _computed_ observations, which depend on computations.

## Choosing a concept

Choosing a concept is a fundamental topic that is covered more extensively in [the full documentation](). For the purposes of this guide, it is important to remember that extending Thinklab's data and model integration capability depends entirely on the reuse of the shared knowledge base. As each Thinklab namespace is an ontology, concepts may be created at any point and by anyone, and nothing prevents a modeler from creating a new concept per each model without thinking of integration. But in a collaborative environment, _new_ concepts should only be added when absolutely necessary and with community agreement on terminology and meaning. Many concepts can be created by combining existing observables with traits and inherent subjects as explained in [Module 2](0102.html). For the purposes of this discussion, the reader is reminded of three key points:

1. Understand and properly use the fundamental types of knowledge: subjects, qualities, processes, and events. Errors in attributing these fundamental types are certain to lead to trouble, both when running models and when interpreting outputs.
2. Learn to use traits, fundamental physical properties, and inherency. When in doubt, use a temporary concept that can easily be traced, and ask the larger modeling community for feedback.
3. Be mindful of common mistakes in attributing semantics to either data or models. A list of common ontology-related misunderstanding is in development to be integrated with this documentation.

## Choosing the data or subject source

Models may have a pre-existing source of information for the semantics they provide. These are referred to as _resolved_ models. Ultimately, all observations must end up as resolved models for each model input. Information sources can be provided for both data and subjects: examples include the value of a constant (e.g., the gravitational constant _g_ or the boiling point of water), data from datasets (e.g., a precipitation or population density map), or subjects from datasets (e.g., villages, watersheds, or roads). Examples of each are provided below to illustrate how the `model` statement uses semantics to dress a "bare" reference to different kinds of data (all specified immediately following the word `model`).

### Values

    model 100 as measure im.chemistry:Water im.physics:BoilingTemperature in Celsius;
    model false as presence of im.theology:Satan;
    model im:High as classify (probability of im.climate:ClimateChange) by im:Level;

All these statements (which definitely belong to subjective [scenarios](0105.html)!) show how the bare value of a quality can be set to a constant, which the resolver will take as the value in the context of validity of the previously specified model. Normally this form, which shows the most direct example of semantic annotation of data, is only used when testing or when creating scenarios, and most likely only for parameters that models should use under carefully controlled conditions: annotating constants as shown above will rarely be used in other ways. More typically, data annotations point Thinklab to data sets, which can be stored externally or directly stored with the other files in a project. This is done in the language by using [functions](0202.html) that can define both data and subject sources, as shown below.

### Data sources

Functions are identifiers followed by lists of named arguments within parentheses:

	<function-name> ( <argument-name> = “parameter_value”, …)

The list of arguments may be empty, but if it is not, each argument will have a name and a value separated by an equal (`=`) sign. The function names and parameter names are not keywords of the language, so they may change and new functions may become available at any time. Thinklab provides a variety of functions, capable of bridging to several commonly used file formats and web-based data retrieval services; each function has its own argument names and rules for validation of arguments. To date, the most commonly used functions in Thinklab connect to spatial data. The following examples detail functions to access raster data from a Web Coverage Service (WCS) and from the filesystem on a user’s computer, respectively:

    model wcs(urn=”im:global.geography:dem90m”, no-data = -32768.0) as measure im.geography:Elevation in m;

    model raster(file="data/landcover.tif") as 
        classify im.landcover:LandCoverType into
            im.landcover:Urban        if 200,
            im.landcover:Agricultural if 201,
            ....
            ;

Like the ‘raster’ function above, the ‘vector’ function can also be used with a ‘file’ argument to point to a file stored on a local disk within the user’s project directory. Employing local files limits opportunities for collaboration and sharing, and it is very onerous to handle for the modeling engine. It is therefore recommended that they should only be used during model testing and development. Importantly, models that refer to local data files should not be shared in the integratedmodelling.org knowledge repositories, as all the data sources referred to in shared models must be accessible to everyone who shares the model itself.

Vector data often specify _subjects_, like roads or bridges, but sometimes they define distributed qualities in a more compact data storage form than raster data. For example, it is common to find vector representations of land cover type, although each polygon in the coverage is not a "subject" in a strict sense. Thinklab can use such data for qualities, as long as an attribute in them contains information in a recognizable form. In the following example, the attribute luc_id from the AFRICOVER vector dataset for Tanzania is accessed using the Web Feature Service (WFS) function:

    model wfs(urn="im:af.tz.landcover:tanzanialandcover", attribute="luc_id") as 
        classify im.landcover:LandCoverType into
            im:Agricultural if "AG",
            ....
            ;

When the resolver decides to use vector data in a grid spatial context, it will automatically rasterize the polygons, extract the value of the `luc_id` attribute, convert it into the concept indicated in the classification, and attribute it to each point of the grid. A numeric attribute would be required to provide a value for a numeric quality like slope or rainfall quantity. Note that the states of the observation will be `unknown` (a kewyword that expresses the notion of 'no data' in Thinklab) where no polygon covers the context.


### Subject sources

In many cases, data sources can be seen as providing _things_ rather than qualities. For spatial data, a common occurrence is vector files (such as shape files) where each record represents one distinct object, such as a road or a bridge. Not all vector files represent objects – some just use a vector representation as a convenience to lump together qualities that have the same values – but many do. In such cases, the models can annotate the sources as _subject sources_ using the keyword `each` and avoiding the observer statement after `as`:

	model each wfs(urn = "im:global.infrastructure:global_rail_merge") 
    named railroad-global
    as im.infrastructure:Railway;

When located within a subject model (with the `each` keyword and only a subject concept after `as`), the source will be interpreted as a source of subjects. Observing the `im.infrastructure:Railway` concept in a context covered by the data above will generate as many railway subjects as there are in it, clipped to the context as necessary.

There is much more to be said about subject models, specifically about quality models that can be automatically inferred from them. We will briefly discuss some examples after the discussing observation semantics for quality models. Another topic that will be discussed farther along is how models can be written to specify what to _do_ when the subject is built – which enables what is commonly called _agent-based modeling_ in Thinklab. We will leave details on this advanced aspects for a further section.

## Observation semantics for qualities

Observing qualities produces what we usually refer to as _data_, i.e., information that approximates the value of the _state_ of the observable by referring it to a known set or scale. This "external" reference system is what we refer to when saying that qualities produce _indirect observations_. For example, elevation needs to refer to a unit of measure such as meters before a model can observe "how many units" of elevation are in a given location. Consider a 1 x 1 km region of land. Observing the elevation quality at the 100-m resolution will produce as many of these "data" as needed to cover the _scale_ of the context - e.g., 100 numbers. In Thinklab, this set of 100 numbers counts as _one_ observation of elevation in that context.

Thinklab provides a number of _observer statements_ that help the user to specify the system of reference for a quality observation. This is equivalent to specifying the _observation semantics_ for the observation. In Thinklab the observation semantics are, in general, only described through the observer. So, for example, a concept called `Measurement` will not be found in the Thinklab ontologies and no concepts of this kind should be added. For all practical purposes, the semantics of the observable and that of the observation are independent, and this is an important founding principle in semantic modeling. Some validation steps are taken to ensure that observables are appropriate for the observation and some exceptions exist to the above rule; still, observation types are best thought of as not directly _implied_ by the observable concept

Data models are typically used by Thinklab when a [computed model](0104.html) defines a [dependency](0104.html) for an observable. This will be shown in detail later, but many models will state a dependency like: 

    model .... 
        as ....
        using
            (Elevation as measure im.geography:Elevation in m) named el,
            ....
            ... instructions to compute the result based on 'el';

In this case, the code instructs the Thinklab resolver to look for the most appropriate model that satisfies it. If a data model is available for the observable, it will be chosen preferentially. Otherwise a computed model will be chosen if available, and its dependencies will be resolved in the same way. Observations made in different units can be converted when their relationships are clear: for example, a dependency on a measurement of elevation in meters may be matched to a data model for elevation in feet, and Thinklab will automatically translate the units.

Thinklab provides semantics for the following observation types: *ranking*, *measurement*, *count*, *valuation*, *classification*, *proportion*, *percentage*, *probability*, *ratio* and *uncertainty*. Each observer type has a correspondent statement that must be used after the keyword `as` (e.g., ‘as rank’, ‘as measure’, ‘as count’), in any model that has a quality as its observable. Observers create values for the states of the concept they describe that may be different for different observers. In the sections that follow, each observer type is explained, including a description of the values produced, a description of how these data models will be used when matched to a dependency, and examples of use.


#### Ranking

Rankings produce numeric values that may use a scale (e.g. 0 to 1 or 1 to 10) or be unbounded, may be restricted to being integer numbers, and are meant to describe qualities for which a higher rank means a higher "level" for the observable concept. They do not have units (they often translate what is referred to as “arbitrary units” colloquially), and as such they should only be used when measurement or valuation observers are not an option. Rankings are typically used to express preferences or survey data, where the quality described uses an arbitrary numeric scale.

    model wcs(...) as rank ...;

    model wcs(...) as rank ...:PerceivedDanger 1 to 5;

This annotation _will produce_ floating point numbers or integers if requested. If a range is given, values outside of that range that are produced by the data source will generate a runtime error, indicating a mismatch between the data source and its intended semantics. Some data source functions may offer the possibility of restricting the output range.
This annotation _will match_ any dependencies for a ranking of a compatible observable. Note that:
* If a `rank` statement in a dependency does not specify a scale, both ranking models with and without a scale will match it;
* If a `rank` dependency defines a scale, only rankings with a scale will match it; if the observables match, the scale is seen as a “unit” of sorts, so the matched ranking may have a different scale, and the values which will be converted to the scale of the dependency before being used;
* Dependencies that define a scale will not be matched to rankings of a compatible observable that do not define a scale.


#### Measurement

Measurements indicate _physical properties_, such as mass, energy, or entropy. Currently Thinklab will only generate a _warning_ when a measurement is defined for a quality that is not a physical property, to give the modeler time to define concepts and still be able to test models. However, properly heeding the warning means that no user should ever "publish" a model with that warning to a public namespace.

More information about physical properties (and their fundamental distinction in being _intensive_ or _extensive_, which is very important for their aggregation) is available [here](http://en.wikipedia.org/wiki/Intensive_and_extensive_properties). For this module, it is important to understand that physical properties are measured and quantifying the measurement relies on the use of a reference system with standard _units_ (e.g., mm, kg, ha). Thinklab requires the user to specify units according to a well-identified [syntax](0201.html) and will validate units in both the client and the server.

    measure ...

This annotation _will produce_ floating point numbers in the specified unit of measure.
This annotation _will match_ any dependencies with a compatible observable. Units will be converted as necessary. If the annotation is correct, compatibility of observables should guarantee compatibility of units, and this compatibility is enforced to some extent in Thinklab, but not yet to a level that guarantees full coherency and safety. Consider the points made previously about aggregation and the use of distributed extents in defining the observables to be certain that observables and units are properly aligned.

Note that scientific practice and published metadata are often sloppy in defining units, both in terms of syntax and in using units for qualities that are not actually measurements (e.g. "10 people" or "20 EUR"). A semantic modeling system cannot afford that, so the appropriate observers must be used to handle qualities that could be called measurements (Note: the two examples above are a `count` and a `value` respectively - see below). Additionally, the raw values of some spatial data may require a conversion factor (e.g., multiplying by 0.01) to express the data in standard units. Such conversions can easily be conducted during data annotation; for example:

    model wcs(urn = "im:na.us.climate.annual:annualprecip")
    	named precipitation-annual-2007-usa
        as measure im:Annual im.hydrology:PrecipitationVolume in mm
        over time (year = 2007)
        on definition change to [precipitation-annual-2007-usa * 0.01];

When in doubt, and particularly if modeled values appear to be consistently ‘off’, check the metadata.

#### Count

Counts are often considered measurements in common practice, but they are semantically unique, as they refer to a set of _countable objects_ of a common type, and define the very particular quality that comes from counting them. The Thinklab `count` observer is special because it _requires_ its argument to be a subject and produces a different concept. For example, when counting im.demography:HumanIndividual, the resulting observable (a quality) will be im.demography:HumanIndividualCount. Two additional observers, ‘presence’ and in some instances, ‘classify’, may also produce new semantics. At a minimum, only the subject need be provided to the count observer:

    model 1 as count Universe;

If, however, a count that is distributed over space, time, or both, is desired, a unit to define the extent of the distribution is required. For example:

    model wcs(urn="aries:global-populationdensity-2006") as
        count im.demography:HumanIndividual per km^2
        over time (year = 2006);

Using the `per` syntax dictates the unit that would otherwise represent the denominator if the annotation (incorrectly) specified the count as a “measurement … in people/km^2”.

This annotation _will produce_ floating point numbers, although this is a bit of a semantic blasphemy, as countable subjects should not normally maintain identity when split, so only integer values are semantically correct. The ability to automatically account for densities or rates when the context is spatial or temporal requires enabling fractional counts. Note that the quality resulting from this model will be represented by a different concept, created (if necessary) by appending "Count" to the subject concept.
This annotation _will match_ any other count of the same observable over compatible extents. Units will be converted appropriately and [automatic aggregation]() will take place.

#### Value

It is common to see metadata referring to value observations as "measurements" of value - either monetary or in "arbitrary units." A semantic system needs to do better than that: value is semantically very distinct from the generic measurement. It entails both subjective and objective comparison between different quantities and the value system that underlies valuation is much more fluid and culturally dependent than a physical measurement.

Value is commonly assessed monetarily, but that is by no means the only way to observe value. Thinklab allows the specification of monetary or conceptual currencies.

Currently, the use of the value observer should be considered experimental and support for value conversion in Thinklab (i.e., in converting between currencies or within a single currency to account for inflation) is limited. The syntax allows currencies to be specified like so:

	model ... as value of ...:PropertyParcel in USD@2004;

Because values can be non-monetary, the specification of the currency does not need to be monetary. If a currency is given, it must be qualified by the year (or month/year) after the `@` character, as the definition of any currency is meaningless without an historical context (which is independent of the possible temporal context). If the currency is not monetary, a concept expressing the type of value should be specified (e.g. “Affection”). In all cases, some currency is necessary, and the semantics defined by this statement will reflect both the observable and the way it is valued.

Note that while Thinklab is expected to enable automatic currency conversion, this feature is in development and is not available yet. For all practical purposes, `value` will behave the same as a `measurement` without a scale even when a temporally-specific currency is specified.

This annotation _will produce_ floating point numbers.
This annotation _will match_ any value that has the same currency and year, or the same currency concept. In the future, translation to other currencies and years will be provided.

#### Classification

Classifications are often used in modeling to define categorizable attributes of a common type (e.g., land cover type) or to "discretize" continuous values into discrete levels that can be more easily understood or fed into computations that require categorical data. In a semantic system, we need to unravel the meaning of categories, and using string values won't do us any good - but in the semantic world, the idea of "categories" corresponds very closely to ontologies and concepts. Indeed, the `classify` observer produces _concepts_, and it is carefully designed to be able to express all the semantic nuances described for observables: types, traits, and discretized levels. As a result, the `classify` observer has several options, although all should read naturally.

In a classification, the state of the indirect observation is a concept. This corresponds to what is commonly called a _categorical_ observation. Because in Thinklab nothing can be devoid of semantics, it is not possible to produce categories that are simply strings (e.g. "high" and low"). Any categorization must have clear semantics, so any time models call for categories, a classification must define the categories in the terms of an explicit concept hierarchy.

Classify has three different forms, which are used in different circumstances. Direct and indirect classifications simply define the list of concepts that make up the classification, and are used when the concepts are produced directly in the model, for example through an algorithm. Classification using observables or traits is used when the concepts are produced by reclassifying some other observation; classification rules are defined to produce each concept. The latter form can use a trait as the basis for classification.

As a special case, the `classify` keyword can include only the observable concept when stating a dependency. This special case will be explained in [Module 4](0104.html).

##### Direct classification

In a direct classification, the concepts that form the concept space are listed directly after the observable concept and the keyword `into`:

	model ManureType as 
    	classify im.agriculture:Manure into PigManure, CattleManure, PoultryManure
      observing 
        	(PigManureProportion as proportion of im.agriculture:Pig  in im.agriculture:Manure im.core:Mass) named pig-manure, 
        	(CattleManureProportion as proportion of im.agriculture:Cattle in im.agriculture:Manure im.core:Mass) named cattle-manure, 
        	(PoultryManureProportion as proportion of im.agriculture:Poultry in im.agriculture:Manure im.core:Mass) named poultry-manure
    	using rand.select(
        	distribution = (pig-manure cattle-manure poultry-manure),
        	values = (PigManure CattleManure PoultryManure)
    	);    

This model creates four concepts: ManureType, PigManure, CattleManure, PoultryManure in the namespace where the model is declared. The last three concepts are children of the first. The concepts are produced directly by the `rand.select` accessor (which selects one of the child concepts based on the probabilities specified by each of the observed dependencies) there is no need to specify any other criterion for classification. The specifics of the rand.select accessor are treated elsewhere.

##### Indirect classification

The indirect classification assumes that all the concepts in the concept space have already been defined and tagged with metadata. The metadata field is used to link the appropriate concept to the value extracted from a data source or observed by a mediated observation. The link is established with the `according to` keyword sequence followed by the metadata field that contains the linking value. Concepts with established numeric encoding, like those used in numerically classified land-cover or soil order data, offer significant time savings and limit the ability of the user to introduce error in the coding scheme:


	model data.wcs(id = "europe:corine2000", no-data = 255) named corine-2000
    	as classify im.landcover:LandCoverType according to im:numeric-encoding;

This will only work if the children of the observable concept (in this case `im.landcover:LandCoverType`) are tagged in the respective ontology with an `im:numeric-encoding` field that specifies the number that will be extracted from the `data.wcs(...)` data source. An example LandCoverType ontology snippet is below:

	...
	class LandCoverType 
		has children
			...
	    	(DiscontinuousUrbanFabric with metadata { im:numeric-encoding 112 })),
			(class IndustrialCommercialTransport
    			has children
        			(class IndustrialCommercialUnits with metadata { im:numeric-encoding 121 }),
        			(class RoadRailNetwork with metadata { im:numeric-encoding 122 }),
        			(class PortArea with metadata { im:numeric-encoding 123 }),
        	...

This approach transfers the burden of annotating the encoded value for a datasource from the model to the concept. This is worth doing if the concepts are used for more than one data source; otherwise the effort is the same and it's just a matter of stylistic preference whether to use this form or the mediating classification discussed next.

##### Classifying values into observables or traits

The most complete classification specifies _classifiers_ that attribute the result concept according to results of the "incoming" information (e.g., the values that come from a dataset). A common use of classifiers is to annotate a data source:

	model wfs(urn = "im:af.tz.landcover:tanzanialandcover",
		    attribute = "lc") 
	named tanzania-lulc
	as classify im.landcover:LandCoverType into
		im.landcover:AgriculturalArea        if "AG",
		im.landcover:ForestSeminaturalArea   if "NVT",
		im.landcover:VegetatedStillWaterBody if "NVW",
		im.landcover:UrbanFabric             if "UR",
		im.landcover:WaterBody               if "WAT";

The _classifier_, the part that follows the `if` keyword in each row, can accommodate many kinds of expressions, discussed in detail in [Module 4](0104.html) and demonstrated further in the [Cookbook](0302.html). Numbers, strings or concepts can be matched using 'is', numeric intervals using a syntax like '1 to 10', or partial intervals using operators like '< 10' or '>= 4.3'.

Data often represent a classification according to a specific _aspect_ of the main observable, one that is best described with an attribute. In such cases, classifications should be annotated using the main observable and instead of defining its subtypes in the classifiers, use the 'by' keyword and mention the specific trait being observed. Then, the classifiers will be defined by the appropriate attributes. A typical case is when data describe a discretization in subjective levels:

	model wcs(...) as classify im.policy:Poverty of im.demography:HumanPopulation by im:Level into
			im:High     if 4,
			im:Moderate if 3,
			im:Low      if 2,
			im:Minimal  if 1;

Note that if data are available and the modeler wants a discretization of _known_ numbers, the proper annotation is not `classify` but the actual numeric observer; models without data sources can be used later to discretize the value. For example, if a discretization of im.geography:Elevation is needed, do not annotate a data source as:

	model wcs(...elevation data...) as measure im.geography:Elevation in m
		discretized by im:Level into
			im:High if > 2000,
			....                     (An INCORRECT example)

This would make available a model of elevation data that ranks it into subjective values that only make sense for a specific application, losing the numeric information in the data. This model would match _any_ dependency for elevation data, and produce discretized midpoint numbers when the requesting side wants numbers, with a great loss in precision. A proper way would be to just provide the undiscretized measurement in a public repository, and create a local model that translates it in the same namespace where it will be used, marked `private` to ensure that nothing else will use it:

	namespace my.namespace using im.geography;

	(RIGHT, BUT STILL NOT PERFECT) 

	private model Elevation as 
		classify im.geography:Elevation by im:Level
			im:High if > 2000,
			....;

We will see in [Module 4](0104.html) that this kind of requirement can be stated directly as a dependency, eliminating the need for a private model (and the risk of forgetting the `private`, making it available for others with potential problems) and keeping all the subjectivity nicely encapsulated within the context where it is needed.

Values in the data source that fall outside the space defined by the classifiers in a `classify` statement will appear as `unknown,` in the final observation, the Thinklab definition of "no data". This is sometimes a handy way of selecting only a few categories or values from a data source. Operations, such as sums or subtractions, which have an unknown operand will result in an unknown result.

#### Proportion and percentage

Proportions and percentages are two almost identical ways of referring to a quantity that represents a portion of an implicit "whole" amount. The semantics of the quantity and the total amount usually only differs by a trait, with the amount at the numerator being more specific than the one at the denominator - for example, the proportion of "vulnerable" land over the total land. Thinklab allows both to be specified:

	model ... as percentage of im.agriculture:Pig  in im.agriculture:Manure im.core:Mass;

In the case above, the quality concept is stated in the second part `im.agriculture:Manure im.core:Mass`: an abstract quality (mass) qualified with a Manure identity. The “specific” case that is compared with the less specific manure mass is identified by another identity after `in`. The whole statement read as “annotate these data as the percentage of manure mass that can be attributed to the Pig identity”.

You can use the keywords `percentage` or `proportion` to annotate data that contain numbers in the interval [0-100] or [0-1], respectively. Thinklab will match proportions and percentages to each other, converting the numbers as necessary. 

Some concepts may be _inherently_ defined as proportions or percentages: for example, ecologists are accustomed to work with “canopy cover”, which represents the proportion of land covered by the tree canopy in a forest when seen from above. In such cases, when you certainly don’t want to think about the “generic ground” and the “canopy” that covers it, you can simply use the concept without specifying `of` and `in`:

	model … as proportion im.ecology:CanopyCover;

Note that in such cases, the concept will be validated as actually expressing a proportion, and an error will be flagged if that’s not the case. In other words, Thinklab will let you use a simpler semantics only if the semantic groundwork has already been done correctly in the ontologies you are using.

#### Ratio

Ratios are interesting because they don’t have _one_ observable but _two_. Indeed, our definition of an observable is something that is “grounded” in reality; while ratios only exist as comparative _observations_ of two observables. There are no true ratios in nature. For this reason, we provide only the full form of the observer where both compared qualities are annotated independently:

	model … as ratio of im.ecology:Soil im.chemistry:Carbon im.core:Mass to im.ecology:Soil im.chemistry:Nitrogen im.core:Mass;

Because there are no concepts that can be seen as natural ratios, we do not provide a simpler form as we did with proportions and percentages. This model will match any ratios of compatible observables and produce floating point numbers. An appropriate concept will be created by the annotation: in the case above, the concept will look a bit complicated – something like `im.ecology:SoilCarbonMassToSoilNitrogenMassRatio`. Fortunately you can state your dependencies using the ratio observer, and never have to write that.

Note that knowing the ratio of two quantities allows an intelligent software to infer one quantity when the other is known. So for example the above model should be enough to satisfy an observation of carbon mass in the soil when a model producing the nitrogen mass in the soil exists. This capability is in development in Thinklab.

#### Presence

Presence is the quality corresponding to a subject, process or event “being there” in the context. As such, it can only take the values `true` or `false`. Presence is the observational equivalent of a “boolean” value in data-oriented languages. 

Thinklab provides a simple statement to annotate presence data:


	model … as presence of im.infrastructure:Building;

Note that the concept after `of` must be a _thing_ and can not be a quality, as there is no such things as “presence of a temperature” for example. If you need to know, for example, whether temperature is above 25 Celsius, the statement you are looking for is not a `true` or `false` statement, but a classification of a restricted conceptual hierarchy that classifies temperature into “above 25” or “below 25”. 

Like in proportions and percentages, it is possible to encounter concepts that are semantically defined as types of presence, so the form without the `of` is admitted with validation. Note, however, that this is rare and the form above is the most common one.

This model will match any dependency requiring presence of a compatible observable. The values resulting from it will be `true` or `false`, and a quality concept such as `im.infrastructure:BuildingPresence` will be created if not already existing. Note that if you have annotations of the subjects themselves (e.g. a vector file of buildings) it is usually not necessary to provide a presence annotation, as that is automatically inferred by Thinklab: see the section on de-reification below for details.

#### Probability and uncertainty

The last two observers refer to the observation of the likelihood of something happening or being observed correctly. They are provided to simplify annotation of risks and to make it easy to track model and data uncertainty.

The probability observer has both the explicit form 
	
	model … as probability of im.physics:Fire within im.ecology:Forest;

and a simpler form for concepts that are naturally probabilities, such as risks. In the explicit form, the observable after `of` must be something that _happens_: an event or a process. Again, you may be tempted to conceptualize probabilities of qualities or subjects as semantic shortcuts, but Thinklab will not allow that. The observer will produce numbers in the range [0-1] and will flag as errors any data output that falls outside this range. The direct form will produce a concept such as `im.physics:FireProbability`.

The uncertainty observer refers generally to the spread of a value around its maximum likelihood estimator, but does not mandate any specific type of uncertainty: simply the _meaning_ for it, producing a numeric expression that means “less certainty” when it is higher. It is used often to annotate expected outputs from external models that produce estimates of uncertainty along with values, such as Bayesian networks. It is more rarely used to annotate data, as few direct estimates of uncertainty are available. Like probability, it has a direct form with `of` and an indirect one for observables that are defined as uncertainties. Unlike probability, it has no constraints on the type of observables that can be used: you can estimate the uncertainty of anything, including another uncertainty. Also unlike probability, it can produce floating point numbers in any positive range. The observer will create a concept such as `im.ecology:CanopyCoverUncertainty` if that does not exist already.

### Discretization

All numeric observers (`rank`, `measure`, `value`, `ratio`, `proportion`, `percentage`, `count`, `probability` and `uncertainty`) can be discretized into discrete levels. The most common way to do it is by using a trait, often using im:Level or a trait that derives from it:

	model wcs(...) as measure im:Length of im.ecology:Leaf in cm
		discretized by im:Level as
			im:Low    if < 10,
			im:Medium if 10 to 30,
			im:High   if > 30;

In this case, the output of the model is a concept as in a classification, but the quantitative meaning is not lost: when used in a numeric context, the values will be automatically converted in the midpoint of the intervals as long as the boundaries are finite (they are not in the example above). When specifying a discretization, the classifiers must be continuous (the endpoints must touch): the convention for intervals specified as above is that the interval is closed at the beginning and open at the end.


### Note: spatial densities and temporal rates refer to observations, not observables

It is important to distinguish between the primary identity of what is to be observed and those secondary aspects of the observation that depend on being distributed over space or time when defining the semantics of an observable. It is common to encounter concepts like "population density," for example, whose definition as a "density" depends on the observation being made over a spatial context (e.g., “per hectare” or “per square kilometer”). In Thinklab, space and time are part of the _context_; for this reason they don't enter the semantics of an observable, but are automatically handled when they are observed over space, time, or both. Confusion in semantic modeling is likely when the common attribution of concepts used in data or models implies that they are distributed over a specific type of context (spatial or temporal), even though they may be used in others. So for example, a concept named PopulationDensity (which implies that applied to the population observable is distributed in space) should be avoided, in favor of the generic semantics of population without such implications. In Thinklab, the appropriate observable for this case is a _count_ of people, expressed for example as

	model .... as count im.demography:HumanIndividual per km^2;

Only subjects can be counted, so this model, which contains all the semantics of what population density is (a count of individuals over space),requires a _subject_ concept (im.demography:HumanIndividual). The model implies a density, but only when the data source is spatially explicit. Thinklab will, if necessary, aggregate densities automatically into total numbers of individuals when the model is matched to a context that is spatial but not _distributed_ in space (e.g. a city defined with only a single polygon without grid cells). The same applies to rates, which are the equivalent of densities with respect to time. Observables and observations are independent, and their semantics should not be "contaminated" by concepts that have to do with the characteristics of the context of observation. Avoiding references to spatial densities and temporal rates will maintain semantic consistency and help to minimize confusion.

## De-reification of subject models

Having discussed quality models in detail, we can go back for a moment to subject models. Observations of subjects are _direct_, so it is not necessary to state anything more than the subject type. Direct observations simply create subjects. So an object source such as a shapefile can be annotated to create subjects as seen above. In addition, the existence of a subject also _implies_ certain qualities: an obvious one is the `presence` of the subject, which takes the values `true` or `false` according to a subject being in a point of the context or not. Thinklab will automatically generate quality observers for presence whenever a subject annotation is encountered, so that observation of `presence of im.infrastructure:Railway` will be made automatically by rasterizing the line contexts coming from the subject annotation above, and returning `true` for each point where a railway is present.

Further, semantics can also be used to specify the qualities referring to each subject. Thinklab expects that subjects produced by a subject source may come with attributes, Adding _observers_ (see below) to the subject will determine their qualities. Consider this example:

	model each wfs(urn = "im:global.infrastructure:grand_reservoirs_v1_1") 
    named reservoirs-global
    as im.hydrology:Reservoir
    interpret
    	GRAND_ID as im:name,
    	AREA_SKM as measure im.core:Area in km^2;

In the example above, the `interpret` keyword introduces a list of attributes and the ways they should be interpreted when creating subjects. As each reservoir subject comes with an attribute (`AREA_SKM`) that contains a measurement of its area in its original source, we specify the second attribute with a quality observer for it. This has two effects in resolving dependencies:

1.	any dependency for a Reservoir subject that is satisfied by the subject source will create reservoirs that contain the quality of Area;
2.	a quality dependency such as `measure im.core:Area within im.infrastructure:Reservoir …` will be satisfied by a model inferred from the above specification. The model will be matched to the context of observation: for example, when asked to measure reservoir area over a spatial grid context covered by the subjects above, Thinklab will automatically rasterize the `AREA_SKM` attribute and if necessary, convert the values in the units requested.

This ability of creating qualities from things is called _de-reification_ (removing the "thing-ness" in things) and is very useful to make the most out of data. You could also annotate each quality explicitly:

    model wfs(urn = “im:global.infrastructure:grand_reservoirs_v1_1”, attr=”AREA_SKM”) 
        named reservoirs-area-global
        as measure im.core:Area within im.hydrology:Reservoir in km^2;

But this is made unnecessary by the subject annotation, which provides this and an automatic `presence of im.hydrology:Reservoir` along with the subjects themselves – three birds with a stone.

You may have noticed the `GRAND_ID` attribute, annotated as just providing a single metadata property, im:name. That will not produce any observation, but tells Thinklab that the subjects should have metadata reflecting the content of that attribute. You cannot observe a name – no physical reality in that – but each reservoir produced can be given the name specified in the attribute, a useful property for visualization. That is the equivalent of the name you specify in an `observe` statement – subjects have individuality, so they can have their own metadata. 


