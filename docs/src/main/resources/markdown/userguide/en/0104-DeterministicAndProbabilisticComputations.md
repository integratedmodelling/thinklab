﻿# Module 4. Computing deterministic and probabilistic observations

What is most commonly referred to as a _model_ (outside of Thinklab/ARIES) is actually an algorithm that computes quantities that describe "virtual observations," which reflect a hypothesis about the observable that is expressed in equations or other processes. Such modeling relates the state of the observable to that of other observables. For example, we can interpret tree biomass as dependent on precipitation, soil type, and incoming solar radiation present or past. With this in mind, we can define the two main differences between "data" and "models" in Thinklab. Models, but not data, can:

1. Define an equation, algorithm, or external process to compute the state of the observable;
2. Have _dependencies_, i.e., the statement of _other observables_ that need to be observed before the state of the model's observable can be computed using some type of algorithm.

These two points introduce additional specification requirements, rather than structural changes, when compared to [_data models_, i.e., data,](0103.html) in Thinklab. We have defined data as _resolved models_ before, because data don't require any other observable to be computed before them. Therefore it is guaranteed that data will produce observations when used in a compatible context. Because the definition of a computed model is just like that of data with some added specifications, we use the `model` statement to annotate both data and models. Data are effectively a pre-computed model of the observable, which does not require any further observations but is, like any model, dependent on scale, assumptions, etc. Data models may also include equations, which are useful when, for example, when we wish to modify raw data before it is used as the state for an observation.

Because Thinklab is a semantic system, each dependency is defined semantically, i.e., by providing the details about the identity of _what_ needs to be observed, not by giving the system equations or other information concerning _how_ to compute it. For this reason, models in Thinklab are usually small; the final, integrated algorithm that computes a top-level observable concept (e.g., "climate") is defined by _resolving_ each dependency to models, a process that may bring in new dependencies as models are chosen. Observation of a complex observable is successful when all the "end points" of the model chain are resolved models. At that point, Thinklab can create a _data flow_ from the model chain, and run that to compute the states of every observable involved, creating the final subject that contains observation for qualities and other subjects as its semantic constraints require.

## Model syntax: observable, dependencies and computations

A simple computed model may sum the state of its dependent qualities to obtain the state of its own quality observable:

	namespace my.namespace using im, im.agriculture;

	...

	model SummerCropYield as
		measure im:Summer im.core:Yield of im.agriculture:Crop in t/km^2
		observing
			(JunCropYield as measure im:June im.core:Yield of im.agriculture:Crop in t/km^2) named jun-yield,
			(JulCropYield as measure im:July im.core:Yield of im.agriculture:Crop in t/km^2) named jul-yield,
			(AugCropYield as measure im:August im.core:Yield of im.agriculture:Crop in t/km^2) named aug-yield
		on definition set to [jun-yield + jul-yield + aug-yield];

There are several new things to note in this example:

1. Instead of a data source, we specify a name after the `model` keyword, using the [capitalization conventions](0204.html) used for concepts. Indeed, we are creating a concept. Essentially this model is an ontological statement: we define the concept `my.namespace:SummerCropYield` while giving an interpretation of how it should be computed (i.e., as the sum of crop yields in June, July, and August). As noted in the introduction, each Thinklab namespace is an ontology: the `im.namespace` ontology that is created by Thinklab reading this statement contains the new concept.
2. A new keyword, `observing`, is followed by a comma-separated list of _dependencies_. Each dependency is a mini-model itself, but without computational details. Within each parenthesis, there is a concept ID for the observable and the full semantics of what is observed. The keyword `named` is used to give the dependency a name, which will only be used _within_ this model and won’t be recognized by other models, even those in the same namespace. This name is used to refer to the value of the observation in equations and conditions. The `im.namespace` ontology will also contain the three concepts corresponding to the first identifier, and a dependency relationship that links them to the main observable (SummerCropYield).
3. The last line, whose syntax we will describe later, shows the simplest way to compute a quality in Thinklab: an _expression_, defined within square brackets, that returns the value of the observation. In this simple expression, we use the names we defined above to refer to each operand (jun-yield, jul-yield, and aug-yield). 

## The context of applicability for a model

At this point, it may be unclear how this model handles time and space, because there are no time or space specifications in it. In Thinklab, when temporal and spatial constraints are not specified, it means that the model applies to any time and any location. In data models, the data source may implicitly contain a temporal and/or spatial context, which automatically becomes the context of validity for the model. For example, if an annotated dataset contains a raster map of elevation that covers Spain, Thinklab will not use it to resolve a context located in Greece. But the SummerCropYield model doesn't have a data source, so the model, as stated, covers every situation: not only every part of the world, but also any definition of SummerCropYield that has no associated space or time. Methods to instruct Thinklab on where and when this model should apply are covered in [Module 5](0105.html).

There is, however, one other important consideration to correct the SummerCropYield model presented above. The units used throughout the module are t/km^2 - implying a dependence on space and no dependency on time. So if this model was used in a non-spatial context (for example created with an `observe` statement without an `over space ...` clause), it would produce the wrong results, returning a _spatial density_ of yield (in t/ha) when a total yield (in t) is wanted. Indeed, the proper way to specify this model is

	model SummerCropYield as
		measure im:Summer im.core:Yield of im.agriculture:Crop in t/km^2
		observing
			(JunCropYield as measure im:June im.core:Yield of im.agriculture:Crop in t/km^2) named jun-yield,
			(JulCropYield as measure im:July im.core:Yield of im.agriculture:Crop in t/km^2) named jul-yield,
			(AugCropYield as measure im:August im.core:Yield of im.agriculture:Crop in t/km^2) named aug-yield
		over space
		on definition set to [jun-yield + jul-yield + aug-yield];

Adding the `over space` statement, without further specification, ensures that whatever the usage of this model, it will only apply to a context where any kind of space is defined. In anticipation of [Module 5]( 0105.html), it should be fairly intuitive that following the ‘over space’ statement with a polygon, as in an `observe` statement, will also limit the use of the model to contexts that cover that _particular_ space. 

Importantly, because a quality has a value over the whole context, this computation will be repeated as many times as necessary to cover a distributed context. So if, for example, this model is chosen to compute a dependency on SummerCropYield in another model that is run over a 10x10 km spatial grid using a 1 km^2 resolution, the equation will be computed 100 times (once for each of the 100 grid cells), each time with the value of the dependencies computed by another model or data in the same cell.

The identification of an observable as a density (e.g., SummerCropYieldDensity) should be avoided for the reasons discussed in [Module 2](0102.html) and further explained in the discussion on scale in [Module 5](0105.html). SummerCropYield should be called a density if the observable was _semantically_ a density, independent of the context; for example, the density of water. The fact of being _distributed_ over a context that is aware of space makes the observable a density only when observed in that context, not by _nature_. Aggregation, which in Thinklab is managed automatically, would remove the density “character” from the observation. The same considerations apply to time: if distributed over time, the units must have a temporal unit in the denominator, but it would be inappropriate to name the concept a SummerCropYieldRate.

## The observable in computed models

The most obvious difference between a “resolved” model that annotates data and one that is "unresolved" model is that the latter includes an observable concept definition after the `model` keyword. Indeed, a computed model creates new knowledge (or "reinterprets" it), and the concept statement defines its semantics. The concept may be defined before the model, or more than one model may be provided for the same concept; the first time an unknown name is encountered, a concept will be created with the semantics that depends on its use. Successive uses of the same name will need to be consistent with that semantics.

In some situations(detailed below), this concept can be written without a colon-separated namespace identifier, which we use to qualify an external ontology in the Thinklab [naming conventions](). This defines a concept that is "local" to the namespace within which the model is defined. So the model in the example above will in fact create the my.namespace:SummerCropYield concept. If the model has an observer, this concept will be a specialized version of the observable defined for it. So SummerCropYield will be in a "is-a" relationship with the SummerCropYield concept created by the `measure` statement inside the model, and will inherit all the traits from it. 

Compare the model above to a "resolved" data model described in the [Module 3](0103.html): only the observer semantics is stated for a resolved data model, because the data source takes the place of the top-level observable. Indeed, we are _annotating_ data so that specification is all we need. When we _compute_ data, we are defining new knowledge, hence the need for a concept after `model.`

When the primary observable is local to the namespace, we expect that the concept will only be useful inside of it, for example to use as a dependency in other models in the same namespace. This is done commonly when a namespace contains models for many different observables, all of which are used in a final model that brings the whole conceptualization together. 

In other situations, the models can use a primary observable concept that has been defined outside the namespace. This choice is normally made when the namespace is meant to provide an “alternative” way of observing a concept that is localized to a particular scale – for example to a given geographical region. In such cases, observations of the observable will only be made with these models _where_ the constraints are met – which may even be only a subsection of a full context of observation. This is discussed more in detail in the next sections; however, just note that models with a “known” (external) observable are typically useful in a shared context, while models with “local” observables are usually written to provide clarity and internal organization in namespaces that want to keep most of their models private. In the examples below, we only create knowledge in the local namespace, which can be used in outside models only when qualified with the full name for the concepts (e.g. my.namespace:Elevation).

## Mediation

Mediation is mainly used with classifications. It is a way to chain different observers together for use in models. Mediation introduces an implicit dependency for "another view" of the same observable. A typical example is:

	namespace my.namespace;

	...

	model ElevationLevel as
		classify (measure im.geography:Elevation in m) by im:Level into
			im:High if > 1000,
			im:Low  if < 1000;

This kind of specification nests a dependency within an observer, without assigning it a concept and a name. The example defines a _classification_ that depends on observing a _measurement_ of elevation. The part within parentheses is indeed a dependency, but because the semantics of the "inner" observation relates directly to that of the observable, and we don't need to preserve the value of the measurement for any computation, we can more simply set it in place of the concept to be observed by the classification. This kind of statement is a shorter, more intuitive and a more "fluent" idiom than the equivalent model expressed with dependencies. While the model below will produce the same results as the one above, it is longer, more complex, and less readable, so the above model would be highly preferable:

	model ElevationLevel as
		classify ElevationLevel by im:Trait into im:High, im:Low
		observing
			(Elevation as measure im.geography:Elevation in m) named elevation
		on definition set to [
			elevation < 1000 ? im:Low : im:High
		];

For this reason, mediation should always be used when it's appropriate, in place of a less readable model with one dependency.

## Expression language

The next few sections provide some guidance on how to write the expressions between the square brackets and where to learn more about them. The language used for these expressions is parsed by a [Groovy](http://groovy.codehaus.org) interpreter, after being pre-processed by Thinklab so that concepts and identifiers known to Thinklab can be used without error.
	
Groovy is a superset of the Java language, with less strict syntax rules and optimized for use in scripts and expressions, but containing all the power of Java plus numerous enhancements. More details about the language are outside the scope of this guide, but it is important to know that the language is much more powerful than the simple expressions we use as examples can show. Exploiting this power obviously requires programming skills. For the interested, there are many resources about the Groovy language, both on the Web and in print. 

The main things to know about the way Thinklab and Groovy interact are:

1. The identifiers used after the `named` keyword in dependencies can be used in Groovy as variables that contain the value being computed for that dependency (in each state determined by the scale, e.g., in each grid cell, with one expression evaluation per state). The [naming conventions used in Thinklab](0204.html) (lowercase and dash-separated) are not compatible with Groovy, which does not use dashes within identifiers. The names that are declared are automatically substituted in the expression before Groovy sees them, but if the wrong identifier is provided (i.e., due to a typo) that contains a dash, Thinklab will not substitute it, and Groovy will interpret that as a subtraction of two variables, yielding a potentially confusing error that will hint at only one part of the variable name being undefined (e.g. “population” when the expression contains “population-density” but that has not been defined as the name of a dependency).
2. The "no data" value, indicated in Thinklab with the keyword `unknown`,  is the equivalent of "null" in Groovy and Java, and will be translated to that before the expressions are passed to Groovy for calculations. If an expression has a chance of encountering an `unknown` value (e.g., from no-data points in a data source) it should be prepared to handle it. Groovy allows null values to be added and subtracted, making the result null without error; but multiplications, for example, will break the computation and produce an error. We are working on solutions to this problem, but for now statements may need to be “null-proofed” like this:

	[(a == unknown || b == unknown) ? unknown : a*b]

The ternary operator above (C ? Y : Z) will return Y if the condition C is true, or Z if it is false. In English, it is the equivalent of saying “is a unknown or b unknown? then return unknown; otherwise return the product of a and b”. Either `unknown` or `null` can be used interchangeably. Note the two equal signs - Groovy, like most languages, uses a single equal sign for assignments to variables, and the equality operator is `==`. If you need more complex expressions, please refer to Groovy documentation for details on syntax, which is compatible with the better known Java, and for functions you may call (e.g. mathematical functions).

3. The Groovy type of the values assigned to dependency names will be 1) a floating point number if it comes from a numeric observer (rank, measure, value, count, percentage, proportion, ratio, uncertainty); 2) a boolean (true/false) value if it comes from a `presence` observer; or 3) a concept if it comes from a `classify` observer. For those who can negotiate a Java/Groovy interface, concepts conform to the IConcept java interface described in the [Thinklab API documentation](). Proficiency in Java/Groovy allows access to many functions related to concepts, but for the uninitiated, it is usually enough to know that the operator "is" can be used for a concept, followed by a literal concept identifier, like so:

	[(land-cover-type == unknown || land-cover-type is im.landcover:Urban) ? 10 : 20]

The [Thinklab cookbook](0302.html) included in this documentation contains examples of expressions of common usage.

### Using expressions in data models

Expressions can be used to modify or create the value of _resolved_ models as well. It is quite normal to link to a data source that requires some processing before it can fully express the desired observation semantics. In such cases, a model this can be used:

	 model wcs(...) 
    	named ghg-emissions-usa
   		as measure im.policy:GreenhouseGasEmissions in t/ha*year
    	on definition change to [ghg-emissions-usa * 0.0001];

The above model will work as a data annotation, but will transform the value into a tenth of a thousand of what the WCS data source contains before producing the observation. Similar expressions can be used for more complex transformations or for filtering of values. Models can have both data sources and dependencies. Each model dependency will itself need to be resolved, but the final value can be assigned using expressions that take into account both the value of the data source and that of the dependencies. 

### Limitations

Processing of expressions in Thinklab is still fairly primitive, so there are several limitations that will be removed in future versions. The most important are:

1. Square brackets ([,]) can only be used within an expression if the closing bracket is _quoted_ using a backslash character, as in `\]`. If this is not done, the closing bracket will be interpreted as the end of the expression. This makes the use of the standard array notation with Groovy a bit awkward, although this is only of concern for modelers with coding skills. This limitation will be removed in the future.
2. The pre-processing of Thinklab concepts and symbols within expressions uses a fairly unsophisticated pattern matching algorithm, which may fail when identifiers contain other identifiers (for example, if dependencies have very simple names like "a" or "b"). When in doubt, use longer, more descriptive names and provide space around all identifiers. We will soon switch to a parser that has no such limitations.
3. The content of the expressions is not analyzed by Thinklab in the same way that rest of the model syntax is. The expression code is only parsed the first time when the model is run, so it is possible to write completely incorrect expressions that will not show errors in the editor yet produce errors at runtime. When models containing wrong expressions are run, errors will be reported that should be easy to relate to the offending expression. In the future, the expression parser will be integrated more deeply in the language so that syntax errors in expressions can be seen during editing.

Of course, no language analysis can identify _logical_ errors in the expressions. So always test models with particular care and under different scenarios of use when they contain expressions. 

## Dependencies in detail

As we have seen above, a list of dependencies is introduced by the keyword `observing.` In order to compute the observable, the dependencies in the list must first be observed. This applies to both qualities and subjects, as it may be desirable to observe subjects (e.g., households or bridges) in order to observe a larger-scale subject, for example a region, that contains them. On the other hand, remember that any dependency is also a semantic statement of a relationship between the observable of the model and that of the dependency. Based on our [definition of a quality](0203.html), it is pointless to depend on subjects when the observable is a quality. For this reason, Thinklab does not allow qualities to be part of a list of model dependencies, although qualities held by subjects may be referenced through the de-reification mechanism discussed above for subject models.

One general, but advanced, semantic specification that applies to both subject and quality dependencies is that of the _property_ that Thinklab adds to the underlying ontology to capture the semantics of the dependency. By _property_ we refer to the ontological specification of the semantics for the _connection_ between the two concepts. We have not discussed properties in these beginner-level modules, and this point can be safely skipped if it comes out as confusing. Otherwise, the specification uses the `for` keyword, like so:

	…
	observing
			(Elevation as measure im.geography:Elevation in m) for im.geography:hasElevation named elevation

This specification is necessary only when full control of the model semantics is desired. Thinklab will create a property automatically if one is not supplied, and in common modeling practice the specification is not required.

### Quality dependencies

In quality models, dependencies are a fairly simple affair: in addition to the local concept, the observer and a name, an optional status can also be specified:

	observing
		(Elevation as measure im.geography:Elevation in m) optional named elevation

This allows Thinklab to compute the model even if that dependency cannot be resolved to a model (a `mandatory` keyword is also provided for completeness, but it's the default setting). 

When an optional dependency cannot be resolved, the corresponding value will be `unknown.` In a deterministic calculation, this makes the result of any expression where it is used `unknown,`, even if other values are valid. In a [_Bayesian network_ (BN) model](0301.html) the BN will use prior probabilities instead of data; a result will be calculated but it will carry greater uncertainty than one where values can be resolved for all model dependencies. In the current Thinklab implementation there are some limitations to this behavior: for example multiplications do not yet allow unknown values. Such situations should be handled using the strategy explained in the expressions section below. This situation may arise even when the dependencies are resolved, e.g., when a dependency is resolved to data that contain "no data" values. So it's important that the expression code, or any other selected computation, is prepared to handle the resulting `unknown` value.

It is acceptable to specify the dependent observer and concept in an independent model as long as it is in the same namespace and defined before the model that uses it. For example:

	namespace my.namespace using im, im.geography;

	private model SoilPH as
		classify (rank im.geography:Soil im.chemistry:PH) by im:Level into
			im:High if > 5,
			im:Low otherwise;

	model SomethingDependentOnPH
		as ...
		observing
			SoilPH named soil-ph
		... ;


can be written using the name of the model (SoilPH) in place of a full definition of it in parentheses. This is mostly useful when the same model needs to be reused in the dependencies of several models below it. When using this syntax, dependent models should always be declared `private`, to ensure that they are not inadvertently used to resolve a dependency in another namespace. In most cases, based on this example, the choice of whether pH is high or low should be decided in the context of the model using those subjective levels. If other models use the *same* conceptualization of high or low pH, the above approach will ensure that the same interpretation is used throughout (or simply to save some typing). In a collaborative context, however, subjective definitions should not be allowed to potentially "contaminate" other namespaces by being available to interpret soil pH, even if it would only be used if my.namespace:SoilPH was referenced in a dependency. For example, other people may search for the concept and (inappropriately) use it based the fact that the name matches a modeling need of theirs, when the more correct approach would be for each modeler to define their own private models (Note: There will be cases where it is entirely appropriate to reuse a model in diverse contexts. This is why it is so important that the applicable context of each model be accurately recorded).

This syntax can also be used to "force" the use of a specific model to resolve a dependency, instead of letting Thinklab resolve the concepts itself. This should be reserved for special situations, as doing so "wires" models together in a rigid way, deactivating much of the utility of semantic modeling. If it is deemed desirable to wire models together this way, a better way would be to refer to the models by name:

	private model SoilPH named the-ph-we-want as
		classify (rank im.geography:Soil im.chemistry:PH) by im:Level into
			im:High if > 5,
			im:Low otherwise;

	model SomethingDependentOnPH
		as ...
		observing
			the-ph-we-want named soil-ph
		... ;

This linkage is guaranteed to work even if there are other models for the same concept in the same namespace (e.g., assigned to different spatial locations). As shown above, the name of a model is specified using the `named` keyword that follows the observable. As per Thinklab [naming conventions](), lowercase, dash-separated names are used for such identifiers. If needed, a model from another namespace can be used as long as it has been imported in the `namespace` declaration at the top of the namespace (the list following the word “using”):

	namespace my.namespace using im, im.geography, (the-ph-we-want) from my.ph.models;

	...

	model SomethingDependentOnPH
		as ...
		observing
			the-ph-we-want named soil-ph
		... ;

A list of symbols within parentheses can be used to identify imported identifiers (multiple identifiers can be included in that list, separated by commas). An asterisk (`*`) can be used to import all symbols from a namespace, although this can generate confusion and unexplained errors (e.g., symbol redefinition) when either namespace is changed, so this should not be done routinely. When no list of symbols is given, as we normally do with imported ontologies, the system merely records the _dependency_ of the namespaces, and ensures that the dependency is read before the namespace that depends on it is parsed. Note that circular dependencies should be avoided: the system will not complain about them, but the results may be unpredictable and symbols or concepts that are expected to be defined may not look so when circular dependencies are defined.

Again, the direct use of models in dependencies is a special situation that shouldn't be the norm. In the normal case, when an observer is specified with the dependency, quite a bit of information can be embedded in the dependency statement. This is helpful to "localize" subjective interpretations to the model they're meant for. This comes in handy for example when using a subjective trait like im:Level, which is likely to only have a precise meaning within each computation. The example above, for instance, can be written in one single model:

	model SomethingDependentOnPH
		as ...
		observing
			(SoilPH as
				classify (rank im.geography:Soil im.chemistry:PH) by im:Level into
					im:High if > 5,
					im:Low otherwise) named soil-ph
		... ;


This way, the SoilPH concept will only have the semantic scope of the model it's in, and will never be used outside of it or appear in searches. This is the safest way to specify dependencies on observables that are interpreted in ways that only apply to one or a few models in a namespace; this approach should thus be adopted as a default.
	
A special syntax can be used when defining a dependency - a classification `by` trait:

	...
	im.ecology:Forest by im.conservation:DegradationLevel
	...

Because the word `by` is reserved for classifications according to a specific subjective trait, the statement above can be used without ambiguity instead of a much more verbose `classify` statement. The output of this dependency will be one of the possible values of `im:DegradationLevel` (`im:Low`, `im:Medium`, ....) when matched to a model that defines that trait for a `im.ecology:Forest`. This alternative syntax may help make model specification as parsimonious and intuitive as possible.

### Subject models and dependencies

Subject models instruct Thinklab to observe _subjects_ within the context of interest. At this point it may be difficult to understand what a subject model _does_: indeed, subjects are _created_ more than _computed_, so what are subject models for? 

As suggested above, subject dependencies are only admitted in subject models, because there is a contextual relationship between the dependent and the observable that makes no sense for a quality. For example, temperature can be observed in the context of a region, based on the observation of the region, but the region to which a temperature refers cannot be observed based on a measure of temperature. 

Because subject observations are _direct_ and do not need observers, their use as dependencies is fairly simple. For example, the following statement:

	model ...
		observing im.infrastructure:Road;

is all it takes for Thinklab to look up a model that annotates a source of subjects annotated with the Road concept, e.g., a vector file. will lookup data or models that can produce roads in the context, and if found, the roads will be created. Each road is a subject in itself, linked to the root subject.

Like with qualities, a property (`for` keyword) and an optional status for the dependency can be specified. Adding a name (`named`) is possible, but not useful, because the model will not refer to the "value" of a subject in any expression, and it’s possible for zero subjects to be obtained as the result of the observation (e.g., roads can be observed in a region that has no roads, and a perfectly good observation of no roads can be obtained). Names for dependent subjects should be attributed, when appropriate, directly in the subject sources using attributes, as in the example we showed in the previous section. 

The same syntax works with concepts describing events and processes, since each of these observers ["stand alone" and are observed directly](0102.html).

When the dependency is on a subject, process, or event, there is another, very useful syntax that enables complex agent-based models to be initialized in a very simple way. It is quite common for a model to want to create subjects where no subject sources are available, but where subject sources may provide a _context_ for the subjects we want. For example, we may have subject sources for households (say a vector file with a point per household) but want a model containing agents for the families inside them. The use of the keyword `each`, which we have seen in subject models before, inside the dependencies allows to specify that:

	model …
		observing im.demography:Family at each im.demography:Household;

This dependency will first try to observe households in the context of observation; if any are found, Thinklab will extract the context (e.g. the location in space) for each of them, and create a Family at that same context. As with any regular subject, the family semantics will be used to define any further observation; for example, if we have told Thinklab that each family must have an income quality associated, then Thinklab will attempt to observe that income for each family generated (this is explained in more detail below). If the knowledge base contains models for a Family, the most appropriate for the context will be retrieved and used to initialize each family, which may also create qualities or subjects (e.g. individual members) in each Family subject. Backed by a knowledge base with contributions from many collaborating participants, the single line dependency above may build a whole simulated world.

### Resolving dependencies vs. making observations in a context

By this point it may be evident that the modeling workflow we outlined in [Module 1](0101.html) (creating a subject then observing concepts of interest within it) is an exact equivalent of writing a model with all the concepts of interested as dependencies then observing that model in the context of the root subject. The only difference is that in the second case, a _generic quality_ is observed without specifying its observer. For example, if the Elevation concept is modeled in a region, the output will be the best observation of Elevation there, whether in feet or meters. By stating a dependency with its observer, specific observation semantics are guaranteed. Obviously this is crucial if the resulting values need to be used in equations, so it would not be acceptable in such a model to write:

	model ... 
		observing im.geography:Elevation;

because there would be no control over the meaning of the numbers output by such a model. The form above is, however, fine for subjects, because subjects are observed directly, so there is no interpretation for their values. As a result, Thinklab only allows the extremely simple syntax shown above when the concept specifies a subject.

### Automatically resolved dependencies

In some cases, the ontologies (i.e., abstract knowledge) contain conditions for some observable’s semantic coherence that _require_ particular observations to be made. For example, the definition of a concept for a Watershed may include a _restriction_ that a watershed must have a spatial configuration that allows a stream network to exist within it. This can be expressed in the ontologies using statements that look like:
	
		thing Watershed is im.geography:Region
			requires StreamNetwork;

If such requirements are present, Thinklab will automatically try to make the necessary observations when a subject like a Watershed is observed. This is because everything in Thinklab is semantically explicit, and the statement above says that no watershed can exist if it does not have an observable stream network. By using semantic restrictions, many tasks that are usually done explicitly by modelers become automatic. When a concept like a Watershed is used as the inherent context of the observable (e.g. `Runoff of Watershed`), it is guaranteed that the Runoff model will be able to access the stream network. If the model is run at all, it will run in a Watershed, and a Watershed can only exist if a StreamNetwork can also be observed.

## Actions linked to transitions

The examples so far have been fairly simple; however, complex operations used in other modeling systems can also be used within Thinklab, such as temporally explicit simulation specifying discrete differential equations for rates of change, or agent-based models. 

All these functionalities are enabled in Thinklab by connecting _actions_ to _transitions_. The full set of possible actions and transitions is both under active development and beyond the intended level of these modules. We will introduce the ideas here for completeness, though they are currently not fully functional and the final syntax may differ.

[Module 5](0105.html) describes how _scale_ is represented in Thinklab, but we have already encountered examples where we assign spatial and/or temporal _extents_ to subjects. Those extents may be represented in a way that implies multiple states (e.g., grid cells or polygons for space, time steps for time). In such cases, Thinklab will choose a computation sequence for these multiple states, and carry on the observation by generating _scale transitions_. The moment when the simulated time moves from _t_ to  _t+1_ is a time transition. 

Dynamic simulation gets is power by attaching _actions_ to time transitions. That is specified using a statement we have seen already:

	model ....
		over time
			integrate population-size as [population-size + birth - death],
			change land-use to im.landcover:Urban if [population-size > 100];

Agent-based modeling is accomplished by providing syntax for dependent subjects to make changes and to access their own observed world from within expressions.

The 'over time/space/...' syntax allows a full specification of scale for each individual model. This allows a fully multi-scale system to be specified very simply. It works very nicely with subject models like so:

	model AdministrativeRegion 
		observing
			Household at each HouseholdLocation,
			Administration at each CapitalCity;

	....

	model Household
		....
		over time (step="1 day")
			....;

	model AdministrativeRegion
		...
		over time (step = "30 day")
			...;

This example illustrates in a simple way how each model can specify a different temporal resolution, which will be automatically negotiated by Thinklab. Of course all these models are linked automatically through the resolution process. Thinklab may thus choose a _different_ household model for only _some_ of the households, say for example when the poverty level is above a threshold. These examples show how the investment in learning to “think semantically” pays off in simplicity. Because the meaning of all involved entities are specified fully and unambiguously, the software can wire components and data together properly, accomplishing difficult tasks that would normally fall to the modeler.

The `on definition` syntax previously seen in the section on [data annotations](0103.html) is a special case of a transition: the difference is that when we say `on definition,` we refer to the _initialization transition_, which brings the model from the uninitialized to an initialized state, just before the temporal component of a simulation begins. The `on definition` syntax allows a list of actions just like `over ....` However, some transitions that require the existence of time (e.g., `integrate`) are not allowed.

## Bridging to external computations

Expressions in Thinklab are full Groovy programs, with which complex models and strategies can be coded. Yet, that requires programming skills, and deferring the logics of complex models to Groovy code is certainly not the primary reason for Thinklab to exist. In fact, many computations that are of interest to modelers are handled by external software, and when possible it is much simpler and cheaper to reuse external modeling software than to write new code to use inside Thinklab – particularly when the modeling software has an independent history and is maintained by others. For this reason, Thinklab provides a mechanism for a model to bridge to any external software that can be run from a language supported by the Thinklab implementation. This includes nearly all non-GUI based programs, as a modeling engine can easily integrate software and run external executables - but cannot as easily click buttons or fill forms.

The link between Thinklab and external computations is established using function syntax that follows the `using` keyword:

	model SoilCarbonStored as
	measure aries.carbon:SoilCarbonStored in t/ha
	discretized by im:Level into 
       im:VeryHigh if 200  to  520,
       im:High     if 110  to  200,
       im:Moderate if  90  to  110,
       im:Low      if  50  to   90,
       im:VeryLow  if 0.01 to   50,
       im:Minimal  if 0    to  0.01
    observing
       im.geography:Slope by im:Level,
       im.soil:SoilPh by im:Level,
       ....
    using bayesian(import="bn/madagascar/sink.xdsl");

The last line includes the `bayesian` function, which defines what we call a _state accessor_. This particular accessor is initialized by reading the file specified with the `import` argument from the project where the model is defined. The file specifies a Bayesian network which is then used to compute the value for each point in the context. The accessor will be passed the value of all dependencies and will compute the SoilCarbonStored result.

There are many accessors available in Thinklab in addition to the Bayesian one illustrated above. They include random number generators, table and spreadsheet readers, random choosers for outcomes based on probability distributions that can be parameterized using observed dependencies, and GIS operations. The existing ones are listed in the [full documentation](). Because function names are not keywords of the language, it is very easy for a developer to add new functions when needed, and the list of available accessors will keep growing with time.

Just as quality models can use the `using` syntax to pass off the computation of values to an external accessor, subject models can have _subject accessors_ that operate on the subject as a whole, after all its quality dependencies have been initialized. For example, this is the current definition of the Watershed module included in the Thinklab hydrology ontology:

	model im.hydrology:Watershed,
    	//  pit-filled land elevation.
      	(im.hydrology:Elevation as measure im.hydrology:Elevation in m),
      	(im.hydrology:FlowDirection as measure im.hydrology:FlowDirection in degree_angle),
      	(im.hydrology:TotalContributingArea as measure im.hydrology:TotalContributingArea in m^2)
      	...
    	observing
        	(Elevation as measure im.geography:ElevationSeaLevel in m)
    	over space
    	using hydrology.watershed();

The bulk of the hydrological computations is not specified as a giant expression at the end, but is left to the subject accessor named in the last line, which will be passed the full digital elevation map (guaranteed to be a map by the `over space` clause) and produce all the outputs indicated in the list. Subject accessors provide convenient ways to encapsulate complex computations in clean “packages” that can be written as Thinklab plug-ins in a variety of languages, providing unlimited points of extension associated to specific semantics while keeping the code clean and readable.

### Multiple observables

A model doesn't necessarily have only one output: indeed, most non-trivial models, such as the watershed model shown above or any Bayesian network with intermediate nodes, usually have more than one. If we want to keep results for future analysis beyond the "primary" observable that comes after the keyword `model,` we must provide observers for all the qualities that the model produces (subjects are directly observed, so they don't require further specification). In the example above, we have used the same syntax that we used for dependencies to provide an observer for each additional output we want to keep. The accessor will be passed a list of the inputs and outputs, and will negotiate with the underlying model code to ensure that these can be produced and passed to Thinklab once computed, to become part of the final “dataset” represented by the subject.
	
The same specification can be used for anything computed by a state accessor. For example, a Bayesian network can be provided with observers for all the computed intermediate nodes and their relative uncertainties, using the `uncertainty` observer. The implementation of the accessor will ensure that the passed observers correspond to nodes in the network that can be computed, and will then create the corresponding observations.

Each of the observables in a model can be used to resolve dependencies within other models on its observable. All else being equal, Thinklab will try to minimize the number of models used, choosing a single model that produces two required observables over two models that produce the same observables independently, unless the latter are "evidence" (data) models. 


